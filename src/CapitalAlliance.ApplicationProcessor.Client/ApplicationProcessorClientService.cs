﻿using CapitalAlliance.ApplicationProcessor.Request;
using LendFoundry.Business.Application;
using LendFoundry.Business.OfferEngine;
using LendFoundry.Foundation.Client;
using LendFoundry.ProductRule;
using LendFoundry.Syndication.Experian.Response;
using LendFoundry.Syndication.LexisNexis.Bankruptcy;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport;
using LendFoundry.Syndication.Paynet.Response;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Business.Applicant;
using LendFoundry.Syndication.LexisNexis.ADLFunctions;

namespace CapitalAlliance.ApplicationProcessor.Client
{
    public class ApplicationProcessorClientService : IApplicationProcessorClientService
    {
        public ApplicationProcessorClientService(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task<IApplicationResponse> SubmitBusinessApplication(ISubmitBusinessApplicationRequest submitBusinessApplicationRequest)
        {
            var request = new RestRequest("submit/business", Method.POST);
            request.AddJsonBody(submitBusinessApplicationRequest);
            return await Client.ExecuteAsync<ApplicationResponse>(request);
        }
        public async Task<IApplicationDetails> AddLead(ISubmitBusinessApplicationRequest submitApplicationRequest)
        {
            var request = new RestRequest("lead", Method.POST);
            request.AddJsonBody(submitApplicationRequest);
            return await Client.ExecuteAsync<ApplicationDetails>(request);
        }

        public async Task<bool> SubmitDocument(string entityId, string category, byte[] fileBytes, string fileName, List<string> tags)
        {
            var objRequest = new RestRequest("{entityid}/{category}/{*tags}", Method.POST);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddUrlSegment("category", category);
            AppendTags(objRequest, tags);
            objRequest.AddFile("file", fileBytes, fileName, "application/octet-stream");
            return await Client.ExecuteAsync<bool>(objRequest);
        }

        public async Task<bool> SignConsent(string entityId, string category, object body)
        {
            var objRequest = new RestRequest("{entityid}/{category}/consent/sign", Method.POST);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddUrlSegment("category", category);
            objRequest.AddParameter("body", body);
            return await Client.ExecuteAsync<bool>(objRequest);
        }

        private static void AppendTags(IRestRequest request, IReadOnlyList<string> tags)
        {
            if (tags == null || !tags.Any())
                return;

            var url = request.Resource;
            for (var index = 0; index < tags.Count; index++)
            {
                var tag = tags[index];
                url = url + $"/{{tag{index}}}";
                request.AddUrlSegment($"tag{index}", tag);
            }
            request.Resource = url;
        }

        public async Task<ISearchBusinessResponse> BusinessSearch(string entityId, IBusinessSearchRequest request)
        {
            var objRequest = new RestRequest("{entityid}/experian/business/search", Method.POST);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<SearchBusinessResponse>(objRequest);
        }

        public async Task<IBusinessReportResponse> GetBusinessReport(string entityId, IBusinessReportRequest request)
        {
            var objRequest = new RestRequest("{entityid}/experian/business/report", Method.POST);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddParameter("request", request);
            return await Client.ExecuteAsync<BusinessReportResponse>(objRequest);
        }

        public async Task<IPersonalReportResponse> GetPersonalReport(string entityId, IPersonalReportRequest request)
        {
            var objRequest = new RestRequest("{entityid}/report/personal", Method.POST);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<PersonalReportResponse>(objRequest);
        }

        public async Task<string> ChooseOffer(string entityId, string offerId)
        {
            var objRequest = new RestRequest("{entityid}/{offerid}/offers/final", Method.GET);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddUrlSegment("offerId", offerId);
            return await Client.ExecuteAsync<string>(objRequest);
        }

        public async Task<string> PerformBusinessVerification1(string entityId)
        {
            var objRequest = new RestRequest("{entityid}/businessverification1", Method.GET);
            objRequest.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<string>(objRequest);
        }

        public async Task<string> PerformBusinessVerification2(string entityId)
        {
            var objRequest = new RestRequest("{entityid}/businessverification2", Method.GET);
            objRequest.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<string>(objRequest);
        }

        public async Task<string> ResendBankLinking(string entityId,string emailId)
        {
            var objRequest = new RestRequest("{entityid}/resend/banklink/{emailId?}", Method.POST);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddUrlSegment("emailId", emailId);
            return await Client.ExecuteAsync<string>(objRequest);
        }

        public async Task<IApplicationOffer> ComputeOffer(string entityId)
        {
            var objRequest = new RestRequest("{entityid}/compute/offer", Method.GET);
            objRequest.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<ApplicationOffer>(objRequest);
        }

        public async Task<IList<IBusinessLoanOffer>> PresentOffer(string entityId)
        {
            var objRequest = new RestRequest("{entityid}/present/offer", Method.GET);
            objRequest.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<IList<IBusinessLoanOffer>>(objRequest);
        }

        public async Task<string> GenerateAndSendAgreement(string entityId)
        {
            var objRequest = new RestRequest("{entityid}/generate/agreement", Method.GET);
            objRequest.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<string>(objRequest);
        }

        public async Task<ISearchCompanyResponse> SearchCompany(string entityId, ISearchCompanyRequest request)
        {
            var objRequest = new RestRequest("{entityid}/paynet/search", Method.POST);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<SearchCompanyResponse>(objRequest);
        }

        public async Task<bool> GetPaynetReport(string entityId, string paynetId)
        {
            var objRequest = new RestRequest("{entityid}/{paynetId}/paynet/report", Method.GET);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddUrlSegment("paynetId", paynetId);
            return await Client.ExecuteAsync<bool>(objRequest);
        }

        public async Task<string> PerformCreditCheckVerification(string entityId)
        {
            var objRequest = new RestRequest("{entityid}/creditcheckverification", Method.GET);
            objRequest.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<string>(objRequest);
        }

        public async Task<string> PerformBusinessTaxIdVerification(string entityId)
        {
            var objRequest = new RestRequest("{entityid}/businesstaxidverification", Method.GET);
            objRequest.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<string>(objRequest);
        }

        public async Task<string> PerformBusinessReviewCreditRisk(string entityId)
        {
            var objRequest = new RestRequest("{entityid}/businessreviewcreditrisk", Method.GET);
            objRequest.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<string>(objRequest);
        }

        public async Task<IApplicationDetails> GetApplicationDetails(string entityId)
        {
            var request = new RestRequest("{entityid}/details", Method.GET);
            request.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<ApplicationDetails>(request);
        }

        public async Task<IApplicationDetails> EditApplication(string entityId, ISubmitBusinessApplicationRequest request)
        {
            var objRequest = new RestRequest("{entityid}/edit", Method.PUT);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<ApplicationDetails>(objRequest);
        }

        public async Task<IApplicationOffer> AddDeal(string entityId, IDealOfferRequest request)
        {
            var objRequest = new RestRequest("{entityid}/add/deal", Method.POST);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<ApplicationOffer>(objRequest);
        }

        public async Task<IADLSearchResponse> ADLSearch(string entityid, SearchADLRequest request)
        {
            var objRequest = new RestRequest("{entityId}/adl/search", Method.POST);
            objRequest.AddUrlSegment("entityid", entityid);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<ADLSearchResponse>(objRequest);
        }

        public async Task<IBankruptcySearchResponse> BankruptcySearch(string entityId, Request.ISearchBankruptcyRequest request)
        {
            var objRequest = new RestRequest("{entityid}/bankruptcy/search", Method.POST);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<BankruptcySearchResponse>(objRequest);
        }

        public async Task<IBankruptcyReportResponse> BankruptcyReport(string entityId, Request.IBankruptcyReportRequest request)
        {
            var objRequest = new RestRequest("{entityid}/bankruptcy/report", Method.POST);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<BankruptcyReportResponse>(objRequest);
        }

        public async Task<ICriminalRecordSearchResponse> CriminalRecordSearch(string entityId, ICriminalRecordSearchRequest request)
        {
            var objRequest = new RestRequest("{entityid}/criminalrecord/search", Method.POST);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<CriminalRecordSearchResponse>(objRequest);
        }

        public async Task<ICriminalReportResponse> CriminalRecordReport(string entityId, ICriminalRecordReportRequest request)
        {
            var objRequest = new RestRequest("{entityid}/criminalrecord/report", Method.POST);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<CriminalReportResponse>(objRequest);
        }

        public async Task AddFundingRequest(string entityid)
        {
            var objRequest = new RestRequest("{entityid}/add/fundingrequest", Method.POST);
            objRequest.AddUrlSegment("entityid", entityid);
            await Client.ExecuteAsync(objRequest);
        }

        Task<IProductRuleResult> IApplicationProcessorClientService.ComputeOffer(string entityId)
        {
            throw new NotImplementedException();
        }

        public Task<IProductRuleResult> GetComputedOffer(string entityId, object offerData)
        {
            throw new NotImplementedException();
        }

        public Task CashflowCalculationCompletedHandler(string entityType, string entityId, object data)
        {
            throw new NotImplementedException();
        }

        public Task<bool> SignDocumentReceivedHandler(string entityType, string entityId, object data)
        {
            throw new NotImplementedException();
        }

        public Task<string> EditCashflow(string entityId, Dictionary<string, object> data)
        {
            throw new NotImplementedException();
        }

        public Task<object> GetSelectedCashflowDataAttribute(string entityId)
        {
            throw new NotImplementedException();
        }

        public Task<bool> BanKLinkRequestedHandler(string entityType, string entityId, object data)
        {
            throw new NotImplementedException();
        }

        public Task FundingStatusHandler(string entityType, string entityId, object data)
        {
            throw new NotImplementedException();
        }
        public Task<string> MCAAgreementHandler(string entityType, string entityId, object data)
        {
            throw new NotImplementedException();
        }

        public Task UpdateSICCode(string entityId, string sicCode)
        {
            throw new NotImplementedException();
        }
        public Task<object> GetApplicationDocuments(string entityId, string documentName)
        {
            throw new NotImplementedException();
        }
        public Task<bool> ExperianBusinessCreditReporHandler(string entityType, string entityId, object data)
        {
            throw new NotImplementedException();
        }

        public Task CashflowAccountSelectedHandler(string entityType, string entityId, object data)
        {
            throw new NotImplementedException();
        }

        public Task<ICashFlowResponse> GetcashflowAccountDetails(string entityid)
        {
            throw new NotImplementedException();
        }

        public Task<ICashFlowResponse> GetFundingAccountDetails(string entityid)
        {
            throw new NotImplementedException();
        }

        public Task<List<IOwner>> GetOwners(string entityId)
        {
            throw new NotImplementedException();
        }

        public Task SwitchOwner(string entityId, string ownerId)
        {
            throw new NotImplementedException();
        }
        public Task NonPrimaryExperianCallHandler(string entityType, string entityId, object data)
        {
            throw new NotImplementedException();
        }

        public Task ApplicationPDFHandler(string entityType, string entityId, object data)
        {
            throw new NotImplementedException();
        }

        public Task MCAAgreementCreationHandler(string entityType, string entityId, object data)
        {
            throw new NotImplementedException();
        }

        public Task<Dictionary<string, object>> ComputeProvisionalOffer(string entityId)
        {
            throw new NotImplementedException();
        }

        public Task<bool> CheckReApplyEligibility(string entityId)
        {
            throw new NotImplementedException();
        }

        public Task<bool> CheckRevivalEligibility(string entityId)
        {
            throw new NotImplementedException();
        }

        public Task ReviveApplication(string entityId)
        {
            throw new NotImplementedException();
        }
        public Task ExperainPersonalPDFHandler(string entityType, string entityId, object data)
        {
            throw new NotImplementedException();
        }
        public Task EnableReapply(string entityId)
        {
            throw new NotImplementedException();
        }
        public Task ApplicationPreApprovedHandler(string entityType, string entityId, object data)
        {
            throw new NotImplementedException();
        }

        public Task LoanOnBoardHandler(string entityType, string entityId, object data)
        {
            throw new NotImplementedException();
        }

        public Task AddAttribute(IAddAttributeRequest request)
        {
            throw new NotImplementedException();
        }

        public async Task<object> GetExtractedData(string entityId,string key)
        {
            var objRequest = new RestRequest("{entityid}/getextracteddata/{key}", Method.GET);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddUrlSegment("key", key);
            return await Client.ExecuteAsync<object>(objRequest);
        }

    }
}