﻿using System;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;

namespace CapitalAlliance.ApplicationProcessor.Client
{
    public class ApplicationProcessorClientServiceFactory : IApplicationProcessorClientServiceFactory
    {
        public ApplicationProcessorClientServiceFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }

        public ApplicationProcessorClientServiceFactory(IServiceProvider provider, Uri uri = null, int cachingExpirationInSeconds = 60)
        {
            Provider = provider;
            Uri = uri;
            CachingExpirationInSeconds = cachingExpirationInSeconds;
        }

        private int CachingExpirationInSeconds { get; }
        private Uri Uri { get; }
        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public IApplicationProcessorClientService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new ApplicationProcessorClientService(client);
        }
    }
}
