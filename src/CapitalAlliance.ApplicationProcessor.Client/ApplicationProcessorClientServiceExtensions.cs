﻿using LendFoundry.Security.Tokens;
using Microsoft.Extensions.DependencyInjection;

namespace CapitalAlliance.ApplicationProcessor.Client
{
    public static class ApplicationProcessorClientServiceExtensions
    {
        public static IServiceCollection AddOfferEngineService(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddTransient<IApplicationProcessorClientServiceFactory>(p => new ApplicationProcessorClientServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IApplicationProcessorClientServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
