﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Xsl;
using CapitalAlliance.ApplicationProcessor.Events;
using CapitalAlliance.ApplicationProcessor.Exceptions;
using CapitalAlliance.ApplicationProcessor.Request;
using LendFoundry.Consent;
using LendFoundry.StatusManagement;
using LendFoundry.Syndication.BBBSearch;
using LendFoundry.Application.Document;
using LendFoundry.AssignmentEngine;
using LendFoundry.Business.Applicant;
using LendFoundry.Business.Application;
using LendFoundry.Business.Funding;
using LendFoundry.Business.OfferEngine;
using CapitalAlliance.Cashflow;
using LendFoundry.DocumentGenerator;
using LendFoundry.Email;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Partner;
using LendFoundry.ProductConfiguration;
using LendFoundry.ProductRule;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Syndication.Experian;
using LendFoundry.Syndication.Experian.Request;
using LendFoundry.Syndication.Experian.Response;
using LendFoundry.Syndication.LexisNexis;
using LendFoundry.Syndication.LexisNexis.ADLFunctions;
using LendFoundry.Syndication.LexisNexis.Bankruptcy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport;
using LendFoundry.Syndication.LFDocuSign;
using LendFoundry.Syndication.LFDocuSign.Models;
using LendFoundry.Syndication.Paynet;
using LendFoundry.Syndication.Paynet.Response;
using LendFoundry.Syndication.Yelp;
using LendFoundry.Syndication.Yelp.Request;
using LendFoundry.Syndication.Yelp.Response;
using LendFoundry.TemplateManager;
using LendFoundry.Token.Generator;
using LendFoundry.VerificationEngine;
using LMS.Foundation.Amortization;
using LMS.LoanManagement.Abstractions;
using LMS.LoanManagement.Client;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CapitalAlliance.ApplicationProcessor
{
    public class ApplicationService : IApplicationProcessorClientService
    {
        #region Constructor

        public ApplicationService(ApplicationProcessorConfiguration applicationProcessorConfiguration, LendFoundry.DataAttributes.IDataAttributesEngine dataAttributesService,
            ILookupService lookupService, IApplicationService businessApplicationService,
            IEventHubClient eventHub, IApplicationDocumentService applicationDocumentService, IDocuSignService docuSignService,
            IDocumentGeneratorService documentGeneratorService,
            IVerificationEngineService verificationEngineService,
            IEntityStatusService entityStatusService,
            IApplicantService businessApplicantService,
            IOfferEngineService offerEngineService,
            IConsentService consentService,
            IPersonalReportService personalReportService,
            ITenantTime tenantTime,
            ILogger logger,
            ICashflowService cashflowService,
            IFundingService fundingService,
            ILoanManagementClientService loanManagementService
        )
        {
            if (businessApplicationService == null)
                throw new ArgumentNullException(nameof(businessApplicationService));

            if (businessApplicantService == null)
                throw new ArgumentNullException(nameof(businessApplicantService));

            if (offerEngineService == null)
                throw new ArgumentNullException(nameof(offerEngineService));

            if (consentService == null)
                throw new ArgumentNullException (nameof (consentService));    

            if (eventHub == null)
                throw new ArgumentNullException(nameof(eventHub));

            if (applicationDocumentService == null)
                throw new ArgumentNullException(nameof(applicationDocumentService));

            if (docuSignService == null)
                throw new ArgumentNullException(nameof(docuSignService));

            if (documentGeneratorService == null)
                throw new ArgumentNullException(nameof(documentGeneratorService));

            if (dataAttributesService == null)
                throw new ArgumentNullException(nameof(dataAttributesService));

            if (verificationEngineService == null)
                throw new ArgumentNullException(nameof(verificationEngineService));

            if (entityStatusService == null)
                throw new ArgumentNullException(nameof(entityStatusService));

            if (personalReportService == null)
                throw new ArgumentNullException(nameof(personalReportService));

            if (tenantTime == null)
                throw new ArgumentNullException(nameof(tenantTime));

            LookupService = lookupService;
            BusinessApplicationService = businessApplicationService;
            ApplicationProcessorConfiguration = applicationProcessorConfiguration;
            EventHub = eventHub;
            ApplicationDocumentService = applicationDocumentService;
            DocuSignService = docuSignService;
            DocumentGeneratorService = documentGeneratorService;
            DataAttributeService = dataAttributesService;
            VerificationEngineService = verificationEngineService;
            EntityStatusService = entityStatusService;
            BusinessApplicantService = businessApplicantService;
            OfferEngineService = offerEngineService;
            ConsentService = consentService;
            PersonalReportService = personalReportService;
            TenantTime = tenantTime;
            Logger = logger;
            LoanManagementService = loanManagementService;
            CashflowService = cashflowService;
            FundingService = fundingService;
        }

        public ApplicationService(ApplicationProcessorConfiguration applicationProcessorConfiguration, IEntityStatusService entityStatusService,
            ILookupService lookupService,
            IApplicationService businessApplicationService,
            IYelpService yelpService,
            IEmailService emailService,
            IBBBSearchService bbbSearchService,
            LendFoundry.DataAttributes.IDataAttributesEngine dataAttributeService,
            IEventHubClient eventHub,
            IApplicationDocumentService applicationDocumentService,
            IConsentService consentService,
            LendFoundry.Syndication.Experian.IBusinessReportService businessReportService,
            IPersonalReportService personalReportService,
            IVerificationEngineService verificationEngineService,
            IDocuSignService docuSignService,
            IPaynetService paynetReportService,
            IApplicantService businessApplicantService,
            IOfferEngineService offerEngineService,
            ILexisNexisService lexisNexisService,
            IProductRuleService productRuleService,
            IAssignmentService assignmentService,
            IIdentityService identityService,
            IDocumentGeneratorService documentGeneratorService,
            IFundingService fundingService,
            ITokenGeneratorService tokenGeneratorService,
            IPartnerService partnerService,
            ICashflowService cashflowService,
            ITenantTime tenantTime, ILogger logger,
            ILoanManagementClientService loanManagementService,
            IProductService productService
        )
        {
            if (partnerService == null)
                throw new ArgumentNullException(nameof(partnerService));
            if (businessApplicationService == null)
                throw new ArgumentNullException(nameof(businessApplicationService));

            if (businessApplicantService == null)
                throw new ArgumentNullException(nameof(businessApplicantService));

            if (entityStatusService == null)
                throw new ArgumentNullException(nameof(entityStatusService));

            if (yelpService == null)
                throw new ArgumentNullException(nameof(yelpService));

            if (bbbSearchService == null)
                throw new ArgumentNullException(nameof(bbbSearchService));

            if (emailService == null)
                throw new ArgumentNullException(nameof(emailService));

            if (dataAttributeService == null)
                throw new ArgumentNullException(nameof(dataAttributeService));

            if (eventHub == null)
                throw new ArgumentNullException(nameof(eventHub));

            if (applicationDocumentService == null)
                throw new ArgumentNullException(nameof(applicationDocumentService));

            if (consentService == null)
                throw new ArgumentNullException(nameof(consentService));

            if (businessReportService == null)
                throw new ArgumentNullException(nameof(businessReportService));

            if (personalReportService == null)
                throw new ArgumentNullException(nameof(personalReportService));

            if (verificationEngineService == null)
                throw new ArgumentNullException(nameof(verificationEngineService));

            if (paynetReportService == null)
                throw new ArgumentNullException(nameof(paynetReportService));

            if (docuSignService == null)
                throw new ArgumentNullException(nameof(docuSignService));

            if (offerEngineService == null)
                throw new ArgumentNullException(nameof(offerEngineService));

            if (productRuleService == null)
                throw new ArgumentNullException(nameof(productRuleService));

            if (assignmentService == null)
                throw new ArgumentNullException(nameof(assignmentService));

            if (identityService == null)
                throw new ArgumentNullException(nameof(identityService));

            if (documentGeneratorService == null)
                throw new ArgumentNullException(nameof(documentGeneratorService));

            if (fundingService == null)
                throw new ArgumentNullException(nameof(fundingService));

            if (tokenGeneratorService == null)
                throw new ArgumentNullException (nameof (tokenGeneratorService));

            if (cashflowService == null)
               throw new ArgumentNullException(nameof(cashflowService));

            if (tenantTime == null)
                throw new ArgumentNullException(nameof(tenantTime));

            LookupService = lookupService;
            BusinessApplicationService = businessApplicationService;
            BusinessApplicantService = businessApplicantService;
            EntityStatusService = entityStatusService;
            ApplicationProcessorConfiguration = applicationProcessorConfiguration;
            YelpService = yelpService;
            BBBSearchService = bbbSearchService;
            EmailService = emailService;
            DataAttributeService = dataAttributeService;
            EventHub = eventHub;
            ApplicationDocumentService = applicationDocumentService;
            ConsentService = consentService;
            BusinessReportService = businessReportService;
            PersonalReportService = personalReportService;
            VerificationEngineService = verificationEngineService;
            DocuSignService = docuSignService;
            PaynetReportService = paynetReportService;
            OfferEngineService = offerEngineService;
            LexisNexisService = lexisNexisService;
            ProductRuleService = productRuleService;
            AssignmentService = assignmentService;
            IdentityService = identityService;
            DocumentGeneratorService = documentGeneratorService;
            FundingService = fundingService;
            TokenGeneratorService = tokenGeneratorService;
            PartnerService = partnerService;
            CashflowService = cashflowService;
            TenantTime = tenantTime;
            Logger = logger;
            LoanManagementService = loanManagementService;
            ProductService = productService;
        }

        #endregion Constructor

        #region Private Variables

        private const string YELPREPORT = "yelpReport";
        private const string BBBREPORT = "bbbReport";
        private const double WeekConstant = 4.34812141;
        private ICashflowService CashflowService { get; }
        private IFundingService FundingService { get; }
        private ILookupService LookupService { get; }
        private ApplicationProcessorConfiguration ApplicationProcessorConfiguration { get; }
        private IEntityStatusService EntityStatusService { get; }
        private LendFoundry.DataAttributes.IDataAttributesEngine DataAttributeService { get; }
        private IApplicationService BusinessApplicationService { get; }
        private IApplicantService BusinessApplicantService { get; }
        private IYelpService YelpService { get; }
        private IBBBSearchService BBBSearchService { get; }
        private ITenantTime TenantTime { get; }
        private IEmailService EmailService { get; }

        private IEventHubClient EventHub { get; set; }
        private IApplicationDocumentService ApplicationDocumentService { get; set; }
        private IConsentService ConsentService { get; set; }
        private LendFoundry.Syndication.Experian.IBusinessReportService BusinessReportService { get; set; }
        private IPersonalReportService PersonalReportService { get; set; }
        private IPaynetService PaynetReportService { get; set; }
        private IVerificationEngineService VerificationEngineService { get; set; }
        private IDocuSignService DocuSignService { get; set; }
        private IOfferEngineService OfferEngineService { get; }
        private ILexisNexisService LexisNexisService { get; set; }
        private IProductRuleService ProductRuleService { get; set; }
        private IDocumentGeneratorService DocumentGeneratorService { get; set; }
        private string EntityType { get; set; } = "application";
        private IAssignmentService AssignmentService { get; }
        private IIdentityService IdentityService { get; }
        private ITokenGeneratorService TokenGeneratorService { get; set; }
        private IPartnerService PartnerService { get; set; }
        private ILogger Logger { get; }
        private ILoanManagementClientService LoanManagementService { get; }
        private IProductService ProductService { get; }

        #endregion Private Variables

        #region Owner Validations
        private void OwnerValidations(ISubmitBusinessApplicationRequest applicantRequest)
        {
            if (applicantRequest.Owners == null)
                throw new ArgumentNullException($"{nameof(applicantRequest.Owners)} Information is needed");

            if (applicantRequest.Owners != null && applicantRequest.Owners.Count > 0)
            {

                foreach (var owner in applicantRequest.Owners)
                {
                    if (string.IsNullOrEmpty(owner.OwnershipPercentage))
                        throw new ArgumentException("Ownership Percentage is mandatory");
                    if (Convert.ToDecimal(owner.OwnershipPercentage) > 100)
                        throw new ArgumentNullException($"{nameof(owner.OwnershipPercentage)} must be between 1 to 100");
                    if (string.IsNullOrEmpty(owner.SSN))
                        throw new ArgumentException("SSN is mandatory");
                    if (owner.SSN.Length >= 11)
                        throw new ArgumentNullException($"{nameof(owner.SSN)} Max length 11");
                }
                if (!applicantRequest.Owners.Any(item => item.IsPrimary == true))
                {
                    applicantRequest.Owners.OrderByDescending(i => Convert.ToDecimal(i.OwnershipPercentage)).FirstOrDefault().IsPrimary = true;
                }

                var primaryOwner = applicantRequest.Owners.FirstOrDefault(i => i.IsPrimary == true);

                if (primaryOwner.FirstName == null)
                    throw new ArgumentNullException("Owners First Name is needed");
                if (primaryOwner.LastName == null)
                    throw new ArgumentNullException("Owners Last Name is needed");
                if (primaryOwner.SSN == null)
                    throw new ArgumentNullException("Owners SSN is needed");
                if (!Regex.IsMatch(primaryOwner.SSN, "^([0-9]{3}-[0-9]{2}-[0-9]{4}|[0-9]+)$"))
                    throw new ArgumentException("Invalid SSN Number");
                if (primaryOwner.Addresses == null)
                    throw new ArgumentNullException ("Owners Addresses is needed");
                if (string.IsNullOrWhiteSpace (primaryOwner.Addresses[0].City))
                    throw new ArgumentNullException ("Owners City is needed");
                if (string.IsNullOrWhiteSpace (primaryOwner.Addresses[0].State))
                    throw new ArgumentNullException ("Owners State is needed");
                if (string.IsNullOrWhiteSpace (primaryOwner.Addresses[0].ZipCode))
                    throw new ArgumentNullException ("Owners ZipCode is needed");
                if (string.IsNullOrWhiteSpace (primaryOwner.Addresses[0].Country))
                    throw new ArgumentNullException ("Country is needed");

                var CountryExists = LookupService.GetLookupEntry ("country", (primaryOwner.Addresses[0].Country).ToLower ());
                if (CountryExists == null)
                throw new InvalidArgumentException ("Country is not valid");

                if (primaryOwner.Addresses[0].City.Length >= 50)
                    throw new ArgumentNullException("City can note be more then 50");
                if (primaryOwner.Addresses[0].ZipCode.Length >= 10)
                    throw new ArgumentNullException("ZipCode can note be more then 10");
            }
        }
        #endregion

        #region Business Application
        public async Task<IApplicationResponse> SubmitBusinessApplication(ISubmitBusinessApplicationRequest submitBusinessApplicationRequest)
        {
            EligibleForLeadCreate(submitBusinessApplicationRequest);
            //if it false it create lead and return, else submit application continue.

            if (!EligibleForApplicationSubmit(submitBusinessApplicationRequest))
            {
                bool isGuestLogin = false;
                var applicationRequest = GetBusinessApplicationRequest(submitBusinessApplicationRequest);
                Logger.Info("Started Execution GetOrCreateUser-info: Date & Time:" + TenantTime.Now + " Service: IdentityService");

                if (submitBusinessApplicationRequest.Source != null)
                {
                    var sourceValue = LookupService.GetLookupEntry("sourceType", submitBusinessApplicationRequest.Source).FirstOrDefault().Value;
                    ApplicationProcessorConfiguration.GuestLoginSources.Contains(sourceValue);
                    if (ApplicationProcessorConfiguration.GuestLoginSources
                        .FindIndex(x => x
                           .Equals(sourceValue,
                               StringComparison.OrdinalIgnoreCase)) != -1)
                    {
                        isGuestLogin = true;
                    }

                }
                applicationRequest.PrimaryApplicant.UserId = await GetOrCreateUser(submitBusinessApplicationRequest.Email, submitBusinessApplicationRequest.Password, $"{applicationRequest?.ContactFirstName} {applicationRequest?.ContactLastName}", isGuestLogin);
                Logger.Info("Completed ExecutionGetOrCreateUser-info: Date & Time:" + TenantTime.Now + " Service: IdentityService");
                Logger.Info("Started Execution add BusinessApplication -info: Date & Time:" + TenantTime.Now + " Service: BusinessApplicationService");
                var objResponse = await BusinessApplicationService.Add(applicationRequest);
                if (isGuestLogin == true)
                {
                    var issuedToken = await GenerateToken(EntityType, objResponse.ApplicationNumber, ApplicationProcessorConfiguration.TokenExpirationDays);
                    objResponse.ApplicationUrl = ApplicationProcessorConfiguration.BorrowerPortalUrl + issuedToken.Value.ToString();
                }
                Logger.Info("Completed Execution  add BusinessApplication-info: Date & Time:" + TenantTime.Now + " Service: BusinessApplicationService");
                return objResponse ;
            }

            //EnsureBusinessInputIsValid(submitBusinessApplicationRequest);
            IApplicationResponse application = null;
            IPartnerUser partnerUser = null;
            IPartner partnerDetail = null;
            OwnerValidations(submitBusinessApplicationRequest);
            if (submitBusinessApplicationRequest.IsReapply)
            {
                var isEligible = await CheckReApplyEligibility(submitBusinessApplicationRequest.ApplicationNumber);
                if (!isEligible)
                    throw new InvalidOperationException($"App#.{submitBusinessApplicationRequest.ApplicationNumber } is not eligible for re-apply");
            }
            try
            {
                Logger.Info("Started Execution for Get Partner Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                partnerUser = await PartnerService.GetByUserId(submitBusinessApplicationRequest.PartnerUserId);
                Logger.Info("Completed Execution for  Get Partner Info: Date & Time:" + TenantTime.Now + " Service: Partner");
            }
            catch (System.Exception ex) { }
            if (string.IsNullOrWhiteSpace(submitBusinessApplicationRequest.ApplicationNumber) || submitBusinessApplicationRequest.IsReapply)
            {
                var applicationRequest = GetBusinessApplicationRequest(submitBusinessApplicationRequest);
                applicationRequest.PartnerId = partnerDetail?.PartnerId;
                applicationRequest.PrimaryApplicant.UserId = await GetOrCreateUser(submitBusinessApplicationRequest.Email, submitBusinessApplicationRequest.Password, $"{applicationRequest?.ContactFirstName} {applicationRequest?.ContactLastName}", false, submitBusinessApplicationRequest.IsReapply);
                application = await BusinessApplicationService.Add(applicationRequest);
                Logger.Info("Completed Execution for add Business Application- Info: Date & Time:" + TenantTime.Now + " Service: BusinessApplicationService");
                if (submitBusinessApplicationRequest.IsReapply)
                    DataAttributeService.RemoveAttribute(EntityType, submitBusinessApplicationRequest.ApplicationNumber, "Reapply");
            }
            else
            {
                application = await EditApplication(submitBusinessApplicationRequest, partnerUser?.PartnerId);
            }
            if (application == null)
                throw new ArgumentNullException(nameof(application));
            Logger.Info("Started Execution for Initial Assign Application- Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: IdentityService");
            InitialAssignApplication(application.ApplicationNumber, submitBusinessApplicationRequest.LeadOwnerId);
            Logger.Info("Completed Execution for Initial Assign Application Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: IdentityService");
            //SaveTermofUseConsent(application.ApplicationNumber, application.ApplicantId, submitBusinessApplicationRequest.ClientIpAddress);
            Logger.Info("Started Execution for Save Privacy policy Consent- Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: ConsentService");
            SavePrivacypolicyConsent(application.ApplicationNumber, application.ApplicantId, submitBusinessApplicationRequest.ClientIpAddress);
            ApplicationPDFRequestEvent(application.ApplicationNumber, EntityType);
            Logger.Info("Completed Execution for Save Privacy policy Consent- Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: ConsentService");
            Logger.Info("Started Execution forGetPrimaryOwner- Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: BusinessApplicantService");
            var objOwner = await BusinessApplicantService.GetPrimaryOwner(application.ApplicantId);
            Logger.Info("Completed Execution for GetPrimaryOwner- Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: BusinessApplicantService");
            var objActiveStatus = await GetActiveStatusWorkFlow(EntityType, application.ApplicationNumber);
            if (objOwner != null)
            {
                bool isCanadaState = LookupService.Exist("canadastates", (objOwner.Addresses[0].State).ToLower());
                if (isCanadaState)
                {
                    List<string> reasons = new List<string>();
                    reasons.Add("CADecline");

                    RequestModel objReason = new RequestModel();
                    objReason.reasons = reasons;

                    Logger.Info("Started Execution ChangeStatus - CADecline- Declined Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: EntityStatusService");
                    EntityStatusService.ChangeStatus(EntityType, application.ApplicationNumber, objActiveStatus.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["Declined"], objReason);
                    Logger.Info("Completed Execution ChangeStatus - CADecline- Declined- Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: EntityStatusService");

                    return application;
                }
                Logger.Info("Started Execution Call Experian Personal Syndication- Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: ExperianPersonalSyndication");
                var objPersonalReportResponse = await CallExperianPersonalSyndication(application.ApplicationNumber, objOwner.FirstName, objOwner.LastName, objOwner.SSN,
                    objOwner.Addresses[0].AddressLine1, objOwner.Addresses[0].City,
                    objOwner.Addresses[0].State, objOwner.Addresses[0].ZipCode, objOwner.OwnerId, PersonalReportService);
                Logger.Info("Started Execution for Call Experian Personal Syndication- Info " + application.ApplicationNumber + " :Date & Time:" + TenantTime.Now + " Service: ExperianPersonalSyndication");

                if (objPersonalReportResponse?.ProductCreditProfile == null)
                {
                    Logger.Info("Started Execution Publish Event NonPrimaryOwnerCreditReportRequested -Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: EventHub");
                    await EventHub.Publish("NonPrimaryOwnerCreditReportRequested", new
                    {
                        EntityId = application.ApplicationNumber,
                        EntityType = EntityType,
                        Response = "NonPrimaryOwnerExperianPerson",
                        Request = "NonPrimaryOwnerExperianPerson",
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });
                    Logger.Info("Completed Execution Publish Event NonPrimaryOwnerCreditReportRequested -Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: EventHub");
                    List<string> reasons = new List<string>();
                    reasons.Add("NoCreditReport");

                    RequestModel objReason = new RequestModel();
                    objReason.reasons = reasons;

                    Logger.Info("Started Execution ChangeStatus - NoCreditReport- Declined Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: EntityStatusService");
                    EntityStatusService.ChangeStatus(EntityType, application.ApplicationNumber, objActiveStatus.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["Declined"], objReason);
                    Logger.Info("Completed Execution ChangeStatus - NoCreditReport- Declined- Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: EntityStatusService");

                    return application;
                }

                JArray personalReport = null;
                FaultRetry.RunWithAlwaysRetry(() =>
                {
                    personalReport = (JArray)DataAttributeService.GetAttribute(EntityType, application.ApplicationNumber, "experianPersonalReport").Result;
                    if (personalReport == null || personalReport.Count <= 0)
                        throw new InvalidOperationException("experianPersonalReport not found");
                });
                if (personalReport == null || personalReport.Count <= 0)
                    throw new InvalidOperationException("experianPersonalReport not found");

                IProductRuleResult m2scoreResult = new ProductRuleResult();
                if (!ApplicationProcessorConfiguration.SkipM2ScoreCalculation)
                {
                    Logger.Info("Started Execution CreditModelM2 RunPricingStrategy - Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: ProductRuleService");
                    m2scoreResult = await ProductRuleService.RunPricingStrategy(EntityType, application.ApplicationNumber, objActiveStatus.ProductId, "CreditModelM2", "1.0", objOwner.OwnerId);
                    Logger.Info("Completed Execution RunPricingStrategy - Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: ProductRuleService");
                    if (m2scoreResult == null)
                        throw new InvalidOperationException("Unable to execute strategy: CreditModelM2");
                    await EventHub.Publish("CreditModelM2ScoreCalculated", new
                    {
                        EntityId = application.ApplicationNumber,
                        EntityType = EntityType,
                        Response = m2scoreResult,
                        Request = "CreditModelM2ScoreCalculated",
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });
                    if (m2scoreResult?.Result == Result.Passed && ApplicationProcessorConfiguration.DeclineOnLowM2Score)
                    {
                        if (m2scoreResult?.IntermediateData != null)
                        {
                            List<string> reasons = new List<string>();
                            JObject intermediateData = JObject.Parse(m2scoreResult.IntermediateData.ToString());
                            if (Convert.ToDecimal(intermediateData["m2_score"]) < ApplicationProcessorConfiguration.MinM2Score)
                            {
                                reasons.Add("LowM2Score");
                                RequestModel objRequest = new RequestModel();
                                objRequest.reasons = reasons;
                                EntityStatusService.ChangeStatus(EntityType, application.ApplicationNumber, objActiveStatus.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["Declined"], objRequest);
                                return application;
                            }
                        }
                    }
                }

                IProductRuleResult pscoreResult = new ProductRuleResult();
                Logger.Info("Started Execution RunPricingStrategy - Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: ProductRuleService");
                pscoreResult = await ProductRuleService.RunPricingStrategy(EntityType, application.ApplicationNumber, objActiveStatus.ProductId, "Eligibility", "1.0", objOwner.OwnerId);
                Logger.Info("Completed Execution RunPricingStrategy - Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: ProductRuleService");

                if (pscoreResult == null)
                    throw new InvalidOperationException("Unable to Strategy: Eligibility");
                Logger.Info("Started Execution Publish Event EligibilityChecked -Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: EventHub");

                await EventHub.Publish("EligibilityChecked", new
                {
                    EntityId = application.ApplicationNumber,
                    EntityType = EntityType,
                    Response = pscoreResult,
                    Request = "EligibilityChecked",
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
                Logger.Info("Completed Execution Publish Event EligibilityChecked -Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: EventHub");

                if (pscoreResult?.Result == Result.Passed)
                {
                    Logger.Info("Started Execution ChangeStatus - ApplicationSubmitted-Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: ProductRuleService");
                    await EntityStatusService.ChangeStatus(EntityType, application.ApplicationNumber, objActiveStatus.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["ApplicationSubmitted"], null);
                    Logger.Info("Completed Execution ChangeStatus - ApplicationSubmitted-Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: ProductRuleService");
                    Logger.Info("Started Execution Yelp -Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: Yelp");
                    Yelp(application.ApplicationNumber, submitBusinessApplicationRequest.Phone, submitBusinessApplicationRequest.LegalBusinessName, submitBusinessApplicationRequest.AddressLine1, submitBusinessApplicationRequest.City, submitBusinessApplicationRequest.State, submitBusinessApplicationRequest.Country, YelpService);
                    Logger.Info("Completed Execution Yelp -Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: Yelp");
                    Logger.Info("Started Execution Bbbsearch -Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: Bbbsearch");
                    bbbsearch(application.ApplicationNumber, submitBusinessApplicationRequest.LegalBusinessName, submitBusinessApplicationRequest.Phone, submitBusinessApplicationRequest.ZipCode, BBBSearchService);
                    Logger.Info("Completed Execution Bbbsearch -Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: Bbbsearch");

                    Logger.Info("Started Execution Publish Event ApplicationPDFRequested -Info " + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: EventHub");
                    //ApplicationPDFRequestEvent (application.ApplicationNumber, EntityType);
                    Logger.Info("Completed Execution Publish Event ApplicationPDFRequested -Info" + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: EventHub");
                    EntityStatusService.ChangeStatus(EntityType, application.ApplicationNumber, objActiveStatus.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["Verification"], null);
                    Logger.Info("Started Execution Publish Event EventWelcomeEmail -Info" + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: EventHub");

                    await EventHub.Publish(new EventWelcomeEmail
                    {
                        EntityId = application.ApplicationNumber,
                        EntityType = EntityType,
                        Response = "EventWelcomeEmail",
                        Request = "EventWelcomeEmail",
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });
                    Logger.Info("Completed Execution Publish Event EventWelcomeEmail -Info" + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: EventHub");
                }
                else
                {
                    List<string> reasons = new List<string>();
                    if (pscoreResult?.IntermediateData != null)
                    {
                        JObject intermediateData = JObject.Parse(pscoreResult.IntermediateData.ToString());
                        if (Convert.ToBoolean(intermediateData["scoreResult"]) == false)
                        {
                            reasons.Add("LowCreditScore");
                        }
                        if (Convert.ToBoolean(intermediateData["industryResult"]) == false)
                        {
                            reasons.Add("IndustryNotSupported");
                        }
                        if (Convert.ToBoolean(intermediateData["businessRevenueResult"]) == false)
                        {
                            reasons.Add("BusinessRevenueLess");
                        }
                        if (Convert.ToBoolean(intermediateData["timeInBusinessResult"]) == false)
                        {
                            reasons.Add("TimeInBusinessLess");
                        }
                        // ApplicationPDFRequestEvent (application.ApplicationNumber, EntityType);
                    }
                    else
                    {
                        reasons.Add("CADecline");
                        //  ApplicationPDFRequestEvent (application.ApplicationNumber, EntityType);
                    }

                    RequestModel objRequest = new RequestModel();
                    objRequest.reasons = reasons;

                    Logger.Info("Started Execution ChangeStatus Declined-CADecline -Info" + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: EventHub");
                    EntityStatusService.ChangeStatus(EntityType, application.ApplicationNumber, objActiveStatus.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["Declined"], objRequest);
                    Logger.Info("Completed Execution ChangeStatus Declined-CADecline -Info" + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: EventHub");
                }
            }
            Logger.Info("Started Execution Event Publish NonPrimaryOwnerCreditReportRequested -Info" + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: EventHub");
            await EventHub.Publish("NonPrimaryOwnerCreditReportRequested", new
            {
                EntityId = application.ApplicationNumber,
                EntityType = EntityType,
                Response = "NonPrimaryOwnerExperianPerson",
                Request = "NonPrimaryOwnerExperianPerson",
                ReferenceNumber = Guid.NewGuid().ToString("N")
            });
            Logger.Info("Completed Execution Event Publish NonPrimaryOwnerCreditReportRequested -Info" + application.ApplicationNumber + " : Date & Time:" + TenantTime.Now + " Service: EventHub");
            if(ApplicationProcessorConfiguration.ExtractedFieldRequired)
                application.ExtractedField = await GetExtractedData(application.ApplicationNumber,"4g5jhg");
            return application;
        }
        #region Non Primary Experian Call

        public async Task NonPrimaryExperianCallHandler(string entityType, string entityId, object data)
        {

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            var allOwners = await GetOwners(entityId, BusinessApplicationService, BusinessApplicantService);
            if (allOwners != null && allOwners.Count > 0)
            {

                foreach (var owner in allOwners)
                {

                    if (owner.IsPrimary == null || (owner.IsPrimary.HasValue && owner.IsPrimary.Value == false))
                    {
                        await CallExperianPersonalSyndication(entityId, owner.FirstName, owner.LastName, owner.SSN,
                            owner.Addresses[0].AddressLine1, owner.Addresses[0].City,
                            owner.Addresses[0].State, owner.Addresses[0].ZipCode, owner.OwnerId, PersonalReportService);
                    }
                }
            }
        }
        #endregion

        private async void Yelp(string entityId,string phone,string LegalBusinessName,string AddressLine1,string city,string state,string country,IYelpService yelpService)
        {
            BusinessMatchSearchRequest objBusinessMatchSearch = new BusinessMatchSearchRequest();
            objBusinessMatchSearch.Name = LegalBusinessName;
            objBusinessMatchSearch.Address1 = AddressLine1;
            objBusinessMatchSearch.City = city;
            objBusinessMatchSearch.State = state;
            objBusinessMatchSearch.Country = country;
            yelpService.GetBusinessMatchDetails(EntityType, entityId, objBusinessMatchSearch);
        }
        private async void bbbsearch(string entityId, string legalBusinessName, string phoneNumber, string postalCode, IBBBSearchService bbbSearchService)
        {
            LendFoundry.Syndication.BBBSearch.Request.BBBSearchRequest objBBBSearchRequest = new LendFoundry.Syndication.BBBSearch.Request.BBBSearchRequest();
            objBBBSearchRequest.OrganizationName = legalBusinessName;
            objBBBSearchRequest.IsBBBAccredited = false;
            objBBBSearchRequest.IsReportable = false;

            objBBBSearchRequest.PostalCode = Regex.Replace(postalCode, "[^0-9.]", "");
            objBBBSearchRequest.Phone = Regex.Replace(phoneNumber, "[^0-9.]", "");
            bbbSearchService.SearchOrganization(EntityType, entityId, objBBBSearchRequest);
        }
        public async Task<IApplicationDetails> AddLead(ISubmitBusinessApplicationRequest submitApplicationRequest)
        {
            IApplicationDetails application = null;
            IApplicationResponse objResponse = null;
            ValidateLeadInput(submitApplicationRequest);
            IPartnerUser partnerUser = null;

            if (string.IsNullOrWhiteSpace(submitApplicationRequest?.ApplicationNumber))
            {
                if (!string.IsNullOrEmpty(submitApplicationRequest.PartnerUserId))
                {
                    Logger.Info("Started Execution Get Partner Info in AddLead: Date & Time:" + TenantTime.Now + " Service: PartnerService");
                    partnerUser = await PartnerService.GetByUserId(submitApplicationRequest.PartnerUserId);
                    Logger.Info("Completed Execution Get Partner Info in AddLead -Info: Date & Time:" + TenantTime.Now + " Service: PartnerService");

                }
                var applicationRequest = GetBusinessApplicationRequest(submitApplicationRequest);
                applicationRequest.PartnerId = partnerUser?.PartnerId;
                Logger.Info("Started Execution GetOrCreateUser-info: Date & Time:" + TenantTime.Now + " Service: IdentityService");
                applicationRequest.PrimaryApplicant.UserId = await GetOrCreateUser(submitApplicationRequest.Email, submitApplicationRequest.Password, $"{applicationRequest?.ContactFirstName} {applicationRequest?.ContactLastName}");
                Logger.Info("Completed ExecutionGetOrCreateUser-info: Date & Time:" + TenantTime.Now + " Service: IdentityService");
                Logger.Info("Started Execution add BusinessApplication -info: Date & Time:" + TenantTime.Now + " Service: BusinessApplicationService");
                objResponse = await BusinessApplicationService.Add(applicationRequest);
                Logger.Info("Completed Execution  add BusinessApplication-info: Date & Time:" + TenantTime.Now + " Service: BusinessApplicationService");
            }
            else
            {
                Logger.Info("Started Execution  EditApplication -info: Date & Time:" + TenantTime.Now + " Service: BusinessApplicationService");

                objResponse = await EditApplication(submitApplicationRequest);
                Logger.Info("Completed Execution  EditApplication -info: Date & Time:" + TenantTime.Now + " Service: BusinessApplicationService");

            }
            application = await BusinessApplicationService.GetApplicationDetails(objResponse.ApplicationNumber);
            return application;
        }

        public async Task<IApplicationResponse> EditApplication(ISubmitBusinessApplicationRequest request, string partnerId = null)
        {
            if (string.IsNullOrWhiteSpace(request.ApplicationNumber))
                throw new ArgumentNullException(nameof(request.ApplicationNumber));
            var application = await BusinessApplicationService.GetByApplicationId(request.ApplicationNumber);
            if (application == null)
                throw new ArgumentNullException($"{ nameof(application) } cannot be empty");
            var applicant = await BusinessApplicantService.Get(application.ApplicantId);
            if (applicant == null)
                throw new ArgumentNullException($"{ nameof(applicant) } cannot be empty");
            if (string.IsNullOrWhiteSpace(partnerId))
            {
                #region Add Partner Details                   
                if (!string.IsNullOrWhiteSpace(request.PartnerUserId))
                {
                    Logger.Info("Started Execution for Get Partner Info-Edit Application: Date & Time:" + TenantTime.Now + " Service: Partner");
                    var partnerUser = await PartnerService.GetByUserId(request.PartnerUserId);
                    partnerId = partnerUser?.PartnerId;
                    Logger.Info("Completed Execution for Get Partner Info-Edit Application: Date & Time:" + TenantTime.Now + " Service: Partner");
                }
                #endregion Add Partner Details
            }

            var applicationRequest = GetBusinessApplicationRequest(request);
            Logger.Info("Started Execution for Update Application Info: Date & Time:" + TenantTime.Now + " Service: BusinessApplicationService");
            var objResponse = await BusinessApplicationService.UpdateApplication(request.ApplicationNumber, applicationRequest);
            Logger.Info("Completed Execution for Update Application Info: Date & Time:" + TenantTime.Now + " Service: PartnerBusinessApplicationService");
            return objResponse;
        }

        private static string GetPgradeValue(dynamic data)
        {
            Func<dynamic> resultProperty = () => data.p_grade;
            return HasProperty(resultProperty) ? GetValue(resultProperty) : null;
        }

        #endregion Business Application

        #region Plaid Methods

        private string FormatMoney(double amount)
        {
            string result = string.Empty;
            var usCulture = CultureInfo.CreateSpecificCulture("en-US");
            var clonedNumbers = (NumberFormatInfo)usCulture.NumberFormat.Clone();
            clonedNumbers.CurrencyNegativePattern = 2;
            result = amount.ToString("C", clonedNumbers);
            return result;
        }

        private string FormatPhoneNumber(string phoneNumber)
        {
            string formattedPhoneNumber = string.Format("{0:(###) ###-####}", Convert.ToInt64(phoneNumber));
            return formattedPhoneNumber;
        }

        private string FormatSSNNumber(string SSNNumber)
        {
            string formattedSSNNumber = string.Empty;
            formattedSSNNumber = string.Format("{0:000-00-0000}", Convert.ToInt32(SSNNumber));
            return formattedSSNNumber;
        }

        private string FormatTaxId(string taxId)
        {
            string formattedTaxId = string.Empty;
            formattedTaxId = string.Format("{0:00-0000000}", Convert.ToInt64(taxId));
            return formattedTaxId;
        }
        #endregion Plaid Methods

        #region Submit Document

        public async Task<bool> SubmitDocument(string entityId, string category, byte[] fileBytes, string fileName, List<string> tags)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            bool result = false;
            var objApplicationDocument = await ApplicationDocumentService.Add(entityId, category, fileBytes, fileName, tags);
            if (objApplicationDocument != null && !string.IsNullOrEmpty(objApplicationDocument.DocumentId))
            {
                result = true;
            }

            return result;
        }

        #endregion Submit Document

        #region Sign Consent

        public async Task<bool> SignConsent(string entityId, string category, object body)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            bool result = false;
            var objConsent = await ConsentService.Sign(EntityType, entityId, category, body);

            if (objConsent != null && !string.IsNullOrEmpty(objConsent.DocumentId))
                result = true;
            return result;
        }

        #endregion Sign Consent

        #region Experian

        #region Business Report

        public async Task<ISearchBusinessResponse> BusinessSearch(string entityId, Request.IBusinessSearchRequest request)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            if (request == null)
                throw new ArgumentNullException(nameof(request));

            if (string.IsNullOrWhiteSpace(request.LegalBusinessName))
                throw new ArgumentException("BusinessName is required", nameof(request.LegalBusinessName));

            if (string.IsNullOrWhiteSpace(request.City))
                throw new ArgumentException("City is required", nameof(request.City));

            if (string.IsNullOrWhiteSpace(request.AddressLine1))
                throw new ArgumentException("Street is required", nameof(request.AddressLine1));

            if (string.IsNullOrWhiteSpace(request.State))
                throw new ArgumentException("State is required", nameof(request.State));

            if (string.IsNullOrWhiteSpace(request.ZipCode))
                throw new ArgumentException("Zip is required", nameof(request.ZipCode));

            if (!Regex.IsMatch(request.ZipCode, "^([0-9]+)$"))
                throw new ArgumentException("Invalid Zip Code");

            LendFoundry.Syndication.Experian.Request.IBusinessSearchRequest objRequest = new LendFoundry.Syndication.Experian.Request.GetBusinessSearchRequest();

            objRequest.BusinessName = request.LegalBusinessName;
            objRequest.AlternateName = request.DBA;
            objRequest.Street = request.AddressLine1;
            objRequest.City = request.City;
            objRequest.State = request.State;
            objRequest.Zip = request.ZipCode;

            var objReport = await BusinessReportService.GetBusinessSearch(EntityType, entityId, objRequest);
            var getReport = objReport?.Products?.PremierProfile?.Any(i => i.ListOfSimilars?.Count == 1);
            var noRecord = objReport?.Products?.PremierProfile?.Any(i => i.ListOfSimilars?.Count == 0);
            var isDirectGetReport = Convert.ToBoolean(getReport);
            var isNoRecord = Convert.ToBoolean(noRecord);
            if (isDirectGetReport)
            {
                var similarBusinessObj = objReport.Products.PremierProfile.FirstOrDefault().ListOfSimilars.FirstOrDefault();
                if (similarBusinessObj != null)
                {
                    Request.IBusinessReportRequest reportRequest = new BusinessReportRequest()
                    {
                        AddressLine1 = similarBusinessObj.StreetAddress,
                        BisListNumber = $"{similarBusinessObj.ExperianFileNumber}{similarBusinessObj.RecordSequenceNumber}",
                        City = similarBusinessObj.City,
                        DBA = similarBusinessObj.BusinessName,
                        LegalBusinessName = similarBusinessObj.BusinessName,
                        State = similarBusinessObj.State,
                        TransactionNumber = objReport.Products.PremierProfile.FirstOrDefault().InputSummary.FirstOrDefault().InquiryTransactionNumber,
                        ZipCode = similarBusinessObj.Zip
                    };
                    var reportResponse = await GetBusinessReport(entityId, reportRequest);
                }
            }
            if (isNoRecord)
            {
                var experianBusinessReport = GetExperianBusinessDefaultObject();
                await DataAttributeService.SetAttribute(EntityType, entityId, "experianBusinessReport", experianBusinessReport, "experianBusinessReport");
            }
            return objReport;
        }

        private object GetExperianBusinessDefaultObject()
        {
            var experianBusinessReport = new
            {
                action = string.Empty,
                CLC006 = 0,
                RTC037 = 0,
                IntelliScore = string.Empty,
                FinancialStability = string.Empty,
                TaxLienFilingCount = string.Empty,
                JudgmentFilingCount = string.Empty,
                UccFilings = string.Empty,
                LegalBalance = string.Empty,
                Bankruptcy = 0,
                m_action_miss = 1,
                m_action_high = 0,
                m_CLC006 = 0,
                m_RTC037 = 0,
                CautionaryUCCFilings = string.Empty,
                Sumoflegalfilings = string.Empty,
                SecuredParty = string.Empty,
                Collateral = string.Empty,
                DateofIncorporation = string.Empty,
                CurrentStatus = string.Empty,
                UCCFiledDate = string.Empty,
                Timeinbusiness = 0,
                referenceNumber = Guid.NewGuid().ToString("N"),
                Failed = true
            };
            return experianBusinessReport;
        }

        public async Task<IBusinessReportResponse> GetBusinessReport(string entityId, Request.IBusinessReportRequest request)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            if (request == null)
                throw new ArgumentNullException(nameof(request));

            if (string.IsNullOrWhiteSpace(request.TransactionNumber))
                throw new ArgumentException("TransactionNumber is required",
                    nameof(request.TransactionNumber));

            if (string.IsNullOrWhiteSpace(request.BisListNumber))
                throw new ArgumentException("BISListNumber is required", nameof(request.BisListNumber));

            LendFoundry.Syndication.Experian.Request.IBusinessReportRequest objReportRequest = new GetBusinessReportRequest();

            //var application = await BusinessApplicationService.GetByApplicationNumber(entityId);

            //if (application == null)
            //    throw new InvalidOperationException("Application not found");

            objReportRequest.TransactionNumber = request.TransactionNumber;
            objReportRequest.BisListNumber = request.BisListNumber; // "70044177300";
            objReportRequest.BusinessName = request.LegalBusinessName;
            objReportRequest.AlternateName = request.DBA;

            objReportRequest.Street = request.AddressLine1;
            objReportRequest.City = request.City;
            objReportRequest.State = request.State;
            objReportRequest.Zip = request.ZipCode;

            var objReportResponse = await BusinessReportService.GetBusinessReport(EntityType, entityId, objReportRequest);

            await EventHub.Publish("ExperianBusinessReportFetches", new
            {
                EntityId = entityId,
                EntityType = EntityType,
                Response = objReportResponse,
                Request = "ExperianBusinessReportFetches",
                ReferenceNumber = Guid.NewGuid().ToString("N")
            });

            return objReportResponse;
        }

        #endregion Business Report

        #region Personal Report

        public async Task<LendFoundry.Syndication.Experian.Response.IPersonalReportResponse> GetPersonalReport(string entityId, Request.IPersonalReportRequest request)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            if (request == null)
                throw new ArgumentNullException(nameof(request));

            if (string.IsNullOrWhiteSpace(request.Firstname))
                throw new ArgumentException("Firstname is required", nameof(request.Firstname));

            if (!Regex.IsMatch(request.Firstname, "^([a-zA-Z\\s]+)$"))
                throw new ArgumentException("Invalid FirstName", nameof(request.Firstname));

            if (string.IsNullOrWhiteSpace(request.Lastname))
                throw new ArgumentException("Lastname is required", nameof(request.Lastname));

            if (!Regex.IsMatch(request.Lastname, "^([a-zA-Z\\s]+)$"))
                throw new ArgumentException("Invalid LastName", nameof(request.Lastname));

            if (string.IsNullOrWhiteSpace(request.Street))
                throw new ArgumentException("Street is required", nameof(request.Street));

            if (string.IsNullOrWhiteSpace(request.Zip))
                throw new ArgumentException("Zip is required", nameof(request.Zip));

            if (!Regex.IsMatch(request.Zip, "^([0-9]+)$"))
                throw new ArgumentException("Invalid Zip Code");

            if (string.IsNullOrWhiteSpace(request.Ssn))
                throw new ArgumentException("Ssn is required", nameof(request.Ssn));

            if (!Regex.IsMatch(request.Ssn, "^([0-9]{9}|[0-9]{3}-[0-9]{2}-[0-9]{4}|[0-9]+)$"))
                throw new ArgumentException("Invalid Ssn Number format");

            if (request.Ssn.Length > 11)
                throw new ArgumentException("Ssn should not more than 9 digits", nameof(request.Ssn));

            LendFoundry.Syndication.Experian.Request.IPersonalReportRequest objRequest = new LendFoundry.Syndication.Experian.Request.GetPersonalReportRequest();
            objRequest.Firstname = request.Firstname;
            objRequest.Lastname = request.Lastname;
            objRequest.City = request.City;
            objRequest.Street = request.Street;
            objRequest.State = request.State;
            objRequest.Zip = request.Zip;
            objRequest.Ssn = request.Ssn;
            try
            {
                var objReport = await PersonalReportService.GetPersonalReportResponse(EntityType, entityId, objRequest);
                return objReport;
            }
            catch (System.Exception ex)
            {
                string e = ex.Message;
            }

            return null;
        }

        #endregion Personal Report

        #endregion Experian

        #region Choose Offer

        public async Task<string> ChooseOffer(string entityId, string offerId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            string result = string.Empty;

            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);

            if (application == null)
                throw new InvalidOperationException("Application not found");
            return result;
        }

        #endregion Choose Offer

        #region Perform Business Verification1

        public async Task<string> PerformBusinessVerification1(string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            string result = string.Empty;

            var referencenumber = Guid.NewGuid().ToString("N");

            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);

            if (application == null)
                throw new InvalidOperationException("Application not found");

            await EventHub.Publish(new BusinessVerification1
            {
                EntityId = entityId,
                EntityType = EntityType,
                Response = null,
                Request = "PerformBusinessVerification1",
                ReferenceNumber = referencenumber
            });
            result = "Business Verification1 Completed";
            return result;
        }

        #endregion Perform Business Verification1

        #region Perform Business Verification2

        public async Task<string> PerformBusinessVerification2(string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            string result = string.Empty;
            var referencenumber = Guid.NewGuid().ToString("N");

            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);

            if (application == null)
                throw new InvalidOperationException("Application not found");

            await EventHub.Publish(new BusinessVerification2
            {
                EntityId = entityId,
                EntityType = EntityType,
                Response = null,
                Request = "BusinessVerification2",
                ReferenceNumber = referencenumber
            });
            result = "Business Verification2 Completed";
            return result;
        }

        #endregion Perform Business Verification2

        #region Resend Bank Linking

        public async Task<string> ResendBankLinking(string entityId, string emailId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            string result = string.Empty;
            string inputEmailId = string.Empty;
            if (emailId != null && emailId != "")
            {
                inputEmailId = emailId;
            }
            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);

            if (application == null)
                throw new InvalidOperationException("Application not found");
            var issuedToken = await GenerateToken(EntityType, application.ApplicationNumber, 0);
            await EventHub.Publish(new BankLinkRequest
            {
                EntityId = application.ApplicationNumber,
                EntityType = EntityType,
                Response = inputEmailId,
                Request = issuedToken.Value,
                ReferenceNumber = Guid.NewGuid().ToString("N")
            });
            result = "Email sent";
            return result;
        }

        #endregion Resend Bank Linking

        #region Get Primary Owner Id
        private async Task<string> GetPrimaryOwnerId(string entityId)
        {
            string OwnerId = string.Empty;
            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);
            if (application != null)
            {
                var objOwner = await BusinessApplicantService.GetPrimaryOwner(application.ApplicantId);
                if (objOwner != null)
                {
                    OwnerId = objOwner.OwnerId;
                }
            }
            return OwnerId;
        }
        #endregion

        #region Compute Offer

        public async Task<IProductRuleResult> ComputeOffer(string entityId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                IProductRuleResult objInitialOffers = null;
                var ownerId = await GetPrimaryOwnerId(entityId);
                if (!string.IsNullOrEmpty(ownerId))
                {
                    var objActiveStatus = await GetActiveStatusWorkFlow(EntityType, entityId);
                    var bp2Score = await ProductRuleService.RunPricingStrategy(EntityType, entityId, objActiveStatus.ProductId, "CalCulateBP2Score", "1.0", ownerId);
                    if (bp2Score.Result == Result.Passed)
                    {
                        objInitialOffers = await ProductRuleService.RunPricingStrategy(EntityType, entityId, objActiveStatus.ProductId, "OfferCalculation", "1.0", ownerId);
                        if (objInitialOffers.Result == Result.Passed)
                        {
                            var intermidiateResult =
                                JObject.FromObject(objInitialOffers.IntermediateData)
                                .ToObject<Dictionary<string, object>>();

                            await SaveOffers(entityId, intermidiateResult);
                            if (objInitialOffers != null && objInitialOffers.Result == Result.Passed && intermidiateResult.Count > 0)
                            {
                                EntityStatusService.ChangeStatus(EntityType, entityId, objActiveStatus.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["OfferGeneration"], null);
                            }
                        }
                    }
                }
                return objInitialOffers;
            }
            catch (System.Exception ex)
            {
                // Logger.Error(ex.Message);
                return null;
            }
        }

        public async Task<Dictionary<string, object>> ComputeProvisionalOffer(string entityId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                IProductRuleResult objInitialOffers = null;
                var ownerId = await GetPrimaryOwnerId(entityId);
                if (!string.IsNullOrEmpty(ownerId))
                {
                    var objActiveStatus = await GetActiveStatusWorkFlow(EntityType, entityId);
                    var bp2Score = await ProductRuleService.RunPricingStrategy(EntityType, entityId, objActiveStatus.ProductId, "CalCulateBP2Score", "1.0", ownerId);
                    if (bp2Score.Result == Result.Passed)
                    {
                        objInitialOffers = await ProductRuleService.RunPricingStrategy(EntityType, entityId, objActiveStatus.ProductId, "OfferCalculation", "1.0", ownerId);
                        if (objInitialOffers.Result == Result.Passed)
                        {
                            return JObject.FromObject(objInitialOffers.IntermediateData).ToObject<Dictionary<string, object>>();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                // Logger.Error(ex.Message);
                return null;
            }
            return null;
        }

        private async Task SaveOffers(string entityId, Dictionary<string, object> intermidiateResult)
        {
            ApplicationOffer applicationOffer = new ApplicationOffer();
            List<IBusinessLoanOffer> loanOffers = new List<IBusinessLoanOffer>();
            var ComputedOffers = JArray.FromObject(intermidiateResult["offers"]).ToObject<List<ComputedOffer>>();

            if (ComputedOffers != null && ComputedOffers.Count > 0)
            {
                foreach (var item in ComputedOffers)
                {
                    var objOffer = new BusinessLoanOffer
                    {
                        OfferId = Guid.NewGuid().ToString("N"),
                        MinAmount = item.loan_offer_min,
                        MaxAmount = item.loan_offer_max,
                        MinFactor = item.buy_rate,
                        MaxFactor = item.sell_rate,
                        MinDuration = item.term,
                        MaxDuration = item.term,
                        LoanAmountCap = item.loan_amount_cap,
                        OfferFees = new List<IOfferFee> {
                new OfferFee {
                FeeAmount = 10.2,
                FeeType = OfferFeeType.OneTime,
                FeePercentage = 4,
                IsIncludedInLoanAmount = true
                }
                }
                    };

                    loanOffers.Add(objOffer);
                }

                applicationOffer.MonthlyRevenue = ComputedOffers[0].monthly_revenue;
                applicationOffer.Offers = loanOffers;
                applicationOffer.EntityId = entityId;
                applicationOffer.EntityType = EntityType;
            }

            await OfferEngineService.SaveOffer(EntityType, entityId, applicationOffer);
        }

        public async Task<IProductRuleResult> GetComputedOffer(string entityId, object offerData)
        {
            try
            {
                var objActiveStatus = await GetActiveStatusWorkFlow(EntityType, entityId);
                var offerResult = await ProductRuleService.RunRule(EntityType, entityId, objActiveStatus.ProductId, RuleName.GetCalculatedOffer.ToString(), offerData);
                return offerResult;
            }
            catch (System.Exception ex)
            {
                //Logger.Error(ex.Message);
                return null;
            }
        }
        #endregion Compute Offer

        #region Execute Request
        private T ExecuteRequest<T>(string response) where T : class
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(response);
            }
            catch (System.Exception exception)
            {
                throw new System.Exception("Unable to deserialize:" + response, exception);
            }
        }
        #endregion

        #region Present Offer

        public async Task<IList<IBusinessLoanOffer>> PresentOffer(string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);

            if (application == null)
                throw new InvalidOperationException("Application not found");

            var applicationOffers = await OfferEngineService.GetApplicationOffers(EntityType, entityId);

            var objActiveStatus = await GetActiveStatusWorkFlow(EntityType, entityId);
            EntityStatusService.ChangeStatus(EntityType, entityId, objActiveStatus.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["OfferGiven"], null);

            return applicationOffers.Offers;
        }

        #endregion Present Offer

        #region Generate And Send Agreement

        public async Task MCAAgreementCreationHandler(string entityType, string entityId, object data)
        {
            string result = string.Empty;
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);
            if (application == null)
                throw new InvalidOperationException("Application not found");
            var applicant = await BusinessApplicantService.Get(application.ApplicantId);
            if (applicant == null)
                throw new InvalidOperationException("Applicant not found");

            #region Update Bank Details in Deal
            var ownerId = await GetPrimaryOwnerId(entityId);
            if (!string.IsNullOrEmpty(ownerId))
            {
                var bankVerificationData = await DataAttributeService.GetAttribute(EntityType, entityId, "BankVerificationData", ownerId);
                var bankVerificationDetailsAll = (JArray)bankVerificationData;
                if (bankVerificationDetailsAll != null && bankVerificationDetailsAll.Count > 0)
                {
                    JObject bankVerificationDetails = (JObject)bankVerificationDetailsAll[0];
                    if (bankVerificationDetails != null)
                    {
                        var dealData = await OfferEngineService.GetApplicationOffers(EntityType, entityId);

                        var routingNumber = bankVerificationDetails.GetValue("routingMatch").ToString();
                        var accountNumber = bankVerificationDetails.GetValue("accountMatch").ToString();
                        dealData.DealOffer.RoutingNumber = routingNumber;
                        dealData.DealOffer.AccountNumber = accountNumber;

                        IDealOfferRequest dealRequest = new DealOfferRequest();
                        dealRequest.dealOffers = new List<IDealOffer>();
                        dealRequest.dealOffers.Add(dealData.DealOffer);

                        await OfferEngineService.AddDeal(EntityType, entityId, dealRequest);
                    }
                }
            }
            #endregion

            await GenerateLoanAgreementPDF(entityId, application, applicant);
        }

        #region Generate Loan Agreement PDF
        private async Task GenerateLoanAgreementPDF(string entityId, IApplication application, IApplicant applicant)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            if (application == null)
                application = await BusinessApplicationService.GetByApplicationNumber(entityId);

            if (application == null)
                throw new InvalidOperationException("Application not found");

            if (applicant == null)
                applicant = await BusinessApplicantService.Get(application.ApplicantId);

            if (applicant == null)
                throw new InvalidOperationException("Applicant not found");

            var applicationOffers = await OfferEngineService.GetApplicationOffers(EntityType, entityId);
            if (applicationOffers?.DealOffer != null)
            {
                ILoanAgreement body = GetLoanAgreementObject(application, applicant, applicationOffers.DealOffer);
                var agreementDocument = new LendFoundry.DocumentGenerator.Document
                {
                    Data = body,
                    Format = DocumentFormat.Pdf,
                    Name = "LoanAgreement.pdf",
                    Version = "1.0"
                };
                var agreementResult = await DocumentGeneratorService.Generate("loanagreement", "1.0", Format.Html, agreementDocument);

                if (agreementResult != null)
                {
                    var objMetaData = new MetaData();
                    objMetaData.TemplateName = "loanagreement";
                    objMetaData.TemplateVersion = "1.0";
                    await ApplicationDocumentService.Add(entityId, ApplicationProcessorConfiguration.DocumentCategory["LoanAgreement"], agreementResult.Content, agreementResult.DocumentName, null, objMetaData);
                }
            }
            else
            {
                throw new InvalidOperationException("Deal not generated");
            }
        }
        #endregion

        public async Task<string> MCAAgreementHandler(string entityType, string entityId, object data)
        {
            string result = string.Empty;
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);

            if (application == null)
                throw new InvalidOperationException("Application not found");
            var applicant = await BusinessApplicantService.Get(application.ApplicantId);
            if (applicant == null)
                throw new InvalidOperationException("Applicant not found");

            var loanAgreementDocuments = await ApplicationDocumentService.GetByCategory(entityId, ApplicationProcessorConfiguration.DocumentCategory["LoanAgreement"]);
            if (loanAgreementDocuments != null && loanAgreementDocuments.Count > 0)
            {
                var loanAggrementDoc = loanAgreementDocuments.LastOrDefault();
                if (loanAggrementDoc != null)
                {
                    LendFoundry.Syndication.LFDocuSign.Request.IEmbedSigningRequest objRequest = new LendFoundry.Syndication.LFDocuSign.Request.EmbedSigningRequest();
                    objRequest.DocumentId = new List<string>();
                    objRequest.DocumentId.Add(loanAggrementDoc.Id);
                    objRequest.Receipients = new List<IReceipient>();
                    applicant.Owners = applicant.Owners.OrderByDescending(i => i.IsPrimary).ToList();
                    foreach (var owner in applicant.Owners)
                    {
                        if (owner?.EmailAddresses != null && owner.EmailAddresses.Count > 0)
                        {
                            var rcpnt = new Receipient()
                            {
                                Email = owner.EmailAddresses.FirstOrDefault().Email,
                                Name = $"{owner.FirstName} {owner.LastName}"
                            };
                            objRequest.Receipients.Add(rcpnt);
                        }
                    }

                    objRequest.EmailBody = $"Hi,\n\n Please review and sign your merchant agreement.";
                    var objDocuSignResponse = await DocuSignService.GenerateEmbeddedView(EntityType, entityId, ApplicationProcessorConfiguration.DocumentCategory["LoanAgreement"], objRequest);

                    if (objDocuSignResponse != null && !string.IsNullOrEmpty(objDocuSignResponse.EnvelopeSummary.EnvelopeId))
                    {
                        var applicationOffers = await OfferEngineService.GetApplicationOffers(entityType, entityId);
                        if (applicationOffers?.DealOffer != null)
                        {
                            await EventHub.Publish(new AgreementDocsOut
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = applicationOffers.DealOffer,
                                Request = "AgreementDocsOut",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                        }
                        result = objDocuSignResponse.EnvelopeSummary.EnvelopeId;
                    }
                    else
                    {
                        throw new InvalidOperationException("Sorry not able to send loan agreement");
                    }
                }
                else
                {
                    throw new InvalidOperationException("Loan Agreement Not Generated Yet");
                }
            }
            return result;
        }

        public async Task<string> GenerateAndSendAgreement(string entityId)
        {
            return await MCAAgreementHandler(EntityType, entityId, null);
        }

        #endregion Generate And Send Agreement

        #region Private Methods

        private async Task<string> GetOrCreateUser(string email, string password, string name, bool isGuestLogin = false, bool isReApplied = false)
        {
            var tempusers = await IdentityService.GetUserByUsername(email);
            if (tempusers == null)
            {
                LendFoundry.Security.Identity.IUser user = new LendFoundry.Security.Identity.User()
                {
                    Email = email,
                    Name = name,
                    Username = email,
                    Password = string.IsNullOrWhiteSpace(password) ? ApplicationProcessorConfiguration.DefaultPassword : password,
                    Roles = ApplicationProcessorConfiguration.DefaultApplicantRoles
                };
                var userInfo = await IdentityService.CreateUser(user);

                if (string.IsNullOrEmpty(userInfo.Id))
                    throw new LendFoundry.Business.Applicant.UnableToCreateUserException();

                if (string.IsNullOrWhiteSpace(password) && isGuestLogin == false)
                {
                    LendFoundry.Security.Identity.IRequestPasswordResetRequest passwordResetrequest = new LendFoundry.Security.Identity.RequestPasswordResetRequest()
                    {
                        Username = userInfo.Username,
                        Portal = ApplicationProcessorConfiguration.ApplicatntPortal
                    };
                    IdentityService.RequestPasswordReset(passwordResetrequest);
                }
                return userInfo?.Id;
            }
            else if (isReApplied)
            {
                return tempusers.Id;
            }
            else
                throw new ApplicantAlreadyAssociateWithApplication(email);
        }

        #region TermofUseConsent
        //private async void SaveTermofUseConsent(string entityId, string applicantId, string ipAddress)
        //{

        //    Dictionary<string, string> objTermofUseConsent = new Dictionary<string, string>();
        //    objTermofUseConsent.Add("Logo", ApplicationProcessorConfiguration.ImageConfiguration.Logo);
        //    objTermofUseConsent.Add("SignedBy", applicantId);
        //    objTermofUseConsent.Add("IPAddress", ipAddress);
        //    ConsentService.Sign(EntityType, entityId, ApplicationProcessorConfiguration.DocumentCategory["TermofUseConsent"], objTermofUseConsent);
        //}

        #endregion TermofUseConsent

        #region PrivacypolicyConsent

        private async void SavePrivacypolicyConsent(string entityId, string applicantId, string ipAddress)
        {
            Dictionary<string, string> objPrivacypolicyConsent = new Dictionary<string, string>();
            objPrivacypolicyConsent.Add("Logo", ApplicationProcessorConfiguration.ImageConfiguration.Logo);
            objPrivacypolicyConsent.Add("SignedBy", applicantId);
            objPrivacypolicyConsent.Add("IPAddress", ipAddress);
            ConsentService.Sign(EntityType, entityId, ApplicationProcessorConfiguration.DocumentCategory["PrivacypolicyConsent"], objPrivacypolicyConsent);
        }

        #endregion PrivacypolicyConsent

        #region Credit Check Verification

        private async Task<IPersonalReportResponse> CallExperianPersonalSyndication(string entityId, string firstName,
            string lastName, string ssn, string address, string city,
            string state, string zipCode, string ownerId, IPersonalReportService personalReportService)
        {
            IPersonalReportResponse objPersonalReportResponse = new PersonalReportResponse();
            try
            {
                LendFoundry.Syndication.Experian.Request.IPersonalReportRequest objPersonalReportRequest = new GetPersonalReportRequest();
                objPersonalReportRequest.Firstname = firstName;
                objPersonalReportRequest.Lastname = lastName;
                objPersonalReportRequest.City = city;
                objPersonalReportRequest.Street = address;
                objPersonalReportRequest.State = state;
                objPersonalReportRequest.Zip = zipCode;
                objPersonalReportRequest.Ssn = ssn;
                objPersonalReportRequest.OwnerId = ownerId;

                objPersonalReportResponse = await personalReportService.GetPersonalReportResponse(EntityType, entityId, objPersonalReportRequest);
                if (objPersonalReportResponse != null)
                {
                    await EventHub.Publish("ExperainPersonalPDFRequested", new
                    {
                        EntityId = entityId,
                        EntityType = EntityType,
                        Response = new { ownerId },
                        Request = "ExperainPersonalPDFRequested",
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });
                }
            }
            catch (System.Exception ex) { }
            return objPersonalReportResponse;
        }

        #endregion Credit Check Verification

        private ApplicationDetailPDF GetApplicationPDFData(IApplication application, IApplicant applicant)
        {
            if (application == null)
                throw new ArgumentNullException(nameof(application));

            if (applicant == null)
                throw new ArgumentNullException(nameof(applicant));

            var applicationData = new ApplicationDetailPDF(application, applicant);
            applicationData.LoanTimeFrame = LookupService.GetLookupEntry("loanTimeFrame", application.LoanTimeFrame).FirstOrDefault().Value;
            applicationData.PurposeOfLoan = LookupService.GetLookupEntry("PurposeOfLoan", application.PurposeOfLoan).FirstOrDefault().Value;
            applicationData.BusinessType = LookupService.GetLookupEntry("businessTypes", applicant.BusinessType).FirstOrDefault().Value;
            applicationData.Industry = LookupService.GetLookupEntry("industry", applicant.Industry).FirstOrDefault().Value;
            applicationData.PropertyType = LookupService.GetLookupEntry("propertyType", application.PropertyType).FirstOrDefault().Value;
            // applicationData.Country = LookupService.GetLookupEntry ("country", (application.PrimaryAddress.Country).ToLower()).FirstOrDefault ().Value;
            applicationData.RequestedAmount = FormatMoney(application.RequestedAmount);
            applicationData.AverageBankBalances = FormatMoney(application.SelfDeclareInformation.AverageBankBalance);
            applicationData.AnnualRevenue = FormatMoney(application.SelfDeclareInformation.AnnualRevenue);
            applicationData.Owners = applicant.Owners.OrderByDescending(i => i.IsPrimary).ToList();
            return applicationData;
        }

        private ILoanAgreement GetLoanAgreementObject(IApplication application, IApplicant applicant, IDealOffer objOffer)
        {
            ILoanAgreement body = new LoanAgreement();

            if (objOffer != null)
            {
                body.PurchasePrice = FormatMoney(objOffer.LoanAmount);
                body.RecieptsPurchasedAmount = FormatMoney(objOffer.RepaymentAmount);
                body.SpecificDailyAmmount = FormatMoney(objOffer.PaymentAmount);
                body.AccountRoutingNumber = objOffer.RoutingNumber;
                body.AccountCheckingAccountNumber = objOffer.AccountNumber;
                body.BankName = objOffer.BankName;
                body.AccountPayment = FormatMoney(objOffer.PaymentAmount);
                body.OpportunityDocPaymentType = objOffer.TypeOfPayment;
                body.OpportunityOrginizationFee = FormatMoney(objOffer.OriginatingFeeAmount);
                // body.SpecifiedPercentage = "10%";
            }
            //Apllication Value
            body.SignedBy = application.ApplicantId;

            body.ContactPerson = application.ContactFirstName + " " + application.ContactLastName;
            if (application.PrimaryPhone != null && !string.IsNullOrEmpty(application.PrimaryPhone.Phone))
            {
                //USPhone Format
                body.ContactPhone = Regex.Replace(application.PrimaryPhone.Phone, ApplicationProcessorConfiguration.PhonePattern, ApplicationProcessorConfiguration.PhoneFormat);
            }
            body.City = application.PrimaryAddress.City;
            body.State = application.PrimaryAddress.State;
            body.Street = application.PrimaryAddress.AddressLine1;
            body.Zip = application.PrimaryAddress.ZipCode;

            //Applicant Value
            body.DBA = applicant.DBA;
            body.IPAddress = application.ClientIpAddress;
            if (applicant.Owners.Count > 0)
            {
                applicant.Owners = applicant.Owners.OrderByDescending(i => i.IsPrimary).ToList();
                body.Owners = applicant.Owners;
            }

            var businesstypeExist = LookupService.GetLookupEntry("businessTypes", applicant.BusinessType);
            if (businesstypeExist != null && businesstypeExist.Count > 0)
            {
                body.BusinessType = businesstypeExist.FirstOrDefault().Value;
            }
            body.Branch = ""; //branch is not available in bankdetails.

            DateTimeOffset defaultValue = new DateTimeOffset(DateTime.MinValue, TimeSpan.Zero);
            if (applicant.BusinessStartDate.Time != defaultValue)
            {
                body.BusinessStartDate = applicant.BusinessStartDate.Time.ToString("MM'/'dd'/'yyyy");
            }
            body.LegalBusinessName = applicant.LegalBusinessName;
            body.FederalTaxID = applicant.BusinessTaxID;

            //images
            body.Logo = ApplicationProcessorConfiguration.ImageConfiguration.Logo;

            //signature date
            body.DateCreated = TenantTime.Now.ToString("MM'/'dd'/'yyyy");

            return body;
        }

        private static bool HasProperty<T>(Func<T> property)
        {
            try
            {
                property();
                return true;
            }
            catch (RuntimeBinderException)
            {
                return false;
            }
        }

        private static T GetValue<T>(Func<T> property)
        {
            return property();
        }
        private void EnsureBusinessInputIsValid(ISubmitBusinessApplicationRequest applicationRequest)
        {
            if (applicationRequest == null)
                throw new ArgumentNullException(nameof(applicationRequest), "application cannot be empty");

            if (string.IsNullOrEmpty(applicationRequest.Phone))
                throw new ArgumentException("Personal mobile/Phone Number is mandatory");

            if (!Regex.IsMatch(applicationRequest.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
                throw new ArgumentException("Email Address is not valid");

            if (string.IsNullOrEmpty(applicationRequest.Email))
                throw new ArgumentException("Personal email address is mandatory");

            if (applicationRequest.RequestedAmount <= 0)
                throw new ArgumentNullException("Requested amount is mandatory");

            if (string.IsNullOrWhiteSpace(applicationRequest.PurposeOfLoan))
                throw new ArgumentNullException("Purpose Of Loan  is mandatory");
            var LoanTimeFrameExists = LookupService.GetLookupEntry("loanTimeFrame", applicationRequest.LoanTimeFrame);
            if (LoanTimeFrameExists == null)
                throw new InvalidArgumentException("LoantTimeFrame is not valid");
            if (string.IsNullOrEmpty(applicationRequest.ContactFirstName))
                throw new ArgumentNullException($"{nameof(applicationRequest.ContactFirstName)} is mandatory");
            if (applicationRequest.ContactFirstName.Length >= 40)
                throw new ArgumentNullException($"{nameof(applicationRequest.ContactFirstName)} Max length is 40");
            if (string.IsNullOrEmpty(applicationRequest.ContactLastName))
                throw new ArgumentNullException($"{nameof(applicationRequest.ContactLastName)} is mandatory");
            if (applicationRequest.ContactLastName.Length >= 80)
                throw new ArgumentNullException($"{nameof(applicationRequest.ContactLastName)} Max length is 80");

            if (string.IsNullOrWhiteSpace(applicationRequest.BusinessTaxID))
                throw new ArgumentNullException($"{nameof(applicationRequest.BusinessTaxID)} is mandatory");

            if (applicationRequest.BusinessStartDate == null)
                throw new ArgumentNullException($"{nameof(applicationRequest.BusinessStartDate)} is mandatory");

            if (string.IsNullOrWhiteSpace(applicationRequest.BusinessType))
                throw new ArgumentNullException($"{nameof(applicationRequest.BusinessType)} is mandatory");

            if (string.IsNullOrWhiteSpace(applicationRequest.LegalBusinessName))
                throw new ArgumentNullException($"{nameof(applicationRequest.LegalBusinessName)} is mandatory");

            if (applicationRequest.LegalBusinessName.Length >= 255)
                throw new ArgumentNullException($"{nameof(applicationRequest.LegalBusinessName)} max length is 255");

            if (applicationRequest.AddressLine1.Length >= 50)
                throw new ArgumentNullException($"{nameof(applicationRequest.AddressLine1)} max length is 50");
            if (applicationRequest.City.Length >= 50)
                throw new ArgumentNullException($"{nameof(applicationRequest.City)} max length is 50");
            if (applicationRequest.ZipCode.Length < 5)
                throw new ArgumentNullException($"{nameof(applicationRequest.ZipCode)} min length is 5");
            if (applicationRequest.BusinessTaxID.Length >= 10)
                throw new ArgumentNullException($"{nameof(applicationRequest.BusinessTaxID)} max length is 10");
            if (!Regex.IsMatch(applicationRequest.BusinessTaxID, ApplicationProcessorConfiguration.BusinessTaxIdRegex))
                throw new ArgumentException($"Invalid {nameof(applicationRequest.BusinessTaxID)} format");

            if (applicationRequest.AnnualRevenue <= 0)
                throw new ArgumentNullException($"{nameof(applicationRequest.AnnualRevenue)} is mandatory");

            if (applicationRequest.AverageBankBalances <= 0)
                throw new ArgumentNullException($"{nameof(applicationRequest.AverageBankBalances)} is mandatory");

            if (string.IsNullOrWhiteSpace(applicationRequest.AddressLine1))
                throw new ArgumentException("Current address line1 is mandatory");

            if (string.IsNullOrWhiteSpace(applicationRequest.City))
                throw new ArgumentNullException("Current address city is mandatory");

            if (string.IsNullOrWhiteSpace(applicationRequest.State))
                throw new ArgumentNullException("Current address state is mandatory");

            if (string.IsNullOrWhiteSpace(applicationRequest.ZipCode))
                throw new ArgumentNullException("Current address ZipCode is mandatory");
        }
        private bool EligibleForApplicationSubmit(ISubmitBusinessApplicationRequest applicationRequest)
        {
            bool eligible = true;
            if (applicationRequest == null)
                return eligible = false;
            if (string.IsNullOrEmpty(applicationRequest.Phone))
                return eligible = false;
            if (!Regex.IsMatch(applicationRequest.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
                return eligible = false;
            if (string.IsNullOrEmpty(applicationRequest.Email))
                return eligible = false;
            if (applicationRequest.RequestedAmount <= 0)
                return eligible = false;
            if (string.IsNullOrWhiteSpace(applicationRequest.PurposeOfLoan))
                return eligible = false;
            var LoanTimeFrameExists = LookupService.GetLookupEntry("loanTimeFrame", applicationRequest.LoanTimeFrame);
            if (LoanTimeFrameExists == null)
                return eligible = false;
            if (string.IsNullOrEmpty(applicationRequest.ContactFirstName))
                return eligible = false;
            if (applicationRequest.ContactFirstName.Length >= 40)
                return eligible = false;
            if (string.IsNullOrEmpty(applicationRequest.ContactLastName))
                return eligible = false;
            if (applicationRequest.ContactLastName.Length >= 80)
                return eligible = false;
            if (string.IsNullOrWhiteSpace(applicationRequest.BusinessTaxID))
                return eligible = false;
            if (applicationRequest.BusinessStartDate == null)
                return eligible = false;
            if (string.IsNullOrWhiteSpace(applicationRequest.BusinessType))
                return eligible = false;
            if (string.IsNullOrWhiteSpace(applicationRequest.LegalBusinessName))
                return eligible = false;
            if (applicationRequest.LegalBusinessName.Length >= 255)
                return eligible = false;
            if (applicationRequest.AddressLine1.Length >= 50)
                return eligible = false;
            if (applicationRequest.City.Length >= 50)
                return eligible = false;
            if (applicationRequest.ZipCode.Length < 5)
                return eligible = false;
            if (applicationRequest.BusinessTaxID.Length >= 10)
                return eligible = false;
            if (!Regex.IsMatch(applicationRequest.BusinessTaxID, ApplicationProcessorConfiguration.BusinessTaxIdRegex))
                return eligible = false;
            if (applicationRequest.AnnualRevenue <= 0)
                return eligible = false;
            if (applicationRequest.AverageBankBalances <= 0)
                return eligible = false;
            if (string.IsNullOrWhiteSpace(applicationRequest.AddressLine1))
                return eligible = false;
            if (string.IsNullOrWhiteSpace(applicationRequest.City))
                return eligible = false;
            if (string.IsNullOrWhiteSpace(applicationRequest.State))
                return eligible = false;
            if (string.IsNullOrWhiteSpace(applicationRequest.ZipCode))
                return eligible = false;
            return eligible;
        }
        private void EligibleForLeadCreate(ISubmitBusinessApplicationRequest applicationRequest)
        {
            if (applicationRequest == null)
                throw new ArgumentNullException(nameof(applicationRequest), "application cannot be empty");
            if (string.IsNullOrEmpty(applicationRequest.ContactFirstName))
                throw new ArgumentException("Contact FirstName is mandatory");
            if (string.IsNullOrEmpty(applicationRequest.ContactLastName))
                throw new ArgumentException("Contact LastName is mandatory");
            if (!Regex.IsMatch(applicationRequest.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
                throw new ArgumentException("Email Address is not valid");
        }

        private void ValidateLeadInput(ISubmitBusinessApplicationRequest applicationRequest)
        {
            if (applicationRequest == null)
                throw new ArgumentNullException(nameof(applicationRequest), "application cannot be empty");

            if (string.IsNullOrEmpty(applicationRequest.Phone))
                throw new ArgumentException("Personal mobile/Phone Number is mandatory");

            if (!Regex.IsMatch(applicationRequest.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
                throw new ArgumentException("Email Address is not valid");

            if (string.IsNullOrEmpty(applicationRequest.Email))
                throw new ArgumentException("Personal email address is mandatory");

            if (applicationRequest.RequestedAmount <= 0)
                throw new ArgumentNullException("Requested amount is mandatory");

            if (string.IsNullOrWhiteSpace(applicationRequest.PurposeOfLoan))
                throw new ArgumentNullException("Purpose Of Loan  is mandatory");
            var LoanTimeFrameExists = LookupService.GetLookupEntry("loanTimeFrame", applicationRequest.LoanTimeFrame);
            if (LoanTimeFrameExists == null)
                throw new InvalidArgumentException("LoantTimeFrame is not valid");
            if (string.IsNullOrEmpty(applicationRequest.ContactFirstName))
                throw new ArgumentNullException($"{nameof(applicationRequest.ContactFirstName)} is mandatory");
            if (applicationRequest.ContactFirstName.Length >= 40)
                throw new ArgumentNullException($"{nameof(applicationRequest.ContactFirstName)} Max length is 40");
            if (string.IsNullOrEmpty(applicationRequest.ContactLastName))
                throw new ArgumentNullException($"{nameof(applicationRequest.ContactLastName)} is mandatory");
            if (applicationRequest.ContactLastName.Length >= 80)
                throw new ArgumentNullException($"{nameof(applicationRequest.ContactLastName)} Max length is 80");
        }

        private LendFoundry.Business.Applicant.IApplicantRequest GetBusinessApplicantRequest(ISubmitBusinessApplicationRequest request)
        {
            LendFoundry.Business.Applicant.IApplicantRequest applicantRequest = new LendFoundry.Business.Applicant.ApplicantRequest();

            #region Basic Information

            if (!string.IsNullOrEmpty(request.ApplicantId))
            {
                applicantRequest.Id = request.ApplicantId;
            }
            applicantRequest.ContactFirstName = request.ContactFirstName;
            applicantRequest.ContactLastName = request.ContactLastName;
            applicantRequest.LegalBusinessName = request.LegalBusinessName;
            applicantRequest.DBA = (!string.IsNullOrEmpty(request.DBA)) ? request.DBA : request.LegalBusinessName;
            applicantRequest.BusinessTaxID = request.BusinessTaxID;
            if (!string.IsNullOrEmpty(request.BusinessType))
            {
                applicantRequest.BusinessType = LookupService.GetLookupEntry("businessTypes", request.BusinessType).Keys.SingleOrDefault();
            }
            if (!string.IsNullOrEmpty(request.Industry))
            {
                applicantRequest.Industry = LookupService.GetLookupEntry("industry", request.Industry).Keys.SingleOrDefault();
            }
            if (!string.IsNullOrEmpty (request.PropertyType)) {
                applicantRequest.PropertyType = LookupService.GetLookupEntry ("propertyType", request.PropertyType).Keys.SingleOrDefault ();
            }     
            if (!string.IsNullOrEmpty (request.Country)) {
                request.Country = LookupService.GetLookupEntry ("country", (request.Country).ToLower ()).Values.FirstOrDefault ();
            }
            applicantRequest.BusinessStartDate = new TimeBucket(request.BusinessStartDate);
            applicantRequest.SICCode = request.SICCode;
           

            #endregion Basic Information

            #region Primary ContactDetails

            applicantRequest.PrimaryAddress = new LendFoundry.Business.Applicant.Address
            {
                AddressLine1 = request.AddressLine1,
                AddressType = LendFoundry.Business.Applicant.AddressType.Business,
                City = request.City,
                ZipCode = request.ZipCode,
                State = request.State,
                IsDefault = true,
                Country = !string.IsNullOrEmpty(request.Country) ? request.Country : LookupService.GetLookupEntry("country", "usa").Values.FirstOrDefault()
            };
            applicantRequest.PrimaryPhone = new LendFoundry.Business.Applicant.PhoneNumber
            {
                IsDefault = true,
                Phone = request.Phone,
                PhoneType = LendFoundry.Business.Applicant.PhoneType.Mobile
            };
            applicantRequest.PrimaryEmail = new LendFoundry.Business.Applicant.EmailAddress
            {
                Email = request.Email,
                EmailType = LendFoundry.Business.Applicant.EmailType.Work,
                IsDefault = true
            };
            applicantRequest.PrimaryFax = null;

            #endregion Primary ContactDetails

            #region Owners Information
            if (request.Owners != null && request.Owners.Any())
            {
                foreach (var owner in request.Owners)
                {
                    if (owner.Addresses != null && owner.Addresses.Any())
                    {
                        foreach (var address in owner.Addresses)
                        {
                            if (!string.IsNullOrEmpty(address.Country))
                            {
                                address.Country = LookupService.GetLookupEntry("country", (address.Country).ToLower()).Values.FirstOrDefault();
                            }
                        }
                    }
                }
            }
            applicantRequest.Owners = request.Owners;

            #endregion Owners Information

            #region ContactDetails

            applicantRequest.PhoneNumbers = new List<LendFoundry.Business.Applicant.IPhoneNumber> {
                new LendFoundry.Business.Applicant.PhoneNumber {
                IsDefault = true,
                Phone = request.Phone,
                PhoneType = LendFoundry.Business.Applicant.PhoneType.Mobile
                }
            };

            applicantRequest.EmailAddresses = new List<LendFoundry.Business.Applicant.IEmailAddress> {
                new LendFoundry.Business.Applicant.EmailAddress {
                Email = request.Email,
                EmailType = LendFoundry.Business.Applicant.EmailType.Work,
                IsDefault = true
                }
            };
            applicantRequest.Addresses = new List<LendFoundry.Business.Applicant.IAddress> {
                new LendFoundry.Business.Applicant.Address {
                AddressLine1 = request.AddressLine1,
                AddressType = LendFoundry.Business.Applicant.AddressType.Business,
                City = request.City,
                ZipCode = request.ZipCode,
                State = request.State,
                Country = !string.IsNullOrEmpty (request.Country) ? request.Country : LookupService.GetLookupEntry ("country", "usa").Values.FirstOrDefault (),
                IsDefault = true
                }
            };

            #endregion ContactDetails

            return applicantRequest;
        }

        private IApplicationRequest GetBusinessApplicationRequest(ISubmitBusinessApplicationRequest request)
        {
            IApplicationRequest applicationRequest = new ApplicationRequest
            {
                LeadOwnerId = request.LeadOwnerId,
                PartnerUserId = request.PartnerUserId,
                PrimaryApplicant = GetBusinessApplicantRequest(request),
                RequestedAmount = request.RequestedAmount,
                DateNeeded = new TimeBucket(request.DateNeeded),
                ContactFirstName = request.ContactFirstName,
                ContactLastName = request.ContactLastName,

                #region Primary ContactDetails

                PrimaryEmail = new LendFoundry.Business.Application.EmailAddress
                {
                    Email = request.Email,
                    EmailType = LendFoundry.Business.Application.EmailType.Work,
                    IsDefault = true
                },
                PrimaryAddress = new LendFoundry.Business.Application.Address
                {
                    AddressLine1 = request.AddressLine1,
                    AddressType = LendFoundry.Business.Application.AddressType.Work,
                    City = request.City,
                    Country = !string.IsNullOrEmpty(request.Country) ? request.Country : LookupService.GetLookupEntry("country", "usa").Values.FirstOrDefault(),
                    ZipCode = request.ZipCode,
                    State = request.State,
                    IsDefault = true
                },
                PrimaryPhone = new LendFoundry.Business.Application.PhoneNumber
                {
                    IsDefault = true,
                    Phone = request.Phone,
                    PhoneType = LendFoundry.Business.Application.PhoneType.Mobile
                },
                PrimaryFax = null,

                #endregion Primary ContactDetails

                Source = GetSource(request.Source),
                SelfDeclareInformation = GetSelfInfomrationDetails(request.AnnualRevenue,
                request.AverageBankBalances,
                request.HaveExistingLoan),
                OtherPurposeDescription = request.OtherPurposeDescription,
                ClientIpAddress = request.ClientIpAddress
            };

            if (request.ExternalReferences != null && request.ExternalReferences.Count > 0)
            {
                applicationRequest.ExternalReferences = new List<IExternalReferences>();
                applicationRequest.ExternalReferences = request.ExternalReferences;
            }
            if (!string.IsNullOrEmpty(request.LoanTimeFrame))
            {
                applicationRequest.LoanTimeFrame = LookupService.GetLookupEntry("loanTimeFrame", request.LoanTimeFrame).Keys.SingleOrDefault();
            }
            if (!string.IsNullOrEmpty(request.PurposeOfLoan))
            {
                applicationRequest.PurposeOfLoan = LookupService.GetLookupEntry("purposeOfLoan", request.PurposeOfLoan).Keys.SingleOrDefault();
            }
            if (!string.IsNullOrEmpty(request.PropertyType))
            {
                applicationRequest.PropertyType = LookupService.GetLookupEntry("propertyType", request.PropertyType).Keys.SingleOrDefault();
            }

            return applicationRequest;
        }

        private ISelfDeclareInformation GetSelfInfomrationDetails(double annualRevenue, double averageBankBalances, bool haveExistingLoan)
        {
            ISelfDeclareInformation selfDeclareInformation = new SelfDeclareInformation();
            selfDeclareInformation.AnnualRevenue = annualRevenue;
            selfDeclareInformation.AverageBankBalance = averageBankBalances;
            selfDeclareInformation.IsExistingBusinessLoan = haveExistingLoan;
            return selfDeclareInformation;
        }

        private ISource GetSource(string Source)
        {
            ISource source = new Source();
            if (!string.IsNullOrEmpty(Source))
            {
                var SourceValues = LookupService.GetLookupEntry("sourceType", Source).SingleOrDefault();
                source.SourceReferenceId = SourceValues.Key;
                source.SourceType = SourceValues.Key;
            }
            return source;
        }

        private async void InitialAssignApplication(string entityId, string leadOwnerId)
        {
            try
            {
                var leadOwner = await IdentityService.GetUserById(leadOwnerId);
                if (leadOwner != null)
                {
                    IAssignmentRequest assignToObject = new AssignmentRequest()
                    {
                        Role = ApplicationProcessorConfiguration.InitialAssigmentRole,
                        User = leadOwner.Username
                    };
                    await AssignmentService.Assign(EntityType, entityId, assignToObject);
                }
            }
            catch (System.Exception ex)
            {

            }
        }
        private string RenderXMLData(string xmlString)
        {
            xmlString = new Regex("\\<\\?xml.*\\?>").Replace(xmlString, "");
            XsltSettings sets = new XsltSettings(true, true);
            string xsltPath = (ApplicationProcessorConfiguration.ExperianXsltPath + ApplicationProcessorConfiguration.ExperianXsltPathPPRProFilePath);
            XsltArgumentList args = new XsltArgumentList();
            XmlReaderSettings xmlSettings = new XmlReaderSettings();
            xmlSettings.DtdProcessing = DtdProcessing.Parse;
            xmlSettings.ValidationType = ValidationType.DTD;
            XslCompiledTransform transform = new XslCompiledTransform();
            transform.Load(xsltPath, sets, new XmlUrlResolver());
            StringWriter results = new StringWriter();
            using (XmlReader reader = XmlReader.Create(new StringReader(xmlString), xmlSettings))
            {
                transform.Transform(reader, args, results);
            }
            return results.ToString();
        }
        #endregion Private Methods

        #region PayNet

        public async Task<ISearchCompanyResponse> SearchCompany(string entityId, ISearchCompanyRequest request)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            if (string.IsNullOrWhiteSpace(request.CompanyName))
                throw new ArgumentNullException(nameof(request.CompanyName));
            if (string.IsNullOrWhiteSpace(request.State))
                throw new ArgumentNullException(nameof(request.State));

            LendFoundry.Syndication.Paynet.Request.ISearchCompanyRequest objRequest = new LendFoundry.Syndication.Paynet.Request.SearchCompanyRequest();

            objRequest.CompanyName = request.CompanyName;
            objRequest.TaxId = request.TaxId;
            objRequest.Alias = request.Alias;
            objRequest.Address = request.Address;
            objRequest.City = request.City;
            objRequest.StateCode = request.State;
            objRequest.Phone = request.Phone;

            var objReport = await PaynetReportService.SearchCompany(EntityType, entityId, objRequest);
            return objReport;
        }

        public async Task<bool> GetPaynetReport(string entityId, string paynetId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrWhiteSpace(paynetId))
                throw new ArgumentNullException(nameof(paynetId));

            bool result = false;
            LendFoundry.Syndication.Paynet.Request.GetReportRequest objReportRequest = new LendFoundry.Syndication.Paynet.Request.GetReportRequest();
            objReportRequest.PaynetId = paynetId;
            objReportRequest.ReportFormat = ApplicationProcessorConfiguration.PaynetReportFormat;

            var objReportResponse = await PaynetReportService.GetCompanyReport(EntityType, entityId, objReportRequest);
            if (objReportRequest.ReportFormat.Equals((PaynetReportFormat.XML).ToString()))
            {
                result = true;
            }
            else
            {
                byte[] fileByte;
                string fileName;
                if (objReportResponse.filesField.Count != 0)
                {
                    fileByte = objReportResponse.filesField.FirstOrDefault().bytesField;
                    fileName = objReportResponse.filesField.FirstOrDefault().fileNameField;
                }
                else
                {
                    fileByte = Encoding.ASCII.GetBytes(objReportResponse.htmlField);
                    fileName = entityId + ".html";
                }
                var objApplicationDocument = await ApplicationDocumentService.Add(entityId, ApplicationProcessorConfiguration.DocumentCategory["Paynet"], fileByte, fileName, null, null);
                if (objApplicationDocument != null && !string.IsNullOrEmpty(objApplicationDocument.DocumentId))
                {
                    result = true;
                }
            }

            return result;
        }

        #endregion PayNet

        #region Perform CreditCheck Verification

        public async Task<string> PerformCreditCheckVerification(string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            string result = string.Empty;

            var referencenumber = Guid.NewGuid().ToString("N");

            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);

            if (application == null)
                throw new InvalidOperationException("Application not found");

            await EventHub.Publish(new CreditCheckVerification
            {
                EntityId = application.ApplicationNumber,
                EntityType = EntityType,
                Response = null,
                Request = "PerformCreditCheckVerification",
                ReferenceNumber = referencenumber
            });
            result = "CreditCheck Verification Completed";
            return result;
        }

        #endregion Perform CreditCheck Verification

        #region Perform Business Tax Id Verification

        public async Task<string> PerformBusinessTaxIdVerification(string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            string result = string.Empty;

            var referencenumber = Guid.NewGuid().ToString("N");

            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);

            if (application == null)
                throw new InvalidOperationException("Application not found");

            await EventHub.Publish(new BusinessTaxIdVerification
            {
                EntityId = application.ApplicationNumber,
                EntityType = EntityType,
                Response = null,
                Request = "PerformBusinessTaxIdVerification",
                ReferenceNumber = referencenumber
            });
            result = "Business Tax Id Verification Completed";
            return result;
        }

        #endregion Perform Business Tax Id Verification

        #region Perform Business Review CreditRisk

        public async Task<string> PerformBusinessReviewCreditRisk(string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            string result = string.Empty;

            var referencenumber = Guid.NewGuid().ToString("N");

            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);

            if (application == null)
                throw new InvalidOperationException("Application not found");

            await EventHub.Publish(new BusinessCreditRiskVerification
            {
                EntityId = application.ApplicationNumber,
                EntityType = EntityType,
                Response = null,
                Request = "PerformBusinessReviewCreditRisk",
                ReferenceNumber = referencenumber
            });
            result = "Business Review CreditRisk Completed";
            return result;
        }

        #endregion Perform Business Review CreditRisk

        #region Get Application Details

        public async Task<IApplicationDetails> GetApplicationDetails(string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            var objResponse = await BusinessApplicationService.GetApplicationDetails(entityId);
            if(ApplicationProcessorConfiguration.ExtractedFieldRequired)
                objResponse.ExtractedField = await GetExtractedData(entityId,"4g5jhg");
            return objResponse;
        }

        #endregion Get Application Details

        #region Edit Application

        public async Task<IApplicationDetails> EditApplication(string entityId, ISubmitBusinessApplicationRequest request)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            EnsureBusinessInputIsValid(request);

            var applicationRequest = GetBusinessApplicationRequest(request);

            await BusinessApplicationService.UpdateApplication(entityId, applicationRequest);
            var objResponse = await BusinessApplicationService.GetApplicationDetails(entityId);
            return objResponse;
        }

        #endregion Edit Application

        #region Add Deal

        public async Task<IApplicationOffer> AddDeal(string entityId, IDealOfferRequest request)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            if (request.dealOffers != null)
            {
                if (request.dealOffers[0].AmountFunded >= ApplicationProcessorConfiguration.MinFundedAmount) { }
                else
                {
                    throw new InvalidOperationException($"{ApplicationProcessorConfiguration.MinFundedAmountMessage}");
                }
            }

            var fundingAccountData = new AccountTypeRequest();
            fundingAccountData.IsFundingAccount = true;

            var fundingAccount = await CashflowService.GetAccountByType(EntityType, entityId, fundingAccountData);

            if (fundingAccount != null && fundingAccount.accounts != null && fundingAccount.accounts.Count > 0) {
                request.dealOffers[0].BankName = fundingAccount.accounts[0].BankName;
                request.dealOffers[0].AccountType = fundingAccount.accounts[0].AccountType;
                var objActiveStatus = await GetActiveStatusWorkFlow(EntityType, entityId);
                var objBankVerification = await VerificationEngineService.GetStatus(EntityType, entityId, objActiveStatus.StatusWorkFlowId, "BankVerification");
                if (objBankVerification != null && objBankVerification.CurrentStatus == LendFoundry.VerificationEngine.CurrentStatus.Completed)
                {
                    var ownerId = await GetPrimaryOwnerId(entityId);
                    var bankVerificationData = await DataAttributeService.GetAttribute(EntityType, entityId, "BankVerificationData", ownerId);
                    var bankVerificationDetailsAll = (JArray)bankVerificationData;
                    if (bankVerificationDetailsAll != null && bankVerificationDetailsAll.Count > 0)
                    {
                        JObject bankVerificationDetails = (JObject)bankVerificationDetailsAll[0];
                        if (bankVerificationDetails != null)
                        {
                            var routingNumber = bankVerificationDetails.GetValue("routingMatch").ToString();
                            var accountNumber = bankVerificationDetails.GetValue("accountMatch").ToString();
                            request.dealOffers[0].RoutingNumber = routingNumber;
                            request.dealOffers[0].AccountNumber = accountNumber;
                        }
                    }
                }
                var offer = await Task.Run(() => OfferEngineService.AddDeal(EntityType, entityId, request));

                if (objBankVerification != null && objBankVerification.CurrentStatus == LendFoundry.VerificationEngine.CurrentStatus.Completed)
                {
                    await GenerateLoanAgreementPDF(entityId, null, null);
                }

                EntityStatusService.ChangeStatus(EntityType, entityId, objActiveStatus.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["DocsRequested"], null);

                await EventHub.Publish(new DealGenerated
                {
                    EntityId = entityId,
                    EntityType = EntityType,
                    Response = offer,
                    Request = "DealGenrated",
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
                return offer;
            }
            else
            {
                throw new InvalidOperationException("Funding account not selected");
            }
        }

        #endregion Add Deal

        #region LexisNexis

        #region ADL Search

        public async Task<IADLSearchResponse> ADLSearch(string entityId, CapitalAlliance.ApplicationProcessor.Request.SearchADLRequest searchRequest)
        {

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (searchRequest == null)
                throw new ArgumentNullException(nameof(searchRequest));
            if (string.IsNullOrWhiteSpace(searchRequest.Ssn))
                throw new ArgumentNullException($"The {nameof(searchRequest.Ssn)} cannot be null");

            if (!Regex.IsMatch(searchRequest.Ssn, "^([0-9]{3}-[0-9]{2}-[0-9]{4}|[0-9]+)$"))
                throw new ArgumentException("Invalid Ssn Number format");

            if (string.IsNullOrWhiteSpace(searchRequest.FirstName))
                throw new ArgumentNullException($"The {nameof(searchRequest.FirstName)} cannot be null");

            if (!Regex.IsMatch(searchRequest.FirstName, "^([a-zA-Z\\s]+)$"))
                throw new ArgumentException($"Invalid {nameof(searchRequest.FirstName)}");

            if (string.IsNullOrWhiteSpace(searchRequest.LastName))
                throw new ArgumentNullException($"The {nameof(searchRequest.LastName)} cannot be null");

            if (!Regex.IsMatch(searchRequest.LastName, "^([a-zA-Z\\s]+)$"))
                throw new ArgumentException($"Invalid {nameof(searchRequest.LastName)}");

            LendFoundry.Syndication.LexisNexis.ADLFunctions.ADLSearchRequest objRequest = new LendFoundry.Syndication.LexisNexis.ADLFunctions.ADLSearchRequest();

            objRequest.Firstname = searchRequest.FirstName;
            objRequest.Lastname = searchRequest.LastName;
            objRequest.Ssn = searchRequest.Ssn;
            if (ApplicationProcessorConfiguration.BankruptcyFcraState == null || ApplicationProcessorConfiguration.BankruptcyFcraState.Contains(searchRequest.State))
            {
                objRequest.Streetaddress1 = searchRequest.Streetaddress1;
                objRequest.City = searchRequest.City;
                objRequest.Zip5 = searchRequest.Zip5;
                objRequest.State = searchRequest.State;
            }
            var objSearchResponse = LexisNexisService.ADLSearch(EntityType, entityId, objRequest).Result;
            if (objSearchResponse?.PossibleADLs?.Count == 1)
            {
                var ReportUniqueId = objSearchResponse.PossibleADLs.FirstOrDefault().Result.Adl;
                //var ReportTMSId = objSearchResponse.Records.FirstOrDefault ().TMSId;
                if (!string.IsNullOrWhiteSpace(ReportUniqueId))
                {
                    if (searchRequest.RequestedProduct == "Bankruptcy")
                    {
                        Request.IBankruptcyReportRequest objReportRequest = new Request.BankruptcyReportRequest()
                        {
                            UniqueId = ReportUniqueId,
                            OwnerId = searchRequest.OwnerId
                        };
                        var reportResponse = await BankruptcyReport(entityId, objReportRequest);
                        if (objSearchResponse?.PossibleADLs?.Count == 0)
                        {
                            var bankruptcyReportRecords = new
                            {
                                Recordcount = 0,
                                referenceNumber = Guid.NewGuid().ToString("N")
                            };
                            if (string.IsNullOrEmpty(searchRequest.OwnerId))
                            {
                                searchRequest.OwnerId = await GetPrimaryOwnerId(entityId);
                            }
                            await DataAttributeService.SetAttribute(EntityType, entityId, "bankruptcyReportRecords", bankruptcyReportRecords, searchRequest.OwnerId);
                        }

                    }
                    if (searchRequest.RequestedProduct == "Criminal")
                    {
                        if (ReportUniqueId != null)
                        {
                            Request.ICriminalRecordReportRequest objReportRequest = new Request.CriminalRecordReportRequest()
                            {
                                UniqueId = ReportUniqueId,
                                // QueryId = ReportQueryId,
                                OwnerId = searchRequest.OwnerId
                            };
                            var reportResponse = await CriminalRecordReport(entityId, objReportRequest);
                        }
                        if (objSearchResponse?.PossibleADLs?.Count == 0)
                        {
                            var criminalRecords = new
                            {
                                Recordcount = 0,
                                referenceNumber = Guid.NewGuid().ToString("N")
                            };
                            if (string.IsNullOrEmpty(searchRequest.OwnerId))
                            {
                                searchRequest.OwnerId = await GetPrimaryOwnerId(entityId);
                            }
                            await DataAttributeService.SetAttribute(EntityType, entityId, "criminalRecords", criminalRecords, searchRequest.OwnerId);
                        }
                    }
                }
            }
            return objSearchResponse;
        }

        #endregion ADL Search

        #region Bankruptcy Search

        public async Task<IBankruptcySearchResponse> BankruptcySearch(string entityId, Request.ISearchBankruptcyRequest searchRequest)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (searchRequest == null)
                throw new ArgumentNullException(nameof(searchRequest));
            if (string.IsNullOrWhiteSpace(searchRequest.Ssn))
                throw new ArgumentNullException($"The {nameof(searchRequest.Ssn)} cannot be null");

            if (!Regex.IsMatch(searchRequest.Ssn, "^([0-9]{3}-[0-9]{2}-[0-9]{4}|[0-9]+)$"))
                throw new ArgumentException("Invalid Ssn Number format");

            if (string.IsNullOrWhiteSpace(searchRequest.FirstName))
                throw new ArgumentNullException($"The {nameof(searchRequest.FirstName)} cannot be null");

            if (!Regex.IsMatch(searchRequest.FirstName, "^([a-zA-Z\\s]+)$"))
                throw new ArgumentException($"Invalid {nameof(searchRequest.FirstName)}");

            if (string.IsNullOrWhiteSpace(searchRequest.LastName))
                throw new ArgumentNullException($"The {nameof(searchRequest.LastName)} cannot be null");

            if (!Regex.IsMatch(searchRequest.LastName, "^([a-zA-Z\\s]+)$"))
                throw new ArgumentException($"Invalid {nameof(searchRequest.LastName)}");

            LendFoundry.Syndication.LexisNexis.Bankruptcy.SearchBankruptcyRequest objRequest = new LendFoundry.Syndication.LexisNexis.Bankruptcy.SearchBankruptcyRequest();

            objRequest.FirstName = searchRequest.FirstName;
            objRequest.LastName = searchRequest.LastName;
            objRequest.Ssn = searchRequest.Ssn;
            if (ApplicationProcessorConfiguration.BankruptcyFcraState == null || ApplicationProcessorConfiguration.BankruptcyFcraState.Contains(searchRequest.State))
            {
                objRequest.AddressLine1 = searchRequest.StreetAddress1;
                objRequest.City = searchRequest.City;
                objRequest.Zip5 = searchRequest.Zip5;
                objRequest.State = searchRequest.State;
            }
            var objSearchResponse = await LexisNexisService.FcraBankruptcySearch(EntityType, entityId, objRequest);
            if (objSearchResponse?.Records?.Count == 1)
            {
                var ReportUniqueId = objSearchResponse.Records.FirstOrDefault().Debtors.FirstOrDefault().UniqueId;
                var ReportTMSId = objSearchResponse.Records.FirstOrDefault().TMSId;
                if (!string.IsNullOrWhiteSpace(ReportUniqueId) && !string.IsNullOrWhiteSpace(ReportTMSId))
                {
                    Request.IBankruptcyReportRequest objReportRequest = new Request.BankruptcyReportRequest()
                    {
                        UniqueId = ReportUniqueId,
                        TMSId = ReportTMSId,
                        OwnerId = searchRequest.OwnerId
                    };
                    var reportResponse = await BankruptcyReport(entityId, objReportRequest);
                }
            }
            if (objSearchResponse.RecordCount == 0)
            {
                var bankruptcyReportRecords = new
                {
                    Recordcount = 0,
                    referenceNumber = Guid.NewGuid().ToString("N")
                };
                if (string.IsNullOrEmpty(searchRequest.OwnerId))
                {
                    searchRequest.OwnerId = await GetPrimaryOwnerId(entityId);
                }
                await DataAttributeService.SetAttribute(EntityType, entityId, "bankruptcyReportRecords", bankruptcyReportRecords, searchRequest.OwnerId);
            }
            return objSearchResponse;
        }

        #endregion Bankruptcy Search

        #region Bankruptcy Report

        public async Task<IBankruptcyReportResponse> BankruptcyReport(string entityId, Request.IBankruptcyReportRequest reportRequest)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (reportRequest == null)
                throw new ArgumentNullException(nameof(reportRequest));
            // if (string.IsNullOrWhiteSpace (reportRequest.TMSId))
            //     throw new ArgumentNullException (nameof (reportRequest.TMSId));
            if (string.IsNullOrWhiteSpace(reportRequest.UniqueId))
                throw new ArgumentNullException(nameof(reportRequest.UniqueId));

            LendFoundry.Syndication.LexisNexis.Bankruptcy.BankruptcyReportRequest objRequest = new LendFoundry.Syndication.LexisNexis.Bankruptcy.BankruptcyReportRequest();

            objRequest.UniqueId = reportRequest.UniqueId;
            // objRequest.TMSId = reportRequest.TMSId;
            if (string.IsNullOrEmpty(reportRequest.OwnerId))
            {
                objRequest.OwnerId = await GetPrimaryOwnerId(entityId);
            }
            else
            {
                objRequest.OwnerId = reportRequest.OwnerId;
            }
            var objReportResponse = await LexisNexisService.BankruptcyReport(EntityType, entityId, objRequest);

            return objReportResponse;
        }

        #endregion Bankruptcy Report

        #region Criminal Search

        public async Task<ICriminalRecordSearchResponse> CriminalRecordSearch(string entityId, ICriminalRecordSearchRequest searchRequest)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            if (string.IsNullOrWhiteSpace(searchRequest.FirstName))
                throw new ArgumentNullException(nameof(searchRequest.FirstName));
            if (string.IsNullOrWhiteSpace(searchRequest.LastName))
                throw new ArgumentNullException(nameof(searchRequest.LastName));
            if (string.IsNullOrWhiteSpace(searchRequest.Ssn))
                throw new ArgumentNullException(nameof(searchRequest.Ssn));
            if (!Regex.IsMatch(searchRequest.Ssn, "^([0-9]{3}-[0-9]{2}-[0-9]{4}|[0-9]+)$"))
                throw new ArgumentException("Invalid Ssn Number format");
            if (!Regex.IsMatch(searchRequest.FirstName, "^([a-zA-Z\\s]+)$"))
                throw new ArgumentException($"Invalid {nameof(searchRequest.FirstName)}");
            if (!Regex.IsMatch(searchRequest.LastName, "^([a-zA-Z\\s]+)$"))
                throw new ArgumentException($"Invalid {nameof(searchRequest.LastName)}");

            LendFoundry.Syndication.LexisNexis.CriminalRecord.CriminalRecordSearchRequest objRequest = new LendFoundry.Syndication.LexisNexis.CriminalRecord.CriminalRecordSearchRequest();
            objRequest.FirstName = searchRequest.FirstName;
            objRequest.LastName = searchRequest.LastName;
            objRequest.Ssn = searchRequest.Ssn;
            if (ApplicationProcessorConfiguration.BankruptcyFcraState == null || ApplicationProcessorConfiguration.BankruptcyFcraState.Contains(searchRequest.State))
            {
                objRequest.StreetAddress1 = searchRequest.StreetAddress1;
                objRequest.City = searchRequest.City;
                objRequest.Zip5 = searchRequest.Zip5;
                objRequest.State = searchRequest.State;
            }
            var objSearchResponse = await LexisNexisService.CriminalSearch(EntityType, entityId, objRequest);
            if (objSearchResponse?.Records?.Count == 1)
            {
                var ReportRecords = objSearchResponse.Records.FirstOrDefault();
                //var ReportQueryId = objSearchResponse.Header?.TransactionId;
                if (ReportRecords != null)
                {
                    Request.ICriminalRecordReportRequest objReportRequest = new Request.CriminalRecordReportRequest()
                    {
                        UniqueId = ReportRecords.UniqueId,
                        //QueryId = ReportQueryId,
                        OwnerId = searchRequest.OwnerId
                    };
                    var reportResponse = await CriminalRecordReport(entityId, objReportRequest);
                }

            }
            if (objSearchResponse.RecordCount == 0)
            {
                var criminalRecords = new
                {
                    Recordcount = 0,
                    referenceNumber = Guid.NewGuid().ToString("N")
                };
                if (string.IsNullOrEmpty(searchRequest.OwnerId))
                {
                    searchRequest.OwnerId = await GetPrimaryOwnerId(entityId);
                }
                await DataAttributeService.SetAttribute(EntityType, entityId, "criminalRecords", criminalRecords, searchRequest.OwnerId);
            }
            return objSearchResponse;
        }

        #endregion Criminal Search

        #region CriminalRecord Report

        public async Task<ICriminalReportResponse> CriminalRecordReport(string entityId, ICriminalRecordReportRequest reportRequest)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (reportRequest == null)
                throw new ArgumentNullException(nameof(reportRequest));
            // if (string.IsNullOrWhiteSpace (reportRequest.QueryId))
            //     throw new ArgumentNullException (nameof (reportRequest.QueryId));
            if (string.IsNullOrWhiteSpace(reportRequest.UniqueId))
                throw new ArgumentNullException(nameof(reportRequest.UniqueId));

            LendFoundry.Syndication.LexisNexis.CriminalRecord.CriminalRecordReportRequest objRequest = new LendFoundry.Syndication.LexisNexis.CriminalRecord.CriminalRecordReportRequest();
            objRequest.UniqueId = reportRequest.UniqueId;
            // objRequest.QueryId = reportRequest.QueryId;
            if (string.IsNullOrEmpty(reportRequest.OwnerId))
            {
                objRequest.OwnerId = await GetPrimaryOwnerId(entityId);
            }
            else
            {
                objRequest.OwnerId = reportRequest.OwnerId;
            }
            var objReportResponse = await LexisNexisService.CriminalRecordReport(EntityType, entityId, objRequest);

            return objReportResponse;
        }
        #endregion CriminalRecord Report

        #endregion LexisNexis

        #region fund application
        public async Task AddFundingRequest(string entityid)
        {
            if (string.IsNullOrWhiteSpace(entityid))
                throw new ArgumentNullException(nameof(entityid));

            var application = await BusinessApplicationService.GetByApplicationNumber(entityid);

            if (application == null)
                throw new InvalidOperationException("Application not found");

            var objPrimaryOwner = await BusinessApplicantService.GetPrimaryOwner(application.ApplicantId);

            if (objPrimaryOwner == null)
                throw new InvalidOperationException("No primary owner found");

            var dealData = await OfferEngineService.GetApplicationOffers(EntityType, entityid);

            IFundingRequest request = new FundingRequest();
            request.AmountFunded = Convert.ToDouble(dealData.DealOffer.AmountFunded);
            request.BankAccountNumber = dealData.DealOffer.AccountNumber;
            request.BankAccountType = dealData.DealOffer.AccountType;
            request.BankRTN = dealData.DealOffer.RoutingNumber;
            request.BorrowersName = $"{objPrimaryOwner.FirstName} {objPrimaryOwner.LastName}";
            request.RequestedDate = DateTime.UtcNow;

            await FundingService.AddFundingRequest(EntityType, entityid, request);

            var objActiveStatus = await GetActiveStatusWorkFlow(EntityType, entityid);
            EntityStatusService.ChangeStatus(EntityType, entityid, objActiveStatus.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["Approved"], null);
        }
        #endregion

        #region Listener Methods
        private string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = LookupService.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }

        public async Task<bool> SignDocumentReceivedHandler(string entityType, string entityId, object data)
        {
            var result = string.Empty;
            entityType = EnsureEntityType(entityType);
            if (String.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (data == null)
                throw new ArgumentNullException(nameof(data));
            var application = await BusinessApplicationService.GetByApplicationId(entityId);
            if (application == null)
                throw new ArgumentNullException($"Application for {nameof(entityId)} {entityId} not exist");
            var objDocuSignData = ExecuteRequest<DocuSignData>(data.ToString());

            var refNumber = Guid.NewGuid().ToString("N");
            await EventHub.Publish(new Events.LoanAgreementSigned
            {
                EntityId = entityId,
                EntityType = EntityType,
                Response = "LoanAgreementSignedResponse",
                Request = "LoanAgreementSignedRequest",
                ReferenceNumber = refNumber
            });
            if (objDocuSignData.DocumentContent != null)
            {
                var objActiveStatus = await GetActiveStatusWorkFlow(EntityType, entityId);
                await VerificationEngineService.AddDocument(EntityType, entityId, objActiveStatus.StatusWorkFlowId, "SignedDocumentVerification", "SignedDocumentVerificationManual", objDocuSignData.DocumentContent, objDocuSignData.DocumentName);
            }
            return true;
        }

        public async Task FundingStatusHandler(string entityType, string entityId, object data)
        {
            entityType = EnsureEntityType(entityType);
            if (String.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (data == null)
                throw new ArgumentNullException(nameof(data));

            var application = await BusinessApplicationService.GetByApplicationId(entityId);
            if (application == null)
                throw new ArgumentNullException($"Application for {nameof(entityId)} {entityId} not exist");

            var objFundingData = ExecuteRequest<ApplicationFunding>(data.ToString());
            if (objFundingData != null && objFundingData.RequestStatus.ToLower() == "confirmed")
            {
                var objActiveStatus = await GetActiveStatusWorkFlow(EntityType, entityId);
                EntityStatusService.ChangeStatus(EntityType, entityId, objActiveStatus.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["Funded"], null);
                await EventHub.Publish("EventFundingEmail", new
                {
                    EntityId = application.ApplicationNumber,
                    EntityType = EntityType,
                    Response = "EventFundingEmail",
                    Request = "EventFundingEmail",
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
            }
        }
        #endregion

        public async Task<bool> BanKLinkRequestedHandler(string entityType, string entityId, object data)
        {
            var issuedToken = await GenerateToken(entityType, entityId, 0);
            await EventHub.Publish(new BankLinkRequest
            {
                EntityId = entityId,
                EntityType = EntityType,
                Response = "",
                Request = issuedToken.Value,
                ReferenceNumber = Guid.NewGuid().ToString("N")
            });

            return true;
        }

        public async Task<bool> ExperianBusinessCreditReporHandler(string entityType, string entityId, object data)
        {
            if (data == null)
                throw new ArgumentNullException(nameof(data));
            if (ApplicationProcessorConfiguration.BusinessCreditReport)
            {
                string htmlData = RenderXMLData(data.ToString());

                htmlData = htmlData.Replace("../", ApplicationProcessorConfiguration.ExperianXsltPath);
                object htmlString = new { htmlString = htmlData };
                var document = new LendFoundry.DocumentGenerator.Document
                {
                    Data = htmlString,
                    Format = DocumentFormat.Pdf,
                    Name = "BusinessCreditReport.pdf",
                    Version = "1.0"
                };
                var documentResult = await DocumentGeneratorService.Generate("BusinessCreditReport", "1.0", Format.Html, document);
                if (documentResult != null)
                {
                    await ApplicationDocumentService.Add(entityId, ApplicationProcessorConfiguration.DocumentCategory["BusinessCreditReport"], documentResult.Content, document.Name, null, null);
                }
            }
            return true;
        }

        public async Task UpdateSICCode(string entityId, string sicCode)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException($"{nameof(entityId)} can not be null");
            if (string.IsNullOrWhiteSpace(sicCode))
                throw new ArgumentNullException($"{nameof(sicCode)} can not be null");

            var application = await BusinessApplicationService.GetByApplicationId(entityId);

            if (application == null)
                throw new ArgumentNullException($"Application not exist for id{entityId}");
            IDictionary<string, object> fieldList = new Dictionary<string, object>();
            fieldList.Add("SICCode", sicCode);
            await BusinessApplicantService.UpdateFields(application.ApplicantId, fieldList);
        }
        public async Task<object> GetApplicationDocuments(string entityId, string documentName)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrWhiteSpace(documentName))
                throw new ArgumentNullException(nameof(documentName));
            var applicationDocuments = await ApplicationDocumentService.GetByApplicationNumber(entityId);
            if (applicationDocuments == null)
                throw new InvalidOperationException("Application documents are not found");
            var documentByName = applicationDocuments.Where(d => d.FileName == documentName).Select(d => d);
            return documentByName;
        }

        #region GenerateToken

        private async Task<ITokenGenerate> GenerateToken(string entityType, string entityId, int expirationDays)
        {
            TokenGenerate token = new TokenGenerate();
            try
            {
                token.Tenant = Environment.GetEnvironmentVariable("TenantId");
                token.EntityId = entityId;
                token.EntityType = entityType;
                token.ExpirationDays = expirationDays;
                return await TokenGeneratorService.Issue(token);
            }
            catch (System.Exception ex)
            {
                return token;
            }

        }
        #endregion

        public async Task CashflowAccountSelectedHandler(string entityType, string entityId, object data)
        {
            if (String.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (data == null)
                throw new ArgumentNullException(nameof(data));

            JObject payload = (JObject)data;

            object plaidSelectedAccountCashflowData = null;

            FaultRetry.RunWithAlwaysRetry(() => {
                plaidSelectedAccountCashflowData = DataAttributeService.GetAttribute(EntityType, entityId, "plaidCashflow", payload["dataAttributeName"].ToString()).Result;
                if (plaidSelectedAccountCashflowData == null)
                    throw new InvalidOperationException("cash flow report not found " + payload["dataAttributeName"].ToString());
               
            });
            if (plaidSelectedAccountCashflowData == null) {
                throw new InvalidOperationException("cash flow report not found for " + payload["dataAttributeName"].ToString());
           
            } else {
                #region Verify Cashflow Fact
                var dataToVerify = new { SelectedAccountData = plaidSelectedAccountCashflowData };   
                 var objActiveStatus = await GetActiveStatusWorkFlow(entityType, entityId);
                await VerificationEngineService.Verify(entityType, entityId, objActiveStatus.StatusWorkFlowId, "CashflowVerification", "CashflowVerification", "CashflowVerification", dataToVerify);

                #endregion
            }
        }

        #region cashflow
        public async Task<ICashFlowResponse> GetcashflowAccountDetails(string entityId)
        {
            ICashFlowResponse accounts = new CashFlowResponse();
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            try
            {
                Logger.Info("Started GetcashflowAccountDetails at:" + TenantTime.Now + " EntityId:" + entityId);
                var cashFlowAccountData = new AccountTypeRequest();
                cashFlowAccountData.IsCashflowAccount = true;
                accounts = await AccountDetails(entityId, cashFlowAccountData);
            }
            catch (System.Exception ex)
            {
                Logger.Error("Error While Processing GetcashflowAccountDetails at:" + TenantTime.Now + " EntityId:" + entityId + "Exception" + ex.Message + ex.StackTrace);
            }
            Logger.Info("Ended GetcashflowAccountDetails at:" + TenantTime.Now + " EntityId:" + entityId);
            return accounts;
        }
        #endregion

        #region Funding
        public async Task<ICashFlowResponse> GetFundingAccountDetails(string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            var fundingAccountData = new AccountTypeRequest();
            fundingAccountData.IsFundingAccount = true;
            return await AccountDetails(entityId, fundingAccountData);
        }
        #endregion

        #region AccountDetails

        private async Task<ICashFlowResponse> AccountDetails(string entityId, IAccountTypeRequest accountType) {
            ICashFlowResponse accountDetail = new CashFlowResponse();

            try {
                Logger.Info("Started AccountDetails at:" + TenantTime.Now + " EntityId:" + entityId);
                var SelectedAccount = await CashflowService.GetAccountByType(EntityType, entityId, accountType);
                if (SelectedAccount != null && SelectedAccount.accounts != null && SelectedAccount.accounts.Count > 0) {
                    accountDetail.AccountNumber = SelectedAccount.accounts[0].AccountNumber;
                    accountDetail.InstituteName = SelectedAccount.accounts[0].BankName;
                    accountDetail.AccountName = SelectedAccount.accounts[0].NameOnAccount;
                    accountDetail.ProviderAccountId = SelectedAccount.accounts[0].ProviderAccountId;
                    accountDetail.Type = SelectedAccount.accounts[0].AccountType;
                }
                else
                {
                    Logger.Info("Account not selected/not fetched yet at:" + TenantTime.Now + " EntityId:" + entityId);
                    return null;
                }
            }
            catch (System.Exception ex)
            {
                Logger.Error("Error While Processing AccountDetails at:" + TenantTime.Now + " EntityId:" + entityId + "Exception" + ex.Message + ex.StackTrace);
            }
            Logger.Info("Ended AccountDetails at:" + TenantTime.Now + " EntityId:" + entityId);
            return accountDetail;
        }
        #endregion

        #region Get Owners
        public async Task<List<IOwner>> GetOwners(string entityId)
        {
            return await GetOwners(entityId, BusinessApplicationService, BusinessApplicantService);
        }
        #endregion

        private async Task<List<IOwner>> GetOwners(string entityId, IApplicationService businessApplicationService, IApplicantService businessApplicantService)
        {
            if (String.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            var application = await businessApplicationService.GetByApplicationNumber(entityId);
            if (application == null)
                throw new InvalidOperationException("Application not found");

            var applicant = await businessApplicantService.Get(application.ApplicantId);
            if (applicant != null && applicant.Owners != null && applicant.Owners.Count > 0)
            {
                return applicant.Owners.ToList();
            }
            return null;
        }

        private async Task PricingStrategyPassHandling(List<string> reCallSyndication, string statusWorkFlowId, IApplication application, IApplicant applicant,
            IYelpService yelpService, IBBBSearchService bbbSearchService,
            IApplicationDocumentService applicationDocumentService,
            IDocumentGeneratorService documentGeneratorService,
            ApplicationProcessorConfiguration applicationProcessorConfiguration,
            IEntityStatusService entityStatusService)
        {
            entityStatusService.ChangeStatus(EntityType, application.ApplicationNumber, statusWorkFlowId, applicationProcessorConfiguration.Statuses["ApplicationSubmitted"], null);
            var primaryAddress = application.PrimaryAddress;
            foreach (var syndication in reCallSyndication)
            {
                if (syndication == YELPREPORT)
                {
                    Yelp(application.ApplicationNumber, application.PrimaryPhone.Phone, applicant.LegalBusinessName, application.PrimaryAddress.AddressLine1, primaryAddress.City, primaryAddress.State, primaryAddress.Country, YelpService);
                }
                else if (syndication == BBBREPORT)
                {
                    bbbsearch(application.ApplicationNumber, applicant.LegalBusinessName, application.PrimaryPhone.Phone, application.PrimaryAddress.ZipCode, bbbSearchService);
                }
            }
            await EventHub.Publish(new EventWelcomeEmail
            {
                EntityId = application.ApplicationNumber,
                EntityType = EntityType,
                Response = "EventWelcomeEmail",
                Request = "EventWelcomeEmail",
                ReferenceNumber = Guid.NewGuid().ToString("N")
            });
        }

        private async Task DeclineBasedOnPScore(string entityId, IProductRuleResult pscoreResult)
        {
            List<string> reasons = new List<string>();
            if (pscoreResult?.IntermediateData != null)
            {
                JObject intermediateData = JObject.Parse(pscoreResult.IntermediateData.ToString());
                if (Convert.ToBoolean(intermediateData["scoreResult"]) == false)
                {
                    reasons.Add("LowCreditScore");
                }
                if (Convert.ToBoolean(intermediateData["industryResult"]) == false)
                {
                    reasons.Add("IndustryNotSupported");
                }
                if (Convert.ToBoolean(intermediateData["businessRevenueResult"]) == false)
                {
                    reasons.Add("BusinessRevenueLess");
                }
                if (Convert.ToBoolean(intermediateData["timeInBusinessResult"]) == false)
                {
                    reasons.Add("TimeInBusinessLess");
                }
            }
            else
            {
                reasons.Add("CADecline");
            }
            RequestModel objRequest = new RequestModel();
            objRequest.reasons = reasons;

            var objActiveStatus = await GetActiveStatusWorkFlow(EntityType, entityId);
            EntityStatusService.ChangeStatus(EntityType, entityId, objActiveStatus.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["Declined"], objRequest);
        }

        private async Task<JArray> GetExperianPersonalDataAttribute(string entityId, string ownerId)
        {
            JArray personalReport = null;

            try
            {
                FaultRetry.RunWithAlwaysRetry(() =>
                {
                    personalReport = (JArray)DataAttributeService.GetAttribute(EntityType, entityId, "experianPersonalReport", ownerId).Result;
                    if (personalReport == null || personalReport.Count <= 0)
                        throw new InvalidOperationException("experianPersonalReport not found " + ownerId);
                });
            }
            catch { }

            return personalReport;
        }

        private async Task<bool> GetExperianPersonalReport(string entityId, string ownerId, IApplicant objApplicant)
        {
            bool result = false;
            try
            {
                JArray personalReport = null;
                personalReport = await GetExperianPersonalDataAttribute(entityId, ownerId);
                if (personalReport == null || personalReport.Count <= 0)
                {
                    if (objApplicant != null && objApplicant.Owners != null && objApplicant.Owners.Count > 0)
                    {
                        var owner = objApplicant.Owners.FirstOrDefault(item => item.OwnerId == ownerId);
                        await CallExperianPersonalSyndication(entityId, owner.FirstName, owner.LastName, owner.SSN,
                            owner.Addresses[0].AddressLine1, owner.Addresses[0].City,
                            owner.Addresses[0].State, owner.Addresses[0].ZipCode, owner.OwnerId, PersonalReportService);

                        personalReport = await GetExperianPersonalDataAttribute(entityId, ownerId);
                        if (personalReport == null)
                        {
                            throw new InvalidOperationException("experianPersonalReport not found for " + ownerId);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                }
                else
                {
                    result = true;
                }
            }
            catch (System.Exception ex)
            {
                string s = ex.Message;
            }

            return result;
        }

        public async Task SwitchOwner(string entityId, string ownerId)
        {
            if (String.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrEmpty(ownerId))
                throw new ArgumentNullException(nameof(ownerId));

            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);
            if (application == null)
                throw new InvalidOperationException("Application not found");

            var objApplicant = await BusinessApplicantService.Get(application.ApplicantId);

            IOwner objCurrentOwner = new LendFoundry.Business.Applicant.Owner();
            if (objApplicant != null && objApplicant.Owners != null && objApplicant.Owners.Count > 0)
            {
                objCurrentOwner = objApplicant.Owners.FirstOrDefault(item => item.IsPrimary == true);
            }
            //var objCurrentOwner = await BusinessApplicantService.GetPrimaryOwner(application.ApplicantId);

            var result = await GetExperianPersonalReport(entityId, ownerId, objApplicant);
            if (result == false)
            {
                throw new InvalidOperationException("experianPersonalReport not found for " + ownerId);
            }
            else
            {
                var objActiveStatus = await GetActiveStatusWorkFlow(EntityType, entityId);

                IResetFactRequest objResetFact = new ResetFactRequest();
                objResetFact.FactNames = new List<string>();
                if (ApplicationProcessorConfiguration.ResetFacts != null && ApplicationProcessorConfiguration.ResetFacts.Count > 0)
                {
                    foreach (var fact in ApplicationProcessorConfiguration.ResetFacts)
                    {
                        objResetFact.FactNames.Add(fact);
                    }
                }
                await VerificationEngineService.ReSetVerification(EntityType, entityId, objResetFact);

                await BusinessApplicationService.SetPrimary(entityId, ownerId);

                await EventHub.Publish("ApplicationPDFRequested", new
                {
                    EntityId = application.ApplicationNumber,
                    EntityType = EntityType,
                    Response = "ApplicationPDFRequested",
                    Request = "ApplicationPDFRequested",
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });

                var objSwitchedOwner = await BusinessApplicantService.GetPrimaryOwner(application.ApplicantId);
                if (objSwitchedOwner != null)
                {
                    IProductRuleResult pscoreResult = new ProductRuleResult();
                    pscoreResult = await ProductRuleService.RunPricingStrategy(EntityType, entityId, objActiveStatus.ProductId, "Eligibility", "1.0", objSwitchedOwner.OwnerId);
                    if (pscoreResult == null)
                        throw new InvalidOperationException("Unable to Strategy: Eligibility");
                    var applicant = await BusinessApplicantService.Get(application.ApplicantId);

                    if (pscoreResult?.Result == Result.Passed)
                    {
                        //check if all 4 syndication data available
                        List<string> strSyndications = new List<string>();
                        List<string> reCallSyndication = new List<string>();
                        strSyndications.Add(YELPREPORT);
                        
                        strSyndications.Add(BBBREPORT);
                        var syndicationDataAttributes = await DataAttributeService.GetAttribute(EntityType, entityId, strSyndications);
                        if (syndicationDataAttributes != null)
                        {
                            //re-call syndication if not called before
                            foreach (var syndication in strSyndications)
                            {
                                if (!syndicationDataAttributes.ContainsKey(syndication))
                                {
                                    reCallSyndication.Add(syndication);
                                }
                            }
                        }
                        if (reCallSyndication.Count > 0)
                        {
                            await PricingStrategyPassHandling(reCallSyndication, objActiveStatus.StatusWorkFlowId, application, applicant, YelpService, BBBSearchService, 
                                ApplicationDocumentService, DocumentGeneratorService, ApplicationProcessorConfiguration, EntityStatusService);
                        }

                        //Delete previously generated offers and deal                           
                        await OfferEngineService.DeleteOffersAndDeal(EntityType, entityId);
                        EntityStatusService.ChangeStatus(EntityType, application.ApplicationNumber, objActiveStatus.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["Verification"], null);

                        //verify bankRuptcy if data available
                        object objBankRuptcy = null;
                        objBankRuptcy = DataAttributeService.GetAttribute(EntityType, entityId, "bankruptcyReportRecords", objSwitchedOwner.OwnerId).Result;
                        if (objBankRuptcy != null)
                        {
                            await VerificationEngineService.Verify(EntityType, entityId, objActiveStatus.StatusWorkFlowId, "BankruptcyVerification", "BankruptcyVerification", "BankruptcyVerification", objBankRuptcy);
                        }
                        await EventHub.Publish("SwitchOwnerCompleted", new
                        {
                            EntityId = application.ApplicationNumber,
                            EntityType = EntityType,
                            Response = objSwitchedOwner,
                            Request = objCurrentOwner,
                            ReferenceNumber = Guid.NewGuid().ToString("N")
                        });
                    }
                    else
                    {
                        await DeclineBasedOnPScore(entityId, pscoreResult);
                    }
                }
                else
                {
                    throw new InvalidOperationException("No primary owner found");
                }
            }
        }

        public async Task ApplicationPDFHandler(string entityType, string entityId, object data)
        {
            if (String.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);
            if (application == null)
                throw new InvalidOperationException("Application not found");

            var applicant = await BusinessApplicantService.Get(application.ApplicantId);
            if (applicant == null)
                throw new InvalidOperationException("Applicant not found");

            var applicationData = new ApplicationDetailPDF(application, applicant);

            applicationData.LoanTimeFrame = LookupService.GetLookupEntry("loanTimeFrame", application.LoanTimeFrame).FirstOrDefault().Value;
            applicationData.PurposeOfLoan = LookupService.GetLookupEntry("PurposeOfLoan", application.PurposeOfLoan).FirstOrDefault().Value;
            applicationData.BusinessType = LookupService.GetLookupEntry("businessTypes", applicant.BusinessType).FirstOrDefault().Value;
            applicationData.Industry = LookupService.GetLookupEntry("industry", applicant.Industry).FirstOrDefault().Value;
            applicationData.PropertyType = LookupService.GetLookupEntry("propertyType", application.PropertyType).FirstOrDefault().Value;
            applicationData.Owners = applicant.Owners.OrderByDescending(i => i.IsPrimary).ToList();
            applicationData.RequestedAmount = string.Format(CultureInfo.GetCultureInfo("en-US"), "{0:C0}", application.RequestedAmount);
            applicationData.AverageBankBalances = FormatMoney(application.SelfDeclareInformation.AverageBankBalance);
            applicationData.AnnualRevenue = FormatMoney(application.SelfDeclareInformation.AnnualRevenue);
            applicationData.BusinessTaxID = FormatTaxId(applicationData.BusinessTaxID);
            applicationData.Phone = FormatPhoneNumber(application.PrimaryPhone.Phone);
            foreach (var owner in applicationData.Owners)
            {
                owner.SSN = FormatSSNNumber(owner.SSN);
                foreach (var phoneNumber in owner.PhoneNumbers)
                {
                    phoneNumber.Phone = FormatPhoneNumber(phoneNumber.Phone);
                }
            }

            var document = new LendFoundry.DocumentGenerator.Document
            {
                Data = applicationData,
                Format = DocumentFormat.Pdf,
                Name = "Application.pdf",
                Version = "1.0"
            };
            var documentResult = await DocumentGeneratorService.Generate("ApplicationDetails", "1.0", Format.Html, document);
            if (documentResult != null)
            {
                await ApplicationDocumentService.Add(entityId, ApplicationProcessorConfiguration.DocumentCategory["Application"], documentResult.Content, document.Name, null, null);
            }
        }

        #region borrower reapply
        public async Task<bool> CheckReApplyEligibility(string entityId)
        {
            if (string.IsNullOrEmpty(entityId))
                throw new ArgumentNullException(nameof(entityId));
            bool reApplyEnabled = false;
            var objActiveStatus = await GetActiveStatusWorkFlow(EntityType, entityId);
            var currentStatus = await EntityStatusService.GetStatusByEntity(EntityType, entityId, objActiveStatus.StatusWorkFlowId);
            var eligibilityResult = await ProductRuleService.RunRule(EntityType, entityId, objActiveStatus.ProductId, "ReApplyEligibilityCheck", new { CurrentStatus = currentStatus });
            if (eligibilityResult != null && eligibilityResult.Result == Result.Passed)
            {
                reApplyEnabled = true;
            }
            return reApplyEnabled;
        }
        public async Task<bool> CheckRevivalEligibility(string entityId)
        {
            if (string.IsNullOrEmpty(entityId))
                throw new ArgumentNullException(nameof(entityId));
            bool reApplyEnabled = false;
            var objActiveStatus = await EntityStatusService.GetActiveStatusWorkFlow(EntityType, entityId);
            var currentStatus = await EntityStatusService.GetStatusByEntity(EntityType, entityId, objActiveStatus.StatusWorkFlowId);
            var eligibilityResult = await ProductRuleService.RunRule(EntityType, entityId, objActiveStatus.ProductId, "RevivalEligibilityCheck", new { CurrentStatus = currentStatus });
            if (eligibilityResult != null && eligibilityResult.Result == Result.Passed)
            {
                reApplyEnabled = true;
            }
            return reApplyEnabled;
        }
        public async Task ReviveApplication(string entityId)
        {
            if (string.IsNullOrEmpty(entityId))
                throw new ArgumentNullException(nameof(entityId));
            bool isEligibleToRevive = await CheckRevivalEligibility(entityId);
            if (!isEligibleToRevive)
                throw new InvalidOperationException("Application Is Not Eligible To Revive");
            var statusTransactionHistory = await EntityStatusService.GetStatusTransitionHistory(EntityType, entityId);
            if (statusTransactionHistory != null && statusTransactionHistory.Count() > 0)
            {
                var objActiveStatus = await GetActiveStatusWorkFlow(EntityType, entityId);
                var currentStatus = await EntityStatusService.GetStatusByEntity(EntityType, entityId, objActiveStatus.StatusWorkFlowId);
                var lastStatus = statusTransactionHistory.Where(i => i.Status != currentStatus.Code && Convert.ToDecimal(i.Status) <
                   Convert.ToDecimal(currentStatus.Code))?.GroupBy(o => o.Id)?.Select(g => g.First())?.LastOrDefault();
                if (lastStatus != null && !string.IsNullOrWhiteSpace(lastStatus.Status))
                {
                    EntityStatusService.ChangeStatus(EntityType, entityId, objActiveStatus.StatusWorkFlowId, lastStatus.Status, null);
                    var revivalConfigForCurrentStatus = ApplicationProcessorConfiguration.RevivalConfigurations.StatusFactsMaping.Where(i => i.StatusCode == lastStatus.Status).FirstOrDefault();
                    if (revivalConfigForCurrentStatus != null && revivalConfigForCurrentStatus.AssociatedFacts?.Count > 0)
                    {
                        ResetFactRequest resetFacts = new ResetFactRequest
                        {
                            FactNames = revivalConfigForCurrentStatus.AssociatedFacts
                        };
                        VerificationEngineService.ReSetVerification(EntityType, entityId, resetFacts);
                    }
                    DataAttributeService.SetAttribute(EntityType, entityId, "LastRevived", new { LastReviveDate = new TimeBucket(TenantTime.Now.DateTime) });
                }
            }
        }
        public async Task EnableReapply(string entityId)
        {
            if (string.IsNullOrEmpty(entityId))
                throw new ArgumentNullException(nameof(entityId));
            DataAttributeService.SetAttribute(EntityType, entityId, "Reapply", new { ReapplyEnabled = true, ReapplyEnabledOn = new TimeBucket(TenantTime.Now) });
        }
        #endregion
        public async Task ExperainPersonalPDFHandler(string entityType, string entityId, object data)
        {
            if (String.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            string ownerId = (JsonConvert.DeserializeObject<dynamic>(data.ToString())).ownerId.Value;
            var application = await BusinessApplicationService.GetByApplicationNumber(entityId);
            if (application == null)
                throw new InvalidOperationException("Application not found");

            var applicant = await BusinessApplicantService.Get(application.ApplicantId);
            if (applicant == null)
                throw new InvalidOperationException("Applicant not found");
            var experianPersonalReport = GetExperianPersonalDataAttribute(entityId, ownerId).Result;

            if (experianPersonalReport != null)
            {
                string documentName = string.Format("CBR-{0}_XPN.pdf", applicant.Owners.Where(x => x.OwnerId == ownerId).Select(x => x.FirstName + "_" + x.LastName).ToArray()[0]);
                var experianPersonalPDFReport = (JsonConvert.DeserializeObject<dynamic>(experianPersonalReport.ToString()))[0].PDFReportAttributes;
                var document = new LendFoundry.DocumentGenerator.Document
                {
                    Data = experianPersonalPDFReport,
                    Format = DocumentFormat.Pdf,
                    Name = documentName,
                    Version = "1.0"
                };
                var documentResult = await DocumentGeneratorService.Generate("experianPersonalReport", "1.0", Format.Html, document);
                if (documentResult != null)
                {
                    await ApplicationDocumentService.Add(entityId, ApplicationProcessorConfiguration.DocumentCategory["PersonalCreditReport"], documentResult.Content, document.Name, null, null);
                }
            }

        }

        #region Application PreApproved Handler
        public async Task ApplicationPreApprovedHandler(string entityType, string entityId, object data)
        {
            if (String.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            var businessReport = await DataAttributeService.GetAttribute(entityType, entityId, "experianBusinessReport");
            if (businessReport == null)
            {
                var experianBusinessReport = GetExperianBusinessDefaultObject();
                await DataAttributeService.SetAttribute(entityType, entityId, "experianBusinessReport", experianBusinessReport, "experianBusinessReport");
            }
        }
        #endregion

        #region LMS

        public async Task LoanOnBoardHandler(string entityType, string entityId, object data)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            if (!ApplicationProcessorConfiguration.AutoOnBoardLoan)
                return;
            var objActiveStatus = await GetActiveStatusWorkFlow(EntityType, entityId);
            var statusDetail = await EntityStatusService.GetStatusByEntity(entityType, entityId, objActiveStatus.StatusWorkFlowId);
            if (statusDetail != null)
            {
                if (statusDetail.Code == ApplicationProcessorConfiguration.Statuses["Funded"])
                {
                    var application = await BusinessApplicationService.GetByApplicationNumber(entityId);
                    if (application == null)
                        throw new InvalidOperationException("Application not found");

                    var applicant = await BusinessApplicantService.Get(application.ApplicantId);
                    if (applicant == null)
                        throw new InvalidOperationException("Applicant not found");

                    var dealData = await OfferEngineService.GetApplicationOffers(entityType, entityId);
                    if (dealData == null || dealData.DealOffer == null)
                        throw new ArgumentNullException(nameof(dealData));

                    var loanInformationRequest = new LoanInformationRequest();
                    loanInformationRequest.AutoInitialize = false;
                    loanInformationRequest.LoanApplicationNumber = application.ApplicationNumber;
                    loanInformationRequest.LoanProductId = ApplicationProcessorConfiguration.DefaultProduct;
                    loanInformationRequest.PortFolioId = "Test";
                    loanInformationRequest.ProductCategory = LMS.LoanManagement.Abstractions.ProductCategory.Business;
                    loanInformationRequest.ProductPortfolioType = ProductPortfolioType.MCA;

                    if (applicant.Owners != null)
                    {
                        loanInformationRequest.ApplicantRequest = new List<LMS.LoanManagement.Abstractions.IApplicantRequest>();
                        foreach (var owner in applicant.Owners)
                        {
                            var applicantRequest = new LMS.LoanManagement.Abstractions.ApplicantRequest();
                            applicantRequest.IsPrimary = owner.IsPrimary != null ? (bool)owner.IsPrimary : false;

                            applicantRequest.Name = new LMS.LoanManagement.Abstractions.Name();
                            applicantRequest.Name.First = owner.FirstName;
                            applicantRequest.Name.Last = owner.LastName;
                            applicantRequest.Name.Middle = ""; // Data not available
                            applicantRequest.Name.Generation = ""; // Data not available
                            applicantRequest.Name.Salutation = "Mr"; // Data not available

                            applicantRequest.Addresses = new List<LMS.LoanManagement.Abstractions.IAddress>();
                            if (owner.Addresses != null && owner.Addresses.Any())
                            {
                                foreach (var address in owner.Addresses)
                                {
                                    if (address == null)
                                        continue;

                                    var applicantAddress = new LMS.LoanManagement.Abstractions.Address();
                                    applicantAddress.AddressLine1 = address.AddressLine1;
                                    applicantAddress.AddressLine2 = address.AddressLine2;
                                    applicantAddress.AddressType = GetAddressType(address.AddressType.ToString());
                                    applicantAddress.City = address.City;
                                    applicantAddress.State = address.State;
                                    applicantAddress.Zip = address.ZipCode;
                                    applicantAddress.IsActive = address.IsDefault;
                                    applicantRequest.Addresses.Add(applicantAddress);
                                }
                            }
                            applicantRequest.Emails = new List<LMS.LoanManagement.Abstractions.IEmail>();
                            if (owner.EmailAddresses != null && owner.EmailAddresses.Any())
                            {
                                foreach (var email in owner.EmailAddresses)
                                {
                                    if (email == null)
                                        continue;

                                    var applicantEmail = new LMS.LoanManagement.Abstractions.Email();
                                    applicantEmail.EmailAddress = email.Email;
                                    applicantEmail.EmailType = GetEmailType(email.EmailType.ToString());
                                    applicantEmail.IsActive = email.IsDefault;
                                    applicantRequest.Emails.Add(applicantEmail);
                                }
                            }
                            applicantRequest.Phones = new List<LMS.LoanManagement.Abstractions.IPhone>();
                            if (owner.PhoneNumbers != null && owner.PhoneNumbers.Any())
                            {
                                foreach (var phone in owner.PhoneNumbers)
                                {
                                    if (phone == null)
                                        continue;

                                    var applicantPhone = new LMS.LoanManagement.Abstractions.Phone();
                                    applicantPhone.PhoneNo = phone.Phone;
                                    applicantPhone.PhoneType = GetPhoneType(phone.PhoneType.ToString());
                                    applicantPhone.IsActive = phone.IsDefault;
                                    applicantRequest.Phones.Add(applicantPhone);
                                }
                            }

                            applicantRequest.PII = new PII();
                            applicantRequest.PII.DOB = owner.DOB != null ? Convert.ToDateTime(owner.DOB) : DateTime.MinValue;
                            applicantRequest.PII.SSN = owner.SSN;

                            applicantRequest.PositionOfLoan = owner.IsPrimary == true ? "Primary" : "Guarantors";
                            applicantRequest.Role = owner.Designation;
                            loanInformationRequest.ApplicantRequest.Add(applicantRequest);
                        }
                    }

                    loanInformationRequest.BusinessRequest = new List<IBusinessRequest>();
                    var businessRequest = new BusinessRequest();
                    businessRequest.BusinessName = applicant.LegalBusinessName;
                    businessRequest.DBA = applicant.DBA;
                    businessRequest.EstablishedDate = applicant.BusinessStartDate.Time;
                    businessRequest.FedTaxId = applicant.BusinessTaxID;
                    businessRequest.IsPrimary = true;

                    if (applicant.PrimaryAddress != null)
                    {
                        businessRequest.Address = new LMS.LoanManagement.Abstractions.Address();
                        businessRequest.Address.AddressLine1 = applicant.PrimaryAddress.AddressLine1;
                        businessRequest.Address.AddressLine2 = applicant.PrimaryAddress.AddressLine2;
                        businessRequest.Address.AddressType = GetAddressType(applicant.PrimaryAddress.AddressType.ToString());
                        businessRequest.Address.City = applicant.PrimaryAddress.City;
                        businessRequest.Address.State = applicant.PrimaryAddress.State;
                        businessRequest.Address.Zip = applicant.PrimaryAddress.ZipCode;
                        businessRequest.Address.IsActive = applicant.PrimaryAddress.IsDefault;
                    }
                    if (applicant.PrimaryEmail != null)
                    {
                        businessRequest.Email = new LMS.LoanManagement.Abstractions.Email();
                        businessRequest.Email.EmailAddress = applicant.PrimaryEmail.Email;
                        businessRequest.Email.EmailType = GetEmailType(applicant.PrimaryEmail.EmailType.ToString());
                        businessRequest.Email.IsActive = applicant.PrimaryEmail.IsDefault;
                    }
                    if (applicant.PrimaryPhone != null)
                    {
                        businessRequest.Phone = new LMS.LoanManagement.Abstractions.Phone();
                        businessRequest.Phone.PhoneNo = applicant.PrimaryPhone.Phone;
                        businessRequest.Phone.PhoneType = GetPhoneType(applicant.PrimaryPhone.PhoneType.ToString());
                        businessRequest.Phone.IsActive = applicant.PrimaryPhone.IsDefault;
                    }

                    loanInformationRequest.BusinessRequest.Add (businessRequest);

                    var fundingAccounts = await CashflowService.GetAccountByType(EntityType, entityId, new AccountTypeRequest { IsFundingAccount = true, IsCashflowAccount = false });
                    var fundingAccount = fundingAccounts != null && fundingAccounts.accounts != null ? fundingAccounts.accounts.FirstOrDefault() : null;
                    loanInformationRequest.PaymentInstruments = new List<IPaymentInstrumentRequest>();
                    if (fundingAccount != null) {
                        var paymentInstrument = new PaymentInstrumentRequest();

                        paymentInstrument.PaymentInstrumentType = PaymentInstrumentType.Ach;
                        paymentInstrument.IsActive = true;
                        paymentInstrument.BankDetails = new LMS.LoanManagement.Abstractions.BankDetails();
                        paymentInstrument.BankDetails.IsActive = true;
                        paymentInstrument.BankDetails.Name = fundingAccount.BankName;
                        paymentInstrument.BankDetails.AccountNumber = fundingAccount.AccountNumber;

                        paymentInstrument.BankDetails.AccountType = string.Equals(fundingAccount.AccountType, "Savings", StringComparison.InvariantCultureIgnoreCase) ? LMS.LoanManagement.Abstractions.AccountType.Savings : LMS.LoanManagement.Abstractions.AccountType.Checking;
                        paymentInstrument.BankDetails.BankHolderName = fundingAccount.NameOnAccount;
                        paymentInstrument.BankDetails.RoutingNumber = dealData.DealOffer.RoutingNumber;
                        paymentInstrument.BankDetails.IsVerified = true;
                        paymentInstrument.BankDetails.BankId = fundingAccount.Id;
                        loanInformationRequest.PaymentInstruments.Add(paymentInstrument);

                    }
                    loanInformationRequest.LoanStartDate = TenantTime.Now; // TODO: Not done
                    loanInformationRequest.LoanFundedDate = TenantTime.Now; // TODO: Not done
                    loanInformationRequest.LoanOnboardedDate = TenantTime.Now;

                    if (loanInformationRequest.ProductPortfolioType == ProductPortfolioType.MCA)
                    {
                        loanInformationRequest.LoanScheduleRequest = await LoanScheduleHandler(entityType, entityId, dealData);
                        loanInformationRequest.AutoInitialize = true;
                    }


                    var loanInformation = await LoanManagementService.OnBoard (loanInformationRequest);
                    //if (fundingAccount != null)
                    //await CashflowService.AddBankLink(fundingAccount.Id, "loan", loanInformation.LoanNumber);

                }
            }
        }

        private async Task<LoanScheduleRequest> LoanScheduleHandler(string entityType, string entityId, IApplicationOffer dealData)
        {
            var fundingData = await FundingService.GetFundingData(entityType, entityId);
            if (fundingData == null)
                throw new ArgumentNullException(nameof(fundingData));

            var fundingDetails = await FundingService.GetAttemptData(fundingData.ReferenceId);
            if (fundingDetails == null || !fundingDetails.Any())
                throw new ArgumentNullException(nameof(fundingDetails));

            var paymentTypes = LookupService.GetLookupEntry("paymentType", dealData.DealOffer.TypeOfPayment).FirstOrDefault();
            var paymentFrequency = paymentTypes.Value == "Weekly" ?
                PaymentFrequency.Weekly :
                PaymentFrequency.Daily;
            var loanScheduleRequest = new LoanScheduleRequest();
            loanScheduleRequest.FactorRate = dealData.DealOffer.SellRate;
            loanScheduleRequest.PaymentFrequency = paymentFrequency;
            loanScheduleRequest.FundedDate = fundingDetails.LastOrDefault().VendorResponseDate.Time;
            loanScheduleRequest.LoanAmount = dealData.DealOffer.ApprovedAmount;
            loanScheduleRequest.FundedAmount = Convert.ToDouble(dealData.DealOffer.AmountFunded);
            loanScheduleRequest.ScheduledCreatedReason = "tempreason";
            loanScheduleRequest.ScheduleNotes = "New schedule add";
            loanScheduleRequest.ScheduleVersionNo = "v1";
            loanScheduleRequest.Tenure = dealData.DealOffer.NumberOfPayment;
            if ((dealData.DealOffer.OriginatingFeeAmount + dealData.DealOffer.ACHFee) > 0)
            {
                loanScheduleRequest.Fees = new List<IFee>();
                var fee = new Fee();
                fee.Applied = true;
                fee.FeeAmount = (dealData.DealOffer.OriginatingFeeAmount + dealData.DealOffer.ACHFee);
                fee.FeeName = "LOAN ORIGINATION FEE";
                fee.FeeType = "preapplied";
                loanScheduleRequest.Fees.Add(fee);
            }
            return loanScheduleRequest;
        }

        private LMS.LoanManagement.Abstractions.EmailType GetEmailType(string emaiType)
        {
            switch (emaiType.ToLower())
            {
                case "personal":
                    return LMS.LoanManagement.Abstractions.EmailType.Personal;
                default:
                    return LMS.LoanManagement.Abstractions.EmailType.Work;
            }
        }

        private LMS.LoanManagement.Abstractions.AddressType GetAddressType(string addressType)
        {
            switch (addressType.ToLower())
            {
                case "home":
                    return LMS.LoanManagement.Abstractions.AddressType.Home;
                case "mailing":
                    return LMS.LoanManagement.Abstractions.AddressType.Mailing;
                case "business":
                    return LMS.LoanManagement.Abstractions.AddressType.Business;
                default:
                    return LMS.LoanManagement.Abstractions.AddressType.Military;
            }
        }

        private LMS.LoanManagement.Abstractions.PhoneType GetPhoneType(string phoneType)
        {
            switch (phoneType.ToLower())
            {
                case "residence":
                    return LMS.LoanManagement.Abstractions.PhoneType.Residence;
                case "mobile":
                    return LMS.LoanManagement.Abstractions.PhoneType.Mobile;
                case "work":
                    return LMS.LoanManagement.Abstractions.PhoneType.Work;
                default:
                    return LMS.LoanManagement.Abstractions.PhoneType.Alternate;
            }
        }

        #endregion

        #region Get Active Status WorkFlow
        private async Task<IStatusResponse> GetActiveStatusWorkFlow(string entityType, string entityId)
        {
            return await EntityStatusService.GetActiveStatusWorkFlow(entityType, entityId);
        }
        #endregion

        private async void ApplicationPDFRequestEvent(string entityId, string entityType)
        {
            await EventHub.Publish("ApplicationPDFRequested", new
            {
                EntityId = entityId,
                EntityType = entityType,
                Response = "ApplicationPDFRequested",
                Request = "ApplicationPDFRequested",
                ReferenceNumber = Guid.NewGuid().ToString("N")
            });
        }

        #region GetExtrctedData
        /// <summary>
        /// To get field value via product rule
        /// </summary>
        /// <param name="entityId">ApplicationNumber</param> 
        /// <param name="key">random Lookup value for rule name for extract field</param>
        /// <returns>returns list of fields outcome of rule </returns>
        public async Task<object> GetExtractedData(string entityId,string key)
        {
            string ruleName = null;
            var keyExists = LookupService.GetLookupEntry ("extractedDataKey", key);
            if (keyExists != null)  
            {
                try
                {
                    keyExists.TryGetValue(key,out ruleName);
                    var objActiveStatus = await GetActiveStatusWorkFlow("application", entityId);
                    var offerResult = await ProductRuleService.RunRule("application", entityId, objActiveStatus.ProductId, ruleName, null);
                    return offerResult.IntermediateData;
                }
                catch (System.Exception ex)
                {
                    //Logger.Error(ex.Message);
                    return null;
                }
            }
            return null;
        }
        #endregion

         #region Utility    
        public async Task AddAttribute(IAddAttributeRequest request)
        {
            string productId = string.Empty;
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            if (request.applicationNumber.Count <= 0)
                throw new ArgumentNullException("Application number request is null");
            productId = request.productId;
            var productData = await BusinessApplicationService.SaveProductAttribute(productId, null);

            var expandoDict = (JObject)productData;
            IProduct product = expandoDict.GetCustomProperty<LendFoundry.ProductConfiguration.Product>("product");

            foreach (var appNumber in request.applicationNumber)
            {
                await DataAttributeService.SetAttribute("application", appNumber, "product", (object)productData);
                try
                {
                    await BusinessApplicationService.InitiateWorkFlow(request.productId, appNumber, product);
                }
                catch (System.Exception e)
                {

                }
            }

        }
        #endregion

    }
    public static class ExtensionMethods
    {
        public static T GetCustomProperty<T>(this Newtonsoft.Json.Linq.JObject eo, string key)
        {
            object r = eo[key];
            return JsonConvert.DeserializeObject<T>(r.ToString());
        }
    }
}