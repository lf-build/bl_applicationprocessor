﻿using System;

namespace CapitalAlliance.ApplicationProcessor
{
    public interface IRecoverabilityPolicy
    {
        bool CanBeRecoveredFrom(Exception exception);
    }
}
