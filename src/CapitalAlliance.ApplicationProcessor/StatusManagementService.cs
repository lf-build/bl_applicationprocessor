﻿
using LendFoundry.StatusManagement;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CapitalAlliance.ApplicationProcessor
{
    public class StatusManagementService : IStatusManagementService
    {
        public StatusManagementService(IEntityStatusService entityStatusService, ILookupService lookup, ApplicationProcessorConfiguration applicationProcessorConfiguration)
        {
            if (entityStatusService == null)
                throw new ArgumentNullException(nameof(entityStatusService));

            EntityStatusService = entityStatusService;           
            ApplicationProcessorConfiguration = applicationProcessorConfiguration;
        }
      

        private IEntityStatusService EntityStatusService { get; }
        private ApplicationProcessorConfiguration ApplicationProcessorConfiguration { get; }
       

        public async Task ChangeStatus(string entityId, string newStatus, List<string> reasons)
        {
            EnsureInputIsValid(entityId);
            RequestModel objReason = new RequestModel();
            objReason.reasons = reasons;
            var objActiveStatus = await EntityStatusService.GetActiveStatusWorkFlow("application", entityId);
            await Task.Run(() => EntityStatusService.ChangeStatus("application", entityId, objActiveStatus.StatusWorkFlowId, newStatus, objReason));
        }

        public async Task RejectApplication(string entityId, List<string> reasons)
        {
            EnsureInputIsValid(entityId);
            RequestModel objReason = new RequestModel();
            objReason.reasons = reasons;
            var objActiveWorkFlow = await EntityStatusService.GetActiveStatusWorkFlow("application", entityId);
            await Task.Run(() => EntityStatusService.ChangeStatus("application", entityId, objActiveWorkFlow.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["Declined"], objReason));
        }

        public async Task ApplicationStatusMove(string applicationNumber)
        {
            EnsureInputIsValid(applicationNumber);
            var objActiveWorkFlow = await EntityStatusService.GetActiveStatusWorkFlow("application", applicationNumber);
            var currentApplicationStatus = await EntityStatusService.GetStatusByEntity("application", applicationNumber, objActiveWorkFlow.StatusWorkFlowId);
            if (currentApplicationStatus == null)
                throw new NotFoundException($"Current Status of Application Number {applicationNumber} can not be found");

            if (currentApplicationStatus.Code == ApplicationProcessorConfiguration.Statuses["FinalofferAccepted"])
                await Task.Run(() => EntityStatusService.ChangeStatus("application", applicationNumber, objActiveWorkFlow.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["CEApproved"],null));
            else if (currentApplicationStatus.Code == ApplicationProcessorConfiguration.Statuses["RBLFraud"])
                await Task.Run(() => EntityStatusService.ChangeStatus("application", applicationNumber,objActiveWorkFlow.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["BankCreditReview"], null));
            else if (currentApplicationStatus.Code == ApplicationProcessorConfiguration.Statuses["BankCreditReview"])
                await Task.Run(() => EntityStatusService.ChangeStatus("application", applicationNumber,objActiveWorkFlow.StatusWorkFlowId, ApplicationProcessorConfiguration.Statuses["Approved"], null));
        }   

        public async Task<IEnumerable<IActivity>> GetActivities(string applicationNumber)
        {

            EnsureInputIsValid(applicationNumber);
            var objActiveWorkFlow = await EntityStatusService.GetActiveStatusWorkFlow("application", applicationNumber);
            var currentApplicationStatus = await EntityStatusService.GetStatusByEntity("application", applicationNumber, objActiveWorkFlow.StatusWorkFlowId);
            if (currentApplicationStatus == null)
                throw new NotFoundException($"Current Status of Application Number {applicationNumber} can not be found");

            return await Task.Run(() => EntityStatusService.GetActivities("application", objActiveWorkFlow.StatusWorkFlowId, currentApplicationStatus.Code));

        }
        private void EnsureInputIsValid(string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

        }

        public static JToken FindToken<T>(string key, string value)
        {

            var jObject = JObject.Parse(value);
            var jToken = jObject.SelectToken(key);
            if (jToken != null)
                return jToken;

            return null;
        }
    }
}
