﻿using LendFoundry.StatusManagement.Client;
using LendFoundry.Application.Document.Client;
using LendFoundry.Business.Application.Client;
using LendFoundry.Business.Applicant.Client;
using LendFoundry.Configuration;
using LendFoundry.DataAttributes.Client;
using LendFoundry.DocumentGenerator.Client;
using LendFoundry.DocuSign.Client;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.VerificationEngine.Client;
using LendFoundry.Business.OfferEngine.Client;
using LendFoundry.Consent.Client;
using Microsoft.Extensions.DependencyInjection;
using System;
using LendFoundry.Syndication.Experian.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LMS.LoanManagement.Client;
using CapitalAlliance.Cashflow.Client;
using LendFoundry.Business.Funding.Client;

namespace CapitalAlliance.ApplicationProcessor
{
    public class ApplicationProcessorServiceFactory : IApplicationProcessorServiceFactory
    {
        public ApplicationProcessorServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IApplicationProcessorClientService Create(ITokenReader reader)
        {
            var eventHubServiceFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHubService = eventHubServiceFactory.Create(reader);

            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var applicationProcessorConfigurationService = configurationServiceFactory.Create<ApplicationProcessorConfiguration>(Settings.ServiceName, reader);
            var applicationProcessorConfiguration = applicationProcessorConfigurationService.Get();

            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTimeService = tenantTimeFactory.Create(configurationServiceFactory, reader);

            var applicationServiceClientFactory = Provider.GetService<IApplicationServiceClientFactory>();
            var applicationService = applicationServiceClientFactory.Create(reader);

           // var plaidServiceClientFactory = Provider.GetService<IPlaidServiceClientFactory>();
           // var plaidService = plaidServiceClientFactory.Create(reader);

            var applicationDocumentServiceClientFactory = Provider.GetService<IApplicationDocumentServiceClientFactory>();
            var applicationDocumentService = applicationDocumentServiceClientFactory.Create(reader);

            var docuSignServiceClientFactory = Provider.GetService<IDocuSignServiceClientFactory>();
            var docuSignService = docuSignServiceClientFactory.Create(reader);

            var documentGeneratorServiceFactory = Provider.GetService<IDocumentGeneratorServiceFactory>();
            var documentGeneratorService = documentGeneratorServiceFactory.Create(reader);

            var dataAttributesClientFactory = Provider.GetService<IDataAttributesClientFactory>();
            var dataAttributesService = dataAttributesClientFactory.Create(reader);

            var lookUpFactory = Provider.GetService<ILookupClientFactory>();
            var lookUpService = lookUpFactory.Create(reader);

            var verificationServiceFactory = Provider.GetService<IVerificationEngineServiceClientFactory>();
            var verificationService = verificationServiceFactory.Create(reader);

            var statusManagementServiceFactory = Provider.GetService<IStatusManagementServiceFactory>();
            var statusManagementService = statusManagementServiceFactory.Create(reader);

            var applicantServiceClientFactory = Provider.GetService<IApplicantServiceClientFactory>();
            var applicantService = applicantServiceClientFactory.Create(reader);

            var offerEngineServiceClientFactory = Provider.GetService<IOfferEngineServiceClientFactory>();
            var offerEngineService = offerEngineServiceClientFactory.Create(reader);

            var consentServiceClientFactory = Provider.GetService<IConsentServiceClientFactory>();
            var consentService = consentServiceClientFactory.Create(reader);

            var personalReportServiceFactory = Provider.GetService<IPersonalReportClientFactory>();
            var personalReportService = personalReportServiceFactory.Create(reader);

            var loggerFactory = Provider.GetService<ILoggerFactory>();
            var logger = loggerFactory.Create(NullLogContext.Instance);

            var loanManagementServiceFactory = Provider.GetService<ILoanManagementClientServiceFactory>();
            var loanManagementService = loanManagementServiceFactory.Create(reader);

            var cashFlowServiceFactory = Provider.GetService<ICashflowClientFactory>();
            var cashFlowService = cashFlowServiceFactory.Create(reader);

            var fundingServiceFactory = Provider.GetService<IFundingServiceClientFactory>();
            var fundingService = fundingServiceFactory.Create(reader);

            return new ApplicationService(applicationProcessorConfiguration, dataAttributesService,
                lookUpService, applicationService, eventHubService,
                applicationDocumentService, docuSignService, documentGeneratorService, verificationService,
                statusManagementService, applicantService, offerEngineService, consentService,
                personalReportService, tenantTimeService, logger, cashFlowService, fundingService, loanManagementService);
        }
    }
}
