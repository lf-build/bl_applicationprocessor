﻿using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Reflection;

namespace CapitalAlliance.ApplicationProcessor
{
    public class ApplicationProcessorListener : IApplicationProcessorListener
    {
        #region Constructor
        public ApplicationProcessorListener
        (
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            ITenantTimeFactory tenantTimeFactory,
            IApplicationProcessorServiceFactory applicationProcessorServicFactory,
            IConfigurationServiceFactory configurationFactory)
        {

            EnsureParameter(nameof(tokenHandler), tokenHandler);
            EnsureParameter(nameof(loggerFactory), loggerFactory);
            EnsureParameter(nameof(eventHubFactory), eventHubFactory);
            EnsureParameter(nameof(tenantServiceFactory), tenantServiceFactory);
            EnsureParameter(nameof(applicationProcessorServicFactory), applicationProcessorServicFactory);
            EnsureParameter(nameof(tenantTimeFactory), tenantTimeFactory);
            EnsureParameter(nameof(configurationFactory), configurationFactory);          

            EventHubFactory = eventHubFactory;
            TokenHandler = tokenHandler;
            LoggerFactory = loggerFactory;
            TenantServiceFactory = tenantServiceFactory;
            ApplicationProcessorServiceFactory = applicationProcessorServicFactory;
            TenantTimeFactory = tenantTimeFactory;
            ConfigurationFactory = configurationFactory;        
        }
        #endregion

        #region Private Properties       
        private IEventHubClientFactory EventHubFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private ILoggerFactory LoggerFactory { get; }
        private IApplicationProcessorServiceFactory ApplicationProcessorServiceFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }       
        #endregion

        #region Public Methods

        public void Start()
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            logger.Info("Starting Listener...");

            try
            {
                var emptyReader = new StaticTokenReader(string.Empty);
                var tenantService = TenantServiceFactory.Create(emptyReader);
                var tenantId = Environment.GetEnvironmentVariable("TenantId");
                logger.Info($"Processing tenant #{tenantId}");
                var token = TokenHandler.Issue(tenantId, Settings.ServiceName, null, "system", null);
                var reader = new StaticTokenReader(token.Value);
                var eventHub = EventHubFactory.Create(reader);
                var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
                var configuration = ConfigurationFactory.Create<ApplicationProcessorConfiguration>(Settings.ServiceName, reader).Get();

                var applicationProcessorService = ApplicationProcessorServiceFactory.Create(reader);
                
                if (configuration != null && configuration.events != null)
                {
                    configuration
                   .events
                   .ToList().ForEach(events =>
                   {
                       eventHub.On(events.Name, ProcessEvent(events, logger, applicationProcessorService));
                       logger.Info($"It was made subscription to EventHub with the Event: #{events.Name}");
                   });

                    logger.Info("-------------------------------------------");
                }
                else
                {
                    logger.Error($"The configuration for service #{Settings.ServiceName} could not be found, please verify");
                }
                eventHub.StartAsync();

                logger.Info("Application Processor listener started");
            }
            catch (Exception ex)
            {
                logger.Error("Error while listening eventhub to process Application Processor listener", ex);
                logger.Info("\n Application Processor listener  is working yet and waiting new event\n");
                Start();
            }
        }

        #endregion

        #region Private Methods

        #region Process Event
        private Action<LendFoundry.EventHub.EventInfo> ProcessEvent(
           EventConfiguration eventConfiguration,
           ILogger logger,
           IApplicationProcessorClientService applicationProcessorClientService
           )
        {
            return async @event =>
            {
                string responseData=string.Empty;
                string responseXmlData = string.Empty;
                object data = null;

                try
                {
                    try
                    {
                        responseData = eventConfiguration.Response.FormatWith(@event); //posted data                        
                        data = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(responseData);
                        responseXmlData = eventConfiguration.ResponseObject.FormatWith(@event); //posted xml data
                    }
                    catch(Exception e)
                    {

                    }

                    var entityId = eventConfiguration.EntityId.FormatWith(@event);
                    var entityType = eventConfiguration.EntityType.FormatWith(@event);
                    var Name = eventConfiguration.Name.FormatWith(@event); //Application Processor event name


                    var methodName = eventConfiguration.MethodToExecute;
                    MethodInfo method = typeof(IApplicationProcessorClientService).GetMethod(methodName);
                    if (Name == "ExperianBusinessReportFetched" && responseXmlData!="" )
                        data = responseXmlData;

                    object[] param = { entityType, entityId, data };
                    await (dynamic)method.Invoke(applicationProcessorClientService, param);
                    
                    logger.Info($"Entity Id: #{entityId}");
                    logger.Info($"Response Data : #{data}");
                    logger.Info($"Application Processor event {Name} completed for {entityId} ");

                }
                catch (Exception ex)
                {
                    logger.Error($"Unhadled exception while listening event {@event.Name}", ex);
                }
            };
        }
        #endregion

        #region Ensure Parameter
        private static void EnsureParameter(string name, object value)
        {
            if (value == null) throw new ArgumentNullException($"{name} cannot be null.");
        }
        #endregion

        #region Execute Request
        private T ExecuteRequest<T>(string response) where T : class
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(response);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + response, exception);
            }
        }
        #endregion

        #endregion

    }
}
