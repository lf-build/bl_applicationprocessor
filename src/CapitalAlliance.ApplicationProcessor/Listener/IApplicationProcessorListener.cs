﻿namespace CapitalAlliance.ApplicationProcessor
{
    public interface IApplicationProcessorListener
    {        void Start();
    }
}
