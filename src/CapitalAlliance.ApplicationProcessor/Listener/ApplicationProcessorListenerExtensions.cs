﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace CapitalAlliance.ApplicationProcessor
{
    public static class ApplicationProcessorListenerExtensions
    {
        public static void UseApplicationProcessorListener(this IApplicationBuilder application)
        {
            application.ApplicationServices.GetRequiredService<IApplicationProcessorListener>().Start();
        }
    }
}
