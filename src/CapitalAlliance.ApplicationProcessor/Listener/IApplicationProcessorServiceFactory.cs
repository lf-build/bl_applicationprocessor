﻿
using LendFoundry.Security.Tokens;

namespace CapitalAlliance.ApplicationProcessor
{
    public interface IApplicationProcessorServiceFactory
    {
        IApplicationProcessorClientService Create(ITokenReader reader);
    }
}
