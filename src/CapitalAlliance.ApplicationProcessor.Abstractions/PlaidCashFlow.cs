﻿using System;
using System.Collections.Generic;

namespace CapitalAlliance.ApplicationProcessor
{
    public class CashFlowResult
    {
        public List<PlaidCashflow> plaidCashflow { get; set; }
    }

    public class MonthlyCashFlow
    {
        public double AverageDailyBalance { get; set; }
        public double BeginingBalance { get; set; }
        public string CustomAttributes { get; set; }
        public int DepositCount { get; set; }
        public double EndingBalance { get; set; }
        public string FirstTransactionDate { get; set; }
        public int LoanPaymentAmount { get; set; }
        public double MaxDepositAmount { get; set; }
        public double MaxWithdrawalAmount { get; set; }
        public double MinDepositAmount { get; set; }
        public double MinWithdrawalAmount { get; set; }
        public int NSFAmount { get; set; }
        public string Name { get; set; }
        public int NumberOfLoanPayment { get; set; }
        public int NumberOfNSF { get; set; }
        public int NumberOfNegativeBalance { get; set; }
        public int NumberOfPayroll { get; set; }
        public double PayrollAmount { get; set; }
        public double TotalDepositAmount { get; set; }
        public double TotalWithdrawalAmount { get; set; }
        public int WithdrawalCount { get; set; }
        public int Year { get; set; }
    }

    public class TransactionSummary
    {
        public double AverageDailyBalance { get; set; }
        public double AverageDeposit { get; set; }
        public double AverageWithdrawal { get; set; }
        public int ChangeInDepositVolume { get; set; }
        public int CountOfMonthlyStatement { get; set; }
        public string EndDate { get; set; }
        public int LoanPaymentAmount { get; set; }
        public int NSFAmount { get; set; }
        public int NumberOfLoanPayment { get; set; }
        public int NumberOfNSF { get; set; }
        public int NumberOfNegativeBalance { get; set; }
        public int NumberOfPayroll { get; set; }
        public double PayrollAmount { get; set; }
        public string StartDate { get; set; }
        public int TotalCreditsCount { get; set; }
        public int TotalDebitsCount { get; set; }
        public double TotalCredits { get; set; }
        public double TotalDebits { get; set; }
        public double AvailableBalance { get; set; }
        public double CurrentBalance { get; set; }
        public double AverageBalanceLastMonth { get; set; }

        public double CVOfDailyBalance { get; set; }
        public double CVOfDailyDeposit { get; set; }
        public int MaxDaysBelow100Count { get; set; }
        public double MedianDailyBalance { get; set; }
        public double MedianMonthlyIncome { get; set; }
    }

    public class CategorySummary
    {
        public int TransactionCount { get; set; }
        public string CategoryName { get; set; }
        public double TransactionTotal { get; set; }
        public string CategoryId { get; set; }
        public string CustomCategoryId { get; set; }
        public int LastMonthTransactionCount { get; set; }
        public double LastMonthTransactionTotal { get; set; }
    }

    public class Transaction
    {
        public string Date { get; set; }
        public double Amount { get; set; }
        public string Description { get; set; }
        public string TransactionType { get; set; }
        public double RunningBalance { get; set; }
        public string Month { get; set; }
        public int Year { get; set; }
    }

    public class RecurringTransaction
    {
        public string Account { get; set; }
        public double Amount { get; set; }
        public double RoudingAmount { get; set; }
        public string Name { get; set; }
        public string Min { get; set; }
        public string Max { get; set; }
        public string MaxMinDiff { get; set; }
        public string TotalTransactions { get; set; }
        public bool IsRecurring { get; set; }
        public string Status { get; set; }
        public string ConfidenceLevel { get; set; }
        public List<Transaction> Transactions { get; set; }
    }

    public class CashFlow
    {
        public List<MonthlyCashFlow> MonthlyCashFlows { get; set; }
        public TransactionSummary TransactionSummary { get; set; }
        public List<CategorySummary> CategorySummary { get; set; }
        public List<Transaction> TransactionList { get; set; }
        public List<RecurringTransaction> RecurringList { get; set; }
    }

    public class PlaidCashflow
    {
        public string AccountNumber { get; set; }
        public string AccountID { get; set; }
        public string InstitutionName { get; set; }
        public string AccountType { get; set; }
        public CashFlow CashFlow { get; set; }
    }
}