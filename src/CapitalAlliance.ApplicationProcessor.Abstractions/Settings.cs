﻿using System;
using LendFoundry.Foundation.Services.Settings;

namespace CapitalAlliance.ApplicationProcessor {
    public static class Settings {
        public static string ServiceName { get; } = "application-processor";
        private static string Prefix { get; } = ServiceName.ToUpper ();
        public static ServiceSettings Configuration { get; } = new ServiceSettings ($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");
        public static ServiceSettings EventHub { get; } = new ServiceSettings ($"{Prefix}_EVENTHUB", "eventhub");
        public static ServiceSettings Tenant { get; } = new ServiceSettings ($"{Prefix}_TENANT", "tenant");
        public static ServiceSettings Identity { get; } = new ServiceSettings ($"{Prefix}_IDENTITY", "identity");
        public static DatabaseSettings Mongo { get; } = new DatabaseSettings ($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", "application-processor");
        public static ServiceSettings LookUpService { get; } = new ServiceSettings ($"{Prefix}_LOOKUP_HOST", "lookup-service", $"{Prefix}_LOOKUP_PORT");
        public static ServiceSettings StatusManagement { get; } = new ServiceSettings ($"{Prefix}_STATUSMANAGEMENT_HOST", "status-management", $"{Prefix}_STATUSMANAGEMENT_PORT");
        public static ServiceSettings BusinessApplication { get; } = new ServiceSettings ($"{Prefix}_BUSINESSAPPLICATION_HOST", "business-application", $"{Prefix}_BUSINESSAPPLICATION_PORT");
        public static ServiceSettings BusinessApplicant { get; } = new ServiceSettings ($"{Prefix}_BUSINESSAPPLICANT_HOST", "business-applicant", $"{Prefix}_BUSINESSAPPLICANT_PORT");
        public static ServiceSettings YelpSyndication { get; } = new ServiceSettings ($"{Prefix}_YELP_HOST", "syndication-yelp", $"{Prefix}_YELP_PORT");
        public static ServiceSettings BBBSearchSyndication { get; } = new ServiceSettings ($"{Prefix}_BBBSEARCH_HOST", "syndication-bbbsearch", $"{Prefix}_BBBSEARCH_PORT");
        public static ServiceSettings PlaidSyndication { get; } = new ServiceSettings ($"{Prefix}_PLAID_HOST", "plaid", $"{Prefix}_PLAID_PORT");

        public static ServiceSettings Email { get; } = new ServiceSettings ($"{Prefix}_EMAIL_HOST", "ce-email", $"{Prefix}_EMAIL_PORT");

        public static ServiceSettings DataAttribute { get; } = new ServiceSettings ($"{Prefix}_DATAATTRIBUTE_HOST", "data-attributes", $"{Prefix}_DATAATTRIBUTE_PORT");
        public static string Nats => Environment.GetEnvironmentVariable ($"{Prefix}_NATS_URL") ?? "nats://nats:4222";

        public static ServiceSettings ApplicationDocument { get; } = new ServiceSettings ($"{Prefix}_APPLICATIONDOCUMENT_HOST", "application-document", $"{Prefix}_APPLICATIONDOCUMENT_PORT");

        public static ServiceSettings Consent { get; } = new ServiceSettings ($"{Prefix}_CONSENT_HOST", "consent", $"{Prefix}_CONSENT_PORT");
        public static ServiceSettings Experian { get; } = new ServiceSettings ($"{Prefix}_EXPERIAN_HOST", "experian", $"{Prefix}_EXPERIAN_PORT");
        public static ServiceSettings VerificationEngine { get; } = new ServiceSettings ($"{Prefix}_VERIFICATIONENGINE_HOST", "verification-engine", $"{Prefix}_VERIFICATIONENGINE_PORT");
        public static ServiceSettings DocuSign { get; } = new ServiceSettings ($"{Prefix}_DOCUSIGN_HOST", "docusign", $"{Prefix}_DOCUSIGN_PORT");
        public static ServiceSettings PaynetSyndication { get; } = new ServiceSettings ($"{Prefix}_PAYNET_HOST", "paynet", $"{Prefix}_PAYNET_PORT");
        public static ServiceSettings BusinessOfferEngine { get; } = new ServiceSettings ($"{Prefix}_BUSINESSOFFERENGINE_HOST", "business-offerengine", $"{Prefix}_BUSINESSOFFERENGINE_PORT");
        public static ServiceSettings LexisNexisSyndication { get; } = new ServiceSettings ($"{Prefix}_LEXISNEXIS_HOST", "LexisNexis", $"{Prefix}_LEXISNEXIS_PORT");
        public static ServiceSettings ProductRule { get; } = new ServiceSettings ($"{Prefix}_PRODUCTRULE_HOST", "ProductRule", $"{Prefix}_PRODUCTRULE_PORT");
        public static ServiceSettings Assignment { get; } = new ServiceSettings ($"{Prefix}_ASSIGNMENT_HOST", "assignment", $"{Prefix}_ASSIGNMENT_PORT");
        public static ServiceSettings DocumentGenerator { get; } = new ServiceSettings ($"{Prefix}_DOCUMENTGENERATOR_HOST", "document-generator", $"{Prefix}_DOCUMENTGENERATOR_PORT");
        public static ServiceSettings businessFunding { get; } = new ServiceSettings ($"{Prefix}_BUSINESS-FUNDING", "business-Funding");
        public static ServiceSettings TokenGenerator { get; } = new ServiceSettings ($"{Prefix}_TOKEN-GENERATOR_HOST", "token-generator", $"{Prefix}_TOKEN-GENERATOR_PORT");
        public static ServiceSettings Partner { get; } = new ServiceSettings ($"{Prefix}_PARTNER_HOST", "partner", $"{Prefix}_PARTNER_PORT");
        public static ServiceSettings LoanManagement { get; } = new ServiceSettings ($"{Prefix}_LOAN-MANAGEMENT", "loan-management");
        public static ServiceSettings Product { get; } = new ServiceSettings ($"{Prefix}_PRODUCT_HOST", "product", $"{Prefix}_PRODUCT_PORT");
        public static ServiceSettings Cashflow { get; } = new ServiceSettings($"{Prefix}_CASHFLOW_HOST", "cashflow", $"{Prefix}_CASHFLOW_PORT");
    }
}