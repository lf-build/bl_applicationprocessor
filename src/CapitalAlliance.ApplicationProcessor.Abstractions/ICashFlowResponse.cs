﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapitalAlliance.ApplicationProcessor
{
    public interface ICashFlowResponse
    {
        string AccountNumber { get; set; }
        string Type { get; set; }        
        string InstituteName { get; set; }
        string AccountName { get; set; }
        string ProviderAccountId { get; set; }
    }
}
