﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapitalAlliance.ApplicationProcessor
{
    public class CashFlowResponse:ICashFlowResponse
    {
       public string AccountNumber { get; set; }
       public string Type { get; set; }      
       public string InstituteName { get; set; }
       public string AccountName { get; set; }
       public string ProviderAccountId { get; set; }

    }
}
