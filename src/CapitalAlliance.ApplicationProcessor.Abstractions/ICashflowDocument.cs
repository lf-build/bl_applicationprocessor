﻿
using System.Collections.Generic;

namespace CapitalAlliance.ApplicationProcessor
{
    public interface ICashflowDocument
    {
        string Logo { get; set; }
        CashflowAccountSection CashflowAccountSection { get; set; }
        List<CashflowTransactionListSection> TransactionList { get; set; }
        List<CashflowCategorySummarySection> CashflowCategorySummary { get; set; }
        TransactionSummarySection TransactionSummary { get; set; }
        List<RecurringTransactionSection> RecurringTransaction { get; set; }
        MonthlyCashFlowSummarySection MonthlySummary { get; set; }
    }
}