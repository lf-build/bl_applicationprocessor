﻿
namespace CapitalAlliance.ApplicationProcessor
{
    public enum RuleType
    {
        EligibilityCriteria = 1,
        PScoreComputation=2,
        OfferCriteria=3
    }
}