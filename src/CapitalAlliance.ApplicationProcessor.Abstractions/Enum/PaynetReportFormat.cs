﻿namespace CapitalAlliance.ApplicationProcessor
{
    public enum PaynetReportFormat
    {
        PDF,
        HTML,
        XML,
        BNDL,
        CDAT
    }
}