﻿
namespace CapitalAlliance.ApplicationProcessor
{
    public enum RuleName
    {
        CheckEligibility = 1,
        CalculatePScore = 2,
        CalculateOffers = 3,
        BusinessTaxIdVerification=4,
        GradeSegmentation = 5,
        TierSegmentation=6,
        SellRateLookup = 7,
        CalculateBP2Score =8,
        IndustryLook=9,
        GetCalculatedOffer=10

    }
}