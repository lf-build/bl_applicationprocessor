﻿
namespace CapitalAlliance.ApplicationProcessor
{
    public enum GroupName
    {
        EligibilityCheck = 1,
        PScoreCheck =2,
        BP2Calculation=3,
        BusinessTaxIdVerification=4
    }
}