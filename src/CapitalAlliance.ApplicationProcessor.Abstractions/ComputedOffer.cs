﻿
namespace CapitalAlliance.ApplicationProcessor
{
    public class ComputedOffer : IComputedOffer
    {
        public int term { get; set; }
        public double sell_rate { get; set; }
        public string pti { get; set; }
        public double monthly_revenue { get; set; }
        public double daily_revenue { get; set; }
        public double max_daily_debt_obligation { get; set; }
        public double max_debt_payment { get; set; }
        public double cal_loan_offer { get; set; }
        public double loan_offer_max { get; set; }
        public double loan_offer_min { get; set; }
        public double buy_rate { get; set; }
        public double loan_amount_cap { get; set; }
    }
}