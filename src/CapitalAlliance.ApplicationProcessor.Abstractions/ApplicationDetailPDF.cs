﻿using LendFoundry.Business.Applicant;
using LendFoundry.Business.Application;
using System;
using System.Collections.Generic;

namespace CapitalAlliance.ApplicationProcessor
{
    public class ApplicationDetailPDF
    {
        public ApplicationDetailPDF()
        {

        }

        public ApplicationDetailPDF(IApplication application, IApplicant applicant)
        {
            if (application.SelfDeclareInformation.IsExistingBusinessLoan == true)
                OtherPurposeDescription = "Yes";
            else
                OtherPurposeDescription = "No";
            ContactFirstName = application.ContactFirstName;
            ContactLastName = application.ContactLastName;
            Phone = application.PrimaryPhone.Phone;
            Email = application.PrimaryEmail.Email;
            LegalBusinessName = applicant.LegalBusinessName;
            DBA = applicant.DBA;
            if (application.PrimaryAddress != null)
            {
                AddressLine1 = application.PrimaryAddress.AddressLine1;
                City = application.PrimaryAddress.City;
                State = application.PrimaryAddress.State;
                Country = application.PrimaryAddress.Country;
                ZipCode = application.PrimaryAddress.ZipCode;
            }
            
            BusinessTaxID = applicant.BusinessTaxID;
            ClientIpAddress = application.ClientIpAddress;
            DateTimeOffset defaultValue = new DateTimeOffset(DateTime.MinValue, TimeSpan.Zero);
            if (applicant.BusinessStartDate.Time != defaultValue)
            {
                BusinessStartDate = applicant.BusinessStartDate.Time.ToString("MM'/'dd'/'yyyy");
            }
            if(application.ApplicationDate.Time!=defaultValue)
            {
                DateCreated = application.ApplicationDate.Time.DateTime.ToString();
            }
        }
        public string LoanTimeFrame { get; set; }
        public string PurposeOfLoan { get; set; }
        public string BusinessType { get; set; }
        public string Industry { get; set; }
        public string PropertyType { get; set; }
        public string OtherPurposeDescription { get; set; }
        public string RequestedAmount { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string LegalBusinessName { get; set; }
        public string DBA { get; set; }
        public string AddressLine1 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string BusinessTaxID { get; set; }
        public IList<IOwner> Owners { get; set; }
        public string AverageBankBalances { get; set; }
        public string AnnualRevenue { get; set; }
        public string ClientIpAddress { get; set; }
        public string BusinessStartDate { get; set; }
        public string DateCreated { get; set; }
    }
}