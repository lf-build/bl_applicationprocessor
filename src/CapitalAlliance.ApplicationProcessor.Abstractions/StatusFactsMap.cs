﻿using System.Collections.Generic;

namespace CapitalAlliance.ApplicationProcessor
{
    public class StatusFactsMap
    {
        public string StatusCode { get; set; }
        public List<string> AssociatedFacts { get; set; }
    }
}
