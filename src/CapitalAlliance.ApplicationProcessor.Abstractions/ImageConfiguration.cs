﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapitalAlliance.ApplicationProcessor
{
    public class ImageConfiguration
    {
        public string ContactAddress { get; set; }
        public string Logo { get; set; }
    }
}