﻿using LendFoundry.Security.Identity;
using System.Collections;
using System.Collections.Generic;

namespace CapitalAlliance.ApplicationProcessor
{
    public class ApplicationProcessorConfiguration
    {
        public CaseInsensitiveDictionary<string> Statuses { get; set; }        
        public string[] ReApplyStatuseCodes { get; set; }
        public int ReApplyTimeFrameDays { get; set; }
        public string DefaultPassword { get; set; }
        public string PaynetReportFormat { get; set; }
        public ImageConfiguration ImageConfiguration { get; set; }       
        public string InitialAssigmentRole { get; set; }
        public Dictionary<string,string> DocumentCategory { get; set; }
        public int FailureMaxRetry { get; set; }
        public string PhoneFormat { get; set; }
        public string PhonePattern { get; set; }
        public List<EventConfiguration> events { get; set; }
        public int ThreadWaitDuration { get; set; }        
        public bool BusinessCreditReport { get; set; } = true;
        public string[] DefaultApplicantRoles { get; set; }
        public string ApplicatntPortal { get; set; }
        public List<string> ResetFacts { get; set; }
        public string ExperianXsltPath { get; set; }
        public string BusinessTaxIdRegex { get; set; }
        public RevivalConfigurations RevivalConfigurations { get; set; }
        public string[] BankruptcyFcraState { get; set; }
        public decimal MinFundedAmount { get; set; }
        public string MinFundedAmountMessage { get; set; }
        public bool AutoOnBoardLoan { get; set; }
        public string DefaultProduct { get; set; }
        public List<string> GuestLoginSources { get; set; }
        public string BorrowerPortalUrl { get; set; }
        public int TokenExpirationDays { get; set; }
        public string ExperianXsltPathPPRProFilePath { get; set; }
        public string NonEligibleCountry { get; set; }
        public bool SkipM2ScoreCalculation { get; set; }
        public bool DeclineOnLowM2Score { get; set; }
        public decimal MinM2Score { get; set; }
        public bool ExtractedFieldRequired {get; set;}
    }
}