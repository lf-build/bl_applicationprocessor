﻿namespace CapitalAlliance.ApplicationProcessor
{
    public class DataAttributes
    {       
        public string EligibilityCheck { get; set; }
        public string PScoreAttributes { get; set; }
    }
}