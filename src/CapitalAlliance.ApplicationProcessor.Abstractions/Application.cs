﻿using LendFoundry.Foundation.Date;
using System.Collections.Generic;

namespace CapitalAlliance.ApplicationProcessor
{
    public class LoanTimeFrame
    {
        public string Time { get; set; }
        public string Hour { get; set; }
        public string Day { get; set; }
        public string Week { get; set; }
        public string Month { get; set; }
        public string Quarter { get; set; }
        public string Year { get; set; }
    }

    public class BusinessAddress
    {
        public string AddressId { get; set; }
        public string AddressLine1 { get; set; }
        public object AddressLine2 { get; set; }
        public object AddressLine3 { get; set; }
        public object AddressLine4 { get; set; }
        public object LandMark { get; set; }
        public int LocationType { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public object Country { get; set; }
        public string AddressType { get; set; }
        public bool IsDefault { get; set; }
    }

    public class PhoneNumber
    {
        public string PhoneId { get; set; }
        public string Phone { get; set; }
        public object CountryCode { get; set; }
        public int PhoneType { get; set; }
        public bool IsDefault { get; set; }
    }

    public class Address
    {
        public string AddressId { get; set; }
        public string AddressLine1 { get; set; }
        public object AddressLine2 { get; set; }
        public object AddressLine3 { get; set; }
        public object AddressLine4 { get; set; }
        public object LandMark { get; set; }
        public int LocationType { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public object Country { get; set; }
        public int AddressType { get; set; }
        public bool IsDefault { get; set; }
    }

    public class Owner
    {
        public string OwnerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SSN { get; set; }
        public string DOB { get; set; }
        public string OwnershipType { get; set; }
        public string OwnershipPercentage { get; set; }
        public object Designation { get; set; }
        public object EmailAddresses { get; set; }
        public List<PhoneNumber> PhoneNumbers { get; set; }
        public List<Address> Addresses { get; set; }
    }

    public class Application
    {
        public string businessApplicantName { get; set; }
        public string sICCode { get; set; }
        public string dBA { get; set; }
        public object dUNSNumber { get; set; }
        public object nAICCode { get; set; }
        public TimeBucket applicationDate { get; set; }
        public string contactFirstName { get; set; }
        public string contactLastName { get; set; }
        public object contactMiddleName { get; set; }
        public object applicantBankIsDefault { get; set; }
        public object applicantBankIsVerified { get; set; }
        public object applicantBankVerifiedDate { get; set; }
        public object applicantBankAccountHolderName { get; set; }
        public object applicantBankAccountNumber { get; set; }
        public object applicantBankAccountType { get; set; }
        public object applicantBankId { get; set; }
        public object applicantBankName { get; set; }
        public object applicantBankRoutingNumber { get; set; }
        public object applicantBankVerifiedBy { get; set; }
        public string applicantPhone { get; set; }
        public LoanTimeFrame loanTimeFrame { get; set; }
        public TimeBucket submitted { get; set; }
        public object productId { get; set; }
        public object requestedTermType { get; set; }
        public string applicationId { get; set; }
        public string applicationNumber { get; set; }
        public int requestedAmount { get; set; }
        public int requestedTermValue { get; set; }
        public string purposeOfLoan { get; set; }
        public object decisionDate { get; set; }
        public TimeBucket expiryDate { get; set; }
        public BusinessAddress businessAddress { get; set; }
        public object socialLinks { get; set; }
        public object businessFax { get; set; }
        public object businessWebsite { get; set; }
        public TimeBucket businessStartDate { get; set; }
        public string businessTaxID { get; set; }
        public string legalBusinessName { get; set; }
        public string businessType { get; set; }
        public string applicantWorkEmail { get; set; }
        public string sourceId { get; set; }
        public string sourceType { get; set; }
        public object statusHistory { get; set; }
        public List<Owner> owners { get; set; }
        public string businessPhone { get; set; }
        public int annualRevenue { get; set; }
        public int averageBankBalance { get; set; }
        public bool haveExistingLoan { get; set; }
        public string propertyType { get; set; }
        public string industry { get; set; }
    }
}