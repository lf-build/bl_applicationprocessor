﻿using LendFoundry.SyndicationStore.Events;

namespace CapitalAlliance.ApplicationProcessor.Events
{
    public class DealGenerated: SyndicationCalledEvent
    {
    }
}
