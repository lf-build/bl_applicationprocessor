﻿namespace CapitalAlliance.ApplicationProcessor
{
    public class DocuSignData
    {
        public byte[] DocumentContent { get; set; }
        public string DocumentName { get; set; }
    }
}