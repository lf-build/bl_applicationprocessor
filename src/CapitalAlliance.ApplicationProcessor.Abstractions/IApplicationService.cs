﻿using CapitalAlliance.ApplicationProcessor.Request;
using LendFoundry.Business.Application;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Business.OfferEngine;
using LendFoundry.Syndication.LexisNexis.ADLFunctions;
using LendFoundry.Syndication.LexisNexis.Bankruptcy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport;
using LendFoundry.ProductRule;
using LendFoundry.Syndication.Experian.Response;
using LendFoundry.Business.Applicant;

namespace CapitalAlliance.ApplicationProcessor
{
    public interface IApplicationProcessorClientService
    {
        Task<IApplicationResponse> SubmitBusinessApplication(ISubmitBusinessApplicationRequest submitBusinessApplicationRequest);

        Task<IApplicationDetails> AddLead(ISubmitBusinessApplicationRequest submitApplicationRequest);

        Task<bool> SubmitDocument(string entityId, string category, byte[] fileBytes, string fileName, List<string> tags);

        Task<bool> SignConsent(string entityId, string category, object body);

        Task<ISearchBusinessResponse> BusinessSearch(string entityId, IBusinessSearchRequest request);

        Task<IBusinessReportResponse> GetBusinessReport(string entityId, IBusinessReportRequest request);

        Task<IPersonalReportResponse> GetPersonalReport(string entityId, IPersonalReportRequest request);

        Task<string> ChooseOffer(string entityId, string offerId);

        Task<LendFoundry.Syndication.Paynet.Response.ISearchCompanyResponse> SearchCompany(string entityId, ISearchCompanyRequest request);

        Task<bool> GetPaynetReport(string entityId, string paynetId);

        Task<string> PerformBusinessVerification1(string entityId);

        Task<string> PerformBusinessVerification2(string entityId);

        Task<string> ResendBankLinking(string entityId,string emailId);

        Task<IProductRuleResult> ComputeOffer(string entityId);

        Task<IList<IBusinessLoanOffer>> PresentOffer(string entityId);

        Task<string> GenerateAndSendAgreement(string entityId);

        Task<string> PerformCreditCheckVerification(string entityId);

        Task<string> PerformBusinessTaxIdVerification(string entityId);

        Task<string> PerformBusinessReviewCreditRisk(string entityId);

        Task<IApplicationDetails> GetApplicationDetails(string applicationId);

        Task<IApplicationDetails> EditApplication(string entityId, ISubmitBusinessApplicationRequest request);

        Task<IApplicationOffer> AddDeal(string entityId, IDealOfferRequest request);

        Task<IADLSearchResponse> ADLSearch(string entityId, CapitalAlliance.ApplicationProcessor.Request.SearchADLRequest searchRequest);

        Task<IBankruptcySearchResponse> BankruptcySearch(string entityid, Request.ISearchBankruptcyRequest request);

        Task<IBankruptcyReportResponse> BankruptcyReport(string entityid, Request.IBankruptcyReportRequest request);

        Task<ICriminalRecordSearchResponse> CriminalRecordSearch(string entityid, ICriminalRecordSearchRequest request);

        Task<ICriminalReportResponse> CriminalRecordReport(string entityid, ICriminalRecordReportRequest request);

        Task AddFundingRequest(string entityid);

        Task<IProductRuleResult> GetComputedOffer(string entityId, object offerData);

        Task<bool> SignDocumentReceivedHandler(string entityType, string entityId, object data);
        Task<bool> BanKLinkRequestedHandler(string entityType, string entityId, object data);
        Task FundingStatusHandler(string entityType, string entityId, object data);
        Task<string> MCAAgreementHandler(string entityType, string entityId, object data);
        Task UpdateSICCode(string entityId, string sicCode);
        Task<object> GetApplicationDocuments(string entityId, string documentName);
        Task<bool> ExperianBusinessCreditReporHandler(string entityType, string entityId, object data);
        Task CashflowAccountSelectedHandler(string entityType, string entityId, object data);
        Task<ICashFlowResponse> GetcashflowAccountDetails(string entityId);
        Task<ICashFlowResponse> GetFundingAccountDetails(string entityId);
        Task<List<IOwner>> GetOwners(string entityId);
        Task SwitchOwner(string entityId, string ownerId);
        Task NonPrimaryExperianCallHandler(string entityType, string entityId, object data);
        Task ApplicationPDFHandler(string entityType, string entityId, object data);
        Task MCAAgreementCreationHandler(string entityType, string entityId, object data);
        Task<Dictionary<string, object>> ComputeProvisionalOffer(string entityId);
        Task<bool> CheckReApplyEligibility(string entityId);
        Task<bool> CheckRevivalEligibility(string entityId);
        Task ReviveApplication(string entityId);
        Task ExperainPersonalPDFHandler(string entityType, string entityId, object data);
        Task EnableReapply(string entityId);
        Task ApplicationPreApprovedHandler(string entityType, string entityId, object data);
        Task LoanOnBoardHandler(string entityType, string entityId, object data);
        Task AddAttribute(IAddAttributeRequest request);
        Task<object> GetExtractedData(string entityId,string key);
    }
}