﻿using LendFoundry.StatusManagement;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CapitalAlliance.ApplicationProcessor
{
    public interface IStatusManagementService
    {
        Task ChangeStatus(string entityId, string newStatus, List<string> reasons);

        Task<IEnumerable<IActivity>> GetActivities(string applicationNumber);

        Task RejectApplication(string entityId, List<string> reasons);

        Task ApplicationStatusMove(string applicationNumber);
    }
}
