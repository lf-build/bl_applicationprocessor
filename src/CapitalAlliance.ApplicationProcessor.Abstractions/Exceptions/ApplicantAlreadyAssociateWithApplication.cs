﻿using System;

namespace CapitalAlliance.ApplicationProcessor.Exceptions
{
    public class ApplicantAlreadyAssociateWithApplication : Exception
    {
        public ApplicantAlreadyAssociateWithApplication(string email, string message) : base(message)
        {
            Email = email;
        }

        public ApplicantAlreadyAssociateWithApplication(string email)
        {
            Email = email;
        }

        public string Email { get; set; }
    }
}
