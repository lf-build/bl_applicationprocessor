﻿namespace CapitalAlliance.ApplicationProcessor.Request
{
    public class CriminalRecordReportRequest : ICriminalRecordReportRequest
    {
        public string UniqueId { get; set; }
        public string QueryId { get; set; }
        public string OwnerId { get; set; }
    }
}