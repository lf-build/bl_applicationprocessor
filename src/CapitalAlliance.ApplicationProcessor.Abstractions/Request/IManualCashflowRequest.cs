﻿namespace CapitalAlliance.ApplicationProcessor.Request
{
    public interface IManualCashflowRequest
    {
        string accountNumber { get; set; }
        string instituteName { get; set; }
        string accountType { get; set; }
        string fileContent { get; set; }
        string fileName { get; set; }
    }
}