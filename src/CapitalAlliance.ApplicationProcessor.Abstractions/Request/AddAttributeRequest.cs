﻿using System.Collections.Generic;

namespace CapitalAlliance.ApplicationProcessor.Request
{
    public class AddAttributeRequest : IAddAttributeRequest
    {
        public List<string> applicationNumber { get; set; }   
        public string productId { get; set; }
    }
}