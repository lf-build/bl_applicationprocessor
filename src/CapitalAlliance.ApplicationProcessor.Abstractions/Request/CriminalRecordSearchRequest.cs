﻿namespace CapitalAlliance.ApplicationProcessor.Request
{
    public class CriminalRecordSearchRequest : ICriminalRecordSearchRequest
    {
        public string Ssn { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OwnerId { get; set; }
        public string StreetAddress1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip5 { get; set; }

    }
}