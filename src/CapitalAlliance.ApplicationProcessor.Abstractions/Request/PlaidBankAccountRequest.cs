﻿namespace CapitalAlliance.ApplicationProcessor.Request
{
    public class PlaidBankAccountRequest : IPlaidBankAccountRequest
    {
        public string PublicToken { get; set; }
        public string BankSupportedProductType { get; set; }
    }
}
