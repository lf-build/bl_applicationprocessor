﻿namespace CapitalAlliance.ApplicationProcessor.Request
{
    public interface IPlaidBankAccountRequest
    {
        string PublicToken { get; set; }
        string BankSupportedProductType { get; set; }       
    }
}
