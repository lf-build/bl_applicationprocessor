﻿namespace CapitalAlliance.ApplicationProcessor.Request
{
    public interface ISearchBankruptcyRequest
    {
        string Ssn { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string OwnerId { get; set; }
        string StreetAddress1 { get; set; }
        string City { get; set; }
        string State { get; set; }
        string Zip5 { get; set; }

    }
}