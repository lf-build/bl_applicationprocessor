﻿namespace CapitalAlliance.ApplicationProcessor.Request
{
    public class ManualCashflowRequest : IManualCashflowRequest
    {
        public string accountNumber { get; set; }
        public string instituteName { get; set; }
        public string accountType { get; set; }

        //base64 CSV file string
        public string fileContent { get; set; }
        public string fileName { get; set; }
    }
}