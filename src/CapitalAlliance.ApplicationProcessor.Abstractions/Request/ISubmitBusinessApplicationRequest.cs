﻿using LendFoundry.Business.Applicant;
using LendFoundry.Business.Application;
using System;
using System.Collections.Generic;

namespace CapitalAlliance.ApplicationProcessor.Request
{
    public interface ISubmitBusinessApplicationRequest
    {
        string Email { get; set; }
        double RequestedAmount { get; set; }
        string PurposeOfLoan { get; set; }
        string LoanTimeFrame { get; set; }
        DateTimeOffset DateNeeded { get; set; }
        string LegalBusinessName { get; set; }
        string DBA { get; set; }
        string AddressLine1 { get; set; }
        string City { get; set; }
        string Country { get; set; }
        string State { get; set; }
        string ZipCode { get; set; }
        int AddressType { get; set; }
        string BusinessTaxID { get; set; }
        string SICCode { get; set; }
        DateTimeOffset BusinessStartDate { get; set; }
        string BusinessType { get; set; }
        string ContactFirstName { get; set; }
        string ContactLastName { get; set; }
        double AnnualRevenue { get; set; }
        double AverageBankBalances { get; set; }
        bool HaveExistingLoan { get; set; }
        string Source { get; set; }
        string Industry { get; set; }
        string Phone { get; set; }
        string Signature { get; set; }
        IList<IOwner> Owners { get; set; }
        string PropertyType { get; set; }
        string ApplicationNumber { get; set; }

        string ApplicantId { get; set; }

        string LeadOwnerId { get; set; }
        string PartnerUserId { get; set; }

        string OtherPurposeDescription { get; set; }
        List<IExternalReferences> ExternalReferences { get; set; }
        string Password { get; set; }
        string ClientIpAddress { get; set; }  
        bool IsReapply { get; set; }
    }
}