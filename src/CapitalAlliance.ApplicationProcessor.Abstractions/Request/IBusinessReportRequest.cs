﻿namespace CapitalAlliance.ApplicationProcessor.Request
{
    public interface IBusinessReportRequest
    {
        string TransactionNumber { get; set; }
        string BisListNumber { get; set; }
        string DBA { get; set; }
        string LegalBusinessName { get; set; }
        string AddressLine1 { get; set; }
        string City { get; set; }
        string State { get; set; }
        string ZipCode { get; set; }
    }
}