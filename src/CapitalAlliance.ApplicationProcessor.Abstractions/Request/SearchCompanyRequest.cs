﻿namespace CapitalAlliance.ApplicationProcessor.Request
{
    public class SearchCompanyRequest : ISearchCompanyRequest
    {
        public string CompanyName { get; set; }

        public string Alias { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Phone { get; set; }

        public string TaxId { get; set; }     
    }
}