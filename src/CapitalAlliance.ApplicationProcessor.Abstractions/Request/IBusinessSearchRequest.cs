﻿namespace CapitalAlliance.ApplicationProcessor.Request
{
    public interface IBusinessSearchRequest
    {
        string LegalBusinessName { get; set; }

        string DBA { get; set; }

        string AddressLine1 { get; set; }

        string City { get; set; }

        string State { get; set; }

        string ZipCode { get; set; }
    }
}