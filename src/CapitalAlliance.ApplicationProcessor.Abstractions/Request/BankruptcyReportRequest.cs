﻿namespace CapitalAlliance.ApplicationProcessor.Request
{
    public class BankruptcyReportRequest : IBankruptcyReportRequest
    {
        public string BusinessId { get; set; }
        public string TMSId { get; set; }
        public string UniqueId { get; set; }
        public string OwnerId { get; set; }
    }
}