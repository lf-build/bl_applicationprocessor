﻿using System;
namespace CapitalAlliance.ApplicationProcessor.Request {
    public class SearchADLRequest : ISearchADLRequest {
        public string Ssn { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string NameSuffix { get; set; }
        public string Streetaddress1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip5 { get; set; }
        public string Phone { get; set; }
        public DateTimeOffset?DOB { get; set; }
        public string RequestedProduct {get;set;}
        public string OwnerId {get;set;}
    }
}