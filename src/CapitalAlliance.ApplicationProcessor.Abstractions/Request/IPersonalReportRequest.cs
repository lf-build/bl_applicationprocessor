﻿namespace CapitalAlliance.ApplicationProcessor.Request
{
    public interface IPersonalReportRequest
    {
        string Firstname { get; set; }
        string Lastname { get; set; }
        string Middlename { get; set; }
        string Gen { get; set; }

        string Ssn { get; set; }
        string Street { get; set; }
        string City { get; set; }

        string State { get; set; }

        string Zip { get; set; }

        string Company { get; set; }
        string CompanyAddress { get; set; }
        string CompanyCity { get; set; }
        string CompanyState { get; set; }
        string CompanyZip { get; set; }
        string PhoneNumber { get; set; }
        string PhoneType { get; set; }

        string Dob { get; set; }

        string AccountType { get; set; }
        string RiskModels { get; set; }
        string CustomRRDashKeyword { get; set; }
    }
}
