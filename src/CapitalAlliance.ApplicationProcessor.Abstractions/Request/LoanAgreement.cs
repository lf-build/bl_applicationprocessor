﻿using LendFoundry.Business.Applicant;
using System.Collections.Generic;

namespace CapitalAlliance.ApplicationProcessor.Request
{
    public class LoanAgreement : ILoanAgreement
    {
        
        public string AccountCheckingAccountNumber { get; set; }

        public string AccountRoutingNumber { get; set; }

        //primary and mail address
        public string Street { get; set; }

        public string BankName { get; set; }

        public string Branch { get; set; }

        public string BusinessStartDate { get; set; }

        public string BusinessType { get; set; }

        public string City { get; set; }

        public string ContactPerson { get; set; }

        public string ContactPhone { get; set; }

        public string DBA { get; set; }

        public string FederalTaxID { get; set; }

        public string IPAddress { get; set; }

        //Merchant Legal Name
        public string LegalBusinessName { get; set; }

        //new added
        //payment method
        public string OpportunityDocPaymentType { get; set; }

        public string OpportunityOrginizationFee { get; set; }

        public IList<IOwner> Owners { get; set; }

        //loan amount-selected offer
        public string PurchasePrice { get; set; }

        public string RecieptsPurchasedAmount { get; set; }

        public string SignedBy { get; set; }

        public string SpecificDailyAmmount { get; set; }
        public string SpecifiedPercentage { get; set; }
        public string State { get; set; }

        //template images
        public string Logo { get; set; }

        public string Zip { get; set; }

        public string DateCreated { get; set; }

        public string AccountPayment { get; set; }
    }
}