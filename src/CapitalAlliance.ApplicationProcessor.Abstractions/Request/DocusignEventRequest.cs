﻿namespace CapitalAlliance.ApplicationProcessor.Request
{
    public class DocusignEventRequest: IDocusignEventRequest
    {
        public string SignerEmail { get; set; }
        public string DocumentBytes { get; set; }
        public string Status { get; set; }
    }
}
