﻿namespace CapitalAlliance.ApplicationProcessor.Request
{
    public class BusinessReportRequest : IBusinessReportRequest
    {
      public string TransactionNumber { get; set; }
      public string BisListNumber { get; set; }
      public string DBA { get; set; }
      public string LegalBusinessName { get; set; }
      public string AddressLine1 { get; set; }
      public string City { get; set; }
      public string State { get; set; }
      public string ZipCode { get; set; }
    }
}