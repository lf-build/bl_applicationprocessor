﻿namespace CapitalAlliance.ApplicationProcessor.Request
{
    public interface IDocusignEventRequest
    {
        string SignerEmail { get; set; }
        string DocumentBytes { get; set; }
        string Status { get; set; }
    }
}
