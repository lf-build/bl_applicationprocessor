﻿using System;
namespace CapitalAlliance.ApplicationProcessor.Request {
    public interface ISearchADLRequest {
        string Ssn { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string MiddleName { get; set; }
        string NameSuffix { get; set; }
        string Streetaddress1 { get; set; }
        string City { get; set; }
        string State { get; set; }
        string Zip5 { get; set; }
        string Phone { get; set; }
        DateTimeOffset?DOB { get; set; }
        string RequestedProduct {get;set;}
        string OwnerId {get;set;}
    }
}