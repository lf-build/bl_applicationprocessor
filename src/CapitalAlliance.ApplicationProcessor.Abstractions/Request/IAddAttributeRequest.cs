﻿using System.Collections.Generic;

namespace CapitalAlliance.ApplicationProcessor.Request
{
    public interface IAddAttributeRequest
    {
        List<string> applicationNumber { get; set; }        
        string productId { get; set; }
    }
}