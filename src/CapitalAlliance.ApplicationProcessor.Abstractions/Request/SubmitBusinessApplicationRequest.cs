﻿using LendFoundry.Business.Applicant;
using LendFoundry.Business.Application;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace CapitalAlliance.ApplicationProcessor.Request
{
    public class SubmitBusinessApplicationRequest : ISubmitBusinessApplicationRequest
    {
        public string Email { get; set; }
        public double RequestedAmount { get; set; }
        public string PurposeOfLoan { get; set; }
        public string LoanTimeFrame { get; set; }
        public DateTimeOffset DateNeeded { get; set; }
        public string LegalBusinessName { get; set; }
        public string DBA { get; set; }
        public string AddressLine1 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public int AddressType { get; set; }
        public string BusinessTaxID { get; set; }
        public string SICCode { get; set; }
        public DateTimeOffset BusinessStartDate { get; set; }
        public string BusinessType { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public double AnnualRevenue { get; set; }

        public double AverageBankBalances { get; set; }
        public bool HaveExistingLoan { get; set; }
        public string BankType { get; set; }
        public string Source { get; set; }

        public string Industry { get; set; }

        public string Phone { get; set; }

        public string Signature { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IOwner, LendFoundry.Business.Applicant.Owner>))]
        public IList<IOwner> Owners { get; set; }

        public string PropertyType { get; set; }

        public string ApplicationNumber { get; set; }

        public string ApplicantId { get; set; }

        public string LeadOwnerId { get; set; }
        public string PartnerUserId { get; set; }
        
        public string OtherPurposeDescription { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IExternalReferences, ExternalReferences>))]
        public List<IExternalReferences> ExternalReferences { get; set; }
        public string Password { get; set; }
        public string ClientIpAddress { get; set; }
        public bool IsReapply { get; set; }
    }
}