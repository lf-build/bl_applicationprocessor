﻿namespace CapitalAlliance.ApplicationProcessor.Request
{
    public interface IBankruptcyReportRequest
    {
         string BusinessId { get; set; }
         string TMSId { get; set; }
         string UniqueId { get; set; }
        string OwnerId { get; set; }
    }
}