﻿using LendFoundry.Business.Applicant;
using System.Collections.Generic;

namespace CapitalAlliance.ApplicationProcessor.Request
{
    public interface ILoanAgreement
    {
        
        string AccountCheckingAccountNumber { get; set; }

        string AccountRoutingNumber { get; set; }

        //primary and mail address
        string Street { get; set; }

        string BankName { get; set; }

        string Branch { get; set; }

        string BusinessStartDate { get; set; }

        string BusinessType { get; set; }

        string City { get; set; }

        string ContactPerson { get; set; }

        string ContactPhone { get; set; }

        string DBA { get; set; }

        string FederalTaxID { get; set; }

        string IPAddress { get; set; }

        //Merchant Legal Name
        string LegalBusinessName { get; set; }

        //new added
        //payment method
        string OpportunityDocPaymentType { get; set; }

        string OpportunityOrginizationFee { get; set; }

        IList<IOwner> Owners { get; set; }

        //loan amount-selected offer
        string PurchasePrice { get; set; }

        string RecieptsPurchasedAmount { get; set; }

        string SignedBy { get; set; }

        string SpecificDailyAmmount { get; set; }
        string SpecifiedPercentage { get; set; }
        string State { get; set; }

        //template images
        string Logo { get; set; }

        string Zip { get; set; }
        string DateCreated { get; set; }
        string AccountPayment { get; set; }
    }
}