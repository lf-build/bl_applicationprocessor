﻿namespace CapitalAlliance.ApplicationProcessor.Request
{
    public interface ICriminalRecordReportRequest
    {
        string UniqueId { get; set; }
        string QueryId { get; set; }
        string OwnerId { get; set; }
    }
}