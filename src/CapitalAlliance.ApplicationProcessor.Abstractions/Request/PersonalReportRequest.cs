﻿namespace CapitalAlliance.ApplicationProcessor.Request
{
    public class PersonalReportRequest : IPersonalReportRequest
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Middlename { get; set; }
        public string Gen { get; set; }
        public string Ssn { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

        public string PhoneNumber { get; set; }
        public string PhoneType { get; set; }
        public string Dob { get; set; }
        public string AccountType { get; set; }
        public string RiskModels { get; set; }
        public string CustomRRDashKeyword { get; set; }
        public string Company { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyCity { get; set; }
        public string CompanyState { get; set; }
        public string CompanyZip { get; set; }
    }
}
