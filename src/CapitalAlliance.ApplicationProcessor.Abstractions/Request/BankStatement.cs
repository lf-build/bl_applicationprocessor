﻿namespace CapitalAlliance.ApplicationProcessor.Request
{
    public class BankStatement
    {
        public string Amount { get; set; }
        public string Date { get; set; }
        public string Name { get; set; }
        public string CategoryId { get; set; }
        public string Available { get; set; }
    }
}