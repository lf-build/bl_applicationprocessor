﻿namespace CapitalAlliance.ApplicationProcessor.Request
{
    public interface ISearchCompanyRequest
    {
        string CompanyName { get; set; }

        string Alias { get; set; }

        string Address { get; set; }

        string City { get; set; }

        string State { get; set; }

        string Phone { get; set; }

        string TaxId { get; set; }       
    }
}