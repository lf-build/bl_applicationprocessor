﻿namespace CapitalAlliance.ApplicationProcessor.Request
{
    public class BusinessSearchRequest : IBusinessSearchRequest
    {
        public string LegalBusinessName { get; set; }

        public string DBA { get; set; }

        public string AddressLine1 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string ZipCode { get; set; }
    }
}