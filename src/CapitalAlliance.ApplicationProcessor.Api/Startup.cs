﻿using System;
using System.IO;
using System.Threading.Tasks;
using LendFoundry.BBBSearch.Client;
using LendFoundry.Consent.Client;
using LendFoundry.StatusManagement.Client;
using LendFoundry.Application.Document.Client;
using LendFoundry.AssignmentEngine.Client;
using LendFoundry.Business.Applicant.Client;
using LendFoundry.Business.Application.Client;
using LendFoundry.Business.Funding.Client;
using LendFoundry.Business.OfferEngine.Client;
using CapitalAlliance.Cashflow.Client;
using LendFoundry.Configuration;
using LendFoundry.Configuration.Client;
using LendFoundry.DataAttributes.Client;
using LendFoundry.DocumentGenerator.Client;
using LendFoundry.DocuSign.Client;
using LendFoundry.Email.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Partner.Client;
using LendFoundry.Paynet.Client;
using LendFoundry.ProductConfiguration.Client;
using LendFoundry.ProductRule.Client;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.Experian.Client;
using LendFoundry.Syndication.LexisNexis.Client;
using LendFoundry.Syndication.Yelp;
using LendFoundry.Syndication.Yelp.Client;
using LendFoundry.Tenant.Client;
using LendFoundry.Token.Generator.Client;
using LendFoundry.VerificationEngine.Client;
using LMS.LoanManagement.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;

namespace CapitalAlliance.ApplicationProcessor.Api {
    public class Startup {
        public void ConfigureServices (IServiceCollection services) {
            services.AddSwaggerGen (c => {
                c.SwaggerDoc ("v1", new Info {
                    Version = "v1",
                        Title = "CapitalAlliance.ApplicationProcessor"
                });
                c.AddSecurityDefinition ("apiKey", new ApiKeyScheme () {
                    Type = "apiKey",
                        Name = "Authorization",
                        Description =
                        "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                        In = "header"
                });
                c.DescribeAllEnumsAsStrings ();
                c.IgnoreObsoleteProperties ();
                c.DescribeStringEnumsInCamelCase ();
                c.IgnoreObsoleteActions ();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine (basePath, "CapitalAlliance.ApplicationProcessor.Api.xml");
                c.IncludeXmlComments (xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor> ();

            services.AddTenantTime ();
            services.AddTokenHandler ();
            services.AddHttpServiceLogging (Settings.ServiceName);
            services.AddEventHub (Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddTenantService (Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddConfigurationService<ApplicationProcessorConfiguration> (Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);

            services.AddCors ();
            services.AddSingleton<IMongoConfiguration> (p => new MongoConfiguration (Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddTransient (provider => provider.GetRequiredService<IConfigurationService<ApplicationProcessorConfiguration>> ().Get ());
            services.AddLookupService (Settings.LookUpService.Host, Settings.LookUpService.Port);
            services.AddStatusManagementService (Settings.StatusManagement.Host, Settings.StatusManagement.Port);
            services.AddApplicationService (Settings.BusinessApplication.Host, Settings.BusinessApplication.Port);
            services.AddApplicantService (Settings.BusinessApplicant.Host, Settings.BusinessApplicant.Port);
            services.AddDataAttributes (Settings.DataAttribute.Host, Settings.DataAttribute.Port);
            services.AddApplicantDocumentService (Settings.ApplicationDocument.Host, Settings.ApplicationDocument.Port);
            services.AddConsentService (Settings.Consent.Host, Settings.Consent.Port);
            services.AddOfferEngineService (Settings.BusinessOfferEngine.Host, Settings.BusinessOfferEngine.Port);
            services.AddProductRuleService (Settings.ProductRule.Host, Settings.ProductRule.Port);
            services.AddDocumentGenerator (Settings.DocumentGenerator.Host, Settings.DocumentGenerator.Port);
            services.AddYelpService (Settings.YelpSyndication.Host, Settings.YelpSyndication.Port);
            services.AddBBBSearchService (Settings.BBBSearchSyndication.Host, Settings.BBBSearchSyndication.Port);
            //services.AddPlaidService(Settings.PlaidSyndication.Host, Settings.PlaidSyndication.Port);
            services.AddEmailService (Settings.Email.Host, Settings.Email.Port);
            services.AddBusinessReportService (Settings.Experian.Host, Settings.Experian.Port);
            services.AddPersonalReportService (Settings.Experian.Host, Settings.Experian.Port);
            services.AddVerificationEngineService (Settings.VerificationEngine.Host, Settings.VerificationEngine.Port);
            services.AddDocuSignService (Settings.DocuSign.Host, Settings.DocuSign.Port);
            services.AddPaynetService (Settings.PaynetSyndication.Host, Settings.PaynetSyndication.Port);
            services.AddLexisNexisService (Settings.LexisNexisSyndication.Host, Settings.LexisNexisSyndication.Port);
            services.AddAssignmentService (Settings.Assignment.Host, Settings.Assignment.Port);
            services.AddIdentityService (Settings.Identity.Host, Settings.Identity.Port);
            services.AddTransient<IApplicationProcessorClientService, ApplicationService> ();
            services.AddTransient<IStatusManagementService, StatusManagementService> ();
            services.AddFundingService (Settings.businessFunding.Host, Settings.businessFunding.Port);
            services.AddTokenGeneratorService (Settings.TokenGenerator.Host, Settings.TokenGenerator.Port);
            services.AddTransient<IApplicationProcessorListener, ApplicationProcessorListener> ();
            services.AddTransient<IApplicationProcessorServiceFactory, ApplicationProcessorServiceFactory> ();
            services.AddPartner (Settings.Partner.Host, Settings.Partner.Port);
            services.AddLoanManagementService (Settings.LoanManagement.Host, Settings.LoanManagement.Port);
            services.AddProductService (Settings.Product.Host, Settings.Product.Port);
            services.AddCashflowService (Settings.Cashflow.Host, Settings.Cashflow.Port);
            services.AddMvc ().AddLendFoundryJsonOptions ();
        }

        public void Configure (IApplicationBuilder app, IHostingEnvironment env) {
            app.UseHealthCheck ();
            app.UseSwagger ();

            app.UseSwaggerUI (c => {
                c.SwaggerEndpoint ("/swagger/v1/swagger.json", "CA ApplicationProcessor Service");
            });
            app.UseCors (env);
            app.UseErrorHandling ();
            app.UseRequestLogging(new RequestLoggingMiddlewareOptions(new[] { "Password" }));             
            app.UseMvc ();
            app.UseApplicationProcessorListener ();
        }
    }
}