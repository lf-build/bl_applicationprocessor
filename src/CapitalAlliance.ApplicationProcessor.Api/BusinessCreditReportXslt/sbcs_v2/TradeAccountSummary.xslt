<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"               
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->


  <!--
  *********************************************
  * SBCSPaymentTotals template
  *********************************************
  -->
  <xsl:template match="prd:SBCSPaymentTotals">
  
     <table class="section dataTable" width="100%" border="0" cellspacing="0" cellpadding="0">
    
      <thead>
        <tr>
          <th colspan="11"><a class="report_section_title">SBCS Trade Account Summary</a></th>
        </tr>
      </thead>

      <!-- column headers -->
      <tr class="datahead">
        <td width="23%">Type</td>
        <td class="rightalign" width="4%">#</td>

        <td width="12%" class="rightalign">Balance</td>
        <td width="12%" class="rightalign">Recent<br />High<br />Credit</td>

        <td class="rightalign" width="6%">Cur</td>
        <td class="rightalign" width="6%">1-30</td>
        <td class="rightalign" width="6%">31-60</td>
        <td class="rightalign" width="6%">61-90</td>
        <td class="rightalign" width="7%">91-120</td>
        <td class="rightalign" width="6%">121+</td>
        <td width="12%">Comments</td>
      </tr>
      
      <!-- ContinuouslyReportedTradeLines template -->
      <xsl:apply-templates select="prd:ContinuouslyReportedTradeLines">
          <xsl:with-param name="type" select="'Continuously Reported'" />
      </xsl:apply-templates>
              
      <!-- NewlyReportedTradeLines template -->
      <xsl:apply-templates select="prd:NewlyReportedTradeLines">
          <xsl:with-param name="type" select="'Newly Reported'" />
      </xsl:apply-templates>

      <!-- CombinedTradeLines template -->
      <xsl:apply-templates select="prd:CombinedTradeLines">
          <xsl:with-param name="type" select="'Current Trade Lines Totals'" />
      </xsl:apply-templates>
      
      <!-- AdditionalTradeLines template -->
      <xsl:apply-templates select="prd:AdditionalTradeLines">
          <xsl:with-param name="type" select="'Additional Experiences'" />
      </xsl:apply-templates>

    </table>
  </xsl:template>
    
  
  <!--
  *********************************************
  * Trade Lines template
  *********************************************
  -->
  <xsl:template match="prd:NewlyReportedTradeLines | prd:ContinuouslyReportedTradeLines | prd:CombinedTradeLines  | prd:AdditionalTradeLines" xml:space="default">
    <xsl:param name="type" />
    
    <xsl:variable name="linesReported">
      <xsl:choose><xsl:when test="prd:NumberOfLines">                    
          <xsl:value-of select="number(prd:NumberOfLines)" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="0" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="recentHighCredit">
      <xsl:choose><xsl:when test="$linesReported = 0">                     
          <xsl:value-of select="'&#160;'" />
        </xsl:when><xsl:when test="prd:TotalHighCreditAmount and number(prd:TotalHighCreditAmount/prd:Amount) != 0">                    
          <xsl:value-of select="concat(prd:TotalHighCreditAmount/prd:Modifier/@code, format-number(prd:TotalHighCreditAmount/prd:Amount, '$###,###,##0'))" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="balance">
      <xsl:choose><xsl:when test="$linesReported = 0">                     
          <xsl:value-of select="'&#160;'" />
        </xsl:when><xsl:when test="prd:TotalAccountBalance and number(prd:TotalAccountBalance/prd:Amount) != 0">                    
          <xsl:value-of select="concat(prd:TotalAccountBalance/prd:Modifier/@code, format-number(prd:TotalAccountBalance/prd:Amount, '$###,###,##0'))" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="current">
      <xsl:choose><xsl:when test="prd:CurrentPercentage and number(prd:CurrentPercentage) != 0">                     
          <xsl:value-of select="format-number(prd:CurrentPercentage div 100, '##0%')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="DBT30">
      <xsl:choose><xsl:when test="prd:DBT30 and number(prd:DBT30) != 0">                     
          <xsl:value-of select="format-number(prd:DBT30 div 100, '##0%')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="DBT60">
      <xsl:choose><xsl:when test="prd:DBT60 and number(prd:DBT60) != 0">                     
          <xsl:value-of select="format-number(prd:DBT60 div 100, '##0%')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="DBT90">
      <xsl:choose><xsl:when test="prd:DBT90 and number(prd:DBT90) != 0">                     
          <xsl:value-of select="format-number(prd:DBT90 div 100, '##0%')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="DBT120">
      <xsl:choose><xsl:when test="prd:DBT120 and number(prd:DBT120) != 0">                     
          <xsl:value-of select="format-number(prd:DBT120 div 100, '##0%')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="DBT121Plus">
      <xsl:choose><xsl:when test="prd:DBT121Plus and number(prd:DBT121Plus) != 0">                     
          <xsl:value-of select="format-number(prd:DBT121Plus div 100, '##0%')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="comments">
      <xsl:choose><xsl:when test="$linesReported = 0">                     
          <xsl:value-of select="'&#160;'" />
        </xsl:when><xsl:when test="prd:DBT and number(prd:DBT) != 0">                     
          <xsl:value-of select="number(prd:DBT)" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="rowClass">
      <xsl:choose>
        <xsl:when test="name(.) = 'ContinuouslyReportedTradeLines'">
          <xsl:value-of select="'odd'" />
        </xsl:when>
        <xsl:when test="name(.) = 'AdditionalTradeLines'">
          <xsl:value-of select="'last'" />
        </xsl:when>
        <xsl:when test="name(.) = 'CombinedTradeLines'">
          <xsl:value-of select="'summary'" />
        </xsl:when>
      </xsl:choose>
    </xsl:variable>
    
    <tr class="{normalize-space($rowClass)}">
      <td><xsl:value-of select="$type" />
      </td>
      <td class="rightalign"><xsl:value-of select="$linesReported" />
      </td>

      <td class="rightalign"><xsl:value-of select="$balance" />
      </td>

      <td class="rightalign"><xsl:value-of select="$recentHighCredit" />
      </td>

      <td class="rightalign"><xsl:value-of select="$current" />
      </td>
      <td class="rightalign"><xsl:value-of select="$DBT30" />
      </td>
      <td class="rightalign"><xsl:value-of select="$DBT60" />
      </td>
      <td class="rightalign"><xsl:value-of select="$DBT90" />
      </td>
      <td class="rightalign"><xsl:value-of select="$DBT120" />
      </td>
      <td class="rightalign"><xsl:value-of select="$DBT121Plus" />
      </td>
      <td>DBT: <xsl:value-of select="$comments" />
      </td>
    </tr>

  </xsl:template>
  
</xsl:stylesheet>
