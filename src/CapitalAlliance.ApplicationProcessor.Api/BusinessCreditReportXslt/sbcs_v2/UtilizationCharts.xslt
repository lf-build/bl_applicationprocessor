<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"                
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->


  <!--
  *********************************************
  * SBCSKeyMetrics template
  *********************************************
  -->
  <xsl:template match="prd:SBCSKeyMetrics">

    <div id="div_sbcs_charts_xml" style="display:none;">
		<xml id="table_SBCSKeyMetrics_CurrentDate">
			<!-- Current Date for SBCSKeyMetrics -->
			<CurrentDate>
				<xsl:copy-of select="//prd:CurrentDate"/>
			</CurrentDate>
		</xml>

		<xml id="table_FAUT" style="display: none;">
			<!-- financialAccountUtilizationTrend -->
			<FinancialAccounts>
				<xsl:copy-of select="//prd:FinancialAccounts/prd:PercentageUsed"/><DoNotRemove></DoNotRemove>
			</FinancialAccounts>
		</xml>

      <xml id="table_RAUT">
        <!-- revolvingAccountUtilizationTrend -->
        <RevolvingAccounts>
          <xsl:copy-of select="//prd:RevolvingAccounts/prd:PercentageUsed"/><DoNotRemove></DoNotRemove>
        </RevolvingAccounts>
      </xml>

      <xml id="table_TAUT">
        <!-- totalAccountUtilizationTrend -->
        <AllAccountsTotal>
          <xsl:copy-of select="//prd:AllAccountsTotal/prd:PercentageUsed"/><DoNotRemove></DoNotRemove>
        </AllAccountsTotal>
      </xml>

      <xml id="table_ADT">
        <!-- averageDBTTrend -->
        <DBTHistory>
          <xsl:copy-of select="//prd:DBTHistory/prd:DBT"/><DoNotRemove></DoNotRemove>
        </DBTHistory>
      </xml>
    </div>



    <xsl:if test="prd:FinancialAccounts and sum(prd:FinancialAccounts/prd:PercentageUsed) &gt; 0">
      <!-- FinancialTrades template -->
      <xsl:apply-templates select="prd:FinancialTrades" />

      <xsl:if test="(prd:RevolvingAccounts and sum(prd:RevolvingAccounts/prd:PercentageUsed) &gt; 0) or (prd:AllAccountsTotal and sum(prd:AllAccountsTotal/prd:PercentageUsed) &gt; 0) or prd:PaymentStatus/prd:Status or prd:DBTHistory/prd:DBT">
        <div class="paragraphSpacer"><span><!-- DO NOT REMOVE THIS EMPTY TAG --></span></div>
      </xsl:if>
    </xsl:if>
    
    <xsl:if test="prd:RevolvingAccounts and sum(prd:RevolvingAccounts/prd:PercentageUsed) &gt; 0">
      <!-- RevolvingTrades template -->
      <xsl:apply-templates select="prd:RevolvingTrades" />

      <xsl:if test="(prd:AllAccountsTotal and sum(prd:AllAccountsTotal/prd:PercentageUsed) &gt; 0) or prd:PaymentStatus/prd:Status or prd:DBTHistory/prd:DBT">
        <div class="paragraphSpacer"><span><!-- DO NOT REMOVE THIS EMPTY TAG --></span></div>
      </xsl:if>
    </xsl:if>
    
    <xsl:if test="prd:AllAccountsTotal and sum(prd:AllAccountsTotal/prd:PercentageUsed) &gt; 0">
      <!-- AllTrades template -->
      <xsl:apply-templates select="prd:AllTrades" />

      <xsl:if test="prd:PaymentStatus/prd:Status or prd:DBTHistory/prd:DBT">
        <div class="paragraphSpacer"><span><!-- DO NOT REMOVE THIS EMPTY TAG --></span></div>
      </xsl:if>
    </xsl:if>
    
    <xsl:if test="prd:DBTHistory/prd:DBT">
      <!-- DBT template -->
      <xsl:call-template name="DBT36Months" />

      <xsl:if test="prd:PaymentStatus/prd:Status">
        <div class="paragraphSpacer"><span><!-- DO NOT REMOVE THIS EMPTY TAG --></span></div>
      </xsl:if>
    </xsl:if>
    
    <xsl:if test="prd:PaymentStatus/prd:Status">
      <!-- PaymentStatus template -->
      <xsl:call-template name="PaymentStatus36Months" />

    </xsl:if>
    
  </xsl:template>
    
    
  <!--
  *********************************************
  * FinancialTrades template
  *********************************************
  -->
  <xsl:template match="prd:FinancialTrades">
    <xsl:variable name="current">
      <xsl:value-of select="number(prd:TotalAccountPercentageUsed)" />
    </xsl:variable>

    <xsl:variable name="compare">
      <xsl:variable name="previous">
        <xsl:value-of select="number(../prd:FinancialAccounts[1]/prd:PercentageUsed)" />
      </xsl:variable>

      <xsl:choose><xsl:when test="$current &gt; $previous">
          <xsl:value-of select="'up'" />
        </xsl:when><xsl:when test="$current &lt; $previous">
          <xsl:value-of select="'down'" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'equal'" />
        </xsl:otherwise></xsl:choose>                
    </xsl:variable>

    <xsl:variable name="balance">
      <xsl:choose><xsl:when test="prd:TotalCurrentBalance">                     
          <xsl:value-of select="format-number(prd:TotalCurrentBalance, '$###,###,##0')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="limit">
      <xsl:choose><xsl:when test="prd:TotalCurrentCreditLimitOLoanAmount">                     
          <xsl:value-of select="format-number(prd:TotalCurrentCreditLimitOLoanAmount, '$###,###,##0')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <colgroup style="width:50%"/>
      <colgroup style="width:50%" />

      <thead>
        <tr class="subtitle">
          <th colspan="2">Financial Account Utilization Trend - Previous 36 Months</th>
        </tr>
      </thead>
      <tbody>
        <tr class="sbscTrendInfo">
          <td class="indent2">
            <div class="block label">Current Financial Account Utilization =&#160;</div>
            <div class="block valueBox">
              <div class="block value"><xsl:value-of select="normalize-space($current)" />%</div>
              <div class="block"><div><img src="../images/arrow_{normalize-space($compare)}.gif" /></div></div>
              <div style="clear:both;"><span><!-- DO NOT REMOVE THIS EMPTY TAG --></span></div>
            </div>
            <div style="clear:both;"><span><!-- DO NOT REMOVE THIS EMPTY TAG --></span></div>
          </td>
          
          <td>
            <div><span class="strong">Current Outstanding Balance = </span><xsl:value-of select="$balance" /></div>
            <div><span class="strong">Original Loan Amount/Limit = </span><xsl:value-of select="$limit" /></div>
          </td>
        </tr>
        
        <tr>
          <td colspan="2" style="padding-bottom:5px;">
            <div id="util_chart_FAUT" style="width: 685px; height: 140px;"></div>
          </td>
        </tr>
        
      </tbody>
    </table>
  </xsl:template>
    

  <!--
  *********************************************
  * RevolvingTrades template
  *********************************************
  -->
  <xsl:template match="prd:RevolvingTrades">
    <xsl:variable name="current">
      <xsl:value-of select="number(prd:CurrentPercentageUsed)" />
    </xsl:variable>

    <xsl:variable name="compare">
      <xsl:variable name="previous">
        <xsl:value-of select="number(../prd:RevolvingAccounts[1]/prd:PercentageUsed)" />
      </xsl:variable>

      <xsl:choose><xsl:when test="$current &gt; $previous">
          <xsl:value-of select="'up'" />
        </xsl:when><xsl:when test="$current &lt; $previous">
          <xsl:value-of select="'down'" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'equal'" />
        </xsl:otherwise></xsl:choose>                
    </xsl:variable>

    <xsl:variable name="balance">
      <xsl:choose><xsl:when test="prd:TotalCurrentBalance">                     
          <xsl:value-of select="format-number(prd:TotalCurrentBalance, '$###,###,##0')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="limit">
      <xsl:choose><xsl:when test="prd:TotalCurrentCreditLimitOrLoanAmount">                     
          <xsl:value-of select="format-number(prd:TotalCurrentCreditLimitOrLoanAmount, '$###,###,##0')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <colgroup style="width:50%"/>
      <colgroup style="width:50%" />

      <thead>
        <tr class="subtitle">
          <th colspan="2">Revolving Account Utilization Trend - Previous 36 Months</th>
        </tr>
      </thead>
      <tbody>
        <tr class="sbscTrendInfo">
          <td class="indent2">
            <div class="block label">Current Revolving Account Utilization =&#160;</div>
            <div class="block valueBox">
              <div class="block value"><xsl:value-of select="normalize-space($current)" />%</div>
              <div class="block"><img src="../images/arrow_{normalize-space($compare)}.gif" /></div>
              <div style="clear:both;"><span><!-- DO NOT REMOVE THIS EMPTY TAG --></span></div>
            </div>
            <div style="clear:both;"><span><!-- DO NOT REMOVE THIS EMPTY TAG --></span></div>
          </td>
          
          <td>
            <div><span class="strong">Current Outstanding Balance = </span><xsl:value-of select="$balance" /></div>
            <div><span class="strong">High Credit/Limit = </span><xsl:value-of select="$limit" /></div>
          </td>
        </tr>
        
        <tr>
          <td colspan="2" style="padding-bottom:5px;">
            <div id="util_chart_RAUT" style="width: 685px; height: 140px;"></div>
          </td>
        </tr>
        
      </tbody>
    </table>

  </xsl:template>
    
  
  <!--
  *********************************************
  * PaymentStatus36Months template
  *********************************************
  -->
  <xsl:template name="PaymentStatus36Months">
  
    <table width="100%" cellspacing="0" cellpadding="0">
      <thead>
        <tr class="subtitle">
          <th>Payment Performance - Previous 36 Months</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="box_padding">
            <div style="width:686px;margin:3px auto 10px; text-align:center;">            
              
                <xsl:apply-templates select="prd:PaymentStatus/prd:Status">
                  <xsl:with-param name="currentDate" select="prd:CurrentDate" />
                  <xsl:sort select="position()" data-type="number" order="descending"/>                      
                </xsl:apply-templates>

              <div style="clear:both;"><span><!-- DO NOT REMOVE THIS EMPTY TAG --></span></div>
            </div>
            
            <table width="100%" border="0" cellspacing="0" cellpadding="1">
              <tr>
                <td width="2%" valign="top">
                  <img src="../images/box_C.gif" /></td>
                <td width="18%" valign="top" align="left">
                  Current</td>
                  
                <td width="2%" valign="top">
                  <img src="../images/box_0.gif" /></td>
                <td width="18%" valign="top" align="left">
                  Current, zero balance</td>
                  
                <td width="2%" valign="top">
                  <img src="../images/box_1.gif" /></td>
                <td width="18%" valign="top" align="left">
                  1-30 days late</td>
                  
                <td width="2%" valign="top">
                  <img src="../images/box_2.gif" /></td>
                <td width="18%" valign="top" align="left">
                  31-60 days late</td>
                  
                <td width="2%" valign="top">
                  <img src="../images/box_3.gif" /></td>
                <td width="17%" valign="top" align="left">
                  61-90 days late</td>
                  
              </tr>
              
              <tr>
                <td valign="top">
                  <img src="../images/box_4.gif" /></td>
                <td valign="top" align="left">
                  91-120 days late</td>
                  
                <td valign="top">
                  <img src="../images/box_5.gif" /></td>
                <td valign="top" align="left">
                  121+ days late</td>
                  
                <td valign="top">
                  <img src="../images/box_B.gif" /></td>
                <td valign="top" align="left">
                  Bankruptcy</td>
                  
                <td valign="top">
                  <img src="../images/box_G.gif" /></td>
                <td valign="top" align="left">
                  Collection</td>
                  
                <td valign="top">
                  <img src="../images/box_H.gif" /></td>
                <td valign="top" align="left">
                  Foreclosure</td>
                  
              </tr>

              <tr>
                <td valign="top">
                  <img src="../images/box_J.gif" /></td>
                <td valign="top" align="left">
                  Voluntary Surrender</td>
                  
                <td valign="top">
                  <img src="../images/box_K.gif" /></td>
                <td valign="top" align="left">
                  Repossession</td>
                  
                <td valign="top">
                  <img src="../images/box_L.gif" /></td>
                <td valign="top" align="left">
                  Uncollected/Charge-off</td>
                  
                <td valign="top">
                  <img src="../images/box_-.gif" /></td>
                <td valign="top" align="left">
                  No data reported</td>
                  
                <td valign="top">
                  <img src="../images/box_.gif" /></td>
                <td valign="top" align="left">
                  No prior payment history</td>
                  
              </tr>              
            </table>
            
          </td>
        </tr>
      </tbody>
    </table>

  </xsl:template>


  <!--
  *********************************************
  * PaymentStatus template
  *********************************************
  -->
  <xsl:template match="prd:Status" xml:space="default">
      <xsl:param name="currentDate" select="''" />

    <xsl:variable name="currentYear">
      <xsl:value-of select="number(substring($currentDate, 1, 4))" />
    </xsl:variable>
        
    <xsl:variable name="currentMonth">
      <xsl:value-of select="number(substring($currentDate, 5, 2))" />
    </xsl:variable>

    <xsl:variable name="newDate">
      <xsl:choose><xsl:when test="$currentMonth - (last() - (position() - 1)) &gt; 0">
          <xsl:variable name="newMonth">
            <xsl:call-template name="FormatMonth">
              <xsl:with-param name="monthValue" select="$currentMonth - (last() - (position() - 1))" />
            </xsl:call-template>
          </xsl:variable>  

          <xsl:variable name="newYear">
            <xsl:value-of select="substring(normalize-space($currentYear), 3, 2)" />
          </xsl:variable>  

          <xsl:value-of select="concat(substring(normalize-space($newMonth), 1, 1), normalize-space($newYear))" />
        </xsl:when><xsl:when test="$currentMonth - (last() - (position() - 1)) = 0">
          <xsl:variable name="newMonth">
            <xsl:call-template name="FormatMonth">
              <xsl:with-param name="monthValue" select="$currentMonth - (last() - (position() - 1)) + 12" />
            </xsl:call-template>
          </xsl:variable>  

          <xsl:variable name="newYear">
            <xsl:value-of select="substring(($currentYear - 1), 3, 2)" />
          </xsl:variable>  

          <xsl:value-of select="concat(substring(normalize-space($newMonth), 1, 1), normalize-space($newYear))" />
        </xsl:when><xsl:otherwise>
          <xsl:variable name="monthDiff">
            <xsl:value-of select="($currentMonth - (last() - (position() - 1))) * (-1)" />
          </xsl:variable>  
          
          <xsl:variable name="newMonth">
            <xsl:call-template name="FormatMonth">
              <xsl:with-param name="monthValue" select="12 - ($monthDiff mod 12)" />
            </xsl:call-template>
          </xsl:variable>  

          <xsl:variable name="newYear">
            <xsl:value-of select="substring(($currentYear - (floor($monthDiff div 12) + 1)), 3, 2)" />
          </xsl:variable>  

          <xsl:value-of select="concat(substring(normalize-space($newMonth), 1, 1), normalize-space($newYear))" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <div class="block" style="width:19px;text-align:center;font-size:9px;float:left;"><img src="../images/box_{normalize-space(./@code)}.gif" /><br/><xsl:value-of select="$newDate" /></div>
  </xsl:template>  
  
  
  <!--
  *********************************************
  * AllTrades template
  *********************************************
  -->
  <xsl:template match="prd:AllTrades">
    <xsl:variable name="current">
      <xsl:value-of select="number(prd:TotalAccountPercentageUsed)" />
    </xsl:variable>

    <xsl:variable name="compare">
      <xsl:variable name="previous">
        <xsl:value-of select="number(../prd:AllAccountsTotal[1]/prd:PercentageUsed)" />
      </xsl:variable>

      <xsl:choose><xsl:when test="$current &gt; $previous">
          <xsl:value-of select="'up'" />
        </xsl:when><xsl:when test="$current &lt; $previous">
          <xsl:value-of select="'down'" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'equal'" />
        </xsl:otherwise></xsl:choose>                
    </xsl:variable>

    <xsl:variable name="balance">
      <xsl:choose><xsl:when test="prd:TotalCurrentBalance">                     
          <xsl:value-of select="format-number(prd:TotalCurrentBalance, '$###,###,##0')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="limit">
      <xsl:choose><xsl:when test="prd:TotalCurrentCreditLimitOrLoanAmount">                     
          <xsl:value-of select="format-number(prd:TotalCurrentCreditLimitOrLoanAmount, '$###,###,##0')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <colgroup style="width:50%"/>
      <colgroup style="width:50%" />

      <thead>
        <tr class="subtitle">
          <th colspan="2">Total Account Utilization Trend - Previous 36 Months</th>
        </tr>
      </thead>
      <tbody>
        <tr class="sbscTrendInfo">
          <td class="indent2">
            <div class="block label">Current Total Credit Utilization =&#160;</div>
            <div class="block valueBox">
              <div class="block value"><xsl:value-of select="normalize-space($current)" />%</div>
              <div class="block"><img src="../images/arrow_{normalize-space($compare)}.gif" /></div>
              <div style="clear:both;"><span><!-- DO NOT REMOVE THIS EMPTY TAG --></span></div>
            </div>
            <div style="clear:both;"><span><!-- DO NOT REMOVE THIS EMPTY TAG --></span></div>
          </td>
          
          <td>
            <div><span class="strong">Current Outstanding Balance = </span><xsl:value-of select="$balance" /></div>
            <div><span class="strong">Original Loan Amount/Limit = </span><xsl:value-of select="$limit" /></div>
          </td>
        </tr>
        
        <tr>
          <td colspan="2" style="padding-bottom:5px;">
           <div id="util_chart_TAUT" style="width: 685px; height: 140px;"></div>
          </td>
        </tr>
        
      </tbody>
    </table>
  </xsl:template>


  <!--
  *********************************************
  * DBT36Months template
  *********************************************
  -->
  <xsl:template name="DBT36Months">
  
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <thead>
        <tr class="subtitle">
          <th colspan="2">Average DBT Trend (Non-financial Accounts) - Previous 36 Months</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td colspan="2" style="padding:6px 0 5px;">
            <div id="util_chart_ADT" style="width: 685px; height: 140px;"></div>
          </td>
        </tr>
        
      </tbody>
    </table>
  </xsl:template>
  
  
    
</xsl:stylesheet>
