<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  
  xmlns:prd="http://www.experian.com/ARFResponse">

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />
  
  
  <!--
  *********************************************
  * Global variables and parameters
  *********************************************
  -->
  <xsl:variable name="baseProduct">
    <xsl:value-of select="'SBCS'" />
  </xsl:variable>

  <xsl:variable name="product">
    <xsl:choose>
      <xsl:when test="//prd:CreditProfile">
        <xsl:value-of select="'SBCSBOP'" />
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="$baseProduct" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="reportTitle">
    <xsl:value-of select="'SBCS'" />
  </xsl:variable>
  

  <!-- This represents name of the target in search.  It could be a business name or an owner name.
       This name will be used as part of the report title and PDF footer -->
  <xsl:variable name="searchName">
   <xsl:choose>
    <xsl:when test="//prd:ExpandedBusinessNameAndAddress/prd:BusinessName">
     <xsl:value-of select="normalize-space(//prd:ExpandedBusinessNameAndAddress/prd:BusinessName)" />
    </xsl:when>
    <xsl:otherwise>
     <xsl:choose>
      <xsl:when
       test="//prd:ExpandedBusinessNameAndAddress/prd:LegalName/prd:LegalBusinessName">
       <xsl:value-of
        select="normalize-space(//prd:ExpandedBusinessNameAndAddress/prd:LegalName/prd:LegalBusinessName)" />
      </xsl:when>
      <xsl:otherwise>
       <xsl:value-of select="''" />
      </xsl:otherwise>
     </xsl:choose>
    </xsl:otherwise>
   </xsl:choose>
  </xsl:variable>
  
  <xsl:variable name="FusionChartHeight">
    <xsl:value-of select="'300px'" />
  </xsl:variable>

  <xsl:param name="financialChartPath" select="'../images/default_line_chart.gif'" />
  <xsl:param name="revolvingChartPath" select="'../images/default_line_chart.gif'" />
  <xsl:param name="totalChartPath" select="'../images/default_line_chart.gif'" />
  <xsl:param name="dbt36MChartPath" select="'../images/default_line_chart.gif'" />
  <xsl:param name="dbt7MChartPath" select="'../images/default_bar_chart.gif'" />
  <xsl:param name="dbt5QChartPath" select="'../images/default_bar_chart.gif'" />
  
  


  <!--
  *********************************************
  * include template
  *********************************************
  -->
    <xsl:include href="../common/ReportHeader.xslt"/>
    <xsl:include href="../common/Util.xslt"/>
    <xsl:include href="../common/case.xslt"/>
    <xsl:include href="../common/BackToTop.xslt"/>
    <xsl:include href="../common/ErrorProcessing.xslt"/>
    <xsl:include href="../common/ReportFooter.xslt"/>
    <xsl:include href="../ownerprofile/CreditProfile.xslt" />
    
    <xsl:include href="SBCSProfile.xslt"/>

  <!--
  *********************************************
  * Initial template
  *********************************************
  --> 
  <xsl:template match="/">
    <xsl:call-template name="MainHTML">
      <xsl:with-param name="productTitle" select="$reportTitle" />
    </xsl:call-template>
  </xsl:template>
  

  <!--
  *********************************************
  * Response template
  *********************************************
  -->
  <xsl:template match="prd:Products">
    <xsl:variable name="isStandalone">
      <xsl:choose>
          <xsl:when test="//prd:CreditProfile">
              <xsl:value-of select="0" />
          </xsl:when>
  
          <xsl:otherwise>
              <xsl:value-of select="1" />
          </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- header template -->
    <xsl:apply-templates select="prd:SmallBusinessCreditShare" mode="header" />

    <xsl:apply-templates select="prd:SmallBusinessCreditShare" mode="SBCS">
      <xsl:with-param name="standalone" select="$isStandalone" />
    </xsl:apply-templates>

    <!-- BOP template -->
    <xsl:if test="prd:CreditProfile"> 
        <xsl:call-template name="BackToTop" />
        <xsl:apply-templates select="prd:CreditProfile">
            <xsl:with-param name="standalone" select="0" /> 
        </xsl:apply-templates>
    </xsl:if>

    <xsl:apply-templates select="prd:SmallBusinessCreditShare" mode="footer" />


  </xsl:template>


  <!--
  *********************************************
  * this template overrides the built-in rule
  * do nothing with stuff I'm not interested in
  *********************************************
  -->
  <xsl:template match="text()|@*"/>

</xsl:stylesheet>