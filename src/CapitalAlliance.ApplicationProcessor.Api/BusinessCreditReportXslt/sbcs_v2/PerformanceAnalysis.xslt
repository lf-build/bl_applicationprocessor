<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"                
                xmlns:prd="http://www.experian.com/ARFResponse">

  <!--
  *********************************************
  * Output method
  *********************************************
  -->



  <!--
  *********************************************
  * PerformanceAnalysis template
  *********************************************
  -->
  <xsl:template name="PerformanceAnalysis">

    <xsl:variable name="avg5QBalance">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveElements/prd:AverageBalance5Quarters">                    
          <xsl:value-of select="format-number(prd:SBCSExecutiveElements/prd:AverageBalance5Quarters, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
  
    <xsl:variable name="highestCredit">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveElements/prd:SingleLineHighCredit">                    
          <xsl:value-of select="format-number(prd:SBCSExecutiveElements/prd:SingleLineHighCredit, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="balanceRange">

      <xsl:variable name="lowBalance">
        <xsl:choose>                  
          <xsl:when test="prd:SBCSExecutiveElements/prd:LowBalance6Months">                    
            <xsl:value-of select="format-number(prd:SBCSExecutiveElements/prd:LowBalance6Months, '$###,###,##0')" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="'$0'" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>

      <xsl:variable name="highBalance">
        <xsl:choose>                  
          <xsl:when test="prd:SBCSExecutiveElements/prd:HighBalance6Months">                    
            <xsl:value-of select="format-number(prd:SBCSExecutiveElements/prd:HighBalance6Months, '$###,###,##0')" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="'$0'" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>

      <xsl:value-of select="concat($lowBalance, ' - ', $highBalance)" />
    </xsl:variable>

    <xsl:variable name="inquiries6M">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:InquiriesPrevious6Months">                    
          <xsl:value-of select="number(prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:InquiriesPrevious6Months)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="inquiries12M">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:InquiriesPrevious12Months">                    
          <xsl:value-of select="number(prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:InquiriesPrevious12Months)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

  
    <xsl:variable name="paymentIndicator">
      <xsl:choose>                  
        <xsl:when test="normalize-space(prd:SBCSExecutiveSummary/prd:PaymentTrendIndicator/@code) != ''">                    
          <xsl:value-of select="prd:SBCSExecutiveSummary/prd:PaymentTrendIndicator" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="paymentComparison">
      <xsl:choose>                  
        <xsl:when test="normalize-space(prd:SBCSExecutiveSummary/prd:IndustryPaymentComparison/@code) != ''">                    
          <xsl:value-of select="concat('Has paid ', prd:SBCSExecutiveSummary/prd:IndustryPaymentComparison, ' similar firms')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="commonTerms">
      <xsl:variable name="terms1">
        <xsl:choose>                  
          <xsl:when test="prd:SBCSExecutiveSummary/prd:CommonTerms">                     
            <xsl:value-of select="prd:SBCSExecutiveSummary/prd:CommonTerms" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>

      <xsl:variable name="terms2">
        <xsl:choose>                  
          <xsl:when test="prd:SBCSExecutiveSummary/prd:CommonTerms2">                     
            <xsl:value-of select="prd:SBCSExecutiveSummary/prd:CommonTerms2" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>

      <xsl:variable name="terms3">
        <xsl:choose>                  
          <xsl:when test="prd:SBCSExecutiveSummary/prd:CommonTerms3">                     
            <xsl:value-of select="prd:SBCSExecutiveSummary/prd:CommonTerms3" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>

      <xsl:choose>                  
        <xsl:when test="normalize-space($terms1) != '' and normalize-space($terms2) != '' and normalize-space($terms3) != ''">                     
          <xsl:value-of select="concat($terms1, ', ', $terms2, ', and ', $terms3)" />
        </xsl:when>

        <xsl:when test="normalize-space($terms1) != '' and normalize-space($terms2) != ''">
          <xsl:value-of select="concat($terms1, ' and ', $terms2)" />
        </xsl:when>
        
        <xsl:when test="normalize-space($terms1) != '' and normalize-space($terms3) != ''">
          <xsl:value-of select="concat($terms1, ' and ', $terms3)" />
        </xsl:when>
        
        <xsl:when test="normalize-space($terms2) != '' and normalize-space($terms3) != ''">
          <xsl:value-of select="concat($terms2, ' and ', $terms3)" />
        </xsl:when>
        
        <xsl:otherwise>
          <xsl:value-of select="concat($terms1, $terms2, $terms3)" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <table class="section" width="100%" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <th>Performance Analysis</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="padding:6px;">
            <table width="100%" border="0" cellspacing="0" cellpadding="1">    

              <tr>  
                <td colspan="2">
                  <b>Performance Metrics:</b></td>
              </tr>

              <tr>
                <td>
                  Average balance previous 5 quarters:</td>
                <td align="right" nowrap="nowrap"><b><xsl:value-of select="$avg5QBalance" /></b></td>
              </tr>
              
              <tr>
                <td>
                  Highest credit amount extended:</td>
                <td align="right" nowrap="nowrap"><b><xsl:value-of select="$highestCredit" /></b></td>
              </tr>            
              
              <tr>
                <td colspan="2">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="40%">
                        6 month balance range:</td>
                      <td width="60%" align="right" nowrap="nowrap"><b><xsl:value-of select="$balanceRange" /></b></td>
                    </tr>
                  </table>
                </td>
              </tr>
            
              <tr>
                <td>
                  Inquiries previous 6 months:</td>
                <td align="right"><b><xsl:value-of select="$inquiries6M" /></b></td>
              </tr>
                          
              <tr>
                <td>
                Inquiries previous 12 months:</td>
                <td align="right"><b><xsl:value-of select="$inquiries12M" /></b></td>
              </tr>

              <tr>
                <td colspan="2" class="paragraphSpacer"></td>
              </tr>              

              <tr>  
                <td colspan="2">
                  <b>Payment Trend Indication:</b><br />
                  <xsl:value-of select="$paymentIndicator" />
                </td>
              </tr>
              
              <tr>
                <td colspan="2" class="paragraphSpacer"></td>
              </tr>              

              <tr>  
                <td colspan="2">
                  <b>Industry Payment Comparison:</b><br />
                  <xsl:value-of select="$paymentComparison" />
                </td>
              </tr>
            
              <tr>
                <td colspan="2" class="paragraphSpacer"></td>
              </tr>              

              <tr>  
                <td colspan="2">
                  <b>Most Frequent Industry Purchasing Terms:</b><br />
                  <xsl:value-of select="$commonTerms" />
                </td>
              </tr>
                    
            </table>
          </td>
        </tr>
      </tbody>
    </table>

  </xsl:template>
      
</xsl:stylesheet>
