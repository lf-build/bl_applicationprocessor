<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->


  <!--
  *********************************************
  * MonthlyPaymentTrends template
  *********************************************
  -->
  <xsl:template name="MonthlyPaymentTrends">

    <xsl:variable name="sic">
      <xsl:choose>
        <xsl:when test="prd:SBCSBusinessNameAndAddress/prd:SIC[prd:SICIndicator = 'P']">
          <xsl:call-template name="convertcase">
            <xsl:with-param name="toconvert" select="concat(prd:SBCSBusinessNameAndAddress/prd:SIC[prd:SICIndicator = 'P']/prd:SICCode, ' INDUSTRY SIC: ', substring(prd:SBCSBusinessNameAndAddress/prd:SIC[prd:SICIndicator = 'P']/prd:SICCode/@code, 1, 6))" />
            <xsl:with-param name="conversion" select="'upper'" />
          </xsl:call-template>
        </xsl:when>

        <xsl:when test="prd:SBCSBusinessNameAndAddress/prd:SIC[prd:SICIndicator = 'A']">
          <xsl:call-template name="convertcase">
            <xsl:with-param name="toconvert" select="concat(prd:SBCSBusinessNameAndAddress/prd:SIC[prd:SICIndicator = 'A']/prd:SICCode, ' INDUSTRY SIC: ', substring(prd:SBCSBusinessNameAndAddress/prd:SIC[prd:SICIndicator = 'A']/prd:SICCode/@code, 1, 6))" />
            <xsl:with-param name="conversion" select="'upper'" />
          </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <table class="section dataTable" width="100%" border="0" cellspacing="0" cellpadding="0">
      <thead>
      <tr>
        <th colspan="11">
          <a class="report_section_title">Monthly Payment Trends</a>
        </th>
      </tr>
      <tr class="subtitle">
          <th colspan="5">
            <div class="doubleSubtitle">
              <div>Payment Trends Analysis</div>
                 <xsl:if test="normalize-space($sic) != ''">
                  <div><xsl:value-of select="$sic" /></div>
                </xsl:if> 
            </div>
          </th>
          <th colspan="6">
            <div class="doubleSubtitle">
              <div>Account Status</div>
              <div>Days Beyond Terms</div>
            </div>
          </th>
      </tr>

      </thead>
      <tbody>

      <tr class="datahead">
        <td width="13%">Date Reported</td>
        <td width="16%" colspan="2" align="center">
          <div>Industry</div>
          <div>
            <div style="width:50%;float:left;text-align:center;">Cur</div>
            <div style="width:50%;float:left;text-align:center;">DBT</div>
          </div>
          <div style="clear:both"></div>
        </td>
        <td width="8%" class="rightalign">Business<br />DBT</td>
        <td width="15%" class="rightalign">Balance</td>
        <td width="8%" class="rightalign">Cur</td>
        <td width="8%" class="rightalign">1-30</td>
        <td width="8%" class="rightalign">31-60</td>
        <td width="8%" class="rightalign">61-90</td>
        <td width="8%" class="rightalign">91-120</td>
        <td width="8%" class="rightalign">121+</td>
      </tr>

      <xsl:apply-templates select="prd:SBCSPaymentTrends/prd:CurrentMonth" mode="mpt" />
      <xsl:apply-templates select="prd:SBCSPaymentTrends/prd:PriorMonth" mode="mpt" />
    </tbody>
    </table>

  </xsl:template>


  <!--
  *********************************************
  * CurrentMonth | PriorMonth template
  *********************************************
  -->
  <xsl:template match="prd:CurrentMonth | prd:PriorMonth" mode="mpt">
    <xsl:variable name="position">
      <xsl:value-of select="position()" />
    </xsl:variable>

    <xsl:variable name="tmpMonth">
  <xsl:value-of select="number(substring(prd:Date, 5, 2))" />
    </xsl:variable>

    <xsl:variable name="tmpYear">
  <xsl:choose><xsl:when test="number($tmpMonth) = 0">
      <xsl:value-of select="number(substring(prd:Date, 1, 4)) - 1" />
    </xsl:when><xsl:otherwise>
      <xsl:value-of select="number(substring(prd:Date, 1, 4))" />
    </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="newMonth">
  <xsl:choose><xsl:when test="number($tmpMonth) = 0">
      <xsl:value-of select="12" />
    </xsl:when><xsl:otherwise>
      <xsl:value-of select="$tmpMonth" />
    </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="dateReported">
      <xsl:choose><xsl:when test="prd:Date">
          <xsl:variable name="month">
            <xsl:call-template name="FormatMonth">
              <xsl:with-param name="monthValue" select="number($newMonth)" />
              <xsl:with-param name="upperCase" select="true()" />
            </xsl:call-template>
          </xsl:variable>

          <xsl:value-of select="concat(normalize-space($month), substring(normalize-space($tmpYear), 3, 2))" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="industryCurrent">
      <xsl:choose><xsl:when test="name() = 'CurrentMonth'">
          <xsl:value-of select="'N/A'" />
        </xsl:when><xsl:when test="$position - 1 = 0 and ../../prd:IndustryPaymentTrends/prd:CurrentMonth/prd:CurrentPercentage">
          <xsl:value-of select="format-number(../../prd:IndustryPaymentTrends/prd:CurrentMonth/prd:CurrentPercentage div 100, '##0%')" />
        </xsl:when><xsl:when test="$position - 1 &gt; 0 and ../../prd:IndustryPaymentTrends/prd:PriorMonth[number($position - 1)]/prd:CurrentPercentage">
          <xsl:value-of select="format-number(../../prd:IndustryPaymentTrends/prd:PriorMonth[number($position - 1)]/prd:CurrentPercentage div 100, '##0%')" />
        </xsl:when></xsl:choose>
    </xsl:variable>

    <xsl:variable name="industryDBT">
      <xsl:choose><xsl:when test="name() = 'CurrentMonth'">
          <xsl:value-of select="'N/A'" />
        </xsl:when><xsl:when test="$position - 1 = 0 and ../../prd:IndustryPaymentTrends/prd:CurrentMonth/prd:DBT">
          <xsl:value-of select="number(../../prd:IndustryPaymentTrends/prd:CurrentMonth/prd:DBT)" />
        </xsl:when><xsl:when test="$position - 1 &gt; 0 and ../../prd:IndustryPaymentTrends/prd:PriorMonth[number($position - 1)]/prd:DBT">
          <xsl:value-of select="number(../../prd:IndustryPaymentTrends/prd:PriorMonth[number($position - 1)]/prd:DBT)" />
        </xsl:when></xsl:choose>
    </xsl:variable>

    <xsl:variable name="businessDBT">
      <xsl:choose><xsl:when test="prd:DBT">
          <xsl:value-of select="number(prd:DBT)" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="businessCurrent">
      <xsl:choose><xsl:when test="prd:CurrentPercentage and number(prd:CurrentPercentage) != 0">
          <xsl:value-of select="format-number(prd:CurrentPercentage div 100, '##0%')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="balance">
      <xsl:choose><xsl:when test="prd:TotalAccountBalance">
          <xsl:value-of select="concat(prd:TotalAccountBalance/prd:Modifier/@code, format-number(prd:TotalAccountBalance/prd:Amount, '$###,###,##0'))" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="DBT30">
      <xsl:choose><xsl:when test="prd:DBT30 and number(prd:DBT30) != 0">
          <xsl:value-of select="format-number(prd:DBT30 div 100, '##0%')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="DBT60">
      <xsl:choose><xsl:when test="prd:DBT60 and number(prd:DBT60) != 0">
          <xsl:value-of select="format-number(prd:DBT60 div 100, '##0%')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="DBT90">
      <xsl:choose><xsl:when test="prd:DBT90 and number(prd:DBT90) != 0">
          <xsl:value-of select="format-number(prd:DBT90 div 100, '##0%')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="DBT120">
      <xsl:choose><xsl:when test="prd:DBT120 and number(prd:DBT120) != 0">
          <xsl:value-of select="format-number(prd:DBT120 div 100, '##0%')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="DBT121Plus">
      <xsl:choose><xsl:when test="prd:DBT121Plus and number(prd:DBT121Plus) != 0">
          <xsl:value-of select="format-number(prd:DBT121Plus div 100, '##0%')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="rowClass">
      <xsl:choose>                  
        <xsl:when test="name() = 'PriorMonth' and position() = last()"><xsl:value-of select="'last'"></xsl:value-of></xsl:when>
        <xsl:when test="position() mod 2=0 or name() = 'CurrentMonth'"><xsl:value-of select="'odd'"></xsl:value-of></xsl:when>
        <xsl:when test="position() mod 2=1"><xsl:value-of select="'even'"></xsl:value-of></xsl:when>
      </xsl:choose>    
    </xsl:variable>

    <tr class="{normalize-space($rowClass)}">
      <td><xsl:value-of select="$dateReported" /></td>
      <td width="8%" class="rightalign" style="padding-right:17px;"><xsl:value-of select="$industryCurrent" /></td>
      <td width="8%" class="rightalign" style="padding-right:23px;"><xsl:value-of select="$industryDBT" /></td>
      <td class="rightalign" style="padding-right:13px;"><xsl:value-of select="$businessDBT" /></td>
      <td class="rightalign"><xsl:value-of select="$balance" /></td>
      <td class="rightalign"><xsl:value-of select="$businessCurrent" /></td>
      <td class="rightalign"><xsl:value-of select="$DBT30" /></td>
      <td class="rightalign"><xsl:value-of select="$DBT60" /></td>
      <td class="rightalign"><xsl:value-of select="$DBT90" /></td>
      <td class="rightalign"><xsl:value-of select="$DBT120" /></td>
      <td class="rightalign"><xsl:value-of select="$DBT121Plus" /></td>
    </tr>

  </xsl:template>

</xsl:stylesheet>
