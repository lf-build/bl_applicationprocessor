<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->


  <!--
  *********************************************
  * ExecutiveSummary template
  *********************************************
  -->
  <xsl:template name="ExecutiveSummary">

    <table class="section" width="100%" cellspacing="0" cellpadding="0">
      <colgroup style="width:50%"/>
      <colgroup style="width:50%" />
      <thead>
        <tr>
          <th colspan="2"><a name="ExecutiveSummary"><a class="report_section_title">SBCS Executive Summary</a></a></th>
        </tr>
        <tr class="subtitle">
          <th>Public Record</th>
          <th>Collections and Derogatory Items</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="box_padding">
            <!-- Public Record column -->
            <xsl:call-template name="PublicRecord" />
          </td>
          <td class="box_padding">
            <!-- Collections Derog column -->
            <xsl:call-template name="CollectionsDerogatory" />
          </td>
        </tr>
      </tbody>
    </table>

    <xsl:if test="prd:SBCSExecutiveElements/prd:UCCDerogatoryCount and number(prd:SBCSExecutiveElements/prd:UCCDerogatoryCount) &gt; 0">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="footnote">
            <i>
              ** Cautionary UCC Filings include one or more of the following collateral:<br/>
              Accounts, Accounts Receivables, Contract Rights, Hereafter Acquired Property, Inventory, Leases, Notes Receivable or Proceeds. 
            </i>
          </td>
        </tr>
      </table>
    </xsl:if>    
        

  </xsl:template>

</xsl:stylesheet>