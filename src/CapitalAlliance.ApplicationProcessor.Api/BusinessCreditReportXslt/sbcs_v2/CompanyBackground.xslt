<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"                
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->


  <!--
  *********************************************
  * CompanyBackground template
  *********************************************
  -->
  <xsl:template name="CompanyBackground">
    
    <table class="section" width="100%" border="0" cellspacing="0" cellpadding="0">

      <thead>
        <tr>
          <th><a class="report_section_title">Company Background Information</a></th>
        </tr>
      </thead>
      
      <xsl:if test="prd:CorporateInformation or prd:CorporateOwnerInformation">
        <tr class="subtitle">
          <th>Corporate Registration</th>
        </tr>
        <xsl:call-template name="CorporateRegistration" />

        <xsl:if test="prd:CorporateLinkageSummary or prd:DemographicInformation or prd:CorporateLinkageNameAndAddress or prd:KeyPersonnelExecutiveInformation">
          <tr><td class="subsectionSpacer"></td></tr>
        </xsl:if>
      </xsl:if>
  
      <xsl:if test="prd:CorporateLinkageSummary or prd:DemographicInformation or prd:CorporateLinkageNameAndAddress or prd:KeyPersonnelExecutiveInformation">
        <!-- AdditionalCompanyBackground -->
        <tr class="subtitle">
          <th>Additional Company Background Information</th>
        </tr>
        <xsl:call-template name="AdditionalCompanyBackground" />
      </xsl:if>

    </table>
    
  </xsl:template>


  <!--
  *********************************************
  * CorporateRegistration template
  *********************************************
  -->
  <xsl:template name="CorporateRegistration">
  
    <xsl:variable name="stateName">
      <xsl:choose>		              
        <xsl:when test="prd:CorporateInformation/prd:StateOfOrigin and normalize-space(prd:CorporateInformation/prd:StateOfOrigin)!=''">		    		   		   
			  <xsl:call-template name="TranslateState">
			    <xsl:with-param name="value" select="prd:CorporateInformation/prd:StateOfOrigin" />
			    <xsl:with-param name="upperCase" select="true()" />
			  </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="profileDate">
      <xsl:variable name="tempDate">  
        <xsl:choose>
          <xsl:when test="//prd:ExpandedBusinessNameAndAddress/prd:ProfileDate">
            <xsl:value-of select="//prd:ExpandedBusinessNameAndAddress/prd:ProfileDate" />
          </xsl:when>
          <xsl:when test="//prd:SBCSBusinessNameAndAddress/prd:ProfileDate">
            <xsl:value-of select="//prd:SBCSBusinessNameAndAddress/prd:ProfileDate" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="//prd:BusinessNameAndAddress/prd:ProfileDate" />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>  
      <xsl:call-template name="FormatDate">
        <xsl:with-param name="pattern" select="'mo/dt/year'" />
        <xsl:with-param name="value" select="normalize-space($tempDate)" />
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:variable name="stateStatement">
      <xsl:choose>		              
        <xsl:when test="normalize-space($stateName) = 'CALIFORNIA'">		    		   		   
          <xsl:value-of select="'THIS DATA IS FOR INFORMATION PURPOSES ONLY. CERTIFICATION CAN ONLY BE OBTAINED THROUGH THE SACRAMENTO OFFICE OF THE CALIFORNIA SECRETARY OF STATE.'" />
        </xsl:when>

        <xsl:when test="normalize-space($stateName) = 'NORTH CAROLINA'">		    		   		   
          <xsl:value-of select="'THIS DATA IS FOR INFORMATION PURPOSES ONLY. CERTIFICATION CAN ONLY BE OBTAINED THROUGH THE NORTH CAROLINA DEPARTMENT OF THE SECRETARY OF STATE.'" />
        </xsl:when>

        <xsl:when test="normalize-space($stateName) = 'PENNSYLVANIA'">
          <xsl:value-of select="'THE FOLLOWING DATA IS FOR INFORMATIONAL PURPOSES ONLY AND IS NOT AN OFFICIAL RECORD. CERTIFIED COPIES MAY BE OBTAINED FROM THE PENNSYLVANIA DEPARTMENT OF STATE.'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="concat('THE FOLLOWING INFORMATION WAS PROVIDED BY THE STATE OF ', $stateName, '.')" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <tr>
      <td class="box_padding">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="1">
                <xsl:if test="normalize-space($stateName)!=''">
                <tr>
                  <td align="left" style="padding-bottom:2px;">
                    <xsl:value-of select="$stateStatement" />
                    THE DATA IS CURRENT AS OF <xsl:value-of select="normalize-space($profileDate)" />.
                  </td>
                </tr>
                </xsl:if>

                <!-- rows of CorporateInformation -->
                <xsl:apply-templates select="prd:CorporateInformation" />

                <xsl:if test="prd:CorporateInformation and prd:CorporateOwnerInformation">
                  <tr>
                    <td class="paragraphSpacer"></td>
                  </tr>
                </xsl:if>

                <!-- rows of CorporateOwnerInformation -->
                <xsl:apply-templates select="prd:CorporateOwnerInformation" />

              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>

  </xsl:template>


  <!--
  *********************************************
  * CorporateInformation template
  *********************************************
  -->
  <xsl:template match="prd:CorporateInformation">

    <xsl:variable name="state">
      <xsl:choose>		              
        <xsl:when test="prd:StateOfOrigin">		    		   		   
          <xsl:value-of select="prd:StateOfOrigin" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="dateIncorporated">
      <xsl:choose>		              
        <xsl:when test="prd:IncorporatedDate and number(prd:IncorporatedDate) != 0">		    		   		   
    		   <xsl:call-template name="FormatDate">
    		     <xsl:with-param name="pattern" select="'mo/dt/year'" />
    		     <xsl:with-param name="value" select="prd:IncorporatedDate" />
    		   </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="statusFlag">
      <xsl:choose>		              
        <xsl:when test="prd:StatusFlag and normalize-space(prd:StatusFlag/@code) != ''">		    		   		   
          <xsl:value-of select="prd:StatusFlag" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="statusDescription">
      <xsl:choose>		              
        <xsl:when test="prd:StatusDescription">		    		   		   
          <xsl:value-of select="prd:StatusDescription" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="currentStatus">
      <xsl:choose>		              
        <xsl:when test="normalize-space($statusFlag) != '' and normalize-space($statusDescription) != ''">		    		   		   
          <xsl:value-of select="concat($statusFlag, ' - ', $statusDescription)" />
        </xsl:when>

        <xsl:when test="normalize-space($statusFlag) != ''">		    		   		   
          <xsl:value-of select="$statusFlag" />
        </xsl:when>

        <xsl:when test="normalize-space($statusDescription) != ''">		    		   		   
          <xsl:value-of select="$statusDescription" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="section1Presents">
      <xsl:choose>		              
        <xsl:when test="normalize-space($state) != '' or normalize-space($dateIncorporated) != '' or normalize-space($currentStatus) != ''">		    		   		   
          <xsl:value-of select="1" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="0" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="profileFlag">
      <xsl:choose>		              
        <xsl:when test="prd:ProfitFlag and normalize-space(prd:ProfitFlag/@code) != ''">		    		   		   
          <xsl:value-of select="prd:ProfitFlag" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="businessTypeCode">
      <xsl:choose>		              
        <xsl:when test="prd:BusinessType and normalize-space(prd:BusinessType/@code) != ''">		    		   		   
    		   <xsl:call-template name="TranslateBusinessType">
    		     <xsl:with-param name="value" select="normalize-space(prd:BusinessType/@code)" />
    		   </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="businessType">
      <xsl:choose>		              
        <xsl:when test="normalize-space($profileFlag) != '' and normalize-space($businessTypeCode) != ''">		    		   		   
          <xsl:value-of select="concat($businessTypeCode, ' - ', $profileFlag)" />
        </xsl:when>

        <xsl:when test="normalize-space($profileFlag) != ''">		    		   		   
          <xsl:value-of select="$profileFlag" />
        </xsl:when>

        <xsl:when test="normalize-space($businessTypeCode) != ''">		    		   		   
          <xsl:value-of select="$businessTypeCode" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="dbaName">
      <xsl:choose>		              
        <xsl:when test="prd:DBAName">		    		   		   
          <xsl:value-of select="prd:DBAName" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="charterNumber">
      <xsl:choose>		              
        <xsl:when test="prd:CharterNumber">		    		   		   
          <xsl:value-of select="prd:CharterNumber" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="federalTaxID">
      <xsl:choose>		              
        <xsl:when test="prd:FederalTaxID">		    		   		   
          <xsl:value-of select="concat(substring(prd:FederalTaxID, 1, 2), '-', substring(prd:FederalTaxID, 3, 7))" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="stateTaxID">
      <xsl:choose>		              
        <xsl:when test="prd:StateTaxID">		    		   		   
          <xsl:value-of select="prd:StateTaxID" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="section2Presents">
      <xsl:choose>		              
        <xsl:when test="normalize-space($businessType) != '' or normalize-space($dbaName) != '' or normalize-space($charterNumber) != '' or normalize-space($federalTaxID) != '' or normalize-space($stateTaxID) != ''">		    		   		   
          <xsl:value-of select="1" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="0" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="agentName">
      <xsl:choose>		              
        <xsl:when test="prd:AgentInformation/prd:Name">		    		   		   
          <xsl:value-of select="prd:AgentInformation/prd:Name" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="agentAddress">
      
      <xsl:variable name="street">
        <xsl:choose>		              
          <xsl:when test="prd:AgentInformation/prd:StreetAddress">		    		   		   
            <xsl:value-of select="prd:AgentInformation/prd:StreetAddress" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>

      <xsl:variable name="city">
        <xsl:choose>		              
          <xsl:when test="prd:AgentInformation/prd:City">		    		   		   
            <xsl:value-of select="prd:AgentInformation/prd:City" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>

      <xsl:variable name="agentState">
        <xsl:choose>		              
          <xsl:when test="prd:AgentInformation/prd:State">		    		   		   
            <xsl:value-of select="prd:AgentInformation/prd:State" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>
      
      <xsl:variable name="zip">
        <xsl:choose>		              
          <xsl:when test="prd:AgentInformation/prd:Zip">		    		   		   
            <xsl:value-of select="prd:AgentInformation/prd:Zip" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>
      
      <xsl:variable name="cityComma">
        <xsl:choose>                  
          <xsl:when test="normalize-space($city) != '' and normalize-space($agentState) != ''">                    
            <xsl:value-of select="concat(normalize-space($city), ',')" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="$city" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>
      
      <xsl:choose>		              
        <xsl:when test="normalize-space($street) != '' or normalize-space($city) != '' or normalize-space($agentState) != '' or normalize-space($zip) != ''">		    		   		   
          <xsl:value-of select="concat($street, $cityComma, $agentState, $zip)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="section3Presents">
      <xsl:choose>		              
        <xsl:when test="normalize-space($agentName) != '' or normalize-space($agentAddress) != ''">		    		   		   
          <xsl:value-of select="1" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="0" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:if test="normalize-space($state) != ''">
      <tr>
        <td align="left">
          <b>State of Origin: </b><xsl:value-of select="$state" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($dateIncorporated) != ''">
      <tr>
        <td align="left">
          <b>Date of Incorporation: </b><xsl:value-of select="$dateIncorporated" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($currentStatus) != ''">
      <tr>
        <td align="left">
          <b>Current Status: </b><xsl:value-of select="$currentStatus" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="boolean(number($section1Presents)) and boolean(number($section2Presents))">
      <tr>
        <td class="paragraphSpacer"></td>
      </tr>
    </xsl:if>
         
    <xsl:if test="normalize-space($businessType) != ''">
      <tr>
        <td align="left">
          <b>Business Type: </b><xsl:value-of select="$businessType" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($dbaName) != ''">
      <tr>
        <td align="left">
          <b>DBA: </b><xsl:value-of select="$dbaName" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($charterNumber) != ''">
      <tr>
        <td align="left">
          <b>Charter Number: </b><xsl:value-of select="$charterNumber" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($federalTaxID) != ''">
      <tr>
        <td align="left">
          <b>Federal Tax ID: </b><xsl:value-of select="$federalTaxID" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($stateTaxID) != ''">
      <tr>
        <td align="left">
          <b>State Tax ID: </b><xsl:value-of select="$stateTaxID" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="(boolean(number($section1Presents)) or boolean(number($section2Presents))) and boolean(number($section3Presents))">
      <tr>
        <td class="paragraphSpacer"></td>
      </tr>
    </xsl:if>
         
    <xsl:if test="normalize-space($agentName) != ''">
      <tr>
        <td align="left">
          <b>Agent: </b><xsl:value-of select="$agentName" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($agentAddress) != ''">
      <tr>
        <td align="left">
          <b>Agent Address: </b><xsl:value-of select="$agentAddress" /></td>
      </tr>
    </xsl:if>

  </xsl:template>


  <!--
  *********************************************
  * CorporateOwnerInformation template
  *********************************************
  -->
  <xsl:template match="prd:CorporateOwnerInformation">
    <xsl:variable name="ownerName">
      <xsl:choose>		              
        <xsl:when test="prd:Name">		    		   		   
          <xsl:value-of select="prd:Name" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="ownerAddress">
      
      <xsl:variable name="street">
        <xsl:choose>		              
          <xsl:when test="prd:Address">		    		   		   
            <xsl:value-of select="prd:Address" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>

      <xsl:variable name="city">
        <xsl:choose>		              
          <xsl:when test="prd:City">		    		   		   
            <xsl:value-of select="prd:City" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>

      <xsl:variable name="ownerState">
        <xsl:choose>		              
          <xsl:when test="prd:State">		    		   		   
            <xsl:value-of select="prd:State" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>
      
      <xsl:variable name="zip">
        <xsl:choose>		              
          <xsl:when test="prd:Zip">		    		   		   
            <xsl:value-of select="prd:Zip" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>
      
      <xsl:choose>		              
        <xsl:when test="normalize-space($street) != '' or normalize-space($city) != '' or normalize-space($ownerState) != '' or normalize-space($zip) != ''">		    		   		   
          <xsl:value-of select="concat($street, $city, $ownerState, $zip)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:if test="normalize-space($ownerName) != ''">
      <tr>
        <td align="left">
          <b>Corporate Owner: </b><xsl:value-of select="$ownerName" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($ownerAddress) != ''">
      <tr>
        <td align="left">
          <b>Address: </b><xsl:value-of select="$ownerAddress" /></td>
      </tr>
    </xsl:if>

  </xsl:template>

</xsl:stylesheet>