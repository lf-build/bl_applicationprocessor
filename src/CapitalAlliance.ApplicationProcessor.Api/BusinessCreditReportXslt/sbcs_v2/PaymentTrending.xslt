<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->


  <!--
  *********************************************
  * Payment Trending template
  *********************************************
  -->
  <xsl:template name="PaymentTrending">
	<table class="section dataTable" width="100%" cellspacing="0" cellpadding="0">
       	<colgroup>
       		<col width="50%"/>
       		<col width="50%"/>
       	</colgroup>
		<thead>
			<tr>
				<th colspan="2"><a class="report_section_title">Payment Trending</a></th>
			</tr>
		</thead>
		<tbody>
			<xsl:call-template name="paymentTrendCharts"></xsl:call-template>
		</tbody>
	</table>
	<xsl:call-template name="BackToTop" />		
  </xsl:template>

  <xsl:template name="paymentTrendCharts">
    <xsl:variable name="currentDBT">
    	<xsl:choose>
    		<xsl:when test="prd:SBCSExecutiveElements/prd:CurrentDBT and normalize-space(prd:SBCSExecutiveElements/prd:CurrentDBT) != ''">
    			<xsl:value-of select="number(prd:SBCSExecutiveElements/prd:CurrentDBT)"></xsl:value-of>
    		</xsl:when>
    		<xsl:otherwise>
    			<xsl:value-of select="-1"></xsl:value-of>
    		</xsl:otherwise>
    	</xsl:choose>
    </xsl:variable>
	<xsl:if test="prd:SBCSPaymentTrends">
		<xsl:variable name="maxDBT">
		<xsl:for-each select="prd:SBCSPaymentTrends/prd:PriorMonth | prd:SBCSPaymentTrends/prd:CurrentMonth | prd:SBCSQuarterlyPaymentTrends/prd:PriorQuarter | prd:SBCSQuarterlyPaymentTrends/prd:MostRecentQuarter">
			<xsl:sort select="number(prd:DBT)" order="descending" data-type="number"></xsl:sort>
			<xsl:if test="position()=1">
				<!-- User floor instead of ceiling, difference is when all values are 0, we want 10, ceiling will just return 0 -->
				<xsl:value-of select="(floor(number(prd:DBT) div 10) +1)*10"/>
			</xsl:if>

		</xsl:for-each>
		</xsl:variable>
		<tr class="subtitle">
			<th colspan="10">DBT Trends</th>
		</tr>
		<tr>
			<td colspan="10" style="width:100%">
				<table cellspacing="0" cellpadding="0" style="width:100%">
					<tr>
 				        <td>
                             <xsl:comment>Monthly Chart</xsl:comment>
                             <xsl:choose>
                                 <xsl:when test="prd:SBCSPaymentTrends">
                                     <!--Div that will hold the bar chart for Monthly DBT Trends -->
                                     <div id="bar_chart_MDT" style="width: 330px; height: 250px;"></div>
                                 </xsl:when>
                                 <xsl:otherwise>
                                     <div style="width: 450px; height: 50px; font-size: 20px; margin-top: 25px; text-align: center">Data not available</div>
                                 </xsl:otherwise>
                             </xsl:choose>

							<!--Div that will hold the bar chart data for Monthly DBT Trends -->
							<div id="table_MDT" style="display: none;">
								<SBCSPaymentTrends>
								    <maxDBT><xsl:value-of select="$maxDBT"/></maxDBT>
									<xsl:for-each select="prd:SBCSPaymentTrends/prd:CurrentMonth | prd:SBCSPaymentTrends/prd:PriorMonth">
										<xsl:apply-templates select="current()"  mode="LoadTableMDT"/>
									</xsl:for-each>
								</SBCSPaymentTrends>
							</div>
						</td>
						<td>
                            <xsl:comment>Quarterly DBT Trends</xsl:comment>
                            <xsl:choose>
                                <xsl:when test="prd:SBCSQuarterlyPaymentTrends">
                                    <div id="bar_chart_QDT" style="width: 330px; height: 250px;"></div>
                                </xsl:when>
                                <xsl:otherwise>
                                    <div style="width: 450px; height: 50px; font-size: 20px; margin-top: 25px; text-align: center">Data not available</div>
                                </xsl:otherwise>
                            </xsl:choose>
		
							<!--Div that will hold the bar chart data for Quarterly DBT Trends -->
							<div id="table_QDT" style="display: none;">
								<SBCSQuarterlyPaymentTrends>
								    <maxDBT><xsl:value-of select="$maxDBT"/></maxDBT>
									<xsl:for-each select="prd:SBCSQuarterlyPaymentTrends/prd:MostRecentQuarter | prd:SBCSQuarterlyPaymentTrends/prd:PriorQuarter">
										<xsl:apply-templates select="current()"  mode="LoadTableQDTV2"/>
									</xsl:for-each>
								</SBCSQuarterlyPaymentTrends>
							</div>
						</td>
					 </tr>
     			</table>
			</td>			
		<!--
			<td colspan="10" style="width:100%">
				<table cellspacing="0" cellpadding="0" style="width:100%"><tr><td>
								<xsl:comment>Monthly Chart</xsl:comment>
								<img class="print_only fusion_chart_print" src="fusion_chart_print.DBTMonthlyChart.gif">
									<xsl:attribute name="style">
										<xsl:value-of select="concat('width:330px;height:',$FusionChartHeight)"></xsl:value-of>
									</xsl:attribute>
								</img>
								<div class="fusion_chart" chart_type="Column3D" id="DBTMonthlyChart" title="** Includes one or more of the followings collateral: accounts receivables, contract rights, hereafter acquired property, inventory, leases, notes, receivables or proceeds.">
									<xsl:attribute name="style">
										<xsl:value-of select="concat('width:330px;height:',$FusionChartHeight)"></xsl:value-of>
									</xsl:attribute>
								<chart caption='Monthly DBT Trends' showValues='1' slantLabels='1' labelDisplay='Rotate' yAxisMinValue='0'
									paletteColors="67A8DB,67A8DB,67A8DB,67A8DB,67A8DB,67A8DB,67A8DB,67A8DB," exportEnabled="1" >
									<xsl:attribute name="yAxisMaxValue"><xsl:value-of select="$maxDBT"/></xsl:attribute>
									<xsl:for-each select="prd:SBCSPaymentTrends/prd:PriorMonth">
									<xsl:sort select="position()" order="descending"></xsl:sort>
									
									
								    <xsl:variable name="tmpMonth">
										<xsl:value-of select="number(substring(prd:Date, 5, 2)) -1" />
								    </xsl:variable>
							
								    <xsl:variable name="tmpYear">
									<xsl:choose>
									  <xsl:when test="number($tmpMonth) = 0">
									    <xsl:value-of select="number(substring(prd:Date, 1, 4)) - 1" />
									  </xsl:when>
							
									  <xsl:otherwise>
									    <xsl:value-of select="number(substring(prd:Date, 1, 4))" />
									  </xsl:otherwise>
									</xsl:choose>
								    </xsl:variable>
							
								    <xsl:variable name="newMonth">
									<xsl:choose>
									  <xsl:when test="number($tmpMonth) = 0">
									    <xsl:value-of select="12" />
									  </xsl:when>
							
									  <xsl:otherwise>
									    <xsl:value-of select="$tmpMonth" />
									  </xsl:otherwise>
									</xsl:choose>
								    </xsl:variable>
							
								    <xsl:variable name="dateReported">
								      <xsl:choose>
								        <xsl:when test="prd:Date">
								          <xsl:variable name="month">
								            <xsl:call-template name="FormatMonth">
								      		    <xsl:with-param name="monthValue" select="number($newMonth)" />
								      		    <xsl:with-param name="upperCase" select="true()" />
								      		  </xsl:call-template>
								          </xsl:variable>
							
								          <xsl:choose>
								          	<xsl:when test="$newMonth &gt; 0">
								          		<xsl:value-of select="concat(normalize-space($month), substring(normalize-space($tmpYear), 3, 2))" />
								          	</xsl:when>
								          	<xsl:otherwise>
								          		<xsl:value-of select="concat(normalize-space($month), substring(normalize-space($tmpYear - 1), 3, 2))" />
								          	</xsl:otherwise>
								          </xsl:choose>
								        </xsl:when>
							
								        <xsl:otherwise>
								          <xsl:value-of select="'N/A'" />
								        </xsl:otherwise>
								      </xsl:choose>
								    </xsl:variable>			
									
									
									
									<set>
										<xsl:attribute name="label">
										  <xsl:value-of select="normalize-space($dateReported)"></xsl:value-of>
										</xsl:attribute>
										<xsl:attribute name="value">
											<xsl:value-of select="number(prd:DBT)"></xsl:value-of>
										</xsl:attribute>
										&#160;
									</set>
									</xsl:for-each>
									<set>
									<xsl:attribute name="name">
										<xsl:value-of select="'Current'"></xsl:value-of>
									</xsl:attribute>
									<xsl:attribute name="value">
										<xsl:choose>
											<xsl:when test="$currentDBT &lt; 0">
												<xsl:value-of select="''"></xsl:value-of>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="$currentDBT"></xsl:value-of>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:attribute>
										&#160;
									</set>
								</chart>
								</div>
								<div>**includes one or more of the following collateral:<br/>
									Accounts Receivables, Contract Rights, Hereafter Acquired Property, Inventory, Leases, notes, Receivables or Proceeds.
								</div>
							</td>
							<td>
								<xsl:comment>Quarterly DBT Trends</xsl:comment>
								<img class="print_only fusion_chart_print" src="fusion_chart_print.DBTQuarterlyChart.gif">
									<xsl:attribute name="style">
										<xsl:value-of select="concat('width:330px;height:',$FusionChartHeight)"></xsl:value-of>
									</xsl:attribute>
								</img>
								<div class="fusion_chart" chart_type="Column3D" id="DBTQuarterlyChart">
									<xsl:attribute name="style">
										<xsl:value-of select="concat('width:330px;height:',$FusionChartHeight)"></xsl:value-of>
									</xsl:attribute>
								<chart caption='Quarterly DBT Trends' showValues='1' slantLabels='1' labelDisplay='Rotate' yAxisMinValue='0'
									paletteColors="67A8DB,67A8DB,67A8DB,67A8DB,67A8DB,67A8DB,67A8DB,67A8DB," exportEnabled="1">
									<xsl:attribute name="yAxisMaxValue"><xsl:value-of select="$maxDBT"/></xsl:attribute>
									<xsl:for-each select="prd:SBCSQuarterlyPaymentTrends/prd:PriorQuarter">
									<xsl:sort select="position()" order="descending"></xsl:sort>
									<set><xsl:attribute name="label">
											<xsl:value-of select="concat(normalize-space(prd:QuarterWithinYear/@code),'Q',substring(normalize-space(prd:YearOfQuarter), 3, 2))"></xsl:value-of>
										</xsl:attribute>
										<xsl:attribute name="value">
											<xsl:value-of select="number(prd:DBT)"></xsl:value-of>
										</xsl:attribute>
									&#160;</set>
									</xsl:for-each>
									<set><xsl:attribute name="label">
											<xsl:value-of select="concat(normalize-space(prd:SBCSQuarterlyPaymentTrends/prd:MostRecentQuarter/prd:QuarterWithinYear/@code),'Q',substring(normalize-space(prd:SBCSQuarterlyPaymentTrends/prd:MostRecentQuarter/prd:YearOfQuarter),3,2))"></xsl:value-of>
										</xsl:attribute>
										<xsl:attribute name="value">
											<xsl:value-of select="number(prd:SBCSQuarterlyPaymentTrends/prd:MostRecentQuarter/prd:DBT)"></xsl:value-of>
										</xsl:attribute>
									&#160;</set>
								</chart>
								</div>
								<div>**includes one or more of the following collateral:<br/>
									Accounts Receivables, Contract Rights, Hereafter Acquired Property, Inventory, Leases, notes, Receivables or Proceeds.
								</div>
							</td>
						</tr>
					</table>
				</td>-->
			</tr>
		</xsl:if>	
	</xsl:template>

 <!--
  *********************************************
  * CurrentMonth | PriorMonth template
  * mode="LoadTableMDT"
  * Build XML for Google Charts
  *********************************************
  -->
  <xsl:template match="prd:CurrentMonth | prd:PriorMonth" mode="LoadTableMDT">

    <xsl:variable name="tmpMonth">
	<xsl:value-of select="number(substring(prd:Date, 5, 2)) - 1" />
    </xsl:variable>
	
    <xsl:variable name="tmpYear">
	<xsl:choose>		              
	  <xsl:when test="number($tmpMonth) = 0">		    		   		   
	    <xsl:value-of select="number(substring(prd:Date, 1, 4)) - 1" />
	  </xsl:when>
	
	  <xsl:otherwise>
	    <xsl:value-of select="number(substring(prd:Date, 1, 4))" />
	  </xsl:otherwise>
	</xsl:choose>    
    </xsl:variable>

    <xsl:variable name="newMonth">
	<xsl:choose>		              
	  <xsl:when test="number($tmpMonth) = 0">		    		   		   
	    <xsl:value-of select="12" />
	  </xsl:when>
	
	  <xsl:otherwise>
	    <xsl:value-of select="$tmpMonth" />
	  </xsl:otherwise>
	</xsl:choose>    
    </xsl:variable>

    <xsl:variable name="dateReported">
      <xsl:choose>		              
          
        <xsl:when test="prd:Date">
          <xsl:variable name="month">
            <xsl:call-template name="FormatMonth">
      		    <xsl:with-param name="monthValue" select="number($newMonth)" />
      		    <xsl:with-param name="upperCase" select="true()" />
      		  </xsl:call-template>
          </xsl:variable>		    		   		   

          <xsl:value-of select="concat(normalize-space($month), substring(normalize-space($tmpYear), 3, 2))" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="businessDBT">
      <xsl:choose>		              
        <xsl:when test="prd:DBT">
          <xsl:value-of select="number(prd:DBT)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

	<xsl:choose>
		<xsl:when test="prd:SBCSPaymentTrends/prd:CurrentMonth">
			<CurrentMonth>
				<Date><xsl:value-of select="$dateReported" /></Date>
				<DBT><xsl:value-of select="$businessDBT" /></DBT>
			</CurrentMonth>
		</xsl:when>
		<xsl:otherwise>
			<PriorMonth>
				<Date><xsl:value-of select="$dateReported" /></Date>
				<DBT><xsl:value-of select="$businessDBT" /></DBT>
			</PriorMonth>
		</xsl:otherwise>
	</xsl:choose>

  </xsl:template>
  
  <!--
  ****************************************************
  * MostRecentQuarter | PriorQuarter template
  * mode="LoadTableQDT"
  * Build XML for Google Charts
  ****************************************************
  -->
  <xsl:template match="prd:MostRecentQuarter | prd:PriorQuarter" mode="LoadTableQDTV2">
	<xsl:variable name="dateQQYY">
	  <xsl:value-of select="concat(prd:QuarterWithinYear/@code, 'Q', substring(prd:YearOfQuarter, 3, 2))" />
	</xsl:variable>

    <xsl:variable name="DBT">
      <xsl:choose>		              
        <xsl:when test="prd:DBT">		    		   		   
          <xsl:value-of select="number(prd:DBT)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

	<xsl:choose>
		<xsl:when test="prd:MostRecentQuarter">
			<MostRecentQuarter>
                <DBT><xsl:value-of select="$DBT" /></DBT>
				<Quarter><xsl:value-of select="$dateQQYY" /></Quarter>
			</MostRecentQuarter>
		</xsl:when>
		<xsl:otherwise>
			<PriorQuarter>
                <DBT><xsl:value-of select="$DBT" /></DBT>
				<Quarter><xsl:value-of select="$dateQQYY" /></Quarter>
			</PriorQuarter>
		</xsl:otherwise>
	</xsl:choose>
  </xsl:template>
  
</xsl:stylesheet>