<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"               
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->


  <!--
  *********************************************
  * Inquiries template
  *********************************************
  -->
  <xsl:template name="Inquiries">

    <table class="section dataTable" width="100%" border="0" cellspacing="0" cellpadding="0">
    
      <thead>
        <tr>
          <th colspan="13"><a name="InquiryDetails" style="background:none"><a class="report_section_title">Inquiries</a></a></th>
        </tr>

        <tr class="subtitle">
          <th colspan="13">
            Summary of Inquiries
          </th>
        </tr>
      </thead>

      <!-- Column Headers -->
      <tr class="datahead">
        <td width="16%">Business<br />Category</td>
        
        <!-- make year month header -->
        <xsl:apply-templates select="prd:SBCSInquiry[last()]/prd:InquiryCount" mode="header" />
      </tr>

      <!-- row of inquiry count -->
      <xsl:apply-templates select="prd:SBCSInquiry" />

    </table>  
    
  </xsl:template>


  <!--
  *********************************************
  * SBCSInquiry template
  *********************************************
  -->
  <xsl:template match="prd:SBCSInquiry">

    <xsl:variable name="category">
      <xsl:choose>                  
        <xsl:when test="position() = last()">
          <xsl:value-of select="'Totals'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="normalize-space(prd:InquiryBusinessCategory)" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
  
    <xsl:variable name="rowClass">
      <xsl:choose>
        <xsl:when test="position() = last()">
          <xsl:value-of select="'summary'" />
        </xsl:when>
        <xsl:when test="position() = (last() - 1)">
          <xsl:value-of select="''" />
        </xsl:when>
        <xsl:when test="position() mod 2 = 0">
          <xsl:value-of select="'even'" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'odd'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <tr class="{normalize-space($rowClass)}">
      <td>
        <xsl:value-of select="$category" />
      </td>

      <xsl:apply-templates select="prd:InquiryCount" mode="count">
      </xsl:apply-templates>
      
    </tr>

  </xsl:template>


  <!--
  *********************************************
  * InquiryCount template
  *********************************************
  -->
  <xsl:template match="prd:InquiryCount" mode="header">
    <xsl:variable name="date">
      <xsl:variable name="month">
  		   <xsl:call-template name="FormatMonth">
  		     <xsl:with-param name="monthValue" select="number(substring(prd:Date, 5, 2))" />
  		     <xsl:with-param name="upperCase" select="true()" />
  		   </xsl:call-template>
      </xsl:variable>		    		   		   

      <xsl:value-of select="concat(normalize-space($month), normalize-space(substring(prd:Date, 3, 2)))" />
    </xsl:variable>

    <td class="rightalign" width="7%"><xsl:value-of select="$date" /></td>
  </xsl:template>


  <!--
  *********************************************
  * InquiryCount template
  *********************************************
  -->
  <xsl:template match="prd:InquiryCount" mode="count" xml:space="default">

    <xsl:variable name="count">
      <xsl:choose><xsl:when test="number(prd:Count) &gt; 0">
          <xsl:value-of select="number(prd:Count)" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>      		              
    </xsl:variable>

    <td class="rightalign" width="7%" style="padding-right:12px;">
      <xsl:value-of select="$count" />
    </td>
    
  </xsl:template>
  
</xsl:stylesheet>