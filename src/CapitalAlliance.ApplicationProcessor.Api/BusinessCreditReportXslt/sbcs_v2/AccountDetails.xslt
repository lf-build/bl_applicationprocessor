<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"               
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->


  <!--
  *********************************************
  * AccountDetails template
  *********************************************
  -->
  <xsl:template name="AccountDetails">

    <table class="section" width="100%" border="0" cellspacing="0" cellpadding="0">

      <thead>
        <tr>
          <th><a class="report_section_title">SBCS Account Details</a></th>
        </tr>
      </thead>

      <!-- loops through trade data -->
      <xsl:apply-templates select="prd:SBCSTradeData" />
    </table>









  </xsl:template>



  <!--
  *********************************************
  * SBCSTradeData template
  *********************************************
  -->
  <xsl:template match="prd:SBCSTradeData">

    <xsl:variable name="recordType">
      <xsl:choose><xsl:when test="prd:RecordType/@code">                     
          <xsl:value-of select="prd:RecordType/@code" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="sequenceNumber">
      <xsl:choose><xsl:when test="prd:TradeLinkKey/prd:SequenceNumber">                    
          <xsl:value-of select="number(prd:TradeLinkKey/prd:SequenceNumber)" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="classDescription">
      <xsl:choose><xsl:when test="normalize-space($recordType) = 'N'">                    
          <xsl:value-of select="'Trade Payment Account'" />
        </xsl:when><xsl:when test="normalize-space($recordType) = 'A'">                    
          <xsl:value-of select="'Additional Payment Experiences'" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="prd:FinancialTradeSubClassification" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="classCode">
      <xsl:choose><xsl:when test="prd:TradeLinkKey/prd:TradeClassificationCode">                    
          <xsl:value-of select="prd:TradeLinkKey/prd:TradeClassificationCode" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="subclassCode">
      <xsl:choose><xsl:when test="prd:TradeLinkKey/prd:TradeSubClassCode">                    
          <xsl:value-of select="prd:TradeLinkKey/prd:TradeSubClassCode" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="linkKey">
      <xsl:value-of select="concat(normalize-space($classCode), normalize-space($subclassCode), normalize-space($sequenceNumber))" />
    </xsl:variable>

    <xsl:variable name="businessCategory">
      <xsl:choose><xsl:when test="prd:BusinessCategory">                     
          <xsl:value-of select="prd:BusinessCategory" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="paymentTerms">
      <xsl:variable name="tmpTerms">
        <xsl:value-of select="prd:Terms" />
      </xsl:variable>

      <xsl:variable name="numPayments">
        <xsl:choose><xsl:when test="string-length(normalize-space($tmpTerms)) &gt;= 4">                     
            <xsl:value-of select="substring(normalize-space($tmpTerms), 1, 4)" />
          </xsl:when><xsl:otherwise>
            <xsl:value-of select="$tmpTerms" />
          </xsl:otherwise></xsl:choose>    
      </xsl:variable>

      <xsl:choose><xsl:when test="string(number($numPayments)) != 'NaN'">                    
          <xsl:variable name="frequency">
            <xsl:call-template name="PaymentTermsTable">
              <xsl:with-param name="value" select="substring(normalize-space($tmpTerms), 6, 2)" />
            </xsl:call-template>
          </xsl:variable>
          
          <xsl:value-of select="concat(number($numPayments), ' payment(s) - ', $frequency)" />
        </xsl:when><xsl:otherwise>
          <xsl:variable name="tmp">
            <xsl:call-template name="PaymentTermsTable">
              <xsl:with-param name="value" select="normalize-space($tmpTerms)" />
            </xsl:call-template>
          </xsl:variable>

          <xsl:choose><xsl:when test="normalize-space($tmp) = ''">                     
              <xsl:value-of select="$tmpTerms" />
            </xsl:when><xsl:otherwise>
              <xsl:value-of select="$tmp" />
            </xsl:otherwise></xsl:choose>    
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="dateReported">
      <xsl:choose><xsl:when test="prd:DateReported">                    
           <xsl:call-template name="FormatDate">
             <xsl:with-param name="pattern" select="'mo/dt/year'" />
             <xsl:with-param name="value" select="prd:DateReported" />
           </xsl:call-template>
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="paymentStatus">
      <xsl:choose><xsl:when test="prd:PaymentStatusCode">                    
          <xsl:value-of select="prd:PaymentStatusCode" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="lastActivity">
      <xsl:variable name="activityType">
        <xsl:choose><xsl:when test="prd:LastActivityType">                     
            <xsl:value-of select="prd:LastActivityType" />
          </xsl:when><xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise></xsl:choose>    
      </xsl:variable>

      <xsl:variable name="activityDate">
        <xsl:choose><xsl:when test="prd:DateLastActivity">                    
             <xsl:call-template name="FormatDate">
               <xsl:with-param name="pattern" select="'mo/dt/year'" />
               <xsl:with-param name="value" select="prd:DateLastActivity" />
             </xsl:call-template>
          </xsl:when><xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise></xsl:choose>    
      </xsl:variable>

      <xsl:choose><xsl:when test="normalize-space($activityType) = '' and normalize-space($activityDate) != ''">                    
          <xsl:value-of select="$activityDate" />
        </xsl:when><xsl:when test="normalize-space($activityType) != '' and normalize-space($activityDate) = ''">                    
          <xsl:value-of select="$activityType" />
        </xsl:when><xsl:when test="normalize-space($activityType) != '' and normalize-space($activityDate) != ''">                    
          <xsl:value-of select="concat($activityType, ' (', normalize-space($activityDate), ')')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="comments">
      <xsl:variable name="comment1">
        <xsl:choose><xsl:when test="prd:CommentCode1Description">                     
            <xsl:value-of select="prd:CommentCode1Description" />
          </xsl:when><xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise></xsl:choose>    
      </xsl:variable>

      <xsl:variable name="comment2">
        <xsl:variable name="tmp2">
          <xsl:choose><xsl:when test="prd:CommentCode2Description">                     
              <xsl:value-of select="prd:CommentCode2Description" />
            </xsl:when><xsl:otherwise>
              <xsl:value-of select="''" />
            </xsl:otherwise></xsl:choose>    
        </xsl:variable>

        <xsl:choose><xsl:when test="normalize-space($comment1) != '' and normalize-space($tmp2) != ''">                    
            <xsl:value-of select="concat(normalize-space($comment1), '; ', normalize-space($tmp2))" />
          </xsl:when><xsl:otherwise>
            <xsl:value-of select="concat($comment1, $tmp2)" />
          </xsl:otherwise></xsl:choose>    
      </xsl:variable>

      <xsl:variable name="comment3">
        <xsl:variable name="tmp3">
          <xsl:choose><xsl:when test="prd:CommentCode3Description">                     
              <xsl:value-of select="prd:CommentCode3Description" />
            </xsl:when><xsl:otherwise>
              <xsl:value-of select="''" />
            </xsl:otherwise></xsl:choose>    
        </xsl:variable>

        <xsl:choose><xsl:when test="normalize-space($comment2) != '' and normalize-space($tmp3) != ''">                    
            <xsl:value-of select="concat(normalize-space($comment2), '; ', normalize-space($tmp3))" />
          </xsl:when><xsl:otherwise>
            <xsl:value-of select="concat($comment2, $tmp3)" />
          </xsl:otherwise></xsl:choose>    
      </xsl:variable>

      <xsl:value-of select="$comment3" />
    </xsl:variable>

    <xsl:variable name="paymentHistory">
      <xsl:choose><xsl:when test="prd:PaymentHistoryProfile">                     
          <xsl:value-of select="translate(prd:PaymentHistoryProfile, ' ', '_')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="amountCurrent">
      <xsl:choose><xsl:when test="prd:CurrentAmountDue and number(prd:CurrentAmountDue) != 0">                    
          <xsl:value-of select="format-number(prd:CurrentAmountDue, '$###,###,##0')" />
        </xsl:when><xsl:otherwise>
          &#160;
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="amount30">
      <xsl:choose><xsl:when test="prd:DBT1to30Amount and number(prd:DBT1to30Amount) != 0">                    
          <xsl:value-of select="format-number(prd:DBT1to30Amount, '$###,###,##0')" />
        </xsl:when><xsl:otherwise>
          &#160;
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="amount60">
      <xsl:choose><xsl:when test="prd:DBT31to60Amount and number(prd:DBT31to60Amount) != 0">                    
          <xsl:value-of select="format-number(prd:DBT31to60Amount, '$###,###,##0')" />
        </xsl:when><xsl:otherwise>
          &#160;
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="amount90">
      <xsl:choose><xsl:when test="prd:DBT61to90Amount and number(prd:DBT61to90Amount) != 0">                    
          <xsl:value-of select="format-number(prd:DBT61to90Amount, '$###,###,##0')" />
        </xsl:when><xsl:otherwise>
          &#160;
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="amount120">
      <xsl:choose><xsl:when test="prd:DBT91to120Amount and number(prd:DBT91to120Amount) != 0">                    
          <xsl:value-of select="format-number(prd:DBT91to120Amount, '$###,###,##0')" />
        </xsl:when><xsl:otherwise>
          &#160;
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="amount121Plus">
      <xsl:choose><xsl:when test="(prd:DBT121to150Amount or prd:DBT151to180Amount or prd:DBT181PlusAmount) and (number(prd:DBT121to150Amount + prd:DBT151to180Amount + prd:DBT181PlusAmount) != 0)">                    
          <xsl:value-of select="format-number((prd:DBT121to150Amount + prd:DBT151to180Amount + prd:DBT181PlusAmount), '$###,###,##0')" />
        </xsl:when><xsl:otherwise>
          &#160;
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="percentCurrent">
      <xsl:choose><xsl:when test="prd:CurrentPercentage and number(prd:CurrentPercentage) != 0">		    		   		   
          <xsl:value-of select="format-number(prd:CurrentPercentage div 100, '##0%')" />
        </xsl:when><xsl:otherwise>
          &#160;
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="percentDBT30">
      <xsl:choose><xsl:when test="prd:DBT30 and number(prd:DBT30) != 0">                     
          <xsl:value-of select="format-number(prd:DBT30 div 100, '##0%')" />
        </xsl:when><xsl:otherwise>
          &#160;
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="percentDBT60">
      <xsl:choose><xsl:when test="prd:DBT60 and number(prd:DBT60) != 0">                     
          <xsl:value-of select="format-number(prd:DBT60 div 100, '##0%')" />
        </xsl:when><xsl:otherwise>
          &#160;
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="percentDBT90">
      <xsl:choose><xsl:when test="prd:DBT90 and number(prd:DBT90) != 0">                     
          <xsl:value-of select="format-number(prd:DBT90 div 100, '##0%')" />
        </xsl:when><xsl:otherwise>
          &#160;
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="percentDBT120">
      <xsl:choose><xsl:when test="prd:DBT120 and number(prd:DBT120) != 0">                     
          <xsl:value-of select="format-number(prd:DBT120 div 100, '##0%')" />
        </xsl:when><xsl:otherwise>
          &#160;
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="percentDBT121Plus">
      <xsl:choose><xsl:when test="prd:DBT150 and number(prd:DBT150) != 0">                     
          <xsl:value-of select="format-number(prd:DBT150 div 100, '##0%')" />
        </xsl:when><xsl:otherwise>
          &#160;
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="creditLoan">
      <xsl:choose><xsl:when test="prd:CurrentCreditLimitOrLoanAmount and number(prd:CurrentCreditLimitOrLoanAmount) != 0">                    
          <xsl:value-of select="format-number(prd:CurrentCreditLimitOrLoanAmount, '$###,###,##0')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="highCredit">
      <xsl:choose><xsl:when test="prd:RecentHighCredit and number(prd:RecentHighCredit/prd:Amount) != 0">                    
          <xsl:value-of select="concat(prd:RecentHighCredit/prd:Modifier/@code, format-number(prd:RecentHighCredit/prd:Amount, '$###,###,##0'))" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="presentBalance">
      <xsl:choose><xsl:when test="(prd:AccountBalance and number(prd:AccountBalance/prd:Amount) != 0) or normalize-space($creditLoan) != '' or normalize-space($highCredit) != ''">                    
          <xsl:value-of select="concat(prd:AccountBalance/prd:Modifier/@code, format-number(prd:AccountBalance/prd:Amount, '$###,###,##0'))" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="accountNumber">
      <xsl:choose><xsl:when test="prd:AccountNumber">                     
          <xsl:value-of select="prd:AccountNumber" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="accountType">
      <xsl:choose><xsl:when test="prd:AccountTypeCode">                     
          <xsl:value-of select="prd:AccountTypeCode" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="accountStatus">
      <xsl:choose><xsl:when test="prd:AccountStatusIndicator">                     
          <xsl:value-of select="prd:AccountStatusIndicator" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="line1Color">
      <xsl:value-of select="'odd'" />
    </xsl:variable>

    <xsl:variable name="dateOpened">
      <xsl:choose><xsl:when test="prd:DateOpened">                    
           <xsl:call-template name="FormatDate">
             <xsl:with-param name="pattern" select="'mo/dt/year'" />
             <xsl:with-param name="value" select="prd:DateOpened" />
           </xsl:call-template>
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="dateClosed">
      <xsl:choose><xsl:when test="prd:DateClosed">                    
           <xsl:call-template name="FormatDate">
             <xsl:with-param name="pattern" select="'mo/dt/year'" />
             <xsl:with-param name="value" select="prd:DateClosed" />
           </xsl:call-template>
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="reasonClosed">
      <xsl:choose><xsl:when test="prd:ReasonClosed">                     
          <xsl:value-of select="prd:ReasonClosed" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="line2Color">
      <xsl:choose><xsl:when test="normalize-space($dateOpened) != '&#160;' or normalize-space($dateClosed) != '&#160;' or normalize-space($reasonClosed) != '&#160;'">                     
          <xsl:choose><xsl:when test="normalize-space($line1Color) = 'even'">                     
              <xsl:value-of select="'odd'" />
            </xsl:when><xsl:otherwise>
              <xsl:value-of select="'even'" />
            </xsl:otherwise></xsl:choose>    
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="$line1Color" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="dateLastPayment">
      <xsl:choose><xsl:when test="prd:DateOfLastPayment">                    
           <xsl:call-template name="FormatDate">
             <xsl:with-param name="pattern" select="'mo/dt/year'" />
             <xsl:with-param name="value" select="prd:DateOfLastPayment" />
           </xsl:call-template>
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="actualPayment">
      <xsl:choose><xsl:when test="prd:ActualPaymentAmount and number(prd:ActualPaymentAmount) != 0">                    
          <xsl:value-of select="format-number(prd:ActualPaymentAmount, '$###,###,##0')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="monthlyPayment">
      <xsl:choose><xsl:when test="(prd:MonthlyPayment/prd:Amount and number(prd:MonthlyPayment/prd:Amount) = 0) and normalize-space(prd:MonthlyPayment/prd:Modifier/@code) = '+'">                    
          <xsl:value-of select="'UNK'" />
        </xsl:when><xsl:when test="prd:MonthlyPayment/prd:Amount and number(prd:MonthlyPayment/prd:Amount) != 0">                    
          <xsl:value-of select="concat(prd:MonthlyPayment/prd:Modifier/@code, format-number(prd:MonthlyPayment/prd:Amount, '$###,###,##0'))" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="line3Color">
      <xsl:choose><xsl:when test="normalize-space($dateLastPayment) != '&#160;' or normalize-space($actualPayment) != '&#160;' or normalize-space($monthlyPayment) != '&#160;'">                     
          <xsl:choose><xsl:when test="normalize-space($line2Color) = 'even'">                     
              <xsl:value-of select="'odd'" />
            </xsl:when><xsl:otherwise>
              <xsl:value-of select="'even'" />
            </xsl:otherwise></xsl:choose>    
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="$line2Color" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="chargeOffDate">
      <xsl:choose><xsl:when test="prd:ChargeOffDate">                    
           <xsl:call-template name="FormatDate">
             <xsl:with-param name="pattern" select="'mo/dt/year'" />
             <xsl:with-param name="value" select="prd:ChargeOffDate" />
           </xsl:call-template>
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="chargeOffAmount">
      <xsl:choose><xsl:when test="prd:ChargeOffAmount and number(prd:ChargeOffAmount) != 0">                    
          <xsl:value-of select="format-number(prd:ChargeOffAmount, '$###,###,##0')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="chargeOff">
      <xsl:choose><xsl:when test="normalize-space($chargeOffDate) != '&#160;' or normalize-space($chargeOffAmount) != '&#160;'">                    
          <xsl:value-of select="'Yes'" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="line4Color">
      <xsl:choose><xsl:when test="normalize-space($chargeOffDate) != '&#160;' or normalize-space($chargeOffAmount) != '&#160;' or normalize-space($chargeOff) != '&#160;'">                     
          <xsl:choose><xsl:when test="normalize-space($line3Color) = 'even'">                     
              <xsl:value-of select="'odd'" />
            </xsl:when><xsl:otherwise>
              <xsl:value-of select="'even'" />
            </xsl:otherwise></xsl:choose>    
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="$line3Color" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="guarantors">
      <xsl:choose><xsl:when test="prd:NumberOfGuarantors and number(prd:NumberOfGuarantors) &gt; 0">                     
          <xsl:value-of select="'Yes'" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'No'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="secured">
      <xsl:choose><xsl:when test="prd:SecuredIndicator and normalize-space(prd:SecuredIndicator) = 'Y'">                     
          <xsl:value-of select="'Yes'" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'No'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="collateral">
      <xsl:choose><xsl:when test="prd:Collateral">                     
          <xsl:value-of select="prd:Collateral" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="line5Color">
      <xsl:choose><xsl:when test="normalize-space($guarantors) = 'Yes' or normalize-space($secured) = 'Yes' or normalize-space($collateral) != '&#160;'">                     
          <xsl:choose><xsl:when test="normalize-space($line4Color) = 'even'">                     
              <xsl:value-of select="'odd'" />
            </xsl:when><xsl:otherwise>
              <xsl:value-of select="'even'" />
            </xsl:otherwise></xsl:choose>    
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="$line4Color" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="balloonDate">
      <xsl:choose><xsl:when test="prd:BalloonPaymentDate">                    
           <xsl:call-template name="FormatDate">
             <xsl:with-param name="pattern" select="'mo/dt/year'" />
             <xsl:with-param name="value" select="prd:BalloonPaymentDate" />
           </xsl:call-template>
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="balloonAmount">
      <xsl:choose><xsl:when test="prd:BalloonPaymentAmount and number(prd:BalloonPaymentAmount) != 0">                    
          <xsl:value-of select="format-number(prd:BalloonPaymentAmount, '$###,###,##0')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="line6Color">
      <xsl:choose><xsl:when test="normalize-space($balloonDate) != '&#160;' or normalize-space($balloonAmount) != '&#160;'">                     
          <xsl:choose><xsl:when test="normalize-space($line5Color) = 'even'">                     
              <xsl:value-of select="'odd'" />
            </xsl:when><xsl:otherwise>
              <xsl:value-of select="'even'" />
            </xsl:otherwise></xsl:choose>    
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="$line5Color" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="accountTypeCode">
      <xsl:choose><xsl:when test="prd:AccountTypeCode/@code">                     
          <xsl:value-of select="prd:AccountTypeCode/@code" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="supplierNumber">
      <xsl:choose><xsl:when test="prd:VerificationReport/prd:SupplierNumber">                     
          <xsl:value-of select="prd:VerificationReport/prd:SupplierNumber" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="SIN">
      <xsl:choose><xsl:when test="prd:VerificationReport/prd:SIN">                     
          <xsl:value-of select="prd:VerificationReport/prd:SIN" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="maturityDate">
      <xsl:choose><xsl:when test="prd:MaturityExpirationDate">                    
           <xsl:call-template name="FormatDate">
             <xsl:with-param name="pattern" select="'mo/dt/year'" />
             <xsl:with-param name="value" select="prd:MaturityExpirationDate" />
           </xsl:call-template>
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="line7Color">
      <xsl:choose><xsl:when test="normalize-space($supplierNumber) != '&#160;' or normalize-space($SIN) != '&#160;'">                     
          <xsl:choose><xsl:when test="normalize-space($line6Color) = 'even'">                     
              <xsl:value-of select="'odd'" />
            </xsl:when><xsl:otherwise>
              <xsl:value-of select="'even'" />
            </xsl:otherwise></xsl:choose>    
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="$line6Color" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="TIN">
      <xsl:choose><xsl:when test="prd:VerificationReport/prd:TIN">                     
          <xsl:value-of select="prd:VerificationReport/prd:TIN" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="LVID">
      <xsl:choose><xsl:when test="prd:VerificationReport/prd:LVID">                     
          <xsl:value-of select="prd:VerificationReport/prd:LVID" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <xsl:variable name="line8Color">
      <xsl:choose><xsl:when test="normalize-space($TIN) != '&#160;' or normalize-space($LVID) != '&#160;'">                     
          <xsl:choose><xsl:when test="normalize-space($line7Color) = 'even'">                     
              <xsl:value-of select="'odd'" />
            </xsl:when><xsl:otherwise>
              <xsl:value-of select="'even'" />
            </xsl:otherwise></xsl:choose></xsl:when><xsl:otherwise>
        <xsl:value-of select="$line7Color" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>

    <tr class="subtitle">
      <th><a name="{normalize-space($linkKey)}"><xsl:value-of select="$classDescription" /> # <xsl:value-of select="normalize-space($sequenceNumber)" /></a></th>
    </tr>
    
    <tr>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <colgroup style="width:33%"/>
          <colgroup style="width:38%" />
          <colgroup style="width:29%" />
          <tr>
            <td class="box_padding">
              <table width="100%" border="0" cellspacing="0" cellpadding="1">
                <colgroup style="width:50%"/>
                <colgroup style="width:50%" />

                <!-- business category -->
                <tr>
                  <td>
                    <b>Business Category:</b></td>
                  <td>
                    <xsl:value-of select="$businessCategory" /></td>
                </tr>

                <!-- date reported -->
                <tr>
                  <td>
                    <b>Date Reported:</b></td>
                  <td>
                    <xsl:value-of select="$dateReported" /></td>
                </tr>

                <!-- last activity -->
                <tr>
                  <td valign="top">
                    <b>Last Activity:</b></td>
                  <td valign="top">
                    <xsl:value-of select="$lastActivity" /></td>
                </tr>
              </table>
            </td>
            
            <td class="box_padding">
              <table width="100%" border="0" cellspacing="0" cellpadding="1">
                <colgroup style="width:40%"/>
                <colgroup style="width:60%" />

                <!-- payment terms -->
                <tr>
                  <td>
                    <b>Payment Terms:</b></td>
                  <td>
                    <xsl:value-of select="$paymentTerms" /></td>
                </tr>

                <!-- payment status -->
                <tr>
                  <td valign="top">
                    <b>Payment Status:</b></td>
                  <td valign="top">
                    <xsl:value-of select="$paymentStatus" /></td>
                </tr>

              </table>
            </td>

            <td class="box_padding">
              <table width="100%" border="0" cellspacing="0" cellpadding="1">
                <colgroup style="width:40%"/>
                <colgroup style="width:60%" />

                <!-- Maturity Date -->
                <tr>
                  <td>
                    <b>Maturity Date:</b></td>
                  <td>
                    <xsl:value-of select="$maturityDate" /></td>
                </tr>

                <!-- comments -->
                <tr>
                  <td>
                    <b>Comments:</b></td>
                  <td>
                    <xsl:value-of select="$comments" /></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>

    <tr>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <colgroup style="width:33%"/>
          <colgroup style="width:67%" />
          <tr>
            <!-- 36 month history -->
            <td class="box_padding">
              <div style="padding-left:2px;"><b>36 Month Payment History</b></div>
              
              <table border="0" cellpadding="0" cellspacing="2">
                <xsl:call-template name="PaymentHistoryRows">
                  <xsl:with-param name="row" select="'1'"/>
                  <xsl:with-param name="totalRows" select="'3'"/>
                  <xsl:with-param name="historyChars" select="normalize-space($paymentHistory)"/>
                </xsl:call-template>
              </table>
            </td>

            <td class="box_padding" style="padding-right:7px;">
              <div><b>Present Payment Status</b></div>

              <table class="section dataTable" width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:2px;">
                <colgroup style="width:17%"/>
                <colgroup style="width:17%"/>
                <colgroup style="width:17%"/>
                <colgroup style="width:17%"/>
                <colgroup style="width:16%"/>
                <colgroup style="width:16%"/>

                <!-- clumn header  -->
                <tr class="datahead">
                  <td class="rightalign" style="padding-right:8px;">Current</td>
                  <td class="rightalign" style="padding-right:8px;">1-30</td>
                  <td class="rightalign" style="padding-right:8px;">31-60</td>
                  <td class="rightalign" style="padding-right:8px;">61-90</td>
                  <td class="rightalign" style="padding-right:8px;">91-120</td>
                  <td class="rightalign" style="padding-right:8px;">121+</td>
                </tr>

                <!-- amount past due -->
                <tr class="odd">
                  <td style="padding-right:8px;" class="riskLow rightalign">
                    <xsl:value-of select="$amountCurrent" /></td>
                
                  <td style="padding-right:8px;" class="riskMedium rightalign">
                    <xsl:value-of select="$amount30"/></td>

                  <td style="padding-right:8px;" class="riskMedium rightalign">
                    <xsl:value-of select="$amount60" /></td>

                  <td style="padding-right:8px;" class="riskMedium rightalign">
                    <xsl:value-of select="$amount90" /></td>

                  <td style="padding-right:8px;" class="riskHigh rightalign">
                    <xsl:value-of select="$amount120" /></td>

                  <td style="padding-right:8px;" class="riskHigh rightalign">
                    <xsl:value-of select="$amount121Plus"/></td>
                </tr>

                <xsl:if test="normalize-space($recordType) = 'N' or normalize-space($recordType) = 'A'">
                
                  <!-- percentage past due -->
                  <tr class="even">
                    <td style="padding-right:8px;" class="riskLow rightalign">
                      <xsl:value-of select="$percentCurrent"/></td>
                  
                    <td style="padding-right:8px;" class="riskMedium rightalign">
                      <xsl:value-of select="$percentDBT30"/></td>

                    <td style="padding-right:8px;" class="riskMedium rightalign">
                      <xsl:value-of select="$percentDBT60"/></td>

                    <td style="padding-right:8px;" class="riskMedium rightalign">
                      <xsl:value-of select="$percentDBT90"/></td>

                    <td style="padding-right:8px;" class="riskHigh rightalign">
                      <xsl:value-of select="$percentDBT120"/></td>

                    <td style="padding-right:8px;" class="riskHigh rightalign">
                      <xsl:value-of select="$percentDBT121Plus"/></td>
                  </tr>
                </xsl:if>
                
                <!-- present balance, recent high credit, original loan -->
                <tr>
                  <td colspan="6" style="padding:3px 7px;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <colgroup style="width:50%" />
                      <colgroup style="width:50%" />
                      
                      <tr>
                        <td style="padding:0; height:14px;"><b>Present Balance:</b></td>
                        <td style="padding:0; height:14px;text-align:right;"><xsl:value-of select="$presentBalance" /></td>
                      </tr>
                      <tr>
                        <td style="height:14px; padding:2px 0;"><b>Recent High Credit:</b></td>
                        <td style="height:14px; padding:2px 0;text-align:right;"><xsl:value-of select="$highCredit" /></td>
                      </tr>
                      <tr>
                        <td style="padding:0; height:14px;"><b>Credit Limit/Original Loan Amount:</b></td>
                        <td style="padding:0; height:14px;text-align:right;"><xsl:value-of select="$creditLoan" /></td>
                      </tr>
                    </table>
                  </td>
                </tr>

              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>

    <tr>
      <td>
        <table class="dataTable stackHeader" width="100%" border="0" cellspacing="0" cellpadding="0">
          <colgroup style="width:34%"/>
          <colgroup style="width:33%"/>
          <colgroup style="width:33%"/>
          
          <tr class="datahead">
            <th colspan="3" class="firstColumn">Additional Account Information</th>
          </tr>
          
          <xsl:variable name="line1Class">
            <xsl:choose>
              <xsl:when test="string-length(translate(concat($line2Color,$line3Color,$line4Color,$line5Color,$line6Color,$line7Color,$line8Color), $line1Color, '')) = 0">
                <xsl:value-of select="'last'"></xsl:value-of>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="$line1Color"></xsl:value-of>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:variable>
          <tr class="{normalize-space($line1Class)}">
            <td class="firstColumn">
              <div>
                <b>Account Number</b></div>
              <div>
                <xsl:value-of select="$accountNumber" /></div>
             </td>
  
            <td>
              <div>
                <b>Account Type</b></div>
              <div>
                <xsl:value-of select="$accountType" /></div>
             </td>
  
            <td>
              <div>
                <b>Account Status</b></div>
              <div>
                <xsl:value-of select="$accountStatus" /></div>
             </td>
          </tr>
          
          <xsl:if test="normalize-space($dateOpened) != '&#160;' or normalize-space($dateClosed) != '&#160;' or normalize-space($reasonClosed) != '&#160;'">
            <xsl:variable name="line2Class">
              <xsl:choose>
                <xsl:when test="string-length(translate(concat($line3Color,$line4Color,$line5Color,$line6Color,$line7Color,$line8Color), $line2Color, '')) = 0">
                  <xsl:value-of select="'last'"></xsl:value-of>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="$line2Color"></xsl:value-of>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:variable>
            <tr class="{normalize-space($line2Class)}">
              <td class="firstColumn">
                <div>
                  <b>Date Opened</b></div>
                <div>
                  <xsl:value-of select="$dateOpened" /></div>
              </td>
  
              <td>
                <div>
                  <b>Date Closed</b></div>
                <div>
                  <xsl:value-of select="$dateClosed" /></div>
              </td>
  
              <td>
                <div>
                  <b>Reason Closed</b></div>
                <div>
                  <xsl:value-of select="$reasonClosed" /></div>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="normalize-space($dateLastPayment) != '&#160;' or normalize-space($actualPayment) != '&#160;' or normalize-space($monthlyPayment) != '&#160;'">
            <xsl:variable name="line3Class">
              <xsl:choose>
                <xsl:when test="string-length(translate(concat($line4Color,$line5Color,$line6Color,$line7Color,$line8Color), $line3Color, '')) = 0">
                  <xsl:value-of select="'last'"></xsl:value-of>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="$line3Color"></xsl:value-of>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:variable>
            <tr class="{normalize-space($line3Class)}">
              <td class="firstColumn">
                <div>
                  <b>Date of Last Payment</b></div>
                <div>
                  <xsl:value-of select="$dateLastPayment" /></div>
              </td>
  
              <td>
                <div>
                  <b>Actual Payment Amt</b></div>
                <div>
                  <xsl:value-of select="$actualPayment" /></div>
              </td>
  
              <td>
                <div>
                  <b>Monthly Payment Amt</b></div>
                <div>
                  <xsl:value-of select="$monthlyPayment" /></div>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="normalize-space($chargeOff) != '&#160;' or normalize-space($chargeOffAmount) != '&#160;' or normalize-space($chargeOffDate) != '&#160;'">
            <xsl:variable name="line4Class">
              <xsl:choose>
                <xsl:when test="string-length(translate(concat($line5Color,$line6Color,$line7Color,$line8Color), $line4Color, '')) = 0">
                  <xsl:value-of select="'last'"></xsl:value-of>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="$line4Color"></xsl:value-of>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:variable>
            <tr class="{normalize-space($line4Class)}">
              <td class="firstColumn">
                <div>
                  <b>Charge off</b></div>
                <div>
                  <xsl:value-of select="$chargeOff" /></div>
              </td>
  
              <td>
                <div>
                  <b>Charge off Amt</b></div>
                <div>
                  <xsl:value-of select="$chargeOffAmount" /></div>
              </td>
  
              <td>
                <div>
                  <b>Charge off Date</b></div>
                <div>
                  <xsl:value-of select="$chargeOffDate" /></div>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="normalize-space($guarantors) = 'Yes' or normalize-space($secured) = 'Yes' or normalize-space($collateral) != '&#160;'">
            <xsl:variable name="line5Class">
              <xsl:choose>
                <xsl:when test="string-length(translate(concat($line6Color,$line7Color,$line8Color), $line5Color, '')) = 0">
                  <xsl:value-of select="'last'"></xsl:value-of>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="$line5Color"></xsl:value-of>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:variable>
            <tr class="{normalize-space($line5Class)}">
              <td class="firstColumn">
                <div>
                  <b>Guarantors</b></div>
                <div>
                  <xsl:value-of select="$guarantors" /></div>
              </td>
  
              <td>
                <div>
                  <b>Secured</b></div>
                <div>
                  <xsl:value-of select="$secured" /></div>
              </td>
  
              <td>
                <div>
                  <b>Collateral</b></div>
                <div>
                  <xsl:value-of select="$collateral" /></div>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="normalize-space($balloonDate) != '&#160;' or normalize-space($balloonAmount) != '&#160;'">
            <xsl:variable name="line6Class">
              <xsl:choose>
                <xsl:when test="string-length(translate(concat($line7Color,$line8Color), $line6Color, '')) = 0">
                  <xsl:value-of select="'last'"></xsl:value-of>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="$line6Color"></xsl:value-of>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:variable>
            <tr class="{normalize-space($line6Class)}">
              <td class="firstColumn">
                <div>
                  <b>Balloon Payment Amt</b></div>
                <div>
                  <xsl:value-of select="$balloonAmount" /></div>
              </td>
  
              <td>
                <div>
                  <b>Balloon Payment Date</b></div>
                <div>
                  <xsl:value-of select="$balloonDate" /></div>
              </td>
  
              <td>
                <div>
                  &#160;</div>
                <div>
                  &#160;</div>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="normalize-space($supplierNumber) != '&#160;' or normalize-space($SIN) != '&#160;'">                     
            <xsl:variable name="line7Class">
              <xsl:choose>
                <xsl:when test="string-length(translate($line8Color, $line7Color, '')) = 0">
                  <xsl:value-of select="'last'"></xsl:value-of>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="$line7Color"></xsl:value-of>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:variable>
            <tr class="{normalize-space($line7Class)}">
              <td class="firstColumn">
                <div>
                  <b>Supplier Number</b></div>
                <div>
                  <xsl:value-of select="$supplierNumber" /></div>
              </td>
                
              <td>
                <div>
                  <b>SIN</b></div>
                <div>
                  <xsl:value-of select="$SIN" /></div>
              </td>
                
              <td>
                <div>
                  <b>Account Type</b></div>
                <div>
                  <xsl:value-of select="$accountTypeCode" /></div>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="normalize-space($TIN) != '&#160;' or normalize-space($LVID) != '&#160;'">                     
            <tr class="'last'">
              <td class="firstColumn">
                <div>
                  <b>TIN</b></div>
                <div>
                  <xsl:value-of select="$TIN" /></div>
              </td>
                
              <td>
                <div>
                  <b>LVID</b></div>
                <div>
                  <xsl:value-of select="$LVID" /></div>
              </td>
                
              <td>
                <div>
                  &#160;</div>
                <div>
                  &#160;</div>
              </td>
            </tr>
          </xsl:if>
        </table>
      </td>
    </tr>
    
    <xsl:if test="position() &lt; last()">
    <tr class="spacer">
      <td style="border-top: solid 1px {$borderColor};">&#160;</td>
    </tr>     
    </xsl:if>

  </xsl:template>



  <!--
  ********************************************************
  * translate PaymentTermsTable
  ********************************************************
  -->
  <xsl:template name="PaymentTermsTable">
    <xsl:param name="value" select="''" />

    <xsl:choose>                  
      <xsl:when test="normalize-space($value) = 'REV' ">
        <xsl:text disable-output-escaping="yes">Revolving</xsl:text>
      </xsl:when>
      <xsl:when test="normalize-space($value) = 'P' ">
        <xsl:text disable-output-escaping="yes">One Payment</xsl:text>
      </xsl:when>
      <xsl:when test="normalize-space($value) = 'DEFERRD' ">
        <xsl:text disable-output-escaping="yes">Deferred</xsl:text>
      </xsl:when>
      <xsl:when test="normalize-space($value) = 'WK' ">
        <xsl:text disable-output-escaping="yes">Weekly</xsl:text>
      </xsl:when>
      <xsl:when test="normalize-space($value) = 'BW' ">
        <xsl:text disable-output-escaping="yes">Biweekly</xsl:text>
      </xsl:when>
      <xsl:when test="normalize-space($value) = 'SM' ">
        <xsl:text disable-output-escaping="yes">Semimonthly</xsl:text>
      </xsl:when>
      <xsl:when test="normalize-space($value) = 'MO' ">
        <xsl:text disable-output-escaping="yes">Monthly</xsl:text>
      </xsl:when>
      <xsl:when test="normalize-space($value) = 'QT' ">
        <xsl:text disable-output-escaping="yes">Quarterly</xsl:text>
      </xsl:when>
      <xsl:when test="normalize-space($value) = 'TR' ">
        <xsl:text disable-output-escaping="yes">Triennially</xsl:text>
      </xsl:when>
      <xsl:when test="normalize-space($value) = 'SA' ">
        <xsl:text disable-output-escaping="yes">Semiannually</xsl:text>
      </xsl:when>
      <xsl:when test="normalize-space($value) = 'AN' ">
        <xsl:text disable-output-escaping="yes">Annually</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text disable-output-escaping="yes"></xsl:text>
      </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>


  <!--
  ********************************************************
  * Row loop for Payment History
  ********************************************************
  -->
  <xsl:template name="PaymentHistoryRows">
    <xsl:param name="row" select="'1'" />
    <xsl:param name="totalRows" select="'3'"  />
    <xsl:param name="historyChars" select="''"  />

    <tr>
      <xsl:call-template name="PaymentHistoryCols">
        <xsl:with-param name="col" select="'1'"/>
        <xsl:with-param name="totalCols" select="'12'"/>
        <xsl:with-param name="historyChars" select="substring(normalize-space($historyChars), (($row - 1) * 12 + 1), 12)"/>
      </xsl:call-template>
    </tr>

    <xsl:if test="$row &lt; $totalRows">
      <xsl:call-template name="PaymentHistoryRows">
        <xsl:with-param name="row" select="$row + 1"/>
        <xsl:with-param name="totalRows" select="$totalRows"/>
        <xsl:with-param name="historyChars" select="normalize-space($historyChars)"/>
      </xsl:call-template>
    </xsl:if>

  </xsl:template>
  
  
  <!--
  ********************************************************
  * Column loop for Payment History
  ********************************************************
  -->
  <xsl:template name="PaymentHistoryCols">
    <xsl:param name="col" select="'1'" />
    <xsl:param name="totalCols" select="'12'"  />
    <xsl:param name="historyChars" select="''"  />

    <td><img height="15" width="15" border="0" src="../images/box_{translate(substring(normalize-space($historyChars),$col , 1), '_', '')}.gif" /></td>

    <xsl:if test="$col &lt; $totalCols">
      <xsl:call-template name="PaymentHistoryCols">
        <xsl:with-param name="col" select="$col + 1"/>
        <xsl:with-param name="totalCols" select="$totalCols"/>
        <xsl:with-param name="historyChars" select="normalize-space($historyChars)"/>
      </xsl:call-template>
    </xsl:if>

  </xsl:template>
  
</xsl:stylesheet>