<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rsp="http://www.experian.com/NetConnectResponse"
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->


  <!--
  *********************************************
  * OwnerDetails template
  *********************************************
  -->
  <xsl:template name="OwnerGuarantors">
    <xsl:param name="title" select="''" />
    <xsl:param name="node" />

    <!-- Section title -->
    <table class="section" width="100%" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <th><a class="report_section_title"><xsl:value-of select="$title"/></a></th>
        </tr>
      </thead>
      <tbody>
        <!-- loops through owners -->
        <xsl:apply-templates select="$node" />
      </tbody>
    </table>

  </xsl:template>



  <!--
  *********************************************
  * IndividualOwnerPrincipal template
  *********************************************
  -->
  <xsl:template match="prd:IndividualPersonalGuarantor | prd:IndividualOwnerPrincipal | prd:CompanyGuarantor | prd:CompanyOwnerPrincipal">

    <xsl:variable name="sequenceNumber">
      <xsl:choose><xsl:when test="prd:TradeLinkKey/prd:SequenceNumber">
          <xsl:value-of select="number(prd:TradeLinkKey/prd:SequenceNumber)" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="classCode">
      <xsl:choose><xsl:when test="prd:TradeLinkKey/prd:TradeClassificationCode">
          <xsl:value-of select="prd:TradeLinkKey/prd:TradeClassificationCode" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="subclassCode">
      <xsl:choose><xsl:when test="prd:TradeLinkKey/prd:TradeSubClassCode">
          <xsl:value-of select="prd:TradeLinkKey/prd:TradeSubClassCode" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="classDescription">
      <xsl:choose><xsl:when test="normalize-space($classCode) = 'N'">
          <xsl:value-of select="'TRADE PAYMENT ACCOUNT'" />
        </xsl:when><xsl:when test="normalize-space($classCode) = 'A'">
          <xsl:value-of select="'ADDITIONAL PAYMENT EXPERIENCES'" />
        </xsl:when><xsl:otherwise>
          <xsl:call-template name="convertcase">
            <xsl:with-param name="toconvert" select="//prd:SBCSTradeData[prd:FinancialTradeSubClassification/@code =  normalize-space($subclassCode)]/prd:FinancialTradeSubClassification" />
            <xsl:with-param name="conversion" select="'upper'" />
          </xsl:call-template>
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="linkKey">
      <xsl:value-of select="concat(normalize-space($classCode), normalize-space($subclassCode), normalize-space($sequenceNumber))" />
    </xsl:variable>

    <xsl:variable name="individualCompany">
      <xsl:choose><xsl:when test="name(.) = 'IndividualPersonalGuarantor' or name(.) = 'IndividualOwnerPrincipal'">
          <xsl:value-of select="'Individual'" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'Company'" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="ownershipType">
      <xsl:choose><xsl:when test="name(.) = 'IndividualPersonalGuarantor' or name(.) = 'CompanyGuarantor'">
          <xsl:value-of select="'Guarantor'" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'Owner'" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="internetAddrType">
      <xsl:choose><xsl:when test="normalize-space($individualCompany) = 'Individual'">
          <xsl:value-of select="'Email'" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'URL'" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="name">
      <xsl:choose><xsl:when test="normalize-space($individualCompany) = 'Individual'">
          <xsl:variable name="first">
            <xsl:choose><xsl:when test="prd:FirstName">
                <xsl:value-of select="normalize-space(prd:FirstName)" />
              </xsl:when><xsl:otherwise>
                <xsl:value-of select="''" />
              </xsl:otherwise></xsl:choose>
          </xsl:variable>

          <xsl:variable name="middle">
            <xsl:choose><xsl:when test="prd:MiddleName and normalize-space(prd:MiddleName) != ''">
                <xsl:value-of select="substring(normalize-space(prd:MiddleName),1,1)" />
              </xsl:when><xsl:otherwise>
                <xsl:value-of select="''" />
              </xsl:otherwise></xsl:choose>
          </xsl:variable>

          <xsl:variable name="last">
            <xsl:choose><xsl:when test="prd:Surname">
                <xsl:value-of select="normalize-space(prd:Surname)" />
              </xsl:when><xsl:otherwise>
                <xsl:value-of select="''" />
              </xsl:otherwise></xsl:choose>
          </xsl:variable>

          <xsl:variable name="generation">
            <xsl:choose><xsl:when test="prd:GenerationCode/@code != ''">
                <xsl:value-of select="normalize-space(prd:GenerationCode)" />
              </xsl:when><xsl:otherwise>
                <xsl:value-of select="''" />
              </xsl:otherwise></xsl:choose>
          </xsl:variable>

          <xsl:value-of select="concat($first, ' ', $middle, ' ', $last, ' ', $generation)" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="prd:BusinessName" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="street1">
      <xsl:choose><xsl:when test="prd:StreetAddress">
          <xsl:value-of select="prd:StreetAddress" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="street2">
      <xsl:choose><xsl:when test="prd:AddressLine2">
          <xsl:value-of select="prd:AddressLine2" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="city">
      <xsl:choose><xsl:when test="prd:City">
          <xsl:value-of select="prd:City" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="state">
      <xsl:choose><xsl:when test="prd:State">
          <xsl:value-of select="prd:State" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="zip">
      <xsl:choose><xsl:when test="prd:Zip">
          <xsl:call-template name="FormatZip">
            <xsl:with-param name="value" select="concat(prd:Zip, prd:ZipExtension)" />
          </xsl:call-template>
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="phone">
      <xsl:choose><xsl:when test="prd:PhoneNumber">
          <xsl:call-template name="FormatPhone">
            <xsl:with-param name="value" select="prd:PhoneNumber" />
          </xsl:call-template>
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="taxIDType">
      <xsl:choose><xsl:when test="normalize-space($individualCompany) = 'Individual'">
          <xsl:value-of select="'Tax ID'" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="prd:TaxIDType" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="taxID">
      <xsl:variable name="tmpID">
        <xsl:choose><xsl:when test="normalize-space($individualCompany) = 'Individual'">
            <xsl:value-of select="prd:SSN" />
          </xsl:when><xsl:otherwise>
            <xsl:value-of select="prd:TaxID" />
          </xsl:otherwise></xsl:choose>
      </xsl:variable>
      <xsl:choose><xsl:when test="normalize-space($tmpID) = 'XXXXX'">
          <xsl:value-of select="''" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="$tmpID" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="internetAddress">
      <xsl:choose><xsl:when test="normalize-space($individualCompany) = 'Individual'">
          <xsl:value-of select="prd:EmailAddress" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="prd:InternetDomain" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="status">
      <xsl:choose><xsl:when test="prd:PreviousCurrentIndicator">
          <xsl:value-of select="concat(prd:PreviousCurrentIndicator, ' ', $ownershipType)" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="dateReported">
      <xsl:choose><xsl:when test="prd:DateReported">
           <xsl:call-template name="FormatDate">
             <xsl:with-param name="pattern" select="'mo/dt/year'" />
             <xsl:with-param name="value" select="prd:DateReported" />
           </xsl:call-template>
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="guarantor">
      <xsl:choose><xsl:when test="prd:OwnerIsGuarantorIndicator">
          <xsl:value-of select="prd:OwnerIsGuarantorIndicator" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="changed">
      <xsl:choose><xsl:when test="prd:NameAddressChangeIndicator">
          <xsl:value-of select="prd:NameAddressChangeIndicator" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <tr class="subtitle">
      <th><xsl:value-of select="normalize-space($ownershipType)" /> #<xsl:value-of select="normalize-space(position())" />-<xsl:value-of select="normalize-space($individualCompany)" /></th>
    </tr>

    <tr>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <colgroup style="width:50%"/>
          <colgroup style="width:50%" />
          <tr>
            <td class="box_padding">
              <table width="100%" border="0" cellspacing="0" cellpadding="1">
                <colgroup style="width:28%"/>
                <colgroup style="width:72%" />

                <tr>
                  <td class="label">Name</td>
                  <td><xsl:value-of select="$name" /></td>
                </tr>
                
                <tr>
                  <td class="label">Address</td>
                  <td>
                    <xsl:if test="normalize-space($street1) != ''">
                      <xsl:value-of select="$street1" />
      
                      <xsl:if test="normalize-space($street2) != ''">
                        <br />
                        <xsl:value-of select="$street2" />
                      </xsl:if>
      
                      <xsl:if test="normalize-space($city) != ''">
                        <br />
                      </xsl:if>
                    </xsl:if>
      
                    <xsl:if test="normalize-space($city) != ''">
                      <xsl:value-of select="concat(normalize-space($city), ', ', normalize-space($state), ' ', normalize-space($zip))" />
                    </xsl:if>
                  </td>
                </tr>
                
                <xsl:if test="normalize-space($phone) != ''">
                  <tr>
                    <td class="label">Phone</td>
                    <td><xsl:value-of select="$phone" /></td>
                  </tr>
                </xsl:if>
                
                <xsl:if test="normalize-space($taxID) != ''">
                  <tr>
                    <td class="label"><xsl:value-of select="normalize-space($taxIDType)" /></td>
                    <td><xsl:value-of select="$taxID" /></td>
                  </tr>
                </xsl:if>

                <xsl:if test="normalize-space($internetAddress) != ''">                
                  <tr>
                    <td class="label"><xsl:value-of select="normalize-space($internetAddrType)" /></td>
                    <td><xsl:value-of select="$internetAddress" /></td>
                  </tr>                
                </xsl:if>
              </table>
            </td>

            <td class="box_padding">
              <table width="100%" border="0" cellspacing="0" cellpadding="1">
                <colgroup style="width:34%"/>
                <colgroup style="width:66%" />

                <tr>
                  <td class="label">Associated Record</td>
                  <td><xsl:value-of select="$classDescription" /> #<xsl:value-of select="normalize-space($sequenceNumber)" />&#160;<a href="#{normalize-space($linkKey)}" class="bright">details</a></td>
                </tr>                

                <tr>
                  <td class="label">Ownership Status</td>
                  <td>
                    <xsl:call-template name="convertcase">
                      <xsl:with-param name="toconvert" select="$status" />
                      <xsl:with-param name="conversion" select="'upper'" />
                    </xsl:call-template>
                  </td>
                </tr>                

                <tr>
                  <td class="label">Date Reported</td>
                  <td><xsl:value-of select="$dateReported" /></td>
                </tr>                

                <xsl:if test="normalize-space($guarantor) = 'Y'">
                  <tr>
                    <td class="label">Guarantor</td>
                    <td>This owner is also a guarantor for the associated record</td>
                  </tr>                
                </xsl:if>
              </table>
            </td>
          </tr>
          
          <xsl:if test="normalize-space($changed) = 'Y'">
            <tr>
              <td colspan="2" style="padding:0 0 6px 6px;">
                This information represents a change to a prior
                <xsl:call-template name="convertcase">
                  <xsl:with-param name="toconvert" select="$ownershipType" />
                  <xsl:with-param name="node" select="'lower'" />
                </xsl:call-template>
                name or address
               </td>
            </tr>
          </xsl:if>
          
        </table>
      </td>
    </tr>

    <xsl:if test="position() &lt; last()">
      <tr><td class="subsectionSpacer"></td></tr>
    </xsl:if>
    
  </xsl:template>


</xsl:stylesheet>
