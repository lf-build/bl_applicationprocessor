<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">
                

  <!--
  *********************************************
  * Output method
  *********************************************
  -->



  <!--
  *********************************************
  * CollectionsDerogatory template
  *********************************************
  -->
  <xsl:template name="CollectionsDerogatory">

    <xsl:variable name="totalCollection">
      <xsl:value-of select="number(prd:SBCSExecutiveElements/prd:CollectionCount)" />
    </xsl:variable>

    <xsl:variable name="collectionDates">
      <xsl:choose>                  
        <xsl:when test="$totalCollection &gt; 0 and prd:SBCSExecutiveElements/prd:EarliestCollectionDate and number(prd:SBCSExecutiveElements/prd:EarliestCollectionDate) != 0">
        
          <xsl:variable name="oldDate">
            <xsl:call-template name="FormatDate">
              <xsl:with-param name="pattern" select="'mo/yr'" />
              <xsl:with-param name="value" select="prd:SBCSExecutiveElements/prd:EarliestCollectionDate" />
            </xsl:call-template>
          </xsl:variable>
        
          <xsl:variable name="recentDate">
            <xsl:choose>                  
              <xsl:when test="prd:SBCSExecutiveElements/prd:MostRecentCollectionDate and number(prd:SBCSExecutiveElements/prd:MostRecentCollectionDate) != 0">
               <xsl:call-template name="FormatDate">
                 <xsl:with-param name="pattern" select="'mo/yr'" />
                 <xsl:with-param name="value" select="prd:SBCSExecutiveElements/prd:MostRecentCollectionDate" />
               </xsl:call-template>
              </xsl:when>
      
              <xsl:otherwise>
                <xsl:value-of select="''" />
              </xsl:otherwise>
            </xsl:choose>                
          </xsl:variable>
                       
          <xsl:choose>                  
            <xsl:when test="normalize-space($oldDate) != normalize-space($recentDate) and normalize-space($recentDate) != ''">                     
              <xsl:value-of select="concat('(PLACED ', normalize-space($oldDate), '-', normalize-space($recentDate), ')')" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="concat('(PLACED ', normalize-space($oldDate), ')')" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="accountDelinquent">
      <xsl:value-of select="number(prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:NbrOfAccountsCurrentlyDelinquent)" />
    </xsl:variable>

    <xsl:variable name="worstFinancial">
      <xsl:choose>
        <xsl:when test="prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:WorstFinancialTradePerformanceEver">                    
          <xsl:value-of select="prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:WorstFinancialTradePerformanceEver" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="worstNonFinancial">
      <xsl:choose>
        <xsl:when test="prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:WorstNonFinancialTradePerformanceEver">                    
          <xsl:value-of select="prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:WorstNonFinancialTradePerformanceEver" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="trades100Percent">
      <xsl:value-of select="number(prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:NbrOfFinancialTradesGreaterThan100Utilized)" />
    </xsl:variable>


    <!-- blue box border -->
    <table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td>
          Total collections: 
          <xsl:if test="$totalCollection &gt; 0 and $baseProduct != 'SBCSScore'"><a href="#collections" class="bright">details</a>&#160;</xsl:if> 
          <xsl:value-of select="$collectionDates" /></td>
        <td align="right" nowrap="nowrap"><b><xsl:value-of select="$totalCollection" /></b></td>
      </tr>

      <tr>
        <td>
          All accounts now delinquent/derogatory:</td>
        <td align="right" nowrap="nowrap"><b><xsl:value-of select="$accountDelinquent" /></b></td>
      </tr>

      <tr>
        <td colspan="2">
          <div>Worst financial performance ever:</div>
          <div style="padding-left:10px;"><b><xsl:value-of select="$worstFinancial" /></b></div>
        </td>
      </tr>

      <tr>
        <td colspan="2">
          <div>Worst non-financial trade performance ever:</div>
          <div style="padding-left:10px;"><b><xsl:value-of select="$worstNonFinancial" /></b></div>
        </td>
      </tr>

      <tr>
        <td>
          Number of financial trades > 100%:</td>
        <td align="right" nowrap="nowrap"><b><xsl:value-of select="$trades100Percent" /></b></td>
      </tr>

    </table>

  </xsl:template>


</xsl:stylesheet>
