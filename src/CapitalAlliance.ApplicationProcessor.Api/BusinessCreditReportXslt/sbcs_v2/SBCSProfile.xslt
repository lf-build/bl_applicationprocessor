<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:prd="http://www.experian.com/ARFResponse">
    <!--
  *********************************************
  * Output method
  *********************************************
  -->
    <!--
  *********************************************
  * include template
  *********************************************
  -->
    <xsl:include href="ExecutiveSummary.xslt"/>
    <xsl:include href="FinancialAccountSummary.xslt"/>
    <xsl:include href="TradeAccountSummary.xslt"/>
    <xsl:include href="PublicRecord.xslt"/>
    <xsl:include href="CollectionsDerogatory.xslt"/>
    <xsl:include href="KeyMetrics.xslt"/>
    <xsl:include href="UtilizationCharts.xslt"/>
    <xsl:include href="CurrentDBT.xslt"/>
    <xsl:include href="PerformanceAnalysis.xslt"/>
    <xsl:include href="LegalFilingsCollections.xslt"/>
    <xsl:include href="OwnerGuarantors.xslt"/>
    <xsl:include href="CoDebtors.xslt"/>
    <xsl:include href="Inquiries.xslt"/>
    <xsl:include href="MonthlyPaymentTrends.xslt"/>
    <xsl:include href="QuarterlyPaymentTrends.xslt"/>
    <xsl:include href="AccountDetails.xslt"/>
    <xsl:include href="RiskDashboard.xslt" />
    <xsl:include href="CreditRiskScore.xslt" />
    <xsl:include href="AdditionalBusinessIdentities.xslt"/>
    <xsl:include href="PaymentTrending.xslt"/>
    <xsl:include href="CompanyBackground.xslt"/>
    <xsl:include href="AdditionalCompanyBackground.xslt"/>
    <xsl:include href="../premierprofile/UCCProfile.xslt"/>
    <xsl:include href="../premierprofile/Demographic.xslt"/>
    <xsl:include href="../premierprofile/BusinessFacts.xslt" />
    <xsl:include href="../premierprofile/CorporateLinkage.xslt" />
    <xsl:include href="../premierprofile/CommercialFraudShield.xslt" />
    <xsl:include href="../intelliscore/IPOwnerSummary.xslt" />
    <xsl:include href="../intelliscore/CustomFooterMessage.xslt" />
    
  
    <!--
  *********************************************
  * SBCS Profile template
  *********************************************
  -->
    <xsl:template match="prd:SmallBusinessCreditShare" mode="SBCS">
        <xsl:param name="standalone" select="1"/>
        <xsl:choose>
            <xsl:when test="normalize-space(prd:ExpandedBusinessNameAndAddress/prd:ProfileType/@code) = 'NO RECORD' or normalize-space(prd:BusinessNameAndAddress/prd:ProfileType/@code) = 'NO RECORD'">
                <xsl:call-template name="businessNotFound"/>
            </xsl:when>
            <!--
            <xsl:when test="//prd:seg_9993/prd:Text">
                <xsl:call-template name="sbcs9993"/>
            </xsl:when>
            -->
            <xsl:otherwise>

                <xsl:if test="$standalone=0">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td valign="top" height="20" align="center"><b><a class="product_title" name="{normalize-space($baseProduct)}">Small Business Credit Share</a></b></td>
                        </tr>
                    </table>
                </xsl:if>

                <xsl:if test="prd:SmallBusinessKeyModelElements">
                    <xsl:call-template name="ConsumerSectionsDisclaimerHeader"/>
                </xsl:if>

                <xsl:call-template name="BusinessDemographic" >
                  <xsl:with-param name="reportName" select="$searchName" />
                  <xsl:with-param name="businessName" select="$searchName" />
                </xsl:call-template>
                <xsl:call-template name="BackToTop" />

                <xsl:call-template name="RiskDashboard" />
                <xsl:call-template name="BackToTop" />
                
                <xsl:if test="prd:BusinessFacts">
                  <!-- Business Facts -->
                  <xsl:apply-templates select="prd:BusinessFacts" />
                  <xsl:call-template name="BackToTop" />
                </xsl:if>
                
                <xsl:if test="prd:SBCSExecutiveElements">
                    <!-- SBCS Executive Summary -->
                    <xsl:call-template name="ExecutiveSummary"/>
                    <xsl:call-template name="BackToTop"/>
                </xsl:if>  
                
                <xsl:if test="prd:CommercialFraudShieldSummary">
                  <!-- Commercial Fraud Shield -->
                  <xsl:call-template name="CommercialFraudShield" >
					<xsl:with-param name="reportName" select="$searchName" />
				  </xsl:call-template>
                  <xsl:call-template name="BackToTop" />
                </xsl:if>
                
                <xsl:if test="prd:IntelliscoreScoreInformation[number(prd:ModelInformation/prd:ModelCode) != $lossScoreModel and number(prd:ModelInformation/prd:ModelCode) != $delinquencyScoreModel]">
                  <!-- Credit Risk Score and Credit Limit Recommendation -->
                    <xsl:call-template name="CreditRiskScoreDetails"/>
                    <xsl:call-template name="BackToTop" />
                </xsl:if>
                
                <xsl:if test="prd:SBCSFinancialSummary">
                 <!-- SBCS Financial Account Summary -->
                  <xsl:apply-templates select="prd:SBCSFinancialSummary" />
                 <xsl:call-template name="BackToTop" />
                </xsl:if>
                
                 <xsl:if test="prd:SBCSPaymentTotals">
                 <!-- SBCS Trade Account Summary -->
                  <xsl:apply-templates select="prd:SBCSPaymentTotals" />
                 <xsl:call-template name="BackToTop" />
                </xsl:if>
                                
               
                
                <xsl:if test="(prd:SBCSKeyMetrics or prd:SBCSExecutiveElements) and $isOwnerGuarantorModel = 0">
                     <!-- SBCS Key Metrics and Current DBT Range -->
                    <xsl:call-template name="KeyMetrics"/>
                </xsl:if>   
            
                            
                <xsl:if test="prd:SBCSQuarterlyPaymentTrends or prd:SBCSPaymentTrends">
                    <!-- Payment Trending -->
                    <xsl:call-template name="PaymentTrending"/>
                </xsl:if>
                
                <xsl:if test="prd:SBCSPaymentTrends">
                    <!-- MonthlyPaymentTrends -->
                    <xsl:call-template name="MonthlyPaymentTrends"/>
                    <xsl:call-template name="BackToTop"/>
                </xsl:if>
                <xsl:if test="prd:SBCSQuarterlyPaymentTrends">
                    <!-- QuarterlyPaymentTrends -->
                    <xsl:call-template name="QuarterlyPaymentTrends"/>
                    <xsl:call-template name="BackToTop"/>
                </xsl:if>
                
                <xsl:if test="prd:CollectionData or prd:SBCSBankruptcy or prd:SBCSTaxLien or prd:SBCSJudgment">
                    <!-- Legal Filings, Charge-offs and Collections -->
                    <xsl:call-template name="LegalFilingsCollections"/>
                    <!-- back to top image -->
                    <xsl:call-template name="BackToTop"/>
                </xsl:if>
                
                <xsl:if test="prd:IndividualPersonalGuarantor">
                    <!-- Owner Guarantors 2110 -->
                    <xsl:call-template name="OwnerGuarantors">
                        <xsl:with-param name="title" select="'Individual Guarantor Details'"/>
                        <xsl:with-param name="node" select="prd:IndividualPersonalGuarantor"/>
                    </xsl:call-template>
                    <xsl:call-template name="BackToTop"/>
                </xsl:if>
                
                <xsl:if test="prd:CompanyGuarantor">
                    <!-- Owner Guarantors 2120 -->
                    <xsl:call-template name="OwnerGuarantors">
                        <xsl:with-param name="title" select="'Company Guarantor Details'"/>
                        <xsl:with-param name="node" select="prd:CompanyGuarantor"/>
                    </xsl:call-template>
                    <xsl:call-template name="BackToTop"/>
                </xsl:if>
                
                <xsl:if test="prd:IndividualOwnerPrincipal">
                    <!-- Owner Guarantors 2130 -->
                    <xsl:call-template name="OwnerGuarantors">
                        <xsl:with-param name="title" select="'Individual Owner Details'"/>
                        <xsl:with-param name="node" select="prd:IndividualOwnerPrincipal"/>
                    </xsl:call-template>
                    <xsl:call-template name="BackToTop"/>
                </xsl:if>
                
                <xsl:if test="prd:CompanyOwnerPrincipal">
                    <!-- Owner Guarantors 2140 -->
                    <xsl:call-template name="OwnerGuarantors">
                        <xsl:with-param name="title" select="'Company Owner Details'"/>
                        <xsl:with-param name="node" select="prd:CompanyOwnerPrincipal"/>
                    </xsl:call-template>
                    <xsl:call-template name="BackToTop"/>
                </xsl:if>
                
                <xsl:if test="prd:UCCCoDebtor">
                    <!-- CoDebtors -->
                    <xsl:call-template name="CoDebtors"/>
                    <xsl:call-template name="BackToTop"/>
                </xsl:if>               

                <xsl:if test="prd:SBCSInquiry">
                    <!-- Inquiries -->
                    <xsl:call-template name="Inquiries"/>
                    <xsl:call-template name="BackToTop"/>
                </xsl:if>

                <xsl:if test="prd:SBCSTradeData">
                    <!-- AccountDetails -->
                    <xsl:call-template name="AccountDetails"/>
                    <xsl:call-template name="BackToTop"/>
                </xsl:if>
                
                <xsl:if test="prd:UCCFilingsSummaryCounts or prd:SBCSUCCFilings">
                    <!-- UCCProfile -->
                    <xsl:call-template name="UCCProfile"/>

                </xsl:if>
                
                <xsl:if test="prd:CorporateLinkage">
                  <!-- Corporate Linkage -->
                  <xsl:call-template name="CorporateLinkage" />
                </xsl:if>      
                
                
                <!-- Company Background Information-->
    
                <xsl:if test="(prd:CorporateInformation or prd:CorporateOwnerInformation or prd:CorporateLinkageSummary or prd:DemographicInformation or prd:CorporateLinkageNameAndAddress or prd:KeyPersonnelExecutiveInformation) and $isOwnerGuarantorModel = 0">
                    
                    <xsl:call-template name="CompanyBackground"/>
                    <xsl:call-template name="BackToTop"/>
                </xsl:if>
          
                
                <xsl:if test="prd:AdditionalBusinessIdentity">
                    <!-- AdditionalBusinessIdentities -->
                    <xsl:call-template name="AdditionalBusinessIdentities"/>
                    <xsl:call-template name="BackToTop"/>
                </xsl:if>

                <xsl:if test="prd:SmallBusinessKeyModelElements">
                 <!-- Owner Guarantor Information -->
                  <xsl:apply-templates select="prd:SmallBusinessKeyModelElements" >
                  </xsl:apply-templates>
                 <xsl:call-template name="BackToTop" />
                </xsl:if>     
                
               <!-- custom footer message -->
               <xsl:call-template name="CustomFooterMessage" />
        
               <!-- back to top image -->
               <!--<xsl:call-template name="BackToTop" />-->
               

            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    
  <!--
  *********************************************
  * Header template
  *********************************************
  -->
  <xsl:template match="prd:SmallBusinessCreditShare" mode="header" xml:space="preserve">

    <!-- Report Header -->
    <xsl:call-template name="ReportHeader">
      <xsl:with-param name="reportType" select="$reportTitle" />
      <xsl:with-param name="reportName" select="$searchName" />
      <xsl:with-param name="reportDate" select="prd:ExpandedBusinessNameAndAddress/prd:ProfileDate" />
      <xsl:with-param name="reportTime" select="prd:ExpandedBusinessNameAndAddress/prd:ProfileTime" />
    </xsl:call-template>

    <br />
  
  </xsl:template>
  

  <!--
  *********************************************
  * Footer template
  *********************************************
  -->
  <xsl:template match="prd:SmallBusinessCreditShare" mode="footer" xml:space="preserve">
    <!-- Report Footer -->
    <xsl:call-template name="ReportFooter">
      <xsl:with-param name="reportDate" select="prd:ExpandedBusinessNameAndAddress/prd:ProfileDate" />
    </xsl:call-template>
  </xsl:template>
    
</xsl:stylesheet>
