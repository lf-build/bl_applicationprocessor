<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"                
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->


  <!--
  *********************************************
  * LegalFilingsCollections template
  *********************************************
  -->
  <xsl:template name="LegalFilingsCollections">

    <table class="section" width="100%" border="0" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <th><a name="PublicRecord" style="background:none"><a class="report_section_title">Legal Filings, Charge-offs and Collections</a></a></th>
        </tr>
      </thead>
      
      <tbody>      
        <xsl:if test="prd:CollectionData">
          <!-- CollectionSection -->
          <tr class="subtitle">
            <th><a name="collections">Agency Reported Collections</a></th>
          </tr>
          <xsl:call-template name="CollectionSection" />
        </xsl:if>
    
        <xsl:if test="prd:SBCSBankruptcy">
          <!-- BankruptcySection -->
          <tr class="subtitle">
            <th><a name="bankruptcies">Bankruptcy</a></th>
          </tr>
          <xsl:call-template name="BankruptcySection" />
        </xsl:if>
      
        <xsl:if test="prd:SBCSTaxLien">
          <!-- TaxLienSection -->
          <tr class="subtitle">
            <th><a name="taxliens">Tax Liens</a></th>
          </tr>
          <xsl:call-template name="TaxLienSection" />
        </xsl:if>

        <xsl:if test="prd:SBCSJudgment">
          <!-- JudgmentSection -->
          <tr class="subtitle">
            <th><a name="judgments">Judgments</a></th>
          </tr>
          <xsl:call-template name="JudgmentSection" />
        </xsl:if>
      </tbody>
    </table>
  </xsl:template>
    
  
  <!--
  *********************************************
  * CollectionSection template
  *********************************************
  -->
  <xsl:template name="CollectionSection">
  
    <tr>
      <td>
        <table class="dataTable" width="100%" border="0" cellspacing="0" cellpadding="0">
          <!-- collection header  -->
          <tr class="datahead">
            <td width="8%">Date<br />Placed</td>
            <td width="16%">Status</td>
            <td width="13%" class="rightalign">Original<br />Balance</td>
            <td width="13%" class="rightalign">Outstanding<br />Balance</td>
            <td width="8%">Date<br />Closed</td>
            <td width="29%">Agency</td>
            <td width="13%">Agency Phone</td>
          </tr>

          <!-- CollectionData template -->
          <xsl:apply-templates select="prd:CollectionData">
            <!-- xsl:sort order="descending" select="prd:DatePlacedForCollection" / -->
          </xsl:apply-templates>

          <xsl:if test="prd:SBCSBankruptcy or prd:SBCSTaxLien or prd:SBCSJudgment">
            <tr class="spacer"><td colspan="7">&#160;</td></tr>
          </xsl:if>
        </table>
      </td>
    </tr>
  </xsl:template>

  
  <!--
  *********************************************
  * CollectionData template
  *********************************************
  -->
  <xsl:template match="prd:CollectionData">

    <!-- StreetAddress -->
    <xsl:variable name="streetAddress">
      <xsl:variable name="StreetAddress">
        <xsl:value-of select="prd:StreetAddress" />
      </xsl:variable>
  
      <xsl:call-template name="convertcase">
        <xsl:with-param name="toconvert" select="$StreetAddress" />
        <xsl:with-param name="conversion" select="'proper'" />
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="status">
      <xsl:choose>                  
        <xsl:when test="prd:AccountStatus/@code != ''">                     
          <xsl:value-of select="prd:AccountStatus" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'Unknown'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="amountPlaced">
      <xsl:choose>                  
        <xsl:when test="prd:AmountPlacedForCollection">                    
          <xsl:value-of select="number(prd:AmountPlacedForCollection)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="amountPaid">
      <xsl:choose>                  
        <xsl:when test="prd:AmountPaid">                     
          <xsl:value-of select="number(prd:AmountPaid)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="outstanding">
      <xsl:choose>                  
        <xsl:when test="($amountPlaced &gt; 0) and ($amountPaid &lt;= $amountPlaced)">                     
          <xsl:value-of select="format-number(($amountPlaced - $amountPaid), '$###,###,##0')" />
        </xsl:when>

        <xsl:when test="($amountPlaced &gt; 0) and ($amountPaid &gt; $amountPlaced)">                    
          <xsl:value-of select="'$0'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'Undisclosed'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="dateClosed">
      <xsl:choose>                  
        <xsl:when test="prd:DateClosed and number(prd:DateClosed) != 0">                     
           <xsl:call-template name="FormatDate">
             <xsl:with-param name="pattern" select="'mo/year'" />
             <xsl:with-param name="value" select="prd:DateClosed" />
           </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="agencyName">
      <xsl:choose>                  
        <xsl:when test="prd:CollectionAgencyInfo/prd:AgencyName">                    
          <xsl:value-of select="prd:CollectionAgencyInfo/prd:AgencyName" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'Undisclosed'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="agencyPhone">
      <xsl:choose>                  
        <xsl:when test="prd:CollectionAgencyInfo/prd:PhoneNumber">                     
        <xsl:call-template name="FormatPhone">
          <xsl:with-param name="value" select="prd:CollectionAgencyInfo/prd:PhoneNumber" />
        </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="guarantorName">
      <xsl:choose>                  
        <xsl:when test="prd:ClientOfCollectionAgency/prd:ClientName">                    
          <xsl:value-of select="prd:ClientOfCollectionAgency/prd:ClientName" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="guarantorPhone">
      <xsl:choose>                  
        <xsl:when test="prd:ClientOfCollectionAgency/prd:PhoneNumber and normalize-space(prd:ClientOfCollectionAgency/prd:PhoneNumber) != ''">                     
        <xsl:call-template name="FormatPhone">
          <xsl:with-param name="value" select="prd:ClientOfCollectionAgency/prd:PhoneNumber" />
        </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="contactName">
      <xsl:choose>                  
        <xsl:when test="prd:ClientOfCollectionAgency/prd:ContactName">                    
          <xsl:value-of select="prd:ClientOfCollectionAgency/prd:ContactName" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
       
    <xsl:variable name="rowClass">
      <xsl:choose>
        <xsl:when test="position() = last()">
          <xsl:value-of select="'last'" />
        </xsl:when>
        <xsl:when test="position() mod 2 = 0">
          <xsl:value-of select="'even'" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'odd'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="rowClass1">
      <xsl:choose>
        <xsl:when test="normalize-space($guarantorName) != ''">
          <xsl:value-of select="''" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$rowClass" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- collection data  -->
    <tr class="{normalize-space($rowClass1)}">
      <td>
        <xsl:call-template name="FormatDate">
          <xsl:with-param name="pattern" select="'mo/year'" />
          <xsl:with-param name="value" select="prd:DatePlacedForCollection" />
        </xsl:call-template>
      </td>

      <td><xsl:value-of select="$status" />
      </td>

      <td class="rightalign"><xsl:value-of select="format-number($amountPlaced, '$###,###,##0')" />
      </td>

      <td class="rightalign" style="padding-right:10px;"><xsl:value-of select="$outstanding" />
      </td>

      <td>
        <xsl:value-of select="$dateClosed" />
      </td>

      <td><xsl:value-of select="$agencyName" />
      </td>

      <td><xsl:value-of select="$agencyPhone" />
      </td>

    </tr>

    <xsl:if test="normalize-space($guarantorName) != ''">
      <tr class="{normalize-space($rowClass)}">
        <td colspan="7" style="padding-bottom:1px;">
          <div class="indent1">
          <b>Original Credit Grantor:</b> <xsl:value-of select="$guarantorName" />
                
          <xsl:if test="normalize-space($guarantorPhone) != ''">
            &#160;<xsl:value-of select="$guarantorPhone" />
          </xsl:if>
          
          <xsl:if test="normalize-space($contactName) != ''">
            &#160;<b>Contact:</b> <xsl:value-of select="$contactName" />
          </xsl:if>
          </div>
        </td>
      </tr>                    
    </xsl:if>

  </xsl:template>


  <!--
  *********************************************
  * BankruptcySection template
  *********************************************
  -->
  <xsl:template name="BankruptcySection">
  
    <tr>
      <td>

        <table class="dataTable" width="100%" border="0" cellspacing="0" cellpadding="0">
          <!-- bankruptcy header  -->
          <tr class="datahead">
            <td width="10%">File Date</td>
            <td width="18%">Status</td>
            <td width="10%" class="rightalign">Liability<br />Amount</td>
            <td width="10%" class="rightalign">Asset<br />Amount</td>
            <td width="10%" class="rightalign">Exempt<br />Amount</td>
            <td width="11%">Owner</td>
            <td width="10%">Filing<br />Number</td>
            <td width="21%">Jurisdiction</td>
          </tr>
    
          <!-- SBCSBankruptcy template -->
          <xsl:apply-templates select="prd:SBCSBankruptcy">
            <xsl:sort order="descending" select="prd:DateFiled" />
          </xsl:apply-templates>

          <xsl:if test="prd:SBCSTaxLien or prd:SBCSJudgment">
            <tr class="spacer"><td colspan="8">&#160;</td></tr>
          </xsl:if>
        </table>
      </td>
    </tr>
  
  </xsl:template>


  <!--
  *********************************************
  * SBCSBankruptcy template
  *********************************************
  -->
  <xsl:template match="prd:SBCSBankruptcy">
    <xsl:variable name="status">
      <xsl:choose>                  
        <xsl:when test="prd:LegalAction/@code != ''">                     
          <xsl:value-of select="prd:LegalAction" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'Unknown'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="filingType">
      <xsl:choose>                  
        <xsl:when test="prd:LegalType/@code != ''">                     
          <xsl:value-of select="prd:LegalType" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'Unknown'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="liabilityAmount">
      <xsl:choose>                  
        <xsl:when test="prd:LiabilityAmount">                    
          <xsl:value-of select="format-number(prd:LiabilityAmount, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="assetAmount">
      <xsl:choose>                  
        <xsl:when test="prd:AssetAmount">                    
          <xsl:value-of select="format-number(prd:AssetAmount, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
        
    <xsl:variable name="exemptAmount">
      <xsl:choose>                  
        <xsl:when test="prd:ExemptAmount">                     
          <xsl:value-of select="format-number(prd:ExemptAmount, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="owner">
      <xsl:choose>                  
        <xsl:when test="prd:Owner">                    
          <xsl:value-of select="prd:Owner" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="jurisdiction">
      <xsl:choose>                  
        <xsl:when test="prd:FilingLocation">                     
          <xsl:value-of select="prd:FilingLocation" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="filingNumber">
      <xsl:value-of select="prd:DocumentNumber" />
    </xsl:variable>
    
    <xsl:variable name="rowClass">
      <xsl:choose>
        <xsl:when test="position() = last()">
          <xsl:value-of select="'last'" />
        </xsl:when>
        <xsl:when test="position() mod 2 = 0">
          <xsl:value-of select="'even'" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'odd'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
     
    <tr class="{normalize-space($rowClass)}">
      <td>
         <xsl:call-template name="FormatDate">
           <xsl:with-param name="pattern" select="'mo/dt/year'" />
           <xsl:with-param name="value" select="prd:DateFiled" />
         </xsl:call-template>        
      </td>

      <td><xsl:value-of select="$status" />
      </td>

      <td class="rightalign">
        <xsl:value-of select="$liabilityAmount" />
      </td>

      <td class="rightalign">
        <xsl:value-of select="$assetAmount" />
      </td>

      <td class="rightalign">
        <xsl:value-of select="$exemptAmount" />
      </td>

      <td>
        <xsl:value-of select="$owner" />
      </td>

      <td><xsl:value-of select="$filingNumber" />
      </td>

      <td><xsl:value-of select="$jurisdiction" />
      </td>

    </tr>

  </xsl:template>


  <!--
  *********************************************
  * TaxLienSection template
  *********************************************
  -->
  <xsl:template name="TaxLienSection">
  
    <tr>
      <td>
        <table class="dataTable" width="100%" border="0" cellspacing="0" cellpadding="0">
          <!-- tax lien header  -->
          <tr class="datahead">
            <td width="10%">File Date</td>
            <td width="14%">Filing Type</td>
            <td width="14%">Status</td>
            <td width="13%" class="rightalign">Amount</td>
            <td width="13%">Filing Number</td>
            <td width="36%">Jurisdiction</td>
          </tr>
    
          <!-- SBCSTaxLien template -->
          <xsl:apply-templates select="prd:SBCSTaxLien">
            <xsl:sort order="descending" select="prd:DateFiled" />
          </xsl:apply-templates>

          <xsl:if test="prd:SBCSJudgment">
            <tr class="spacer"><td colspan="6">&#160;</td></tr>
          </xsl:if>
        </table>
      </td>
    </tr>
  </xsl:template>


  <!--
  *********************************************
  * SBCSTaxLien template
  *********************************************
  -->
  <xsl:template match="prd:SBCSTaxLien">
    <xsl:variable name="status">
      <xsl:choose>                  
        <xsl:when test="prd:LegalAction/@code != ''">                     
          <xsl:value-of select="prd:LegalAction" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'Unknown'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="filingType">
      <xsl:choose>                  
        <xsl:when test="prd:LegalType/@code != ''">                     
          <xsl:value-of select="prd:LegalType" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'Unknown'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
   
    <xsl:variable name="amount">
      <xsl:choose>                  
        <xsl:when test="prd:LiabilityAmount">                    
          <xsl:value-of select="format-number(prd:LiabilityAmount, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="jurisdiction">
      <xsl:choose>                  
        <xsl:when test="prd:FilingLocation">                     
          <xsl:value-of select="prd:FilingLocation" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="filingNumber">
      <xsl:value-of select="prd:DocumentNumber" />
    </xsl:variable>
    
    <xsl:variable name="rowClass">
      <xsl:choose>
        <xsl:when test="position() = last()">
          <xsl:value-of select="'last'" />
        </xsl:when>
        <xsl:when test="position() mod 2 = 0">
          <xsl:value-of select="'even'" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'odd'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <tr class="{normalize-space($rowClass)}">
      <td>
       <xsl:call-template name="FormatDate">
         <xsl:with-param name="pattern" select="'mo/dt/year'" />
         <xsl:with-param name="value" select="prd:DateFiled" />
       </xsl:call-template>
      </td>

      <td><xsl:value-of select="$filingType" />
      </td>

      <td><xsl:value-of select="$status" />
      </td>

      <td class="rightalign"><xsl:value-of select="$amount" />
      </td>

      <td><xsl:value-of select="$filingNumber" />
      </td>

      <td><xsl:value-of select="$jurisdiction" />
      </td>

    </tr>
  </xsl:template>


  <!--
  *********************************************
  * JudgmentSection template
  *********************************************
  -->
  <xsl:template name="JudgmentSection">
  
    <tr>
      <td>
        <table class="dataTable" width="100%" border="0" cellspacing="0" cellpadding="0">
          <!-- judgment header  -->
          <tr class="datahead">
            <td width="10%">File Date</td>
            <td width="10%">Status</td>
            <td width="12%" class="rightalign">Amount</td>
            <td width="25%">Plaintiff</td>
            <td width="14%">Filing Number</td>
            <td width="29%">Jurisdiction</td>
          </tr>
    
          <!-- SBCSJudgment template -->
          <xsl:apply-templates select="prd:SBCSJudgment">
            <xsl:sort order="descending" select="prd:DateFiled" />
          </xsl:apply-templates>
        </table>
      </td>
    </tr>
  
  </xsl:template>


  <!--
  *********************************************
  * SBCSJudgment template
  *********************************************
  -->
  <xsl:template match="prd:SBCSJudgment">
    <xsl:variable name="status">
      <xsl:choose>                  
        <xsl:when test="prd:LegalAction/@code != ''">                     
          <xsl:value-of select="prd:LegalAction" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'Unknown'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="filingType">
      <xsl:choose>                  
        <xsl:when test="prd:LegalType/@code != ''">                     
          <xsl:value-of select="prd:LegalType" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'Unknown'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="amount">
      <xsl:choose>                  
        <xsl:when test="prd:LiabilityAmount">                    
          <xsl:value-of select="format-number(prd:LiabilityAmount, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="jurisdiction">
      <xsl:choose>                  
        <xsl:when test="prd:FilingLocation">                     
          <xsl:value-of select="prd:FilingLocation" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="plaintiff">
      <xsl:choose>                  
        <xsl:when test="prd:PlaintiffName">                    
          <xsl:value-of select="prd:PlaintiffName" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="filingNumber">
      <xsl:value-of select="prd:DocumentNumber" />
    </xsl:variable>
    
    <xsl:variable name="rowClass">
      <xsl:choose>
        <xsl:when test="position() = last()">
          <xsl:value-of select="'last'" />
        </xsl:when>
        <xsl:when test="position() mod 2 = 0">
          <xsl:value-of select="'even'" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'odd'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <tr class="{normalize-space($rowClass)}">
      <td>
         <xsl:call-template name="FormatDate">
           <xsl:with-param name="pattern" select="'mo/dt/year'" />
           <xsl:with-param name="value" select="prd:DateFiled" />
         </xsl:call-template>
      </td>

      <td><xsl:value-of select="$status" />
      </td>

      <td class="rightalign"><xsl:value-of select="$amount" />
      </td>

      <td><xsl:value-of select="$plaintiff" />
      </td>

      <td><xsl:value-of select="$filingNumber" />
      </td>

      <td><xsl:value-of select="$jurisdiction" />
      </td>
    </tr>
  </xsl:template>

</xsl:stylesheet>