<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:prd="http://www.experian.com/ARFResponse">
	<!--
  *********************************************
  * Output method
  *********************************************
  -->
	<!--
  *********************************************
  * include template
  *********************************************
  -->
  
   <xsl:include href="../premierprofile/Demographic.xslt"/>
   <xsl:include href="../premierprofile/BusinessFacts.xslt" />
   <xsl:include href="../premierprofile/CorporateLinkage.xslt" />
   <xsl:include href="../intelliscore/IPOwnerSummary.xslt" />
   <xsl:include href="../intelliscore/CustomFooterMessage.xslt" />
   <xsl:include href="CreditRiskScore.xslt" />
   <xsl:include href="ExecutiveSummary.xslt"/>
   <xsl:include href="PublicRecord.xslt"/>	
   <xsl:include href="CollectionsDerogatory.xslt"/>
   <xsl:include href="RiskDashboard.xslt" />
  
  <!--
  *********************************************
  * SBCSScore template
  *********************************************
  -->
	<xsl:template match="prd:SmallBusinessCreditShare" mode="SBCS">
		<xsl:param name="standalone" select="1"/>
		<xsl:choose>
			<xsl:when test="normalize-space(prd:ExpandedBusinessNameAndAddress/prd:ProfileType/@code) = 'NO RECORD' and not(contains($product, 'BOP'))">
				<xsl:call-template name="businessNotFound"/>
			</xsl:when>
			
			<!--
			<xsl:when test="//prd:seg_9993/prd:Text">
				<xsl:call-template name="sbcs9993"/>
			</xsl:when>
			-->
			<xsl:otherwise>
                <xsl:if test="$standalone=0">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td valign="top" height="20" align="center"><b><a class="product_title" name="{normalize-space($baseProduct)}">Small Business Credit Share</a></b></td>
                        </tr>
                    </table>
                </xsl:if>

                <xsl:if test="prd:SmallBusinessKeyModelElements">
                    <xsl:call-template name="ConsumerSectionsDisclaimerHeader"/>
                </xsl:if>

				<xsl:call-template name="BusinessDemographic" >
			      <xsl:with-param name="reportName" select="$searchName" />
			      <xsl:with-param name="businessName" select="$searchName" />
				</xsl:call-template>
				<xsl:call-template name="BackToTop" />

                <xsl:call-template name="RiskDashboard" />
                <xsl:call-template name="BackToTop" />

				<!-- Business Facts -->
     			<xsl:if test="prd:BusinessFacts">  
				  <xsl:apply-templates select="prd:BusinessFacts" />
				  <xsl:call-template name="BackToTop" />
				</xsl:if>
				
				<!-- Corporate Linkage -->		
				<xsl:call-template name="CorporateLinkageMessage" />
				<xsl:call-template name="BackToTop" />

			    <!-- Credit Risk Score and Credit Limit Recommendation -->		   
			    <xsl:if test="prd:IntelliscoreScoreInformation[number(prd:ModelInformation/prd:ModelCode) != $lossScoreModel and number(prd:ModelInformation/prd:ModelCode) != $delinquencyScoreModel]">
					<xsl:call-template name="CreditRiskScoreDetails"/>
					<xsl:call-template name="BackToTop" />
			    </xsl:if>
			 
			    <!-- SBCS Executive Summary -->
				<xsl:if test="prd:SBCSExecutiveElements">		
					<xsl:call-template name="ExecutiveSummary"/>
					<xsl:call-template name="BackToTop"/>
				</xsl:if>
				
				<!-- Owner Guarantor Information -->
			    <xsl:if test="prd:SmallBusinessKeyModelElements">
			      <xsl:apply-templates select="prd:SmallBusinessKeyModelElements" >
			      </xsl:apply-templates>
			     <xsl:call-template name="BackToTop" />
			    </xsl:if>
			    
		       <!-- custom footer message -->
		       <xsl:call-template name="CustomFooterMessage" />
		
		       <!-- back to top image -->
		       <!--<xsl:call-template name="BackToTop" />-->
		       

			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
