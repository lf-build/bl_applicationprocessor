<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->


  <!--
  *********************************************
  * QuarterlyPaymentTrends template
  *********************************************
  -->
  <xsl:template name="QuarterlyPaymentTrends">
    <table class="section dataTable" width="100%" border="0" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <th colspan="10">
            <a class="report_section_title">Quarterly Payment Trends</a>
          </th>
        </tr>
        <tr class="subtitle">
          <th colspan="4">
            <div class="doubleSubtitle">
              <div>Payment History - Quarterly Averages</div>
            </div>
          </th>
          <th colspan="6">
            <div class="doubleSubtitle">
              <div>Account Status</div>
              <div>Days Beyond Terms</div>
            </div>
          </th>
        </tr>
  
      </thead>
      <tbody>
        <!-- Column Headers -->
        <tr class="datahead">
          <td width="12%">Quarter</td>
          <td width="13%">Months</td>
          <td width="8%" class="rightalign">DBT</td>
          <td width="19%" class="rightalign">Balance</td>
          <td width="8%" class="rightalign">Cur</td>
          <td width="8%" class="rightalign">1-30</td>
          <td width="8%" class="rightalign">31-60</td>
          <td width="8%" class="rightalign">61-90</td>
          <td width="8%" class="rightalign">91-120</td>
          <td width="8%" class="rightalign">121+</td>
        </tr>

        <xsl:apply-templates
          select="prd:SBCSQuarterlyPaymentTrends/prd:MostRecentQuarter" mode="dbt" />
        <xsl:apply-templates
          select="prd:SBCSQuarterlyPaymentTrends/prd:PriorQuarter" mode="dbt" />
      </tbody>
    </table>

  </xsl:template>


  <!--
  *********************************************
  * MostRecentQuarter | PriorQuarter template
  *********************************************
  -->
  <xsl:template match="prd:MostRecentQuarter | prd:PriorQuarter" mode="dbt">

    <xsl:variable name="quarter">
      <xsl:value-of select="concat('Q', prd:QuarterWithinYear/@code, ' - ', substring(prd:YearOfQuarter, 3, 2))" />
    </xsl:variable>

    <xsl:variable name="months">
      <xsl:choose><xsl:when test="prd:QuarterWithinYear/@code = 1">
          <xsl:value-of select="'JAN - MAR'" />
        </xsl:when><xsl:when test="prd:QuarterWithinYear/@code = 2">
          <xsl:value-of select="'APR - JUN'" />
        </xsl:when><xsl:when test="prd:QuarterWithinYear/@code = 3">
          <xsl:value-of select="'JUL - SEP'" />
        </xsl:when><xsl:when test="prd:QuarterWithinYear/@code = 4">
          <xsl:value-of select="'OCT - DEC'" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="DBT">
      <xsl:choose><xsl:when test="prd:DBT">
          <xsl:value-of select="number(prd:DBT)" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="balance">
      <xsl:choose><xsl:when test="prd:TotalAccountBalance">
          <xsl:value-of select="concat(prd:TotalAccountBalance/prd:Modifier/@code, format-number(prd:TotalAccountBalance/prd:Amount, '$###,###,##0'))" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="current">
      <xsl:choose><xsl:when test="prd:CurrentPercentage and number(prd:CurrentPercentage) != 0">
          <xsl:value-of select="format-number(prd:CurrentPercentage div 100, '##0%')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="DBT30">
      <xsl:choose><xsl:when test="prd:DBT30 and number(prd:DBT30) != 0">
          <xsl:value-of select="format-number(prd:DBT30 div 100, '##0%')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="DBT60">
      <xsl:choose><xsl:when test="prd:DBT60 and number(prd:DBT60) != 0">
          <xsl:value-of select="format-number(prd:DBT60 div 100, '##0%')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="DBT90">
      <xsl:choose><xsl:when test="prd:DBT90 and number(prd:DBT90) != 0">
          <xsl:value-of select="format-number(prd:DBT90 div 100, '##0%')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="DBT120">
      <xsl:choose><xsl:when test="prd:DBT120 and number(prd:DBT120) != 0">
          <xsl:value-of select="format-number(prd:DBT120 div 100, '##0%')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="DBT121Plus">
      <xsl:choose><xsl:when test="prd:DBT121Plus and number(prd:DBT121Plus) != 0">
          <xsl:value-of select="format-number(prd:DBT121Plus div 100, '##0%')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="rowClass">
      <xsl:choose>		              
        <xsl:when test="name() = 'PriorQuarter' and position() = last()">                
          <xsl:value-of select="'last'" />
        </xsl:when>

        <xsl:when test="name() = 'MostRecentQuarter' or position() mod 2 = 0">		    		   		   
          <xsl:value-of select="'odd'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'even'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <tr class="{normalize-space($rowClass)}">
      <td><xsl:value-of select="$quarter" /></td>
      <td><xsl:value-of select="$months" /></td>
      <td class="rightalign" style="padding-right:7px;"><xsl:value-of select="$DBT" /></td>
      <td class="rightalign"><xsl:value-of select="$balance" /></td>
      <td class="rightalign"><xsl:value-of select="$current" /></td>
      <td class="rightalign"><xsl:value-of select="$DBT30" /></td>
      <td class="rightalign"><xsl:value-of select="$DBT60" /></td>
      <td class="rightalign"><xsl:value-of select="$DBT90" /></td>
      <td class="rightalign"><xsl:value-of select="$DBT120" /></td>
      <td class="rightalign"><xsl:value-of select="$DBT121Plus" /></td>
    </tr>

  </xsl:template>

</xsl:stylesheet>


