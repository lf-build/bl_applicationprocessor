<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">
                

  <!--
  *********************************************
  * Output method
  *********************************************
  -->



  <!--
  *********************************************
  * SBCSFinancialSummary template
  *********************************************
  -->
  <xsl:template match="prd:SBCSFinancialSummary">

    <table class="section dataTable" width="100%" border="0" cellspacing="0" cellpadding="0">
    
      <thead>
        <tr>
          <th colspan="6"><a class="report_section_title">SBCS Financial Account Summary</a></th>
        </tr>
      </thead>

      <!-- column headers -->
      <tr class="datahead">
        <td width="27%">Type</td>
        <td class="rightalign" width="6%">#</td>
        <td class="rightalign" width="17%">Balance</td>
        <td class="rightalign" width="17%">Credit Limit/<br/>Original Loan Amt</td>
        <td class="rightalign" width="17%">Monthly<br/>Payment</td>
        <td class="rightalign" width="16%">Past Due</td>
      </tr>

      <xsl:apply-templates select="prd:AccountTypeSummary" />
    </table>

  </xsl:template>



  <!--
  *********************************************
  * AccountTypeSummary template
  *********************************************
  -->
  <xsl:template match="prd:AccountTypeSummary">

    <xsl:variable name="category">
      <xsl:call-template name="convertcase">
        <xsl:with-param name="toconvert" select="prd:AccountCategory" />
        <xsl:with-param name="conversion" select="'upper'" />
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="linesReported">
      <xsl:choose><xsl:when test="prd:NumberOfTradeLines">                    
          <xsl:value-of select="number(prd:NumberOfTradeLines)" />
      </xsl:when><xsl:otherwise>
          <xsl:value-of select="0" />
      </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="balance">
      <xsl:choose><xsl:when test="prd:TotalBalance and number(prd:TotalBalance/prd:Amount) != 0">                    
          <xsl:value-of select="concat(prd:TotalBalance/prd:Modifier/@code, format-number(prd:TotalBalance/prd:Amount, '$###,###,##0'))" />
      </xsl:when><xsl:otherwise>
          <xsl:value-of select="'$0'" />
      </xsl:otherwise></xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="creditLimit">
      <xsl:choose><xsl:when test="prd:HighCredit and number(prd:HighCredit/prd:Amount) != 0">                    
          <xsl:value-of select="concat(prd:HighCredit/prd:Modifier/@code, format-number(prd:HighCredit/prd:Amount, '$###,###,##0'))" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="monthlyPayment">
      <xsl:choose><xsl:when test="prd:MonthlyPayment and number(prd:MonthlyPayment/prd:Amount) != 0">                    
          <xsl:value-of select="concat(prd:MonthlyPayment/prd:Modifier/@code, format-number(prd:MonthlyPayment/prd:Amount, '$###,###,##0'))" />
        </xsl:when><xsl:when test="prd:MonthlyPayment and number(prd:MonthlyPayment/prd:Amount) = 0 and prd:MonthlyPayment/prd:Modifier/@code = '+'">                    
          <xsl:value-of select="'UNK'" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="pastDue">
      <xsl:choose><xsl:when test="prd:TotalPastDueAmount and number(prd:TotalPastDueAmount) != 0">                    
          <xsl:value-of select="format-number(prd:TotalPastDueAmount, '$###,###,##0')" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise></xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="rowClass">
      <xsl:choose>
        <xsl:when test="position() = last()">
          <xsl:value-of select="'summary'" />
        </xsl:when>
        <xsl:when test="position() mod 2 = 1">
          <xsl:value-of select="'odd'" />
        </xsl:when>
        <xsl:when test="position() mod 2 = 0">
          <xsl:value-of select="'even'" />
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <tr class="{normalize-space($rowClass)}">
      <td><xsl:value-of select="$category" />
      </td>
      
      <td class="rightalign"><xsl:value-of select="$linesReported" />
      </td>
      
      <td class="rightalign"><xsl:value-of select="$balance" />
      </td>
      
      <td class="rightalign" style="padding-right:15px;"><xsl:value-of select="$creditLimit" />
      </td>
      
      <td class="rightalign"><xsl:value-of select="$monthlyPayment" />
      </td>
      
      <td class="rightalign"><xsl:value-of select="$pastDue" />
      </td>
    </tr>
    
  </xsl:template>



</xsl:stylesheet>
