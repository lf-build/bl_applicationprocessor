<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->


  <!--
  *********************************************
  * CoDebtors template
  *********************************************
  -->
  <xsl:template name="CoDebtors">
    <table class="section" width="100%" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <th><a class="report_section_title">Uniform Commercial Code Co-debtors</a></th>
        </tr>
      </thead>

      <tbody>
        <!-- loops through codebtors -->
        <xsl:apply-templates select="prd:UCCCoDebtor" />
      </tbody>
    </table>

  </xsl:template>



  <!--
  *********************************************
  * UCCCoDebtor template
  *********************************************
  -->
  <xsl:template match="prd:UCCCoDebtor">

    <xsl:variable name="sequenceNumber">
      <xsl:choose><xsl:when test="prd:SequenceNumber">
          <xsl:value-of select="number(prd:SequenceNumber)" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="dateFiled">
      <xsl:choose><xsl:when test="prd:DateFiled">
           <xsl:call-template name="FormatDate">
             <xsl:with-param name="pattern" select="'mo/dt/year'" />
             <xsl:with-param name="value" select="prd:DateFiled" />
           </xsl:call-template>
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="filingNumber">
      <xsl:choose><xsl:when test="prd:DocumentNumber">
          <xsl:value-of select="prd:DocumentNumber" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="filingLocation">
      <xsl:choose><xsl:when test="prd:FilingLocation">
          <xsl:value-of select="prd:FilingLocation" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="name">
      <xsl:choose><xsl:when test="prd:Name">
          <xsl:value-of select="prd:Name" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="streetAddress">
      <xsl:choose><xsl:when test="prd:StreetAddress">
          <xsl:value-of select="prd:StreetAddress" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="city">
      <xsl:choose><xsl:when test="prd:City">
          <xsl:value-of select="prd:City" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="state">
      <xsl:choose><xsl:when test="prd:State">
          <xsl:value-of select="prd:State" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="zip">
      <xsl:choose><xsl:when test="prd:Zip">
          <xsl:call-template name="FormatZip">
            <xsl:with-param name="value" select="concat(prd:Zip, prd:ZipExtension)" />
          </xsl:call-template>
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <xsl:variable name="taxID">
      <xsl:choose><xsl:when test="prd:TaxId">
          <xsl:value-of select="prd:TaxId" />
        </xsl:when><xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise></xsl:choose>
    </xsl:variable>

    <tr class="subtitle">
      <th>Co-debtor #<xsl:value-of select="normalize-space($sequenceNumber)" /></th>
    </tr>

    <tr>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <colgroup style="width:50%"/>
          <colgroup style="width:50%" />
          <tr>
            <td class="box_padding">
              <table width="100%" border="0" cellspacing="0" cellpadding="1">
                <colgroup style="width:20%"/>
                <colgroup style="width:80%" />
                
                <tr>
                  <td class="label">Name</td>
                  <td><xsl:value-of select="$name" /></td>
                </tr>
                
                <tr>
                  <td class="label" style="vertical-align:top;">Address</td>
                  <td>
                    <xsl:if test="normalize-space($streetAddress) != ''">
                      <xsl:value-of select="$streetAddress" />
      
                      <xsl:if test="normalize-space($city) != ''">
                        <br />
                      </xsl:if>
                    </xsl:if>
      
                    <xsl:if test="normalize-space($city) != ''">
                      <xsl:value-of select="concat(normalize-space($city), ', ', normalize-space($state), ' ', normalize-space($zip))" />
                    </xsl:if>
                  </td>
                </tr>
                
                <xsl:if test="normalize-space($taxID) != ''">
                  <tr>
                    <td class="label">Tax ID</td>
                    <td><xsl:value-of select="$taxID" /></td>
                  </tr>
                </xsl:if>
              </table>
            </td>

            <td class="box_padding">
              <table width="100%" border="0" cellspacing="0" cellpadding="1">
                <colgroup style="width:33%"/>
                <colgroup style="width:67%" />

                <tr>
                  <td class="label">Filing Number</td>
                  <td><xsl:value-of select="$filingNumber" /></td>
                </tr>

                <tr>
                  <td class="label">Filing Source</td>
                  <td><xsl:value-of select="$filingLocation" /></td>
                </tr>

                <tr>
                  <td class="label">Original Filing Date</td>
                  <td><xsl:value-of select="$dateFiled" /></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>

    <xsl:if test="position() &lt; last()">
      <tr><td class="subsectionSpacer"></td></tr>
    </xsl:if>
    
  </xsl:template>


</xsl:stylesheet>