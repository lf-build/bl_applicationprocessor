<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->


  <!--
  *********************************************
  * Risk Dashboard template
  *********************************************
  -->
  <xsl:template name="RiskDashboard">
	<xsl:variable name="BankruptcyCount">
		<xsl:choose>
		<xsl:when test="prd:SBCSExecutiveElements/prd:BankruptcyFilingCount">
			<xsl:value-of select="number(prd:SBCSExecutiveElements/prd:BankruptcyFilingCount)"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="0"/>
		</xsl:otherwise>
		</xsl:choose>
    </xsl:variable>
	<xsl:variable name="TaxLienCount">
		<xsl:choose>
		<xsl:when test="prd:SBCSExecutiveElements/prd:TaxLienCount">
			<xsl:value-of select="number(prd:SBCSExecutiveElements/prd:TaxLienCount)"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="0"/>
		</xsl:otherwise>
		</xsl:choose>
    </xsl:variable>
	<xsl:variable name="JudgmentCount">
		<xsl:choose>
		<xsl:when test="prd:SBCSExecutiveElements/prd:JudgmentCount">
			<xsl:value-of select="number(prd:SBCSExecutiveElements/prd:JudgmentCount)"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="0"/>
		</xsl:otherwise>
		</xsl:choose>
    </xsl:variable>
    <xsl:variable name="currentDBT">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveElements/prd:CurrentDBT and normalize-space(prd:SBCSExecutiveElements/prd:CurrentDBT) != ''">                    
          <xsl:value-of select="number(prd:SBCSExecutiveElements/prd:CurrentDBT)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
	<xsl:variable name="industryDbt">
		<xsl:choose>
		<xsl:when test="prd:SBCSExecutiveSummary/prd:IndustryDBT and string(number(prd:SBCSExecutiveSummary/prd:IndustryDBT))!='NaN'">
			<xsl:value-of select="format-number(prd:SBCSExecutiveSummary/prd:IndustryDBT,'###,##0')"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="''"/>
		</xsl:otherwise>
		</xsl:choose>
    </xsl:variable>
    <xsl:variable name="textBoxHeight">
		<xsl:value-of select="45+16+14+60"/>
    </xsl:variable>
    <xsl:variable name="inquiry6Class">
		<xsl:choose>
			<xsl:when test="prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:InquiriesPrevious6Months and number(prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:InquiriesPrevious6Months &gt; 4)">
				<xsl:value-of select="'scoreHighRisk'"/>
			</xsl:when>
			<xsl:when test="prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:InquiriesPrevious6Months and number(prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:InquiriesPrevious6Months &gt; 2)">
				<xsl:value-of select="'scoreMedRisk'"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="'scoreLowRisk'"/>
			</xsl:otherwise>
		</xsl:choose>
    </xsl:variable>
    <xsl:variable name="inquiry12Class">
		<xsl:choose>
			<xsl:when test="prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:InquiriesPrevious12Months and number(prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:InquiriesPrevious12Months &gt; 7)">
				<xsl:value-of select="'scoreHighRisk'"/>
			</xsl:when>
			<xsl:when test="prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:InquiriesPrevious12Months and number(prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:InquiriesPrevious12Months &gt; 4)">
				<xsl:value-of select="'scoreMedRisk'"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="'scoreLowRisk'"/>
			</xsl:otherwise>
		</xsl:choose>
    </xsl:variable>
    <xsl:variable name="legalFilingLink">
        <xsl:choose>
        <xsl:when test="prd:CollectionData or prd:SBCSBankruptcy or prd:SBCSTaxLien or prd:SBCSJudgment">
            <xsl:value-of select="'#PublicRecord'"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="'#ExecutiveSummary'"/>
        </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

	<!-- <span style="float:left;width:74%;padding:0;margin:0;"> -->
	<table class="section" height="100%" cellspacing="0" cellpadding="0" style="width:100%;height:100%">
		<colgroup style="width:25%"/>
		<colgroup style="width:19%" />
		<colgroup style="width:18%" />
		<colgroup style="width:38%" />
		<thead>
			<tr>
				<th colspan="4"><a class="report_section_title">Risk Dashboard</a></th>
				<!--<th>Credit Assessment</th>-->
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<table style="width:100%;height:100%" height="100%" cellspacing="0" cellpadding="0">
						<tr class="subtitle"><th class="centerLabel"><a href="#RiskScore" style="text-decoration:none">Risk Scores</a></th></tr>

						<tr>
							<td align="center">
								<xsl:apply-templates select="prd:IntelliscoreScoreInformation[number(prd:ModelInformation/prd:ModelCode) = $sbcsV2Model]" mode="RiskDashboard"/>
								<div style="clear:both;padding-top:5px;">
							    	<xsl:choose>
										<xsl:when test="prd:IntelliscoreScoreInformation[number(prd:ScoreInfo/prd:Score) div 100 != $score999 and number(prd:ScoreInfo/prd:Score) div 100 != $score998]">
											<b>Score range: 1 - 100 percentile</b>
										</xsl:when>
										<xsl:otherwise>
											&#160;
										</xsl:otherwise>
									</xsl:choose>
								</div>
							</td>

						</tr>
					</table>

				</td>
				<td>
					<table cellspacing="0" cellpadding="0" height="100%" style="width:100%;height:100%">
                        <tr class="subtitle"><th class="centerLabel">
                            <a style="text-decoration:none">
                                <xsl:if test="$baseProduct != 'SBCSScore'">
                                    <xsl:attribute name="href">
                                        <xsl:value-of select="'#DBTDetails'"></xsl:value-of>
                                    </xsl:attribute>
                                </xsl:if>
                                Days Beyond Terms
                            </a></th>
                        </tr>
						<tr>
							<td align="center" class="leftborder">
											<xsl:attribute name="style">
												<xsl:value-of select="concat('height:',$textBoxHeight,'px')"/>
											</xsl:attribute>
                                <a style="display:block;text-decoration:none">
                                    <xsl:if test="$baseProduct != 'SBCSScore'">
                                        <xsl:attribute name="href">
                                            <xsl:value-of select="'#DBTDetails'"></xsl:value-of>
                                        </xsl:attribute>
                                    </xsl:if>
                                    
								<xsl:choose>
										 <!-- @TODO not clear from BRD, has to check again  -->
										<xsl:when test="normalize-space($currentDBT)=''">
											<br/>
											<div>Company DBT</div>
											<div class="grayOuterBox" style="width:80px;margin-top:0px">
												<div class="whiteInnerBox">
													<div class="grayInnerBox" style="height:40px">
														<div class="verticalMiddleBox" style="width:100%;height:100%"><div class="wrapInner"><div class="innerText">DBT Unavailable</div></div></div>
													</div>
												</div>
											</div>
										</xsl:when>
										<xsl:otherwise>
											<br/>
											<div>Company DBT</div>
											<xsl:choose>
												<xsl:when test="number($currentDBT) &lt; 6">
													<div class="MiddlePad MiddlePadGreen">
														<!--<div class="title">DBT</div>-->
														<div class="value"><xsl:value-of select="number($currentDBT)"></xsl:value-of></div>
													</div>
												</xsl:when>
												<xsl:when test="number($currentDBT) &lt; 16">
													<div class="MiddlePad MiddlePadYellow">
														<!--<div class="title">DBT</div>-->
														<div class="value"><xsl:value-of select="number($currentDBT)"></xsl:value-of></div>
													</div>
												</xsl:when>
												<xsl:otherwise>
													<div class="MiddlePad MiddlePadRed">
														<!--<div class="title">DBT</div>-->
														<div class="value"><xsl:value-of select="number($currentDBT)"></xsl:value-of></div>
													</div>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:otherwise>
								</xsl:choose>
								<xsl:if test="normalize-space($industryDbt)!=''">
									<div>Industry DBT: <xsl:value-of select="$industryDbt"></xsl:value-of></div>
								</xsl:if>
							</a></td>
						</tr>
					</table>
				</td>
				<td>
					<table height="100%" cellspacing="0" cellpadding="0" style="width:100%;height:100%">
						<tr class="subtitle"><th class="centerLabel">
                            <a style="text-decoration:none">
                                <xsl:if test="$baseProduct != 'SBCSScore'">
                                    <xsl:attribute name="href">
                                        <xsl:value-of select="$legalFilingLink"></xsl:value-of>
                                    </xsl:attribute>
                                </xsl:if>
                                Derogatory Legal
                            </a></th></tr>
						<tr><td align="center" class="leftborder">
											<xsl:attribute name="style">
												<xsl:value-of select="concat('height:',$textBoxHeight,'px')"/>
											</xsl:attribute>
                            <a style="display:block;text-decoration:none">
                                <xsl:if test="$baseProduct != 'SBCSScore'">
                                    <xsl:attribute name="href">
                                        <xsl:value-of select="$legalFilingLink"></xsl:value-of>
                                    </xsl:attribute>
                                </xsl:if>
                            
							<br/>
							<div>Original Filings</div>
							<!--@TODO Shall numeric validation be present here for all 3 fields?-->
							<xsl:choose>
								<xsl:when test="$BankruptcyCount &gt; 0">
									<div class="MiddlePad scoreHighRisk">
										<!--<div class="title">Filings</div>-->
										<div class="value"><xsl:value-of select="$BankruptcyCount+$TaxLienCount+$JudgmentCount"></xsl:value-of></div>
									</div>
								</xsl:when>
								<xsl:otherwise>
									<xsl:choose>
										<xsl:when test="$TaxLienCount+$JudgmentCount &gt; 0">
										<div class="MiddlePad scoreMedRisk">
											<!--<div class="title">Filings</div>-->
											<div class="value"><xsl:value-of select="$BankruptcyCount+$TaxLienCount+$JudgmentCount"></xsl:value-of></div>
										</div>
										</xsl:when>
										<xsl:otherwise>
										<div class="MiddlePad scoreLowRisk">
											<!--<div class="title">Filings</div>-->
											<div class="value">0</div>
										</div>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:otherwise>
							</xsl:choose>
						</a></td></tr>
					</table>
				</td>
				<td>
					<table height="100%" cellspacing="0" cellpadding="0" style="width:100%;height:100%">
                        <tr class="subtitle"><th colspan="2" class="centerLabel">
                            <a style="text-decoration:none">
                                <xsl:choose>
                                    <xsl:when test="prd:SBCSInquiry and $baseProduct != 'SBCSScore'">
                                        <xsl:attribute name="href">
                                            <xsl:value-of select="'#InquiryDetails'"></xsl:value-of>
                                        </xsl:attribute>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:attribute name="style">
                                            <xsl:value-of select="'pointer-events: none;cursor: default;'"></xsl:value-of>
                                        </xsl:attribute>
                                    </xsl:otherwise>
                                </xsl:choose>
                                Inquiry History</a></th></tr>
                        <tr>
                            <td align="center" class="leftborder">
                                            <xsl:attribute name="style">
                                                <xsl:value-of select="concat('height:',$textBoxHeight,'px')"/>
                                            </xsl:attribute>
                            <a style="display:block;text-decoration:none">
                                <xsl:choose>
                                    <xsl:when test="prd:SBCSInquiry and $baseProduct != 'SBCSScore'">
                                        <xsl:attribute name="href">
                                            <xsl:value-of select="'#InquiryDetails'"></xsl:value-of>
                                        </xsl:attribute>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:attribute name="style">
                                            <xsl:value-of select="'pointer-events: none;cursor: default;'"></xsl:value-of>
                                        </xsl:attribute>
                                    </xsl:otherwise>
                                </xsl:choose>
							<br/>
							<div>Last 6 Months</div>
								<div class="MiddlePad {$inquiry6Class}">
									<div class="value">
										<xsl:choose>
											<xsl:when test="string(number(prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:InquiriesPrevious6Months)) != 'NaN'">
												<xsl:value-of select="number(prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:InquiriesPrevious6Months)"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="0"/>
											</xsl:otherwise>
										</xsl:choose>
									</div>
								</div>
							</a></td>
							<td align="center" class="leftborder">
											<xsl:attribute name="style">
												<xsl:value-of select="concat('height:',$textBoxHeight,'px')"/>
											</xsl:attribute>
                            <a style="display:block;text-decoration:none">
                                <xsl:choose>
                                    <xsl:when test="prd:SBCSInquiry and $baseProduct != 'SBCSScore'">
                                        <xsl:attribute name="href">
                                            <xsl:value-of select="'#InquiryDetails'"></xsl:value-of>
                                        </xsl:attribute>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:attribute name="style">
                                            <xsl:value-of select="'pointer-events: none;cursor: default;'"></xsl:value-of>
                                        </xsl:attribute>
                                    </xsl:otherwise>
                                </xsl:choose>
							<br/>
							<div>Last 12 Months</div>
								<div class="MiddlePad {$inquiry12Class}">
									<div class="value">
										<xsl:choose>
											<xsl:when test="string(number(prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:InquiriesPrevious12Months)) != 'NaN'">
												<xsl:value-of select="number(prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:InquiriesPrevious12Months)"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="0"/>
											</xsl:otherwise>
										</xsl:choose>
									</div>
								</div>
							</a></td>
						</tr>
					</table>
				</td>

			</tr>
		</tbody>
	</table>
	<!-- </span>
	<span style="clear:both"/> -->
  </xsl:template>


  <xsl:template match="prd:IntelliscoreScoreInformation" mode="RiskDashboard">
    <xsl:variable name="model">
      <xsl:value-of select="number(prd:ModelInformation/prd:ModelCode)" />
    </xsl:variable>

    <xsl:variable name="score">
      <xsl:value-of select="number(prd:ScoreInfo/prd:Score) div 100" />
    </xsl:variable>

    <xsl:variable name="scoreText">
      <xsl:choose>
        <xsl:when test="contains($score, '.')">
          <xsl:value-of select="normalize-space(substring-before($score, '.'))" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$score" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

	<xsl:variable name="riskClass">
      <xsl:choose>
        <xsl:when test="prd:RiskClass">
          <xsl:value-of select="prd:RiskClass" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="RiskClassByScore">
            <xsl:with-param name="model" select="$model" />
            <xsl:with-param name="score" select="$score" />
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
	</xsl:variable>

    <xsl:variable name="action">
      <xsl:choose>
        <xsl:when test="$riskClass = 1">
          <xsl:value-of select="$lowRiskText" />
        </xsl:when>
        <xsl:when test="$riskClass = 2">
          <xsl:value-of select="$lowMedRiskText" />
        </xsl:when>
        <xsl:when test="$riskClass = 3">
          <xsl:value-of select="$medRiskText" />
        </xsl:when>
        <xsl:when test="$riskClass = 4">
          <xsl:value-of select="$medHighRiskText" />
        </xsl:when>
        <xsl:when test="$riskClass = 5">
          <xsl:value-of select="$highRiskText" />
        </xsl:when>
        <xsl:when test="prd:Action">
          <xsl:value-of select="normalize-space(prd:Action)" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

	<xsl:variable name="actionColor">
		<xsl:choose>
			<xsl:when test="$riskClass = 1">
				<xsl:value-of select="$lowRiskColor"></xsl:value-of>
			</xsl:when>
			<xsl:when test="$riskClass = 2">
				<xsl:value-of select="lowMedRiskColor"></xsl:value-of>
			</xsl:when>
			<xsl:when test="$riskClass = 3">
				<xsl:value-of select="medRiskColor"></xsl:value-of>
			</xsl:when>
			<xsl:when test="$riskClass = 4">
				<xsl:value-of select="medHighRiskColor"></xsl:value-of>
			</xsl:when>
			<xsl:when test="$riskClass = 5">
				<xsl:value-of select="highRiskColor"></xsl:value-of>
			</xsl:when>
			<xsl:otherwise><xsl:value-of select="'#cccccc'"></xsl:value-of></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="scoreRiskClass">
		<xsl:choose>
			<xsl:when test="$riskClass = 1">
				<xsl:value-of select="'scoreLowRisk'"></xsl:value-of>
			</xsl:when>
			<xsl:when test="$riskClass = 2">
				<xsl:value-of select="'scoreLowMedRisk'"></xsl:value-of>
			</xsl:when>
			<xsl:when test="$riskClass = 3">
				<xsl:value-of select="'scoreMedRisk'"></xsl:value-of>
			</xsl:when>
			<xsl:when test="$riskClass = 4">
				<xsl:value-of select="'scoreMedHighRisk'"></xsl:value-of>
			</xsl:when>
			<xsl:when test="$riskClass = 5">
				<xsl:value-of select="'scoreHighRisk'"></xsl:value-of>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="'scoreUnkownRisk'"></xsl:value-of>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
    <xsl:variable name="scoreName">
		<xsl:value-of select="'SBCS Acquisition Score'" />
    </xsl:variable>

    <xsl:variable name="predictText">
		<xsl:value-of select="'serious future risk'"></xsl:value-of>
    </xsl:variable>

	<xsl:attribute name="style">
		<xsl:value-of select="'width: 100%;'"/>
	</xsl:attribute>



		<br/>
		<div>
			<a href="#RiskScore" style="display:block;text-decoration:none"><xsl:value-of select="$scoreName" /></a>
		</div>
		<xsl:choose>
			<xsl:when test="$score = $score999">
				<div style="padding: 10px 0;">
					<div>
						Score unavailable.<br/>
						Information on file not proven to predict
						<xsl:value-of select="$predictText" />.
					</div>
				</div>
			</xsl:when>
			<xsl:when test="$score = $score998">
				<div style="padding: 10px 0;">
					<div>
						Score unavailable.<br/>
						Bankruptcy on file.
					</div>
				</div>
			</xsl:when>
			<xsl:otherwise>
			    <xsl:variable name="marginLeft">
				<xsl:choose>
					<xsl:when test="$action = ''">
						<xsl:value-of select="'53px'"></xsl:value-of>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="'23px'"></xsl:value-of>
						</xsl:otherwise>
					</xsl:choose>			
			    </xsl:variable>
				<div style="float:left; margin: 5px 0pt 5px {$marginLeft};">
					<xsl:attribute name="class">
						<xsl:value-of select="concat('MiddlePad ',$scoreRiskClass)"></xsl:value-of>
					</xsl:attribute>
					<a href="#RiskScore" style="display:block;text-decoration:none"><div class="value"><xsl:value-of select="$scoreText"/></div></a>
				</div>
				<xsl:if test="$action != ''">
					<div style="margin: 20px 18px 0pt 0pt; width: 70px; float: right;"><a href="#RiskScore" style="display:block;text-decoration:none"><xsl:value-of select="$action"/></a></div>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
  </xsl:template>

</xsl:stylesheet>