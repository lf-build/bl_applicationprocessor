<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"                
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->


  <!--
  *********************************************
  * AdditionalCompanyBackground template
  *********************************************
  -->
  <xsl:template name="AdditionalCompanyBackground">
  
    <tr>
      <td class="box_padding">
        <table width="100%" border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="1">
                <xsl:if test="prd:KeyPersonnelExecutiveInformation">
                  <tr>
                    <td align="left" class="bright">
                      <b>Key Personnel</b></td>
                  </tr>

                  <tr>
                    <td align="left">
                      <table border="0" cellspacing="0" cellpadding="0">

                        <!-- rows of KeyPersonnelExecutiveInformation -->
                        <xsl:apply-templates select="prd:KeyPersonnelExecutiveInformation" />

                      </table>
                    </td>
                  </tr>
                </xsl:if>

                <!-- 
                    CorporateLinkageSummary section is involved with other 3 section.
                    We need to check other 3 to determine if this section needs to be handled 
                -->
                <xsl:if test="prd:CorporateInformation and prd:CorporateInformation/prd:StatusFlag/@code != '' and prd:DemographicInformation and prd:DemographicInformation/prd:PrimarySICCode">
                  <xsl:if test="prd:KeyPersonnelExecutiveInformation">
                    <tr>
                      <td class="paragraphSpacer"></td>
                    </tr>
                  </xsl:if>

                  <tr>
                    <td align="left" class="bright">
                      <b>Operating Information</b></td>
                  </tr>

                  <!-- rows of CorporateLinkageSummary -->
                  <xsl:call-template name="OperatingInformation" />
                </xsl:if>

                <xsl:if test="prd:DemographicInformation">
                  <xsl:if test="prd:KeyPersonnelExecutiveInformation or (prd:CorporateInformation and prd:CorporateInformation/prd:StatusFlag/@code != '' and prd:DemographicInformation and prd:DemographicInformation/prd:PrimarySICCode)">
                    <!-- 
                    <tr>
                      <td class="paragraphSpacer"></td>
                    </tr>
                    -->
                  </xsl:if>

                  <!-- rows of DemographicInformation -->
                  <xsl:apply-templates select="prd:DemographicInformation" />
                </xsl:if>

                <xsl:if test="prd:CorporateLinkageNameAndAddress">
                  <xsl:if test="prd:KeyPersonnelExecutiveInformation or (prd:CorporateInformation and prd:CorporateInformation/prd:StatusFlag/@code != '' and prd:DemographicInformation and prd:DemographicInformation/prd:PrimarySICCode) or prd:DemographicInformation">
                    <tr>
                      <td class="paragraphSpacer"></td>
                    </tr>
                  </xsl:if>

                  <tr>
                    <td align="left" class="bright">
                      <b>Affiliated Companies</b></td>
                  </tr>

                  <!-- rows of CorporateLinkageNameAndAddress -->
                  <xsl:apply-templates select="prd:CorporateLinkageNameAndAddress" />
                </xsl:if>

              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>

  </xsl:template>


  <!--
  *********************************************
  * KeyPersonnelExecutiveInformation template
  *********************************************
  -->
  <xsl:template match="prd:KeyPersonnelExecutiveInformation">

    <xsl:variable name="label">
      <xsl:choose>		              
        <xsl:when test="position() &gt; 1">		    		   		   
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'Principal(s):'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="name">
      <xsl:variable name="title">
        <xsl:choose>		              
          <xsl:when test="prd:Title">		    		   		   
            <xsl:value-of select="prd:Title" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>

      <xsl:variable name="mi">
        <xsl:choose>		              
          <xsl:when test="normalize-space(substring(prd:Name,21,1)) != '' ">		    		   		   
            <xsl:value-of select="concat(substring(prd:Name,21,1), '.')" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="' '" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>

      <xsl:variable name="tmpName">
        <xsl:choose>		              
          <xsl:when test="prd:NameFlag/@code = '0'">		    		   		   
            <xsl:value-of select="concat(substring(prd:Name,1,20), $mi, substring(prd:Name,22,40), substring(prd:Name,62, 4))" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="prd:Name" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>

      <xsl:choose>		              
        <xsl:when test="prd:Name and normalize-space($title) != ''">		    		   		   
          <xsl:value-of select="concat(normalize-space($tmpName), ', ', $title)" />
        </xsl:when>

        <xsl:when test="prd:Name">
          <xsl:value-of select="$tmpName" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <tr>
      <td align="left">
        <b><xsl:value-of select="$label" /> </b></td>
      <td>&#160;</td>
      <td align="left"><xsl:value-of select="normalize-space($name)" /></td>
    </tr>
          
  </xsl:template>


  <!--
  *********************************************
  * OperatingInformation template
  *********************************************
  -->
  <xsl:template name="OperatingInformation">

    <xsl:variable name="businessType">
      <xsl:choose>		              
        <xsl:when test="prd:CorporateInformation/prd:BusinessType and normalize-space(prd:CorporateInformation/prd:BusinessType/@code) != ''">		    		   		   
    		   <xsl:call-template name="TranslateBusinessType">
    		     <xsl:with-param name="value" select="normalize-space(prd:CorporateInformation/prd:BusinessType/@code)" />
    		   </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="priorName">
      <xsl:choose>		              
        <xsl:when test="prd:AdditionalBusinessNameInformation/prd:PriorName">		    		   		   
          <xsl:value-of select="prd:AdditionalBusinessNameInformation/prd:PriorName" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="section1Presents">
      <xsl:choose>		              
        <xsl:when test="normalize-space($businessType) != '' or normalize-space($priorName) != ''">		    		   		   
          <xsl:value-of select="1" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="0" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="locationType">
      <xsl:choose>		              
        <xsl:when test="prd:DemographicInformation/prd:Location and normalize-space(prd:DemographicInformation/prd:Location/@code) != ''">		    		   		   
          <xsl:value-of select="prd:DemographicInformation/prd:Location" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="divisionName">
      <xsl:choose>		              
        <xsl:when test="prd:AdditionalBusinessNameInformation/prd:DivisionName">		    		   		   
          <xsl:value-of select="prd:AdditionalBusinessNameInformation/prd:DivisionName" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="numberSubsidiaries">
      <xsl:choose>		              
        <xsl:when test="prd:CorporateLinkageSummary/prd:NumberOfSubsidiaries">		    		   		   
          <xsl:value-of select="number(prd:CorporateLinkageSummary/prd:NumberOfSubsidiaries)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="numberBranches">
      <xsl:choose>		              
        <xsl:when test="prd:CorporateLinkageSummary/prd:NumberOfBranches">		    		   		   
          <xsl:value-of select="number(prd:CorporateLinkageSummary/prd:NumberOfBranches)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="numberCustomers">
      <xsl:choose>		              
        <xsl:when test="prd:DemographicInformation/prd:CustomerCount and number(prd:DemographicInformation/prd:CustomerCount) &gt; 0">		    		   		   
          <xsl:value-of select="number(prd:DemographicInformation/prd:CustomerCount)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="section2Presents">
      <xsl:choose>		              
        <xsl:when test="normalize-space($locationType) != '' or normalize-space($divisionName) != '' or normalize-space($numberSubsidiaries) != '' or normalize-space($numberBranches) != '' or normalize-space($numberCustomers) != ''">		    		   		   
          <xsl:value-of select="1" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="0" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:if test="normalize-space($businessType) != ''">
      <tr>
        <td align="left">
          <b>Business Type: </b><xsl:value-of select="$businessType" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($priorName) != ''">
      <tr>
        <td align="left">
          <b>Prior Company Name: </b><xsl:value-of select="$priorName" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="boolean(number($section1Presents)) and boolean(number($section2Presents))">
      <tr>
        <td class="paragraphSpacer"></td>
      </tr>
    </xsl:if>
         
    <xsl:if test="normalize-space($locationType) != ''">
      <tr>
        <td align="left">
          <b>Location Type: </b><xsl:value-of select="$locationType" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($divisionName) != ''">
      <tr>
        <td align="left">
          <b>Division Name: </b><xsl:value-of select="$divisionName" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($numberSubsidiaries) != ''">
      <tr>
        <td align="left">
          <b>Affiliated Subsidiaries: </b><xsl:value-of select="$numberSubsidiaries" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($numberBranches) != ''">
      <tr>
        <td align="left">
          <b>Affiliated Locations: </b><xsl:value-of select="$numberBranches" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($numberCustomers) != ''">
      <tr>
        <td align="left">
          <b>Number of Customers: </b><xsl:value-of select="$numberCustomers" /></td>
      </tr>
    </xsl:if>
          
  </xsl:template>


  <!--
  *********************************************
  * DemographicInformation template
  *********************************************
  -->
  <xsl:template match="prd:DemographicInformation">

    <xsl:variable name="primarySICCode">
      <xsl:choose>		              
        <xsl:when test="prd:PrimarySICCode">		    		   		   
          <xsl:value-of select="prd:PrimarySICCode" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="primarySICDescription">
      <xsl:choose>		              
        <xsl:when test="prd:SICDescription">		    		   		   
          <xsl:value-of select="prd:SICDescription" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="primarySIC">
      <xsl:choose>		              
        <xsl:when test="normalize-space($primarySICCode) != '' and normalize-space($primarySICDescription) != ''">		    		   		   
          <xsl:value-of select="concat($primarySICDescription, ' - ', $primarySICCode)" />
        </xsl:when>

        <xsl:when test="normalize-space($primarySICDescription) != ''">		    		   		   
          <xsl:value-of select="$primarySICDescription" />
        </xsl:when>

        <xsl:when test="normalize-space($primarySICCode) != ''">		    		   		   
          <xsl:value-of select="$primarySICCode" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="secondarySICCode">
      <xsl:choose>		              
        <xsl:when test="prd:SecondarySICCode">		    		   		   
          <xsl:value-of select="prd:SecondarySICCode" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="secondarySICDescription">
      <xsl:choose>		              
        <xsl:when test="prd:SecondarySICDescription">		    		   		   
          <xsl:value-of select="prd:SecondarySICDescription" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="secondarySIC">
      <xsl:choose>		              
        <xsl:when test="normalize-space($secondarySICCode) != '' and normalize-space($secondarySICDescription) != ''">		    		   		   
          <xsl:value-of select="concat($secondarySICDescription, ' - ', $secondarySICCode)" />
        </xsl:when>

        <xsl:when test="normalize-space($secondarySICDescription) != ''">		    		   		   
          <xsl:value-of select="$secondarySICDescription" />
        </xsl:when>

        <xsl:when test="normalize-space($secondarySICCode) != ''">		    		   		   
          <xsl:value-of select="$secondarySICCode" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="additionalSIC">
      <xsl:variable name="delimiter">
        <xsl:choose>		              
          <xsl:when test="prd:AdditionalSICCodes/prd:SICCode[1] and normalize-space(prd:AdditionalSICCodes/prd:SICCode[1]) != '' and prd:AdditionalSICCodes/prd:SICCode[2] and normalize-space(prd:AdditionalSICCodes/prd:SICCode[2])">		    		   		   
            <xsl:value-of select="' - '" />
          </xsl:when>

          <xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise>
        </xsl:choose>          
      </xsl:variable>
      
      <xsl:choose>		              
        <xsl:when test="prd:AdditionalSICCodes/prd:SICCode">		    		   		   
          <xsl:call-template name="JoinNodeset">
            <xsl:with-param name="nodeset" select="prd:AdditionalSICCodes/prd:SICCode" />
            <xsl:with-param name="order" select="'descending'" />
            <xsl:with-param name="delimiter" select="$delimiter" />
          </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="section1Presents">
      <xsl:choose>		              
        <xsl:when test="normalize-space($primarySIC) != '' or normalize-space($secondarySIC) != '' or normalize-space($additionalSIC) != ''">		    		   		   
          <xsl:value-of select="1" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="0" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="yearsInBusiness">
      <xsl:choose>		              
        <xsl:when test="prd:YearsInBusinessIndicator and normalize-space(prd:YearsInBusinessIndicator/@code) != ''">		    		   		   
          <xsl:choose>		              
            <xsl:when test="prd:YearsInBusinessIndicator/@code = 0">		    		   		   
              <xsl:value-of select="concat(number(prd:YearsInBusinessOrLowRange) , ' to ', number(prd:HighRangeYears))" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="number(prd:YearsInBusinessOrLowRange)" />
            </xsl:otherwise>
          </xsl:choose>          
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="employeeSize">
      <xsl:choose>		              
        <xsl:when test="prd:EmployeeIndicator and normalize-space(prd:EmployeeIndicator/@code) != ''">		    		   		   
          <xsl:choose>		              
            <xsl:when test="prd:EmployeeIndicator/@code = 0">		    		   		   
              <xsl:value-of select="concat(format-number(prd:EmployeeSizeOrLowRange, '#,##0') , ' to ', format-number(prd:HighEmployeeRange, '#,##0'))" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="format-number(prd:EmployeeSizeOrLowRange, '#,##0')" />
            </xsl:otherwise>
          </xsl:choose>          
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="sales">
      <xsl:choose>		              
        <xsl:when test="prd:SalesIndicator and normalize-space(prd:SalesIndicator/@code) != ''">		    		   		   
          <xsl:choose>		              
            <xsl:when test="prd:SalesIndicator/@code = 0">		    		   		   
              <xsl:value-of select="concat(format-number(prd:SalesRevenueOrLowRange, '$#,##0') , ' to ', format-number(prd:HighRangeOfSales, '$#,##0'))" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="format-number(prd:SalesRevenueOrLowRange, '$#,##0')" />
            </xsl:otherwise>
          </xsl:choose>          
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="netWorth">
      <xsl:choose>		              
        <xsl:when test="prd:NetWorthIndicator and normalize-space(prd:NetWorthIndicator/@code) != ''">		    		   		   
          <xsl:choose>		              
            <xsl:when test="prd:NetWorthIndicator/@code = 0">		    		   		   
              <xsl:value-of select="concat(prd:NetWorthAmountOrLowRange/prd:Modifier, format-number(prd:NetWorthAmountOrLowRange/prd:Amount, '$#,##0') , ' to ', prd:HighRangeOrNetWorth/prd:Modifier, format-number(prd:HighRangeOrNetWorth/prd:Amount, '$#,##0'))" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="concat(prd:NetWorthAmountOrLowRange/prd:Modifier, format-number(prd:NetWorthAmountOrLowRange/prd:Amount, '$#,##0'))" />
            </xsl:otherwise>
          </xsl:choose>          
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="section2Presents">
      <xsl:choose>		              
        <xsl:when test="normalize-space($yearsInBusiness) != '' or normalize-space($employeeSize) != '' or normalize-space($sales) != '' or normalize-space($netWorth) != ''">		    		   		   
          <xsl:value-of select="1" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="0" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="buildingSize">
      <xsl:choose>		              
        <xsl:when test="prd:BuildingSize and prd:BuildingSize &gt; 0">		    		   		   
          <xsl:value-of select="number(prd:BuildingSize)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="buildingOwnership">
      <xsl:choose>		              
        <xsl:when test="prd:BuildingOwnership and normalize-space(prd:BuildingOwnership/@code) != ''">		    		   		   
          <xsl:value-of select="prd:BuildingOwnership" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="section3Presents">
      <xsl:choose>		              
        <xsl:when test="normalize-space($buildingSize) != '' or normalize-space($buildingOwnership) != ''">		    		   		   
          <xsl:value-of select="1" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="0" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:if test="normalize-space($primarySIC) != ''">
      <tr>
        <td align="left">
          <b>Primary SIC Code: </b><xsl:value-of select="$primarySIC" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($secondarySIC) != ''">
      <tr>
        <td align="left">
          <b>Secondary SIC Code: </b><xsl:value-of select="$secondarySIC" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($additionalSIC) != '' and normalize-space($additionalSIC) != '-' ">
      <tr>
        <td align="left">
          <b>Additional SIC Code: </b><xsl:value-of select="$additionalSIC" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="boolean(number($section1Presents)) and boolean(number($section2Presents))">
      <tr>
        <td class="paragraphSpacer"></td>
      </tr>
    </xsl:if>
         
    <xsl:if test="normalize-space($yearsInBusiness) != ''">
      <tr>
        <td align="left">
          <b>Years in Business: </b><xsl:value-of select="$yearsInBusiness" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($employeeSize) != ''">
      <tr>
        <td align="left">
          <b>Number of Employees: </b><xsl:value-of select="$employeeSize" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($sales) != ''">
      <tr>
        <td align="left">
          <b>Sales: </b><xsl:value-of select="$sales" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($netWorth) != ''">
      <tr>
        <td align="left">
          <b>Net Worth: </b><xsl:value-of select="$netWorth" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="(boolean(number($section1Presents)) or boolean(number($section2Presents))) and boolean(number($section3Presents))">
      <tr>
        <td class="paragraphSpacer"></td>
      </tr>
    </xsl:if>
         
    <xsl:if test="normalize-space($buildingSize) != ''">
      <tr>
        <td align="left">
          <b>Location Square Footage: </b><xsl:value-of select="$buildingSize" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($buildingOwnership) != ''">
      <tr>
        <td align="left">
          <b>Ownership: </b><xsl:value-of select="$buildingOwnership" /></td>
      </tr>
    </xsl:if>

  </xsl:template>


  <!--
  *********************************************
  * CorporateLinkageNameAndAddress template
  *********************************************
  -->
  <xsl:template match="prd:CorporateLinkageNameAndAddress">
    <xsl:variable name="locationType">
      <xsl:choose>		              
        <xsl:when test="prd:RecordType and normalize-space(prd:RecordType/@code) != ''">		    		   		   
          <xsl:value-of select="prd:RecordType" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="fileNumber">
      <xsl:choose>		              
        <xsl:when test="prd:ExperianFileNumber">		    		   		   
          <xsl:value-of select="prd:ExperianFileNumber" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="companyName">
      <xsl:choose>		              
        <xsl:when test="prd:Name">		    		   		   
          <xsl:value-of select="prd:Name" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="address">
      
      <xsl:variable name="street">
        <xsl:choose>		              
          <xsl:when test="prd:Address">		    		   		   
            <xsl:value-of select="prd:Address" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>

      <xsl:variable name="city">
        <xsl:choose>		              
          <xsl:when test="prd:City">		    		   		   
            <xsl:value-of select="prd:City" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>

      <xsl:variable name="state">
        <xsl:choose>		              
          <xsl:when test="prd:State">		    		   		   
            <xsl:value-of select="prd:State" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>
      
      <xsl:variable name="zip">
        <xsl:choose>		              
          <xsl:when test="prd:Zip">		    		   		   
            <xsl:value-of select="prd:Zip" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>
      
      <xsl:choose>		              
        <xsl:when test="normalize-space($street) != '' or normalize-space($city) != '' or normalize-space($state) != '' or normalize-space($zip) != ''">		    		   		   
          <xsl:value-of select="concat($street, $city, $state, $zip)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="stateIncorporation">
      <xsl:choose>		              
        <xsl:when test="prd:IncorporationState">		    		   		   
			  <xsl:call-template name="TranslateState">
			    <xsl:with-param name="value" select="prd:IncorporationState" />
			    <xsl:with-param name="upperCase" select="true()" />
			  </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="dateIncorporation">
      <xsl:choose>		              
        <xsl:when test="prd:IncorporatedDate and number(prd:IncorporatedDate) != 0">		    		   		   
    		   <xsl:call-template name="FormatDate">
    		     <xsl:with-param name="pattern" select="'mo/dt/year'" />
    		     <xsl:with-param name="value" select="prd:IncorporatedDate" />
    		   </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="publicRecord">
      <xsl:choose>		              
        <xsl:when test="prd:SignificantPublicRecordIndicator">		    		   		   
          <xsl:choose>		              
            <xsl:when test="prd:SignificantPublicRecordIndicator = 'Y'">		    		   		   
              <xsl:value-of select="'YES'" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="'NO'" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:if test="position() &gt; 1">
      <tr>
        <td class="paragraphSpacer"></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($locationType) != ''">
      <tr>
        <td align="left">
          <b>Location Type: </b><xsl:value-of select="$locationType" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($fileNumber) != ''">
      <tr>
        <td align="left">
          <b>Experian File No: </b><xsl:value-of select="$fileNumber" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($companyName) != ''">
      <tr>
        <td align="left">
          <b>Company Name: </b><xsl:value-of select="$companyName" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($address) != ''">
      <tr>
        <td align="left">
          <b>Address: </b><xsl:value-of select="$address" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($stateIncorporation) != ''">
      <tr>
        <td align="left">
          <b>State of Incorporation: </b><xsl:value-of select="$stateIncorporation" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($dateIncorporation) != ''">
      <tr>
        <td align="left">
          <b>Date of Incorporation: </b><xsl:value-of select="$dateIncorporation" /></td>
      </tr>
    </xsl:if>

    <xsl:if test="normalize-space($publicRecord) != ''">
      <tr>
        <td align="left">
          <b>Significant Derogatory Filings: </b><xsl:value-of select="$publicRecord" /></td>
      </tr>
    </xsl:if>

  </xsl:template>

</xsl:stylesheet>