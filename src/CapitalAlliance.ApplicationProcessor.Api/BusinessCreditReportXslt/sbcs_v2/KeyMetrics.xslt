<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->


  <!--
  *********************************************
  * KeyMetrics template
  *********************************************
  -->
  <xsl:template name="KeyMetrics">

    <xsl:if test="prd:SBCSKeyMetrics">
      <table class="section" width="100%" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th><a class="report_section_title">SBCS Key Metrics</a></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <!-- SBCSKeyMetrics template -->
              <xsl:apply-templates select="prd:SBCSKeyMetrics" />
            </td>
          </tr>
        </tbody>
      </table>
      
      <xsl:call-template name="BackToTop" />
    </xsl:if>

    <xsl:if test="prd:SBCSExecutiveElements or (prd:SBCSQuarterlyPaymentTrends and prd:SBCSPaymentTrends and prd:SBCSExecutiveSummary and prd:SBCSExecutiveElements and prd:SBCSPaymentTotals)">
      <xsl:if test="prd:SBCSExecutiveElements">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        
          <tr>
            <!-- Public Record column -->
            <td width="49%" valign="top">
              <xsl:call-template name="CurrentDBT" />
            </td>
        
            <td width="2%">
            </td>
        
            <!-- Performance Analysis column -->
            <td width="49%" valign="top">
              <xsl:call-template name="PerformanceAnalysis" />
            </td>
          </tr>
    
          <xsl:if test="prd:SBCSExecutiveElements">
            <tr>
              <td colspan="3" class="footnote">
                <i>
              * Days Beyond Terms (DBT) is a dollar weighted calculation of the average number of 
                days that payment was made beyond the invoice due date based on trades on file that 
                have been updated in the previous 3 months.
                </i>
              </td>
            </tr>
          </xsl:if>    
              
        </table>
  
      </xsl:if>
      
    <xsl:call-template name="BackToTop" />
  </xsl:if>
  </xsl:template>

</xsl:stylesheet>