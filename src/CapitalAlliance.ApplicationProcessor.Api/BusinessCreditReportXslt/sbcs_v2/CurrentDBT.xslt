<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"                
                xmlns:prd="http://www.experian.com/ARFResponse">

  <!--
  *********************************************
  * Output method
  *********************************************
  -->



  <!--
  *********************************************
  * CurrentDBT template
  *********************************************
  -->
  <xsl:template name="CurrentDBT">
  
    <xsl:variable name="currentDBT">
      <xsl:choose>                  
        <xsl:when test="normalize-space(prd:SBCSExecutiveElements/prd:CurrentDBT) != ''">                    
          <xsl:value-of select="number(prd:SBCSExecutiveElements/prd:CurrentDBT)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="dbtPointer">
      <xsl:choose>                  
        <xsl:when test="$currentDBT &gt;= 0 and $currentDBT &lt;= 15">                     
          <xsl:value-of select="1" />
        </xsl:when>

        <xsl:when test="$currentDBT &gt;= 16 and $currentDBT &lt;= 50">                    
          <xsl:value-of select="2" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="3" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="accountBalance">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveElements/prd:CurrentAccountBalance">                    
          <xsl:value-of select="format-number(prd:SBCSExecutiveElements/prd:CurrentAccountBalance, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="allIndustryDBT">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveSummary/prd:AllIndustryDBT">                    
          <xsl:value-of select="concat(number(prd:SBCSExecutiveSummary/prd:AllIndustryDBT), ' DBT')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="industryDBT">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveSummary/prd:IndustryDBT">                     
          <xsl:value-of select="concat(number(prd:SBCSExecutiveSummary/prd:IndustryDBT), ' DBT')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="monthlyAvgDBT">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveElements/prd:MonthlyAverageDBT and normalize-space($currentDBT) != ''">                    
          <xsl:value-of select="number(prd:SBCSExecutiveElements/prd:MonthlyAverageDBT)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="highestDBT6M">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveElements/prd:HighestDBT6Months and normalize-space($currentDBT) != ''">                    
          <xsl:value-of select="number(prd:SBCSExecutiveElements/prd:HighestDBT6Months)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="highestDBT5Q">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveElements/prd:HighestDBT5Quarters and normalize-space($currentDBT) != ''">                    
          <xsl:value-of select="number(prd:SBCSExecutiveElements/prd:HighestDBT5Quarters)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="highestDBTEver">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:HighestDBTEver and normalize-space($currentDBT) != ''">                    
          <xsl:value-of select="number(prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:HighestDBTEver)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="predictedDBT">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveSummary/prd:PredictedDBT">                    
          <xsl:value-of select="concat(number(prd:SBCSExecutiveSummary/prd:PredictedDBT), ' DBT')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="predictedDBTDate">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveSummary/prd:PredictedDBTDate">                    
          <xsl:variable name="date">
             <xsl:call-template name="FormatDate">
               <xsl:with-param name="pattern" select="'mo/dt/year'" />
               <xsl:with-param name="value" select="prd:SBCSExecutiveSummary/prd:PredictedDBTDate" />
             </xsl:call-template>
          </xsl:variable>
          <xsl:value-of select="concat(' for ', $date)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <table class="section" width="100%" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <th><a name="DBTDetails" style="background:none">Current DBT Range (Non-financial Accounts) *</a></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="box_padding">
            <table width="100%" border="0" cellspacing="0" cellpadding="1">
            
              <tr>
                <td>
                  <div style="width:300px; text-align:center; margin:auto;">
                    <xsl:choose>                
                      <xsl:when test="normalize-space($currentDBT) != ''">                
                        <xsl:choose>                
                          <xsl:when test="$dbtPointer = 1">                
                            <div style="width:60px; float:right;">&#160;</div>
                            <div style="width:240px;"><b><span class="font_size_3"><xsl:value-of select="$currentDBT" /> DBT</span></b><br />
                                      (<xsl:value-of select="$accountBalance" /> Balance)</div>
                            <div style="clear:both;"></div>    
                          </xsl:when>
                  
                          <xsl:otherwise>
                            <div style="text-align:right;">
                              <div class="block">
                                <div style="text-align:center;">
                                  <span class="font_size_3"><b><xsl:value-of select="$currentDBT" /> DBT</b></span>
                                </div>
                                <div style="text-align:center;">
                                   (<xsl:value-of select="$accountBalance" /> Balance)
                                </div>
                              </div>
                            </div>
                          </xsl:otherwise>
                        </xsl:choose>    

                      </xsl:when>
              
                      <xsl:otherwise>
                        <div style="margin:3px 0 12px;" class="font_size_2"><b>Current DBT cannot be calculated.</b></div>
                      </xsl:otherwise>
                    </xsl:choose>    

                  </div>
                </td>
              </tr>  

              <xsl:if test="normalize-space($currentDBT) != ''">                
                <tr>
                  <td style="padding:0;">
                    <!-- pointer -->
                    <div style="width:300px; text-align:center; margin:auto;">
                      <xsl:call-template name="PointerLoop">
                        <xsl:with-param name="dbtPointer" select="$dbtPointer" />
                      </xsl:call-template>
                      <div style="clear:both;"></div>
                    </div>
                  </td>
                </tr>  
              </xsl:if>

              <tr>
                <td style="padding:0;">
                    <!-- color bar-->
                    <div class="legacyDBTBar">
                      <div class="block riskLow">
                        0-15
                      </div>
                      <div class="block riskMedium">
                        16-50
                      </div>
                      <div class="block riskHigh">
                        51+
                      </div>
                      <div style="clear:both;"></div>
                    </div>
                    <!-- end color bar-->
                    
                    <!-- color bar labels -->
                    <div style="width:300px; text-align:center; margin:1px auto auto;">
                      <div class="block" style="float:left; width:240px;">
                        <b>80%</b>
                      </div>
                      <div class="block" style="float:left; width:33px;">
                        <b>11%</b>
                      </div>
                      <div class="block" style="float:left; width:27px;">
                        <b>9%</b>
                      </div>
                      <div style="clear:both;"></div>
                    </div>
                    <!-- end color bar labels -->
                </td>
              </tr>  

              <tr>
                <td>  
                  <table width="100%" border="0" cellspacing="0" cellpadding="1">
                               
                    <tr>  
                      <td colspan="2" align="center" width="100%">
                        <b>% of US businesses falling within DBT range</b>
                      </td>
                    </tr>
                    <tr> 
                      <td width="100%" colspan="2" style="padding-top:8px;"> <b>DBT Norms:</b>
                      </td>
                    </tr>
                    <tr>  
                      <td nowrap="nowrap">
                        All industries: <b><xsl:value-of select="$allIndustryDBT" /></b> 
                      </td>
                        
                      <td aligh="left" nowrap="nowrap">
                        Same industry: <b><xsl:value-of select="$industryDBT" /></b>
                     </td>
                    </tr>
                    
                  </table> 
                </td>
              </tr>    

              <tr>
                <td>  
                  <table width="100%" border="0" cellspacing="0" cellpadding="1" style="margin-top:3px;">
                    <colgroup style="width:70%"/>
                    <colgroup style="width:30%" />
                    
                    <tr>  
                      <td colspan="2">
                        <b>DBT Performance Analysis:</b></td>
                    </tr>

                    <tr>
                      <td>
                        Monthly average DBT:</td>
                      <td align="right"><b><xsl:value-of select="$monthlyAvgDBT" /></b></td>
                    </tr>
                    
                    <tr>
                      <td>
                        Highest DBT previous 6 months:</td>
                      <td align="right"><b><xsl:value-of select="$highestDBT6M" /></b></td>
                    </tr>
                  
                    
                    <tr>
                      <td>
                      Highest DBT previous 5 quarters:</td>
                      <td align="right"><b><xsl:value-of select="$highestDBT5Q" /></b></td>
                    </tr>
                  
                    
                    <tr>
                      <td>
                      Highest DBT ever:</td>
                      <td align="right"><b><xsl:value-of select="$highestDBTEver" /></b></td>
                    </tr>
                  
                    
                    <tr>
                      <td>
                      Predicted DBT<xsl:value-of select="$predictedDBTDate" />:</td>
                      <td align="right"><b><xsl:value-of select="$predictedDBT" /></b></td>
                    </tr>
                  
                  </table> 
                </td>
              </tr>    
            </table>
          </td>
        </tr>
      </tbody>
    </table>

  </xsl:template>


  <!--
  *********************************************
  * PointerLoop template
  *********************************************
  -->
  <xsl:template name="PointerLoop">
    <xsl:param name="dbtPointer" select="''" />
    <xsl:param name="index" select="1" />
    
    <xsl:variable name="width">
      <xsl:choose>
        <xsl:when test="$index = 1">
          <xsl:value-of select="240" />
        </xsl:when>
        <xsl:when test="$index = 2">
          <xsl:value-of select="33" />
        </xsl:when>
        <xsl:when test="$index = 3">
          <xsl:value-of select="27" />
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="float">
      <xsl:choose>
        <xsl:when test="$index = 1">
          <xsl:value-of select="'float:left;'" />
        </xsl:when>
        <xsl:when test="$index = 3">
          <xsl:value-of select="'float:right;'" />
        </xsl:when>
      </xsl:choose>
    </xsl:variable>
    
    <div class="block" style="float:left; width:{$width}px; height:15px;">
      <xsl:choose>
        <xsl:when test="$dbtPointer = $index">
          <img src="../images/triangle_blue.gif" border="0" width="3" height="14" alt=""/>
        </xsl:when>
        <xsl:otherwise>
          &#160;
        </xsl:otherwise>
      </xsl:choose>
    </div>

    <!-- Test condition and call template if less than number -->
    <xsl:if test="$index &lt; 3">
      <xsl:call-template name="PointerLoop">
        <xsl:with-param name="index" select="($index + 1)" />
        <xsl:with-param name="dbtPointer" select="$dbtPointer" />
      </xsl:call-template>
    </xsl:if>
    
  </xsl:template>

      
</xsl:stylesheet>