<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->


  <xsl:template name="CreditRiskScoreDetails">

    <xsl:variable name="sectionTitle">
      <xsl:value-of select="'Credit Risk Score Summary'"/>
    </xsl:variable>

    <xsl:variable name="modelTypeText">
      <xsl:choose>
        <xsl:when test="$isCommercial = 1">
          <xsl:value-of select="' - Commercial Model'" />
        </xsl:when>

        <xsl:when test="$isBlendedModel = 1">
          <xsl:value-of select="' - Blended Model'" />
        </xsl:when>

        <xsl:when test="$isOwnerGuarantorModel = 1">
          <xsl:value-of select="' - Business Owner/Guarantor Model'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

  <table class="section pageBreak" width="100%" cellspacing="0" cellpadding="0">
    <colgroup style="width:50%"/>
    <colgroup style="width:50%" />
    <thead>
      <tr>
        <th colspan="2">
          <xsl:comment>For left side label</xsl:comment>
          <a name="RiskScore" style="background:none"><a class="report_section_title"><xsl:value-of select="$sectionTitle"/></a></a>
          <xsl:if test="normalize-space($modelTypeText) != ''">
            <span class="label"><xsl:value-of select="$modelTypeText"/></span>
          </xsl:if>
        </th>
      </tr>
    </thead>

      <xsl:apply-templates select="prd:IntelliscoreScoreInformation" mode="ScoreDetails" >
      </xsl:apply-templates>

  </table>
  </xsl:template>



  <!--
  *********************************************
  * Business Facts template
  *********************************************
  -->
  <xsl:template match="prd:IntelliscoreScoreInformation" mode="ScoreDetails">

    <xsl:variable name="model">
      <xsl:choose>
        <xsl:when test="prd:ModelInformation/prd:ModelCode">
          <xsl:value-of select="number(prd:ModelInformation/prd:ModelCode)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="-1"/>
      </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="score">
      <xsl:value-of select="number(prd:ScoreInfo/prd:Score) div 100" />
    </xsl:variable>

    <xsl:variable name="scoreText">
      <xsl:choose>
        <xsl:when test="contains($score, '.')">
          <xsl:value-of select="normalize-space(substring-before($score, '.'))" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$score" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="scoreHeaderText">
      <xsl:choose>
        <xsl:when test="$score=$score998">
          <xsl:value-of select="'This is an exclusion report (998) due to a bankruptcy reported within the last 24 months on the inquired business. &lt;br/&gt;Therefore an SBCS V2 Acquisition Score cannot be calculated.'"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$score=$score999">
          <xsl:value-of select="'This is an exclusion report (999). This business does not have the information necessary to statistically predict &lt;br/&gt;serious future delinquency. Therefore an SBCS V2 Acquisition Score cannot be calculated.'"></xsl:value-of>
        </xsl:when>
        <xsl:when test="$model=$sbcsV2Model">
          <xsl:value-of select="'This V2 score predicts the likelihood of serious credit delinquencies for this business within the next 24 months.  Payment history and public record along with other variables are used to predict future risk.  Higher scores indicate lower risk.'"></xsl:value-of>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="scoreName">
        <xsl:value-of select="'SBCS V2 Acquisition'" />
    </xsl:variable>

    <xsl:variable name="scoreMeterClass">
        <xsl:value-of select="'meter214'"/>
    </xsl:variable>

  <xsl:variable name="riskClass">
      <xsl:choose>
        <xsl:when test="prd:RiskClass">
          <xsl:value-of select="prd:RiskClass" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="RiskClassByScore">
            <xsl:with-param name="model" select="$model" />
            <xsl:with-param name="score" select="$score" />
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
  </xsl:variable>

    <xsl:variable name="action">
      <xsl:choose>
        <xsl:when test="prd:Action">
          <xsl:value-of select="normalize-space(prd:Action)" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="actionUpperCase">
      <xsl:value-of select="translate($action, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')" />
    </xsl:variable>

    <xsl:variable name="customAction">
      <xsl:choose>
        <xsl:when test="$score!=$score998 and $score!=$score999 
                        and $actionUpperCase != $lowRiskText
                        and $actionUpperCase != $lowMedRiskText
                        and $actionUpperCase != $medRiskText
                        and $actionUpperCase != $medHighRiskText
                        and $actionUpperCase != $highRiskText">
          <xsl:value-of select="$action" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

  <xsl:variable name="actionColor">
    <xsl:choose>
      <xsl:when test="$riskClass = 1">
        <xsl:value-of select="$lowRiskColor"></xsl:value-of>
      </xsl:when>
      <xsl:when test="$riskClass = 2">
        <xsl:value-of select="lowMedRiskColor"></xsl:value-of>
      </xsl:when>
      <xsl:when test="$riskClass = 3">
        <xsl:value-of select="medRiskColor"></xsl:value-of>
      </xsl:when>
      <xsl:when test="$riskClass = 4">
        <xsl:value-of select="medHighRiskColor"></xsl:value-of>
      </xsl:when>
      <xsl:when test="$riskClass = 5">
        <xsl:value-of select="highRiskColor"></xsl:value-of>
      </xsl:when>
      <xsl:otherwise><xsl:value-of select="'#cccccc'"></xsl:value-of></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="actionImage">
    <xsl:choose>
      <xsl:when test="$riskClass = 1">
        <xsl:value-of select="concat($basePath,'low-risk.gif')"/>
      </xsl:when>
      <xsl:when test="$riskClass = 2">
        <xsl:value-of select="concat($basePath,'low-medium-risk.gif')"/>
      </xsl:when>
        <xsl:when test="$riskClass = 3">
        <xsl:value-of select="concat($basePath,'medium-risk.gif')"/>
      </xsl:when>
      <xsl:when test="$riskClass = 4">
        <xsl:value-of select="concat($basePath,'medium-high-risk.gif')"/>
      </xsl:when>
      <xsl:when test="$riskClass = 5">
        <xsl:value-of select="concat($basePath,'high-risk.gif')"/>
      </xsl:when>
      <xsl:otherwise><xsl:value-of select="''"></xsl:value-of></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  
    <xsl:variable name="percentRanking">
      <xsl:choose>
        <xsl:when test="prd:PercentileRanking">
          <xsl:value-of select="format-number(prd:PercentileRanking div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="Probability">
      <xsl:choose>
        <xsl:when test="prd:Probability">
          <xsl:value-of select="concat(prd:Probability/prd:Amount, ':1')" />  <!-- ??? or prd:Probability/prd:Text ???  -->
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'???'" /> <!-- What to do here ??? -->
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

  <tbody>
  </tbody>
  
  <tbody class="pageNobreak">
  <xsl:if test="$score=$score998 or $score=$score999">
    <tr><td colspan="2" style="padding:5px 0 10px 5px;"><div><b><xsl:value-of select="$scoreHeaderText" disable-output-escaping="yes"></xsl:value-of></b></div></td></tr>
  </xsl:if>

  <xsl:if test="$score!=$score998 and $score!=$score999">
  <tr>
    <td class="label graphicTitle" style="padding:5px 0 0 5px;">Current <xsl:value-of select="$scoreName"></xsl:value-of> Score: <xsl:value-of select="$scoreText"></xsl:value-of></td>
    <td class="label graphicTitle" style="padding:5px 0 0 0;">Risk Class: <xsl:value-of select="$riskClass"></xsl:value-of></td>
  </tr>
  </xsl:if>
  
  <tr>
    <td style="padding:0 0 0 5px;">
      <div>
        <xsl:attribute name="class">
          <xsl:value-of select="concat('scoreGraphic',' ',$scoreMeterClass)"/>
        </xsl:attribute>
        <xsl:if test="$score!=$score998 and $score!=$score999">
          <div class="scoreValue">
            <xsl:attribute name="style">
              <xsl:value-of select="concat('left:',$scoreText*$scoreMeterWidth div 100+42-16,'px')"></xsl:value-of>
            </xsl:attribute>
            <xsl:value-of select="$scoreText"></xsl:value-of>
          </div>
          <div>
            <xsl:attribute name="class">scoreValueArrow</xsl:attribute>
            <xsl:attribute name="style">
              <xsl:value-of select="concat('left:',$scoreText*$scoreMeterWidth div 100+42-6,'px')"></xsl:value-of>
            </xsl:attribute>
            <xsl:value-of select="'&#160;'"/>
          </div>
        </xsl:if>
        <div class="scoreMeter">&#160;</div>
      </div>
    </td>

    <td style="padding:0 5px 0 0;">
      <xsl:if test="$score!=$score998 and $score!=$score999">
          <div style="padding-top:3px;">
            <xsl:element name="img">
              <xsl:attribute name="style">border:none</xsl:attribute>
              <xsl:attribute name="src">
                <xsl:value-of select="$actionImage"></xsl:value-of>
              </xsl:attribute>
            </xsl:element>
          </div>

          <div style="padding-top:2px;">The risk class groups scores by risk into ranges of similar performance.
          Range 5 is the highest risk, range 1 is the lowest risk.</div>
          
          <br/>
          <!-- 
          **** Below is disabled as we don't have data yet. ****
          <div class="label"><xsl:value-of select="$scoreName"></xsl:value-of> Delinquency Rate</div>
          <div>At the <b>50th</b> percentile good/bad odds are <xsl:value-of select="$Probability"></xsl:value-of>
            < ! - -<span><b>...</b></span>--><!-- @TODO 30:1 - ->
            with an average delinquency rate of <b><xsl:value-of select="$percentRanking"></xsl:value-of></b>
          </div>
          -->
      </xsl:if>
    </td>
  </tr>
  </tbody>
  
  <tbody>
  <tr>
    <td style="padding:0 0 0 5px;">
      
      <xsl:if test="$score = $score998 or $score = $score999">
        <div style="padding:3px 0 0"></div>
      </xsl:if>
      
      <xsl:if test="$score != $score998 and $score != $score999">
        <div style="padding-bottom:10px;"><xsl:value-of select="$scoreHeaderText" disable-output-escaping="yes"></xsl:value-of>
        </div>
      </xsl:if>
      
    </td>

    <td style="padding:0 5px 0 0;">
    </td>

  </tr>
  
  <xsl:if test="$score != $score998 and $score != $score999 and (../prd:ScoreFactors/prd:ScoreFactor or $percentRanking!='')">
    <tr>
      <td class="firstColumn">
        <xsl:if test="../prd:ScoreFactors/prd:ScoreFactor">
          <div class="label">Factors lowering the score</div>
          <ul class="list">
            <xsl:for-each select="../prd:ScoreFactors/prd:ScoreFactor">
              <li><xsl:value-of select="text()"></xsl:value-of></li>
            </xsl:for-each>
          </ul>
        </xsl:if>
      </td>
      <td>
        <xsl:if test="$percentRanking!=''">
        <div class="label">
          Industry Risk Comparison</div>
        <div><b><xsl:value-of select="$percentRanking"/></b> of businesses indicate a higher likelihood of severe delinquency.
        </div>
        <br/>
        </xsl:if>
      </td>
    </tr>
  </xsl:if>
  
  <xsl:if test="normalize-space($customAction) != ''">
     <tr>
       <td colspan="2">
         <div class="paragraphSpacer"><span><!-- DO NOT REMOVE THIS EMPTY TAG --></span></div>
         <div style="border:1px solid {$borderColor}; margin: 0pt auto 4px; width: 90%; vertical-align: middle; height: 37px; text-align: center; padding:5px;">
           <div class="label">   
             Based on your company's action or risk threshold, this business
             falls within the following category:
           </div>
           
           <div class="alert font_size_3 label" style="padding-top:4px;">
             <xsl:value-of select="$action" />
           </div>
         </div>
       </td>
     </tr>
  </xsl:if>
  
  <tr><td class="paragraphSpacer" colspan="2"></td></tr>

  <xsl:if test="../prd:ScoreTrendsCreditLimit and $model != $fsrModel and $product != 'SBCSScore'">
    <xsl:apply-templates select="../prd:ScoreTrendsCreditLimit" />
  </xsl:if>  
  <xsl:if test="../prd:FSRScoreTrendsCreditLimit and $model = $fsrModel and $product != 'SBCSScore'">
    <xsl:apply-templates select="../prd:FSRScoreTrendsCreditLimit" />
  </xsl:if>  
  </tbody>
  </xsl:template>
  

  <xsl:template match="prd:ScoreTrendsCreditLimit | prd:FSRScoreTrendsCreditLimit">
    <xsl:variable name="title">
        <xsl:choose>
          <xsl:when test="name() = 'ScoreTrendsCreditLimit'">
            <xsl:value-of select="'Small Business Credit Share Quarterly Score Trends'"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="'Financial Stability Risk Quarterly Score Trends'"/>
          </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <tr class="subtitle">
      <th colspan="2"><xsl:value-of select="$title"/></th>
    </tr>
    <tr>    
      <td>
        <QSTChart>
            <div id="chartTitle" style="display: none;">Quarterly Score Trends</div>
            <!--Div that will hold the bar chart data  for Quarterly Score Trends -->
            <div id="table_STCL" style="display: none;">
                <ScoreTrendsCreditLimit>
                    <xsl:for-each select="prd:MostRecentQuarter | prd:PriorQuarter">
                        <xsl:apply-templates select="current()" mode="LoadTableSTCLV2"/>
                    </xsl:for-each>
                </ScoreTrendsCreditLimit>
            </div>
            <!--Div that will hold the bar chart for Quarterly Score Trends -->
            <div id="bar_chart_QST" style="width: 400px; height: 300px;"></div>
        </QSTChart>
      </td>

      <td style="vertical-align:middle;padding-right:5px;">
        <xsl:choose>
          <xsl:when test="name() = 'ScoreTrendsCreditLimit'">
            The Small Business Credit Share Quarterly Score Trend indicates if the score improved, remained stable, fluctuated or declined over the last 12 months.
          </xsl:when>
          <xsl:otherwise>
            The Financial Stability Risk Quarterly Score Trends provide a view of the likelihood of financial stability risk over the past 12 months for this business. The trends will indicate if the score improved, remained stable, fluctuated or declined over the last 12 months.
          </xsl:otherwise>
        </xsl:choose>
      </td>
    </tr>

    <tr><td class="subsectionSpacer" colspan="2"></td></tr>
  </xsl:template>
  
  
  <xsl:template match="prd:PriorQuarter | prd:MostRecentQuarter">
  <set>
    <xsl:variable name="noScoreNote">
      <xsl:choose>
        <xsl:when test="number(prd:Score) &lt; 0 or number(prd:Score) &gt; 100">
          <xsl:value-of select="'*'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:attribute name="name"><xsl:value-of select="prd:Quarter"/><xsl:value-of select="$noScoreNote"/></xsl:attribute>
    <xsl:attribute name="value">
    <xsl:choose>
      <xsl:when test="number(prd:Score) &lt; 0 or number(prd:Score) &gt; 100">
      <xsl:value-of select="''"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="prd:Score"/>
      </xsl:otherwise>
    </xsl:choose>
    </xsl:attribute>
  &#160;</set>
  </xsl:template>
  
  
  <!--
  ****************************************************
  * MostRecentQuarter | PriorQuarter template
  * mode="LoadTableQDT"
  * Build XML for Google Charts
  ****************************************************
  -->
  <xsl:template match="prd:MostRecentQuarter | prd:PriorQuarter" mode="LoadTableSTCLV2">
	<xsl:variable name="dateQQYY">
	  <xsl:value-of select="prd:Quarter" />
	</xsl:variable>
	
	<xsl:variable name="score">
	  <xsl:value-of select="prd:Score" />
	</xsl:variable>


	<xsl:choose>
		<xsl:when test="prd:MostRecentQuarter">
			<MostRecentQuarter>
				<Quarter><xsl:value-of select="$dateQQYY" /></Quarter>
				<Score><xsl:value-of select="$score" /></Score>
			</MostRecentQuarter>
		</xsl:when>
		<xsl:otherwise>
			<PriorQuarter>
				<Quarter><xsl:value-of select="$dateQQYY" /></Quarter>			
				<Score><xsl:value-of select="$score" /></Score>
			</PriorQuarter>
		</xsl:otherwise>
	</xsl:choose>
	
	
	
  </xsl:template>
  
  
</xsl:stylesheet>
