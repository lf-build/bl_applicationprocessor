<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">
                

  <!--
  *********************************************
  * Output method
  *********************************************
  -->



  <!--
  *********************************************
  * PublicRecord template
  *********************************************
  -->
  <xsl:template name="PublicRecord">

    <xsl:variable name="bankruptcy">
      <xsl:choose>
        <xsl:when test="number(prd:SBCSExecutiveElements/prd:BankruptcyFilingCount) = 0 and prd:SBCSExecutiveElements/prd:BankruptcyFlag and prd:SBCSExecutiveElements/prd:BankruptcyFlag = 'Y'">      
          <xsl:value-of select="'Closed'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="number(prd:SBCSExecutiveElements/prd:BankruptcyFilingCount)" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="bankruptcyDates">
      <xsl:choose>                  
        <xsl:when test="$bankruptcy &gt; 0 and prd:SBCSExecutiveElements/prd:EarliestBankruptcyDate and number(prd:SBCSExecutiveElements/prd:EarliestBankruptcyDate) != 0">
        
          <xsl:variable name="oldDate">
            <xsl:call-template name="FormatDate">
              <xsl:with-param name="pattern" select="'mo/yr'" />
              <xsl:with-param name="value" select="prd:SBCSExecutiveElements/prd:EarliestBankruptcyDate" />
            </xsl:call-template>
          </xsl:variable>
        
          <xsl:variable name="recentDate">
            <xsl:choose>                  
              <xsl:when test="prd:SBCSExecutiveElements/prd:MostRecentBankruptcyDate and number(prd:SBCSExecutiveElements/prd:MostRecentBankruptcyDate) != 0">
               <xsl:call-template name="FormatDate">
                 <xsl:with-param name="pattern" select="'mo/yr'" />
                 <xsl:with-param name="value" select="prd:SBCSExecutiveElements/prd:MostRecentBankruptcyDate" />
               </xsl:call-template>
              </xsl:when>
      
              <xsl:otherwise>
                <xsl:value-of select="''" />
              </xsl:otherwise>
            </xsl:choose>                
          </xsl:variable>
                       
          <xsl:choose>                  
            <xsl:when test="normalize-space($oldDate) != normalize-space($recentDate) and normalize-space($recentDate) != ''">                     
              <xsl:value-of select="concat('(FILED ', normalize-space($oldDate), '-', normalize-space($recentDate), ')')" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="concat('(FILED ', normalize-space($oldDate), ')')" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="lien">
      <xsl:choose>
        <xsl:when test="number(prd:SBCSExecutiveElements/prd:TaxLienCount) = 0 and prd:SBCSExecutiveElements/prd:TaxLienFlag and prd:SBCSExecutiveElements/prd:TaxLienFlag = 'Y'">      
          <xsl:value-of select="'Released'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="number(prd:SBCSExecutiveElements/prd:TaxLienCount)" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="lienDates">
      <xsl:choose>                  
        <xsl:when test="$lien &gt; 0 and prd:SBCSExecutiveElements/prd:EarliestTaxLienDate and number(prd:SBCSExecutiveElements/prd:EarliestTaxLienDate) != 0">
        
          <xsl:variable name="oldDate">
            <xsl:call-template name="FormatDate">
              <xsl:with-param name="pattern" select="'mo/yr'" />
              <xsl:with-param name="value" select="prd:SBCSExecutiveElements/prd:EarliestTaxLienDate" />
            </xsl:call-template>
          </xsl:variable>
        
          <xsl:variable name="recentDate">
            <xsl:choose>                  
              <xsl:when test="prd:SBCSExecutiveElements/prd:MostRecentTaxLienDate and number(prd:SBCSExecutiveElements/prd:MostRecentTaxLienDate) != 0">
               <xsl:call-template name="FormatDate">
                 <xsl:with-param name="pattern" select="'mo/yr'" />
                 <xsl:with-param name="value" select="prd:SBCSExecutiveElements/prd:MostRecentTaxLienDate" />
               </xsl:call-template>
              </xsl:when>
      
              <xsl:otherwise>
                <xsl:value-of select="''" />
              </xsl:otherwise>
            </xsl:choose>                
          </xsl:variable>
                       
          <xsl:choose>                  
            <xsl:when test="normalize-space($oldDate) != normalize-space($recentDate) and normalize-space($recentDate) != ''">                     
              <xsl:value-of select="concat('(FILED ', normalize-space($oldDate), '-', normalize-space($recentDate), ')')" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="concat('(FILED ', normalize-space($oldDate), ')')" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="judgment">
      <xsl:choose>
        <xsl:when test="number(prd:SBCSExecutiveElements/prd:JudgmentCount) = 0 and prd:SBCSExecutiveElements/prd:JudgmentFlag and prd:SBCSExecutiveElements/prd:JudgmentFlag = 'Y'">      
          <xsl:value-of select="'Satisfied'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="number(prd:SBCSExecutiveElements/prd:JudgmentCount)" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="judgmentDates">
      <xsl:choose>                  
        <xsl:when test="$judgment &gt; 0 and prd:SBCSExecutiveElements/prd:EarliestJudgmentDate and number(prd:SBCSExecutiveElements/prd:EarliestJudgmentDate) != 0">
        
          <xsl:variable name="oldDate">
            <xsl:call-template name="FormatDate">
              <xsl:with-param name="pattern" select="'mo/yr'" />
              <xsl:with-param name="value" select="prd:SBCSExecutiveElements/prd:EarliestJudgmentDate" />
            </xsl:call-template>
          </xsl:variable>
        
          <xsl:variable name="recentDate">
            <xsl:choose>                  
              <xsl:when test="prd:SBCSExecutiveElements/prd:MostRecentJudgmentDate and number(prd:SBCSExecutiveElements/prd:MostRecentJudgmentDate) != 0">
               <xsl:call-template name="FormatDate">
                 <xsl:with-param name="pattern" select="'mo/yr'" />
                 <xsl:with-param name="value" select="prd:SBCSExecutiveElements/prd:MostRecentJudgmentDate" />
               </xsl:call-template>
              </xsl:when>
      
              <xsl:otherwise>
                <xsl:value-of select="''" />
              </xsl:otherwise>
            </xsl:choose>                
          </xsl:variable>
                       
          <xsl:choose>                  
            <xsl:when test="normalize-space($oldDate) != normalize-space($recentDate) and normalize-space($recentDate) != ''">                     
              <xsl:value-of select="concat('(FILED ', normalize-space($oldDate), '-', normalize-space($recentDate), ')')" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="concat('(FILED ', normalize-space($oldDate), ')')" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="sumLegal">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveElements/prd:LegalBalance">                     
          <xsl:value-of select="format-number(prd:SBCSExecutiveElements/prd:LegalBalance, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="ucc">
      <xsl:value-of select="number(prd:SBCSExecutiveElements/prd:UCCFilings)" />
    </xsl:variable>

    <xsl:variable name="uccDates">
      <xsl:choose>                  
        <xsl:when test="$ucc &gt; 0 and prd:SBCSExecutiveElements/prd:EarliestUCCDate and number(prd:SBCSExecutiveElements/prd:EarliestUCCDate) != 0">
        
          <xsl:variable name="oldDate">
            <xsl:call-template name="FormatDate">
              <xsl:with-param name="pattern" select="'mo/yr'" />
              <xsl:with-param name="value" select="prd:SBCSExecutiveElements/prd:EarliestUCCDate" />
            </xsl:call-template>
          </xsl:variable>
        
          <xsl:variable name="recentDate">
            <xsl:choose>                  
              <xsl:when test="prd:SBCSExecutiveElements/prd:MostRecentUCCDate and number(prd:SBCSExecutiveElements/prd:MostRecentUCCDate) != 0">
               <xsl:call-template name="FormatDate">
                 <xsl:with-param name="pattern" select="'mo/yr'" />
                 <xsl:with-param name="value" select="prd:SBCSExecutiveElements/prd:MostRecentUCCDate" />
               </xsl:call-template>
              </xsl:when>
      
              <xsl:otherwise>
                <xsl:value-of select="''" />
              </xsl:otherwise>
            </xsl:choose>                
          </xsl:variable>
                       
          <xsl:choose>                  
            <xsl:when test="normalize-space($oldDate) != normalize-space($recentDate) and normalize-space($recentDate) != ''">                     
              <xsl:value-of select="concat('(FILED ', normalize-space($oldDate), '-', normalize-space($recentDate), ')')" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="concat('(FILED ', normalize-space($oldDate), ')')" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="derogUCC">
      <xsl:choose>
        <xsl:when test="prd:SBCSExecutiveElements/prd:UCCDerogatoryCount and number(prd:SBCSExecutiveElements/prd:UCCDerogatoryCount) &gt; 0">                     
          <xsl:value-of select="'Yes**'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'No'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <!-- begin public records -->
    <table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td>
          Bankruptcy filings: 
          <xsl:if test="$bankruptcy &gt; 0 and $baseProduct != 'SBCSScore'"><a href="#bankruptcies" class="bright">details</a>&#160;</xsl:if> 
          <xsl:value-of select="$bankruptcyDates" /></td>
        <td align="right" nowrap="nowrap"><b><xsl:value-of select="$bankruptcy" /></b></td>
      </tr>

      <tr>
        <td>
          Tax lien filings: 
          <xsl:if test="$lien &gt; 0 and $baseProduct != 'SBCSScore'"><a href="#taxliens" class="bright">details</a>&#160;</xsl:if>
          <xsl:value-of select="$lienDates" /></td>
        <td align="right" nowrap="nowrap"><b><xsl:value-of select="$lien" /></b></td>
      </tr>

      <tr>
        <td>
          Judgment filings: 
          <xsl:if test="$judgment &gt; 0 and $baseProduct != 'SBCSScore'"><a href="#judgments" class="bright">details</a>&#160;</xsl:if> 
          <xsl:value-of select="$judgmentDates" /></td>
        <td align="right" nowrap="nowrap"><b><xsl:value-of select="$judgment" /></b></td>
      </tr>

      <tr>  
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td>
                Sum of legal filings:</td>
              <td align="right" nowrap="nowrap"><b><xsl:value-of select="$sumLegal" /></b></td>
            </tr>
          </table>
        </td>  
      </tr>

      <tr>
        <td>
          UCC filings: 
          <xsl:if test="$ucc &gt; 0 and $baseProduct != 'SBCSScore'"><a href="#uccfilings" class="bright">details</a>&#160;</xsl:if> 
          <xsl:value-of select="$uccDates" /></td>
        <td align="right" nowrap="nowrap"><b><xsl:value-of select="$ucc" /></b></td>
      </tr>
      
      <tr>
        <td>
          Cautionary UCC filings</td>
        <td align="right" nowrap="nowrap"><b><xsl:value-of select="$derogUCC" /></b></td>
      </tr>
    </table>

  </xsl:template>

</xsl:stylesheet>
