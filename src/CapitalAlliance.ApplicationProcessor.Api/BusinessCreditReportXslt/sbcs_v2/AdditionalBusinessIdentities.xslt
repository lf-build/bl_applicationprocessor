<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"               
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->


  <!--
  *********************************************
  * AdditionalBusinessIdentities template
  *********************************************
  -->
  <xsl:template name="AdditionalBusinessIdentities">

    <table class="section" width="100%" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <th><a class="report_section_title">Additional Business Identities</a></th>
        </tr>
      </thead>

      <tbody>
        <!-- loops through business identites -->
        <xsl:apply-templates select="prd:AdditionalBusinessIdentity" />
      </tbody>
    </table>

  </xsl:template>



  <!--
  *********************************************
  * AdditionalBusinessIdentity template
  *********************************************
  -->
  <xsl:template match="prd:AdditionalBusinessIdentity">

    <xsl:variable name="name">
      <xsl:choose>                  
        <xsl:when test="prd:BusinessName">                    
          <xsl:value-of select="prd:BusinessName" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="street">
      <xsl:choose>                  
        <xsl:when test="prd:StreetAddress">                     
          <xsl:value-of select="prd:StreetAddress" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="city">
      <xsl:choose>                  
        <xsl:when test="prd:City">                     
          <xsl:value-of select="prd:City" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="state">
      <xsl:choose>                  
        <xsl:when test="prd:State">                     
          <xsl:value-of select="prd:State" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="zip">
      <xsl:choose>                  
        <xsl:when test="prd:Zip">                     
          <xsl:call-template name="FormatZip">
            <xsl:with-param name="value" select="concat(prd:Zip, prd:ZipExtension)" />
          </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="phone">
      <xsl:choose>                  
        <xsl:when test="prd:PhoneNumber">                     
          <xsl:call-template name="FormatPhone">
            <xsl:with-param name="value" select="translate(prd:PhoneNumber, '-', '')" />
          </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="taxID">
      <xsl:choose>                  
        <xsl:when test="prd:FederalTaxID">                     
          <xsl:value-of select="prd:FederalTaxID" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <tr class="subtitle">
      <th>Additional Business Identity #<xsl:value-of select="position()" /></th>
    </tr>

    <tr>
      <td class="box_padding">
        <table width="100%" border="0" cellspacing="0" cellpadding="1">
          <colgroup style="width:16%"/>
          <colgroup style="width:84%" />

          <tr>
            <td class="label">Name</td>              
            <td><xsl:value-of select="$name" /></td>
          </tr>

          <tr>
            <td class="label" style="vertical-align:top;">Address</td>              
            <td>
              <xsl:if test="normalize-space($street) != ''">
                <xsl:value-of select="$street" />
                <xsl:if test="normalize-space($city) != ''">
                  <br />
                </xsl:if>
              </xsl:if>

              <xsl:if test="normalize-space($city) != ''">
                <xsl:value-of select="concat(normalize-space($city), ', ', normalize-space($state), ' ', normalize-space($zip))" />
              </xsl:if>
            </td>
          </tr>

          <xsl:if test="normalize-space($taxID) != ''">
            <tr>
              <td class="label">Tax ID</td>              
              <td><xsl:value-of select="$taxID" /></td>
            </tr>
          </xsl:if>

          <xsl:if test="normalize-space($phone) != ''">
            <tr>
              <td class="label">Phone Number</td>              
              <td><xsl:value-of select="$phone" /></td>
            </tr>
          </xsl:if>
        </table>
      </td>
    </tr>

    <xsl:if test="position() &lt; last()">
      <tr><td class="subsectionSpacer"></td></tr>
    </xsl:if>
    
  </xsl:template>
  
  
</xsl:stylesheet>