
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rsp="http://www.experian.com/NetConnectResponse"
                xmlns:prd="http://www.experian.com/ARFResponse">

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />



  <!--
  *********************************************
  * LegalFilingSummary template
  *********************************************
  -->
  <xsl:template name="LegalFilingSummary">

    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="{$borderColor}">
        
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#ffffff">
                <!-- box header -->
                <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                  <tr>
                    <td height="23" bgcolor="{$borderColor}" align="left" valign="middle">
                      <img src="../images/spacer.gif" border="0" width="5" height="1" alt=""/>
                      <font color="#ffffff"><b>Legal Filing Summary</b></font>
                    </td>
                  </tr>  

                  <tr>
                    <td style="line-height:3px;" valign="bottom">&#160;</td>
                  </tr>  

                  <tr>
                    <td align="center">
                      <table bgcolor="#ffffff" width="98%" border="0" cellspacing="0" cellpadding="0">
                        <xsl:apply-templates select="../prd:ExecutiveElements" />
                      </table>
                    </td>
                  </tr>


                </table>
                
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </xsl:template>

  <!--
  *********************************************
  * ExecutiveElements template
  *********************************************
  -->
  <xsl:template match="prd:ExecutiveElements" >

    <xsl:variable name="publicRecord">
      <xsl:choose>                    
        <xsl:when test="sum(prd:BankruptcyCount|prd:TaxLienCount|prd:JudgmentCount|CollectionCount) &gt; 0">                               
          <xsl:value-of select="'Yes'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'No'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="uccFilings">
      <xsl:choose>                    
        <xsl:when test="sum(prd:UCCFilings) &gt; 0">                               
          <xsl:value-of select="'Yes'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'No'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <tr>
      <td height="20" width="47%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="95%" align="left">
              Public Records present:</td>
            <td width="5%" align="right" nowrap="nowrap"><b><xsl:value-of select="$publicRecord" /></b></td>
          </tr>
        </table>
      </td>  

      <td width="4%">
      </td>
      
      <td height="20" width="47%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="95%" align="left">
              UCC filings present:</td>
            <td width="5%" align="right" nowrap="nowrap"><b><xsl:value-of select="$uccFilings" /></b></td>
          </tr>
        </table>
      </td>  
    </tr>

  </xsl:template>
      
</xsl:stylesheet>
