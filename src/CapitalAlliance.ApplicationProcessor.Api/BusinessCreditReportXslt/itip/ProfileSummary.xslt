
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * ProfileSummary template
  *********************************************
  -->
  <xsl:template name="ProfileSummaryITIP">
    <xsl:param name="group" />

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    
      <tr>
        <!-- DBT Summary box column -->
        <td width="49%" valign="top">
          <xsl:call-template name="CurrentDBTITIP" />
        </td>
    
        <td width="2%">
        </td>
    
        <!-- Trade Payment Summary column -->
        <td width="49%" valign="top">
          <xsl:if test="../prd:PaymentTotals[prd:ITIPConsolidatedNumber/prd:GroupDisclosureID = normalize-space($group)]/prd:CombinedTradeLines">  
            <xsl:call-template name="TradePaymentSummary" >
              <xsl:with-param name="group" select="$group" />
            </xsl:call-template>
          </xsl:if>
          
        </td>
      </tr>

      <tr>
        <!-- DBT Summary box column -->
        <td colspan="3">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><img src="../images/spacer.gif" border="0" width="5" height="1" alt=""/></td>
              <td align="left"><font size="1" style="FONT-FAMILY: 'verdana';">
                * Days Beyond Terms (DBT) is a dollar weighted average number of days that payment was made 
                beyond the invoice due date.  The DBT calculation is based on trade payments on file 
                updated within the previous three months. 
                </font></td>
            </tr>
          </table>
        </td>
      </tr> 
    </table>

  </xsl:template>
    
</xsl:stylesheet>