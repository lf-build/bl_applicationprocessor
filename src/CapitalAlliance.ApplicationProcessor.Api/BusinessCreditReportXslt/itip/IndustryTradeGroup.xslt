
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * IndustryTradeGroup template
  *********************************************
  -->
  <xsl:template match="prd:IndustryTradeGroup" >
    <!-- busName -->
    <xsl:variable name="busName">
      <xsl:variable name="tmpBusName">
        <xsl:choose>                    
          <xsl:when test="contains(prd:GroupName, '-')">
            <xsl:value-of select="normalize-space(substring-before(prd:GroupName, '-'))" />
          </xsl:when>

          <xsl:otherwise>
            <xsl:value-of select="prd:GroupName" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>
    
      <xsl:call-template name="convertcase">
        <xsl:with-param name="toconvert" select="$tmpBusName" />
        <xsl:with-param name="conversion" select="'proper'" />
      </xsl:call-template>
    </xsl:variable>

    <!-- agingBucket -->
    <xsl:variable name="agingBucket">
      <xsl:variable name="tmpAgingBucket">
        <xsl:value-of select="normalize-space(substring-before(substring-after(prd:GroupName, '-'), '('))" />
      </xsl:variable>
    
      <xsl:call-template name="convertcase">
        <xsl:with-param name="toconvert" select="$tmpAgingBucket" />
        <xsl:with-param name="conversion" select="'proper'" />
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="group">
      <xsl:value-of select="./prd:GroupID" />
    </xsl:variable>
    
    <xsl:variable name="paymentExpTitle">
        <xsl:choose>                    
          <xsl:when test="normalize-space($agingBucket) = ''">
            <xsl:value-of select="''" />
          </xsl:when>

          <xsl:otherwise>
            <xsl:value-of select="concat('(', normalize-space($agingBucket), ')')" />
          </xsl:otherwise>
        </xsl:choose>    
    </xsl:variable>

    
    <table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>
        <td align="center">
          <font size="3" color="{$titleColor}"><b>
          <xsl:value-of select="concat($busName, ' - ', $agingBucket, '(Group ', normalize-space($group), ')')" />
          </b></font>
        </td>
      </tr>

      <tr>
        <td>
          <!-- Section title -->
          <xsl:call-template name="SectionTitle">
            <xsl:with-param name="title" select="'Executive Summary'" />
            <xsl:with-param name="color" select="$titleColor" />
          </xsl:call-template>
        </td>
      </tr>

      <xsl:if test="../prd:ExecutiveElements">  
        <tr>
          <td>
            <xsl:call-template name="LegalFilingSummary" />
          </td>
        </tr>
        <tr>
          <td valign="bottom" style="line-height:15px;">&#160;</td>
        </tr>  
      </xsl:if>
        
      <xsl:if test="sum(../prd:ExecutiveElements/prd:BankruptcyCount|../prd:ExecutiveElements/prd:TaxLienCount|../prd:ExecutiveElements/prd:JudgmentCount|../prd:ExecutiveElements/CollectionCount) &gt; 0">
        <tr>
          <td align="left">
            <font size="2" color="#ff0000">
              Derogatory public record information (tax liens, judgment or bankruptcy) or collections exist
              on this company.  Pull the Business Profile report for details.
            </font>
          </td>
        </tr>
        <tr>
          <td valign="bottom" style="line-height:15px;">&#160;</td>
        </tr>  
      </xsl:if>
      
    </table>
    
    <xsl:if test="$group != 161">
      <!-- ProfileSummary -->
      <xsl:call-template name="ProfileSummaryITIP">
        <xsl:with-param name="group" select="$group" />
      </xsl:call-template>

      <!-- back to top graphic -->
      <xsl:call-template name="BackToTop" />
    </xsl:if>
    
    <xsl:if test="../prd:TradePaymentExperiences[normalize-space(prd:ITIPConsolidatedNumber/prd:GroupDisclosureID) = normalize-space($group)]">  
      <!-- TradePaymentInformation -->
      <xsl:call-template name="PaymentExperiencesITIP">
        <xsl:with-param name="title" select="concat($busName, ' ', 'Trade Payment Information', ' ', $paymentExpTitle)" />
        <xsl:with-param name="group" select="$group" />
      </xsl:call-template>

      <!-- back to top graphic -->
      <xsl:call-template name="BackToTop" />
    </xsl:if>

    <xsl:if test="../prd:AdditionalPaymentExperiences[normalize-space(prd:ITIPConsolidatedNumber/prd:GroupDisclosureID) = normalize-space($group)]">  
      <!-- AdditionalPaymentExperiences -->
      <xsl:call-template name="PaymentExperiencesITIP">
        <xsl:with-param name="title" select="'Additional Payment Experiences (Non-Trade Accounts &amp; Previously Reported Trade Lines)'" />
        <xsl:with-param name="group" select="$group" />
      </xsl:call-template>

      <!-- back to top graphic -->
      <xsl:call-template name="BackToTop" />
    </xsl:if>
        
    <xsl:if test="$group != 161 and ../prd:PaymentTrends[normalize-space(prd:ITIPConsolidatedNumber/prd:GroupDisclosureID) = normalize-space($group)]">  
      <!-- MonthlyPaymentTrends -->
      <xsl:call-template name="MonthlyPaymentTrendsITIP">
        <xsl:with-param name="group" select="$group" />
      </xsl:call-template>

      <!-- back to top graphic -->
      <xsl:call-template name="BackToTop" />
    </xsl:if>
        
  </xsl:template>
</xsl:stylesheet>