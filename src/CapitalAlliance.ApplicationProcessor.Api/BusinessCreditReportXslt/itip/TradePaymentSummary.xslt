
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rsp="http://www.experian.com/NetConnectResponse"
                xmlns:prd="http://www.experian.com/ARFResponse">

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />



  <!--
  *********************************************
  * TradePaymentSummary template
  *********************************************
  -->
  <xsl:template name="TradePaymentSummary">
    <xsl:param name="group" />

    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="{$borderColor}">
        
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#ffffff">
                <!-- box header -->
                <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                  <tr>
                    <td height="23" bgcolor="{$borderColor}" align="left" valign="middle">
                      <img src="../images/spacer.gif" border="0" width="5" height="1" alt=""/>
                      <font color="#ffffff"><b>Trade Payment Summary</b></font>
                    </td>
                  </tr>  

                  <tr>
                    <td style="line-height:3px;" valign="bottom">&#160;</td>
                  </tr>  

                  <tr>
                    <td align="center">
                      <table bgcolor="#ffffff" width="98%" border="0" cellspacing="0" cellpadding="0">
                        <xsl:apply-templates select="../prd:PaymentTotals[prd:ITIPConsolidatedNumber/prd:GroupDisclosureID = normalize-space($group)]/prd:CombinedTradeLines" />
                      </table>
                    </td>
                  </tr>

                  <tr>
                    <td style="line-height:2px;" valign="bottom">&#160;</td>
                  </tr>  

                </table>
                
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </xsl:template>

  <!--
  *********************************************
  * TradePaymentExperiences template
  *********************************************
  -->
  <xsl:template match="prd:CombinedTradeLines" >

    <xsl:variable name="highCredit">
      <xsl:choose>                    
        <xsl:when test="prd:TotalHighCreditAmount and number(prd:TotalHighCreditAmount/prd:Amount) != 0">                               
          <xsl:value-of select="concat(prd:TotalHighCreditAmount/prd:Modifier/@code, format-number(prd:TotalHighCreditAmount/prd:Amount, '$###,###,##0'))" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="balance">
      <xsl:choose>                    
        <xsl:when test="prd:TotalAccountBalance and number(prd:TotalAccountBalance/prd:Amount) != 0">                               
          <xsl:value-of select="concat(prd:TotalAccountBalance/prd:Modifier/@code, format-number(prd:TotalAccountBalance/prd:Amount, '$###,###,##0'))" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="percentCurrent">
      <xsl:choose>                    
        <xsl:when test="prd:CurrentPercentage and number(prd:CurrentPercentage) != 0">                             
          <xsl:value-of select="format-number(prd:CurrentPercentage div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="percent30">
      <xsl:choose>                    
        <xsl:when test="prd:DBT30 and number(prd:DBT30) != 0">                             
          <xsl:value-of select="format-number(prd:DBT30 div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="percent60">
      <xsl:choose>                    
        <xsl:when test="prd:DBT60 and number(prd:DBT60) != 0">                             
          <xsl:value-of select="format-number(prd:DBT60 div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="percent90">
      <xsl:choose>                    
        <xsl:when test="prd:DBT90 and number(prd:DBT90) != 0">                             
          <xsl:value-of select="format-number(prd:DBT90 div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="percent91">
      <xsl:choose>                    
        <xsl:when test="prd:DBT90Plus and number(prd:DBT90Plus) != 0">                             
          <xsl:value-of select="format-number(prd:DBT90Plus div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="dbt">
      <xsl:choose>                    
        <xsl:when test="prd:DBT and string(number(prd:DBT)) != 'NaN'">                             
          <xsl:value-of select="number(prd:DBT)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <tr>
      <td height="21">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="95%" align="left">
              Total recent high credit:</td>
            <td width="5%" align="right" nowrap="nowrap"><b><xsl:value-of select="$highCredit" /></b></td>
          </tr>
        </table>
      </td>  
    </tr>

    <tr>
      <td height="21">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="95%" align="left">
              Total balance:</td>
            <td width="5%" align="right" nowrap="nowrap"><b><xsl:value-of select="$balance" /></b></td>
          </tr>
        </table>
      </td>  
    </tr>

    <tr>
      <td height="21">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="95%" align="left">
              Total percent current:</td>
            <td width="5%" align="right" nowrap="nowrap"><b><xsl:value-of select="$percentCurrent" /></b></td>
          </tr>
        </table>
      </td>  
    </tr>

    <tr>
      <td height="21">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="95%" align="left">
              Total percent 1-30 days:</td>
            <td width="5%" align="right" nowrap="nowrap"><b><xsl:value-of select="$percent30" /></b></td>
          </tr>
        </table>
      </td>  
    </tr>

    <tr>
      <td height="21">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="95%" align="left">
              Total percent 31-60 days:</td>
            <td width="5%" align="right" nowrap="nowrap"><b><xsl:value-of select="$percent60" /></b></td>
          </tr>
        </table>
      </td>  
    </tr>

    <tr>
      <td height="21">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="95%" align="left">
              Total percent 61-90 days:</td>
            <td width="5%" align="right" nowrap="nowrap"><b><xsl:value-of select="$percent90" /></b></td>
          </tr>
        </table>
      </td>  
    </tr>

    <tr>
      <td height="21">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="95%" align="left">
              Total percent 91+ days:</td>
            <td width="5%" align="right" nowrap="nowrap"><b><xsl:value-of select="$percent91" /></b></td>
          </tr>
        </table>
      </td>  
    </tr>

    <tr>
      <td height="21">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="95%" align="left">
              DBT:</td>
            <td width="5%" align="right" nowrap="nowrap"><b><xsl:value-of select="$dbt" /></b></td>
          </tr>
        </table>
      </td>  
    </tr>

  </xsl:template>
      
</xsl:stylesheet>
