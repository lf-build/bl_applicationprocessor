
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:rsp="http://www.experian.com/NetConnectResponse"
  xmlns:prd="http://www.experian.com/ARFResponse">

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * Global variables and parameters
  *********************************************
  <xsl:param name="systemYear" select="'2006'" />
  -->
  <xsl:param name="product" select="'ITP'" />
  <xsl:param name="baseProduct" select="''" />


  <!--
  *********************************************
  * include template
  *********************************************
  -->
  <xsl:include href="../common/ReportHeader.xslt" />
  <xsl:include href="../common/Util.xslt" />
  <xsl:include href="../common/case.xslt" />
  <xsl:include href="../common/BackToTop.xslt" />
  <xsl:include href="../common/ErrorProcessing.xslt" />
  <xsl:include href="../common/ReportFooter.xslt" />

  <xsl:include href="IndustryTradeProfile.xslt" />

  <!--
  *********************************************
  * Initial template
  *********************************************
  -->  
  <xsl:template match="/">
    <html>
      <head>
        <title>Industry Trade Profile</title>

        <style type="text/css">
            td {font-size: 9pt; font-family: 'arial';}
        </style>
      </head>

      <body>
        <a name="top"></a>
        <table width="715" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td>      
              <xsl:apply-templates select="//prd:IndustryTradeProfile" mode="header" />
               
              <!--IndustryTradeProfile template -->
              <xsl:apply-templates select="//prd:IndustryTradeProfile" mode="ITIP" />
              
              <xsl:apply-templates select="//prd:IndustryTradeProfile" mode="footer" />
               
            </td>
          </tr>
        </table>    
      </body>
    </html>
  </xsl:template>


  <!--
  *********************************************
  * Header template
  *********************************************
  -->
  <xsl:template match="prd:IndustryTradeProfile" mode="header" >
    <!-- Report Header -->
    <xsl:call-template name="ReportHeader">
      <xsl:with-param name="reportType" select="'Industry Trade Profile'" />
      <xsl:with-param name="reportName" select="prd:BusinessNameAndAddress/prd:BusinessName" />
    </xsl:call-template>

    <br />

 </xsl:template>
  
  <!--
  *********************************************
  * Footer template
  *********************************************
  -->
  <xsl:template match="prd:IndustryTradeProfile" mode="footer" >
    <!-- Report Date  -->
    <xsl:variable name="reportDate">
      <xsl:value-of select="prd:BusinessNameAndAddress/prd:ProfileDate" />
    </xsl:variable>

    <!-- is it a limited report?  -->
    <xsl:variable name="reportEnd">
       <xsl:choose>                   
         <xsl:when test="normalize-space(prd:BillingIndicator/@code) = 'A' ">
            <xsl:value-of select="'End of limited'" />
         </xsl:when>

         <xsl:otherwise>
            <xsl:value-of select="'End of report'" />
         </xsl:otherwise>
       </xsl:choose>    
    </xsl:variable>

    <!-- Report Footer -->
    <xsl:call-template name="ReportFooter">
      <xsl:with-param name="reportType" select="$product" />
      <xsl:with-param name="reportDate" select="$reportDate" />
      <xsl:with-param name="reportEnd" select="$reportEnd" />
    </xsl:call-template>
  </xsl:template>
  
  <!--
  *********************************************
  * this template overrides the built-in rule
  * do nothing with stuff I'm not interested in
  *********************************************
  -->
  <xsl:template match="text()|@*">
  </xsl:template>

</xsl:stylesheet>