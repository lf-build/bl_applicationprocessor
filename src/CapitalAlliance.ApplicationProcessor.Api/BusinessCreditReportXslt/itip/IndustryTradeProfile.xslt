
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * include template
  *********************************************
  -->
  <xsl:include href="IdentifyingInformation.xslt" />
  <xsl:include href="IndustryTradeGroup.xslt" />
  <xsl:include href="ProfileSummary.xslt" />
  <xsl:include href="CurrentDBT.xslt" />
  <xsl:include href="TradePaymentSummary.xslt" />
  <xsl:include href="LegalFilingSummary.xslt" />
  <xsl:include href="PaymentExperiences.xslt" />
  <xsl:include href="MonthlyPaymentTrends.xslt" />


  <!--
  *********************************************
  * IndustryTradeProfile template
  *********************************************
  -->
  <xsl:template match="prd:IndustryTradeProfile | prd:Intelliscore | prd:BusinessProfile" mode="ITIP" >
    <xsl:param name="standalone" select="1" />

    <xsl:choose>
      <xsl:when test="normalize-space(prd:BusinessNameAndAddress/prd:ProfileType/@code) = 'NO RECORD' ">
        <xsl:call-template name="businessNotFound" />
      </xsl:when>

      <xsl:otherwise>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top" height="20" align="center"><font size="4" color="{$titleColor}"><b><a name="ITIP">Industry Trade Profile</a></b></font></td>
          </tr>
        </table>

        <!-- Identifying Information -->
        <xsl:call-template name="IdentifyingInformation" />
    
        <br />
      
        <!-- group by IndustryTradeGroup -->
        <xsl:apply-templates select="prd:IndustryTradeGroup"/>
    
      </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>

</xsl:stylesheet>