
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">
                

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * FraudShield template
  *********************************************
  -->
  <xsl:template name="ConsumerStatementSBI">
  
      <tr>
        <td colspan="3">
          <font size="3" color="{$titleColor}"><b>
          <xsl:value-of select="'Consumer Statement'" />
          </b></font>
        </td>
      </tr>

      <tr>
        <td colspan="3">
    		<xsl:apply-templates select="../prd:CreditProfile/prd:Statement/prd:StatementText/prd:MessageText" />
        </td>
      </tr>

  </xsl:template>


  <!--
  *********************************************
  * FraudShield Summary template
  *********************************************
  -->
  <xsl:template match="prd:MessageText">

    <xsl:if test="position() &gt; 1">
    	<br /><br />
    </xsl:if>

    <xsl:value-of select="." />
      
  </xsl:template>

</xsl:stylesheet>