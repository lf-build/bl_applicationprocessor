
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:rsp="http://www.experian.com/NetConnectResponse"
  xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * Global variables and parameters
  *********************************************
  <xsl:param name="systemYear" select="'2006'" />
  -->
  <xsl:param name="product" select="'SBIBOP'" />
  <xsl:param name="baseProduct" select="''" />


  <!--
  *********************************************
  * include template
  *********************************************
  -->
  <xsl:include href="../common/ReportHeader.xslt" />
  <xsl:include href="../common/Util.xslt" />
  <xsl:include href="../common/case.xslt" />
  <xsl:include href="../common/BackToTop.xslt" />
  <xsl:include href="../common/TradeFilingSummary.xslt" />
  <xsl:include href="../common/ErrorProcessing.xslt" />
  <xsl:include href="IdentifyingInformation.xslt" />
  <xsl:include href="ReportHeaderMessage.xslt" />
  <xsl:include href="BusinessCreditInfo.xslt" />
  <xsl:include href="CIScorable.xslt" />
  <xsl:include href="BusinessSummary.xslt" />
  <xsl:include href="SignatoryCreditInfo.xslt" />
  <xsl:include href="ConsumerStatementSBI.xslt" />
  <xsl:include href="CustomFooterMessage.xslt" />
  <xsl:include href="../common/ReportFooter.xslt" />

  <xsl:include href="../ownerprofile/IdentifyingInformation.xslt" />
  <xsl:include href="../ownerprofile/SignatoryProfileSummary.xslt" />
  <xsl:include href="../ownerprofile/ConsumerStatement.xslt" />
  <xsl:include href="../ownerprofile/OFACWarning.xslt" />
  <xsl:include href="../ownerprofile/FraudShield.xslt" />
  <xsl:include href="../ownerprofile/LegalFilings.xslt" />
  <xsl:include href="../ownerprofile/Inquiries.xslt" />
  <xsl:include href="../ownerprofile/TradeInformation.xslt" />


  <!--
  *********************************************
  * Initial template
  *********************************************
  -->  
  <xsl:template match="/">
    <html>
      <head>
        <title>SBI / BOP Report</title>

          <style type="text/css">
            td {font-size: 9pt; font-family: 'arial';}
          </style>

      </head>
      <body>
        <a name="top"></a>
        <table width="715" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td>      
               
              <!-- Intelliscore template -->
              <xsl:apply-templates select="//prd:Intelliscore" />
              
            </td>
          </tr>
        </table>    
      </body>
    </html>
  </xsl:template>


  <!--
  *********************************************
  * Intelliscore template
  *********************************************
  -->
  <xsl:template match="prd:Intelliscore" >
    <!-- Report Date  -->
    <xsl:variable name="reportDate">
      <xsl:value-of select="prd:BusinessNameAndAddress/prd:ProfileDate" />
    </xsl:variable>

    <!-- Profile Type  -->
    <xsl:variable name="profileType">
      <xsl:choose>
        <xsl:when test="contains(prd:InputSummary/prd:Inquiry, '/BOP') ">
          <xsl:value-of select=" 'Blended Score and BOP'" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'Blended Score'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- Report Header -->
    <xsl:call-template name="ReportHeader">
      <xsl:with-param name="reportType" select="$profileType" />
      <xsl:with-param name="reportName" select="prd:BusinessNameAndAddress/prd:BusinessName" />
    </xsl:call-template>

    <br />

    <xsl:variable name="isHTMLModel">
       <xsl:call-template name="isHTMLModel" />
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="normalize-space($isHTMLModel) = 'false' ">
        <xsl:call-template name="NoHTMLReport" />
      </xsl:when>

      <xsl:otherwise>

        <xsl:if test="$product = 'SBIBOP' ">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td valign="top" height="20" align="center"><font size="3" color="{$titleColor}"><b><a name="SBI"><u>Blended Score</u></a></b></font></td>
            </tr>
          </table>
        </xsl:if>

  	    <!-- Identifying Information -->
  	    <xsl:call-template name="IdentifyingInformationScore" />
  	
  	    <br />
  	    
  	    <!-- Report Header message -->
  	    <xsl:call-template name="ReportHeaderMessage" />
  	
  	    <br />
  	
  	    <!-- Business Credit Info -->
  	    <xsl:call-template name="BusinessCreditInfo" />
  
  	    <xsl:if test="prd:SmallBusinessKeyModelElements">	
  
  	      <!-- Back to Top grapic -->
  	      <xsl:call-template name="BackToTop" />
  
  	      <!-- Consumer Credit Info -->
  	      <xsl:call-template name="SignatoryCreditInfo" />
  	    </xsl:if>
  
  	    <xsl:if test="../prd:CreditProfile ">	
  	      <!-- bop processing -->
  	      <xsl:apply-templates select="../prd:CreditProfile" mode="SBIBOP" />
  	    </xsl:if>
  
  	    <!-- is it a limited report?  -->
  	    <xsl:variable name="reportEnd">
  	       <xsl:choose>		              
  	         <xsl:when test="normalize-space(prd:BillingIndicator/@code) = 'A' ">
  	            <xsl:value-of select="'End of limited'" />
  	         </xsl:when>
  	
  	         <xsl:otherwise>
  	            <xsl:value-of select="'End of report'" />
  	         </xsl:otherwise>
  	       </xsl:choose>    
  	    </xsl:variable>
  	
  	    <!-- Report Footer -->
  	    <xsl:call-template name="ReportFooter">
  	      <xsl:with-param name="reportType" select="'BOP'" />
  	      <xsl:with-param name="reportDate" select="$reportDate" />
  	      <xsl:with-param name="reportEnd" select="$reportEnd" />
  	    </xsl:call-template>

      </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>


  <!--
  *********************************************
  * bop  template
  *********************************************
  -->
  <xsl:template match="prd:CreditProfile" mode="SBIBOP" >

    <xsl:variable name="idx">
      <xsl:value-of select="position()" />
    </xsl:variable>

    <xsl:if test="normalize-space(//prd:ProprietorNameAndAddress[number($idx)]/prd:ProfileType/@code) != 'NO RECORD'">

	    <!-- Report Date  -->
	    <xsl:variable name="reportDate">
	      <xsl:value-of select="../prd:BusinessProfile/prd:BusinessNameAndAddress/prd:ProfileDate" />
	    </xsl:variable>
	
	    <!-- Back to Top grapic -->
	    <xsl:call-template name="BackToTop" />
	
	    <br />
	
	    <!-- Identifying Information -->
	    <xsl:call-template name="IdentifyingInformationBOP">
	      <xsl:with-param name="index" select="position()" />
	    </xsl:call-template>
	
	    <br />
	
	    <xsl:if test="prd:ProfileSummary">
	      <!-- Signatory Profile Summary -->
	      <xsl:call-template name="SignatoryProfileSummary" >
	        <xsl:with-param name="position" select="position()" />
        </xsl:call-template>
  
	      <!-- Back to Top grapic -->
	      <xsl:call-template name="BackToTop" />
	    </xsl:if>
	
	    <xsl:if test="prd:Statement">
	      <!-- Consumer Statement -->
	      <xsl:call-template name="ConsumerStatement" />
	
	      <!-- Back to Top grapic -->
	      <xsl:call-template name="BackToTop" />
	    </xsl:if>
	
	    <xsl:if test="prd:InformationalMessage and number(prd:InformationalMessage/prd:MessageNumber) = 57 and starts-with(prd:InformationalMessage/prd:MessageText, '1200 ')  ">
	      <!-- Consumer Statement -->
	      <xsl:call-template name="OFACWarning" />
	
	      <!-- Back to Top grapic -->
	      <xsl:call-template name="BackToTop" />
	    </xsl:if>
	
	    <xsl:if test="prd:FraudServices/prd:Indicator">
	      <!-- Fraud Shield -->
	      <xsl:call-template name="FraudShield" />
	
	      <!-- Back to Top grapic -->
	      <xsl:call-template name="BackToTop" />
	    </xsl:if>
	
	    <xsl:if test="prd:PublicRecord">
	
	      <!-- Legal Filings -->
	      <xsl:call-template name="LegalFilings" />
	    </xsl:if>
	
	    <xsl:if test="prd:Inquiry">
	
	      <!-- Inquiries -->
	      <xsl:call-template name="InquiriesBOP" />
	
	      <!-- Back to Top grapic -->
	      <xsl:call-template name="BackToTop" />
	    </xsl:if>
	
	    <xsl:if test="prd:TradeLine">
	
	      <!-- TradeInformation -->
	      <xsl:call-template name="TradeInformation" />
	    </xsl:if>
    </xsl:if>
  </xsl:template>


  <!--
  *********************************************
  * this template overrides the built-in rule
  * do nothing with stuff I'm not interested in
  *********************************************
  -->
  <xsl:template match="text()|@*">
  </xsl:template>

</xsl:stylesheet>