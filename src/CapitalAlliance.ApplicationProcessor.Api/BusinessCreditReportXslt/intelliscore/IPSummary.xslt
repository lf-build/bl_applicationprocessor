
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * IP Summary template
  *********************************************
  -->
  <xsl:template name="IPSummary">

    <xsl:variable name="scorable">
       <xsl:choose>                 
         <xsl:when test="not (prd:ExecutiveSummary) or not (prd:PaymentTrends) or not (prd:QuarterlyPaymentTrends) ">
            <xsl:value-of select="0" />
         </xsl:when>

         <xsl:otherwise>
            <xsl:value-of select="1" />
         </xsl:otherwise>
       </xsl:choose>    
    </xsl:variable>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">

      <xsl:if test="boolean(number($scorable))">
        <tr>
          <!-- begin left column -->
          <td width="49%" valign="top">
  
            <!-- Current DBT -->
            <xsl:call-template name="CurrentDBT" />
            <br />
          </td>
          <!-- end left column -->
  
  
          <!-- begin center space -->
          <td align="center" width="10">
            <xsl:text disable-output-escaping="yes">
              &#160;
            </xsl:text>
          </td>
          <!-- end center space -->
  
  
          <!-- begin right column -->
          <td width="49%" valign="top">
  
            <xsl:if test="prd:ExecutiveSummary ">
              <!-- Performance Analysis -->
              <xsl:call-template name="PerformanceAnalysis" />
              <br />
            </xsl:if>
          </td>
          <!-- end right column -->
        </tr>
      </xsl:if>      

      <xsl:if test="boolean(number($scorable))">
        <tr>
          <!-- begin left column -->
          <td width="49%" valign="top">
  
            <!-- Monthly DBT -->
            <xsl:call-template name="MonthlyDBT" />
  
          </td>
          <!-- end left column -->
  
  
          <!-- begin center space -->
          <td align="center" width="10">
            <xsl:text disable-output-escaping="yes">
              &#160;
            </xsl:text>
          </td>
          <!-- end center space -->
  
          <!-- begin right column -->
          <td width="49%" valign="top">
  
            <!-- Quarterly DBT -->
            <xsl:call-template name="QuarterlyDBT" />
  
          </td>
          <!-- end right column -->
        </tr>
      </xsl:if>      
      
    </table>

    <xsl:if test="prd:ExecutiveSummary and prd:ExecutiveElements">                     
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
            <font size="1"><i>
            * Days Beyond Terms (DBT) is a dollar weighted calculation of the average number of 
              days that payment was made beyond the invoice due date based on trades on file that 
              have been updated in the previous 3 months.
            </i></font>
            
            <xsl:choose>                  
              <xsl:when test="prd:ExecutiveElements/prd:UCCDerogatoryCount and number(prd:ExecutiveElements/prd:UCCDerogatoryCount) &gt; 0">
                <br /><img src="../images/spacer.gif" border="0" width="1" height="5" alt=""/><br />
              </xsl:when>
  
              <xsl:otherwise>
                <br /><br />
              </xsl:otherwise>
            </xsl:choose>   
          </td>
        </tr>
      </table>
    </xsl:if>

    <xsl:if test="prd:IntelliscoreScoreInformation/prd:ModelInformation/prd:ModelCode and number(prd:IntelliscoreScoreInformation/prd:ModelInformation/prd:ModelCode) != 215 and prd:ExecutiveElements/prd:UCCDerogatoryCount and number(prd:ExecutiveElements/prd:UCCDerogatoryCount) &gt; 0">                     
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
            <font size="1"><i>
              ** Cautionary UCC Filings include one or more of the following collateral:<br/>
              Accounts, Accounts Receivables, Contract Rights, Hereafter Acquired Property, Inventory, Leases, Notes Receivable or Proceeds.            
            </i></font>
          </td>
        </tr>
      </table>
    </xsl:if>

  </xsl:template>
    
  
</xsl:stylesheet>