
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * ReportHeaderMessage template
  *********************************************
  -->
  <xsl:template name="ReportHeaderMessage">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
<!--
          <xsl:choose>
            <xsl:when test="not (prd:KeyModelElements) and not (prd:SmallBusinessKeyModelElements)">		    		   		   
              <font size="2">
              This report is limited and displays the only available information reported on this 
              business.
              </font>
            </xsl:when>

            <xsl:when test="not (prd:SmallBusinessKeyModelElements) and (prd:KeyModelElements or (prd:IntelliscoreScoreInformation/prd:ModelInformation/prd:CustomModelCode = '06'))">
               A Commercial Intelliscore has been returned in place of the requested Small Business 
               Intelliscore because no consumer data is available for this business.
            </xsl:when>

            <xsl:otherwise>
               The Small Business Intelliscore is designed to predict the likelihood of serious 
               credit delinquency within the next 12 months based on business and consumer risk 
               factors. Scores range from 0 to 100 where higher scores indicate lower risk.
            </xsl:otherwise>
          </xsl:choose>
-->
		The Small Business Intelliscore is designed to predict the likelihood of serious 
		credit delinquency within the next 12 months based on business and consumer risk 
		factors. Scores range from 0 to 100 where higher scores indicate lower risk.

        </td>
      </tr>
    </table>

  </xsl:template>

</xsl:stylesheet>