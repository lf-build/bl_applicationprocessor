
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">

  <!--
  *********************************************
  * import template
  *********************************************
  -->
  <xsl:import href="../common/CorporateLinkage.xslt" />


  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * include template
  *********************************************
  -->
  <xsl:include href="IdentifyingInformation.xslt" />
  <xsl:include href="../common/MatchingNameAndAddress.xslt" />
  <xsl:include href="IPScore.xslt" />
  <xsl:include href="IPBusinessInfo.xslt" />
  <xsl:include href="IPOwnerInfo.xslt" />
  <xsl:include href="CustomFooterMessage.xslt" />
  <xsl:include href="CustomHeaderMessage.xslt" />


  <!--
  *********************************************
  * Intelliscore template
  *********************************************
  -->
  <xsl:template match="prd:Intelliscore" >
    <xsl:param name="standalone" select="1" />

    <xsl:variable name="isHTMLModel">
       <xsl:call-template name="isHTMLModel" />
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="normalize-space($isHTMLModel) = 'false' ">
        <xsl:call-template name="NoHTMLReport" />
      </xsl:when>

      <xsl:when test="normalize-space(prd:BusinessNameAndAddress/prd:ProfileType/@code) = 'NO RECORD' and not(contains($product, 'BOP')) ">
        <xsl:call-template name="businessNotFound" />
      </xsl:when>

      <xsl:otherwise>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top" height="20" align="center"><font size="4" color="{$titleColor}"><b><a name="IP">Intelliscore Plus<font size="3"><sup>SM</sup></font></a></b></font></td>
          </tr>
        </table>

        <!-- Identifying Information -->
        <xsl:call-template name="IdentifyingInformationScore" />
    
        <br />
  
        <!-- Custom Header message -->
        <xsl:if test="prd:CustomHeader">
          <xsl:call-template name="CustomHeaderMessage" />
          <br />
        </xsl:if>
    
        <xsl:if test="prd:BusinessNameAndAddress/prd:MatchingNameAndAddress">
          <!-- Matching Name And Address -->
          <xsl:apply-templates select="prd:BusinessNameAndAddress/prd:MatchingNameAndAddress" />
  
          <br />
        </xsl:if>
  
	    <xsl:if test="not(contains($product, 'BPR')) and prd:CorporateLinkage">
	      <!-- Corporate Linkage -->
	      <xsl:call-template name="CorporateLinkage" />
	    </xsl:if>

        <!-- Intelliscore Plus Score -->
        <xsl:call-template name="IntelliscorePlusScore" />
    
        <br />
    
        <xsl:if test="prd:ExecutiveElements and prd:IntelliscoreScoreInformation/prd:ModelInformation/prd:ModelCode and number(prd:IntelliscoreScoreInformation/prd:ModelInformation/prd:ModelCode) != 215 and prd:IntelliscoreScoreInformation/prd:Filler and number(prd:IntelliscoreScoreInformation/prd:Filler) != 7">
          <!-- Business Infomation -->
          <xsl:call-template name="BusinessInformation" />
  
          <br />
        </xsl:if>
        
        <xsl:if test="prd:SmallBusinessKeyModelElements and prd:IntelliscoreScoreInformation/prd:ModelInformation/prd:ModelCode and number(prd:IntelliscoreScoreInformation/prd:ModelInformation/prd:ModelCode) != 215">
          <!-- Owner Infomation -->
          <xsl:apply-templates select="prd:SmallBusinessKeyModelElements" />
  
          <br />
        </xsl:if>
  
        <!-- custom footer message -->
        <xsl:call-template name="CustomFooterMessage" />
        
      </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>


</xsl:stylesheet>