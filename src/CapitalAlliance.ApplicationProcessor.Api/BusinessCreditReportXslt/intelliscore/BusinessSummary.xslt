
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">
                

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * BusinessSummary template
  *********************************************
  -->
  <xsl:template name="BusinessSummary">
    <xsl:variable name="limitedReport">
      <xsl:choose>
        <xsl:when test="prd:ExecutiveElements">
          <xsl:value-of select="0" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="1" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="bankruptcy">
      <xsl:choose>
        <xsl:when test="prd:BusinessSummary/prd:BankruptcyFlag/@code = 'Y'">
          <xsl:value-of select="'Yes'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'No'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="lien">
      <xsl:choose>
        <xsl:when test="prd:BusinessSummary/prd:TaxLienFlag/@code = 'Y'">
          <xsl:value-of select="'Yes'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'No'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="judgment">
      <xsl:choose>
        <xsl:when test="prd:BusinessSummary/prd:JudgmentFlag/@code = 'Y'">
          <xsl:value-of select="'Yes'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'No'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="collection">
      <xsl:choose>
        <xsl:when test="prd:BusinessSummary/prd:CollectionFlag/@code = 'Y'">
          <xsl:value-of select="'Yes'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'No'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="bank">
      <xsl:choose>
        <xsl:when test="prd:BusinessSummary/prd:BankFlag/@code = 'Y'">
          <xsl:value-of select="'Yes'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'No'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="ucc">
      <xsl:choose>
        <xsl:when test="prd:BusinessSummary/prd:UCCFilingsFlag/@code = 'Y'">
          <xsl:value-of select="'Yes'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'No'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="corpfiling">
      <xsl:choose>
        <xsl:when test="number(prd:BusinessSummary/prd:CorporateFilingStatus/@code) = 29">
          <xsl:value-of select="'Active Business in Good Standing'" />
        </xsl:when>

        <xsl:when test="number(prd:BusinessSummary/prd:CorporateFilingStatus/@code) = 31">
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:when test="number(prd:BusinessSummary/prd:CorporateFilingStatus/@code) = 42">
          <xsl:value-of select="'Business has been reinstated'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="tradelines">
      <xsl:choose>
        <xsl:when test="prd:BusinessSummary/prd:NumberOfTradeLines">
          <xsl:value-of select="format-number(prd:BusinessSummary/prd:NumberOfTradeLines, '##,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="employees">
      <xsl:choose>
        <xsl:when test="prd:BusinessSummary/prd:NumberOfEmployees">
          <xsl:value-of select="format-number(prd:BusinessSummary/prd:NumberOfEmployees, '#,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="officer">
      <xsl:if test="prd:BusinessSummary/prd:KeyPersonnel1">
         <xsl:value-of select="normalize-space(prd:BusinessSummary/prd:KeyPersonnel1)" />
      </xsl:if>

      <xsl:if test="prd:BusinessSummary/prd:KeyPersonnel2">
         <xsl:if test="prd:BusinessSummary/prd:KeyPersonnel1">
         	<xsl:text>&lt;br/&gt;</xsl:text>
         </xsl:if>
         <xsl:value-of select="normalize-space(prd:BusinessSummary/prd:KeyPersonnel2)" />
      </xsl:if>

      <xsl:if test="prd:BusinessSummary/prd:KeyPersonnel3">
         <xsl:if test="prd:BusinessSummary/prd:KeyPersonnel1 or prd:BusinessSummary/prd:KeyPersonnel2">
         	<xsl:text>&lt;br/&gt;</xsl:text>
         </xsl:if>
         <xsl:value-of select="normalize-space(prd:BusinessSummary/prd:KeyPersonnel3)" />
      </xsl:if>
    </xsl:variable>


    <!-- begin business summary -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top" height="20">
          <font color="#0099cc"><b>Business Summary</b></font>
        </td>
      </tr>  
          
      <tr>
        <td width="100%" valign="top">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                               
            <tr>  
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="60%">
                      Bankruptcy filings:</td>
                    <td width="40%" align="right" nowrap="nowrap"><b><xsl:value-of select="$bankruptcy" /></b></td>
                  </tr>
                </table>
              </td>  
            </tr>

            <tr>
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="60%">
			    Tax lien filings:</td> 
                    <td width="40%" align="right" nowrap="nowrap"><b><xsl:value-of select="$lien" /></b></td>
                  </tr>
                </table>
              </td>  
            </tr>

            <tr>
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="60%">
			   Judgment filings:</td>
                    <td width="40%" align="right" nowrap="nowrap"><b><xsl:value-of select="$judgment" /></b></td>
                  </tr>
                </table>
              </td>  
            </tr>

            <tr>
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="60%">
			   Collection data:</td>
                    <td width="40%" align="right" nowrap="nowrap"><b><xsl:value-of select="$collection" /></b></td>
                  </tr>
                </table>
              </td>  
            </tr>

            <tr>
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="60%">
			   Banking data:</td>
                    <td width="40%" align="right" nowrap="nowrap"><b><xsl:value-of select="$bank" /></b></td>
                  </tr>
                </table>
              </td>  
            </tr>

            <tr>
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="60%">
			   UCC filings:</td>
                    <td width="40%" align="right" nowrap="nowrap"><b><xsl:value-of select="$ucc" /></b></td>
                  </tr>
                </table>
              </td>
            </tr>

            <tr>  
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="45%">
                      Corporate filing status:</td>
                    <td width="55%" align="right" nowrap="nowrap"><b><xsl:value-of select="$corpfiling" /></b></td>
                  </tr>
                </table>
              </td>  
            </tr>

            <tr>  
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="60%">
                      Number of trade lines:</td>
                    <td width="40%" align="right" nowrap="nowrap"><b><xsl:value-of select="$tradelines" /></b></td>
                  </tr>
                </table>
              </td>  
            </tr>

            <tr>  
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="60%">
                      Number of employees</td>
                    <td width="40%" align="right" nowrap="nowrap"><b><xsl:value-of select="$employees" /></b></td>
                  </tr>
                </table>
              </td>  
            </tr>

            <tr>  
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="35%" valign="top">
                      Principal officer(s):</td>
                    <td width="65%" align="right" nowrap="nowrap"><b><xsl:value-of select="$officer"  disable-output-escaping="yes" /></b></td>
                  </tr>
                </table>
              </td>  
            </tr>

          </table>
        </td>
      </tr>    
    </table>
    <!-- end legal filings and collections -->
    
  </xsl:template>

</xsl:stylesheet>