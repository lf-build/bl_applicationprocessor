
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">
                

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * Global variables and parameters
  *********************************************
  -->
  <xsl:variable name="hasIP">
    <xsl:choose>
      <xsl:when test="contains($product, 'IP')">
        <xsl:value-of select="1" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="0" />
      </xsl:otherwise>
    </xsl:choose>    
  </xsl:variable>
    
  <xsl:variable name="textCase">
    <xsl:choose>
      <xsl:when test="boolean(number($hasIP))">
        <xsl:value-of select="'upper'" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="'proper'" />
      </xsl:otherwise>
    </xsl:choose>    
  </xsl:variable>

  <!-- SSN Label -->
  <xsl:variable name="consumerOwner">
    <xsl:choose>
      <xsl:when test="boolean(number($hasIP))">
        <xsl:value-of select="'owner'" />
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="'consumer'" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
    

  <!--
  *********************************************
  * IdentifyingInformation template
  *********************************************
  -->
  <xsl:template name="IdentifyingInformationScore">

    <!-- Section title -->
    <xsl:call-template name="SectionTitle">
      <xsl:with-param name="title" select="'Identifying Information'" />
      <xsl:with-param name="color" select="$titleColor" />
    </xsl:call-template>

    <!-- blue box border -->
    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="{$borderColor}">

          <!-- inner white box -->
          <table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tr>
              <td bgcolor="#ffffff">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  
                  <!-- business data section -->  
                  <xsl:choose>		              
                    <xsl:when test="prd:BusinessNameAndAddress">
                      <xsl:apply-templates select="prd:BusinessNameAndAddress" mode="intelliscore" />
                    </xsl:when>
                    <xsl:otherwise>
                      <tr>
                        <td colspan="2">
                          <font color="#ff0000" size="1"><b>No business data available</b></font>
                        </td>
                      </tr>  
                    </xsl:otherwise>
                  </xsl:choose>    
                  <!-- end business data section -->  

                  <xsl:if test="prd:ProprietorNameAndAddress">

	                  <!-- space row -->
	                  <tr>
	                    <td style="line-height:5px;" colspan="2">&#160;</td>
	                  </tr>  
	                  
	                  <!-- divider line -->
	                  <tr>
	                    <td bgcolor="{$borderColor}" colspan="2" style="line-height:1px">&#160;</td>
	                  </tr>
	
	                  <!-- space row -->
	                  <tr>
	                    <td style="line-height:5px;" colspan="2">&#160;</td>
	                  </tr>  
	                    
	                  <!-- consumer data section -->  
	                  <xsl:choose>		              
	                    <xsl:when test="count(prd:ProprietorNameAndAddress) &gt; 0">
	                      <xsl:apply-templates select="prd:ProprietorNameAndAddress" mode="intelliscore" />
	                    </xsl:when>
	                    <xsl:otherwise>
	                      <tr>
	                        <td colspan="2">
	                          <font color="#ff0000" size="1" style="font-family: 'verdana';"><b>No <xsl:value-of select="$consumerOwner" /> data available</b></font>
	                        </td>
	                      </tr>  
	                    </xsl:otherwise>
	                  </xsl:choose>    
	                  <!-- end consumer data section -->  
                  
                  </xsl:if>
                  
                </table>
              </td>
            </tr>
          </table>
          <!-- end inner white box -->
        </td>
      </tr>  
    </table>

  </xsl:template>



  <!--
  *********************************************
  * BusinessNameAndAddress template
  *********************************************
  -->
  <xsl:template match="prd:BusinessNameAndAddress" mode="intelliscore" >

    <!-- Report Date  -->
    <xsl:variable name="reportDate">
      <xsl:value-of select="prd:ProfileDate" />
    </xsl:variable>

    <!-- get year from date extension -->
    <xsl:variable name="reportYear">
      <xsl:value-of select="substring(normalize-space($reportDate), 1, 4)"/>
    </xsl:variable>

    <!-- StreetAddress -->
    <xsl:variable name="streetAddress">
      <xsl:variable name="StreetAddress">
        <xsl:value-of select="prd:StreetAddress" />
      </xsl:variable>
	
      <xsl:call-template name="convertcase">
	      <xsl:with-param name="toconvert" select="$StreetAddress" />
	      <xsl:with-param name="conversion" select="$textCase" />
      </xsl:call-template>
    </xsl:variable>

    <!-- City -->
    <xsl:variable name="city">
      <xsl:variable name="City">
	      <xsl:value-of select="normalize-space(prd:City)" />
      </xsl:variable>
	
      <xsl:call-template name="convertcase">
        <xsl:with-param name="toconvert" select="$City" />
        <xsl:with-param name="conversion" select="$textCase" />
      </xsl:call-template>
    </xsl:variable>

    <!-- Date Incorporation -->
    <xsl:variable name="dateIncorporation">
      <xsl:choose>
        <xsl:when test="//prd:ExecutiveElements/prd:YearofIncorporation and normalize-space(//prd:ExecutiveElements/prd:YearofIncorporation) != '' ">
          <xsl:call-template name="FormatDate">
            <xsl:with-param name="pattern" select="'mo/dt/year'" />
            <xsl:with-param name="value" select="//prd:ExecutiveElements/prd:YearofIncorporation" />
          </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- SIC -->
    <xsl:variable name="sicCode">
      <xsl:choose>
        <xsl:when test="prd:SIC and normalize-space(prd:SIC/@code) != '' ">
          <xsl:call-template name="convertcase">
            <xsl:with-param name="toconvert" select="prd:SIC" />
            <xsl:with-param name="conversion" select="$textCase" />
          </xsl:call-template>
          <xsl:value-of select="concat(' - ' , prd:SIC/@code)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <!-- Tax ID -->
    <xsl:variable name="taxID">
      <xsl:choose>
        <xsl:when test="//prd:ExecutiveElements/prd:TaxID and normalize-space(//prd:ExecutiveElements/prd:TaxID) != '' ">
          <xsl:variable name="tmpID">
            <xsl:value-of select="//prd:ExecutiveElements/prd:TaxID" />
          </xsl:variable>
          <xsl:value-of select="concat(substring(normalize-space($tmpID), 1, 2), '-' , substring(normalize-space($tmpID), 3))" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <tr>
	<td width="100%" valign="top" colspan="2"><font size="1" style="FONT-FAMILY: 'verdana';">
	This information is the primary name and address for the business you inquired on. All data in this report pertains to the business.</font></td>
    </tr>
    <tr>
      <td style="line-height:5px;" colspan="2">&#160;</td>
    </tr>

    <tr>
      <!-- company name etc column -->
      <td width="50%" valign="top">
        <font size="1" style="FONT-FAMILY: 'verdana';">
          <b><xsl:value-of select="prd:BusinessName" /></b>
          <xsl:if test="prd:StreetAddress and string-length(normalize-space(prd:StreetAddress)) &gt; 0">
             <br />
             <xsl:value-of select="$streetAddress" /> 
          </xsl:if>
          <br />
          <xsl:value-of select="normalize-space($city)" /><xsl:if test="normalize-space($city) != '' and normalize-space(prd:State) != ''">,</xsl:if> 
          <xsl:value-of select="prd:State" />
          <xsl:text disable-output-escaping="yes"> </xsl:text>
          <xsl:call-template name="FormatZip">
            <xsl:with-param name="value" select="concat(prd:Zip, prd:ZipExtension)" />
          </xsl:call-template>
          <br />
          <xsl:if test="prd:PhoneNumber">
				  <xsl:call-template name="FormatPhone">
				    <xsl:with-param name="value" select="translate(prd:PhoneNumber, '-', '')" />
				  </xsl:call-template>
          </xsl:if>

          <xsl:if test="../prd:IntelliscoreScoreInformation/prd:CustomerDispute/prd:Statement/@code = 'Y' or ../prd:Statement">
	          <br />
	          <font color="#ff0000" size="1" style="font-family: 'verdana';">
	          	<b>See business statement on file</b>
	          	<!-- <a href="#BusinessStatement">details</a> -->
 		   </font>
          </xsl:if>
        </font>

      </td>
      <!-- end company name etc column -->
      
      <!-- file number etc column -->
      <td width="50%" valign="top">


        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td colspan="2">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
	            <td width="40%" nowrap="nowrap"><font size="1" style="font-family: 'verdana';">
	              <b>Business Identification Number:</b></font></td>
	            <td width="60%" align="right" nowrap="nowrap"><font size="1" style="font-family: 'verdana';">
	              <xsl:value-of select="prd:ExperianFileNumber" /></font></td>
          	   </tr>
          	 </table>
            </td>
          </tr>

          <xsl:if test="../prd:IntelliscoreScoreInformation/prd:ProfileNumber">
            <tr>
              <td nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">
                <b>Full Report Number:</b></font></td>
              <td align="right" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">
                FR-<xsl:value-of select="../prd:IntelliscoreScoreInformation/prd:ProfileNumber" /></font></td>
            </tr>
          </xsl:if>

          <tr>
            <td width="40%" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">
              <b>Years on File:</b></font></td>
            <td width="60%" align="right" nowrap="nowrap">
              <font size="1" style="FONT-FAMILY: 'verdana';">
              <xsl:if test="prd:FileEstablishDate">
              	<xsl:value-of select="($reportYear - number(substring(prd:FileEstablishDate, 1, 4)))" /><xsl:if test="prd:FileEstablishFlag/@code = 'P'">+ </xsl:if>
	              <xsl:choose>		              
	                <xsl:when test="prd:FileEstablishFlag/@code = 'P'">
	                  (FILE ESTABLISHED PRIOR TO 01/1977)
	                </xsl:when>
	
	                <xsl:otherwise>
	                  (FILE ESTABLISHED 
							   <xsl:call-template name="FormatDate">
							     <xsl:with-param name="pattern" select="'mo/year'" />
							     <xsl:with-param name="value" select="prd:FileEstablishDate" />
							   </xsl:call-template>)
	                </xsl:otherwise>
	              </xsl:choose>    
              </xsl:if>
              </font>
            </td> 
             
          </tr>

          <xsl:if test="normalize-space($dateIncorporation) and boolean(number($hasIP))">
            <tr>
              <td nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">
                <b>Date of Incorporation:</b></font></td>
              <td align="right" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">
                <xsl:value-of select="$dateIncorporation" /></font></td>
            </tr>
          </xsl:if>
          
          <xsl:if test="normalize-space($sicCode) and boolean(number($hasIP))">
			       <tr>
			         <td colspan="2">
			           <table width="100%" border="0" cellspacing="0" cellpadding="0">
			             <tr>
			               <td width="15%" nowrap="nowrap" valign="top"><font size="1" style="FONT-FAMILY: 'verdana';">
			                 <b>SIC Code:</b></font></td>
			               <td width="85%" nowrap="nowrap" align="right"><font size="1" style="FONT-FAMILY: 'verdana';">
			                 <xsl:value-of select="$sicCode" disable-output-escaping="yes" /></font></td>
			             </tr>
			           </table>
			         </td>
			       </tr>
          </xsl:if>
          
          <xsl:if test="normalize-space($taxID) and boolean(number($hasIP))">
            <tr>
              <td nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">
                <b>Tax ID:</b></font></td>
              <td align="right" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">
                <xsl:value-of select="$taxID" /></font></td>
            </tr>
          </xsl:if>
          
        </table>

      </td>
      <!-- end file number etc column -->

    </tr>

  </xsl:template>


  <!--
  *********************************************
  * ProprietorNameAndAddress template
  *********************************************
  -->
  <xsl:template match="prd:ProprietorNameAndAddress" mode="intelliscore" >
    <xsl:variable name="ownerCount">
      <xsl:value-of select="count(../prd:ProprietorNameAndAddress)" />
    </xsl:variable>

    <xsl:variable name="href">
      <xsl:choose>
        <xsl:when test="position() > count(../../prd:CreditProfile)">
          <xsl:value-of select="concat('#BOP', count(../../prd:CreditProfile) - 1) " />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="concat('#BOP', position() - 1) " />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- StreetAddress -->
    <xsl:variable name="streetAddress">
      <xsl:variable name="StreetAddress">
        <xsl:value-of select="prd:StreetAddress" />
      </xsl:variable>
	
      <xsl:call-template name="convertcase">
        <xsl:with-param name="toconvert" select="$StreetAddress" />
        <xsl:with-param name="conversion" select="$textCase" />
      </xsl:call-template>
    </xsl:variable>

    <!-- City -->
    <xsl:variable name="city">
      <xsl:variable name="City">
        <xsl:value-of select="normalize-space(prd:City)" />
      </xsl:variable>
	
      <xsl:call-template name="convertcase">
        <xsl:with-param name="toconvert" select="$City" />
        <xsl:with-param name="conversion" select="$textCase" />
      </xsl:call-template>
    </xsl:variable>

    <!-- SSN Label -->
    <xsl:variable name="ssnLabel">
      <xsl:choose>
        <xsl:when test="boolean(number($hasIP))">
          <xsl:value-of select="'Owner/Guarantor'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'Signatory'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- SSN  -->
    <xsl:variable name="ssn">
      <xsl:choose>
        <xsl:when test="prd:SSN">
<!--
            <xsl:value-of select="concat('XXX-XX-', substring(prd:SSN, 6))" />
-->
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- FCRA Label -->
    <xsl:variable name="fcraLabel">
      <xsl:choose>
        <xsl:when test="boolean(number($hasIP))">
          <xsl:value-of select="'FCRA'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <tr>
      <!-- owner name etc column -->
      <td width="50%" valign="top">
        <font size="1" style="FONT-FAMILY: 'verdana';">
          <xsl:value-of select="prd:ProprietorName" />
          <br />
          <xsl:value-of select="$streetAddress" /> 
          <br />
          <xsl:value-of select="normalize-space($city)" /><xsl:if test="prd:City and prd:State">, </xsl:if>
          <xsl:value-of select="prd:State" />
          <xsl:text disable-output-escaping="yes"> </xsl:text>
          <xsl:value-of select="prd:Zip" /> 
        </font>            
      </td>
      <!-- end owner name etc column -->
      
      <!-- SSN etc column -->
      <td width="50%" valign="top">

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="40%" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">
                <b><xsl:value-of select="$ssnLabel" /> SSN:</b></font></td>
              <td width="60%" align="right" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">
                <xsl:value-of select="$ssn" /></font></td>
            </tr>
        </table>

      </td>
      <!-- end SSN etc column -->
    </tr>

    <xsl:if test="starts-with(prd:ProfileType/@code, 'NO RECORD')">
      <tr>
        <td colspan="2">
          <img src="../images/spacer.gif" border="0" width="1" height="3" alt="" /><br />
          <font color="#ff0000" size="1" style="font-family: 'verdana';"><b>No <xsl:value-of select="$consumerOwner" /> data available</b></font>
        </td>
      </tr>  
    </xsl:if>

    <xsl:if test="starts-with(prd:ProfileType/@code, 'DISPUTE') ">
      <tr>
        <td colspan="2" style="padding-top:3px;">
          <font size="1" style="FONT-FAMILY: 'verdana';" color="#ff0000"><b>See consumer statement / <xsl:value-of select="$fcraLabel" /> alert on Business Owner Profile report</b></font>
          <a href="{$href}">details</a>
        </td>
      </tr>
    </xsl:if>

    <xsl:if test="position() &lt; $ownerCount">
      <!-- space row -->
      <tr>
        <td style="line-height:5px;" colspan="2">&#160;</td>
      </tr>  
                    
      <!-- divider line -->
      <tr>
        <td bgcolor="{$borderColor}" colspan="2" style="line-height:1px">&#160;</td>
      </tr>

      <!-- space row -->
      <tr>
        <td style="line-height:5px;" colspan="2">&#160;</td>
      </tr>  
      
    </xsl:if>

  </xsl:template>

</xsl:stylesheet>
