
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:rsp="http://www.experian.com/NetConnectResponse"
  xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * Global variables and parameters
  *********************************************
  <xsl:param name="systemYear" select="'2006'" />
  -->
  <xsl:variable name="Product">
    <xsl:choose>
      <xsl:when test="//prd:CreditProfile or normalize-space(//prd:Intelliscore/prd:BusinessNameAndAddress/prd:ProfileType/@code) = 'SBI/OWNER' ">
        <xsl:value-of select="'IPBOP'" />
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="'IP'" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
   
  <xsl:param name="product" select="$Product" />
  <xsl:param name="baseProduct" select="'IP'" />


  <!--
  *********************************************
  * include template
  *********************************************
  -->
  <xsl:include href="../common/ReportHeader.xslt" />
  <xsl:include href="../common/Util.xslt" />
  <xsl:include href="../common/case.xslt" />
  <xsl:include href="../common/BackToTop.xslt" />
  <xsl:include href="../common/ErrorProcessing.xslt" />
  <xsl:include href="../common/ReportFooter.xslt" />

  <xsl:include href="IPIntelliscore.xslt" />
  <xsl:include href="../ownerprofile/CreditProfile.xslt" />


  <!--
  *********************************************
  * Initial template
  *********************************************
  -->  
  <xsl:template match="/">
    <html>
      <head>
        <title>Intelliscore Plus Report</title>

          <style type="text/css">
            td {font-size: 9pt; font-family: 'arial';}
          </style>

      </head>
      <body>
        <a name="top"></a>
        <table width="715" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td>      
              <xsl:apply-templates select="//prd:Intelliscore" mode="header" />
              
              <!-- Intelliscore template -->
              <xsl:apply-templates select="//prd:Intelliscore" />
              
              <xsl:if test="//prd:CreditProfile"> 
                <!-- Back to Top graphic -->
                <xsl:call-template name="BackToTop" />
              </xsl:if>
              
              <!-- BOP template -->
              <xsl:apply-templates select="//prd:CreditProfile">
                <xsl:with-param name="standalone" select="0" /> 
              </xsl:apply-templates>

              <xsl:apply-templates select="//prd:Intelliscore" mode="footer" />
            </td>
          </tr>
        </table>    
      </body>
    </html>
  </xsl:template>


  <!--
  *********************************************
  * Header template
  *********************************************
  -->
  <xsl:template match="prd:Intelliscore" mode="header" >
    <!-- Profile Type  -->
    <xsl:variable name="profileType">
      <xsl:choose>
        <xsl:when test="contains(prd:InputSummary/prd:Inquiry, '/BOP') ">
          <xsl:value-of select=" 'Intelliscore Plus and BOP'" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'Intelliscore Plus'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- Report Header -->
    <xsl:call-template name="ReportHeader">
      <xsl:with-param name="reportType" select="$profileType" />
      <xsl:with-param name="reportName" select="prd:BusinessNameAndAddress/prd:BusinessName" />
    </xsl:call-template>

    <br />
  </xsl:template>
  
  <!--
  *********************************************
  * Footer template
  *********************************************
  -->
  <xsl:template match="prd:Intelliscore" mode="footer" >
    <!-- Report Date  -->
    <xsl:variable name="reportDate">
      <xsl:value-of select="prd:BusinessNameAndAddress/prd:ProfileDate" />
    </xsl:variable>

    <!-- is it a limited report?  -->
    <xsl:variable name="reportEnd">
       <xsl:choose>                   
         <xsl:when test="normalize-space(prd:BillingIndicator/@code) = 'A' ">
            <xsl:value-of select="'End of limited'" />
         </xsl:when>

         <xsl:otherwise>
            <xsl:value-of select="'End of report'" />
         </xsl:otherwise>
       </xsl:choose>    
    </xsl:variable>

    <!-- Report Footer -->
    <xsl:call-template name="ReportFooter">
      <xsl:with-param name="reportType" select="$product" />
      <xsl:with-param name="reportDate" select="$reportDate" />
      <xsl:with-param name="reportEnd" select="$reportEnd" />
    </xsl:call-template>
  </xsl:template>
  
  
  <!--
  *********************************************
  * this template overrides the built-in rule
  * do nothing with stuff I'm not interested in
  *********************************************
  -->
  <xsl:template match="text()|@*">
  </xsl:template>

</xsl:stylesheet>