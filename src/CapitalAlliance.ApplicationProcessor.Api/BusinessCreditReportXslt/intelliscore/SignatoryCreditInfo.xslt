
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rsp="http://www.experian.com/NetConnectResponse"
                xmlns:prd="http://www.experian.com/ARFResponse">

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * SignatoryCreditInfo template
  *********************************************
  -->
  <xsl:template name="SignatoryCreditInfo">
  
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <!-- SmallBusinessKeyModelElements template -->
      <xsl:apply-templates select="prd:SmallBusinessKeyModelElements" />
      
    </table>

  </xsl:template>


  <!--
  *********************************************
  * SmallBusinessKeyModelElements template
  *********************************************
  -->
  <xsl:template match="prd:SmallBusinessKeyModelElements" >

    <xsl:variable name="position">
      <xsl:value-of select="position()" />
    </xsl:variable>

    <xsl:if test="position() &gt; 1">
      <tr>
		<td colspan="3">
		    <!-- Back to Top grapic -->
		    <xsl:call-template name="BackToTop" />
		</td>
      </tr>
    </xsl:if>

    <tr>
      <!-- score box column -->
      <td width="49%"  valign="top">
        <xsl:call-template name="SignatoryScore" />

	    <xsl:if test="position() = last()">
	       <br />
	       <xsl:call-template name="CustomFooterMessage" />
	       <br />
	    </xsl:if>
      </td>

      <td align="center" width="10">
      		<xsl:text disable-output-escaping="yes">&#160;</xsl:text>
      </td>

      <td width="49%"  valign="top">
<!--
        <img src="../images/spacer.gif" border="0" width="1" height="6" alt=""/>
-->
        <xsl:call-template name="SignatoryCreditDetail" />
      </td>

    </tr>

   <!-- Display consumer statement, if available  -->
    <xsl:if test="../../prd:CreditProfile[position() = $position]/prd:Statement/prd:StatementText/prd:MessageText">
      <tr>
        <td style="line-height:20px;" colspan="3">&#160;</td>
      </tr>  

      <tr>
        <td colspan="3">
          <font size="3" color="{$titleColor}"><b>
          <xsl:value-of select="'Consumer Statement'" />
          </b></font>
        </td>
      </tr>
      
      <tr>
        <td colspan="3">
    		<xsl:apply-templates select="../../prd:CreditProfile[position() = $position]/prd:Statement/prd:StatementText/prd:MessageText" />
        </td>
      </tr>
    </xsl:if>

  </xsl:template>


  <!--
  *********************************************
  * consumer statement template
  *********************************************
  -->
  <xsl:template match="prd:MessageText">

    <xsl:if test="position() &gt; 1">
    	<br /><br />
    </xsl:if>

    <xsl:value-of select="." />
      
  </xsl:template>


  <!--
  *********************************************
  * SignatoryScore template
  *********************************************
  -->
  <xsl:template name="SignatoryScore">

    <xsl:variable name="unscorable">
       <xsl:choose>
         <xsl:when test="number(prd:ScorexPlusValue) >= 9000">
	  	<xsl:value-of select="'true'" />
         </xsl:when>

         <xsl:otherwise>
	  	<xsl:value-of select="'false'" />
         </xsl:otherwise>
       </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="name">
      <xsl:value-of select="concat(prd:ProprietorFirstName, ' ', prd:ProprietorSurname)" />
    </xsl:variable>
  
    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="{$borderColor}">
        
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#ffffff">
                <!-- box header -->
                <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                  <tr>
                    <td height="20" bgcolor="{$borderColor}" align="left" valign="middle">
                      <img src="../images/spacer.gif" border="0" width="5" height="1" alt=""/>
                      <font color="#ffffff" size="2"><b>ScorexPLUS<sup>SM</sup> Score
                      - <xsl:value-of select="$name" /></b></font>
                    </td>
                  </tr>  

                  <tr>
                    <td style="line-height:6px;">&#160;</td>
                  </tr>  

                  <tr>
                    <td align="center"><font size="3">ScorexPLUS<font size="2"><sup>SM</sup></font> score:
                      <b><xsl:value-of select="number(prd:ScorexPlusValue)" /></b></font></td>
                  </tr>  

                  <tr>
                    <td style="line-height:5px;" valign="bottom">&#160;</td>
                  </tr>  

                  <tr bgcolor="#ffffff">
                    <td align="center">
                      <table cellspacing="0" cellpadding="0">
                        <tr>
                          <td align="left"><font size="1" style="FONT-FAMILY: 'verdana';">
                              <b>Risk Category</b></font></td>
                          <td>
                            <img src="../images/global/spacer.gif" border="0" width="10" height="1" alt="" /></td>
                          <td><font size="1" style="FONT-FAMILY: 'verdana';">
                              <b>Score Range</b></font></td>
                        </tr>
                        <tr>
                          <td align="left" colspan="2"><font size="1" style="FONT-FAMILY: 'verdana';">
                              Low</font></td>
                          <td align="left"><font size="1" style="FONT-FAMILY: 'verdana';">
                              780 - 900</font></td>
                        </tr>
                        <tr>
                          <td align="left" colspan="2"><font size="1" style="FONT-FAMILY: 'verdana';">
                              Low - Medium</font></td>
                          <td align="left"><font size="1" style="FONT-FAMILY: 'verdana';">
                              681 - 779</font></td>
                        </tr>
                        <tr>
                          <td align="left" colspan="2"><font size="1" style="FONT-FAMILY: 'verdana';">
                              Medium</font></td>
                          <td align="left"><font size="1" style="FONT-FAMILY: 'verdana';">
                              620 - 680</font></td>
                        </tr>
                        <tr>
                          <td align="left" colspan="2"><font size="1" style="FONT-FAMILY: 'verdana';">
                              Medium - High</font></td>
                          <td align="left"><font size="1" style="FONT-FAMILY: 'verdana';">
                              521 - 619</font></td>
                        </tr>
                        <tr>
                          <td align="left" colspan="2"><font size="1" style="FONT-FAMILY: 'verdana';">
                              High</font></td>
                          <td align="left"><font size="1" style="FONT-FAMILY: 'verdana';">
                              300 - 520</font></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  
                  <tr>
                    <td style="line-height:10px;" valign="bottom">&#160;</td>
                  </tr>  
                </table>

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>  
                    <td width="1%">
                      <img src="../images/spacer.gif" border="0" width="5" height="1" alt=""/></td>

                    <xsl:choose>
                    	<xsl:when test="$unscorable = 'true'">
		                    <td width="98%">
		                      <b>Unscorable due to the following reason:</b>
		                      <br />
		                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
							    <tr>
				                  <td>
				                    <ul class="list" style=" padding:0;margin:0;padding-left:5px;margin-bottom:5px;">
				                      <li style="padding: 0 0 0 10px;margin: 5px 0 0;background: transparent url(../images/sprites_icons.png) no-repeat -95px -244px; list-style:none;">
			                            <xsl:call-template name="translateUnscorableScorexPlus">
			                               <xsl:with-param name="code" select="number(prd:ScorexPlusValue)" />
			                            </xsl:call-template>
				                      </li>
				                    </ul>
				                  </td>
							    </tr>
		                      </table>  
		                    </td>
                    	</xsl:when>
                    	<xsl:otherwise>
		                    <td width="98%">
		                      <xsl:if test="prd:ConsumerAdverseAction">
	  		                      <b>Negative score factors:</b>
			                      <br />
			                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
			                        <xsl:apply-templates select="prd:ConsumerAdverseAction" />
			                      </table>
		                      </xsl:if>  
		                    </td>
                    	</xsl:otherwise>
                    </xsl:choose>
    
                    <td width="1%">
                      <img src="../images/spacer.gif" border="0" width="5" height="1" alt=""/></td>

                  </tr>

                  <tr>
                    <td style="line-height:3px;" colspan="3">&#160;</td>
                  </tr>
                </table>
               
              </td>
            </tr>

          </table>
        </td>
      </tr>
    </table>
  </xsl:template>


  <!--
  *********************************************
  * ConsumerAdverseAction template
  *********************************************
  -->
  <xsl:template match="prd:ConsumerAdverseAction" >

    <!-- Suppress displaying "No Message Found" -->
<!--
    Remove this code per Business Decision (Display all score factors!)
    <xsl:if test="number(prd:Code) > 0">
-->
	    <!-- ScoreFactor -->
	     <xsl:variable name="advAction">
		<xsl:variable name="description">
		  <xsl:value-of select="normalize-space(prd:Description)" />
		</xsl:variable>
		
		<xsl:call-template name="convertcase">
		   <xsl:with-param name="toconvert" select="$description" />
		   <xsl:with-param name="conversion" select="'proper'" />
		</xsl:call-template>
	    </xsl:variable>

	    <tr>
	      <td>
	        <ul class="list" style=" padding:0;margin:0;padding-left:5px;margin-bottom:5px;"><li style="padding: 0 0 0 10px;margin: 5px 0 0;background: transparent url(../images/sprites_icons.png) no-repeat -95px -244px; list-style:none;"><xsl:value-of select="normalize-space($advAction)" /></li></ul>
	      </td>
	    </tr>
	
  </xsl:template>


  <!--
  *********************************************
  * SignatoryCreditDetail template
  *********************************************
  -->
  <xsl:template name="SignatoryCreditDetail">

    <xsl:variable name="proprietorName">
      <xsl:choose>
        <xsl:when test="prd:ProprietorFirstName and prd:ProprietorSurname">		    		   		   
          <xsl:value-of select="concat(normalize-space(prd:ProprietorFirstName), ' ', normalize-space(prd:ProprietorSurname))" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

   <!-- ALL701-->
    <xsl:variable name="ageOldestTrade">
      <xsl:choose>
        <xsl:when test="number(prd:AgeOldestTradeline) &gt; 0">
           <xsl:variable name="months">
		<xsl:value-of select="number(prd:AgeOldestTradeline) mod 12" />
	    </xsl:variable>

           <xsl:variable name="years">
		<xsl:value-of select="floor(number(prd:AgeOldestTradeline) div 12) " />
	    </xsl:variable>
	    
	   <xsl:if test="$years > 0">
	      <xsl:value-of select="concat($years, ' YRS ')" />
	   </xsl:if>
          <xsl:value-of select="concat($months, ' MO')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0 MO'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

   <!-- ALL803 -->
    <xsl:variable name="totalInquiries6Months">
      <xsl:choose>
        <xsl:when test="number(prd:TotalCreditPurposeInquiriesLast6Mos) &gt; 0">
          <xsl:value-of select="number(prd:TotalCreditPurposeInquiriesLast6Mos)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

   <!-- ALL021 -->
    <xsl:variable name="totalOpenTrades6Months">
      <xsl:choose>
        <xsl:when test="number(prd:TotalOpenTrades6Months) &gt; 0">		   
          <xsl:value-of select="number(prd:TotalOpenTrades6Months)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

   <!-- ALL208 -->
    <xsl:variable name="monthlyPayment6Months">
      <xsl:choose>
        <xsl:when test="number(prd:MonthlyPayment6Months) &gt; 0">		   
          <xsl:value-of select="format-number(prd:MonthlyPayment6Months, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

   <!-- ALL403 -->
    <xsl:variable name="balanceLimitRatio6Months">
      <xsl:choose>
        <xsl:when test="number(prd:BalanceLimitRatio6Months) &gt; 0">		   
          <xsl:value-of select="number(prd:BalanceLimitRatio6Months)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

   <!-- ALL003 -->
    <xsl:variable name="totalOpenTrades">
      <xsl:choose>
        <xsl:when test="number(prd:TotalOpenTrades) &gt; 0">		   
          <xsl:value-of select="number(prd:TotalOpenTrades)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

   <!-- ALL201 -->
    <xsl:variable name="balanceOpenTrades6Months">
      <xsl:choose>
        <xsl:when test="number(prd:BalanceOpenTrades6Months) &gt; 0">		   
          <xsl:value-of select="format-number(prd:BalanceOpenTrades6Months, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

   <!-- REV026 -->
    <xsl:variable name="totalRevolving6Months">
      <xsl:choose>
        <xsl:when test="number(prd:TotalRevolving6Months) &gt; 0">		   
          <xsl:value-of select="number(prd:TotalRevolving6Months)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

   <!-- REV201 -->
    <xsl:variable name="balanceRevolving6Months">
      <xsl:choose>
        <xsl:when test="number(prd:BalanceRevolving6Months) &gt; 0">		   
          <xsl:value-of select="format-number(prd:BalanceRevolving6Months, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

   <!-- REV401 -->
    <xsl:variable name="availableCreditRevolving6Months">
      <xsl:choose>
        <xsl:when test="number(prd:TotAvailableRevCreditLast6Mos) &gt; 0">		   
          <xsl:value-of select="format-number(prd:TotAvailableRevCreditLast6Mos, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

   <!-- BAC007 -->
    <xsl:variable name="totalOpenBankcards">
      <xsl:choose>
        <xsl:when test="number(prd:TotalOpenBankcards) &gt; 0">		   
          <xsl:value-of select="number(prd:TotalOpenBankcards)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

   <!-- ALL062 -->
    <xsl:variable name="totalPresentCurrent6Months">
      <xsl:choose>
        <xsl:when test="number(prd:TotalPresentCurrent6Months) &gt; 0">		   
          <xsl:value-of select="number(prd:TotalPresentCurrent6Months)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

   <!-- ALL051 -->
    <xsl:variable name="totalDelinquent6Months">
      <xsl:choose>
        <xsl:when test="number(prd:TotalDelinquent6Months) &gt; 0">		   
          <xsl:value-of select="number(prd:TotalDelinquent6Months)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

   <!-- ALL155 -->
    <xsl:variable name="totalDerogatoryCollectionPublicRecord">
      <xsl:choose>
        <xsl:when test="number(prd:TotCollectionsLegalFilingsOrDerogAccts) &gt; 0">		   
          <xsl:value-of select="number(prd:TotCollectionsLegalFilingsOrDerogAccts)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- begin legal filings and collections -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top" height="20">
          <font color="{$borderColor}"><b>Signatory Credit Information</b></font>
        </td>
      </tr>  

      <tr>
        <td width="100%" valign="top">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">

            <tr>  
              <td height="20">
              	<b><xsl:value-of select="$proprietorName" /></b>
              </td>  
            </tr>

            <tr>  
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="90%">
                      Age of oldest trade line:</td>
                    <td width="10%" align="right" nowrap="nowrap"><b><xsl:value-of select="$ageOldestTrade" /></b></td>
                  </tr>
                </table>
              </td>  
            </tr>

            <tr>  
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="90%">
                      Total credit inquires previous 6 months:</td>
                    <td width="10%" align="right" nowrap="nowrap"><b><xsl:value-of select="$totalInquiries6Months" /></b></td>
                  </tr>
                </table>
              </td>  
            </tr>

            <tr>  
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="90%">
                      Total trades opened previous 6 months:</td>
                    <td width="10%" align="right" nowrap="nowrap"><b><xsl:value-of select="$totalOpenTrades6Months" /></b></td>
                  </tr>
                </table>
              </td>  
            </tr>

            <tr>  
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="90%">
                      Current monthly obligation:</td>
                    <td width="10%" align="right" nowrap="nowrap"><b><xsl:value-of select="$monthlyPayment6Months" /></b></td>
                  </tr>
                </table>
              </td>  
            </tr>

            <tr>  
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="90%">
                      Balance to limit ratio:</td>
                    <td width="10%" align="right" nowrap="nowrap"><b><xsl:value-of select="$balanceLimitRatio6Months" />%</b></td>
                  </tr>
                </table>
              </td>  
            </tr>

            <tr>
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="90%">
                      Open trade balance to <xsl:value-of select="$totalOpenTrades" /> account(s):</td>
                    <td width="10%" align="right" nowrap="nowrap"><b><xsl:value-of select="$balanceOpenTrades6Months" /></b></td>
                  </tr>
                </table>
              </td>  
            </tr>

            <tr>
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="90%">
                      Open revolving trade balance for <xsl:value-of select="$totalRevolving6Months" /> account(s):</td>
                    <td width="10%" align="right" nowrap="nowrap"><b><xsl:value-of select="$balanceRevolving6Months" /></b></td>
                  </tr>
                </table>
              </td>  
            </tr>

            <tr>  
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="65%">
                      Available revolving credit:</td>
                    <td width="35%" align="right" nowrap="nowrap"><b><xsl:value-of select="$availableCreditRevolving6Months" /></b></td>
                  </tr>
                </table>
              </td>  
            </tr>

            <tr>  
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="65%">
                      Open bank card accounts:</td>
                    <td width="35%" align="right" nowrap="nowrap"><b><xsl:value-of select="$totalOpenBankcards" /></b></td>
                  </tr>
                </table>
              </td>  
            </tr>

            <tr>  
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="60%">
                      Trades presently current:</td>
                    <td width="40%" align="right" nowrap="nowrap"><b><xsl:value-of select="$totalPresentCurrent6Months" /></b></td>
                  </tr>
                </table>
              </td>  
            </tr>

            <tr>  
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="90%">
                      Trades presently delinquent or past due:</td>
                    <td width="10%" align="right" nowrap="nowrap"><b><xsl:value-of select="$totalDelinquent6Months" /></b></td>
                  </tr>
                </table>
              </td>  
            </tr>

            <tr>  
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="90%">
                      Collections, legal filings or derogatory accounts:</td>
                    <td width="10%" align="right" nowrap="nowrap"><b><xsl:value-of select="$totalDerogatoryCollectionPublicRecord" /></b></td>
                  </tr>
                </table>
              </td>  
            </tr>

          </table>
        </td>
      </tr>    
    </table>
    <!-- end legal filings and collections -->
    
  </xsl:template>


</xsl:stylesheet>