
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:rsp="http://www.experian.com/NetConnectResponse"
  xmlns:prd="http://www.experian.com/ARFResponse">
  

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * Global variables and parameters
  *********************************************
  <xsl:param name="systemYear" select="'2006'" />
  -->
  <xsl:variable name="Product">
    <xsl:choose>
      <xsl:when test="//prd:CreditProfile or normalize-space(//prd:Intelliscore/prd:BusinessNameAndAddress/prd:ProfileType/@code) = 'SBI/OWNER' ">
        <xsl:value-of select="'IPBPRBOP'" />
      </xsl:when>
      <xsl:when test="//prd:CreditProfile or normalize-space(//prd:Intelliscore/prd:BusinessNameAndAddress/prd:ProfileType/@code) = 'SBI' ">
        <xsl:value-of select="'IPBPRCon'" />
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="'IPBPR'" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  
  <xsl:param name="product" select="$Product" />
  <xsl:param name="baseProduct" select="''" />

<!-- 
  <xsl:variable name="modelCode">
    <xsl:choose>
      <xsl:when test="//prd:Intelliscore/prd:IntelliscoreScoreInformation/prd:ModelInformation/prd:ModelCode">
        <xsl:value-of select="number(//prd:Intelliscore/prd:IntelliscoreScoreInformation/prd:ModelInformation/prd:ModelCode)" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="0" />
      </xsl:otherwise>
    </xsl:choose>    
  </xsl:variable>
-->

  <xsl:variable name="SingleLineHighCredit">
      <xsl:if test="//prd:Intelliscore/prd:ExecutiveElements/prd:SingleLineHighCredit and number(//prd:Intelliscore/prd:ExecutiveElements/prd:SingleLineHighCredit) != 0 and number($modelCode) = 214">		    		   		   
        <xsl:value-of select="//prd:Intelliscore/prd:ExecutiveElements/prd:SingleLineHighCredit" />
      </xsl:if>
  </xsl:variable>
  <xsl:param name="singleLineHighCredit" select="$SingleLineHighCredit" />

  
  <!--
  *********************************************
  * include template
  *********************************************
  -->
  <xsl:include href="../common/ReportHeader.xslt" />
  <xsl:include href="../common/Util.xslt" />
  <xsl:include href="../common/case.xslt" />
  <xsl:include href="../common/BackToTop.xslt" />
  <xsl:include href="../common/ErrorProcessing.xslt" />
  <xsl:include href="../common/CorporateLinkage.xslt" />
  <xsl:include href="IdentifyingInformation.xslt" />
  <xsl:include href="IPSummary.xslt" />
  <xsl:include href="IPScore.xslt" />
  <xsl:include href="IPBusinessInfo.xslt" />
  <xsl:include href="IPOwnerInfo.xslt" />
  <xsl:include href="../common/CompanyInformation.xslt" />
  <xsl:include href="../businessprofile/TradeFilingSummary.xslt" />
  <xsl:include href="../businessprofile/ProfileSummary.xslt" />
  <xsl:include href="../businessprofile/CurrentDBT.xslt" />
  <xsl:include href="../businessprofile/QuarterlyDBT.xslt" />
  <xsl:include href="../businessprofile/MonthlyDBT.xslt" />
  <xsl:include href="../businessprofile/PerformanceAnalysis.xslt" />
  <xsl:include href="../businessprofile/LegalFilingsCollections.xslt" />
  <xsl:include href="../businessprofile/PaymentExperiences.xslt" />
  <xsl:include href="../businessprofile/TradePaymentTotals.xslt" />
  <xsl:include href="../businessprofile/MonthlyPaymentTrends.xslt" />
  <xsl:include href="../businessprofile/QuarterlyPaymentTrends.xslt" />
  <xsl:include href="../businessprofile/Inquiries.xslt" />
  <xsl:include href="../businessprofile/UCCProfile.xslt" />
  <xsl:include href="../businessprofile/CommercialFinance.xslt" />
  <xsl:include href="../businessprofile/CompanyBackground.xslt" />
  <xsl:include href="../businessprofile/AdditionalCompanyBackground.xslt" />
  <xsl:include href="../businessprofile/BusinessStatement.xslt" />
  <xsl:include href="../businessprofile/StandardAndPoors.xslt" />

  <xsl:include href="CustomHeaderMessage.xslt" />
  <xsl:include href="BusinessSummary.xslt" />
  <xsl:include href="CustomFooterMessage.xslt" />

  <xsl:include href="../ownerprofile/IdentifyingInformation.xslt" />
  <xsl:include href="../ownerprofile/SignatoryProfileSummary.xslt" />
  <xsl:include href="../ownerprofile/ConsumerStatement.xslt" />
  <xsl:include href="../ownerprofile/OFACWarning.xslt" />
  <xsl:include href="../ownerprofile/FraudShield.xslt" />
  <xsl:include href="../ownerprofile/LegalFilings.xslt" />
  <xsl:include href="../ownerprofile/Inquiries.xslt" />
  <xsl:include href="../ownerprofile/TradeInformation.xslt" />

  <xsl:include href="../common/ReportFooter.xslt" />


  <!--
  *********************************************
  * Initial template
  *********************************************
  -->  
  <xsl:template match="/">
    <html>
      <head>
        <title>Intelliscore Plus and BPR</title>

        <style type="text/css">
            td {font-size: 9pt; font-family: 'arial';}
        </style>
      </head>

      <body>
        <a name="top"></a>
        <table width="715" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td>      
               
              <!-- BusinessProfile template -->
              <xsl:apply-templates select="//prd:Intelliscore" />
              
            </td>
          </tr>
        </table>    
      </body>
    </html>
  </xsl:template>


  <!--
  *********************************************
  * Intelliscore template
  *********************************************
  -->
  <xsl:template match="prd:Intelliscore" >
    <!-- Report Date  -->
    <xsl:variable name="reportDate">
      <xsl:value-of select="prd:BusinessNameAndAddress/prd:ProfileDate" />
    </xsl:variable>

    <xsl:variable name="comboTitle">
      <xsl:choose>
        <xsl:when test="boolean(number($isCommercial))">
          <xsl:value-of select="'and BPR'" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
  
    <!-- Report Header -->
    <xsl:call-template name="ReportHeader">
      <xsl:with-param name="reportType" select="'Intelliscore Plus and BPR'" />
      <xsl:with-param name="reportName" select="prd:BusinessNameAndAddress/prd:BusinessName" />
    </xsl:call-template>

    <br />

    <xsl:variable name="isHTMLModel">
      <xsl:call-template name="isHTMLModel" />
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="normalize-space($isHTMLModel) = 'false' ">
        <xsl:call-template name="NoHTMLReport" />
      </xsl:when>

      <xsl:when test="normalize-space(prd:BusinessNameAndAddress/prd:ProfileType/@code) = 'NO RECORD' and not(contains($product, 'BOP')) ">
        <xsl:call-template name="businessNotFound" />
      </xsl:when>

      <xsl:otherwise>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top" height="20" align="center"><font size="4" color="{$titleColor}"><b><a name="IP">Intelliscore Plus<font size="3"><sup>SM</sup></font><xsl:value-of select="$comboTitle"/></a></b></font></td>
          </tr>
        </table>
        
  	    <!-- Identifying Information (for Intelliscore) -->
        <xsl:call-template name="IdentifyingInformationScore" />
        
        <br />

        <!-- Custom Header message -->
        <xsl:if test="prd:CustomHeader">
          <xsl:call-template name="CustomHeaderMessage" />
          <br />
        </xsl:if>
    
        <xsl:if test="prd:BusinessNameAndAddress/prd:MatchingNameAndAddress">
          <!-- Matching Name And Address -->
          <xsl:apply-templates select="prd:BusinessNameAndAddress/prd:MatchingNameAndAddress" />
  
          <br />
        </xsl:if>

        <xsl:if test="prd:CorporateLinkage">
          <!-- Corporate Linkage -->
          <xsl:call-template name="CorporateLinkage">
		    <xsl:with-param name="productOverride" select="'Yes'" />
          </xsl:call-template>
        </xsl:if>

        <!-- Intelliscore Plus Score -->
        <xsl:call-template name="IntelliscorePlusScore" />
    
        <br />

        <xsl:if test="prd:ExecutiveElements and prd:IntelliscoreScoreInformation/prd:ModelInformation/prd:ModelCode and number(prd:IntelliscoreScoreInformation/prd:ModelInformation/prd:ModelCode) != 215 and prd:IntelliscoreScoreInformation/prd:Filler and number(prd:IntelliscoreScoreInformation/prd:Filler) != 7">
          <!-- Business Infomation -->
          <xsl:call-template name="BusinessInformation" />
  
          <br />
        </xsl:if>
        
        <xsl:if test="prd:SmallBusinessKeyModelElements and prd:IntelliscoreScoreInformation/prd:ModelInformation/prd:ModelCode and number(prd:IntelliscoreScoreInformation/prd:ModelInformation/prd:ModelCode) != 215">
          <!-- Owner Infomation -->
          <xsl:apply-templates select="prd:SmallBusinessKeyModelElements" />
  
          <br />
        </xsl:if>
        
        <xsl:choose>
          <xsl:when test="boolean(number($isCommercial))">
          
            <xsl:if test="prd:BusinessNameAndAddress/prd:MatchingNameAndAddress">
              <!-- Matching Name And Address -->
              <xsl:apply-templates select="prd:BusinessNameAndAddress/prd:MatchingNameAndAddress" />
      
              <br />
            </xsl:if>
    
            <!-- Profile Summary -->
            <xsl:call-template name="IPSummary" />
            
            <!-- Back to Top graphic -->
            <xsl:call-template name="BackToTop" />

            <!-- rest of BPR sections -->
            <xsl:call-template name="BusinessProfile" />
            
            <br />
            <!-- custom footer message -->
            <xsl:call-template name="CustomFooterMessage" />
          </xsl:when>
          
          <xsl:otherwise>
          
            <!-- custom footer message -->
            <xsl:call-template name="CustomFooterMessage" />

            <!-- Back to Top graphic -->
            <xsl:call-template name="BackToTop" />
            <br/>

            <!--  starts BPR sections -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" height="20" align="center"><font size="4" color="{$titleColor}"><b><a name="BPR">Business Profile</a></b></font></td>
              </tr>
            </table>
        
            <xsl:call-template name="CompanyInformation" />
            
            <br />
            
            <xsl:if test="prd:BusinessNameAndAddress/prd:MatchingNameAndAddress">
              <!-- Matching Name And Address -->
              <xsl:apply-templates select="prd:BusinessNameAndAddress/prd:MatchingNameAndAddress" />
      
              <br />
            </xsl:if>
    
            <xsl:if test="prd:CorporateLinkage and $product='IPBPRCon' ">
	          <!-- Corporate Linkage -->
	          <xsl:call-template name="CorporateLinkage" />
            </xsl:if>

            <!-- Profile Summary -->
            <xsl:call-template name="ProfileSummary" />

            <!-- rest of BPR sections -->
            <xsl:call-template name="BusinessProfile" />

            <xsl:if test="contains($product, 'BOP') and ../prd:CreditProfile">  
              <!-- Back to Top graphic -->
              <xsl:call-template name="BackToTop" />

              <!-- bop processing -->
              <xsl:apply-templates select="../prd:CreditProfile" mode="IPBPR" />
            </xsl:if>
                    
          </xsl:otherwise>
        </xsl:choose>

  	    <!-- is it a limited report?  -->
  	    <xsl:variable name="reportEnd">
  	       <xsl:choose>		              
  	         <xsl:when test="normalize-space(prd:BillingIndicator/@code) = 'A' ">
  	            <xsl:value-of select="'End of limited'" />
  	         </xsl:when>
  	
  	         <xsl:otherwise>
  	            <xsl:value-of select="'End of report'" />
  	         </xsl:otherwise>
  	       </xsl:choose>    
  	    </xsl:variable>

  	    <!-- Report Footer -->
  	    <xsl:call-template name="ReportFooter">
  	      <xsl:with-param name="reportType" select="'BPR'" />
  	      <xsl:with-param name="reportDate" select="$reportDate" />
  	      <xsl:with-param name="reportEnd" select="$reportEnd" />
  	    </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>


    
  <!--
  *********************************************
  * BusinessProfile template
  *********************************************
  -->
  <xsl:template name="BusinessProfile">
    <xsl:if test="prd:CollectionData or prd:Bankruptcy or prd:TaxLien or prd:JudgmentOrAttachmentLien">
      <!-- LegalFilingsCollections -->
      <xsl:call-template name="LegalFilingsCollections" />
    </xsl:if>
  
    <xsl:if test="prd:TradePaymentExperiences">  
      <!-- TradePaymentInformation -->
      <xsl:call-template name="PaymentExperiences">
        <xsl:with-param name="title" select="'Trade Payment Information'" />
      </xsl:call-template>
      <br />
    </xsl:if>
  
    <xsl:if test="prd:PaymentTotals">
      <!-- TradePaymentTotals -->
      <xsl:call-template name="TradePaymentTotals" />
      <br />
    </xsl:if>
  
    <xsl:if test="prd:AdditionalPaymentExperiences">  
      <!-- TradePaymentInformation -->
      <xsl:call-template name="PaymentExperiences">
        <xsl:with-param name="title" select="'Additional Payment Experiences'" />
      </xsl:call-template>
      <br />
    </xsl:if>
  
    <xsl:if test="prd:IndustryPaymentTrends">
      <!-- MonthlyPaymentTrends -->
      <xsl:call-template name="MonthlyPaymentTrends" />
      <br />
    </xsl:if>
  
    <xsl:if test="prd:QuarterlyPaymentTrends">
      <!-- QuarterlyPaymentTrends -->
      <xsl:call-template name="QuarterlyPaymentHistory" />
      <br />
    </xsl:if>
  
    <xsl:if test="prd:Inquiry">
      <!-- QuarterlyPaymentTrends -->
      <xsl:call-template name="Inquiries" />
      <br />
    </xsl:if>
  
    <xsl:if test="prd:GovernmentFinancialExperiences">  
      <!-- GovernmentFinancialExperiences -->
      <xsl:call-template name="PaymentExperiences">
        <xsl:with-param name="title" select="'Government Financial Profile'" />
      </xsl:call-template>
      <br />
    </xsl:if>

    <xsl:if test="prd:UCCFilingsSummaryCounts or prd:UCCFilings">
      <!-- UCCProfile -->
      <xsl:call-template name="UCCProfile" />
      <br />
    </xsl:if>
  
    <xsl:if test="prd:CommercialBankInformation or prd:InsuranceData or prd:LeasingInformation">
      <!-- CommercialFinance -->
      <xsl:call-template name="CommercialFinance" />
    </xsl:if>

    <xsl:if test="prd:CorporateInformation or prd:CorporateOwnerInformation or prd:CorporateLinkageSummary or prd:CorporateLinkageNameAndAddress or prd:DemographicInformation or prd:KeyPersonnelExecutiveInformation">
      <!-- CompanyBackground -->
      <xsl:call-template name="CompanyBackground" />
      <br />
    </xsl:if>

    <xsl:if test="prd:StandardAndPoorsFinancialInformation">
      <!-- StandardAndPoorsFinancialInformation -->
      <xsl:call-template name="StandardAndPoors" />
    </xsl:if>

    <xsl:if test="prd:ConsumerStatement">
      <!-- BusinessStatement -->
      <xsl:call-template name="BusinessStatement" />
      <br />
    </xsl:if>

  </xsl:template>


  <!--
  *********************************************
  * bop  template
  *********************************************
  -->
  <xsl:template match="prd:CreditProfile" mode="IPBPR" >
    <!-- Report Date  -->
    <xsl:variable name="reportDate">
      <xsl:value-of select="../prd:BusinessProfile/prd:BusinessNameAndAddress/prd:ProfileDate" />
    </xsl:variable>

    <xsl:variable name="idx">
      <xsl:value-of select="position()" />
    </xsl:variable>

    <xsl:if test="normalize-space(//prd:ProprietorNameAndAddress[number($idx)]/prd:ProfileType/@code) != 'NO RECORD'">
      <xsl:if test="position() &gt; 1">
        <!-- Back to Top graphic -->
        <xsl:call-template name="BackToTop" />
      </xsl:if>
  
      <br />
  
      <!-- Identifying Information -->
      <xsl:call-template name="IdentifyingInformationBOP">
        <xsl:with-param name="index" select="position()" />
        <xsl:with-param name="underline" select="0" />
        <xsl:with-param name="titleSize" select="4" />
      </xsl:call-template>
  
      <br />
  
      <xsl:if test="prd:ProfileSummary">
        <!-- Signatory Profile Summary -->
        <xsl:call-template name="SignatoryProfileSummary">
          <xsl:with-param name="position" select="position()" />
        </xsl:call-template>
  
        <!-- Back to Top graphic -->
        <xsl:call-template name="BackToTop" />
      </xsl:if>
  
      <xsl:if test="prd:Statement">
        <!-- Consumer Statement -->
        <xsl:call-template name="ConsumerStatement" />
  
        <!-- Back to Top graphic -->
        <xsl:call-template name="BackToTop" />
      </xsl:if>
  
      <xsl:if test="prd:InformationalMessage and number(prd:InformationalMessage/prd:MessageNumber) = 57 and starts-with(prd:InformationalMessage/prd:MessageText, '1200 ')  ">
        <!-- Consumer Statement -->
        <xsl:call-template name="OFACWarning" />
  
        <!-- Back to Top graphic -->
        <xsl:call-template name="BackToTop" />
      </xsl:if>
  
      <xsl:if test="prd:FraudServices/prd:Indicator">
        <!-- Fraud Shield -->
        <xsl:call-template name="FraudShield" />
  
        <!-- Back to Top graphic -->
        <xsl:call-template name="BackToTop" />
      </xsl:if>
  
      <xsl:if test="prd:PublicRecord">
  
        <!-- Legal Filings -->
        <xsl:call-template name="LegalFilings" />
      </xsl:if>
  
      <xsl:if test="prd:Inquiry">
  
        <!-- Inquiries -->
        <xsl:call-template name="InquiriesBOP" />
  
        <!-- Back to Top graphic -->
        <xsl:call-template name="BackToTop" />
      </xsl:if>
  
      <xsl:if test="prd:TradeLine">
  
        <!-- TradeInformation -->
        <xsl:call-template name="TradeInformation" />
      </xsl:if>
    </xsl:if>
  </xsl:template>


  <!--
  *********************************************
  * this template overrides the built-in rule
  * do nothing with stuff I'm not interested in
  *********************************************
  -->
  <xsl:template match="text()|@*">
  </xsl:template>




</xsl:stylesheet>
