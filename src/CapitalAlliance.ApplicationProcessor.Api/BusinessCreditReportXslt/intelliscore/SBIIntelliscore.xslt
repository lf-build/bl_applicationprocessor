
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * import template
  *********************************************
  -->


  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * include template
  *********************************************
  -->
  <xsl:include href="IdentifyingInformation.xslt" />
  <xsl:include href="TradeFilingSummary.xslt" />
  <xsl:include href="ReportHeaderMessage.xslt" />
  <xsl:include href="BusinessCreditInfo.xslt" />
  <xsl:include href="CIScorable.xslt" />
  <xsl:include href="BusinessSummary.xslt" />
  <xsl:include href="SignatoryCreditInfo.xslt" />
  <xsl:include href="ConsumerStatementSBI.xslt" />
  <xsl:include href="CustomFooterMessage.xslt" />


  <!--
  *********************************************
  * Intelliscore template
  *********************************************
  -->
  <xsl:template match="prd:Intelliscore" >
    <xsl:param name="standalone" select="1" />

    <xsl:variable name="isHTMLModel">
       <xsl:call-template name="isHTMLModel" />
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="normalize-space($isHTMLModel) = 'false' ">
        <xsl:call-template name="NoHTMLReport" />
      </xsl:when>

      <xsl:otherwise>

        <xsl:if test="$product != 'SBI' ">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td valign="top" height="20" align="center"><font size="4" color="{$titleColor}"><b><a name="SBI">Blended Score</a></b></font></td>
            </tr>
          </table>
        </xsl:if>

        <!-- Identifying Information -->
        <xsl:call-template name="IdentifyingInformationScore" />
    
        <br />
        
        <!-- Report Header message -->
        <xsl:call-template name="ReportHeaderMessage" />
    
        <br />
    
        <!-- Business Credit Info -->
        <xsl:call-template name="BusinessCreditInfo" />
  
        <xsl:if test="prd:SmallBusinessKeyModelElements">   
  
          <!-- Back to Top graphic -->
          <xsl:call-template name="BackToTop" />
  
          <!-- Consumer Credit Info -->
          <xsl:call-template name="SignatoryCreditInfo" />
        </xsl:if>
  

      </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>


</xsl:stylesheet>