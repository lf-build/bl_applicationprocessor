
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  *CI Summary template
  *********************************************
  -->
  <xsl:template name="CISummary">

    <xsl:variable name="scorable">
       <xsl:choose>		              
         <xsl:when test="not (prd:ExecutiveSummary) or not (prd:PaymentTrends) or not (prd:QuarterlyPaymentTrends) ">
            <xsl:value-of select="'false'" />
         </xsl:when>

         <xsl:otherwise>
            <xsl:value-of select="'true'" />
         </xsl:otherwise>
       </xsl:choose>    
    </xsl:variable>

    <!-- Section title -->
    <xsl:call-template name="SectionTitle">
      <xsl:with-param name="title" select="'Executive Summary'" />
    </xsl:call-template>

    <!-- Custom Header message -->
    <xsl:if test="prd:CustomHeader">
      <img src="../images/spacer.gif" border="0" width="1" height="5" alt=""/>
      <xsl:call-template name="CustomHeaderMessage" />
      <br />
    </xsl:if>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    
      <tr>
        <!-- trade payment status column -->
        <td width="49%" valign="top">

	    <!-- CI graph -->
	    <xsl:call-template name="CommercialIntelliscore" />
          <br />

	    <!-- CurrentDBT -->
	    <xsl:if test="$scorable = 'true'">
	       <xsl:call-template name="CurrentDBT" />
	       <br />
	    </xsl:if>

	    <!-- Research Tips -->
	    <xsl:if test="$scorable = 'false'">
	       <xsl:call-template name="ResearchTips" />
	       <br />
	    </xsl:if>

        </td>
    
        <td width="2%">
        </td>
    
        <!-- legal, trade counts etc. column -->
        <td width="49%" valign="top">

          <xsl:call-template name="TradeFilingSummary" />
          <br />

      	   <xsl:if test="prd:ExecutiveSummary and prd:ExecutiveElements">
            <font size="1"><i>
            * Days Beyond Terms (DBT) is a dollar weighted calculation of the average number of 
              days that payment was made beyond the invoice due date based on trades on file that 
              have been updated in the previous 3 months.
            </i></font>

            <xsl:choose>		              
              <xsl:when test="prd:ExecutiveElements/prd:UCCDerogatoryCount and number(prd:ExecutiveElements/prd:UCCDerogatoryCount) &gt; 0">
                <br /><img src="../images/spacer.gif" border="0" width="1" height="5" alt=""/><br />
              </xsl:when>
  
              <xsl:otherwise>
                <br /><br />
              </xsl:otherwise>
            </xsl:choose>   
          </xsl:if>
        
          <xsl:if test="prd:ExecutiveElements/prd:UCCDerogatoryCount and number(prd:ExecutiveElements/prd:UCCDerogatoryCount) &gt; 0">
            <font size="1"><i>
            ** Cautionary UCC Filings include one or more of the following collateral:<br/>
            Accounts, Accounts Receivables, Contract Rights, Hereafter Acquired Property, Inventory, Leases, Notes Receivable or Proceeds. 
            </i></font>
            <br /><br />
          </xsl:if>
          
        </td>
      </tr>
    </table>


    <xsl:if test="$scorable = 'true'">
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    
	      <tr>
	        <!-- trade payment status column -->
	        <td width="49%" valign="top">
	
	          <!-- MonthlyDBT -->
	          <xsl:call-template name="MonthlyDBT" />
	          <br />
	
	          <xsl:if test="prd:ExecutiveSummary ">
	            <!-- PerformanceAnalysis -->
	            <xsl:call-template name="PerformanceAnalysis" />
	          </xsl:if>
	    
	        </td>
	    
	        <td width="2%">
	        </td>
	    
	        <!-- legal, trade counts etc. column -->
	        <td width="49%" valign="top">
	
	          <!-- QuarterlyDBT -->
	          <xsl:call-template name="QuarterlyDBT" />
	          <br />
	          
	        </td>
	      </tr>
	    </table>
    </xsl:if>

  </xsl:template>
    
  
  <!--
  *********************************************
  * ResearchTips template
  *********************************************
  -->
  <xsl:template name="ResearchTips">
    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="#0099cc">
        
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#ffffff">
                <!-- box header -->
                <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                  <tr>
                    <td height="23" bgcolor="#0099cc" align="left" valign="middle">
                      <img src="../images/spacer.gif" border="0" width="5" height="1" alt=""/>
                      <font color="#ffffff" size="2"><b>Research Tips</b></font>
                    </td>
                  </tr>  

                  <tr>
                    <td style="line-height:10px;" valign="bottom">&#160;</td>
                  </tr>  
                </table>

                <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                  <tr>
                    <td width="1%">
                      <img src="../images/spacer.gif" border="0" width="5" height="1" alt=""/></td>
                      
                    <td width="98%">  
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                   
                        <tr>  
                          <td colspan="2">
              
                            Additional information may be available on this business.
                            <br /><br />
                          </td>
                        </tr>
                        
                        <tr>
                          <td style="line-height:5px;" colspan="2">&#160;</td>
                        </tr>
                        
                      </table> 
                    </td>
                    <td width="1%">
                      <img src="../images/spacer.gif" border="0" width="5" height="1" alt=""/></td>
                  </tr>    
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </xsl:template>

</xsl:stylesheet>