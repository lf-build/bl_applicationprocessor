
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:rsp="http://www.experian.com/NetConnectResponse"
  xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * Global variables and parameters
  *********************************************
  <xsl:param name="systemYear" select="'2006'" />
  -->
  <xsl:param name="product" select="'CI'" />
  <xsl:param name="baseProduct" select="''" />


  <!--
  *********************************************
  * include template
  *********************************************
  -->
  <xsl:include href="../common/ReportHeader.xslt" />
  <xsl:include href="../common/Util.xslt" />
  <xsl:include href="../common/case.xslt" />
  <xsl:include href="../common/BackToTop.xslt" />
  <xsl:include href="../common/CompanyInformation.xslt" />
  <xsl:include href="../common/TradeFilingSummary.xslt" />
  <xsl:include href="../common/ErrorProcessing.xslt" />
  <xsl:include href="../common/CorporateLinkage.xslt" />
  <xsl:include href="CustomHeaderMessage.xslt" />
  <xsl:include href="ExecutiveSummary.xslt" />
  <xsl:include href="BusinessSummary.xslt" />
  <xsl:include href="CIScorable.xslt" />
  <xsl:include href="CustomFooterMessage.xslt" />
  <xsl:include href="../common/ReportFooter.xslt" />


  <!--
  *********************************************
  * Initial template
  *********************************************
  -->  
  <xsl:template match="/">
    <html>
      <head>
        <title>CI Report</title>

        <style type="text/css">
            td {font-size: 9pt; font-family: 'arial';}
        </style>
      </head>

      <body>
        <a name="top"></a>
        <table width="715" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td>      
              <!-- Intelliscore template -->
              <xsl:apply-templates select="//prd:Intelliscore" />
            </td>
          </tr>
        </table>    
      </body>
    </html>
  </xsl:template>


  <!--
  *********************************************
  * Intelliscore template
  *********************************************
  -->
  <xsl:template match="prd:Intelliscore" >
    <!-- Report Date  -->
    <xsl:variable name="reportDate">
      <xsl:value-of select="prd:BusinessNameAndAddress/prd:ProfileDate" />
    </xsl:variable>

    <!-- Report Header -->
    <xsl:call-template name="ReportHeader">
      <xsl:with-param name="reportType" select="'Commercial Score'" />
      <xsl:with-param name="reportName" select="prd:BusinessNameAndAddress/prd:BusinessName" />
    </xsl:call-template>

    <br />

    <xsl:variable name="isHTMLModel">
       <xsl:call-template name="isHTMLModel" />
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="normalize-space($isHTMLModel) = 'false' ">
        <xsl:call-template name="NoHTMLReport" />
      </xsl:when>

      <xsl:when test="normalize-space(prd:BusinessNameAndAddress/prd:ProfileType/@code) = 'NO RECORD' ">
        <xsl:call-template name="businessNotFound" />
      </xsl:when>

      <xsl:otherwise>

	    <!-- Identifying Information -->
	    <xsl:call-template name="CompanyInformation" />
	
	    <br />

	    <xsl:if test="prd:CorporateLinkage">
	      <!-- Corporate Linkage -->
	      <xsl:call-template name="CorporateLinkage" />
	    </xsl:if>

	    <!-- Custom Header message -->
	    <xsl:if test="prd:CustomHeader">
	      <xsl:call-template name="CustomHeaderMessage" />
	    </xsl:if>
	
	    <br />

	    <!-- Business Credit Info -->
	    <xsl:call-template name="ExecutiveSummary" />
	    
	    <!-- custom footer message (if in XML) -->
	    <xsl:if test="prd:CustomFooter">
	       <br />
	       <xsl:call-template name="CustomFooterMessage" />
	       <br />
	    </xsl:if>
	
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	      <tr>  
	        <!-- UCC cautionary statement -->
	        <td valign="bottom">
	          <xsl:if test="(prd:ExecutiveElements/prd:UCCDerogatoryCount) > 0">		    		   		   
	            <img src="../images/spacer.gif" border="0" width="1" height="2" alt=""/><br />
	            <font size="1"><i>
	            ** Cautionary UCC Filings include one or more of the following collateral:<br/>
	            Accounts, Accounts Receivables, Contract Rights, Hereafter Acquired Property, Inventory, Leases, Notes Receivable or Proceeds. 
	            </i></font>
	          </xsl:if>
	        </td>
	      </tr>
	    </table>
	
	    <!-- is it a limited report?  -->
	    <xsl:variable name="reportEnd">
	       <xsl:choose>		              
	         <xsl:when test="normalize-space(prd:BillingIndicator/@code) = 'A' ">
	            <xsl:value-of select="'End of limited'" />
	         </xsl:when>
	
	         <xsl:otherwise>
	            <xsl:value-of select="'End of report'" />
	         </xsl:otherwise>
	       </xsl:choose>    
	    </xsl:variable>

	    <!-- Report Footer -->
	    <xsl:call-template name="ReportFooter">
	      <xsl:with-param name="reportType" select="'CI'" />
	      <xsl:with-param name="reportDate" select="$reportDate" />
	      <xsl:with-param name="reportEnd" select="$reportEnd" />
	    </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>


  <!--
  *********************************************
  * this template overrides the built-in rule
  * do nothing with stuff I'm not interested in
  *********************************************
  -->
  <xsl:template match="text()|@*">
  </xsl:template>


</xsl:stylesheet>
