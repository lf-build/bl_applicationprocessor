
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rsp="http://www.experian.com/NetConnectResponse"
                xmlns:prd="http://www.experian.com/ARFResponse">

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />

  
  <!--
  *********************************************
  * CIScorable template
  *********************************************
  -->
  <xsl:template name="CIScorable">
    <xsl:variable name="barWidth">
      <xsl:value-of select="'300'" />
    </xsl:variable>
    
    <xsl:variable name="scoreTableWidth">
      <xsl:value-of select="'300'" />
    </xsl:variable>
  
    <xsl:variable name="pointerTableWidth">
      <xsl:value-of select="'302'" />
    </xsl:variable>
  
    <xsl:variable name="pointerWidth">
      <xsl:value-of select="'3'" />
    </xsl:variable>
    
    <xsl:variable name="action">
      <xsl:choose>
        <xsl:when test="prd:IntelliscoreScoreInformation/prd:Action">
          <xsl:value-of select="prd:IntelliscoreScoreInformation/prd:Action" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="scoreWidth">
      <xsl:value-of select="'36'" />
    </xsl:variable>
  
    <xsl:variable name="model">
      <xsl:value-of select="(prd:IntelliscoreScoreInformation/prd:ModelInformation/prd:ModelCode)" />
    </xsl:variable>

    <xsl:variable name="high">
      <xsl:choose>
        <!-- CI Model -->
        <xsl:when test="$model = 113 or $model = 173">
          <xsl:value-of select="'68.06'" />
        </xsl:when>
        <!-- SBI Model -->
        <xsl:when test="$model = 157 or $model = 174">
          <xsl:value-of select="'40.40'" />
        </xsl:when>
        <!-- SBI3 Model -->
        <xsl:when test="contains($product, 'SBI') ">
          <xsl:value-of select="'52.35'" />
        </xsl:when>
        <!-- CI2 Model (default) -->
        <xsl:otherwise>
          <xsl:value-of select="'32.47'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="medHigh">
      <xsl:choose>
        <!-- CI Model -->
        <xsl:when test="$model = 113 or $model = 173">
          <xsl:value-of select="'79.56'" />
        </xsl:when>
        <!-- SBI Model -->
        <xsl:when test="$model = 157 or $model = 174">
          <xsl:value-of select="'58.04'" />
        </xsl:when>
        <!-- SBI3 Model -->
        <xsl:when test="contains($product, 'SBI') ">
          <xsl:value-of select="'73.11'" />
        </xsl:when>
        <!-- CI2 Model (default) -->
        <xsl:otherwise>
          <xsl:value-of select="'58.63'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="medium">
      <xsl:choose>
        <!-- CI Model -->
        <xsl:when test="$model = 113 or $model = 173">
          <xsl:value-of select="'85.33'" />
        </xsl:when>
        <!-- SBI Model -->
        <xsl:when test="$model = 157 or $model = 174">
          <xsl:value-of select="'66.73'" />
        </xsl:when>
        <!-- SBI3 Model -->
        <xsl:when test="contains($product, 'SBI') ">
          <xsl:value-of select="'85.88'" />
        </xsl:when>
        <!-- CI2 Model (default) -->
        <xsl:otherwise>
          <xsl:value-of select="'70.19'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="medLow">
      <xsl:choose>
        <!-- CI Model -->
        <xsl:when test="$model = 113 or $model = 173">
          <xsl:value-of select="'92.96'" />
        </xsl:when>
        <!-- SBI Model -->
        <xsl:when test="$model = 157 or $model = 174">
          <xsl:value-of select="'74.98'" />
        </xsl:when>
        <!-- SBI3 Model -->
        <xsl:when test="contains($product, 'SBI') ">
          <xsl:value-of select="'89.87'" />
        </xsl:when>
        <!-- CI2 Model (default) -->
        <xsl:otherwise>
          <xsl:value-of select="'77.71'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
  
    <xsl:variable name="low">
      <xsl:value-of select="'100.00'" />
    </xsl:variable>
  
    <xsl:variable name="colorWidth">
      <xsl:value-of select="'60'" />
    </xsl:variable>
  
    <xsl:variable name="score">
      <xsl:value-of select="(prd:IntelliscoreScoreInformation/prd:ScoreInfo/prd:Score div 100)" />
    </xsl:variable>
  
    <xsl:variable name="pointerCenter">
      <xsl:choose>		              
        <xsl:when test="$score &lt;= $high">		    		   		   
          <xsl:value-of select="round((($score - 0) * $colorWidth) div ($high - 0))" />
        </xsl:when>

        <xsl:when test="($score &lt;= $medHigh) and ($score &gt; $high)">		    		   		   
          <xsl:value-of select="round((($score - $high) * $colorWidth) div ($medHigh - $high)) + ($colorWidth * 1)" />
        </xsl:when>

        <xsl:when test="($score &lt;= $medium) and ($score &gt; $medHigh)">		    		   		   
          <xsl:value-of select="round((($score - $medHigh) * $colorWidth) div ($medium - $medHigh)) + ($colorWidth * 2)" />
        </xsl:when>

        <xsl:when test="($score &lt;= $medLow) and ($score &gt; $medium)">		    		   		   
          <xsl:value-of select="round((($score - $medium) * $colorWidth) div ($medLow - $medium)) + ($colorWidth * 3)" />
        </xsl:when>
        
        <xsl:otherwise>
          <xsl:value-of select="round((($score - $medLow) * $colorWidth) div ($low - $medLow)) + ($colorWidth * 4)" />
        </xsl:otherwise>
      </xsl:choose>    

    </xsl:variable>
  
    <xsl:variable name="pointerLeft">
      <xsl:value-of select="$pointerCenter" />
    </xsl:variable>
  
    <xsl:variable name="scoreLeft">
      <xsl:choose>		              
        <xsl:when test="$pointerCenter &lt;= ($scoreWidth div 2)">		    		   		   
          <xsl:value-of select="'0'" />
        </xsl:when>

        <xsl:when test="$pointerCenter &gt;= ($scoreTableWidth - ($scoreWidth div 2))">		    		   		   
          <xsl:value-of select="$scoreTableWidth - $scoreWidth" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="$pointerCenter - ($scoreWidth div 2)" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="scoreAlign">
      <xsl:choose>		              
        <xsl:when test="$pointerCenter &lt;= ($scoreWidth div 2)">		    		   		   
          <xsl:value-of select="'left'" />
        </xsl:when>

        <xsl:when test="$pointerCenter &gt;= ($scoreTableWidth - ($scoreWidth div 2))">		    		   		   
          <xsl:value-of select="'right'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'center'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="factorWidth">
       <xsl:value-of select="'100%'" />
    </xsl:variable>

    <xsl:variable name="factorSpan">
       <xsl:value-of select="'3'" />
    </xsl:variable>


    <!-- score text -->
    <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center">
          <table width="{$scoreTableWidth}" border="0" cellspacing="0" cellpadding="0">
            <tr>
            
              <xsl:if test="$scoreLeft &gt; 0">
                <td width="{$scoreLeft}"></td>
              </xsl:if>
                
              <td width="{$scoreTableWidth - $scoreLeft}" align="left">
                <table width="{$scoreWidth}" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="{$scoreAlign}">                              
                      <font size="3"><b><xsl:value-of select="format-number($score, '###.00')" /></b></font></td>
                  </tr>
                </table>    
              </td>    
            </tr>
          </table>    
        </td>
      </tr>  
    </table>  
  
  
    <!-- Pointer gif -->
    <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center">
          <table width="{$pointerTableWidth}" border="0" cellspacing="0" cellpadding="0">
            <tr>

              <xsl:if test="$pointerLeft &gt; 0">
                <td width="{$pointerLeft}"><img src="../images/spacer.gif" border="0" width="{$pointerLeft}" height="1" alt=""/></td>
              </xsl:if>

              <td width="{$pointerTableWidth - $pointerLeft}" align="left" style="line-height:13px;" valign="middle"><img src="../images/triangle_blue.gif" border="0" width="3" height="14" alt=""/></td>
            </tr>
          </table>    
        </td>
      </tr>  
    </table>  


    <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="3" align="center">
          <table width="{$barWidth}" border="0" cellspacing="0" cellpadding="0">
            
            <!-- color bar-->
            <tr>
              <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="25" width="20%" bgcolor="#ff0000" align="center" valign="middle"><font size="1" color="#000000">0-<xsl:value-of select="format-number($high, '##.00')" /></font></td>
                    <td height="25" width="20%" bgcolor="#ffaa00" align="center" valign="middle"><font size="1" color="#000000"><xsl:value-of select="format-number($high + .01, '##.00')" />-<xsl:value-of select="format-number($medHigh, '##.00')" /></font></td>
                    <td height="25" width="20%" bgcolor="#ffff00" align="center" valign="middle"><font size="1" color="#000000"><xsl:value-of select="format-number($medHigh + .01, '##.00')" />-<xsl:value-of select="format-number($medium, '##.00')" /></font></td>
                    <td height="25" width="20%" bgcolor="#00ee00" align="center" valign="middle"><font size="1" color="#000000"><xsl:value-of select="format-number($medium + .01, '##.00')" />-<xsl:value-of select="format-number($medLow, '##.00')" /></font></td>
                    <td height="25" width="20%" bgcolor="#00aa00" align="center" valign="middle"><font size="1" color="#000000"><xsl:value-of select="format-number($medLow + .01, '##.00')" />-100</font></td>
                  </tr>
                </table>
              </td>
            </tr>
            <!-- end color bar-->

            <!-- color bar labels -->
            <tr>
              <td>
                <table width="{$barWidth}" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="left">
                      <font size="1" style="FONT-FAMILY: 'verdana';">0</font></td>

                    <xsl:if test="$baseProduct = 'SBI' ">
	                    <td valign="top" align="center">
	                      <font size="1" style="FONT-FAMILY: 'verdana';">Based on business and signatory's credit</font>
	                    </td>
                    </xsl:if>

                    <td align="right">
                      <font size="1" style="FONT-FAMILY: 'verdana';">100</font></td>
                  </tr>
                </table>

                <table width="{$barWidth}" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="left">
                      <font size="1" style="FONT-FAMILY: 'verdana';"><b>High risk</b></font></td>
                    
                    <td align="right">
                      <font size="1" style="FONT-FAMILY: 'verdana';"><b>Low risk</b></font></td>
                  </tr>
                </table>
              </td>
            </tr>
            <!-- end color bar labels -->

          </table>    
        </td>
      </tr>  


      <tr>
        <td width="1%">
          <img src="../images/spacer.gif" border="0" width="5" height="1" alt=""/></td>
        <td width="98%">  
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                       
            <tr>  
              <td align="center" colspan="3">

               <xsl:if test="$action">
                  <br /><xsl:value-of select="$action" />
               </xsl:if>

                <img src="../images/spacer.gif" border="0" width="1" height="8" alt=""/><br />
                <b><xsl:value-of select="format-number((prd:IntelliscoreScoreInformation/prd:PercentileRanking div 100), '##0%')" /> 
                of businesses indicate a higher risk of becoming seriously delinquent than this business</b><br />
                <img src="../images/spacer.gif" border="0" width="1" height="10" alt=""/><br /></td>
            </tr>

            <xsl:if test="count(prd:ScoreFactors/prd:ScoreFactor) &gt; 0">
              <tr>  
                <td width="{normalize-space($factorWidth)}" colspan="{normalize-space($factorSpan)}" valign="top">
                  <b>Key score factors:</b>
                  <br />
                  <table width="100%" border="0" cellspacing="0" cellpadding="2">
                    <xsl:apply-templates select="prd:ScoreFactors/prd:ScoreFactor[position() &lt; 5]" />
                  </table>  
                </td>
              </tr>
            </xsl:if>

            <tr>
              <td style="line-height:3px;" colspan="3">&#160;</td>
            </tr>
          </table> 
        </td>
        <td width="1%" align="left">
          <img src="../images/spacer.gif" border="0" width="3" height="1" alt=""/></td>
      </tr>    

    </table>
  </xsl:template>



  <!--
  *********************************************
  * ScoreFactor template
  *********************************************
  -->
  <xsl:template match="prd:ScoreFactor" >

    <!-- ScoreFactor -->
     <xsl:variable name="scoreFactor">
	<xsl:variable name="ScoreFactor">
	  <xsl:value-of select="normalize-space(.)" />
	</xsl:variable>
	
	<xsl:call-template name="convertcase">
	   <xsl:with-param name="toconvert" select="$ScoreFactor" />
	   <xsl:with-param name="conversion" select="'proper'" />
	</xsl:call-template>
    </xsl:variable>

    <tr>
      <td width="3%" valign="top">
        <table width="6" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="10" valign="bottom">
              <img src="../images/ball_bullet_small.gif" border="0" width="6" height="6" alt=""/></td>
          </tr>    
        </table>
      </td>
        
      <td width="97%"><xsl:value-of select="$scoreFactor" /></td>
    </tr>
  </xsl:template>
  
</xsl:stylesheet>