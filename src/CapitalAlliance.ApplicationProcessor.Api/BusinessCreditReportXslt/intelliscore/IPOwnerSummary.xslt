<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">
                

  <!--
  *********************************************
  * Output method
  *********************************************
  -->

  <!--
  *********************************************
  * SmallBusinessKeyModelElements template
  *********************************************
  -->
  <xsl:template match="prd:SmallBusinessKeyModelElements">
    <xsl:variable name="index">
        <xsl:value-of select="position()" />
    </xsl:variable>

    <xsl:variable name="sbiFirstName">
      <xsl:choose>                  
        <xsl:when test="prd:ProprietorFirstName">                    
          <xsl:call-template name="convertcase">
            <xsl:with-param name="toconvert" select="prd:ProprietorFirstName" />
            <xsl:with-param name="conversion" select="'upper'" />
          </xsl:call-template>          
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="sbiLastName">
      <xsl:choose>                  
        <xsl:when test="prd:ProprietorSurname">                    
          <xsl:call-template name="convertcase">
            <xsl:with-param name="toconvert" select="prd:ProprietorSurname" />
            <xsl:with-param name="conversion" select="'upper'" />
          </xsl:call-template>          
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="consumerStatement">
      <xsl:choose>                  
        <xsl:when test="//prd:CreditProfile[position()=number($index)]/prd:Statement/prd:StatementText/prd:MessageText">
          <xsl:value-of select="//prd:CreditProfile[position()=number($index)]/prd:Statement/prd:StatementText/prd:MessageText" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="sbiCount">
        <xsl:value-of select="count(//prd:SmallBusinessKeyModelElements)" />
    </xsl:variable>
    
    <xsl:variable name="hasDispute">
      <xsl:choose>                  
        <xsl:when test="$sbiCount = 1 and //prd:ProprietorNameAndAddress/prd:ProfileType[normalize-space(@code)='DISPUTE']">
          <xsl:value-of select="1" />
        </xsl:when>

        <xsl:when test="$sbiCount &gt; 1 and normalize-space(//prd:ProprietorNameAndAddress[position()=number($index)]/prd:ProfileType/@code)='DISPUTE'">
          <xsl:value-of select="1" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="0" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="vantageScore">
      <xsl:choose>                  
        <xsl:when test="prd:VantageScore and string(number(prd:VantageScore)) != 'NaN' and number(prd:VantageScore) &gt; 0">
          <xsl:value-of select="number(prd:VantageScore)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="unscorable">
      <xsl:choose>                  
        <xsl:when test="prd:VantageScore and string(number(prd:VantageScore)) != 'NaN' and number(prd:VantageScore) &gt;= 9000">
          <xsl:value-of select="1" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="0" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="scoreText">
      <xsl:choose>
        <xsl:when test="$unscorable = 1">
          <xsl:value-of select="'Unscorable'" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$vantageScore" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="sectionTitle">
      <xsl:choose>
        <xsl:when test="$modelCode = $sbcsV2Model">
          <xsl:value-of select="'Owner/Guarantor Summary'" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'Owner/Guarantor Information'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:if test="position() &gt; 1">
      <br/>
    </xsl:if>
    
    <!-- begin owner information -->
    <a name="Owner{position()}"><font><!-- FOR IE7, DO NOT REMOVE THIS EMPTY TAG --></font></a>
    <table class="section" width="100%" cellspacing="0" cellpadding="0">
      <colgroup style="width:50%"/>
      <colgroup style="width:50%" />
      <thead>
        <tr>
          <th colspan="2"><span style="vertical-align:15%;">+</span>&#160;<a class="report_section_title"><xsl:value-of select="normalize-space($sectionTitle)" /></a> <span class="label"> - <xsl:value-of select="prd:ProprietorFirstName" />&#160;<xsl:value-of select="prd:ProprietorSurname" /></span></th>
        </tr>
        <tr class="subtitle">
          <th>Owner Account Information</th>
          <th>Owner Legal Filings and Payment Status</th>
        </tr>  
      </thead>
      <tbody>
        <xsl:if test="$hasDispute=1  and normalize-space($consumerStatement) != ''">
          <tr>
            <td colspan="2" style="padding:5px 7px 3px;">
              <span class="alert"><b>Consumer statement:</b>
              <xsl:value-of select="$consumerStatement" />
              </span>
            </td>
          </tr>  
        </xsl:if>

        <tr>
          <td class="box_padding">
            <xsl:call-template name="OwnerAccountInformation" />
          </td>
          <td class="box_padding">
            <xsl:call-template name="OwnerLegalFilings" />
          </td>
        </tr>
      </tbody>
    </table>
	
	<xsl:if test="$product != 'PPRIPBOP'">
		<xsl:if test="(normalize-space($vantageScore) != 'N/A' or prd:ConsumerAdverseAction1 or prd:ConsumerAdverseAction2 or prd:ConsumerAdverseAction3 or prd:ConsumerAdverseAction4 or prd:ConsumerAdverseAction5) and $modelCode != $sbcsV2Model">

		<br/>
		
		<!-- begin vantage score -->
		<table class="section" width="100%" cellspacing="0" cellpadding="0">
		  <colgroup style="width:50%"/>
		  <colgroup style="width:50%" />
		  <thead>
			<tr>
			  <th colspan="2"><a class="report_section_title">Vantage Score</a> <span class="label"> - <xsl:value-of select="prd:ProprietorFirstName" /> <xsl:value-of select="prd:ProprietorSurname" /></span></th>
			</tr>
		  </thead>
		  <tbody>
			<tr>
			  <td class="box_padding font_size_3" style="vertical-align:middle;text-align:center;">
				<span class="label">Vantage Score:</span>&#160;<xsl:value-of select="$scoreText" />
			  </td>
			  <td class="box_padding">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <xsl:choose>
					<xsl:when test="$unscorable = 1">
					  <tr>
						<td>
						  <div class="label">Unscorable due to the following reason:</div>
						  <ul class="list">
							<li>
							  <xsl:call-template name="translateUnscorableScorexPlus">
								<xsl:with-param name="code" select="number($vantageScore)" />
							  </xsl:call-template>
							</li>
						  </ul>                      
						</td>
					  </tr>
					</xsl:when>

					<xsl:otherwise>
					  <tr>
						<td>
						  <div class="label">Negative Score Factors</div>

						  <ul class="list">
								  <xsl:apply-templates select="prd:ConsumerAdverseAction1" />
								  <xsl:apply-templates select="prd:ConsumerAdverseAction2" />
								  <xsl:apply-templates select="prd:ConsumerAdverseAction3" />
								  <xsl:apply-templates select="prd:ConsumerAdverseAction4" />
								  <xsl:apply-templates select="prd:ConsumerAdverseAction5" />
						  </ul>                      
							</td>
						  </tr>

					</xsl:otherwise>
				  </xsl:choose>
				</table>
			  </td>
			</tr>
		  </tbody>
		</table>
		<!-- end vantage score -->
		</xsl:if>
	</xsl:if>
  </xsl:template>


  <!--
  *********************************************
  * ConsumerAdverseAction template
  *********************************************
  -->
  <xsl:template match="prd:ConsumerAdverseAction1 | prd:ConsumerAdverseAction2 | prd:ConsumerAdverseAction3 | prd:ConsumerAdverseAction4 | prd:ConsumerAdverseAction5">
    <!-- ScoreFactor -->
    <xsl:variable name="advAction">
      <xsl:variable name="description">
        <xsl:value-of select="normalize-space(prd:Description)" />
      </xsl:variable>
        
      <xsl:call-template name="convertcase">
        <xsl:with-param name="toconvert" select="$description" />
        <xsl:with-param name="conversion" select="'proper'" />
      </xsl:call-template>
    </xsl:variable>
    
      <li><xsl:value-of select="normalize-space($advAction)" /></li>
  </xsl:template>


  <!--
  *********************************************
  * OwnerAccountInformation template
  *********************************************
  -->
  <xsl:template name="OwnerAccountInformation">

    <xsl:variable name="bankCard6Months">
      <xsl:choose>                  
        <xsl:when test="prd:TotOpenBankcardTradesLast6Mos and string(number(prd:TotOpenBankcardTradesLast6Mos)) != 'NaN'">                    
          <xsl:value-of select="number(prd:TotOpenBankcardTradesLast6Mos)" />
        </xsl:when>

        <xsl:when test="prd:TotNumOpenRevolvingBankcardTradesReportedLast6Months and string(number(prd:TotNumOpenRevolvingBankcardTradesReportedLast6Months)) != 'NaN'">                    
          <xsl:value-of select="number(prd:TotNumOpenRevolvingBankcardTradesReportedLast6Months)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
  
    <xsl:variable name="bankCardBalanceLimitRatio6Months">
      <xsl:choose>                  
        <xsl:when test="prd:TotBankcardBalToLimitRatioLast6Mos and string(number(prd:TotBankcardBalToLimitRatioLast6Mos)) != 'NaN'">                    
          <xsl:value-of select="format-number(prd:TotBankcardBalToLimitRatioLast6Mos div 10000, '0.##%')" />
        </xsl:when>

        <xsl:when test="prd:TotBalToCreditAmtRatioOpenRevolvingBankcardTradesLast6Months and string(number(prd:TotBalToCreditAmtRatioOpenRevolvingBankcardTradesLast6Months)) != 'NaN'">                    
          <xsl:value-of select="format-number(prd:TotBalToCreditAmtRatioOpenRevolvingBankcardTradesLast6Months div 10000, '0.##%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0%'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
  
    <xsl:variable name="openRevolvingTrades">
      <xsl:choose>                  
        <xsl:when test="prd:TotOpenRevTrades and string(number(prd:TotOpenRevTrades)) != 'NaN'">                    
          <xsl:value-of select="number(prd:TotOpenRevTrades)" />
        </xsl:when>

        <xsl:when test="prd:TotNumOpenRevolvingTradesReportedLast6Months and string(number(prd:TotNumOpenRevolvingTradesReportedLast6Months)) != 'NaN'">                    
          <xsl:value-of select="number(prd:TotNumOpenRevolvingTradesReportedLast6Months)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="revolvingCredit6Months">
      <xsl:choose>                  
        <xsl:when test="prd:TotAvailableRevCreditLast6Mos">                    
          <xsl:value-of select="format-number(prd:TotAvailableRevCreditLast6Mos, '$###,###,##0')" />
        </xsl:when>

        <xsl:when test="prd:TotAvailableCreditAmtOpenRevolvingTradesReportedLast6Months">                    
          <xsl:value-of select="format-number(prd:TotAvailableCreditAmtOpenRevolvingTradesReportedLast6Months, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="percentAutoLease30PlusDays">
      <xsl:choose>                  
        <xsl:when test="prd:PctAutoLeaseTrades30PlusDPD and string(number(prd:PctAutoLeaseTrades30PlusDPD)) != 'NaN'">                    
          <xsl:value-of select="format-number(prd:PctAutoLeaseTrades30PlusDPD div 10000, '0.##%')" />
        </xsl:when>

        <xsl:when test="prd:PctAutoLoanLeaseTrades30PlusDaysDelinquentDerog and string(number(prd:PctAutoLoanLeaseTrades30PlusDaysDelinquentDerog)) != 'NaN'">                    
	      <xsl:choose>                  
	        <xsl:when test="number(prd:PctAutoLoanLeaseTrades30PlusDaysDelinquentDerog) = 999 or number(prd:PctAutoLoanLeaseTrades30PlusDaysDelinquentDerog) = 998">                    
	          <xsl:value-of select="'N/A'" />
	        </xsl:when>
	
	        <xsl:otherwise>
	          <xsl:value-of select="format-number(prd:PctAutoLoanLeaseTrades30PlusDaysDelinquentDerog div 10000, '0.##%')" />
	        </xsl:otherwise>
	      </xsl:choose>    
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0%'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
  
    <xsl:variable name="percentrealEstate30PlusDays">
      <xsl:choose>                  
        <xsl:when test="prd:PctRealEstateTrades30PlusDPD and string(number(prd:PctRealEstateTrades30PlusDPD)) != 'NaN'">                    
          <xsl:value-of select="format-number(prd:PctRealEstateTrades30PlusDPD div 10000, '0.##%')" />
        </xsl:when>

        <xsl:when test="prd:PctMortgageTypeTrades30PlusDaysDelinquentDerog and string(number(prd:PctMortgageTypeTrades30PlusDaysDelinquentDerog)) != 'NaN'">                    
          <xsl:choose>                  
            <xsl:when test="number(prd:PctMortgageTypeTrades30PlusDaysDelinquentDerog) = 999 or number(prd:PctMortgageTypeTrades30PlusDaysDelinquentDerog) = 998">                    
              <xsl:value-of select="'N/A'" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="format-number(prd:PctMortgageTypeTrades30PlusDaysDelinquentDerog div 10000, '0.##%')" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0%'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="creditInquiries6Months">
      <xsl:choose>                  
        <xsl:when test="prd:TotalCreditPurposeInquiriesLast6Mos and string(number(prd:TotalCreditPurposeInquiriesLast6Mos)) != 'NaN'">                    
          <xsl:value-of select="number(prd:TotalCreditPurposeInquiriesLast6Mos)" />
        </xsl:when>

        <xsl:when test="prd:TotNumCreditInquiriesLast6Months and string(number(prd:TotNumCreditInquiriesLast6Months)) != 'NaN'">                    
          <xsl:value-of select="number(prd:TotNumCreditInquiriesLast6Months)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td>
          Open bank card trades reported previous 6 months:</td>
        <td align="right" nowrap="nowrap"><b><xsl:value-of select="$bankCard6Months" /></b></td>
      </tr>
      
      <tr>
        <td>
          Bank card balance to limit ratio previous 6 months:</td>
        <td align="right" nowrap="nowrap"><b><xsl:value-of select="$bankCardBalanceLimitRatio6Months" /></b></td>
      </tr>
      
      <tr>
        <td>
          Open revolving trades:</td>
        <td align="right" nowrap="nowrap"><b><xsl:value-of select="$openRevolvingTrades" /></b></td>
      </tr>
      
      <tr>  
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="90%">
                Available revolving credit reported previous 6 months:</td>
              <td width="10%" align="right" valign="top" nowrap="nowrap"><b><xsl:value-of select="$revolvingCredit6Months" /></b></td>
            </tr>
          </table>
        </td>  
      </tr>
      
      <tr>
        <td>
          Percentage of auto lease trades 30+ days delinquent:</td>
        <td align="right" nowrap="nowrap"><b><xsl:value-of select="$percentAutoLease30PlusDays" /></b></td>
      </tr>
      
      <tr>
        <td>
          Percentage of real estate trades 30+ days delinquent:</td>
        <td align="right" nowrap="nowrap"><b><xsl:value-of select="$percentrealEstate30PlusDays" /></b></td>
      </tr>
      
      <tr>
        <td>
          Credit inquiries previous 6 months:</td>
        <td align="right" nowrap="nowrap"><b><xsl:value-of select="$creditInquiries6Months" /></b></td>
      </tr>
      
    </table>  
  
  </xsl:template>
  
  
  <!--
  *********************************************
  * OwnerLegalFilings template
  *********************************************
  -->
  <xsl:template name="OwnerLegalFilings">

    <xsl:variable name="ageOldestTrade">
      <xsl:variable name="tmpAgeOldestTrade">
          <xsl:choose>                  
            <xsl:when test="prd:AgeOldestTradeline and string(number(prd:AgeOldestTradeline)) != 'NaN'">                    
              <xsl:value-of select="prd:AgeOldestTradeline" />
            </xsl:when>
    
            <xsl:when test="prd:NumMonthsOldestTradeOpenedExcludingExternalCollections and string(number(prd:NumMonthsOldestTradeOpenedExcludingExternalCollections)) != 'NaN'">                    
              <xsl:value-of select="prd:NumMonthsOldestTradeOpenedExcludingExternalCollections" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="''" />
            </xsl:otherwise>
          </xsl:choose>    
      </xsl:variable>                

      <xsl:choose>                  
        <xsl:when test="normalize-space($tmpAgeOldestTrade) != ''">    
          <xsl:variable name="months">
            <xsl:value-of select="$tmpAgeOldestTrade mod 12" />
          </xsl:variable>                
          
          <xsl:variable name="years">
            <xsl:value-of select="floor($tmpAgeOldestTrade div 12)" />
          </xsl:variable>                
          
          <xsl:choose>                  
            <xsl:when test="$years &gt; 0">    
              <xsl:value-of select="concat($years, ' YRS ', $months, ' MO')" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="concat($months, ' MO')" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0 MO'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="satisfactoryTrades24Months">
      <xsl:choose>                  
        <xsl:when test="prd:SatisfactoryTradesLast24Mos and string(number(prd:SatisfactoryTradesLast24Mos)) != 'NaN'">                    
          <xsl:value-of select="number(prd:SatisfactoryTradesLast24Mos)" />
        </xsl:when>

        <xsl:when test="prd:TotNumTradesNeverDelinquentDerogLast24Months and string(number(prd:TotNumTradesNeverDelinquentDerogLast24Months)) != 'NaN'">                    
          <xsl:value-of select="number(prd:TotNumTradesNeverDelinquentDerogLast24Months)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="presentlyDelinquent6Months">
      <xsl:choose>                  
        <xsl:when test="prd:TradesPresentlyDelinquentOrPastDueLast6Mos and string(number(prd:TradesPresentlyDelinquentOrPastDueLast6Mos)) != 'NaN'">                    
          <xsl:value-of select="number(prd:TradesPresentlyDelinquentOrPastDueLast6Mos)" />
        </xsl:when>

        <xsl:when test="prd:TotNumTrades30PlusDelinquentDerogLast6Months and string(number(prd:TotNumTrades30PlusDelinquentDerogLast6Months)) != 'NaN'">                    
          <xsl:value-of select="number(prd:TotNumTrades30PlusDelinquentDerogLast6Months)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="collectionLegalAccounts">
      <xsl:choose>                  
        <xsl:when test="prd:TotCollectionsLegalFilingsOrDerogAccts and string(number(prd:TotCollectionsLegalFilingsOrDerogAccts)) != 'NaN'">                    
          <xsl:value-of select="number(prd:TotCollectionsLegalFilingsOrDerogAccts)" />
        </xsl:when>

        <xsl:when test="prd:TotNumTradesEverDerogIncludingCollectionsPublicRecords and string(number(prd:TotNumTradesEverDerogIncludingCollectionsPublicRecords)) != 'NaN'">                    
          <xsl:value-of select="number(prd:TotNumTradesEverDerogIncludingCollectionsPublicRecords)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="derogPublicRecords24Months">
      <xsl:choose>                  
        <xsl:when test="prd:TotDerogPubRecsLast24Mos and string(number(prd:TotDerogPubRecsLast24Mos)) != 'NaN'">                    
          <xsl:value-of select="number(prd:TotDerogPubRecsLast24Mos)" />
        </xsl:when>

        <xsl:when test="prd:TotTrades90PlusDelinqDerogLast24MonWithExternalCollections and string(number(prd:TotTrades90PlusDelinqDerogLast24MonWithExternalCollections)) != 'NaN'">                    
          <xsl:value-of select="number(prd:TotTrades90PlusDelinqDerogLast24MonWithExternalCollections)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="derogPublicRecords250Dollars">
      <xsl:choose>                  
        <xsl:when test="prd:TotDerogPubRecsGT250 and string(number(prd:TotDerogPubRecsGT250)) != 'NaN'">                    
          <xsl:value-of select="number(prd:TotDerogPubRecsGT250)" />
        </xsl:when>

        <xsl:when test="prd:TotNumExternalCollectionsBalanceGreater250 and string(number(prd:TotNumExternalCollectionsBalanceGreater250)) != 'NaN'">                    
          <xsl:value-of select="number(prd:TotNumExternalCollectionsBalanceGreater250)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <table width="100%" border="0" cellspacing="0" cellpadding="2">
     
      <tr>  
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="75%">
                Age of oldest trade:</td>
              <td width="25%" align="right" nowrap="nowrap"><b><xsl:value-of select="$ageOldestTrade" /></b></td>
            </tr>
          </table>
        </td>  
      </tr>
      
      <tr>
        <td>
          Satisfactory trades previous 24 months:</td>
        <td align="right" nowrap="nowrap"><b><xsl:value-of select="$satisfactoryTrades24Months" /></b></td>
      </tr>
      
      <tr>
        <td>
          Trades presently delinquent previous 6 months:</td>
        <td align="right" valign="top" nowrap="nowrap"><b><xsl:value-of select="$presentlyDelinquent6Months" /></b></td>
      </tr>
      
      <tr>
        <td>
          Collections, legal filings, or derogatory accounts:</td>
        <td align="right" nowrap="nowrap"><b><xsl:value-of select="$collectionLegalAccounts" /></b></td>
      </tr>
      
      <tr>
        <td>
          <xsl:choose>
            <xsl:when test="$modelCode = $sbcsV2Model">
              Trades 90+ or derogatory previosue 24 months, including external collections:
            </xsl:when>
            <xsl:otherwise>
              Derogatory public records filed within 24 months:
            </xsl:otherwise>
          </xsl:choose>        
        </td>
        <td align="right" nowrap="nowrap"><b><xsl:value-of select="$derogPublicRecords24Months" /></b></td>
      </tr>
      
      <tr>
        <td>
          <xsl:choose>
            <xsl:when test="$modelCode = $sbcsV2Model">
              Trades placed for collection greater than $250:
            </xsl:when>
            <xsl:otherwise>
              Derogatory public record greater than $250:
            </xsl:otherwise>
          </xsl:choose>
        </td>
        <td align="right" nowrap="nowrap"><b><xsl:value-of select="$derogPublicRecords250Dollars" /></b></td>
      </tr>
      
    </table>  

  </xsl:template>  
</xsl:stylesheet>
