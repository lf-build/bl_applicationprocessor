
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rsp="http://www.experian.com/NetConnectResponse"
                xmlns:prd="http://www.experian.com/ARFResponse">

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />



  <!--
  *********************************************
  * ExecutiveElements template
  *********************************************
  -->
  <xsl:template name="ExecutiveSummary">
   
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <!-- score box column -->
        <td width="49%" valign="top">
          <xsl:call-template name="CommercialIntelliscore" />
        </td>

      </tr>
    </table>

  </xsl:template>
  
    
  <!--
  *********************************************
  * CommercialIntelliscore template
  *********************************************
  -->
  <xsl:template name="CommercialIntelliscore">

    <xsl:variable name="scorable">
       <xsl:choose>		              
         <xsl:when test="not (prd:IntelliscoreScoreInformation) or prd:IntelliscoreScoreInformation/prd:LimitedProfile/@code = 'Y' ">		    		   		   
            <xsl:value-of select="'false'" />
         </xsl:when>

         <xsl:otherwise>
            <xsl:value-of select="'true'" />
         </xsl:otherwise>
       </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="boxTitle">
       <xsl:choose>		              
         <xsl:when test="$scorable = 'true'">		    		   		   
            <xsl:value-of select="'Intelliscore for '" /><xsl:value-of select="prd:BusinessNameAndAddress/prd:BusinessName" />
         </xsl:when>

         <xsl:otherwise>
            <xsl:value-of select="'Alert Messages'" />
         </xsl:otherwise>
       </xsl:choose>    
    </xsl:variable>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <!-- graphy -->
        <td width="49%" valign="top">
	    <table width="100%" border="0" cellspacing="0" cellpadding="1">
	      <tr>
	        <td bgcolor="#0099cc">
	        
	          <table width="100%" border="0" cellspacing="0" cellpadding="0">
	            <tr>
	              <td bgcolor="#ffffff">
	                <!-- box header -->
	                <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
	                        
	                  <tr>
	                    <td height="23" bgcolor="#0099cc" align="left" valign="middle">
	                      <img src="../images/spacer.gif" border="0" width="5" height="1" alt=""/>
	                      <font color="#ffffff"><b><xsl:value-of select="$boxTitle" disable-output-escaping="yes" /></b></font>
	                    </td>
	                  </tr>  
	
	                  <tr>
	                    <td style="line-height:10px;" valign="bottom">&#160;</td>
	                  </tr>  
	                </table>

	                <xsl:choose>		              
	                  <xsl:when test="$scorable = 'true'">
	                    <xsl:call-template name="CIScorable" />
	                  </xsl:when>
	
	                  <xsl:otherwise>
	                    <xsl:call-template name="Unscorable" />
	                  </xsl:otherwise>
	                </xsl:choose>    

	              </td>
	            </tr>
	          </table>
	        </td>
	      </tr>
	    </table>

	    <!-- custom footer message (if in XML) -->
	    <xsl:if test="not(prd:CustomFooter) and $product = 'CI'">
	        <br />
	       <xsl:call-template name="CustomFooterMessage" />
	    </xsl:if>

        </td>

        <xsl:if test="$product = 'CI'">

	        <td width="2%">
	        </td>
	    
	        <!-- legal, trade counts etc. column -->
	        <td width="49%" valign="top">
	
                 <xsl:choose>		              
                   <xsl:when test="$scorable = 'true' or not(prd:BusinessSummary)">
                     <xsl:call-template name="TradeFilingSummary" />
                   </xsl:when>

                   <xsl:otherwise>
                     <xsl:call-template name="BusinessSummary" />
                   </xsl:otherwise>
                 </xsl:choose>    

	          <br />
	          
	        </td>
        </xsl:if>
      </tr>
    </table>

  </xsl:template>
  
  
  <!--
  *********************************************
  * Unscorable template
  *********************************************
  -->
  <xsl:template name="Unscorable">
    <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
            
      <tr>
        <td width="1%">
          <img src="../images/spacer.gif" border="0" width="5" height="1" alt=""/></td>
          
        <td width="98%">  
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                       
            <tr>  
              <td colspan="2">
                <font color="#0099cc"><b>Unscorable Report</b></font><br />                                    
                 This report does not include data elements that have been statistically 
                 proven to predict serious future delinquency. Therefore a Commercial 
                 Intelliscore cannot be calculated.<br /><img src="../images/global/spacer.gif" border="0" width="1" height="10" alt="" /><br />

                Additional information may be available on this business.
              </td>
            </tr>
            
            <tr>
              <td style="line-height:10px;" colspan="2">&#160;</td>
            </tr>
            
          </table> 
        </td>
        <td width="1%">
          <img src="../images/spacer.gif" border="0" width="5" height="1" alt=""/></td>
      </tr>    
    </table>

  </xsl:template>
    
</xsl:stylesheet>