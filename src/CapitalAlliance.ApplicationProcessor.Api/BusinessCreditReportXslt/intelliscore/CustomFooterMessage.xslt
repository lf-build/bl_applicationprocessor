
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">
                

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * CustomFooterMessage template
  *********************************************
  -->
  <xsl:template name="CustomFooterMessage">
  
    <!-- custom-header -->
    <xsl:variable name="custom-footer">
      <xsl:call-template name="get-custom-footer">
      </xsl:call-template>
    </xsl:variable>

    <xsl:choose>		              
	<xsl:when test="prd:CustomFooter">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    <tr>
	      <td >
	        <xsl:value-of select="$custom-footer" disable-output-escaping="yes" />
	      </td>
	    </tr>
	 </table>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="defaultFooter" />
      </xsl:otherwise>
    </xsl:choose>    

</xsl:template>
  
  <!--
  *********************************************
  * get-custom-footertemplate
  * ouput text from custom footer
  *********************************************
  -->
  <xsl:template name="get-custom-footer">
    <xsl:for-each select="prd:CustomFooter">

	<!-- custom footer -->
<!--
	<xsl:variable name="customFooter">
         <xsl:variable name="CustomFooter">
           <xsl:value-of select="." />
        </xsl:variable>

        <xsl:call-template name="convertcase">
          <xsl:with-param name="toconvert" select="$CustomFooter" />
          <xsl:with-param name="conversion" select="'proper'" />
        </xsl:call-template>
	</xsl:variable>
-->

      <xsl:if test="normalize-space(.) != ''">
         <xsl:value-of select="." disable-output-escaping="yes" /><xsl:text>&lt;br&gt;&lt;br&gt;</xsl:text>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

  <!--
  *********************************************
  * default footer template
  *********************************************
  -->
  <xsl:template name="defaultFooter">

       <!-- begin Intelliscore Footer -->
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
             

         <tr>  
           <td>
             <table width="100%" border="0" cellspacing="0" cellpadding="0">
       
               <tr>
                 <td height="19" width="1%" valign="bottom" nowrap="nowrap">CREDIT LIMIT:</td>
                 <td height="19" width="99%" valign="middle"><img src="../images/global/spacer.gif" border="0" width="1" height="19" alt="" /></td>
               </tr>  
               <tr>
                 <td height="1"><img src="../images/global/spacer.gif" border="0" width="1" height="1" alt="" /></td>
                 <td height="1" bgcolor="#486c92"><img src="../images/global/spacer.gif" border="0" width="1" height="1" alt="" /></td>
               </tr>  
     
     
             </table>
           </td>  
         </tr>
       
         <tr> 
           <td><img src="../images/global/spacer.gif" border="0" width="1" height="4" alt="" /></td>
         </tr>

         <tr>  
           <td>
             <table width="100%" border="0" cellspacing="0" cellpadding="0">
       
               <tr>
                 <td height="19" width="1%" valign="bottom"  nowrap="nowrap">TERMS:</td>
                 <td height="19" width="99%" valign="middle"><img src="../images/global/spacer.gif" border="0" width="1" height="19" alt="" /></td>
               </tr>  
               <tr>
                 <td height="1"><img src="../images/global/spacer.gif" border="0" width="1" height="1" alt="" /></td>
                 <td height="1" bgcolor="#486c92"><img src="../images/global/spacer.gif" border="0" width="1" height="1" alt="" /></td>
               </tr>  
     
     
             </table>
           </td>  
         </tr>
       
         <tr> 
           <td><img src="../images/global/spacer.gif" border="0" width="1" height="4" alt="" /></td>
         </tr>

         <tr>  
           <td>
             <table width="100%" border="0" cellspacing="0" cellpadding="0">
       
               <tr>
                 <td height="19" width="1%" valign="bottom"  nowrap="nowrap">COMMENTS:</td>
                 <td height="19" width="99%" valign="middle"><img src="../images/global/spacer.gif" border="0" width="1" height="19" alt="" /></td>
               </tr>  
               <tr>
                 <td height="1"><img src="../images/global/spacer.gif" border="0" width="1" height="1" alt="" /></td>
                 <td height="1" bgcolor="#486c92"><img src="../images/global/spacer.gif" border="0" width="1" height="1" alt="" /></td>
               </tr>  
     
     
             </table>
           </td>  
         </tr>

         <tr>  
           <td>
             <table width="100%" border="0" cellspacing="0" cellpadding="0">

               <tr>
                 <td style="line-height:19px;" width="100%" valign="middle">&#160;</td>
               </tr>  
               <tr>
                 <td style="line-height:1px;" bgcolor="#486c92">&#160;</td>
               </tr>  
     
     
             </table>
           </td>  
         </tr>
       
         <tr> 
           <td><img src="../images/global/spacer.gif" border="0" width="1" height="4" alt="" /></td>
         </tr>

         <tr>  
           <td>
             <table width="100%" border="0" cellspacing="0" cellpadding="0">
       
               <tr>
                 <td height="19" width="1%" valign="bottom"  nowrap="nowrap">SIGNATURE:</td>
                 <td height="19" width="99%" valign="middle"><img src="../images/global/spacer.gif" border="0" width="1" height="19" alt="" /></td>
               </tr>  
               <tr>
                 <td height="1"><img src="../images/global/spacer.gif" border="0" width="1" height="1" alt="" /></td>
                 <td height="1" bgcolor="#486c92"><img src="../images/global/spacer.gif" border="0" width="1" height="1" alt="" /></td>
               </tr>  
     
     
             </table>
           </td>  
         </tr>
        
       </table>
       <!-- end Intelliscore Footer -->

  </xsl:template>

</xsl:stylesheet>