
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rsp="http://www.experian.com/NetConnectResponse"
                xmlns:prd="http://www.experian.com/ARFResponse">

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * BusinessCreditInfo template
  *********************************************
  -->
  <xsl:template name="BusinessCreditInfo">
   
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <!-- score box column -->
        <td width="49%" valign="top">
          <xsl:call-template name="SmallBusinessIntelliscore" />
        </td>

      </tr>
    </table>

  </xsl:template>


  <!--
  *********************************************
  * SmallBusinessIntelliscore template
  *********************************************
  -->
  <xsl:template name="SmallBusinessIntelliscore">

    <xsl:variable name="scorable">
       <xsl:choose>		              
         <xsl:when test="not (prd:IntelliscoreScoreInformation) or prd:IntelliscoreScoreInformation/prd:LimitedProfile/@code = 'Y' or number(prd:IntelliscoreScoreInformation/prd:ScoreInfo/prd:Score) = 99900 or number(prd:IntelliscoreScoreInformation/prd:ScoreInfo/prd:Score) = 99800 ">		    		   		   
            <xsl:value-of select="'false'" />
         </xsl:when>

         <xsl:otherwise>
            <xsl:value-of select="'true'" />
         </xsl:otherwise>
       </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="boxTitle">
      <xsl:choose>		              
        <xsl:when test="$scorable = 'false'">
          <xsl:value-of select="'Alert Messages'" />
        </xsl:when>

        <xsl:when test="$baseProduct = 'SBI'">
          Small Business Intelliscore<sup>SM</sup>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="concat('Intelliscore for ', normalize-space(prd:BusinessNameAndAddress/prd:BusinessName))" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <!-- graphy -->
        <td width="49%" valign="top">
	    <table width="100%" border="0" cellspacing="0" cellpadding="1">
	      <tr>
	        <td bgcolor="{$borderColor}">
	        
	          <table width="100%" border="0" cellspacing="0" cellpadding="0">
	            <tr>
	              <td bgcolor="#ffffff">
	                <!-- box header -->
	                <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
	                        
	                  <tr>
	                    <td height="23" bgcolor="{$borderColor}" align="left" valign="middle">
	                      <img src="../images/spacer.gif" border="0" width="5" height="1" alt=""/>
	                      <font color="#ffffff"><b><xsl:copy-of select="$boxTitle"/></b></font>
	                    </td>
	                  </tr>  
	
	                  <tr>
	                    <td style="line-height:10px;" valign="bottom">&#160;</td>
	                  </tr>  
	                </table>

	                <xsl:choose>		              
	                  <xsl:when test="$scorable = 'true'">
	                    <xsl:call-template name="CIScorable" />
	                  </xsl:when>
	
	                  <xsl:otherwise>
	                    <xsl:call-template name="Unscorable" />
	                  </xsl:otherwise>
	                </xsl:choose>    

	              </td>
	            </tr>
	          </table>
	        </td>
	      </tr>

	      <tr>  
	        <!-- UCC cautionary statement -->
	        <td valign="bottom">
	          <xsl:if test="prd:ExecutiveElements/prd:UCCDerogatoryCount and number(prd:ExecutiveElements/prd:UCCDerogatoryCount) &gt; 0">
	            <font size="1" style="padding-top:3px;"><i>
	            ** Cautionary UCC Filings include one or more of the following collateral:<br />
	            Accounts, Accounts Receivables, Contract Rights, Hereafter Acquired Property, 
	            Inventory, Leases, Notes Receivable or Proceeds. 
	            </i></font>
	          </xsl:if>
	        </td>
	      </tr>
	    </table>
        </td>

        <td width="2%">
        </td>
    
        <!-- legal, trade counts etc. column -->
        <td width="49%" valign="top">
            <xsl:choose>		              
              <xsl:when test="$scorable = 'true' or not(prd:BusinessSummary)">
                <xsl:call-template name="TradeFilingSummary" />
              </xsl:when>

              <xsl:otherwise>
                <xsl:call-template name="BusinessSummary" />
              </xsl:otherwise>
            </xsl:choose>
        </td>
      </tr>
    </table>

  </xsl:template>


  <!--
  *********************************************
  * Unscorable template
  *********************************************
  -->
  <xsl:template name="Unscorable">
    <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
            
      <tr>
        <td width="1%">
          <img src="../images/spacer.gif" border="0" width="5" height="1" alt="" /></td>
          
        <td width="98%">  
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                       
            <tr>  
              <td colspan="2">
                <font color="{$borderColor}"><b>Unscorable Report</b></font><br />                                    
                We are unable to produce a Small Business Intelliscore for the business 
                and owner you requested.  This is due to limited information on the 
                business, limited information on the owner or presence of items on the 
                owner's credit report that prohibit us from returning a score.<br /><img src="../images/global/spacer.gif" border="0" width="1" height="10" alt="" /><br />

                Additional information may be available on this business.
              </td>
            </tr>

<!--
            <tr>
              <td align="right"><img src="../images/ball_bullet_small.gif" border="0" width="6" height="6" alt="" /></td>
              <td>
                <xsl:text disable-output-escaping="yes">&#160;</xsl:text>Order Public Record<br />
              </td>
            </tr>
-->

            <tr>
              <td style="line-height:5px;" colspan="2">&#160;</td>
            </tr>
            
          </table> 
        </td>
        <td width="1%">
          <img src="../images/spacer.gif" border="0" width="5" height="1" alt="" /></td>
      </tr>    
    </table>

  </xsl:template>

</xsl:stylesheet>