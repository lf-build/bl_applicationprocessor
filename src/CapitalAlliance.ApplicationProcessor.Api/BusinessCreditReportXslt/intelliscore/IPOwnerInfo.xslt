
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">
                

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * SmallBusinessKeyModelElements template
  *********************************************
  -->
  <xsl:template match="prd:SmallBusinessKeyModelElements" >

    <xsl:variable name="sbiFirstName">
      <xsl:choose>                  
        <xsl:when test="prd:ProprietorFirstName">                    
          <xsl:call-template name="convertcase">
            <xsl:with-param name="toconvert" select="prd:ProprietorFirstName" />
            <xsl:with-param name="conversion" select="'upper'" />
          </xsl:call-template>          
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="sbiLastName">
      <xsl:choose>                  
        <xsl:when test="prd:ProprietorSurname">                    
          <xsl:call-template name="convertcase">
            <xsl:with-param name="toconvert" select="prd:ProprietorSurname" />
            <xsl:with-param name="conversion" select="'upper'" />
          </xsl:call-template>          
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="consumerStatement">
      <xsl:choose>                  
        <xsl:when test="normalize-space($sbiFirstName) != '' and normalize-space($sbiLastName) != '' and //prd:CreditProfile/prd:ConsumerIdentity/prd:Name/prd:Gen[contains(., normalize-space($sbiFirstName)) and contains(., normalize-space($sbiLastName))]">
          <xsl:value-of select="//prd:CreditProfile[contains(prd:ConsumerIdentity/prd:Name/prd:Gen, normalize-space($sbiFirstName)) and contains(prd:ConsumerIdentity/prd:Name/prd:Gen, normalize-space($sbiLastName))]/prd:Statement/prd:StatementText/prd:MessageText" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="hasDispute">
      <xsl:choose>                  
        <xsl:when test="normalize-space($sbiFirstName) != '' and normalize-space($sbiLastName) != '' and //prd:ProprietorNameAndAddress/prd:ProprietorName[contains(., normalize-space($sbiFirstName)) and contains(., normalize-space($sbiLastName))]">
          <xsl:value-of select="//prd:ProprietorNameAndAddress[contains(prd:ProprietorName, normalize-space($sbiFirstName)) and contains(prd:ProprietorName, normalize-space($sbiLastName))]/prd:ProfileType/@code" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    


    <xsl:if test="position() &gt; 1">
      <br/>
    </xsl:if>
    
    <!-- begin owner information -->
    <a name="Owner{position()}"></a>
    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="#015CAE">
        
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#ffffff">
                <!-- header banner -->
                <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                  <tr height="20">
                    <td bgcolor="#015CAE" align="center" valign="middle">
                      <font color="#ffffff"><b>Owner/Guarantor Information - <xsl:value-of select="prd:ProprietorFirstName" /> <xsl:value-of select="prd:ProprietorSurname" /></b></font>
                    </td>
                  </tr>  

                  <tr>
                    <td style="line-height:5px;" valign="bottom">&#160;</td>
                  </tr>  

                </table>
                <!-- end header banner -->

                <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="1%">
                      <img src="../images/spacer.gif" border="0" width="5" height="1" /></td>

                    <td width="98%">  
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">

                        <xsl:if test="normalize-space($hasDispute) != ''  and normalize-space($consumerStatement) != ''">
                          <tr>
                            <td colspan="3">
                              <font color="#ff0000"><b>Consumer statement:</b>
                              <xsl:value-of select="$consumerStatement" />
                              </font>
                            </td>
                          </tr>  
  
                          <tr>
                            <td colspan="3" style="line-height:7px;" valign="bottom">&#160;</td>
                          </tr>  
                        </xsl:if>

                        <tr>  

                          <!-- owner account information -->
                          <td width="48%" valign="top">
                            <xsl:call-template name="OwnerAccountInformation" />
                          </td>
                          <!-- end owner account information -->
                        
                          <td width="4%"><xsl:text disable-output-escaping="yes">&#160;</xsl:text></td>
                        
                          <!-- owner legal filing, collections -->
                          <td width="48%" valign="top">
                            <xsl:call-template name="OwnerLegalFilings" />
                          </td>
                          <!-- end owner legal filing, collections -->
                                                           
                        </tr>

                        <tr>
                          <td style="line-height:3px;" colspan="3">&#160;</td>
                        </tr>
                      </table> 
                    </td>
                    
                    <td width="1%" align="left">
                      <img src="../images/spacer.gif" border="0" width="3" height="1" /></td>
                  </tr>    
                </table>
  
              </td>
            </tr>     
          </table> 
  
        </td>
      </tr>
    </table>
    <!-- end owner Information -->
  
  </xsl:template>
  
  
  <!--
  *********************************************
  * OwnerAccountInformation template
  *********************************************
  -->
  <xsl:template name="OwnerAccountInformation">

    <xsl:variable name="bankCard6Months">
      <xsl:choose>                  
        <xsl:when test="prd:TotOpenBankcardTradesLast6Mos and string(number(prd:TotOpenBankcardTradesLast6Mos)) != 'NaN'">                    
          <xsl:value-of select="number(prd:TotOpenBankcardTradesLast6Mos)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
  
    <xsl:variable name="bankCardBalanceLimitRatio6Months">
      <xsl:choose>                  
        <xsl:when test="prd:TotBankcardBalToLimitRatioLast6Mos and string(number(prd:TotBankcardBalToLimitRatioLast6Mos)) != 'NaN'">                    
          <xsl:value-of select="format-number(prd:TotBankcardBalToLimitRatioLast6Mos div 10000, '0.##%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0%'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
  
    <xsl:variable name="openRevolvingTrades">
      <xsl:choose>                  
        <xsl:when test="prd:TotOpenRevTrades and string(number(prd:TotOpenRevTrades)) != 'NaN'">                    
          <xsl:value-of select="number(prd:TotOpenRevTrades)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="revolvingCredit6Months">
      <xsl:choose>                  
        <xsl:when test="prd:TotAvailableRevCreditLast6Mos">                    
          <xsl:value-of select="format-number(prd:TotAvailableRevCreditLast6Mos, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="percentAutoLease30PlusDays">
      <xsl:choose>                  
        <xsl:when test="prd:PctAutoLeaseTrades30PlusDPD and string(number(prd:PctAutoLeaseTrades30PlusDPD)) != 'NaN'">                    
          <xsl:value-of select="format-number(prd:PctAutoLeaseTrades30PlusDPD div 10000, '0.##%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0%'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
  
    <xsl:variable name="percentrealEstate30PlusDays">
      <xsl:choose>                  
        <xsl:when test="prd:PctRealEstateTrades30PlusDPD and string(number(prd:PctRealEstateTrades30PlusDPD)) != 'NaN'">                    
          <xsl:value-of select="format-number(prd:PctRealEstateTrades30PlusDPD div 10000, '0.##%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0%'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="creditInquiries6Months">
      <xsl:choose>                  
        <xsl:when test="prd:TotalCreditPurposeInquiriesLast6Mos and string(number(prd:TotalCreditPurposeInquiriesLast6Mos)) != 'NaN'">                    
          <xsl:value-of select="number(prd:TotalCreditPurposeInquiriesLast6Mos)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>
        <td height="18">
          <font color="#015CAE"><b>Owner Account Information</b></font></td>
      </tr>  

      <tr height="18">  
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="90%">
                Open bank card trades reported previous 6 months:</td>
              <td width="10%" align="right" nowrap="nowrap"><b><xsl:value-of select="$bankCard6Months" /></b></td>
            </tr>
          </table>
        </td>  
      </tr>
      
      <tr height="18">  
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="90%">
                Bank card balance to limit ratio previous 6 months:</td>
              <td width="10%" align="right" nowrap="nowrap"><b><xsl:value-of select="$bankCardBalanceLimitRatio6Months" /></b></td>
            </tr>
          </table>
        </td>  
      </tr>
      
      <tr height="18">  
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="90%">
                Open revolving trades:</td>
              <td width="10%" align="right" nowrap="nowrap"><b><xsl:value-of select="$openRevolvingTrades" /></b></td>
            </tr>
          </table>
        </td>  
      </tr>
      
      <tr height="18">  
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="90%">
                Available revolving credit reported previous 6 months:</td>
              <td width="10%" align="right" valign="top" nowrap="nowrap"><b><xsl:value-of select="$revolvingCredit6Months" /></b></td>
            </tr>
          </table>
        </td>  
      </tr>
      
      <tr height="18">  
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="90%">
                Percentage of auto lease trades 30+ days delinquent:</td>
              <td width="10%" align="right" nowrap="nowrap"><b><xsl:value-of select="$percentAutoLease30PlusDays" /></b></td>
            </tr>
          </table>
        </td>  
      </tr>
      
      <tr height="18">  
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="90%">
                Percentage of real estate trades 30+ days delinquent:</td>
              <td width="10%" align="right" nowrap="nowrap"><b><xsl:value-of select="$percentrealEstate30PlusDays" /></b></td>
            </tr>
          </table>
        </td>  
      </tr>
      
      <tr height="18">  
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="90%">
                Credit inquiries previous 6 months:</td>
              <td width="10%" align="right" nowrap="nowrap"><b><xsl:value-of select="$creditInquiries6Months" /></b></td>
            </tr>
          </table>
        </td>  
      </tr>
      
    </table>  
  
  </xsl:template>
  
  
  <!--
  *********************************************
  * OwnerLegalFilings template
  *********************************************
  -->
  <xsl:template name="OwnerLegalFilings">

    <xsl:variable name="ageOldestTrade">
      <xsl:choose>                  
        <xsl:when test="prd:AgeOldestTradeline and string(number(prd:AgeOldestTradeline)) != 'NaN'">    
          <xsl:variable name="months">
            <xsl:value-of select="prd:AgeOldestTradeline mod 12" />
          </xsl:variable>                
          
          <xsl:variable name="years">
            <xsl:value-of select="floor(prd:AgeOldestTradeline div 12)" />
          </xsl:variable>                
          
          <xsl:choose>                  
            <xsl:when test="$years &gt; 0">    
              <xsl:value-of select="concat($years, ' YRS ', $months, ' MO')" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="concat($months, ' MO')" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0 MO'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="satisfactoryTrades24Months">
      <xsl:choose>                  
        <xsl:when test="prd:SatisfactoryTradesLast24Mos and string(number(prd:SatisfactoryTradesLast24Mos)) != 'NaN'">                    
          <xsl:value-of select="number(prd:SatisfactoryTradesLast24Mos)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="presentlyDelinquent6Months">
      <xsl:choose>                  
        <xsl:when test="prd:TradesPresentlyDelinquentOrPastDueLast6Mos and string(number(prd:TradesPresentlyDelinquentOrPastDueLast6Mos)) != 'NaN'">                    
          <xsl:value-of select="number(prd:TradesPresentlyDelinquentOrPastDueLast6Mos)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="collectionLegalAccounts">
      <xsl:choose>                  
        <xsl:when test="prd:TotCollectionsLegalFilingsOrDerogAccts and string(number(prd:TotCollectionsLegalFilingsOrDerogAccts)) != 'NaN'">                    
          <xsl:value-of select="number(prd:TotCollectionsLegalFilingsOrDerogAccts)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="derogPublicRecords24Months">
      <xsl:choose>                  
        <xsl:when test="prd:TotDerogPubRecsLast24Mos and string(number(prd:TotDerogPubRecsLast24Mos)) != 'NaN'">                    
          <xsl:value-of select="number(prd:TotDerogPubRecsLast24Mos)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="derogPublicRecords250Dollars">
      <xsl:choose>                  
        <xsl:when test="prd:TotDerogPubRecsGT250 and string(number(prd:TotDerogPubRecsGT250)) != 'NaN'">                    
          <xsl:value-of select="number(prd:TotDerogPubRecsGT250)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
     
      <tr>
        <td height="18">
          <font color="#015CAE"><b>Owner Legal Filings and Payment Status</b></font></td>
      </tr>  

      <tr height="18">  
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="90%">
                Age of oldest trade:</td>
              <td width="10%" align="right" nowrap="nowrap"><b><xsl:value-of select="$ageOldestTrade" /></b></td>
            </tr>
          </table>
        </td>  
      </tr>
      
      <tr height="18">  
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="90%">
                Satisfactory trades previous 24 months:</td>
              <td width="10%" align="right" nowrap="nowrap"><b><xsl:value-of select="$satisfactoryTrades24Months" /></b></td>
            </tr>
          </table>
        </td>  
      </tr>
      
      <tr height="18">  
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="90%">
                Trades presently delinquent previous 6 months:</td>
              <td width="10%" align="right" valign="top" nowrap="nowrap"><b><xsl:value-of select="$presentlyDelinquent6Months" /></b></td>
            </tr>
          </table>
        </td>  
      </tr>
      
      <tr height="18">  
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="90%">
                Collections, legal filings, or derogatory accounts:</td>
              <td width="10%" align="right" nowrap="nowrap"><b><xsl:value-of select="$collectionLegalAccounts" /></b></td>
            </tr>
          </table>
        </td>  
      </tr>
      
      <tr height="18">  
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="90%">
                Derogatory public records filed within 24 months:</td>
              <td width="10%" align="right" nowrap="nowrap"><b><xsl:value-of select="$derogPublicRecords24Months" /></b></td>
            </tr>
          </table>
        </td>  
      </tr>
      
      <tr height="18">  
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="90%">
                Derogatory public record greater than $250:</td>
              <td width="10%" align="right" nowrap="nowrap"><b><xsl:value-of select="$derogPublicRecords250Dollars" /></b></td>
            </tr>
          </table>
        </td>  
      </tr>
      
    </table>  

  </xsl:template>  
</xsl:stylesheet>
