
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">
                

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * CustomHeaderMessage template
  *********************************************
  -->
  <xsl:template name="CustomHeaderMessage">
  
    <!-- custom-header -->
    <xsl:variable name="custom-header">
      <xsl:call-template name="get-custom-header">
      </xsl:call-template>
    </xsl:variable>

    <xsl:if test="prd:CustomHeader">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td>
	      <xsl:value-of select="$custom-header" disable-output-escaping="yes" />
	    </td>
	  </tr>
	</table>
    </xsl:if>

</xsl:template>
  
  <!--
  *********************************************
  * get-custom-header template
  * ouput text from custom header
  *********************************************
  -->
  <xsl:template name="get-custom-header">
    <xsl:for-each select="prd:CustomHeader">

	<!-- custom header -->
	<xsl:variable name="customHeader">
         <xsl:variable name="CustomHeader">
           <xsl:value-of select="." />
        </xsl:variable>

        <xsl:call-template name="convertcase">
          <xsl:with-param name="toconvert" select="$CustomHeader" />
          <xsl:with-param name="conversion" select="'proper'" />
        </xsl:call-template>
	</xsl:variable>
	
      <xsl:if test="normalize-space($customHeader) != ''">
        <xsl:value-of select="." disable-output-escaping="yes" /><xsl:text>&#160;</xsl:text>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>