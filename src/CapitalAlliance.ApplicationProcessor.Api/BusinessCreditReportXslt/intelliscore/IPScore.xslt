
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">
                

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />

  <!--
  *********************************************
  * IntelliscorePlusScore template
  *********************************************
  -->
  <xsl:template name="IntelliscorePlusScore">

    <xsl:variable name="scoreBarWidth">
      <xsl:value-of select="'300'" />
    </xsl:variable>
    
    <xsl:variable name="pointerTableWidth">
      <xsl:value-of select="'308'" />
    </xsl:variable>
  
    <xsl:variable name="pointerWidth">
      <xsl:value-of select="'9'" />
    </xsl:variable>
    
    <xsl:variable name="action">
      <xsl:choose>
        <xsl:when test="prd:IntelliscoreScoreInformation/prd:Action">
          <xsl:value-of select="prd:IntelliscoreScoreInformation/prd:Action" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="score">
      <xsl:value-of select="(prd:IntelliscoreScoreInformation/prd:ScoreInfo/prd:Score div 100)" />
    </xsl:variable>
  
    <xsl:variable name="scoreText">
      <xsl:choose>
        <xsl:when test="contains($score, '.')">
          <xsl:value-of select="normalize-space(substring-before($score, '.'))" />
        </xsl:when>
        
        <xsl:otherwise>
          <xsl:value-of select="$score" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
  
    <xsl:variable name="unscorableText">
      <xsl:choose>
        <xsl:when test="number($scoreText) = 998">
          <xsl:value-of select="'This report includes a bankruptcy within the last 24 months. Therefore an Intelliscore Plus score cannot be calculated.'" />
        </xsl:when>
        
        <xsl:when test="number($scoreText) = 999">
          <xsl:value-of select="'This report does not include data elements that have been statistically proven to predict serious future delinquency. Therefore an Intelliscore Plus score cannot be calculated.'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="isExclusion">
      <xsl:choose>
        <xsl:when test="number($scoreText) = 998 or number($scoreText) = 999">
          <xsl:value-of select="1" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="0" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="hasScore">
      <xsl:choose>
        <xsl:when test="prd:IntelliscoreScoreInformation/prd:ScoreInfo/prd:Score">
          <xsl:value-of select="1" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="0" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="pointerCenter">
      <xsl:value-of select="round($score div 100 * $scoreBarWidth)" />
    </xsl:variable>
    
    <xsl:variable name="pointerLeft">
      <xsl:value-of select="$pointerCenter" />
    </xsl:variable>
    
    <xsl:variable name="segmentUsed">
      <xsl:choose>
        <xsl:when test="prd:IntelliscoreScoreInformation/prd:Filler and not(boolean(number($isExclusion)))">
          <xsl:value-of select="number(prd:IntelliscoreScoreInformation/prd:Filler)" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="0" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="boxTitle">
      <xsl:choose>
        <xsl:when test="$segmentUsed &gt;= 1 and $segmentUsed &lt;= 5">
          <xsl:value-of select="'Commercial Model'" />
        </xsl:when>
        <xsl:when test="$segmentUsed = 6">
          <xsl:value-of select="'Blended Model'" />
        </xsl:when>
        <xsl:when test="$segmentUsed = 7">
          <xsl:value-of select="'Business Owner/Guarantor Model'" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'Exclusion Score'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="scoreHeaderText1">
      <xsl:value-of select="'Intelliscore Plus predicts the likelihood of serious credit delinquency within the next 12 months'" />
    </xsl:variable>
    <xsl:variable name="scoreHeaderText2">
      <xsl:value-of select="'based on business and/or owner/guarantor risk factors. Higher scores indicate lower risk.'" />
    </xsl:variable>
    <xsl:variable name="scoreHeaderText3">
      <xsl:value-of select="'This company is classified as a large business and is compared to businesses of similar size.'" />
    </xsl:variable>

    <xsl:variable name="scoreBasedText">
      <xsl:choose>
        <xsl:when test="$segmentUsed &gt;= 1 and $segmentUsed &lt;= 5">
          <xsl:value-of select="'Based on business credit'" />
        </xsl:when>
        <xsl:when test="$segmentUsed = 6">
          <xsl:value-of select="'Based on business and owner credit'" />
        </xsl:when>
        <xsl:when test="$segmentUsed = 7">
          <xsl:value-of select="'Based on owner credit'" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="percentRanking">
      <xsl:choose>                  
        <xsl:when test="prd:IntelliscoreScoreInformation/prd:PercentileRanking">                     
          <xsl:value-of select="format-number(prd:IntelliscoreScoreInformation/prd:PercentileRanking div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="#015CAE">
        
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#ffffff">
                <!-- header banner -->
                <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                  <tr height="20">
                    <td bgcolor="#015CAE" align="center" valign="middle">
                      <font color="#ffffff"><b><xsl:value-of select="$boxTitle" /></b></font>
                    </td>
                  </tr>  

                  <tr>
                    <td style="line-height:10px;" valign="bottom">&#160;</td>
                  </tr>  

                  <tr>
                    <td align="center" style="padding:0 5px;">
                      <font color="#015CAE"><b><xsl:value-of select="$scoreHeaderText1" />
                      <br/><xsl:value-of select="$scoreHeaderText2" />
                      <xsl:if test="$segmentUsed = 1">
                        <br/><xsl:value-of select="$scoreHeaderText3" />
                      </xsl:if>
                      </b></font></td>
                  </tr>  

                  <tr>
                    <td style="line-height:14px;" valign="bottom">&#160;</td>
                  </tr>  

                </table>
                <!-- end header banner -->

                <xsl:if test="boolean(number($hasScore))" >

                  <!-- score text -->
                  <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td align="center">
                        <font size="4" color="000000"><b>Intelliscore Plus: <xsl:value-of select="$scoreText" /></b></font></td>
                    </tr>  
                    <tr>
                      <td style="line-height:3px;">&#160;</td>
                    </tr>
                  </table>  

                  <!-- Pointer gif -->
                  <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                    
                      <xsl:choose>
                        <xsl:when test="boolean(number($isExclusion))">
                          <td style="line-height:12px;" align="center">&#160;</td>
                        </xsl:when>
    
                        <xsl:otherwise>
                          <td align="center">
                            <table width="{$pointerTableWidth}" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                        
                                <xsl:if test="$pointerLeft &gt; 0" >
                                  <td width="{$pointerLeft}"><img src="../images/spacer.gif" border="0" width="{$pointerLeft}" height="1" /></td>
                                </xsl:if>

                                <td width="{$pointerTableWidth - $pointerLeft}" align="left" height="10" valign="middle"><img src="../images/triangle_black_down.gif" border="0" width="11" height="12" /></td>
                              </tr>
                            </table>    
                          </td>
                        
                        </xsl:otherwise>
                      </xsl:choose>    
                
                    </tr>  
                  </table>  
                  <!-- end Pointer down gif -->
                </xsl:if>

                <!-- color bar-->
                <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="center">
                      <table width="{$scoreBarWidth + 120}" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="60" align="right">
                            High risk <xsl:text disable-output-escaping="yes">&#160;</xsl:text>
                          </td>
                          
                          <td width="{$scoreBarWidth}">
                            <img src="../images/color_meter_round.gif" border="0" width="{$scoreBarWidth}" height="25" /></td>

                          <td width="60" align="left">
                            <xsl:text disable-output-escaping="yes">&#160;</xsl:text> Low risk 
                          </td>
                        </tr>
                      </table>    
                    </td>
                  </tr>  
                </table>  
                <!-- end color bar-->

                <!-- color bar labels -->
                <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="center">
                      <table width="{$scoreBarWidth}" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1" align="left">0</td>

                          <td width="{$scoreBarWidth - 2}" align="center"><font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$scoreBasedText" /></font></td>

                          <td width="1" align="right">100</td>
                        </tr>
                      </table>
                    </td>
                  </tr>  
                </table>    
                <!-- end color bar labels -->
                        
                <xsl:choose>
                  <xsl:when test="boolean(number($isExclusion))">
                    <!-- Exclusion Score info -->
                    <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td style="line-height:14px;" valign="bottom">&#160;</td>
                      </tr>  
                      
                      <tr>
                        <td align="center">
                          <table width="{$scoreBarWidth + 120}" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td align="left">
                                <font color="#015CAE"><b>Unscorable Business</b></font>
                              </td>
                            </tr>
                            <tr>
                              <td align="left">
                                <xsl:value-of select="$unscorableText" />
                              </td>
                            </tr>
                          </table>    
                        </td>
                      </tr>  
                      
                      <tr>
                        <td style="line-height:10px;" valign="bottom">&#160;</td>
                      </tr>  
                    </table>  
                    <!-- end Exclusion Score info -->
                  </xsl:when>

                  <xsl:otherwise>
                    <!-- score factors -->
                    <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td style="line-height:14px;" colspan="3" valign="bottom">&#160;</td>
                      </tr>  

                      <tr>
                        <td width="2%">
                          <img src="../images/spacer.gif" border="0" width="5" height="1" /></td>
                        <td width="96%">  
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>  
                              <td width="48%" valign="top">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td colspan="2"><font color="#015CAE"><b>Factors Lowering the Score</b></font></td>
                                  </tr>
                                  <tr>
                                    <td style="line-height:1px;" colspan="2">&#160;</td>
                                  </tr>      
                                  
                                  <xsl:apply-templates select="prd:ScoreFactors/prd:ScoreFactor" />                                
                                </table>  
                              </td>
                              
                              <td width="4%"><xsl:text disable-output-escaping="yes">&#160;</xsl:text></td>
                              
                              <td width="48%" valign="top">
                                <font color="#015CAE"><b>All Industry Risk Comparison</b></font>
                                <br/>
                                When compared to all businesses, 
                                <xsl:value-of select="$percentRanking" /> of businesses indicate 
                                a higher likelihood of severe delinquency than this business.

                              </td>
                            </tr>
                            <tr>
                              <td style="line-height:3px;" colspan="3">&#160;</td>
                            </tr>
                          </table> 
                        </td>
                        <td width="2%" align="left">
                          <img src="../images/spacer.gif" border="0" width="3" height="1" /></td>
                      </tr>    
                    </table>
                  
                    <xsl:if test="normalize-space($action) != ''">
                      <!-- action code -->
                      <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
                      
                        <tr>
                          <td style="line-height:14px;" valign="bottom">&#160;</td>
                        </tr>  

                        <tr>
                          <td align="center">
                          
                          
                            <table width="95%" border="0" cellspacing="0" cellpadding="1">
                              <tr>
                                <td bgcolor="#015CAE">
                              
                                  <table width="100%" border="0" cellspacing="0" cellpadding="5">
                                    <tr>
                                      <td align="center" bgcolor="#ffffff">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                            <td align="center" >
                                              <font color="#015CAE">
                                              <b>Based on your company's action or risk threshold, this business
                                              falls within the following category:</b></font>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td style="line-height:3px;">&#160;</td>
                                          </tr>
                                          <tr>
                                            <td align="center" >
                                              <b><font color="#ff0000" size="3"><xsl:value-of select="$action" /></font></b>
                                            </td>
                                          </tr>
                                        </table>
                                      </td>
                                    </tr>     
                                  </table> 
                           
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>  
                        
                        <tr>
                          <td valign="bottom" style="line-height:9px;">&#160;</td>
                        </tr>  
                      </table>
                      <!-- end action code -->
                      
                    </xsl:if>
                  </xsl:otherwise>
                </xsl:choose>    
                        
                        
                        
              </td>
            </tr>     
          </table> 
  
        </td>
      </tr>
    </table>
    
  </xsl:template>


  <!--
  *********************************************
  * ScoreFactor template
  *********************************************
  -->
  <xsl:template match="prd:ScoreFactor" >
    <!-- Only need 4 factors -->
    <xsl:if test="position() &lt; 5">
      <xsl:if test="position() &gt; 1">
        <tr>
          <td colspan="2" style="line-height:3px;">&#160;</td>
        </tr>                                      
      </xsl:if>

        <tr>
          <td width="1%" align="left" valign="top" style="padding-top:1px; "><img src="../images/ball_bullet_small_green.gif" border="0" /></td>
          <td width="99%" valign="bottom"><font size="1"><xsl:value-of select="." /></font></td>
        </tr>
    </xsl:if>
  </xsl:template>
  
</xsl:stylesheet>
