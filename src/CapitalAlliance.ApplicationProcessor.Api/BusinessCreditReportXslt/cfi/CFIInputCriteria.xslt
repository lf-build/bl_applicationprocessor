
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * CFI Input Criteria template
  *********************************************
  -->
  <xsl:template name="CFIInputCriteria">
  
    <!-- Section title -->
    <xsl:call-template name="SectionTitle">
      <xsl:with-param name="title" select="'Input Criteria'" />
    </xsl:call-template>

    <!-- match ident   -->
    <xsl:variable name="matchIdent">
      <xsl:call-template name="CFIBusinessMatch" >
      </xsl:call-template>
    </xsl:variable>

    <!-- blue box border -->
    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="#0099cc">

          <!-- inner white box -->
          <table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tr>
              <td bgcolor="#ffffff">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                  <!-- business data section -->  
                  <xsl:call-template name="InputCriteria" >
                     <xsl:with-param name="InputIdent" select="$matchIdent" />
                  </xsl:call-template>
<!--
                  <xsl:choose>	
                    <xsl:when test="prd:StdAddr[@type='Business'][@ident='B1A1']">
                      <xsl:apply-templates select="prd:StdAddr[@type='Business'][@ident='B1A1']" />
                    </xsl:when>
                    <xsl:otherwise>
                      <tr>
                        <td colspan="2">
                          <font color="#ff0000" size="1"><b>No business data available</b></font>
                        </td>
                      </tr>  
                    </xsl:otherwise>
                  </xsl:choose>
-->
                  <!-- end business data section -->  
                </table>
              </td>
            </tr>
          </table>
          <!-- end inner white box -->
        </td>
      </tr>  
    </table>

    <!-- begin Header notes -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td style="line-height:8px;" width="100%">&#160;</td>
      </tr>

      <tr>
        <td width="100%">  
          The Business  report verifies application information, 
          identifies application irregularities and flags submitted data elements 
          that match the Office of Foreign Assets Control (OFAC) Specially Designated 
          Nationals list. 
        </td>
      </tr>    
    </table>
    <!-- end Header notes -->

  </xsl:template>


  <!--
  *********************************************
  * BusinessNameAndAddress template
  *********************************************
  -->
  <xsl:template name="InputCriteria">
  <xsl:param name="InputIdent" />

  <!-- bus name -->
  <xsl:variable name="busName">
    <xsl:call-template name="CFIBusinessNameAddress" >
       <xsl:with-param name="VerificationIdent" select="'B1A1'" />
       <xsl:with-param name="request" select="'BUSNAME'" />
    </xsl:call-template>
  </xsl:variable>

  <!--dba-->
  <xsl:variable name="dba">
    <xsl:call-template name="CFIBusinessNameAddress" >
       <xsl:with-param name="VerificationIdent" select="'B1A1'" />
       <xsl:with-param name="request" select="'DBA'" />
    </xsl:call-template>
  </xsl:variable>

  <!-- StreetAddress -->
  <xsl:variable name="streetAddress">
    <xsl:call-template name="CFIBusinessNameAddress" >
       <xsl:with-param name="VerificationIdent" select="$InputIdent" />
       <xsl:with-param name="request" select="'PRIADDR'" />
    </xsl:call-template>
  </xsl:variable>

  <!-- city, state, zip -->
  <xsl:variable name="cityStateZip">
    <xsl:call-template name="CFIBusinessNameAddress" >
       <xsl:with-param name="VerificationIdent" select="$InputIdent" />
       <xsl:with-param name="request" select="'PRICSZ'" />
    </xsl:call-template>
  </xsl:variable>

  <!-- phone -->
  <xsl:variable name="phone">
    <xsl:call-template name="CFIBusinessNameAddress" >
       <xsl:with-param name="VerificationIdent" select="$InputIdent" />
       <xsl:with-param name="request" select="'PRIPHONE'" />
    </xsl:call-template>
    </xsl:variable>

  <!-- Tax ID -->
  <xsl:variable name="taxID">
    <xsl:call-template name="CFIBusinessNameAddress" >
       <xsl:with-param name="VerificationIdent" select="$InputIdent" />
       <xsl:with-param name="request" select="'PRITAXID'" />
    </xsl:call-template>
  </xsl:variable>

  <!-- ALT streetAddress -->
  <xsl:variable name="ALTstreetAddress">
    <xsl:call-template name="CFIBusinessNameAddress" >
       <xsl:with-param name="VerificationIdent" select="$InputIdent" />
       <xsl:with-param name="request" select="'ALTADDR'" />
    </xsl:call-template>
  </xsl:variable>

  <!-- ALT city, state, zip -->
  <xsl:variable name="ALTCityStateZip">
    <xsl:call-template name="CFIBusinessNameAddress" >
       <xsl:with-param name="VerificationIdent" select="$InputIdent" />
       <xsl:with-param name="request" select="'ALTCSZ'" />
    </xsl:call-template>
  </xsl:variable>

  <!--  #3 streetAddress -->
  <xsl:variable name="N3streetAddress">
    <xsl:call-template name="CFIBusinessNameAddress" >
       <xsl:with-param name="VerificationIdent" select="$InputIdent" />
       <xsl:with-param name="request" select="'N3ADDR'" />
    </xsl:call-template>
  </xsl:variable>

  <!--  #3 city, state, zip -->
  <xsl:variable name="N3CityStateZip">
    <xsl:call-template name="CFIBusinessNameAddress" >
       <xsl:with-param name="VerificationIdent" select="$InputIdent" />
       <xsl:with-param name="request" select="'N3CSZ'" />
    </xsl:call-template>
  </xsl:variable>

  <!--  #4 streetAddress -->
  <xsl:variable name="N4streetAddress">
    <xsl:call-template name="CFIBusinessNameAddress" >
       <xsl:with-param name="VerificationIdent" select="$InputIdent" />
       <xsl:with-param name="request" select="'N4ADDR'" />
    </xsl:call-template>
  </xsl:variable>

  <!--  #4 city, state, zip -->
  <xsl:variable name="N4CityStateZip">
    <xsl:call-template name="CFIBusinessNameAddress" >
       <xsl:with-param name="VerificationIdent" select="$InputIdent" />
       <xsl:with-param name="request" select="'N4CSZ'" />
    </xsl:call-template>
  </xsl:variable>

    <tr>
      <td colspan="2" align="left"><font size="1" style="FONT-FAMILY: 'verdana';"><b><xsl:value-of select="$busName" /></b></font></td>
    </tr>

    <tr>
      <td width="50%" valign="top">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">

          <xsl:if test="normalize-space($dba) != '' ">
            <tr>
              <td width="35%">
                <font size="1" style="FONT-FAMILY: 'verdana';">DBA:</font></td>
              <td width="65%">
                <font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$dba"/></font></td>
            </tr>

            <tr>
              <td colspan="2" style="line-height:5px;">&#160;</td>
            </tr>
          </xsl:if>

          <tr>
            <td width="35%" valign="top">
              <font size="1" style="FONT-FAMILY: 'verdana';">Primary address:</font></td>
            <td width="65%">
              <font size="1" style="FONT-FAMILY: 'verdana';">
                <xsl:value-of select="$streetAddress"/>
                <br />
                <xsl:value-of select="normalize-space($cityStateZip)" /> 
                <xsl:if test="normalize-space($phone) != '' ">
                   <br />
                   <xsl:value-of select="normalize-space($phone)" />
                </xsl:if>
              </font>
            </td>
          </tr>

          <xsl:if test="normalize-space($N3streetAddress) != '' ">
            <tr>
              <td colspan="2" style="line-height:5px;">&#160;</td>
            </tr>

            <tr>
              <td width="35%" valign="top">
                <font size="1" style="FONT-FAMILY: 'verdana';">Address #3*:</font></td>
              <td width="65%">
                <font size="1" style="FONT-FAMILY: 'verdana';">
                  <xsl:value-of select="$N3streetAddress"/>
                  <br />
                  <xsl:value-of select="$N3CityStateZip"/>
                </font>
              </td>
            </tr>
          </xsl:if>

        </table>                                                       
      </td>

      <td width="50%" valign="top">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <xsl:if test="normalize-space($taxID) != '' ">
            <tr>
              <td width="35%">
                <font size="1" style="FONT-FAMILY: 'verdana';">Tax ID:</font></td>
              <td width="65%">
                <font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$taxID"/></font></td>
            </tr>

            <tr>
              <td colspan="2" style="line-height:5px;">&#160;</td>
            </tr>
          </xsl:if>

          <xsl:if test="normalize-space($ALTstreetAddress) != '' ">
            <tr>
              <td width="35%" valign="top">
                <font size="1" style="FONT-FAMILY: 'verdana';">Alternate address:</font></td>
              <td width="65%">
                <font size="1" style="FONT-FAMILY: 'verdana';">
                  <xsl:value-of select="$ALTstreetAddress"/>
                  <br />
                  <xsl:value-of select="$ALTCityStateZip"/>
                </font>
              </td>
            </tr>

            <tr>
              <td colspan="2" style="line-height:5px;">&#160;</td>
            </tr>
          </xsl:if>

          <xsl:if test="normalize-space($N4streetAddress) != '' ">
            <tr>
              <td width="35%" valign="top">
                <font size="1" style="FONT-FAMILY: 'verdana';">Address #4*:</font></td>
              <td width="65%">
                <font size="1" style="FONT-FAMILY: 'verdana';">
                  <xsl:value-of select="$N4streetAddress"/>
                  <br />
                  <xsl:value-of select="$N4CityStateZip"/>
                </font>
              </td>
            </tr>
          </xsl:if>

        </table>                                                       
      </td>
    </tr>

  </xsl:template>

</xsl:stylesheet>