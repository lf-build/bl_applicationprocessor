
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:rsp="http://www.experian.com/NetConnectResponse"
  xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * Global variables and parameters
  *********************************************
  <xsl:param name="product" select="'CFI'" />
  -->
  <xsl:param name="index" select="1" />
  <xsl:param name="systemYear" select="'2008'" />
    <xsl:variable name="Product">
      <xsl:choose>
        <xsl:when test="count(//prd:CommercialFraudInsight/prd:GeneralSummary[@type = 'Business']) &gt; 0 and count(//prd:CommercialFraudInsight/prd:GeneralSummary[@type = 'Principal']) &gt; 0  ">
          <xsl:value-of select="'CFICOMBO'" />
        </xsl:when>

        <xsl:when test="count(//prd:CommercialFraudInsight/prd:GeneralSummary[@type = 'Business']) = 0 and count(//prd:CommercialFraudInsight/prd:GeneralSummary[@type = 'Principal']) &gt; 0  ">
          <xsl:value-of select="'CFIBP'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'CFI'" />
        </xsl:otherwise>
      </xsl:choose>
  </xsl:variable>
  <xsl:param name="product" select="$Product" />
  <xsl:param name="baseProduct" select="'CFI'" />



  <!--
  *********************************************
  * include template
  *********************************************
  -->
  <xsl:include href="../common/ReportHeader.xslt" />
  <xsl:include href="../common/Util.xslt" />
  <xsl:include href="../common/case.xslt" />
  <xsl:include href="../common/BackToTop.xslt" />
  <xsl:include href="../common/CompanyInformation.xslt" />
  <xsl:include href="../common/TradeFilingSummary.xslt" />
  <xsl:include href="CFIInputCriteria.xslt" />
  <xsl:include href="CFINameAddressPhone.xslt" />
  <xsl:include href="CFIExecutiveSummary.xslt" />
  <xsl:include href="CFIAlertsAndVerificationDetails.xslt" />
  <xsl:include href="CFIAuthenticationDetails.xslt" />
  <xsl:include href="../common/ReportFooter.xslt" />


  <!--
  *********************************************
  * Initial template
  *********************************************
  -->
  <xsl:template match="/">
    <html>
      <head>
        <title>Commercial Fraud Insight</title>

          <style type="text/css">
            td {font-size: 9pt; font-family: 'arial';}
          </style>
      </head>

      <body>
        <a name="top"></a>
        <table width="715" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td>      
               
              <!-- CommericalFraudInsighttemplate -->
              <xsl:apply-templates select="//prd:CommercialFraudInsight[$index]" />
              
            </td>
          </tr>
        </table>    
      </body>
    </html>
  </xsl:template>


  <!--
  *********************************************
  * CommercialFraudInsight template
  *********************************************
  -->
  <xsl:template match="prd:CommercialFraudInsight" >

    <!-- Report Date  -->
    <xsl:variable name="reportDate">
      <xsl:value-of select="$systemYear" />
    </xsl:variable>

    <!-- Profile Type  -->
    <xsl:variable name="profileType">
      <xsl:choose>
        <xsl:when test="$product = 'CFICOMBO' ">
          <xsl:value-of select="'Business and Business Principal CFI'" />
        </xsl:when>

        <xsl:when test="$product = 'CFIBP' ">
          <xsl:value-of select="'Business Principal CFI'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'Business CFI'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- Report Header -->
    <xsl:call-template name="ReportHeader">
      <xsl:with-param name="reportType" select="$profileType" />
      <xsl:with-param name="reportName" select="prd:StdAddr/prd:BusName" />
      <xsl:with-param name="reference" select="prd:InputSummary/prd:Comments" />
    </xsl:call-template>

    <br />

    <!-- Identifying Information -->
    <xsl:call-template name="CFIInputCriteria" />

    <br />

    <!-- Executive Summary -->
    <xsl:call-template name="CFIExecutiveSummary" />

    <br />

    <!-- Business Alerts and Verification Details -->
    <xsl:if test="prd:GeneralSummary[@ident='B1A1']/prd:ResponseType != ''">
      <!-- CF IBusiness Alerts And Verification Details -->
      <xsl:call-template name="CFIBusinessAlertsAndVerification" />
    </xsl:if>

    <!-- Business Authentication Details -->
    <xsl:call-template name="CFIBusinessAuthenticationDetails">
       <xsl:with-param name="AuthenticationType" select="'BUSINESS'" />
    </xsl:call-template>

     <!-- is it a limited report?  -->
     <xsl:variable name="reportEnd">
        <xsl:choose>		              
          <xsl:when test="normalize-space(prd:BillingIndicator) = 'A' ">
             <xsl:value-of select="'End of limited'" />
          </xsl:when>

          <xsl:otherwise>
             <xsl:value-of select="'End of report'" />
          </xsl:otherwise>
        </xsl:choose>
     </xsl:variable>

     <!-- Report Footer -->
     <xsl:call-template name="ReportFooter">
       <xsl:with-param name="reportType" select="'CFI'" />
       <xsl:with-param name="reportDate" select="$reportDate" />
       <xsl:with-param name="reportEnd" select="$reportEnd" />
     </xsl:call-template>

  </xsl:template>

  <!--
  *********************************************
  * this template overrides the built-in rule
  * do nothing with stuff I'm not interested in
  *********************************************
  -->
  <xsl:template match="text()|@*">
  </xsl:template>

</xsl:stylesheet>