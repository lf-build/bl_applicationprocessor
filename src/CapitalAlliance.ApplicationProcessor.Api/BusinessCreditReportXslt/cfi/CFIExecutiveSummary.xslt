
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rsp="http://www.experian.com/NetConnectResponse"
                xmlns:prd="http://www.experian.com/ARFResponse">

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


 <!--
 *********************************************
 * CFIExecutiveSummary template
 *********************************************
 -->
 <xsl:template name="CFIExecutiveSummary">

     <!-- N3ADDR -->
     <xsl:variable name="N3ADDR">
      <xsl:call-template name="CFIBusinessNameAddress" >
         <xsl:with-param name="VerificationIdent" select="'B1A1'" />
         <xsl:with-param name="request" select="'N3ADDR'" />
      </xsl:call-template>
     </xsl:variable>

     <!-- N4ADDR -->
     <xsl:variable name="N4ADDR">
      <xsl:call-template name="CFIBusinessNameAddress" >
         <xsl:with-param name="VerificationIdent" select="'B1A1'" />
         <xsl:with-param name="request" select="'N4ADDR'" />
      </xsl:call-template>
     </xsl:variable>

     <xsl:variable name="N3address">
      <xsl:choose>
        <xsl:when test="normalize-space($N3ADDR)">
          <xsl:value-of select="'True'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'False'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

     <xsl:variable name="N4address">
      <xsl:choose>
        <xsl:when test="normalize-space($N4ADDR)">
          <xsl:value-of select="'True'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'False'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- Section title -->
    <xsl:call-template name="SectionTitle">
      <xsl:with-param name="title" select="'Executive Summary'" />
    </xsl:call-template>

   <table cellpadding="0" cellspacing="0" border="0" width="100%">
     <tr>
       <!-- begin Business Verification Matrix -->
       <td valign="top" width="49%" rowspan="2">
          <xsl:call-template name="BusinessGraph" />
        </td>

        <td width="10" align="center" rowspan="2">
          <xsl:text disable-output-escaping="yes">&#160;</xsl:text>
        </td>

        <!-- legal, trade counts etc. column -->
        <td width="49%" valign="top">
          <xsl:call-template name="BusinessAlerts" />
        </td>
      </tr>

      <tr>
        <td height="10" valign="bottom">
          <xsl:if test="normalize-space($N3address) = 'True' or normalize-space($N4address) = 'True' ">
	          <!-- begin footnotes -->
	          <table width="100%" border="0" cellspacing="0" cellpadding="0">
	            <tr>
	              <td width="100%" valign="top">
	                <font size="1"><i>* Addresses #3 and #4 are submitted for historical match only.</i></font>
	             </td>
	            </tr>
	          </table>
          </xsl:if>
        </td>
      </tr>
    </table>

      <!-- begin Legends -->
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="100%" colspan="2" style="line-height:2px;">&#160;</td>
        </tr>
        <tr>
          <td width="80%" valign="bottom">
            <font size="1" style="FONT-FAMILY: 'verdana';">
            <b>V</b> = Verified <xsl:text disable-output-escaping="yes">&#160;&#160;&#160;</xsl:text>
            <b>NF</b> = Data not found <xsl:text disable-output-escaping="yes">&#160;&#160;&#160;</xsl:text>
            <b>X</b> = Not verified <xsl:text disable-output-escaping="yes">&#160;&#160;&#160;</xsl:text>
            <b>NI</b> = No input data
            </font>                   
          </td>

          <td width="20%" valign="bottom">
          </td>
        </tr>
    </table>

      <!-- Back to Top grapic -->
      <xsl:call-template name="BackToTop" />

 </xsl:template>


   <!--
  *********************************************
  * BusinessGraph template
  *********************************************
  -->
  <xsl:template name="BusinessGraph">

    <xsl:variable name="inquiry">
      <xsl:value-of select="prd:InputSummary/prd:Inquiry" />
    </xsl:variable>

    <xsl:variable name="busName">
      <xsl:call-template name="CFIBusinessNameAddress" >
         <xsl:with-param name="VerificationIdent" select="'B1A1'" />
         <xsl:with-param name="request" select="'BUSNAME'" />
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="dba">
      <xsl:call-template name="CFIBusinessNameAddress" >
         <xsl:with-param name="VerificationIdent" select="'B1A1'" />
         <xsl:with-param name="request" select="'DBA'" />
      </xsl:call-template>
    </xsl:variable>

    <!-- match ident   -->
    <xsl:variable name="matchIdent">
      <xsl:call-template name="CFIBusinessMatch" >
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="priAddressIdent">
      <xsl:choose>
        <xsl:when test="normalize-space($matchIdent) = 'B1A1' or normalize-space($matchIdent) = 'B1A2' ">
          <xsl:value-of select="'B1A1'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'B2A1'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="altAddressIdent">
      <xsl:choose>
        <xsl:when test="normalize-space($matchIdent) = 'B1A1' or normalize-space($matchIdent) = 'B1A2' ">
          <xsl:value-of select="'B1A2'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'B2A2'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="phoneTaxIdent">
      <xsl:choose>
        <xsl:when test="normalize-space($matchIdent) = 'B1A1' or normalize-space($matchIdent) = 'B1A2' ">
          <xsl:value-of select="'B1A1'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'B2A1'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="priAddressGreen">
      <xsl:choose>
        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$priAddressIdent]/prd:AddrCode),1,1) = 'B'">
          <xsl:value-of select="'V'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="priAddressYellow">
      <xsl:choose>
        <xsl:when test="not (prd:VerifySummary[@ident=$priAddressIdent]/prd:AddrCode) or normalize-space(prd:VerifySummary[@ident=$priAddressIdent]/prd:AddrCode) = '' ">
          <xsl:value-of select="'NI'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$priAddressIdent]/prd:AddrCode),1,1) = 'A' or substring(normalize-space(prd:VerifySummary[@ident=$priAddressIdent]/prd:AddrCode),1,1) = 'U'">
          <xsl:value-of select="'NF'" />
        </xsl:when>

        <xsl:when test="normalize-space(prd:VerifySummary[@ident=$priAddressIdent]/prd:AddrCode) = 'N/A' ">
          <xsl:value-of select="'N/A'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$priAddressIdent]/prd:AddrCode),1,1) = 'B' ">
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$priAddressIdent]/prd:AddrCode),1,1) = 'N' ">
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$priAddressIdent]/prd:AddrCode),1,1) = 'H' ">
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$priAddressIdent]/prd:AddrCode),1,1) = 'I' ">
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$priAddressIdent]/prd:AddrCode),1,1) = 'R' ">
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$priAddressIdent]/prd:AddrCode),1,1) = 'T' ">
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$priAddressIdent]/prd:AddrCode),1,1) = 'Z' ">
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'NI'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="priAddressRed">
      <xsl:choose>
        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$priAddressIdent]/prd:AddrCode),1,1) = 'N' ">
          <xsl:value-of select="'X'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$priAddressIdent]/prd:AddrCode),1,1) = 'H' ">
          <xsl:value-of select="'Invalid'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$priAddressIdent]/prd:AddrCode),1,1) = 'I' ">
          <xsl:value-of select="'Invalid'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$priAddressIdent]/prd:AddrCode),1,1) = 'R' ">
          <xsl:value-of select="'Invalid'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$priAddressIdent]/prd:AddrCode),1,1) = 'T' ">
          <xsl:value-of select="'Invalid'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$priAddressIdent]/prd:AddrCode),1,1) = 'Z' ">
          <xsl:value-of select="'Invalid'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="altAddressGreen">
      <xsl:choose>
        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$altAddressIdent]/prd:AddrCode),1,1) = 'B'">
          <xsl:value-of select="'V'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="altAddressYellow">
      <xsl:choose>
        <xsl:when test="not (prd:VerifySummary[@ident=$altAddressIdent]/prd:AddrCode) or normalize-space(prd:VerifySummary[@ident=$altAddressIdent]/prd:AddrCode) = '' ">
          <xsl:value-of select="'NI'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$altAddressIdent]/prd:AddrCode),1,1) = 'A' or substring(normalize-space(prd:VerifySummary[@ident=$altAddressIdent]/prd:AddrCode),1,1) = 'U'">
          <xsl:value-of select="'NF'" />
        </xsl:when>

        <xsl:when test="normalize-space(prd:VerifySummary[@ident=$altAddressIdent]/prd:AddrCode) = 'N/A' ">
          <xsl:value-of select="'N/A'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$altAddressIdent]/prd:AddrCode),1,1) = 'B' ">
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$altAddressIdent]/prd:AddrCode),1,1) = 'N' ">
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$altAddressIdent]/prd:AddrCode),1,1) = 'H' ">
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$altAddressIdent]/prd:AddrCode),1,1) = 'I' ">
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$altAddressIdent]/prd:AddrCode),1,1) = 'R' ">
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$altAddressIdent]/prd:AddrCode),1,1) = 'T' ">
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$altAddressIdent]/prd:AddrCode),1,1) = 'Z' ">
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'NI'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="altAddressRed">
      <xsl:choose>
        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$altAddressIdent]/prd:AddrCode),1,1) = 'N' ">
          <xsl:value-of select="'X'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$altAddressIdent]/prd:AddrCode),1,1) = 'H' ">
          <xsl:value-of select="'Invalid'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$altAddressIdent]/prd:AddrCode),1,1) = 'I' ">
          <xsl:value-of select="'Invalid'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$altAddressIdent]/prd:AddrCode),1,1) = 'R' ">
          <xsl:value-of select="'Invalid'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$altAddressIdent]/prd:AddrCode),1,1) = 'T' ">
          <xsl:value-of select="'Invalid'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$altAddressIdent]/prd:AddrCode),1,1) = 'Z' ">
          <xsl:value-of select="'Invalid'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="phoneGreen">
      <xsl:choose>
        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:PhnCode),1,1) = 'B'">
          <xsl:value-of select="'V'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:PhnCode),1,1) = 'Y'">
          <xsl:value-of select="'V'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="phoneYellow">
      <xsl:choose>
        <xsl:when test="not (prd:VerifySummary[@ident=$phoneTaxIdent]/prd:PhnCode) or normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:PhnCode) = '' ">
          <xsl:value-of select="'NI'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:PhnCode),1,1) = 'U'">
          <xsl:value-of select="'NF'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:PhnCode),1,1) = 'A'">
          <xsl:value-of select="'Partial'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:PhnCode),1,1) = 'D'">
          <xsl:value-of select="'Partial'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:PhnCode),1,1) = 'F'">
          <xsl:value-of select="'Partial'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:PhnCode),1,1) = 'H'">
          <xsl:value-of select="'Partial'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:PhnCode),1,1) = 'S'">
          <xsl:value-of select="'Partial'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:PhnCode),1,2) = 'MA'">
          <xsl:value-of select="'Partial'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:PhnCode),1,1) = 'C'">
          <xsl:value-of select="'Mobile'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:PhnCode),1,1) = 'P'">
          <xsl:value-of select="'Mobile'" />
        </xsl:when>

        <xsl:when test="normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:PhnCode) = 'N/A' ">
          <xsl:value-of select="'N/A'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:PhnCode),1,1) = 'B'">
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:PhnCode),1,1) = 'Y'">
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:PhnCode),1,1) = 'N' ">
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:PhnCode),1,1) = 'I' ">
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:PhnCode),1,1) = 'X' ">
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'NI'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="phoneRed">
      <xsl:choose>
        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:PhnCode),1,1) = 'N' ">
          <xsl:value-of select="'X'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:PhnCode),1,1) = 'I' ">
          <xsl:value-of select="'Invalid'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:PhnCode),1,1) = 'X' ">
          <xsl:value-of select="'Invalid'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="taxIDGreen">
      <xsl:choose>
        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:TaxIDCode),1,1) = 'B'">
          <xsl:value-of select="'V'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="taxIDYellow">
      <xsl:choose>
        <xsl:when test="not (prd:VerifySummary[@ident=$phoneTaxIdent]/prd:TaxIDCode) or normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:TaxIDCode) = '' ">
          <xsl:value-of select="'NI'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:TaxIDCode),1,1) = 'U'">
          <xsl:value-of select="'NF'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:TaxIDCode),1,2) = 'AB'">
          <xsl:value-of select="'Partial'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:TaxIDCode),1,2) = 'DB'">
          <xsl:value-of select="'Partial'" />
        </xsl:when>

        <xsl:when test="normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:TaxIDCode) = 'N/A' ">
          <xsl:value-of select="'N/A'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:TaxIDCode),1,1) = 'B'">
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:TaxIDCode),1,1) = 'N' ">
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:TaxIDCode),1,1) = 'I' ">
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'NI'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="taxIDRed">
      <xsl:choose>
        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:TaxIDCode),1,1) = 'N' ">
          <xsl:value-of select="'X'" />
        </xsl:when>

        <xsl:when test="substring(normalize-space(prd:VerifySummary[@ident=$phoneTaxIdent]/prd:TaxIDCode),1,1) = 'I' ">
          <xsl:value-of select="'Invalid'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="#0099cc">

          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#ffffff">
                <!-- box header -->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                  <tr>
                    <td height="20" bgcolor="#0099cc" align="left" valign="middle" colspan="3">
                      <font color="#ffffff"><b><xsl:text disable-output-escaping="yes">&#160;&#160;</xsl:text>Business Verification Matrix</b></font>
                    </td>
                  </tr>  

                  <tr>
                    <td colspan="3" style="line-height:4px;">&#160;</td>
                  </tr>

                  <tr>
                    <td width="1%">
                      <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

                    <td width="98%">
                      <font size="1" style="FONT-FAMILY: 'verdana';">Input business name or DBA compared to Experian's databases.</font>
                    </td>

                    <td width="1%">
                      <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>
                  </tr> 

                  <!-- space row -->
                  <tr>
                    <td style="line-height:4px;" colspan="3">&#160;</td>
                  </tr>

                  <!-- matrix row -->
                  <tr>
                    <td colspan="3" align="center">
                    
                      <table width="300" bgcolor="#0099cc" border="0" cellspacing="0" cellpadding="1">

                        <tr>
                          <td bgcolor="#0099cc">

                          <table width="100%" border="0" cellspacing="2" cellpadding="0">
                      
                            <tr>
                              <td height="16" colspan="4" align="center" valign="middle" bgcolor="#0099cc">
                                <font size="1" style="FONT-FAMILY: 'verdana';" color="#ffffff"><b><xsl:value-of select="$busName"/><xsl:if test="normalize-space($dba) != '' "> / <xsl:value-of select="$dba"/></xsl:if></b></font>
                              </td>
                            </tr>

                            <tr>
                              <td height="16" width="114" align="left" valign="middle" bgcolor="#ffffff">
                                <font size="1" style="FONT-FAMILY: 'verdana';">
                                  <xsl:text disable-output-escaping="yes">&#160;</xsl:text>Primary address
                                </font>
                              </td>

                              <td width="62" bgcolor="#00dd55" align="center" valign="middle">
                                <font size="1" style="FONT-FAMILY: 'verdana';">
                                <b><xsl:value-of select="$priAddressGreen" /></b></font></td>

                              <td width="62" bgcolor="#ffff00" align="center" valign="middle">
                                <font size="1" style="FONT-FAMILY: 'verdana';">
                                <b><xsl:value-of select="$priAddressYellow" /></b></font></td>

                              <td width="62" bgcolor="#ff6666" align="center" valign="middle">
                                <font size="1" style="FONT-FAMILY: 'verdana';" color="#ffffff">
                                <b><xsl:value-of select="$priAddressRed" /></b></font></td>
                            </tr>

                            <tr>
                              <td height="16" align="left" valign="middle" bgcolor="#ffffff">
                                <font size="1" style="FONT-FAMILY: 'verdana';">
                                  <xsl:text disable-output-escaping="yes">&#160;</xsl:text>Alternate address
                                </font>
                              </td>

                              <td bgcolor="#00dd55" align="center" valign="middle">
                                <font size="1" style="FONT-FAMILY: 'verdana';">
                                <b><xsl:value-of select="$altAddressGreen" /></b></font></td>

                              <td bgcolor="#ffff00" align="center" valign="middle">
                                <font size="1" style="FONT-FAMILY: 'verdana';">
                                <b><xsl:value-of select="$altAddressYellow" /></b></font></td>

                              <td bgcolor="#ff6666" align="center" valign="middle">
                                <font size="1" style="FONT-FAMILY: 'verdana';" color="#ffffff">
                                <b><xsl:value-of select="$altAddressRed" /></b></font></td>
                            </tr>

                            <tr>
                              <td height="16" align="left" valign="middle" bgcolor="#ffffff">
                                <font size="1" style="FONT-FAMILY: 'verdana';">
                                  <xsl:text disable-output-escaping="yes">&#160;</xsl:text>Phone number
                                </font>
                              </td>

                              <td bgcolor="#00dd55" align="center" valign="middle">
                                <font size="1" style="FONT-FAMILY: 'verdana';">
                                <b><xsl:value-of select="$phoneGreen" /></b></font></td>

                              <td bgcolor="#ffff00" align="center" valign="middle">
                                <font size="1" style="FONT-FAMILY: 'verdana';">
                                <b><xsl:value-of select="$phoneYellow" /></b></font></td>

                              <td bgcolor="#ff6666" align="center" valign="middle">
                                <font size="1" style="FONT-FAMILY: 'verdana';" color="#ffffff">
                                <b><xsl:value-of select="$phoneRed" /></b></font></td>
                            </tr>

                            <tr>
                              <td height="16" align="left" valign="middle" bgcolor="#ffffff">
                                <font size="1" style="FONT-FAMILY: 'verdana';">
                                  <xsl:text disable-output-escaping="yes">&#160;</xsl:text>Tax ID
                                </font>
                              </td>

                              <td bgcolor="#00dd55" align="center" valign="middle">
                                <font size="1" style="FONT-FAMILY: 'verdana';">
                                <b><xsl:value-of select="$taxIDGreen" /></b></font></td>

                              <td bgcolor="#ffff00" align="center" valign="middle">
                                <font size="1" style="FONT-FAMILY: 'verdana';">
                                <b><xsl:value-of select="$taxIDYellow" /></b></font></td>

                              <td bgcolor="#ff6666" align="center" valign="middle">
                                <font size="1" style="FONT-FAMILY: 'verdana';" color="#ffffff">
                                <b><xsl:value-of select="$taxIDRed" /></b></font></td>
                            </tr>
                          </table>

                          </td>
                        </tr>    

                      </table>
                    </td>
                  </tr>

                  <!-- space row -->
                  <tr>
                    <td style="line-height:8px;" colspan="3">&#160;</td>
                  </tr>  
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

  </xsl:template>


   <!--
  *********************************************
  * BusinessAlerts template
  *********************************************
  -->
  <xsl:template name="BusinessAlerts">

    <xsl:variable name="highRiskAddrAlert">
      <xsl:choose>
        <xsl:when test="prd:CountsSummary[@ident = 'B1A1']/prd:AddrHiRiskCodes and prd:CountsSummary[@ident = 'B1A2']/prd:AddrHiRiskCodes ">
          <xsl:value-of select="format-number((prd:CountsSummary[@ident = 'B1A1']/prd:AddrHiRiskCodes + prd:CountsSummary[@ident = 'B1A2']/prd:AddrHiRiskCodes), '#,##0')" />
        </xsl:when>

        <xsl:when test="prd:CountsSummary[@ident = 'B1A1']/prd:AddrHiRiskCodes and not (prd:CountsSummary[@ident = 'B1A2']/prd:AddrHiRiskCodes) ">
          <xsl:value-of select="format-number((prd:CountsSummary[@ident = 'B1A1']/prd:AddrHiRiskCodes), '#,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="highRiskPhoneAlert">
      <xsl:choose>
        <xsl:when test="prd:CountsSummary[@ident = 'B1A1']/prd:PhnHiRiskCodes and prd:CountsSummary[@ident = 'B1A2']/prd:PhnHiRiskCodes ">
          <xsl:value-of select="format-number((prd:CountsSummary[@ident = 'B1A1']/prd:PhnHiRiskCodes + prd:CountsSummary[@ident = 'B1A2']/prd:PhnHiRiskCodes), '#,##0')" />
        </xsl:when>

        <xsl:when test="prd:CountsSummary[@ident = 'B1A1']/prd:PhnHiRiskCodes and not (prd:CountsSummary[@ident = 'B1A2']/prd:PhnHiRiskCodes) ">
          <xsl:value-of select="format-number((prd:CountsSummary[@ident = 'B1A1']/prd:PhnHiRiskCodes), '#,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="historicalAlert">
      <xsl:choose>
        <xsl:when test="prd:HistoricalValidation">
          <xsl:value-of select="format-number(count(prd:HistoricalValidation), '#,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="OFACAlert">
      <xsl:choose>
        <xsl:when test="prd:CountsSummary[@ident = 'B1A1']/prd:OFACRecs and prd:CountsSummary[@ident = 'B1A2']/prd:OFACRecs ">
          <xsl:value-of select="format-number((prd:CountsSummary[@ident = 'B1A1']/prd:OFACRecs + prd:CountsSummary[@ident = 'B1A2']/prd:OFACRecs), '#,##0')" />
        </xsl:when>

        <xsl:when test="prd:CountsSummary[@ident = 'B1A1']/prd:OFACRecs and not (prd:CountsSummary[@ident = 'B1A2']/prd:OFACRecs) ">
          <xsl:value-of select="format-number((prd:CountsSummary[@ident = 'B1A1']/prd:OFACRecs), '#,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top" height="20">
          <font color="#0099cc"><b>Critical business alerts</b></font>
        </td>
      </tr>  

      <tr>
        <td width="100%" valign="top">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">

            <tr>
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="95%">
                      High-risk address alerts:</td>
                    <td width="5%" align="right" nowrap="nowrap"><b><xsl:value-of select="$highRiskAddrAlert" /></b></td>
                  </tr>
                </table>
              </td>  
            </tr>

            <tr>  
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="95%">
                      High-risk phone alerts:</td>
                    <td width="5%" align="right" nowrap="nowrap"><b><xsl:value-of select="$highRiskPhoneAlert" /></b></td>
                  </tr>
                </table>
              </td>  
            </tr>

            <tr>  
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="95%">
                      Historical alerts:</td>
                    <td width="5%" align="right" nowrap="nowrap"><b><xsl:value-of select="$historicalAlert" /></b></td>
                  </tr>
                </table>
              </td>  
            </tr>

            <tr>  
              <td height="20">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="95%">
                      Possible OFAC matches:</td>
                    <td width="5%" align="right" nowrap="nowrap"><b><xsl:value-of select="$OFACAlert" /></b></td>
                  </tr>
                </table>
              </td>  
            </tr>

          </table>
        </td>
      </tr>

    </table>

  </xsl:template>

</xsl:stylesheet>