
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rsp="http://www.experian.com/NetConnectResponse"
                xmlns:prd="http://www.experian.com/ARFResponse">

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


<!--
*****************************************************
* CFIBusinessAuthenticationDetails template
*****************************************************
-->
 <xsl:template name="CFIBusinessAuthenticationDetails">        
  <xsl:param name="AuthenticationType" />

  <!-- match ident   -->
  <xsl:variable name="matchIdent">
    <xsl:call-template name="CFIBusinessMatch" >
    </xsl:call-template>
  </xsl:variable>

  <xsl:variable name="processIdent1">
    <xsl:choose>
      <xsl:when test="normalize-space($matchIdent) = 'B1A1' or normalize-space($matchIdent) = 'B1A2' ">
        <xsl:value-of select="'B1A1'" />
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="'B2A1'" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="processIdent2">
    <xsl:choose>
      <xsl:when test="normalize-space($matchIdent) = 'B1A1' or normalize-space($matchIdent) = 'B1A2' ">
        <xsl:value-of select="'B1A2'" />
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="'B2A2'" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!-- Business Authentication Details -->
  <xsl:apply-templates select="prd:GeneralSummary">
     <xsl:with-param name="AuthenticationIdent" select="$processIdent1" />
     <xsl:with-param name="AuthenticationType" select="$AuthenticationType" />
  </xsl:apply-templates>

  <xsl:if test="prd:GeneralSummary[@ident='B1A2']/prd:ResponseType != ''">
     <xsl:apply-templates select="prd:GeneralSummary">
        <xsl:with-param name="AuthenticationIdent" select="$processIdent2" />
        <xsl:with-param name="AuthenticationType" select="$AuthenticationType" />
     </xsl:apply-templates>
  </xsl:if>

  <xsl:if test="prd:HistoricalValidation">
     <xsl:call-template name="HistoricalMatchDetail">
        <xsl:with-param name="HistoricalIdent" select="$matchIdent" />
     </xsl:call-template>
  </xsl:if>

 </xsl:template>


<!--
*****************************************************
* General summary template of 
* authentication details
*****************************************************
-->
 <xsl:template match="prd:GeneralSummary">        
  <xsl:param name="AuthenticationIdent" />
  <xsl:param name="AuthenticationType" />

  <!-- bus name address template builds full name and address -->
  <xsl:variable name="busNameAddress">
    <xsl:call-template name="CFIBusinessNameAddress" >
       <xsl:with-param name="VerificationIdent" select="$AuthenticationIdent" />
       <xsl:with-param name="request" select="'NAMEADDRCSZ'" />
    </xsl:call-template>
  </xsl:variable>

 <xsl:if test="./@ident = $AuthenticationIdent">

   <xsl:if test="$AuthenticationType = 'BUSINESS'" >
     <table cellpadding="0" cellspacing="0" border="0" width="100%">
       <tr>
         <td height="20" valign="top"><font color="#193385" size="3">
           <b>Authentication Details</b></font><br />
           <b><xsl:value-of select="$busNameAddress" /></b>
         </td>
       </tr>
     </table>

     <table width="100%" border="0" cellspacing="0" cellpadding="0">
       <tr>
         <td>
           <xsl:call-template name="HighRiskAddress" >
              <xsl:with-param name="AddrHiRiskDescsIdent" select="$AuthenticationIdent" />
           </xsl:call-template>

           <xsl:call-template name="BusinessAddressDetail" >
              <xsl:with-param name="BusAddrDetIdent" select="$AuthenticationIdent" />
           </xsl:call-template>

           <xsl:call-template name="HighRiskPhonel" >
              <xsl:with-param name="PhnHiRiskDescsIdent" select="$AuthenticationIdent" />
           </xsl:call-template>

           <xsl:call-template name="BusinessPhoneDetail" >
              <xsl:with-param name="BusPhoneDetIdent" select="$AuthenticationIdent" />
           </xsl:call-template>

           <xsl:call-template name="OFACValidation" >
              <xsl:with-param name="OFACIdent" select="$AuthenticationIdent" />
           </xsl:call-template>

           <xsl:call-template name="TaxIDDetail" >
              <xsl:with-param name="TaxIDDetIdent" select="$AuthenticationIdent" />
           </xsl:call-template>
         </td>
        </tr>
     </table>
   </xsl:if>
 </xsl:if> 
 </xsl:template>


<!--
*****************************************************
* HistoricalMatchDetail template 
*****************************************************
-->
 <xsl:template name="HistoricalMatchDetail">        
  <xsl:param name="HistoricalIdent" />

  <!-- begin Historical Match Detail -->
  <table width="100%" border="0" cellspacing="0" cellpadding="1">
    <tr>
      <td bgcolor="#0099cc">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td bgcolor="#ffffff">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">

                <tr>
                  <td bgcolor="#0099cc" width="1%">
                    <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

                  <td bgcolor="#0099cc" width="99%" align="left" valign="middle" height="20">
                    <b><font color="#ffffff">
                      Historical Match Detail</font></b></td>
                  <td bgcolor="#0099cc" width="1%">
                    <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>
                </tr>

                <!-- space row -->
                <tr>
                  <td colspan="2" style="line-height:5px;">&#160;</td>
                </tr>

                <tr>
                  <td>
                    <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

                  <td bgcolor="#ffffff">

                     <xsl:apply-templates select="prd:HistoricalValidation" >
                        <xsl:with-param name="HistoricalIdent" select="$HistoricalIdent" />
                     </xsl:apply-templates>

                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

  <!-- Back to Top grapic -->
  <xsl:call-template name="BackToTop" />

 </xsl:template>


<!--
*****************************************************
* HistoricalValidation template 
*****************************************************
-->
<xsl:template match="prd:HistoricalValidation">
  <xsl:param name="HistoricalIdent" />

  <!-- aTag -->
  <xsl:variable name="aTag">
      <xsl:value-of select="concat('#Historical_', position() - 1)" />
  </xsl:variable>

  <!-- bus name -->
  <xsl:variable name="busName">
      <xsl:value-of select="prd:PreviousBusinessApplicant/prd:BusName" />
  </xsl:variable>

  <!-- dba -->
  <xsl:variable name="dba">
      <xsl:value-of select="prd:PreviousBusinessApplicant/prd:AlternateName" />
  </xsl:variable>

  <!-- street address -->
  <xsl:variable name="addr">
      <xsl:value-of select="prd:PreviousBusinessApplicant/prd:PrimaryAddress/prd:Addr" />
  </xsl:variable>

  <!-- city, state, zip-->
  <xsl:variable name="cityStateZip">
      <xsl:value-of select="concat(normalize-space(prd:PreviousBusinessApplicant/prd:PrimaryAddress/prd:City), ', ', normalize-space(prd:PreviousBusinessApplicant/prd:PrimaryAddress/prd:State), '  ', normalize-space(prd:PreviousBusinessApplicant/prd:PrimaryAddress/prd:Zip)  )" />
  </xsl:variable>

  <!-- Alt street address -->
  <xsl:variable name="AltAddr">
      <xsl:value-of select="prd:PreviousBusinessApplicant/prd:AlternateAddress/prd:Addr" />
  </xsl:variable>

  <!-- Alt city, state, zip-->
  <xsl:variable name="AltCityStateZip">
      <xsl:value-of select="concat(normalize-space(prd:PreviousBusinessApplicant/prd:AlternateAddress/prd:City), ', ', normalize-space(prd:PreviousBusinessApplicant/prd:AlternateAddress/prd:State), '  ', normalize-space(prd:PreviousBusinessApplicant/prd:AlternateAddress/prd:Zip)  )" />
  </xsl:variable>

  <!-- match name -->
   <xsl:variable name="matchName">
    <xsl:choose>
      <xsl:when test="substring($HistoricalIdent, 1, 2) = 'B1' ">
        <xsl:value-of select="$busName" />
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="$dba" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!-- match street addr -->
   <xsl:variable name="matchAddr">
    <xsl:choose>
      <xsl:when test="substring($HistoricalIdent, 3, 2) = 'A1' ">
        <xsl:value-of select="$addr" />
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="$AltAddr" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!-- match city state zip -->
   <xsl:variable name="matchCityStateZip">
    <xsl:choose>
      <xsl:when test="substring($HistoricalIdent, 3, 2) = 'A1' ">
        <xsl:value-of select="$cityStateZip" />
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="$AltCityStateZip" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!-- match alt street addr -->
   <xsl:variable name="matchAltAddr">
    <xsl:choose>
      <xsl:when test="substring($HistoricalIdent, 3, 2) = 'A1' ">
        <xsl:value-of select="$AltAddr" />
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="$addr" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!-- match alt city state zip -->
   <xsl:variable name="matchAltCityStateZip">
    <xsl:choose>
      <xsl:when test="substring($HistoricalIdent, 3, 2) = 'A1' ">
        <xsl:value-of select="$AltCityStateZip" />
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="$cityStateZip" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!-- #3 street address -->
  <xsl:variable name="No3Addr">
      <xsl:value-of select="prd:PreviousBusinessApplicant/prd:ThirdAddress/prd:Addr" />
  </xsl:variable>

  <!-- #3 city, state, zip-->
  <xsl:variable name="No3CityStateZip">
      <xsl:value-of select="concat(normalize-space(prd:PreviousBusinessApplicant/prd:ThirdAddress/prd:City), ', ', normalize-space(prd:PreviousBusinessApplicant/prd:ThirdAddress/prd:State), '  ', normalize-space(prd:PreviousBusinessApplicant/prd:ThirdAddress/prd:Zip)  )" />
  </xsl:variable>

  <!-- #4 street address -->
  <xsl:variable name="No4Addr">
      <xsl:value-of select="prd:PreviousBusinessApplicant/prd:FourthAddress/prd:Addr" />
  </xsl:variable>

  <!-- #4 city, state, zip-->
  <xsl:variable name="No4CityStateZip">
      <xsl:value-of select="concat(normalize-space(prd:PreviousBusinessApplicant/prd:FourthAddress/prd:City), ', ', normalize-space(prd:PreviousBusinessApplicant/prd:FourthAddress/prd:State), '  ', normalize-space(prd:PreviousBusinessApplicant/prd:FourthAddress/prd:Zip)  )" />
  </xsl:variable>

  <!-- phone -->
   <xsl:variable name="phone">
    <xsl:choose>
      <xsl:when test="prd:PreviousBusinessApplicant/prd:Phone/prd:PhnNbr and string-length(normalize-space(prd:PreviousBusinessApplicant/prd:Phone/prd:PhnNbr)) = 10">
        <xsl:value-of select="concat('(', substring(prd:PreviousBusinessApplicant/prd:Phone/prd:PhnNbr, 1, 3), ') ', substring(prd:PreviousBusinessApplicant/prd:Phone/prd:PhnNbr, 4, 3), '-', substring(prd:PreviousBusinessApplicant/prd:Phone/prd:PhnNbr, 7))" />
      </xsl:when>

      <xsl:when test="prd:PreviousBusinessApplicant/prd:Phone/prd:PhnNbr and string-length(normalize-space(prd:PreviousBusinessApplicant/prd:Phone/prd:PhnNbr)) = 7">
        <xsl:value-of select="concat(substring(prd:PreviousBusinessApplicant/prd:Phone/prd:PhnNbr, 1, 3), '-', substring(prd:PreviousBusinessApplicant/prd:Phone/prd:PhnNbr, 4))" />
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="''" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!-- taxID -->
  <xsl:variable name="taxID">
      <xsl:value-of select="concat(substring(prd:PreviousBusinessApplicant/prd:TaxID, 1, 2), '-', substring(prd:PreviousBusinessApplicant/prd:TaxID, 3)) " />
  </xsl:variable>

  <!-- principle count -->
  <xsl:variable name="prinCount">
     <xsl:value-of select="count(prd:PreviousBusinessPrincipal) + count(prd:PreviousSecondaryPrincipal) " />
  </xsl:variable>

  <!-- principle 1 name -->
  <xsl:variable name="prin1Name">
    <xsl:choose>
      <xsl:when test="prd:PreviousBusinessPrincipal/prd:PrincipalName and normalize-space(prd:PreviousBusinessPrincipal/prd:PrincipalName/prd:MI) != '' ">
        <xsl:value-of select="concat(normalize-space(prd:PreviousBusinessPrincipal/prd:PrincipalName/prd:FirstName), ' ', normalize-space(prd:PreviousBusinessPrincipal/prd:PrincipalName/prd:MI), '. ', normalize-space(prd:PreviousBusinessPrincipal/prd:PrincipalName/prd:LastName))" />
      </xsl:when>

      <xsl:when test="prd:PreviousBusinessPrincipal/prd:PrincipalName and normalize-space(prd:PreviousBusinessPrincipal/prd:PrincipalName/prd:MI) = '' ">
        <xsl:value-of select="concat(normalize-space(prd:PreviousBusinessPrincipal/prd:PrincipalName/prd:FirstName), ' ', normalize-space(prd:PreviousBusinessPrincipal/prd:PrincipalName/prd:LastName))" />
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="''" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!-- principle 1 ssn -->
  <xsl:variable name="prin1SSN">
    <xsl:choose>
      <xsl:when test="prd:PreviousBusinessPrincipal/prd:SSN and normalize-space(prd:PreviousBusinessPrincipal/prd:SSN) != '' ">
        <xsl:value-of select="concat('XXX-XX-', substring(normalize-space(prd:PreviousBusinessPrincipal/prd:SSN), 6))" />
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="''" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

    <!-- principle 1 street address -->
    <xsl:variable name="prin1StreetAddress">
      <xsl:choose>
        <xsl:when test="prd:PreviousBusinessPrincipal/prd:PrincipalAddress/prd:Addr and normalize-space(prd:PreviousBusinessPrincipal/prd:PrincipalAddress/prd:Addr) != '' ">
          <xsl:value-of select="normalize-space(prd:PreviousBusinessPrincipal/prd:PrincipalAddress/prd:Addr)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- principle 1 city -->
    <xsl:variable name="prin1City">
      <xsl:choose>
        <xsl:when test="prd:PreviousBusinessPrincipal/prd:PrincipalAddress/prd:City and normalize-space(prd:PreviousBusinessPrincipal/prd:PrincipalAddress/prd:City) != '' ">
          <xsl:value-of select="normalize-space(prd:PreviousBusinessPrincipal/prd:PrincipalAddress/prd:City)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- principle 1 state -->
    <xsl:variable name="prin1State">
      <xsl:choose>
        <xsl:when test="prd:PreviousBusinessPrincipal/prd:PrincipalAddress/prd:State and normalize-space(prd:PreviousBusinessPrincipal/prd:PrincipalAddress/prd:State) != '' ">
          <xsl:value-of select="normalize-space(prd:PreviousBusinessPrincipal/prd:PrincipalAddress/prd:State)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- principle 1 zip -->
    <xsl:variable name="prin1Zip">
      <xsl:choose>
        <xsl:when test="prd:PreviousBusinessPrincipal/prd:PrincipalAddress/prd:Zip and normalize-space(prd:PreviousBusinessPrincipal/prd:PrincipalAddress/prd:Zip) != '' ">
          <xsl:value-of select="normalize-space(prd:PreviousBusinessPrincipal/prd:PrincipalAddress/prd:Zip)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
       </xsl:choose>
    </xsl:variable>

    <!-- principle 1 city, state, zip -->
    <xsl:variable name="prin1CityStateZip">
       <xsl:value-of select="concat(normalize-space($prin1City), ', ', normalize-space($prin1State), '  ', normalize-space($prin1Zip)  )" />
    </xsl:variable>

    <!-- principle 1 phone -->
     <xsl:variable name="prin1Phone">
      <xsl:choose>
        <xsl:when test="prd:PreviousBusinessPrincipal/prd:Phone/prd:PhnNbr and normalize-space(prd:PreviousBusinessPrincipal/prd:Phone/prd:PhnNbr) ">
          <xsl:value-of select="normalize-space(prd:PreviousBusinessPrincipal/prd:Phone/prd:PhnNbr) " />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- principle 1 phone formatted -->
     <xsl:variable name="prin1PhoneFormatted">
      <xsl:choose>
        <xsl:when test="$prin1Phone and string-length(normalize-space($prin1Phone)) = 10">
          <xsl:value-of select="concat('(', substring($prin1Phone, 1, 3), ') ', substring($prin1Phone, 4, 3), '-', substring($prin1Phone, 7))" />
        </xsl:when>

        <xsl:when test="$prin1Phone and string-length(normalize-space($prin1Phone)) = 7">
          <xsl:value-of select="concat('(', substring($prin1Phone, 1, 3), '-', substring($prin1Phone, 4))" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- principle 1 drivers license -->
     <xsl:variable name="prin1DL">
      <xsl:choose>
        <xsl:when test="prd:PreviousBusinessPrincipal/prd:DriverLicense and normalize-space(prd:PreviousBusinessPrincipal/prd:DriverLicense/prd:DLNumber) != '' ">
          <xsl:value-of select="concat(normalize-space(prd:PreviousBusinessPrincipal/prd:DriverLicense/prd:DLState), ' ', normalize-space(prd:PreviousBusinessPrincipal/prd:DriverLicense/prd:DLNumber) )" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- principle 1 DOB -->
     <xsl:variable name="prin1DOB">
      <xsl:choose>
        <xsl:when test="prd:PreviousBusinessPrincipal/prd:DOB and normalize-space(prd:PreviousBusinessPrincipal/prd:DOB) != '' and number(substring(prd:PreviousBusinessPrincipal/prd:DOB, 1, 4)) &lt; 1232 ">
	   <xsl:call-template name="FormatDate">
	     <xsl:with-param name="pattern" select="'mo/dt/year'" />
	     <xsl:with-param name="value" select="prd:PreviousBusinessPrincipal/prd:DOB" />
	     <xsl:with-param name="isYearLast" select="true()" />
	   </xsl:call-template>
        </xsl:when>

        <xsl:when test="prd:PreviousBusinessPrincipal/prd:DOB and normalize-space(prd:PreviousBusinessPrincipal/prd:DOB) != '' and number(substring(prd:PreviousBusinessPrincipal/prd:DOB, 5)) &lt; 1232 ">
	   <xsl:call-template name="FormatDate">
	     <xsl:with-param name="pattern" select="'mo/dt/year'" />
	     <xsl:with-param name="value" select="prd:PreviousBusinessPrincipal/prd:DOB" />
	   </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

  <!-- principle 2 name -->
  <xsl:variable name="prin2Name">
    <xsl:choose>
      <xsl:when test="prd:PreviousSecondaryPrincipal/prd:PrincipalName and normalize-space(prd:PreviousSecondaryPrincipal/prd:PrincipalName/prd:MI) != '' ">
        <xsl:value-of select="concat(normalize-space(prd:PreviousSecondaryPrincipal/prd:PrincipalName/prd:FirstName), ' ', normalize-space(prd:PreviousSecondaryPrincipal/prd:PrincipalName/prd:MI), '. ', normalize-space(prd:PreviousSecondaryPrincipal/prd:PrincipalName/prd:LastName))" />
      </xsl:when>

      <xsl:when test="prd:PreviousSecondaryPrincipal/prd:PrincipalName and normalize-space(prd:PreviousSecondaryPrincipal/prd:PrincipalName/prd:MI) = '' ">
        <xsl:value-of select="concat(normalize-space(prd:PreviousSecondaryPrincipal/prd:PrincipalName/prd:FirstName), ' ', normalize-space(prd:PreviousSecondaryPrincipal/prd:PrincipalName/prd:LastName))" />
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="''" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!-- principle 2 ssn -->
  <xsl:variable name="prin2SSN">
    <xsl:choose>
      <xsl:when test="prd:PreviousSecondaryPrincipal/prd:SSN and normalize-space(prd:PreviousSecondaryPrincipal/prd:SSN) != '' ">
        <xsl:value-of select="concat('XXX-XX-', substring(normalize-space(prd:PreviousSecondaryPrincipal/prd:SSN), 6))" />
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="''" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

    <!-- principle 2 street address -->
    <xsl:variable name="prin2StreetAddress">
      <xsl:choose>
        <xsl:when test="prd:PreviousSecondaryPrincipal/prd:PrincipalAddress/prd:Addr and normalize-space(prd:PreviousSecondaryPrincipal/prd:PrincipalAddress/prd:Addr) != '' ">
          <xsl:value-of select="normalize-space(prd:PreviousSecondaryPrincipal/prd:PrincipalAddress/prd:Addr)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- principle 2 city -->
    <xsl:variable name="prin2City">
      <xsl:choose>
        <xsl:when test="prd:PreviousSecondaryPrincipal/prd:PrincipalAddress/prd:City and normalize-space(prd:PreviousSecondaryPrincipal/prd:PrincipalAddress/prd:City) != '' ">
          <xsl:value-of select="normalize-space(prd:PreviousSecondaryPrincipal/prd:PrincipalAddress/prd:City)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- principle 2 state -->
    <xsl:variable name="prin2State">
      <xsl:choose>
        <xsl:when test="prd:PreviousSecondaryPrincipal/prd:PrincipalAddress/prd:State and normalize-space(prd:PreviousSecondaryPrincipal/prd:PrincipalAddress/prd:State) != '' ">
          <xsl:value-of select="normalize-space(prd:PreviousSecondaryPrincipal/prd:PrincipalAddress/prd:State)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- principle 2 zip -->
    <xsl:variable name="prin2Zip">
      <xsl:choose>
        <xsl:when test="prd:PreviousSecondaryPrincipal/prd:PrincipalAddress/prd:Zip and normalize-space(prd:PreviousSecondaryPrincipal/prd:PrincipalAddress/prd:Zip) != '' ">
          <xsl:value-of select="normalize-space(prd:PreviousSecondaryPrincipal/prd:PrincipalAddress/prd:Zip)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
       </xsl:choose>
    </xsl:variable>

    <!-- principle 2 city, state, zip -->
    <xsl:variable name="prin2CityStateZip">
       <xsl:value-of select="concat(normalize-space($prin2City), ', ', normalize-space($prin2State), '  ', normalize-space($prin2Zip)  )" />
    </xsl:variable>

    <!-- principle 2 phone -->
     <xsl:variable name="prin2Phone">
      <xsl:choose>
        <xsl:when test="prd:PreviousSecondaryPrincipal/prd:Phone/prd:PhnNbr and normalize-space(prd:PreviousSecondaryPrincipal/prd:Phone/prd:PhnNbr) ">
          <xsl:value-of select="normalize-space(prd:PreviousSecondaryPrincipal/prd:Phone/prd:PhnNbr) " />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- principle 2 phone formatted -->
     <xsl:variable name="prin2PhoneFormatted">
      <xsl:choose>
        <xsl:when test="$prin2Phone and string-length(normalize-space($prin2Phone)) = 10">
          <xsl:value-of select="concat('(', substring($prin2Phone, 1, 3), ') ', substring($prin2Phone, 4, 3), '-', substring($prin2Phone, 7))" />
        </xsl:when>

        <xsl:when test="$prin2Phone and string-length(normalize-space($prin2Phone)) = 7">
          <xsl:value-of select="concat('(', substring($prin2Phone, 1, 3), '-', substring($prin2Phone, 4))" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- principle 2 drivers license -->
     <xsl:variable name="prin2DL">
      <xsl:choose>
        <xsl:when test="prd:PreviousSecondaryPrincipal/prd:DriverLicense and normalize-space(prd:PreviousSecondaryPrincipal/prd:DriverLicense/prd:DLNumber) != '' ">
          <xsl:value-of select="concat(normalize-space(prd:PreviousSecondaryPrincipal/prd:DriverLicense/prd:DLState), ' ', normalize-space(prd:PreviousSecondaryPrincipal/prd:DriverLicense/prd:DLNumber) )" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- principle 2 DOB -->
     <xsl:variable name="prin2DOB">
      <xsl:choose>
        <xsl:when test="prd:PreviousSecondaryPrincipal/prd:DOB and normalize-space(prd:PreviousSecondaryPrincipal/prd:DOB) != '' ">
	   <xsl:call-template name="FormatDate">
	     <xsl:with-param name="pattern" select="'mo/dt/year'" />
	     <xsl:with-param name="value" select="prd:PreviousSecondaryPrincipal/prd:DOB" />
	   </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

  <!-- matched data -->
   <xsl:variable name="matchedData">
    <xsl:choose>
      <xsl:when test="normalize-space(prd:MatchIndicators/prd:BusNameMatch) = 'Y' ">
        <xsl:value-of select="'True'" />
      </xsl:when>

      <xsl:when test="normalize-space(prd:MatchIndicators/prd:BusAddrMatch) = 'Y' ">
        <xsl:value-of select="'True'" />
      </xsl:when>

      <xsl:when test="normalize-space(prd:MatchIndicators/prd:AlternateNameMatch) = 'Y' ">
        <xsl:value-of select="'True'" />
      </xsl:when>

      <xsl:when test="normalize-space(prd:MatchIndicators/prd:AlternateAddrMatch) = 'Y' ">
        <xsl:value-of select="'True'" />
      </xsl:when>

      <xsl:when test="normalize-space(prd:MatchIndicators/prd:BusPhnNbrMatch) = 'Y' ">
        <xsl:value-of select="'True'" />
      </xsl:when>

      <xsl:when test="normalize-space(prd:MatchIndicators/prd:TaxIDMatch) = 'Y' ">
        <xsl:value-of select="'True'" />
      </xsl:when>

      <xsl:when test="normalize-space(prd:MatchIndicators/prd:AddlAddrMatch) = 'Y' ">
        <xsl:value-of select="'True'" />
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="'False'" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <!-- header row -->
    <tr>
      <!-- left column -->
      <td width="50%" valign="top">
        <a name="{normalize-space($aTag)}"></a>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">

          <!-- app ID -->
          <tr>
            <td width="40%">
              <font size="1" style="FONT-FAMILY: 'verdana';"><b>Previous application ID:</b></font></td>
            <td width="60%">
              <font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="prd:AppID" /></font></td>
          </tr>

          <!-- Region -->
          <tr>
            <td width="40%">
              <font size="1" style="FONT-FAMILY: 'verdana';"><b>Region:</b></font></td>
            <td width="60%">
              <font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="prd:RegionID" /></font></td>
          </tr>
        </table>
      </td>

      <!-- right column -->
      <td width="50%" valign="top">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">

          <!-- Date ordered -->
          <tr>
            <td width="40%">
              <font size="1" style="FONT-FAMILY: 'verdana';"><b>Date ordered:</b></font></td>
            <td width="60%">
              <font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="substring(prd:CreationDate,5,2)" />/<xsl:value-of select="substring(prd:CreationDate,7,2)" />/<xsl:value-of select="substring(prd:CreationDate,1,4)" />
              </font>
            </td>
          </tr>

          <!-- User ID -->
          <tr>
            <td width="40%">
              <font size="1" style="FONT-FAMILY: 'verdana';"><b>User ID:</b></font></td>
            <td width="60%">
              <font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="prd:UserID" /></font></td>
          </tr>
        </table>
      </td>
    </tr>
    <!-- end head row -->

    <xsl:if test="prd:PreviousBusinessApplicant/prd:BusName != ''">
      <!-- space row -->
      <tr>
        <td colspan="2" style="line-height:5px;">&#160;</td>
      </tr>

      <!-- business block -->
      <tr>
        <td colspan="2" align="left"><font size="1" style="FONT-FAMILY: 'verdana';"><b><xsl:value-of select="$busName" /></b></font></td>
      </tr>
    </xsl:if>

    <tr>
      <!-- DBA etc column -->
      <td width="50%" valign="top">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">

        <xsl:if test="prd:PreviousBusinessApplicant/prd:AlternateName != ''">
          <tr>
            <td width="40%">
              <font size="1" style="FONT-FAMILY: 'verdana';">DBA:</font></td>
            <td width="60%">
              <font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$dba" /></font></td>
          </tr>

          <!-- space row -->
          <tr>
            <td colspan="2" style="line-height:5px;">&#160;</td>
          </tr>
        </xsl:if>

        <tr>
          <td width="40%" valign="top">
            <font size="1" style="FONT-FAMILY: 'verdana';">Primary address:</font></td>
          <td width="60%">
            <font size="1" style="FONT-FAMILY: 'verdana';">
              <xsl:value-of select="$addr" /><br />
              <xsl:value-of select="$cityStateZip" />
              <xsl:if test="normalize-space($phone) != '' "><br /><xsl:value-of select="$phone" /></xsl:if>
            </font>
          </td>
        </tr>

        <xsl:if test="prd:PreviousBusinessApplicant/prd:ThirdAddress != ''">
          <!-- space row -->
          <tr>
            <td colspan="2" style="line-height:5px;">&#160;</td>
          </tr>

          <tr>
            <td width="40%" valign="top">
              <font size="1" style="FONT-FAMILY: 'verdana';">Address #3:</font></td>
            <td width="60%">
              <font size="1" style="FONT-FAMILY: 'verdana';">
                <xsl:value-of select="$No3Addr" />
                <br />
                <xsl:value-of select="$No3CityStateZip" />
              </font>
            </td>
          </tr>
        </xsl:if>
        </table>
      </td>
      <!-- end DBA etc column -->

      <!-- tax ID etc column -->
      <td width="50%" valign="top">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <xsl:if test="prd:PreviousBusinessApplicant/prd:TaxID != ''">
            <tr>
              <td width="40%">
                <font size="1" style="FONT-FAMILY: 'verdana';">Tax ID:</font></td>
              <td width="60%">
                <font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$taxID" /></font></td>
            </tr>
          </xsl:if>

          <xsl:if test="prd:PreviousBusinessApplicant/prd:AlternateAddress != ''">
            <xsl:if test="prd:PreviousBusinessApplicant/prd:TaxID != ''">
              <!-- space row -->
              <tr>
                <td colspan="2" style="line-height:5px;">&#160;</td>
              </tr>
            </xsl:if>

              <tr>
                <td width="40%" valign="top">
                  <font size="1" style="FONT-FAMILY: 'verdana';">Alternate address:</font></td>
                <td width="60%">
                  <font size="1" style="FONT-FAMILY: 'verdana';">
                    <xsl:value-of select="$AltAddr" />
                    <br />
                    <xsl:value-of select="$AltCityStateZip" />
                  </font>
                </td>
              </tr>
          </xsl:if>

          <xsl:if test="prd:PreviousBusinessApplicant/prd:FourthAddress != ''">
            <xsl:if test="prd:PreviousBusinessApplicant/prd:TaxID != '' or prd:PreviousBusinessApplicant/prd:AlternateAddress != '' ">
              <!-- space row -->
              <tr>
                <td colspan="2" style="line-height:5px;">&#160;</td>
              </tr>
            </xsl:if>

              <tr>
                <td width="40%" valign="top">
                  <font size="1" style="FONT-FAMILY: 'verdana';">Address #4:</font></td>
                <td width="60%">
                  <font size="1" style="FONT-FAMILY: 'verdana';">
                    <xsl:value-of select="$No4Addr" />
                    <br />
                    <xsl:value-of select="$No4CityStateZip" />
                  </font>
                </td>
              </tr>
          </xsl:if>

          <xsl:if test="not (prd:PreviousBusinessApplicant/prd:TaxID) and not (prd:PreviousBusinessApplicant/prd:AlternateAddress) and not (prd:PreviousBusinessApplicant/prd:FourthAddress) ">
            <tr>
              <td width="40%" valign="top">
                <font size="1" style="FONT-FAMILY: 'verdana';"><xsl:text disable-output-escaping="yes">&#160;</xsl:text></font></td>
              <td width="60%">
                <font size="1" style="FONT-FAMILY: 'verdana';"><xsl:text disable-output-escaping="yes">&#160;</xsl:text></font></td>
            </tr>
          </xsl:if>
        </table>
      </td>
      <!-- end tax ID etc column -->

    </tr>
    <!-- end business block -->

    <xsl:if test="$prinCount &gt; 0">
      <!-- space row -->
       <tr>
         <td colspan="2" style="line-height:5px;">&#160;</td>
       </tr>

       <xsl:choose>
         <xsl:when test="$prinCount = 2">
           <!-- principal #1 name -->
           <tr>
             <td colspan="2" align="left"><font size="1" style="FONT-FAMILY: 'verdana';"><b>Business Principal #1</b></font></td>
           </tr>
         </xsl:when>

         <xsl:otherwise>
           <tr>
             <td colspan="2" align="left"><font size="1" style="FONT-FAMILY: 'verdana';"><b><xsl:value-of select="$prin1Name" /></b></font></td>
           </tr>
         </xsl:otherwise>
       </xsl:choose>

       <tr>
         <!-- address column -->
         <td width="50%" valign="top">
           <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr>
               <td width="40%" valign="top">
                 <xsl:text disable-output-escaping="yes">&#160;</xsl:text>
               </td>
               <td width="60%">
                 <font size="1" style="FONT-FAMILY: 'verdana';">
                   <xsl:if test="$prinCount &gt; 1"><xsl:value-of select="$prin1Name" /><br /></xsl:if>
                   <xsl:if test="$prin1StreetAddress"><xsl:value-of select="$prin1StreetAddress" /><br /></xsl:if>
                   <xsl:if test="$prin1CityStateZip"><xsl:value-of select="$prin1CityStateZip" /><br /></xsl:if>
                   <xsl:if test="$prin1PhoneFormatted"><xsl:value-of select="$prin1PhoneFormatted" /><br /></xsl:if>
                 </font>
               </td>
             </tr>
           </table>
         </td>
         <!-- end address column -->

         <!-- SSN etc column -->
         <td width="50%" valign="top">
           <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <xsl:if test="normalize-space($prin1SSN) != '' ">
               <tr>
                 <td width="40%">
                   <font size="1" style="FONT-FAMILY: 'verdana';">
                     SSN:
                   </font>
                 </td>
                 <td width="60%">
                   <font size="1" style="FONT-FAMILY: 'verdana';">
                     <xsl:value-of select="$prin1SSN" />
                   </font>
                 </td>
               </tr>
             </xsl:if>

             <xsl:if test="normalize-space($prin1DL) != '' ">
               <tr>
                 <td width="40%">
                   <font size="1" style="FONT-FAMILY: 'verdana';">
                     DL#:
                   </font>
                 </td>
                 <td width="60%">
                   <font size="1" style="FONT-FAMILY: 'verdana';">
                     <xsl:value-of select="$prin1DL" />
                   </font>
                 </td>
               </tr>
             </xsl:if>

             <xsl:if test="normalize-space($prin1DOB) != '' ">
               <tr>
                 <td width="40%">
                   <font size="1" style="FONT-FAMILY: 'verdana';">
                     DOB:
                   </font>
                 </td>
                 <td width="60%">
                   <font size="1" style="FONT-FAMILY: 'verdana';">
                     <xsl:value-of select="$prin1DOB" />
                   </font>
                 </td>
               </tr>
             </xsl:if>
           </table>
         </td>
         <!-- end SSN etc column -->

       </tr>
       <!-- end principal #1> -->
    </xsl:if>

    <xsl:if test="$prinCount &gt; 1">
      <!-- space row -->
       <tr>
         <td colspan="2" style="line-height:5px;">&#160;</td>
       </tr>

       <tr>
         <td colspan="2" align="left"><font size="1" style="FONT-FAMILY: 'verdana';"><b>Business Principal #2</b></font></td>
       </tr>

       <tr>
         <!-- address column -->
         <td width="50%" valign="top">
           <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr>
               <td width="40%" valign="top">
                 <xsl:text disable-output-escaping="yes">&#160;</xsl:text>
               </td>
               <td width="60%">
                 <font size="1" style="FONT-FAMILY: 'verdana';">
                   <xsl:if test="$prin2Name"><xsl:value-of select="$prin2Name" /><br /></xsl:if>
                   <xsl:if test="$prin2StreetAddress"><xsl:value-of select="$prin2StreetAddress" /><br /></xsl:if>
                   <xsl:if test="$prin2CityStateZip"><xsl:value-of select="$prin2CityStateZip" /><br /></xsl:if>
                   <xsl:if test="$prin2PhoneFormatted"><xsl:value-of select="$prin2PhoneFormatted" /><br /></xsl:if>
                 </font>
               </td>
             </tr>
           </table>
         </td>
         <!-- end address column -->

         <!-- SSN etc column -->
         <td width="50%" valign="top">
           <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <xsl:if test="normalize-space($prin2SSN) != '' ">
               <tr>
                 <td width="40%">
                   <font size="1" style="FONT-FAMILY: 'verdana';">
                     SSN:
                   </font>
                 </td>
                 <td width="60%">
                   <font size="1" style="FONT-FAMILY: 'verdana';">
                     <xsl:value-of select="$prin2SSN" />
                   </font>
                 </td>
               </tr>
             </xsl:if>

             <xsl:if test="normalize-space($prin2DL) != '' ">
               <tr>
                 <td width="40%">
                   <font size="1" style="FONT-FAMILY: 'verdana';">
                     DL#:
                   </font>
                 </td>
                 <td width="60%">
                   <font size="1" style="FONT-FAMILY: 'verdana';">
                     <xsl:value-of select="$prin2DL" />
                   </font>
                 </td>
               </tr>
             </xsl:if>

             <xsl:if test="normalize-space($prin2DOB) != '' ">
               <tr>
                 <td width="40%">
                   <font size="1" style="FONT-FAMILY: 'verdana';">
                     DOB:
                   </font>
                 </td>
                 <td width="60%">
                   <font size="1" style="FONT-FAMILY: 'verdana';">
                     <xsl:value-of select="$prin2DOB" />
                   </font>
                 </td>
               </tr>
             </xsl:if>
           </table>
         </td>
         <!-- end SSN etc column -->

       </tr>
       <!-- end principal #2 -->
    </xsl:if>

    <xsl:if test="normalize-space($matchedData) = 'True'">
      <!-- space row -->
       <tr>
         <td colspan="2" style="line-height:5px;">&#160;</td>
       </tr>

       <tr>
         <td colspan="2" align="left"><font size="1" style="FONT-FAMILY: 'verdana';"><b>Data matched from current application</b></font></td>
       </tr>

       <!-- matched data bloack -->
       <tr>
         <!-- left column -->
         <td colspan="2" valign="top">
           <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <xsl:if test="normalize-space(prd:MatchIndicators/prd:BusNameMatch) = 'Y'">
                <tr>
                  <td width="20%" valign="top">
                    <font size="1" style="FONT-FAMILY: 'verdana';">Business Name:</font></td>
                  <td width="80%">
                    <font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$matchName" /></font></td>
                </tr>
             </xsl:if>

             <xsl:if test="normalize-space(prd:MatchIndicators/prd:AlternateNameMatch) = 'Y'">
                <tr>
                  <td width="20%" valign="top">
                    <font size="1" style="FONT-FAMILY: 'verdana';">DBA Name:</font></td>
                  <td width="80%">
                    <font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$dba" /></font></td>
                </tr>
             </xsl:if>

             <xsl:if test="normalize-space(prd:MatchIndicators/prd:BusAddrMatch) = 'Y'">
                <tr>
                  <td width="20%" valign="top">
                    <font size="1" style="FONT-FAMILY: 'verdana';">Business Address:</font></td>
                  <td width="80%">
                    <font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="concat(normalize-space($matchAddr), ', ', normalize-space($matchCityStateZip))" /></font></td>
                </tr>
             </xsl:if>

             <xsl:if test="normalize-space(prd:MatchIndicators/prd:AlternateAddrMatch) = 'Y'">
                <tr>
                  <td width="20%" valign="top">
                    <font size="1" style="FONT-FAMILY: 'verdana';">Business Address:</font></td>
                  <td width="80%">
                    <font size="1" style="FONT-FAMILY: 'verdana';">
		       <xsl:choose>
		         <xsl:when test="normalize-space($matchAltAddr) = '' ">
		           <xsl:value-of select="concat(normalize-space($addr), ', ', normalize-space($cityStateZip))" />
		         </xsl:when>

		         <xsl:otherwise>
		           <xsl:value-of select="concat(normalize-space($matchAltAddr), ', ', normalize-space($matchAltCityStateZip))" />
		         </xsl:otherwise>
		       </xsl:choose>
                    </font></td>
                </tr>
             </xsl:if>

             <xsl:if test="normalize-space(prd:MatchIndicators/prd:AddlAddrMatch) = 'Y' and prd:PreviousBusinessApplicant/prd:ThirdAddress != '' ">
                <tr>
                  <td width="20%" valign="top">
                    <font size="1" style="FONT-FAMILY: 'verdana';">Business Address:</font></td>
                  <td width="80%">
                    <font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="concat(normalize-space($No3Addr), ', ', normalize-space($No3CityStateZip))" /></font></td>
                </tr>
             </xsl:if>

             <xsl:if test="normalize-space(prd:MatchIndicators/prd:AddlAddrMatch) = 'Y' and prd:PreviousBusinessApplicant/prd:FourthAddress != '' ">
                <tr>
                  <td width="20%" valign="top">
                    <font size="1" style="FONT-FAMILY: 'verdana';">Business Address:</font></td>
                  <td width="80%">
                    <font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="concat(normalize-space($No4Addr), ', ', normalize-space($No4CityStateZip))" /></font></td>
                </tr>
             </xsl:if>

             <xsl:if test="normalize-space(prd:MatchIndicators/prd:BusPhnNbrMatch) = 'Y'">
                <tr>
                  <td width="20%" valign="top">
                    <font size="1" style="FONT-FAMILY: 'verdana';">Business Phone:</font></td>
                  <td width="80%">
                    <font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$phone" /></font></td>
                </tr>
             </xsl:if>

             <xsl:if test="normalize-space(prd:MatchIndicators/prd:TaxIDMatch) = 'Y'">
                <tr>
                  <td width="20%" valign="top">
                    <font size="1" style="FONT-FAMILY: 'verdana';">Tax Id:</font></td>
                  <td width="80%">
                    <font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$taxID" /></font></td>
                </tr>
             </xsl:if>
           </table>
         </td>
         <!-- end left column -->
         <!-- right column -->
         <!-- td width="50%" valign="top">
           <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr>
               <td width="40%" valign="top">
                 <font size="1" style="FONT-FAMILY: 'verdana';">
                   &#160;
                 </font>
               </td>
               <td width="60%">
                 <font size="1" style="FONT-FAMILY: 'verdana';">
                  &#160;
                 </font>
               </td>
             </tr>
           </table>
         </td -->
         <!-- end right column -->

       </tr>
       <!-- end matched data -->

       <xsl:if test="position() != last()">
         <!-- space row -->
         <tr>
           <td colspan="2" style="line-height:5px;">&#160;</td>
         </tr>

         <!-- divider line -->
         <tr>
           <td style="line-height:1px;" bgcolor="#0099cc" colspan="2">&#160;</td>
         </tr>
       </xsl:if>
       <!-- space row -->
       <tr>
         <td colspan="2" style="line-height:5px;">&#160;</td>
       </tr>
    </xsl:if>

  </table>
</xsl:template>


<!--
*****************************************************
* Address high risk template
*****************************************************
-->
<xsl:template name="HighRiskAddress">        
 <xsl:param name="AddrHiRiskDescsIdent" />

    <!-- bus address template builds full address -->
    <xsl:variable name="busAddress">
      <xsl:call-template name="CFIBusinessNameAddress" >
         <xsl:with-param name="VerificationIdent" select="$AddrHiRiskDescsIdent" />
         <xsl:with-param name="request" select="'ADDRCSZ'" />
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="msg">
     <xsl:choose>
       <xsl:when test="number(../prd:CountsSummary[@ident=$AddrHiRiskDescsIdent]/prd:AddrHiRiskCodes) &gt; 0">
         <xsl:value-of select="concat('Address High-risk Result: ', ../prd:AddrHiRiskDescs/prd:HighRiskDesc)" />
       </xsl:when>

       <xsl:when test="./@ident = $AddrHiRiskDescsIdent and normalize-space(../prd:VerifySummary[@ident=$AddrHiRiskDescsIdent]/prd:AddrCode) = 'N/A' ">
         <xsl:value-of select="'N/A'" />
       </xsl:when>

       <xsl:otherwise>
         <xsl:value-of select="'No high-risk address indicator identified.'" />
       </xsl:otherwise>
     </xsl:choose>
   </xsl:variable>

    <xsl:if test="./@ident=$AddrHiRiskDescsIdent">
    
       <xsl:if test="substring(./@ident, 3, 2) = 'A1'">
          <a name="#BUSHIGHRISKADDRB1A1"></a>
       </xsl:if>
       <xsl:if test="substring(./@ident, 3, 2) = 'A2'">
          <a name="#BUSHIGHRISKADDRB1A2"></a>
       </xsl:if>

       <!-- begin  High-risk Address Indicators -->
       <table width="100%" border="0" cellspacing="0" cellpadding="1">
         <tr>
           <td bgcolor="#0099cc">
             <table width="100%" border="0" cellspacing="0" cellpadding="0">
               <tr>
                 <td bgcolor="#ffffff">
                   <table width="100%" border="0" cellspacing="0" cellpadding="0">

                     <tr>
                       <td bgcolor="#0099cc" width="1%">
                         <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

                       <td bgcolor="#0099cc" width="99%" align="left" valign="middle" height="20">
                         <b><font color="#ffffff">
                           High-risk Address Indicators - <xsl:value-of select="$busAddress" /></font></b></td>
                     </tr>

                     <!-- space row -->
                     <tr>
                       <td colspan="2" style="line-height:5px;">&#160;</td>
                     </tr>  

                     <tr>
                       <td>
                         <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

                       <td bgcolor="#ffffff">
                         <table width="100%" border="0" cellspacing="0" cellpadding="0">

                           <tr>
                             <td align="left" valign="top" colspan="2">
                               <font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$msg" /></font></td>
                           </tr>

                           <!-- space row -->
                           <tr>
                             <td colspan="2" style="line-height:5px;">&#160;</td>
                           </tr>

                           <xsl:apply-templates select="../prd:AddrHiRiskCodes">
                             <xsl:with-param name="AddrHiRiskCodesIdent" select="$AddrHiRiskDescsIdent" />
                           </xsl:apply-templates>

                         </table>  
                       </td>
                     </tr>
                   </table>
                 </td>      
               </tr>
             </table>
           </td>
         </tr>
       </table>
       <!-- end High-risk Address Indicators -->

       <!-- Back to Top grapic -->
       <xsl:call-template name="BackToTop" />

    </xsl:if>
</xsl:template>


<!--
*****************************************************
* Address high risk codes template
*****************************************************
-->
<xsl:template match="prd:AddrHiRiskCodes" >
 <xsl:param name="AddrHiRiskCodesIdent" />

    <!-- city line template -->
    <xsl:variable name="cityStateZip">
     <xsl:choose>
       <xsl:when test="normalize-space(prd:Zip4) != '' ">
         <xsl:value-of select="concat(normalize-space(prd:City), ', ', normalize-space(prd:State), '  ', normalize-space(prd:Zip), '-', normalize-space(prd:Zip4)  )" />
       </xsl:when>

       <xsl:otherwise>
         <xsl:value-of select="concat(normalize-space(prd:City), ', ', normalize-space(prd:State), '  ', normalize-space(prd:Zip)  )" />
       </xsl:otherwise>
     </xsl:choose>
    </xsl:variable>

    <!-- full phone -->
    <xsl:variable name="fullphone">
     <xsl:choose>
       <xsl:when test="normalize-space(prd:AreaCode) != '' ">
         <xsl:value-of select="concat('(', normalize-space(prd:AreaCode), ') ', substring(prd:PhnNbr, 1, 3 ), '-', substring(prd:PhnNbr, 4) )" />
       </xsl:when>

       <xsl:otherwise>
         <xsl:value-of select="concat(substring(prd:PhnNbr, 1, 3 ), '-', substring(prd:PhnNbr, 4) )" />
       </xsl:otherwise>
     </xsl:choose>
    </xsl:variable>

    <xsl:if test="./@ident=$AddrHiRiskCodesIdent">
       <tr>
         <td align="left" valign="top" width="50%">
           <font size="1" style="FONT-FAMILY: 'verdana';">
             <xsl:value-of select="prd:BusName" />
             <br />
             <xsl:value-of select="prd:Addr" />
             <br />
             <xsl:value-of select="$cityStateZip" />
           </font>
         </td>

         <td align="left" valign="top" width="50%">
           <font size="1" style="FONT-FAMILY: 'verdana';">
           <xsl:if test="./prd:PhnNbr != ''">
              <xsl:value-of select="$fullphone" />
           </xsl:if>
           </font>
         </td>
       </tr>

       <!-- space row -->
       <tr>
         <td colspan="2" style="line-height:5px;">&#160;</td>
       </tr>  

    </xsl:if>
</xsl:template>


<!--
*****************************************************
* business address detail template
*****************************************************
-->
<xsl:template name="BusinessAddressDetail">        
 <xsl:param name="BusAddrDetIdent" />

    <!-- bus address template builds full address -->
    <xsl:variable name="busAddress">
      <xsl:call-template name="CFIBusinessNameAddress" >
         <xsl:with-param name="VerificationIdent" select="$BusAddrDetIdent" />
         <xsl:with-param name="request" select="'ADDRCSZ'" />
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="msg">
     <xsl:choose>
       <xsl:when test="./@ident = $BusAddrDetIdent and normalize-space(../prd:VerifySummary[@ident=$BusAddrDetIdent]/prd:AddrCode) = 'N/A' ">
         <xsl:value-of select="'N/A'" />
       </xsl:when>

       <xsl:otherwise>
         <xsl:value-of select="'No business identified at input address.'" />
       </xsl:otherwise>
     </xsl:choose>
   </xsl:variable>

    <xsl:if test="./@ident=$BusAddrDetIdent">

       <xsl:if test="substring(./@ident, 3, 2) = 'A1'">
          <a name="#BUSADDRVERIFYB1A1"></a>
       </xsl:if>
       <xsl:if test="substring(./@ident, 3, 2) = 'A2'">
          <a name="#BUSADDRVERIFYB1A2"></a>
       </xsl:if>

       <!-- begin  Business Address Indicators -->
       <table width="100%" border="0" cellspacing="0" cellpadding="1">
         <tr>
           <td bgcolor="#0099cc">
             <table width="100%" border="0" cellspacing="0" cellpadding="0">
               <tr>
                 <td bgcolor="#ffffff">
                   <table width="100%" border="0" cellspacing="0" cellpadding="0">

                     <tr>
                       <td bgcolor="#0099cc" width="1%">
                         <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

                       <td bgcolor="#0099cc" width="99%" align="left" valign="middle" height="20">
                         <b><font color="#ffffff">
                           Business Address Detail - <xsl:value-of select="$busAddress" /></font></b></td>
                     </tr>

                     <!-- space row -->
                     <tr>
                       <td colspan="2" style="line-height:5px;">&#160;</td>
                     </tr>  

                     <tr>
                       <td>
                         <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

                       <td bgcolor="#ffffff">
                         <table width="100%" border="0" cellspacing="0" cellpadding="0">

                           <xsl:if test="number(../prd:CountsSummary[@ident=$BusAddrDetIdent]/prd:AddrBusMatches) = 0">
	                           <tr>
	                             <td align="left" valign="top" colspan="2">
	                               <font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$msg" /></font></td>
	                           </tr>

	                           <!-- space row -->
	                           <tr>
	                             <td colspan="2" style="line-height:5px;">&#160;</td>
	                           </tr>
                           </xsl:if>

                           <xsl:apply-templates select="../prd:BusAddr">
                              <xsl:with-param name="BusAddrIdent" select="$BusAddrDetIdent" />
                           </xsl:apply-templates>

                         </table>  
                       </td>
                     </tr>
                   </table>
                 </td>      
               </tr>
             </table>
           </td>
         </tr>
       </table>
       <!-- end Business  Address Indicators -->

       <!-- Back to Top grapic -->
       <xsl:call-template name="BackToTop" />

    </xsl:if>
</xsl:template>


<!--
*****************************************************
* business address template
*****************************************************
-->
<xsl:template match="prd:BusAddr" >
 <xsl:param name="BusAddrIdent" />

    <!-- city line template -->
    <xsl:variable name="cityStateZip">
     <xsl:choose>
       <xsl:when test="normalize-space(prd:Zip4) != '' ">
         <xsl:value-of select="concat(normalize-space(prd:City), ', ', normalize-space(prd:State), '  ', normalize-space(prd:Zip), '-', normalize-space(prd:Zip4)  )" />
       </xsl:when>

       <xsl:otherwise>
         <xsl:value-of select="concat(normalize-space(prd:City), ', ', normalize-space(prd:State), '  ', normalize-space(prd:Zip)  )" />
       </xsl:otherwise>
     </xsl:choose>
    </xsl:variable>

    <!-- full phone -->
    <xsl:variable name="fullphone">
     <xsl:choose>
       <xsl:when test="normalize-space(prd:AreaCode) != '' ">
         <xsl:value-of select="concat('(', normalize-space(prd:AreaCode), ') ', substring(prd:PhnNbr, 1, 3 ), '-', substring(prd:PhnNbr, 4) )" />
       </xsl:when>

       <xsl:otherwise>
         <xsl:value-of select="concat(substring(prd:PhnNbr, 1, 3 ), '-', substring(prd:PhnNbr, 4) )" />
       </xsl:otherwise>
     </xsl:choose>
    </xsl:variable>

    <xsl:if test="./@ident=$BusAddrIdent">
       <tr>
         <td align="left" valign="top" width="50%">
           <font size="1" style="FONT-FAMILY: 'verdana';">
             <xsl:value-of select="prd:BusName" />
             <br />
             <xsl:value-of select="prd:Addr" />
             <br />
             <xsl:value-of select="$cityStateZip" />
           </font>
         </td>

         <td align="left" valign="top" width="50%">
           <font size="1" style="FONT-FAMILY: 'verdana';">
           <xsl:if test="./prd:PhnNbr != ''">
              <xsl:value-of select="$fullphone" />
           </xsl:if>
           </font>
         </td>
       </tr>

       <!-- space row -->
       <tr>
         <td colspan="2" style="line-height:5px;">&#160;</td>
       </tr>  

    </xsl:if>

</xsl:template>


<!--
*****************************************************
* high risk phone template
*****************************************************
-->
<xsl:template name="HighRiskPhonel">        
 <xsl:param name="PhnHiRiskDescsIdent" />

    <!-- full phone -->
    <xsl:variable name="fullphone">
      <xsl:call-template name="CFIBusinessNameAddress" >
         <xsl:with-param name="VerificationIdent" select="$PhnHiRiskDescsIdent" />
         <xsl:with-param name="request" select="'PRIPHONE'" />
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="msg">
     <xsl:choose>
       <xsl:when test="number(../prd:CountsSummary[@ident=$PhnHiRiskDescsIdent]/prd:PhnHiRiskCodes) &gt; 0">
         <xsl:value-of select="concat('Phone High-risk Result: ', ../prd:PhnHiRiskDescs/prd:HighRiskDesc)" />
       </xsl:when>

       <xsl:when test="./@ident = $PhnHiRiskDescsIdent and normalize-space(../prd:VerifySummary[@ident=$PhnHiRiskDescsIdent]/prd:PhnCode) = 'N/A' ">
         <xsl:value-of select="'N/A'" />
       </xsl:when>

       <xsl:otherwise>
         <xsl:value-of select="'No high-risk phone indicator identified.'" />
       </xsl:otherwise>
     </xsl:choose>
   </xsl:variable>

     <xsl:if test="./@ident=$PhnHiRiskDescsIdent">
    
       <xsl:if test="substring(./@ident, 3, 2) = 'A1'">
          <a name="#BUSHIGHRISKPHONEB1A1"></a>
       </xsl:if>
       <xsl:if test="substring(./@ident, 3, 2) = 'A2'">
          <a name="#BUSHIGHRISKPHONEB1A2"></a>
       </xsl:if>

       <!-- begin  High-risk Address Indicators -->
       <table width="100%" border="0" cellspacing="0" cellpadding="1">
         <tr>
           <td bgcolor="#0099cc">
             <table width="100%" border="0" cellspacing="0" cellpadding="0">
               <tr>
                 <td bgcolor="#ffffff">
                   <table width="100%" border="0" cellspacing="0" cellpadding="0">

                     <tr>
                       <td bgcolor="#0099cc" width="1%">
                         <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

                       <td bgcolor="#0099cc" width="99%" align="left" valign="middle" height="20">
                         <b><font color="#ffffff">
                           High-risk Phone Indicators<xsl:if test="string-length(normalize-space($fullphone)) &gt; 0 "><xsl:value-of select="concat(' - ', $fullphone)" /></xsl:if></font></b></td>
                     </tr>

                     <!-- space row -->
                     <tr>
                       <td colspan="2" style="line-height:5px;">&#160;</td>
                     </tr>  

                     <tr>
                       <td>
                         <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

                       <td bgcolor="#ffffff">
                         <table width="100%" border="0" cellspacing="0" cellpadding="0">

                           <tr>
                             <td align="left" valign="top" colspan="2">
                               <font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$msg" /></font></td>
                           </tr>

                           <!-- space row -->
                           <tr>
                             <td colspan="2" style="line-height:5px;">&#160;</td>
                           </tr>

                           <xsl:apply-templates select="../prd:PhnHiRiskCodes">
                             <xsl:with-param name="PhnHiRiskCodesIdent" select="$PhnHiRiskDescsIdent" />
                           </xsl:apply-templates>

                         </table>  
                       </td>
                     </tr>
                   </table>
                 </td>      
               </tr>
             </table>
           </td>
         </tr>
       </table>
       <!-- end High-risk Address Indicators -->

       <!-- Back to Top grapic -->
       <xsl:call-template name="BackToTop" />

    </xsl:if>
 
</xsl:template>


<!--
*****************************************************
* phone high risk codes template
*****************************************************
-->
<xsl:template match="prd:PhnHiRiskCodes" >
 <xsl:param name="PhnHiRiskCodesIdent" />

    <!-- city line template -->
    <xsl:variable name="cityStateZip">
     <xsl:choose>
       <xsl:when test="normalize-space(prd:Zip4) != '' ">
         <xsl:value-of select="concat(normalize-space(prd:City), ', ', normalize-space(prd:State), '  ', normalize-space(prd:Zip), '-', normalize-space(prd:Zip4)  )" />
       </xsl:when>

       <xsl:otherwise>
         <xsl:value-of select="concat(normalize-space(prd:City), ', ', normalize-space(prd:State), '  ', normalize-space(prd:Zip)  )" />
       </xsl:otherwise>
     </xsl:choose>
    </xsl:variable>

    <!-- full phone -->
    <xsl:variable name="fullphone">
     <xsl:choose>
       <xsl:when test="normalize-space(prd:AreaCode) != '' ">
         <xsl:value-of select="concat('(', normalize-space(prd:AreaCode), ') ', substring(prd:PhnNbr, 1, 3 ), '-', substring(prd:PhnNbr, 4) )" />
       </xsl:when>

       <xsl:otherwise>
         <xsl:value-of select="concat(substring(prd:PhnNbr, 1, 3 ), '-', substring(prd:PhnNbr, 4) )" />
       </xsl:otherwise>
     </xsl:choose>
    </xsl:variable>

    <xsl:if test="./@ident=$PhnHiRiskCodesIdent">
       <tr>
         <td align="left" valign="top" width="50%">
           <font size="1" style="FONT-FAMILY: 'verdana';">
             <xsl:value-of select="prd:BusName" />
             <br />
             <xsl:value-of select="prd:Addr" />
             <br />
             <xsl:value-of select="$cityStateZip" />
           </font>
         </td>

         <td align="left" valign="top" width="50%">
           <font size="1" style="FONT-FAMILY: 'verdana';">
           <xsl:if test="./prd:PhnNbr != ''">
              <xsl:value-of select="$fullphone" />
           </xsl:if>
           </font>
         </td>
       </tr>

       <!-- space row -->
       <tr>
         <td colspan="2" style="line-height:5px;">&#160;</td>
       </tr>  

    </xsl:if>

</xsl:template>


<!--
*****************************************************
* business phone detail template
*****************************************************
-->
<xsl:template name="BusinessPhoneDetail">
 <xsl:param name="BusPhoneDetIdent" />

    <!-- full phone -->
    <xsl:variable name="fullphone">
      <xsl:call-template name="CFIBusinessNameAddress" >
         <xsl:with-param name="VerificationIdent" select="$BusPhoneDetIdent" />
         <xsl:with-param name="request" select="'PRIPHONE'" />
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="msg">
     <xsl:choose>
       <xsl:when test="./@ident = $BusPhoneDetIdent and normalize-space(../prd:VerifySummary[@ident=$BusPhoneDetIdent]/prd:PhnCode) = 'N/A' ">
         <xsl:value-of select="'N/A'" />
       </xsl:when>

       <xsl:otherwise>
         <xsl:value-of select="'No phone identified.'" />
       </xsl:otherwise>
     </xsl:choose>
   </xsl:variable>

    <xsl:if test="./@ident=$BusPhoneDetIdent">

       <xsl:if test="substring(./@ident, 3, 2) = 'A1'">
          <a name="#BUSPHONEVERIFYB1A1"></a>
       </xsl:if>
       <xsl:if test="substring(./@ident, 3, 2) = 'A2'">
          <a name="#BUSPHONEVERIFYB1A2"></a>
       </xsl:if>

       <!-- begin  Business Address Indicators -->
       <table width="100%" border="0" cellspacing="0" cellpadding="1">
         <tr>
           <td bgcolor="#0099cc">
             <table width="100%" border="0" cellspacing="0" cellpadding="0">
               <tr>
                 <td bgcolor="#ffffff">
                   <table width="100%" border="0" cellspacing="0" cellpadding="0">

                     <tr>
                       <td bgcolor="#0099cc" width="1%">
                         <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

                       <td bgcolor="#0099cc" width="99%" align="left" valign="middle" height="20">
                         <b><font color="#ffffff">
                           Business Phone Detail<xsl:if test="string-length(normalize-space($fullphone)) &gt; 0 "><xsl:value-of select="concat(' - ', $fullphone)" /></xsl:if></font></b></td>
                     </tr>

                     <!-- space row -->
                     <tr>
                       <td colspan="2" style="line-height:5px;">&#160;</td>
                     </tr>  

                     <tr>
                       <td>
                         <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

                       <td bgcolor="#ffffff">
                         <table width="100%" border="0" cellspacing="0" cellpadding="0">

                           <xsl:if test="number(../prd:CountsSummary[@ident=$BusPhoneDetIdent]/prd:PhnBusRecs) = 0">
	                           <tr>
	                             <td align="left" valign="top" colspan="2">
	                               <font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$msg" /></font></td>
	                           </tr>

	                           <!-- space row -->
	                           <tr>
	                             <td colspan="2" style="line-height:5px;">&#160;</td>
	                           </tr>
                           </xsl:if>

                           <xsl:apply-templates select="../prd:BusPhn">
                              <xsl:with-param name="BusPhnIdent" select="$BusPhoneDetIdent" />
                           </xsl:apply-templates>

                         </table>  
                       </td>
                     </tr>
                   </table>
                 </td>      
               </tr>
             </table>
           </td>
         </tr>
       </table>
       <!-- end Business  Address Indicators -->

       <!-- Back to Top grapic -->
       <xsl:call-template name="BackToTop" />

    </xsl:if>
</xsl:template>


<!--
*****************************************************
* business phone template
*****************************************************
-->
<xsl:template match="prd:BusPhn" >
 <xsl:param name="BusPhnIdent" />

    <!-- city line template -->
    <xsl:variable name="cityStateZip">
     <xsl:choose>
       <xsl:when test="normalize-space(prd:Zip4) != '' ">
         <xsl:value-of select="concat(normalize-space(prd:City), ', ', normalize-space(prd:State), '  ', normalize-space(prd:Zip), '-', normalize-space(prd:Zip4)  )" />
       </xsl:when>

       <xsl:otherwise>
         <xsl:value-of select="concat(normalize-space(prd:City), ', ', normalize-space(prd:State), '  ', normalize-space(prd:Zip)  )" />
       </xsl:otherwise>
     </xsl:choose>
    </xsl:variable>

    <!-- full phone -->
    <xsl:variable name="fullphone">
     <xsl:choose>
       <xsl:when test="normalize-space(prd:AreaCode) != '' ">
         <xsl:value-of select="concat('(', normalize-space(prd:AreaCode), ') ', substring(prd:PhnNbr, 1, 3 ), '-', substring(prd:PhnNbr, 4) )" />
       </xsl:when>

       <xsl:otherwise>
         <xsl:value-of select="concat(substring(prd:PhnNbr, 1, 3 ), '-', substring(prd:PhnNbr, 4) )" />
       </xsl:otherwise>
     </xsl:choose>
    </xsl:variable>

    <xsl:if test="./@ident=$BusPhnIdent">
       <tr>
         <td align="left" valign="top" width="50%">
           <font size="1" style="FONT-FAMILY: 'verdana';">
             <xsl:value-of select="prd:BusName" />
             <br />
             <xsl:value-of select="prd:Addr" />
             <br />
             <xsl:value-of select="$cityStateZip" />
           </font>
         </td>

         <td align="left" valign="top" width="50%">
           <font size="1" style="FONT-FAMILY: 'verdana';">
           <xsl:if test="./prd:PhnNbr != ''">
              <xsl:value-of select="$fullphone" />
           </xsl:if>
           </font>
         </td>
       </tr>

       <!-- space row -->
       <tr>
         <td colspan="2" style="line-height:5px;">&#160;</td>
       </tr>  

    </xsl:if>

</xsl:template>


<!--
*****************************************************
* OFAC template
*****************************************************
-->
<xsl:template name="OFACValidation">
 <xsl:param name="OFACIdent" />

    <xsl:variable name="msg">
     <xsl:choose>
       <xsl:when test="./@ident = $OFACIdent and not (../prd:CountsSummary[@ident=$OFACIdent]/prd:OFACRecs) ">
         <xsl:value-of select="'N/A'" />
       </xsl:when>

       <xsl:otherwise>
         <xsl:value-of select="'No OFAC matches identified.'" />
       </xsl:otherwise>
     </xsl:choose>
   </xsl:variable>

    <xsl:if test="./@ident=$OFACIdent">

       <xsl:if test="substring(./@ident, 3, 2) = 'A1'">
          <a name="#BUSOFACVALIDATEB1A1"></a>
       </xsl:if>
       <xsl:if test="substring(./@ident, 3, 2) = 'A2'">
          <a name="#BUSOFACVALIDATEB1A2"></a>
       </xsl:if>

       <!-- begin  Business Address Indicators -->
       <table width="100%" border="0" cellspacing="0" cellpadding="1">
         <tr>
           <td bgcolor="#0099cc">
             <table width="100%" border="0" cellspacing="0" cellpadding="0">
               <tr>
                 <td bgcolor="#ffffff">
                   <table width="100%" border="0" cellspacing="0" cellpadding="0">

                     <tr>
                       <td bgcolor="#0099cc" width="1%">
                         <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

                       <td bgcolor="#0099cc" width="99%" align="left" valign="middle" height="20">
                         <b><font color="#ffffff">
                          OFAC Validation</font></b></td>
                     </tr>

                     <!-- space row -->
                     <tr>
                       <td colspan="2" style="line-height:5px;">&#160;</td>
                     </tr>  

                     <tr>
                       <td>
                         <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

                       <td bgcolor="#ffffff">
                         <table width="100%" border="0" cellspacing="0" cellpadding="0">

                           <xsl:if test="number(../prd:CountsSummary[@ident=$OFACIdent]/prd:OFACRecs) = 0">
	                           <tr>
	                             <td align="left" valign="top" colspan="2">
	                               <font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$msg" /></font></td>
	                           </tr>

	                           <!-- space row -->
	                           <tr>
	                             <td colspan="2" style="line-height:5px;">&#160;</td>
	                           </tr>
                           </xsl:if>

                           <xsl:apply-templates select="../prd:OFACDetail[@ident=$OFACIdent]">
                              <xsl:with-param name="OFACIdent" select="$OFACIdent" />
                           </xsl:apply-templates>

                         </table>  
                       </td>
                     </tr>
                   </table>
                 </td>      
               </tr>
             </table>
           </td>
         </tr>
       </table>
       <!-- end Business  Address Indicators -->

       <!-- Back to Top grapic -->
       <xsl:call-template name="BackToTop" />

    </xsl:if>
</xsl:template>


<!--
*****************************************************
* OFAC template
*****************************************************
-->
<xsl:template match="prd:OFACDetail" >
 <xsl:param name="OFACIdent" />

    <xsl:variable name="OFACMsg">
       <xsl:variable name="code">
      		<xsl:value-of select="../prd:VerifySummary[@ident=$OFACIdent]/prd:OFACCode" />
      	</xsl:variable>

	<xsl:call-template name="AuthOFACValidationTable">
	   <xsl:with-param name="code" select="$code" />
	</xsl:call-template>
    </xsl:variable>

    <xsl:variable name="OFACDesc">
     <xsl:choose>
       <xsl:when test="count(../prd:OFACDetail[@ident=$OFACIdent]) > 1">
         <xsl:value-of select="concat(../prd:OFACDetail[position() = 1]/prd:OFACDesc, ../prd:OFACDetail[position() = 2]/prd:OFACDesc )" />
       </xsl:when>

       <xsl:when test="count(../prd:OFACDetail[@ident=$OFACIdent]) = 1">
         <xsl:value-of select="../prd:OFACDetail/prd:OFACDesc" />
       </xsl:when>

       <xsl:otherwise>
         <xsl:value-of select="''" />
       </xsl:otherwise>
     </xsl:choose>
    </xsl:variable>

<!--
<br/>(<xsl:value-of select="$OFACDesc" />)<br/>
-->

    <xsl:variable name="OFACHeader">
     <xsl:choose>
       <xsl:when test="contains($OFACDesc, '(a.k.a.')">
         <xsl:value-of select="substring-before($OFACDesc, '(a.k.a.')" />
       </xsl:when>

       <xsl:when test="contains($OFACDesc, '(aka')">
         <xsl:value-of select="substring-before($OFACDesc, '(aka')" />
       </xsl:when>

       <xsl:when test="contains($OFACDesc, '(f.k.a.')">
         <xsl:value-of select="substring-before($OFACDesc, '(f.k.a.')" />
       </xsl:when>

       <xsl:when test="contains($OFACDesc, '(fka')">
         <xsl:value-of select="substring-before($OFACDesc, '(fka')" />
       </xsl:when>

       <xsl:when test="contains($OFACDesc, '(n.k.a.')">
         <xsl:value-of select="substring-before($OFACDesc, '(n.k.a.')" />
       </xsl:when>

       <xsl:when test="contains($OFACDesc, '(nka')">
         <xsl:value-of select="substring-before($OFACDesc, '(nka')" />
       </xsl:when>

       <xsl:otherwise>
         <xsl:value-of select="''" />
       </xsl:otherwise>
     </xsl:choose>
   </xsl:variable>

    <xsl:variable name="OFACaka">
     <xsl:choose>
       <xsl:when test="contains($OFACDesc, '(a.k.a.')">
         <xsl:value-of select="substring-before(substring-after($OFACDesc, '(a.k.a.'), ')' ) " />
       </xsl:when>

       <xsl:when test="contains($OFACDesc, '(aka')">
         <xsl:value-of select="concat('aka ', substring-before(substring-after($OFACDesc, '(aka'), ')' )) " />
       </xsl:when>

       <xsl:when test="contains($OFACDesc, '(f.k.a.')">
         <xsl:value-of select="concat('f.k.a. ', substring-before(substring-after($OFACDesc, '(f.k.a.'), ')' )) " />
       </xsl:when>

       <xsl:when test="contains($OFACDesc, '(fka')">
         <xsl:value-of select="concat('fka ', substring-before(substring-after($OFACDesc, '(fka'), ')' )) " />
       </xsl:when>

       <xsl:when test="contains($OFACDesc, '(n.k.a.')">
         <xsl:value-of select="concat('n.k.a. ', substring-before(substring-after($OFACDesc, '(n.k.a.'), ')' )) " />
       </xsl:when>

       <xsl:when test="contains($OFACDesc, '(nka')">
         <xsl:value-of select="concat('nka ', substring-before(substring-after($OFACDesc, '(nka'), ')' )) " />
       </xsl:when>

       <xsl:otherwise>
         <xsl:value-of select="''" />
       </xsl:otherwise>
     </xsl:choose>
   </xsl:variable>

    <xsl:variable name="OFAClines">
     <xsl:choose>
       <xsl:when test="contains($OFACDesc, '(a.k.a.')">
         <xsl:value-of select="substring-after(substring-after(substring-after($OFACDesc, '(a.k.a.'), ')'), ' ' ) " />
       </xsl:when>

       <xsl:when test="contains($OFACDesc, '(aka')">
         <xsl:value-of select="substring-after(substring-after(substring-after($OFACDesc, '(aka'), ')'), ' ' ) " />
       </xsl:when>

       <xsl:when test="contains($OFACDesc, '(f.k.a.')">
         <xsl:value-of select="substring-after(substring-after(substring-after($OFACDesc, '(f.k.a.'), ')'), ' ' ) " />
       </xsl:when>

       <xsl:when test="contains($OFACDesc, '(fka')">
         <xsl:value-of select="substring-after(substring-after(substring-after($OFACDesc, '(fka'), ')'), ' ' ) " />
       </xsl:when>

       <xsl:when test="contains($OFACDesc, '(n.k.a.')">
         <xsl:value-of select="substring-after(substring-after(substring-after($OFACDesc, '(n.k.a.'), ')'), ' ' ) " />
       </xsl:when>

       <xsl:when test="contains($OFACDesc, '(nka')">
         <xsl:value-of select="substring-after(substring-after(substring-after($OFACDesc, '(nka'), ')'), ' ' ) " />
       </xsl:when>

       <xsl:when test="position() > 1">
         <xsl:value-of select="$OFACDesc" />
       </xsl:when>

       <xsl:otherwise>
         <xsl:value-of select="''" />
       </xsl:otherwise>
     </xsl:choose>
   </xsl:variable>

    <xsl:if test="./@ident=$OFACIdent and position() = 1">
       <tr>
         <td align="left" valign="top" colspan="2">
           <font size="1" style="FONT-FAMILY: 'verdana';">
           <b>Result Code:</b><xsl:text disable-output-escaping="yes">&#160;</xsl:text><xsl:value-of select="../prd:VerifySummary[@ident=$OFACIdent]/prd:OFACCode" /></font>
           <font size="1" style="FONT-FAMILY: 'verdana'; text-transform: uppercase; "><xsl:value-of select="$OFACMsg" /></font>
         </td>
       </tr>

       <!-- space row -->
       <tr>
         <td colspan="2" style="line-height:5px;">&#160;</td>
       </tr>

       <xsl:if test="normalize-space($OFACHeader)">
        <tr>
          <td align="left" valign="top" colspan="2">
            <font size="1" style="FONT-FAMILY: 'verdana';">
            <b>OFAC Record:</b><xsl:text disable-output-escaping="yes">&#160;</xsl:text><xsl:value-of select="$OFACHeader" /></font>
          </td>
        </tr>

        <!-- space row -->
        <tr>
          <td colspan="2" style="line-height:5px;">&#160;</td>
        </tr>
       </xsl:if>

       <xsl:if test="normalize-space($OFACaka)">
        <tr>
          <td align="left" valign="top" colspan="2">
            <font size="1" style="FONT-FAMILY: 'verdana';">
            <b>AKA:</b><br /></font>
	      <xsl:call-template name="OFACLoop">
	        <xsl:with-param name="buffer" select="$OFACaka" />
	        <xsl:with-param name="type" select="'AKA'" />
	        <xsl:with-param name="index" select="1" />
	      </xsl:call-template>
          </td>
        </tr>

        <!-- space row -->
        <tr>
          <td colspan="2" style="line-height:5px;">&#160;</td>
        </tr>
       </xsl:if>

       <tr>
         <td align="left" valign="top" width="50%">
	      <xsl:call-template name="OFACLoop">
	        <xsl:with-param name="buffer" select="$OFAClines" />
	        <xsl:with-param name="type" select="'LINES'" />
	        <xsl:with-param name="index" select="1" />
	      </xsl:call-template>
         </td>
       </tr>

       <!-- space row -->
       <tr>
         <td colspan="2" style="line-height:5px;">&#160;</td>
       </tr>  
    </xsl:if>

</xsl:template>


  <!--
  *********************************************
  * OFACLoop template
  *********************************************
  -->
  <xsl:template name="OFACLoop">
    <xsl:param name="buffer" />
    <xsl:param name="type" />
    <xsl:param name="index" />

    <xsl:variable name="tmpMsgLine">
      <xsl:choose>		              
        <xsl:when test="contains($buffer, ';')">
          <xsl:value-of select="substring-before($buffer, ';')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="$buffer" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="msgLine">
      <xsl:choose>		              
        <xsl:when test="$type = 'AKA' and $index > 1">
	     <xsl:choose>
	       <xsl:when test="contains($tmpMsgLine, 'a.k.a.')">
	         <xsl:value-of select="substring-after($tmpMsgLine, 'a.k.a.') " />
	       </xsl:when>
	
	       <xsl:when test="contains($tmpMsgLine, 'aka')">
	         <xsl:value-of select="substring-after($tmpMsgLine, 'aka') " />
	       </xsl:when>
	
	       <xsl:when test="contains($tmpMsgLine, 'f.k.a.')">
	         <xsl:value-of select="substring-after($tmpMsgLine, 'f.k.a.') " />
	       </xsl:when>
	
	       <xsl:when test="contains($tmpMsgLine, 'fka')">
	         <xsl:value-of select="substring-after($tmpMsgLine, 'fka') " />
	       </xsl:when>
	
	       <xsl:when test="contains($tmpMsgLine, 'n.k.a.')">
	         <xsl:value-of select="substring-after($tmpMsgLine, 'n.k.a.') " />
	       </xsl:when>
	
	       <xsl:when test="contains($tmpMsgLine, 'nka')">
	         <xsl:value-of select="substring-after($tmpMsgLine, 'nka') " />
	       </xsl:when>

	       <xsl:otherwise>
	         <xsl:value-of select="$tmpMsgLine" />
	       </xsl:otherwise>
	     </xsl:choose>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="$tmpMsgLine" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="not (contains($buffer, ';')) and contains($buffer, '[')">
        <font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="normalize-space(substring-before($msgLine,'['))" /><br />
        [<xsl:value-of select="normalize-space(substring-after($msgLine,'['))" /><br /></font>
      </xsl:when>

      <xsl:otherwise>
        <font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="normalize-space($msgLine)" /></font><br />
      </xsl:otherwise>
    </xsl:choose>    

    <!-- Test condition and call OFACLoop template if semi-colon found -->
    <xsl:if test="contains($buffer, ';')">
      <xsl:call-template name="OFACLoop">
        <xsl:with-param name="buffer" select="substring-after($buffer, ';') " />
        <xsl:with-param name="type" select="$type" />
        <xsl:with-param name="index" select="$index+1" />
      </xsl:call-template>
    </xsl:if>

  </xsl:template>  


<!--
*****************************************************
*TaxIDDetail template
*****************************************************
-->
<xsl:template name="TaxIDDetail">        
 <xsl:param name="TaxIDDetIdent" />

    <!-- Tax ID -->
     <xsl:variable name="taxID">
      <xsl:choose>
        <xsl:when test="number(../prd:CountsSummary[@ident=$TaxIDDetIdent]/prd:TaxIDRecs) &gt; 0 and count(../prd:TaxIDDetail[prd:HiConfidenceLevel = 1]) &gt; 0 and count(../prd:TaxIDDetail[prd:TaxIDMatch = 'Y']) &gt; 0 and ../prd:BusinessNameAndAddress  ">
          <xsl:value-of select="concat(substring(../prd:TaxIDDetail/prd:TaxID, 1, 2), '-', substring(../prd:TaxIDDetail/prd:TaxID, 3))" />
        </xsl:when>

        <xsl:when test="../prd:HistoricalValidation/prd:MatchIndicators/prd:TaxIDMatch = 'Y' ">
          <xsl:value-of select="concat(substring(../prd:HistoricalValidation/prd:PreviousBusinessApplicant/prd:TaxID, 1, 2), '-', substring(../prd:HistoricalValidation/prd:PreviousBusinessApplicant/prd:TaxID, 3))" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="msg">
     <xsl:choose>
       <xsl:when test="./@ident = $TaxIDDetIdent and normalize-space(../prd:VerifySummary[@ident=$TaxIDDetIdent]/prd:TaxIDCode) = 'N/A' ">
         <xsl:value-of select="'N/A'" />
       </xsl:when>

       <xsl:otherwise>
         <xsl:value-of select="'No match to business name or address.'" />
       </xsl:otherwise>
     </xsl:choose>
   </xsl:variable>

    <xsl:if test="./@ident=$TaxIDDetIdent">

       <xsl:if test="substring(./@ident, 3, 2) = 'A1'">
          <a name="#BUSTAXIDVERIFYB1A1"></a>
       </xsl:if>
       <xsl:if test="substring(./@ident, 3, 2) = 'A2'">
          <a name="#BUSTAXIDVERIFYB1A2"></a>
       </xsl:if>

       <!-- begin Tax ID Detail -->
       <table width="100%" border="0" cellspacing="0" cellpadding="1">
         <tr>
           <td bgcolor="#0099cc">
             <table width="100%" border="0" cellspacing="0" cellpadding="0">
               <tr>
                 <td bgcolor="#ffffff">
                   <table width="100%" border="0" cellspacing="0" cellpadding="0">

                     <tr>
                       <td bgcolor="#0099cc" width="1%">
                         <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

                       <td bgcolor="#0099cc" width="99%" align="left" valign="middle" height="20">
                         <b><font color="#ffffff">
                           Tax ID Detail<xsl:if test="string-length(normalize-space($taxID)) &gt; 0 "><xsl:value-of select="concat(' - ', $taxID)" /></xsl:if></font></b></td>
                     </tr>

                     <!-- space row -->
                     <tr>
                       <td colspan="2" style="line-height:5px;">&#160;</td>
                     </tr>  

                     <tr>
                       <td>
                         <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

                       <td bgcolor="#ffffff">
                         <table width="100%" border="0" cellspacing="0" cellpadding="0">

                           <xsl:if test="number(../prd:CountsSummary[@ident=$TaxIDDetIdent]/prd:TaxIDRecs) = 0 or count(../prd:TaxIDDetail[prd:HiConfidenceLevel = 1]) = 0 or count(../prd:TaxIDDetail[prd:TaxIDMatch = 'Y']) = 0  ">
	                           <tr>
	                             <td align="left" valign="top" colspan="2">
	                               <font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$msg" /></font></td>
	                           </tr>

	                           <!-- space row -->
	                           <tr>
	                             <td colspan="2" style="line-height:5px;">&#160;</td>
	                           </tr>
                           </xsl:if>

                           <xsl:if test="number(../prd:CountsSummary[@ident=$TaxIDDetIdent]/prd:TaxIDRecs) &gt; 0 and count(../prd:TaxIDDetail[prd:HiConfidenceLevel = 1]) &gt; 0 and count(../prd:TaxIDDetail[prd:TaxIDMatch = 'Y']) &gt; 0 and ../prd:BusinessNameAndAddress  ">
                              <xsl:apply-templates select="../prd:TaxIDDetail">
                                 <xsl:with-param name="TaxIDIdent" select="$TaxIDDetIdent" />
                              </xsl:apply-templates>
                           </xsl:if>

                         </table>  
                       </td>
                     </tr>
                   </table>
                 </td>      
               </tr>
             </table>
           </td>
         </tr>
       </table>
       <!-- end Business  Address Indicators -->

       <!-- Back to Top grapic -->
       <xsl:call-template name="BackToTop" />

    </xsl:if>
</xsl:template>


<!--
*****************************************************
* business phone template
*****************************************************
-->
<xsl:template match="prd:TaxIDDetail" >
 <xsl:param name="TaxIDIdent" />

    <!-- bus name -->
    <xsl:variable name="busName">
       <xsl:value-of select="../prd:BusinessNameAndAddress/prd:BusinessName" />
    </xsl:variable>

    <!-- street address -->
    <xsl:variable name="streetAddress">
       <xsl:value-of select="../prd:BusinessNameAndAddress/prd:StreetAddress" />
    </xsl:variable>

    <!-- city line template -->
    <xsl:variable name="cityStateZip">
        <xsl:value-of select="concat(normalize-space(../prd:BusinessNameAndAddress/prd:City), ', ', normalize-space(../prd:BusinessNameAndAddress/prd:State), '  ', normalize-space(../prd:BusinessNameAndAddress/prd:Zip)  )" />
    </xsl:variable>

    <!-- Tax ID -->
    <xsl:variable name="taxID">
      <xsl:value-of select="concat(substring(prd:TaxID, 1, 2), '-', substring(prd:TaxID, 3))" />
    </xsl:variable>

    <xsl:if test="position() = 1">
       <tr>
         <td align="left" valign="top" width="50%">
           <font size="1" style="FONT-FAMILY: 'verdana';">
             <xsl:value-of select="$busName" />
             <br />
             <xsl:value-of select="$streetAddress" />
             <br />
             <xsl:value-of select="$cityStateZip" />
           </font>
         </td>

         <td align="left" valign="top" width="50%">
           <font size="1" style="FONT-FAMILY: 'verdana';">
           <xsl:if test="normalize-space($taxID)">
              Tax ID: <xsl:value-of select="$taxID" />
           </xsl:if>
           </font>
         </td>
       </tr>

       <!-- space row -->
       <tr>
         <td colspan="2" style="line-height:5px;">&#160;</td>
       </tr>  

    </xsl:if>

</xsl:template>

</xsl:stylesheet>