
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rsp="http://www.experian.com/NetConnectResponse"
                xmlns:prd="http://www.experian.com/ARFResponse">

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


 <!--
 ******************************************************************
 * CFI Business Alerts And Verification Details template
 ******************************************************************
 -->
 <xsl:template name="CFIBusinessAlertsAndVerification">
  
    <!-- Section title -->
    <xsl:call-template name="SectionTitle">
      <xsl:with-param name="title" select="'Business Alerts and Verification Details'" />
    </xsl:call-template>

    <!-- match ident   -->
    <xsl:variable name="matchIdent">
      <xsl:call-template name="CFIBusinessMatch" >
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="processIdent1">
      <xsl:choose>
        <xsl:when test="normalize-space($matchIdent) = 'B1A1' or normalize-space($matchIdent) = 'B1A2' ">
          <xsl:value-of select="'B1A1'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'B2A1'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="processIdent2">
      <xsl:choose>
        <xsl:when test="normalize-space($matchIdent) = 'B1A1' or normalize-space($matchIdent) = 'B1A2' ">
          <xsl:value-of select="'B1A2'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'B2A2'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:call-template name="CFIBusinessAlertsAndVerificationDetails">
        <xsl:with-param name="VerificationIdent" select="$processIdent1" />
    </xsl:call-template>

    <!-- Back to Top grapic -->
    <xsl:call-template name="BackToTop" />

    <xsl:if test="prd:GeneralSummary[@ident=$processIdent2]/prd:ResponseType != ''">
       <xsl:call-template name="CFIBusinessAlertsAndVerificationDetails">
          <xsl:with-param name="VerificationIdent" select="$processIdent2" />
       </xsl:call-template>

       <!-- Back to Top grapic -->
       <xsl:call-template name="BackToTop" />
    </xsl:if>

 </xsl:template>


<!--
*********************************************
* CFIBusinessAlertsAndVerificationDetails template
*********************************************
-->
 <xsl:template name="CFIBusinessAlertsAndVerificationDetails">        
  <xsl:param name="VerificationIdent" />

    <!-- bus name address template builds full name and address -->
    <xsl:variable name="busNameAddress">
      <xsl:call-template name="CFIBusinessNameAddress" >
         <xsl:with-param name="VerificationIdent" select="$VerificationIdent" />
         <xsl:with-param name="request" select="'NAMEADDRCSZ'" />
      </xsl:call-template>
    </xsl:variable>

    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="#0099cc">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#ffffff">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td bgcolor="#0099cc" width="1%">
                      <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

                    <td bgcolor="#0099cc" width="99%" colspan="2" align="left" valign="middle" height="20">
                      <b><font color="#ffffff"><xsl:value-of select="$busNameAddress" /></font></b>
                    </td>
                  </tr>

                  <xsl:apply-templates select="prd:HighRiskSummary[@type='Business'][@ident=$VerificationIdent]" >
                    <xsl:with-param name="HighRiskSummaryIdent" select="$VerificationIdent" />
                    <xsl:with-param name="HighRiskSummaryItem" select="'HIGHRISKADDR'" />
                  </xsl:apply-templates>

                  <xsl:apply-templates select="prd:VerifySummary[@type='Business'][@ident=$VerificationIdent]" >
                    <xsl:with-param name="VerifySummaryIdent" select="$VerificationIdent" />
                    <xsl:with-param name="VerifySummaryItem" select="'VERIFYADDR'" />
                  </xsl:apply-templates>

                  <xsl:apply-templates select="prd:VerifySummary[@type='Business'][@ident=$VerificationIdent]" >
                    <xsl:with-param name="VerifySummaryIdent" select="$VerificationIdent" />
                    <xsl:with-param name="VerifySummaryItem" select="'VERIFYADDRTYPE'" />
                  </xsl:apply-templates>

                  <xsl:apply-templates select="prd:VerifySummary[@type='Business'][@ident=$VerificationIdent]" >
                    <xsl:with-param name="VerifySummaryIdent" select="$VerificationIdent" />
                    <xsl:with-param name="VerifySummaryItem" select="'ADDRMISMATCH'" />
                  </xsl:apply-templates>

                  <xsl:apply-templates select="prd:HighRiskSummary[@type='Business'][@ident=$VerificationIdent]" >
                    <xsl:with-param name="HighRiskSummaryIdent" select="$VerificationIdent" />
                    <xsl:with-param name="HighRiskSummaryItem" select="'HIGHRISKPHONE'" />
                  </xsl:apply-templates>

                  <xsl:apply-templates select="prd:VerifySummary[@type='Business'][@ident=$VerificationIdent]" >
                    <xsl:with-param name="VerifySummaryIdent" select="$VerificationIdent" />
                    <xsl:with-param name="VerifySummaryItem" select="'VERIFYPHONE'" />
                  </xsl:apply-templates>

                  <xsl:apply-templates select="prd:VerifySummary[@type='Business'][@ident=$VerificationIdent]" >
                    <xsl:with-param name="VerifySummaryIdent" select="$VerificationIdent" />
                    <xsl:with-param name="VerifySummaryItem" select="'PHONEMISMATCH'" />
                  </xsl:apply-templates>

                  <xsl:apply-templates select="prd:VerifySummary[@type='Business'][@ident=$VerificationIdent]" >
                    <xsl:with-param name="VerifySummaryIdent" select="$VerificationIdent" />
                    <xsl:with-param name="VerifySummaryItem" select="'VERIFYOFAC'" />
                  </xsl:apply-templates>

                  <xsl:apply-templates select="prd:VerifySummary[@type='Business'][@ident=$VerificationIdent]" >
                    <xsl:with-param name="VerifySummaryIdent" select="$VerificationIdent" />
                    <xsl:with-param name="VerifySummaryItem" select="'VERIFYTAXID'" />
                  </xsl:apply-templates>

                  <xsl:call-template name="CFIMatchIndicators" >
                    <xsl:with-param name="HistoricalValidationIdent" select="$VerificationIdent" />
                  </xsl:call-template>

                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>  
 </xsl:template>      


  <!--
  *********************************************
  * high risk summary template
  *********************************************
  -->
  <xsl:template match="prd:HighRiskSummary" >
    <xsl:param name="HighRiskSummaryIdent" />
    <xsl:param name="HighRiskSummaryItem" />

    <xsl:variable name="AddrMsg">
       <xsl:variable name="code">
      		<xsl:value-of select="prd:AddrCode" />
      	</xsl:variable>

	<xsl:call-template name="AuthAddressHighRiskTable">
	   <xsl:with-param name="code" select="$code" />
	</xsl:call-template>
    </xsl:variable>

    <xsl:variable name="PhnMsg">
       <xsl:variable name="code">
      		<xsl:value-of select="prd:PhnCode" />
      	</xsl:variable>

	<xsl:call-template name="AuthPhoneHighRiskTable">
	   <xsl:with-param name="code" select="$code" />
	</xsl:call-template>
    </xsl:variable>

    <xsl:if test="./@ident=$HighRiskSummaryIdent">

       <xsl:if test="$HighRiskSummaryItem = 'HIGHRISKADDR'">
          <xsl:if test="prd:AddrMsg != '' ">
          <tr style="background-color:expression(this.previousSibling.style.backgroundColor == 'white'? '#E5F5FA':'white')">
            <td height="20" width="1%">
               <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

            <td width="30%" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">Address High-risk Result:</font></td>

            <td width="69%"><font size="1" style="FONT-FAMILY: 'verdana'; text-transform: uppercase; "><xsl:value-of select="$AddrMsg" /><xsl:text disable-output-escaping="yes">&#160;</xsl:text>
              </font><font size="1" style="FONT-FAMILY: 'verdana'; ">
               <xsl:if test="number(../prd:CountsSummary[@ident=$HighRiskSummaryIdent]/prd:AddrHiRiskCodes) &gt; 0" >
	               <xsl:if test="substring($HighRiskSummaryIdent, 3, 2) = 'A1'">
	                  <a href="#BUSHIGHRISKADDRB1A1">details</a>
	               </xsl:if>
	               <xsl:if test="substring($HighRiskSummaryIdent, 3, 2) = 'A2'">
	                  <a href="#BUSHIGHRISKADDRB1A2">details</a>
	               </xsl:if>
               </xsl:if>
              </font>
            </td>
          </tr>
          </xsl:if>
       </xsl:if>

       <xsl:if test="$HighRiskSummaryItem = 'HIGHRISKPHONE'">
          <xsl:if test="prd:PhnMsg != '' ">
          <tr style="background-color:expression(this.previousSibling.style.backgroundColor == 'white'? '#E5F5FA':'white')">
            <td height="20" width="1%">
               <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

            <td width="30%" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">Phone High-risk Result:</font></td>

            <td width="69%"><font size="1" style="FONT-FAMILY: 'verdana'; text-transform: uppercase; "><xsl:value-of select="$PhnMsg" /><xsl:text disable-output-escaping="yes">&#160;</xsl:text>
              </font><font size="1" style="FONT-FAMILY: 'verdana'; ">
               <xsl:if test="number(../prd:CountsSummary[@ident=$HighRiskSummaryIdent]/prd:PhnHiRiskCodes) &gt; 0" >
	               <xsl:if test="substring($HighRiskSummaryIdent, 3, 2) = 'A1'">
	                  <a href="#BUSHIGHRISKPHONEB1A1">details</a>
	               </xsl:if>
	               <xsl:if test="substring($HighRiskSummaryIdent, 3, 2) = 'A2'">
	                  <a href="#BUSHIGHRISKPHONEB1A2">details</a>
	               </xsl:if>
               </xsl:if>
              </font>
            </td>
          </tr>
          </xsl:if>
       </xsl:if>
    </xsl:if>

</xsl:template>


  <!--
  *********************************************
  * verify summary template
  *********************************************
  -->
  <xsl:template match="prd:VerifySummary" >
    <xsl:param name="VerifySummaryIdent" />
    <xsl:param name="VerifySummaryItem" /> 

    <xsl:variable name="AddrMsg">
       <xsl:variable name="code">
      		<xsl:value-of select="prd:AddrCode" />
      	</xsl:variable>

	<xsl:call-template name="AuthAddressVerifTable">
	   <xsl:with-param name="code" select="$code" />
	</xsl:call-template>
    </xsl:variable>

    <xsl:variable name="AddrTypeMsg">
       <xsl:variable name="code">
      		<xsl:value-of select="prd:AddrTypeCode" />
      	</xsl:variable>

	<xsl:call-template name="AuthAddressTypeTable">
	   <xsl:with-param name="code" select="$code" />
	</xsl:call-template>
    </xsl:variable>

    <xsl:variable name="AddrUnitMismatchMsg">
       <xsl:variable name="code">
      		<xsl:value-of select="prd:AddrUnitMismatchCode" />
      	</xsl:variable>

	<xsl:call-template name="AuthAddressUnitTable">
	   <xsl:with-param name="code" select="$code" />
	</xsl:call-template>
    </xsl:variable>

    <xsl:variable name="PhnMsg">
       <xsl:variable name="code">
      		<xsl:value-of select="prd:PhnCode" />
      	</xsl:variable>

	<xsl:call-template name="AuthPhoneVerifTable">
	   <xsl:with-param name="code" select="$code" />
	</xsl:call-template>
    </xsl:variable>

    <xsl:variable name="PhnUnitMismatchMsg">
       <xsl:variable name="code">
      		<xsl:value-of select="prd:PhnUnitMismatchCode" />
      	</xsl:variable>

	<xsl:call-template name="AuthPhoneUnitTable">
	   <xsl:with-param name="code" select="$code" />
	</xsl:call-template>
    </xsl:variable>

    <xsl:variable name="OFACMsg">
       <xsl:variable name="code">
      		<xsl:value-of select="prd:OFACCode" />
      	</xsl:variable>

	<xsl:call-template name="AuthOFACValidationTable">
	   <xsl:with-param name="code" select="$code" />
	</xsl:call-template>
    </xsl:variable>

    <xsl:variable name="TaxIDMsg">
       <xsl:variable name="code">
      		<xsl:value-of select="prd:TaxIDCode" />
      	</xsl:variable>

	<xsl:call-template name="AuthTaxIDTable">
	   <xsl:with-param name="code" select="$code" />
	</xsl:call-template>
    </xsl:variable>

    <xsl:if test="./@ident=$VerifySummaryIdent">

       <xsl:if test="$VerifySummaryItem = 'VERIFYADDR'">
          <xsl:if test="prd:AddrMsg != '' ">
          <tr style="background-color:expression(this.previousSibling.style.backgroundColor == 'white'? '#E5F5FA':'white')">
            <td height="20" width="1%">
               <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

            <td width="30%" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">Address Verification Result:</font></td>

            <td width="69%"><font size="1" style="FONT-FAMILY: 'verdana'; text-transform: uppercase; "><xsl:value-of select="$AddrMsg" /><xsl:text disable-output-escaping="yes">&#160;</xsl:text>
              </font><font size="1" style="FONT-FAMILY: 'verdana'; ">
               <xsl:if test="../prd:BusAddr[@ident=$VerifySummaryIdent][1]/prd:Addr != ''" >
	               <xsl:if test="substring($VerifySummaryIdent, 3, 2) = 'A1'">
	                  <a href="#BUSADDRVERIFYB1A1">details</a>
	               </xsl:if>
	               <xsl:if test="substring($VerifySummaryIdent, 3, 2) = 'A2'">
	                  <a href="#BUSADDRVERIFYB1A2">details</a>
	               </xsl:if>
               </xsl:if>
              </font>
            </td>
          </tr>
          </xsl:if>
       </xsl:if>

       <xsl:if test="$VerifySummaryItem = 'VERIFYADDRTYPE'">
          <xsl:if test="prd:AddrTypeMsg != '' ">
          <tr style="background-color:expression(this.previousSibling.style.backgroundColor == 'white'? '#E5F5FA':'white')">
            <td height="20" width="1%">
               <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

            <td width="30%" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">Address Type Result:</font></td>

            <td width="69%"><font size="1" style="FONT-FAMILY: 'verdana'; text-transform: uppercase; "><xsl:value-of select="$AddrTypeMsg" /></font></td>
          </tr>
          </xsl:if>
       </xsl:if>

       <xsl:if test="$VerifySummaryItem = 'ADDRMISMATCH'">
          <xsl:if test="prd:AddrUnitMismatchCode != '' ">
          <tr style="background-color:expression(this.previousSibling.style.backgroundColor == 'white'? '#E5F5FA':'white')">
            <td height="20" width="1%">
               <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

            <td width="30%" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">Address Unit Mismatch Result:</font></td>

            <td width="69%"><font size="1" style="FONT-FAMILY: 'verdana'; text-transform: uppercase; "><xsl:value-of select="$AddrUnitMismatchMsg" /></font></td>
          </tr>
          </xsl:if>
       </xsl:if>

       <xsl:if test="$VerifySummaryItem = 'VERIFYPHONE'">
          <xsl:if test="prd:PhnMsg != '' ">
          <tr style="background-color:expression(this.previousSibling.style.backgroundColor == 'white'? '#E5F5FA':'white')">
            <td height="20" width="1%">
               <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

            <td width="30%" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">Phone Verification Result:</font></td>

            <td width="69%"><font size="1" style="FONT-FAMILY: 'verdana'; text-transform: uppercase; "><xsl:value-of select="$PhnMsg" /><xsl:text disable-output-escaping="yes">&#160;</xsl:text>
              </font><font size="1" style="FONT-FAMILY: 'verdana'; ">
               <xsl:if test="../prd:BusPhn[@ident=$VerifySummaryIdent][1]/prd:PhnNbr != ''" >
	               <xsl:if test="substring($VerifySummaryIdent, 3, 2) = 'A1'">
	                  <a href="#BUSPHONEVERIFYB1A1">details</a>
	               </xsl:if>
	               <xsl:if test="substring($VerifySummaryIdent, 3, 2) = 'A2'">
	                  <a href="#BUSPHONEVERIFYB1A2">details</a>
	               </xsl:if>
               </xsl:if>
              </font>
            </td>
          </tr>
          </xsl:if>
       </xsl:if>

       <xsl:if test="$VerifySummaryItem = 'PHONEMISMATCH'">
          <xsl:if test="prd:PhnUnitMismatchCode != '' ">
          <tr style="background-color:expression(this.previousSibling.style.backgroundColor == 'white'? '#E5F5FA':'white')">
            <td height="20" width="1%">
               <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

            <td width="30%" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">Phone Unit Mismatch Result:</font></td>

            <td width="69%"><font size="1" style="FONT-FAMILY: 'verdana'; text-transform: uppercase; "><xsl:value-of select="$PhnUnitMismatchMsg" /></font></td>
          </tr>
          </xsl:if>
       </xsl:if>

       <xsl:if test="$VerifySummaryItem = 'VERIFYOFAC'">
          <xsl:if test="prd:OFACMsg != '' ">
          <tr style="background-color:expression(this.previousSibling.style.backgroundColor == 'white'? '#E5F5FA':'white')">
            <td height="20" width="1%">
               <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

            <td width="30%" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">OFAC Result:</font></td>

            <td width="69%"><font size="1" style="FONT-FAMILY: 'verdana'; text-transform: uppercase; "><xsl:value-of select="$OFACMsg" /><xsl:text disable-output-escaping="yes">&#160;</xsl:text>
              </font><font size="1" style="FONT-FAMILY: 'verdana'; ">
               <xsl:if test="number(../prd:CountsSummary[@ident=$VerifySummaryIdent]/prd:OFACRecs) &gt; 0" >
	               <xsl:if test="substring($VerifySummaryIdent, 3, 2) = 'A1'">
	                  <a href="#BUSOFACVALIDATEB1A1">details</a>
	               </xsl:if>
	               <xsl:if test="substring($VerifySummaryIdent, 3, 2) = 'A2'">
	                  <a href="#BUSOFACVALIDATEB1A2">details</a>
	               </xsl:if>
               </xsl:if>
              </font>
            </td>
          </tr>
          </xsl:if>
       </xsl:if>

       <xsl:if test="$VerifySummaryItem = 'VERIFYTAXID'">
          <xsl:if test="prd:TaxIDMsg != '' ">
          <tr style="background-color:expression(this.previousSibling.style.backgroundColor == 'white'? '#E5F5FA':'white')">
            <td height="20" width="1%">
               <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

            <td width="30%" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">Tax ID Verification Result:</font></td>

            <td width="69%"><font size="1" style="FONT-FAMILY: 'verdana'; text-transform: uppercase; "><xsl:value-of select="$TaxIDMsg" /><xsl:text disable-output-escaping="yes">&#160;</xsl:text>
              </font><font size="1" style="FONT-FAMILY: 'verdana'; ">
               <xsl:if test="number(../prd:CountsSummary[@ident=$VerifySummaryIdent]/prd:TaxIDRecs) &gt; 0" >
	               <xsl:if test="substring($VerifySummaryIdent, 3, 2) = 'A1'">
	                  <a href="#BUSTAXIDVERIFYB1A1">details</a>
	               </xsl:if>
	               <xsl:if test="substring($VerifySummaryIdent, 3, 2) = 'A2'">
	                  <a href="#BUSTAXIDVERIFYB1A2">details</a>
	               </xsl:if>
               </xsl:if>
              </font>
            </td>
          </tr>
          </xsl:if>
       </xsl:if>
    </xsl:if>

</xsl:template>


  <!--
  *********************************************
  * historical validation match ind template
  *********************************************
  -->
 <xsl:template name="CFIMatchIndicators">        
    <xsl:param name="HistoricalValidationIdent" />

    <xsl:if test="$HistoricalValidationIdent = 'B1A1' or $HistoricalValidationIdent = 'B2A1'  ">

       <xsl:if test="count(prd:HistoricalValidation/prd:MatchIndicators[prd:BusNameMatch='Y']) > 0 ">
       <tr style="background-color:expression(this.previousSibling.style.backgroundColor == 'white'? '#E5F5FA':'white')">
         <td height="20" width="1%">
            <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

         <td width="30%" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">Historical Match Result:</font></td>

         <td width="69%"><font size="1" style="FONT-FAMILY: 'verdana'; ">MATCH TO BUSINESS NAME<xsl:text disable-output-escaping="yes">&#160;</xsl:text>
           <a href="#Historical_0">details</a>
           </font>
         </td>
       </tr>
       </xsl:if>

       <xsl:if test="count(prd:HistoricalValidation/prd:MatchIndicators[prd:AlternateNameMatch='Y']) > 0 ">
       <tr style="background-color:expression(this.previousSibling.style.backgroundColor == 'white'? '#E5F5FA':'white')">
         <td height="20" width="1%">
            <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

         <td width="30%" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">Historical Match Result:</font></td>

         <td width="69%"><font size="1" style="FONT-FAMILY: 'verdana'; ">MATCH TO ADDITIONAL BUSINESS NAME<xsl:text disable-output-escaping="yes">&#160;</xsl:text>
           <a href="#Historical_0">details</a>
           </font>
         </td>
       </tr>
       </xsl:if>

       <xsl:if test="count(prd:HistoricalValidation/prd:MatchIndicators[prd:BusAddrMatch='Y']) > 0 ">
       <tr style="background-color:expression(this.previousSibling.style.backgroundColor == 'white'? '#E5F5FA':'white')">
         <td height="20" width="1%">
            <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

         <td width="30%" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">Historical Match Result:</font></td>

         <td width="69%"><font size="1" style="FONT-FAMILY: 'verdana'; ">MATCH TO BUSINESS ADDRESS<xsl:text disable-output-escaping="yes">&#160;</xsl:text>
           <a href="#Historical_0">details</a>
           </font>
         </td>
       </tr>
       </xsl:if>

       <xsl:if test="count(prd:HistoricalValidation/prd:MatchIndicators[prd:AlternateAddrMatch='Y']) > 0 ">
       <tr style="background-color:expression(this.previousSibling.style.backgroundColor == 'white'? '#E5F5FA':'white')">
         <td height="20" width="1%">
            <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

         <td width="30%" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">Historical Match Result:</font></td>

         <td width="69%"><font size="1" style="FONT-FAMILY: 'verdana'; ">MATCH TO ADDITIONAL BUSINESS ADDRESS<xsl:text disable-output-escaping="yes">&#160;</xsl:text>
           <xsl:if test="count(prd:HistoricalValidation) &lt; 2"><a href="#Historical_0">details</a></xsl:if>
           <xsl:if test="count(prd:HistoricalValidation) &gt; 1"><a href="#Historical_1">details</a></xsl:if>
           </font>
         </td>
       </tr>
       </xsl:if>

       <xsl:if test="count(prd:HistoricalValidation/prd:MatchIndicators[prd:BusPhnNbrMatch='Y']) > 0 ">
       <tr style="background-color:expression(this.previousSibling.style.backgroundColor == 'white'? '#E5F5FA':'white')">
         <td height="20" width="1%">
            <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

         <td width="30%" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">Historical Match Result:</font></td>

         <td width="69%"><font size="1" style="FONT-FAMILY: 'verdana'; ">MATCH TO BUSINESS PHONE<xsl:text disable-output-escaping="yes">&#160;</xsl:text>
           <a href="#Historical_0">details</a>
           </font>
         </td>
       </tr>
       </xsl:if>

       <xsl:if test="count(prd:HistoricalValidation/prd:MatchIndicators[prd:TaxIDMatch='Y']) > 0 ">
       <tr style="background-color:expression(this.previousSibling.style.backgroundColor == 'white'? '#E5F5FA':'white')">
         <td height="20" width="1%">
            <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

         <td width="30%" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">Historical Match Result:</font></td>

         <td width="69%"><font size="1" style="FONT-FAMILY: 'verdana'; ">MATCH TO TAX ID<xsl:text disable-output-escaping="yes">&#160;</xsl:text>
           <a href="#Historical_0">details</a>
           </font>
         </td>
       </tr>
       </xsl:if>

    </xsl:if>

    <xsl:if test="$HistoricalValidationIdent = 'B1A2' or $HistoricalValidationIdent = 'B2A2'  ">

       <xsl:if test="count(prd:HistoricalValidation/prd:MatchIndicators[prd:AlternateAddrMatch='Y']) > 0 ">
       <tr style="background-color:expression(this.previousSibling.style.backgroundColor == 'white'? '#E5F5FA':'white')">
         <td height="20" width="1%">
            <img src="../images/global/spacer.gif" border="0" width="5" height="1" alt="" /></td>

         <td width="30%" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">Historical Match Result:</font></td>

         <td width="69%"><font size="1" style="FONT-FAMILY: 'verdana'; ">MATCH TO BUSINESS ADDRESS<xsl:text disable-output-escaping="yes">&#160;</xsl:text>
           <xsl:if test="count(prd:HistoricalValidation) &lt; 2"><a href="#Historical_0">details</a></xsl:if>
           <xsl:if test="count(prd:HistoricalValidation) &gt; 1"><a href="#Historical_1">details</a></xsl:if>
           </font>
         </td>
       </tr>
       </xsl:if>

    </xsl:if>

</xsl:template>

</xsl:stylesheet>