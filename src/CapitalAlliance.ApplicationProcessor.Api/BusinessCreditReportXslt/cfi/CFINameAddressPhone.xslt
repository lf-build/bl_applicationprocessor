
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rsp="http://www.experian.com/NetConnectResponse"
                xmlns:prd="http://www.experian.com/ARFResponse">

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


 <!--
 ******************************************************************
 * CFI Business name, address template
 ******************************************************************
 -->

 <xsl:template name="CFIBusinessNameAddress">        
  <xsl:param name="VerificationIdent" />
  <xsl:param name="request" />

    <xsl:variable name="inquiry">
      <xsl:value-of select="//prd:CommercialFraudInsight//prd:InputSummary/prd:Inquiry" />
    </xsl:variable>

    <!-- inquiry bus name -->
    <xsl:variable name="inqBusName">
      <xsl:choose>
        <xsl:when test="contains($inquiry, 'NARQ/') ">
          <xsl:value-of select="substring-before(substring-after($inquiry, 'NARQ/'),';')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- inquiry street address -->
    <xsl:variable name="inqStreetAddress">
      <xsl:choose>
        <xsl:when test="contains($inquiry, 'CA-') ">
          <xsl:value-of select="substring-before(substring-after($inquiry, 'CA-'),'/')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- inquiry city, state, zip buffer -->
    <xsl:variable name="tmpBuffer1">
      <xsl:choose>
        <xsl:when test="contains($inquiry, 'CA-') ">
          <xsl:value-of select="substring-before(substring-after(substring-after($inquiry, 'CA-'),'/'), '/')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- buffer length -->
    <xsl:variable name="bufferLen">
       <xsl:value-of select="string-length(normalize-space($tmpBuffer1))"/>
    </xsl:variable>

    <!-- inquiry city, state, zip -->
    <xsl:variable name="inqCityStateZip">
      <xsl:choose>
         <xsl:when test="string(number(substring(normalize-space($tmpBuffer1), $bufferLen - 4))) != 'NaN' and number(substring(normalize-space($tmpBuffer1), $bufferLen - 4)) > 0">
          <xsl:value-of select="concat(substring($tmpBuffer1, 1, $bufferLen - 9), ', ', substring($tmpBuffer1, $bufferLen - 8)) " />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="bnTag">
      <xsl:choose>
        <xsl:when test="contains($inquiry, '/BN-') ">
          <xsl:value-of select="substring-before(substring-after($inquiry, '/BN-'),';')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- inquiry dba -->
    <xsl:variable name="dba">
      <xsl:choose>
        <xsl:when test=" normalize-space($bnTag) != normalize-space(prd:BusName) ">
          <xsl:value-of select="$bnTag" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="phTag">
      <xsl:choose>
        <xsl:when test="contains($inquiry, '/PH-') ">
          <xsl:value-of select="substring-before(substring-after($inquiry, '/PH-'),'/')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- inquiry phone -->
    <xsl:variable name="inqPhone">
      <xsl:choose>
        <xsl:when test="$phTag and normalize-space($phTag) != '' ">
          <xsl:value-of select="concat('(', substring($phTag, 1, 3), ') ', substring($phTag, 4, 3), '-', substring($phTag, 7))" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="tidTag">
      <xsl:choose>
        <xsl:when test="contains($inquiry, '/TID-1;') ">
          <xsl:value-of select="substring-before(substring-after($inquiry, '/TID-1;'),'/')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- inquiry tax id -->
     <xsl:variable name="inqTaxID">
      <xsl:choose>
        <xsl:when test="$tidTag and normalize-space($tidTag) != '' ">
          <xsl:value-of select="concat(substring($tidTag, 1, 2), '-', substring($tidTag, 3))" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="inqALTStreetAddress">
      <xsl:choose>
        <xsl:when test="contains($inquiry, ';3/CA-') ">
          <xsl:value-of select="substring-before(substring-after($inquiry, ';3/CA-'),'/')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- inquiry ALT city, state, zip buffer -->
    <xsl:variable name="tmpBuffer2">
      <xsl:choose>
        <xsl:when test="contains($inquiry, ';3/CA-') ">
          <xsl:value-of select="substring-before(substring-after(substring-after($inquiry, ';3/CA-'),'/'), '/')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- buffer length -->
    <xsl:variable name="buffer2Len">
       <xsl:value-of select="string-length(normalize-space($tmpBuffer2))"/>
    </xsl:variable>

    <!-- inquiry ALT city, state, zip -->
    <xsl:variable name="inqALTCityStateZip">
      <xsl:choose>
         <xsl:when test="string(number(substring(normalize-space($tmpBuffer2), $buffer2Len - 4))) != 'NaN' and number(substring(normalize-space($tmpBuffer2), $buffer2Len - 4)) > 0">
          <xsl:value-of select="concat(substring($tmpBuffer2, 1, $buffer2Len - 9), ', ', substring($tmpBuffer2, $buffer2Len - 8)) " />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- inquiry #3 street address -->
    <xsl:variable name="inqN3StreetAddress">
      <xsl:choose>
        <xsl:when test="contains($inquiry, 'HA-') ">
          <xsl:value-of select="substring-before(substring-after($inquiry, 'HA-'),'/')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- #3&4 inquiry -->
    <xsl:variable name="inquiryN34">
      <xsl:choose>
        <xsl:when test="contains($inquiry, 'HA-') ">
          <xsl:value-of select="substring-after($inquiry, 'HA-')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- inquiry #3 city, state, zip buffer -->
    <xsl:variable name="tmpBuffer3">
      <xsl:choose>
        <xsl:when test="contains($inquiryN34, 'HA-') ">
          <xsl:value-of select="substring-before(substring-after($inquiryN34, '/'), '/')" />
        </xsl:when>

        <xsl:when test="not(contains($inquiryN34, 'HA-')) ">
          <xsl:value-of select="substring-after($inquiryN34, '/')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- buffer length -->
    <xsl:variable name="buffer3Len">
       <xsl:value-of select="string-length(normalize-space($tmpBuffer3))"/>
    </xsl:variable>

    <!-- inquiry #3 city, state, zip -->
    <xsl:variable name="inqN3CityStateZip">
      <xsl:choose>
         <xsl:when test="string(number(substring(normalize-space($tmpBuffer3), $buffer3Len - 4))) != 'NaN' and number(substring(normalize-space($tmpBuffer3), $buffer3Len - 4)) > 0">
          <xsl:value-of select="concat(substring($tmpBuffer3, 1, $buffer3Len - 9), ', ', substring($tmpBuffer3, $buffer3Len - 8)) " />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- inquiry #4 street address -->
    <xsl:variable name="inqN4StreetAddress">
      <xsl:choose>
        <xsl:when test="contains($inquiryN34, 'HA-') ">
          <xsl:value-of select="substring-before(substring-after($inquiryN34, 'HA-'),'/')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- inquiry #4 city, state, zip buffer -->
    <xsl:variable name="tmpBuffer4">
      <xsl:choose>
        <xsl:when test="contains($inquiryN34, 'HA-') ">
          <xsl:value-of select="substring-after(substring-after($inquiryN34, 'HA-'), '/')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- buffer length -->
    <xsl:variable name="buffer4Len">
       <xsl:value-of select="string-length(normalize-space($tmpBuffer4))"/>
    </xsl:variable>

    <!-- inquiry #4 city, state, zip -->
    <xsl:variable name="inqN4CityStateZip">
      <xsl:choose>
         <xsl:when test="string(number(substring(normalize-space($tmpBuffer4), $buffer4Len - 4))) != 'NaN' and number(substring(normalize-space($tmpBuffer4), $buffer4Len - 4)) > 0">
          <xsl:value-of select="concat(substring($tmpBuffer4, 1, $buffer4Len - 9), ', ', substring($tmpBuffer4, $buffer4Len - 8)) " />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- bus name -->
    <xsl:variable name="busName">
       <xsl:value-of select="//prd:CommercialFraudInsight/prd:StdAddr[@type='Business'][@ident=$VerificationIdent]/prd:BusName" />
    </xsl:variable>

    <!-- street address -->
    <xsl:variable name="streetAddress">
       <xsl:value-of select="//prd:CommercialFraudInsight/prd:StdAddr[@type='Business'][@ident=$VerificationIdent]/prd:Addr" />
    </xsl:variable>

    <!-- city -->
    <xsl:variable name="city">
       <xsl:value-of select="//prd:CommercialFraudInsight/prd:StdAddr[@type='Business'][@ident=$VerificationIdent]/prd:City" />
    </xsl:variable>

    <!-- state -->
    <xsl:variable name="state">
       <xsl:value-of select="//prd:CommercialFraudInsight/prd:StdAddr[@type='Business'][@ident=$VerificationIdent]/prd:State" />
    </xsl:variable>

    <!-- zip -->
    <xsl:variable name="zip">
      <xsl:choose>
         <xsl:when test="//prd:CommercialFraudInsight/prd:StdAddr[@type='Business'][@ident=$VerificationIdent]/prd:Zip4">
            <xsl:value-of select="concat(//prd:CommercialFraudInsight/prd:StdAddr[@type='Business'][@ident=$VerificationIdent]/prd:Zip, '-', //prd:CommercialFraudInsight/prd:StdAddr[@type='Business'][@ident=$VerificationIdent]/prd:Zip4)" />
         </xsl:when>

         <xsl:otherwise>
            <xsl:value-of select="//prd:CommercialFraudInsight/prd:StdAddr[@type='Business'][@ident=$VerificationIdent]/prd:Zip" />
         </xsl:otherwise>
       </xsl:choose>
    </xsl:variable>

    <!-- city, state, zip -->
    <xsl:variable name="cityStateZip">
       <xsl:value-of select="concat(normalize-space($city), ', ', normalize-space($state), '  ', normalize-space($zip)  )" />
    </xsl:variable>

    <!-- area code -->
    <xsl:variable name="areacode">
      <xsl:choose>
        <xsl:when test="string-length(normalize-space(//prd:CommercialFraudInsight/prd:BusAddr[@type='Business'][@ident='B1A1']/prd:AreaCode)) > 0">
          <xsl:value-of select="normalize-space(//prd:CommercialFraudInsight/prd:BusAddr[@type='Business'][@ident='B1A1']/prd:AreaCode)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- phone -->
    <xsl:variable name="phone7">
      <xsl:choose>
        <xsl:when test="//prd:CommercialFraudInsight/prd:BusAddr[@type='Business'][@ident='B1A1']/prd:PhnNbr and string-length(normalize-space(//prd:CommercialFraudInsight/prd:BusAddr[@type='Business'][@ident='B1A1']/prd:PhnNbr)) = 7">
          <xsl:value-of select="normalize-space(//prd:CommercialFraudInsight/prd:BusAddr[@type='Business'][@ident='B1A1']/prd:PhnNbr)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- full phone -->
    <xsl:variable name="phone">
      <xsl:choose>
        <xsl:when test="$areacode and $phone7">
          <xsl:value-of select="concat('(', $areacode, ') ', substring($phone7, 1, 3), '-', substring($phone7, 4))" />
        </xsl:when>

        <xsl:when test="not ($areacode) and $phone7">
          <xsl:value-of select="concat(substring($phone7, 1, 3), '-', substring($phone7, 4))" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- ALT streetAddress -->
    <xsl:variable name="ALTstreetAddress">
	<xsl:value-of select="//prd:CommercialFraudInsight/prd:StdAddr[@type='Business'][@ident='B1A2']/prd:Addr" />
    </xsl:variable>

    <!-- ALT city -->
    <xsl:variable name="ALTcity">
	<xsl:value-of select="normalize-space(//prd:CommercialFraudInsight/prd:StdAddr[@type='Business'][@ident='B1A2']/prd:City)" />
    </xsl:variable>

    <!-- ALT state -->
    <xsl:variable name="ALTstate">
	<xsl:value-of select="normalize-space(//prd:CommercialFraudInsight/prd:StdAddr[@type='Business'][@ident='B1A2']/prd:State)" />
    </xsl:variable>

    <!-- ALT zip -->
    <xsl:variable name="ALTzip">
      <xsl:choose>
        <xsl:when test="//prd:CommercialFraudInsight/prd:StdAddr[@type='Business'][@ident='B1A2']/prd:Zip4">
          <xsl:value-of select="concat(//prd:CommercialFraudInsight/prd:StdAddr[@type='Business'][@ident='B1A2']/prd:Zip, '-', //prd:CommercialFraudInsight/prd:StdAddr[@type='Business'][@ident='B1A2']/prd:Zip4)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="//prd:CommercialFraudInsight/prd:StdAddr[@type='Business'][@ident='B1A2']/prd:Zip" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- ALT city, state, zip -->
    <xsl:variable name="ALTCityStateZip">
       <xsl:value-of select="concat(normalize-space($ALTcity), ', ', normalize-space($ALTstate), '  ', normalize-space($ALTzip)  )" />
    </xsl:variable>

<!--
(<xsl:value-of select="$inquiryN34"/>)<br/>
(<xsl:value-of select="contains($inquiryN34, 'HA-')"/>)<br/>
(<xsl:value-of select="not(contains($inquiryN34, 'HA-'))"/>)<br/>
-->

    <xsl:choose>
      <xsl:when test="normalize-space($request) = 'BUSNAME' ">
	      <xsl:choose>
	        <xsl:when test="$busName and normalize-space($busName) != '' ">
	          <xsl:value-of select="$busName" />
	        </xsl:when>
	
	        <xsl:otherwise>
	          <xsl:value-of select="$inqBusName" />
	        </xsl:otherwise>
	      </xsl:choose>
      </xsl:when>

      <xsl:when test="normalize-space($request) = 'DBA' ">
        <xsl:value-of select="$dba" />
      </xsl:when>

      <xsl:when test="normalize-space($request) = 'PRIADDR' ">
	      <xsl:choose>
	        <xsl:when test="$streetAddress and normalize-space($streetAddress) != '' and substring($VerificationIdent, 3, 2) = 'A1' ">
	          <xsl:value-of select="$streetAddress" />
	        </xsl:when>
	
	        <xsl:otherwise>
	          <xsl:value-of select="$inqStreetAddress" />
	        </xsl:otherwise>
	      </xsl:choose>
      </xsl:when>

      <xsl:when test="normalize-space($request) = 'PRICSZ'  ">
	      <xsl:choose>
	        <xsl:when test="$city and $state and normalize-space($city) != '' and normalize-space($state) != '' and substring($VerificationIdent, 3, 2) = 'A1' ">
	          <xsl:value-of select="$cityStateZip" />
	        </xsl:when>
	
	        <xsl:otherwise>
	          <xsl:value-of select="$inqCityStateZip" />
	        </xsl:otherwise>
	      </xsl:choose>
      </xsl:when>

      <xsl:when test="normalize-space($request) = 'NAMEADDRCSZ' ">
	 <!-- temp bus name -->
	 <xsl:variable name="tmpBusName">
	      <xsl:choose>
	        <xsl:when test="substring(normalize-space($VerificationIdent), 1, 2) = 'B1' ">
		      <xsl:choose>
		        <xsl:when test="$busName and normalize-space($busName) != '' ">
		          <xsl:value-of select="$busName " />
		        </xsl:when>
		
		        <xsl:otherwise>
		          <xsl:value-of select="$inqBusName" />
		        </xsl:otherwise>
		      </xsl:choose>
	        </xsl:when>
	
	        <xsl:otherwise>
	          <xsl:value-of select="$dba" />
	        </xsl:otherwise>
	      </xsl:choose>
	 </xsl:variable>

        <xsl:choose>
          <xsl:when test="$VerificationIdent = 'B1A1' or $VerificationIdent = 'B2A1' ">
	      <xsl:choose>
	        <xsl:when test="$streetAddress and $city and $state and normalize-space($streetAddress) != '' and normalize-space($city) != '' and normalize-space($state) != '' ">
	          <xsl:value-of select="concat($tmpBusName, ', ', $streetAddress, ', ', $cityStateZip)" />
	        </xsl:when>
	
	        <xsl:otherwise>
	          <xsl:value-of select="concat($tmpBusName, ', ', $inqStreetAddress, ', ', $inqCityStateZip)" />
	        </xsl:otherwise>
	      </xsl:choose>
          </xsl:when>
          
          <xsl:otherwise>
	      <xsl:choose>
	        <xsl:when test="$ALTstreetAddress and $ALTcity and $ALTstate and normalize-space($ALTstreetAddress) != '' and normalize-space($ALTcity) != '' and normalize-space($ALTstate) != '' ">
	          <xsl:value-of select="concat($tmpBusName, ', ', $ALTstreetAddress, ', ', $ALTCityStateZip)" />
	        </xsl:when>
	
	        <xsl:otherwise>
	          <xsl:value-of select="concat($tmpBusName, ', ', $inqALTStreetAddress, ', ', $inqALTCityStateZip)" />
	        </xsl:otherwise>
	      </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>

      <xsl:when test="normalize-space($request) = 'ADDRCSZ' ">
        <xsl:choose>
          <xsl:when test="$VerificationIdent = 'B1A1' or $VerificationIdent = 'B2A1' ">
	      <xsl:choose>
	        <xsl:when test="$streetAddress and $city and $state and normalize-space($streetAddress) != '' and normalize-space($city) != '' and normalize-space($state) != '' ">
	          <xsl:value-of select="concat($streetAddress, ', ', $cityStateZip)" />
	        </xsl:when>
	
	        <xsl:otherwise>
	          <xsl:value-of select="concat($inqStreetAddress, ', ', $inqCityStateZip)" />
	        </xsl:otherwise>
	      </xsl:choose>
          </xsl:when>
          
          <xsl:otherwise>
	      <xsl:choose>
	        <xsl:when test="$ALTstreetAddress and $ALTcity and $ALTstate and normalize-space($ALTstreetAddress) != '' and normalize-space($ALTcity) != '' and normalize-space($ALTstate) != '' ">
	          <xsl:value-of select="concat($ALTstreetAddress, ', ', $ALTCityStateZip)" />
	        </xsl:when>
	
	        <xsl:otherwise>
	          <xsl:value-of select="concat($inqALTStreetAddress, ', ', $inqALTCityStateZip)" />
	        </xsl:otherwise>
	      </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>

      <xsl:when test="normalize-space($request) = 'PRIPHONE' ">
	      <xsl:choose>
	        <xsl:when test="$areacode and $phone and normalize-space($areacode) != '' and normalize-space($phone) != '' ">
	          <xsl:value-of select="$phone" />
	        </xsl:when>
	
	        <xsl:otherwise>
	          <xsl:value-of select="$inqPhone" />
	        </xsl:otherwise>
	      </xsl:choose>
      </xsl:when>

      <xsl:when test="normalize-space($request) = 'PRITAXID' ">
        <xsl:value-of select="$inqTaxID" />
      </xsl:when>

      <xsl:when test="normalize-space($request) = 'ALTADDR' ">
	      <xsl:choose>
	        <xsl:when test="$ALTstreetAddress and normalize-space($ALTstreetAddress) != '' ">
	          <xsl:value-of select="$ALTstreetAddress" />
	        </xsl:when>
	
	        <xsl:otherwise>
	          <xsl:value-of select="$inqALTStreetAddress" />
	        </xsl:otherwise>
	      </xsl:choose>
      </xsl:when>

      <xsl:when test="normalize-space($request) = 'ALTCSZ' ">
	      <xsl:choose>
	        <xsl:when test="$ALTcity and $ALTstate and normalize-space($ALTcity) != '' and normalize-space($ALTstate) != '' ">
	          <xsl:value-of select="$ALTCityStateZip" />
	        </xsl:when>
	
	        <xsl:otherwise>
	          <xsl:value-of select="$inqALTCityStateZip" />
	        </xsl:otherwise>
	      </xsl:choose>
      </xsl:when>

      <xsl:when test="normalize-space($request) = 'N3ADDR' ">
        <xsl:value-of select="$inqN3StreetAddress" />
      </xsl:when>

      <xsl:when test="normalize-space($request) = 'N3CSZ' ">
        <xsl:value-of select="$inqN3CityStateZip" />
      </xsl:when>

      <xsl:when test="normalize-space($request) = 'N4ADDR' ">
        <xsl:value-of select="$inqN4StreetAddress" />
      </xsl:when>

      <xsl:when test="normalize-space($request) = 'N4CSZ' ">
        <xsl:value-of select="$inqN4CityStateZip" />
      </xsl:when>

      <xsl:when test="normalize-space($request) = 'ADDR' ">
        <xsl:value-of select="concat(normalize-space($streetAddress), ', ', normalize-space($city), ', ', normalize-space($state), '  ', normalize-space($zip)  )" />
      </xsl:when>

      <xsl:when test="normalize-space($request) = 'CITY' ">
        <xsl:value-of select="concat(normalize-space($city), ', ', normalize-space($state), '  ', normalize-space($zip)  )" />
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="concat(normalize-space($busName), ', ', normalize-space($streetAddress), ', ', normalize-space($city), ', ', normalize-space($state), '  ', normalize-space($zip)  )" />
      </xsl:otherwise>
    </xsl:choose>
    
 </xsl:template>


 <!--
 ******************************************************************
 * CFI Business match ident
 ******************************************************************
 -->
 <xsl:template name="CFIBusinessMatch">        

    <!-- parse AddrCode -->
    <xsl:variable name="parseAddrCode">
	   <xsl:for-each select="prd:VerifySummary/prd:AddrCode">
	      <xsl:choose>
	        <xsl:when test="substring(normalize-space(.),1,1) = 'B'">
	          <xsl:value-of select="../@ident" />
	        </xsl:when>
	
	        <xsl:otherwise>
	          <xsl:value-of select="''" />
	        </xsl:otherwise>
	      </xsl:choose>
	   </xsl:for-each>
    </xsl:variable>

    <!-- match ident -->
     <xsl:variable name="matchIdent">
      <xsl:choose>
        <xsl:when test="$parseAddrCode and normalize-space($parseAddrCode) != '' and string-length($parseAddrCode) &gt; 3">
          <xsl:value-of select="substring($parseAddrCode, 1, 4)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'B1A1'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:value-of select="$matchIdent" />

 </xsl:template>      

</xsl:stylesheet>