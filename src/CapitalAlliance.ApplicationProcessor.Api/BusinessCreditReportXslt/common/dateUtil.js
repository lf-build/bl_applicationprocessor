function dateConvertMYY(date, idx)
 {
	var year = date.substring(0, 4);
	var month = date.substring(4, 6);
	
	//format as mm/dd/yyyy
 	var sDate = month+"/01/"+year;

  //var d = new Date("01/01/2015");
  //d.setMonth(d.getMonth()-13);

	var fDate = new Date(sDate);
	fDate.setMonth(fDate.getMonth()-idx-1);

	//alert("idx="+idx+" - sDate="+sDate+" - fDate="+fDate+" - refMonth="+refMonth);
	switch (fDate.getMonth()) {
     case 0:
     case 5:
     case 6:
         sDate = "J"+fDate.getFullYear().toString().substring(2,4);
         break;
     case 1:
         sDate = "F"+fDate.getFullYear().toString().substring(2,4);
         break;
     case 2:
     case 4:
         sDate = "M"+fDate.getFullYear().toString().substring(2,4);
         break;
     case 3:
     case 7:
         sDate = "A"+fDate.getFullYear().toString().substring(2,4);
         break;
     case 8:
         sDate = "S"+fDate.getFullYear().toString().substring(2,4);
         break;
     case 9:
         sDate = "O"+fDate.getFullYear().toString().substring(2,4);
         break;
     case 10:
         sDate = "N"+fDate.getFullYear().toString().substring(2,4);
         break;
     case 11:
         sDate = "D"+fDate.getFullYear().toString().substring(2,4);
         break;
 } 
 //alert("dateConvertMYY: sDate="+sDate);
 	return sDate;
 }