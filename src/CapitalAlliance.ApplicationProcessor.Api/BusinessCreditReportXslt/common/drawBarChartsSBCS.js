google.load("visualization", "1", {packages:["corechart"], "callback" : googleVisualizationCallback});

function googleVisualizationCallback(){
	drawFAUTChart();
	drawRAUTChart();
	drawTAUTChart();
	drawADTChart();
	drawMDTChart();
	drawQDTChart();
};

// Draw the Financial Account Utilization Trend Chart
function drawFAUTChart() {
//alert("drawFAUTChart");

	var tableFAUT=document.getElementById("table_FAUT");
	var tableDate=document.getElementById("table_SBCSKeyMetrics_CurrentDate");
 	var cnt, dateMYY, pctUsed, sumPctUsed=0, bExists=false;
 	var currentDate = new Date();

	// FinancialAccounts should only occur once and exists if not null
	if (tableFAUT != null) {
 		bExists=true;
		var xmlFAUT=loadXMLString(tableFAUT.innerHTML);
		//alert("tableFAUT="+tableFAUT.innerHTML);
		// documentElement always represents the root node
		var xmlFAUTChild=xmlFAUT.documentElement.childNodes;
		cnt = xmlFAUTChild.length-1;
//alert("cnt="+cnt);
		var xmlDate = loadXMLString(tableDate.innerHTML); //Format: <CurrentDate>20150200</CurrentDate>
		currentDate = xmlDate.documentElement.childNodes[0].childNodes[0].nodeValue;

		// Create Google Charts DataTable
		var data = new google.visualization.DataTable();
		// Declare columns
		data.addColumn('string', 'Date');
		data.addColumn('number', 'PercentageUsed');
		data.addColumn({type: 'string', role: 'tooltip'});

 		for (i=0;i<cnt;i++){
 			dateMYY = dateConvertMYY(currentDate, i);
 			pctUsed = xmlFAUTChild[i].childNodes[0].nodeValue;
 			sumPctUsed = Number(sumPctUsed)+Number(pctUsed);
	 		//alert("i["+i+"]="+dateMYY+"-["+pctUsed+"]");
			data.addRow([dateMYY, Number(pctUsed)/100, dateMYY+', '+Number(pctUsed)+"%"]);
		}
	}

	if (bExists && Number(sumPctUsed)>0) {
 		var view = new google.visualization.DataView(data);
  	view.setColumns([0, 1,
                   { calc: "stringify",
                     sourceColumn: 1,
                     type: "string",
                     role: "annotation" }, 2]);

	  var options = {
	    title: '',
	    width: 685,
	    height: 130,
	    legend: { position: 'none' },
	    chart: { title: '',
	             subtitle: '' },
	    colors: ['#B23F05'],
	    annotations: {textStyle: {auraColor: '#fd7c16' }},
	    areaOpacity: 0.8,
	    chartArea: {left: '30', width: '640', backgroundColor: '#647300'},
	    vAxis: {format: '##%', textStyle: {fontSize: '8'}, minValue: 0, maxValue: 1, viewWindow: {max: 1}, gridlines: {count: 5}},
	    hAxis: {direction:-1, showTextEvery: 1, maxAlternation: 1, maxTextLines: 1, minTextSpacing: 1, textStyle: {fontSize: '8'}},
	    axes: {
	      x: {
	        0: { side: 'bottom', label: ''} // bottom x-axis.
	      },
	      y: {
	        0: { side: 'left', label: ''} // left side y-axis.
	      }
	    }
	  };

	  var chart = new google.visualization.AreaChart(document.getElementById("util_chart_FAUT"));
	  chart.draw(data, options);		
	}
};

// Draw the Revolving Account Utilization Trend Chart
function drawRAUTChart() {
//alert("drawRAUTChart");

	var tableRAUT=document.getElementById("table_RAUT");
	var tableDate=document.getElementById("table_SBCSKeyMetrics_CurrentDate");
 	var cnt, dateMYY, pctUsed, sumPctUsed=0, bExists=false;
 	var currentDate = new Date();

	// RevolvingAccounts should only occur once and exists if not null
	if (tableRAUT != null) {
 		bExists=true;
		var xmlRAUT=loadXMLString(tableRAUT.innerHTML);
		//alert("tableRAUT="+tableRAUT.innerHTML);
		// documentElement always represents the root node
		var xmlRAUTChild=xmlRAUT.documentElement.childNodes;
		cnt = xmlRAUTChild.length-1;
//alert("cnt="+cnt);
		var xmlDate = loadXMLString(tableDate.innerHTML); //Format: <CurrentDate>20150200</CurrentDate>
		currentDate = xmlDate.documentElement.childNodes[0].childNodes[0].nodeValue;

		// Create Google Charts DataTable
		var data = new google.visualization.DataTable();
		// Declare columns
		data.addColumn('string', 'Date');
		data.addColumn('number', 'PercentageUsed');
		data.addColumn({type: 'string', role: 'tooltip'});

 		for (i=0;i<cnt;i++){
 			dateMYY = dateConvertMYY(currentDate, i);
 			pctUsed = xmlRAUTChild[i].childNodes[0].nodeValue;
 			sumPctUsed = Number(sumPctUsed)+Number(pctUsed);
	 		//alert("i["+i+"]="+dateMYY+"-["+pctUsed+"]");
			data.addRow([dateMYY, Number(pctUsed)/100, dateMYY+', '+Number(pctUsed)+"%"]);
		}
	}

	if (bExists && Number(sumPctUsed)>0) {
 		var view = new google.visualization.DataView(data);
  	view.setColumns([0, 1,
                   { calc: "stringify",
                     sourceColumn: 1,
                     type: "string",
                     role: "annotation" }, 2]);

	  var options = {
	    title: '',
	    width: 685,
	    height: 130,
	    legend: { position: 'none' },
	    chart: { title: '',
	             subtitle: '' },
	    colors: ['#B23F05'],
	    areaOpacity: 0.8,
	    chartArea: {left: '30', width: '640', backgroundColor: '#647300'},
	    vAxis: {format: '##%', textStyle: {fontSize: '8'}, minValue: 0, maxValue: 1, viewWindow: {max: 1}, gridlines: {count: 5}},
	    hAxis: {direction:-1, showTextEvery: 1, maxAlternation: 1, maxTextLines: 1, minTextSpacing: 1, textStyle: {fontSize: '8'}},
	    axes: {
	      x: {
	        0: { side: 'bottom', label: ''} // bottom x-axis.
	      },
	      y: {
	        0: { side: 'left', label: ''} // left side y-axis.
	      }
	    }
	  };

	  var chart = new google.visualization.AreaChart(document.getElementById("util_chart_RAUT"));
	  chart.draw(data, options);		
	}
};

// Draw the Total Account Utilization Trend Chart
function drawTAUTChart() {
//alert("drawTAUTChart");

	var tableTAUT=document.getElementById("table_TAUT");
	var tableDate=document.getElementById("table_SBCSKeyMetrics_CurrentDate");
 	var cnt, dateMYY, pctUsed, sumPctUsed=0, bExists=false;
 	var currentDate = new Date();

	// TotalAccounts should only occur once and exists if not null
	if (tableTAUT != null) {
 		bExists=true;
		var xmlTAUT=loadXMLString(tableTAUT.innerHTML);
		//alert("tableTAUT="+tableTAUT.innerHTML);
		// documentElement always represents the root node
		var xmlTAUTChild=xmlTAUT.documentElement.childNodes;
		cnt = xmlTAUTChild.length-1;
//alert("cnt="+cnt);
		var xmlDate = loadXMLString(tableDate.innerHTML); //Format: <CurrentDate>20150200</CurrentDate>
		currentDate = xmlDate.documentElement.childNodes[0].childNodes[0].nodeValue;

		// Create Google Charts DataTable
		var data = new google.visualization.DataTable();
		// Declare columns
		data.addColumn('string', 'Date');
		data.addColumn('number', 'PercentageUsed');
		data.addColumn({type: 'string', role: 'tooltip'});

 		for (i=0;i<cnt;i++){
 			dateMYY = dateConvertMYY(currentDate, i);
 			pctUsed = xmlTAUTChild[i].childNodes[0].nodeValue;
 			sumPctUsed = Number(sumPctUsed)+Number(pctUsed);
	 		//alert("i["+i+"]="+dateMYY+"-["+pctUsed+"]");
			data.addRow([dateMYY, Number(pctUsed)/100, dateMYY+', '+Number(pctUsed)+"%"]);
		}
	}

	if (bExists && Number(sumPctUsed)>0) {
 		var view = new google.visualization.DataView(data);
  	view.setColumns([0, 1,
                   { calc: "stringify",
                     sourceColumn: 1,
                     type: "string",
                     role: "annotation" }, 2]);

	  var options = {
	    title: '',
	    width: 685,
	    height: 130,
	    legend: { position: 'none' },
	    chart: { title: '',
	             subtitle: '' },
	    colors: ['#B23F05'],
	    areaOpacity: 0.8,
	    chartArea: {left: '30', width: '640', backgroundColor: '#647300'},
	    vAxis: {format: '##%', textStyle: {fontSize: '8'}, minValue: 0, maxValue: 1, viewWindow: {max: 1}, gridlines: {count: 5}},
	    hAxis: {direction:-1, showTextEvery: 1, maxAlternation: 1, maxTextLines: 1, minTextSpacing: 1, textStyle: {fontSize: '8'}},
	    axes: {
	      x: {
	        0: { side: 'bottom', label: ''} // bottom x-axis.
	      },
	      y: {
	        0: { side: 'left', label: ''} // left side y-axis.
	      }
	    }
	  };

	  var chart = new google.visualization.AreaChart(document.getElementById("util_chart_TAUT"));
	  chart.draw(data, options);		
	}
};

// Draw the Average DBT Trend (Non-financial Accounts) Chart
function drawADTChart() {
//alert("drawADTChart");

	var tableADT=document.getElementById("table_ADT");
	var tableDate=document.getElementById("table_SBCSKeyMetrics_CurrentDate");
 	var cnt, dateMYY, dbt, bExists=false;
 	var currentDate = new Date();

	// DBTHistory should only occur once and exists if not null
	if (tableADT != null) {
 		bExists=true;
		var xmlADT=loadXMLString(tableADT.innerHTML);
		//alert("tableADT="+tableADT.innerHTML);
		// documentElement always represents the root node
		var xmlADTChild=xmlADT.documentElement.childNodes;
		cnt = xmlADTChild.length-1;
//alert("cnt="+cnt);
		var xmlDate = loadXMLString(tableDate.innerHTML); //Format: <CurrentDate>20150200</CurrentDate>
		currentDate = xmlDate.documentElement.childNodes[0].childNodes[0].nodeValue;

		// Create Google Charts DataTable
		var data = new google.visualization.DataTable();
		// Declare columns
		data.addColumn('string', 'Date');
		data.addColumn('number', 'DBT');
		data.addColumn({type: 'string', role: 'tooltip'});

 		for (i=0;i<cnt;i++){
 			dateMYY = dateConvertMYY(currentDate, i);
 			dbt = xmlADTChild[i].childNodes[0].nodeValue;
	 		//alert("i["+i+"]="+dateMYY+"-["+dbt+"]");
			data.addRow([dateMYY, Number(dbt), dateMYY+', '+Number(dbt)]);
		}
	}

	if (bExists) {
 		var view = new google.visualization.DataView(data);
  	view.setColumns([0, 1,
                   { calc: "stringify",
                     sourceColumn: 1,
                     type: "string",
                     role: "annotation" }, 2]);

	  var options = {
	    title: '',
	    width: 685,
	    height: 130,
	    legend: { position: 'none' },
	    chart: { title: '',
	             subtitle: '' },
	    colors: ['#B23F05'],
	    areaOpacity: 0.8,
	    chartArea: {left: '30', width: '640', backgroundColor: '#647300'},
	    vAxis: {textStyle: {fontSize: '8'}, minValue: 0, maxValue: 120, viewWindow: {max: 120}, gridlines: {count: 5}},
	    hAxis: {direction:-1, showTextEvery: 1, maxAlternation: 1, maxTextLines: 1, minTextSpacing: 1, textStyle: {fontSize: '8'}},
	    axes: {
	      x: {
	        0: { side: 'bottom', label: ''} // bottom x-axis.
	      },
	      y: {
	        0: { side: 'left', label: ''} // left side y-axis.
	      }
	    }
	  };

	  var chart = new google.visualization.AreaChart(document.getElementById("util_chart_ADT"));
	  chart.draw(data, options);		
	}
};

// Draw the Monthly DBT Trends Chart
function drawMDTChart() {
//alert("drawMDTChart");

	var tableMDT=document.getElementById("table_MDT");
 	var cnt, dateMMMYY, DBT, bExists=false;

	// SBCSPaymentTrends should only occur once and exists if not null
	if (tableMDT != null) {
 		bExists=true;
		var xmlMDT=loadXMLString(tableMDT.innerHTML);
		//alert("tableMDT="+tableMDT.innerHTML);
		// documentElement always represents the root node
		var xmlMDTChild=xmlMDT.documentElement.childNodes;
		cnt = xmlMDTChild.length;
		
		// Create Google Charts DataTable
		var data = new google.visualization.DataTable();
		// Declare columns
		data.addColumn('string', 'Date');
		data.addColumn('number', 'DBT');
		data.addColumn({type: 'string', role: 'tooltip'});
 		
 		for (i=0;i<cnt;i++){
 			dateMMMYY = xmlMDTChild[i].childNodes[0].childNodes[0].nodeValue;
 			DBT = xmlMDTChild[i].childNodes[1].childNodes[0].nodeValue;
	 		//alert("i["+i+"]="+dateMMMYY+"-["+DBT+"]");
				data.addRow([dateMMMYY, Number(DBT), dateMMMYY+', '+Number(DBT)]);
		}
	}

	if (bExists) {
 		var view = new google.visualization.DataView(data);
  	view.setColumns([0, 1,
                   { calc: "stringify",
                     sourceColumn: 1,
                     type: "string",
                     role: "annotation" }, 2]);

	  var options = {
	    title: '',
	    width: 340,
	    legend: { position: 'none' },
	    chart: { title: '',
	             subtitle: '' },
	    annotations: {alwaysOutside: true, textStyle: {color: 'black'}},
	    chartArea: {height: '70%', width: '85%'},
	    colors: ['#647300'],
	    bars: 'horizontal', // Required for Material Bar Charts.
	    vAxis: {minValue: 0, maxValue: 120, viewWindow: {max: 120}, gridlines: {count: 6}},
	    hAxis: {direction:-1, textStyle: {fontSize: 9}},
	    axes: {
	      x: {
	        0: { side: 'bottom', label: ''} // bottom x-axis.
	      },
	      y: {
	        0: { side: 'left', label: ''} // Left side y-axis.
	      }
	    },
	    bar: { groupWidth: "80%" }
	  };
	
	  var chart = new google.visualization.ColumnChart(document.getElementById("bar_chart_MDT"));
	  chart.draw(view, options);
	}
};

// Draw the Quarterly DBT Trends Chart
function drawQDTChart() {
//alert("drawQDTChart");

	var tableQDT=document.getElementById("table_QDT");
 	var cnt, dateQQYY, DBT, bExists=false;

	// SBCSQuarterlyPaymentTrends should only occur once and exists if not null
	if (tableQDT != null) {
 		bExists=true;
		var xmlQDT=loadXMLString(tableQDT.innerHTML);
		//alert("tableQDT="+tableQDT.innerHTML);
		// documentElement always represents the root node
		var xmlQDTChild=xmlQDT.documentElement.childNodes;
		cnt = xmlQDTChild.length;
		
		// Create Google Charts DataTable
		var data = new google.visualization.DataTable();
		// Declare columns
		data.addColumn('string', 'Date');
		data.addColumn('number', 'DBT');
		data.addColumn({type: 'string', role: 'tooltip'});
 		
 		for (i=0;i<cnt;i++){
 			dateQQYY = xmlQDTChild[i].childNodes[0].childNodes[0].nodeValue;
 			DBT = xmlQDTChild[i].childNodes[1].childNodes[0].nodeValue;
	 		//alert("i["+i+"]="+dateQQYY+"-["+DBT+"]");
				data.addRow([dateQQYY, Number(DBT), dateQQYY+', '+Number(DBT)]);
		}
	}

	if (bExists) {
	 	var view = new google.visualization.DataView(data);
  	view.setColumns([0, 1,
                   { calc: "stringify",
                     sourceColumn: 1,
                     type: "string",
                     role: "annotation" },2]);

	  var options = {
	    title: '',
	    width: 340,
	    legend: { position: 'none' },
	    chart: { title: '',
	             subtitle: '' },
	    chartArea: {height: '70%', width: '85%'},
	    annotations: {alwaysOutside: true, textStyle: {color: 'black' }},
	    colors: ['#647300'],
	    bars: 'horizontal', // Required for Material Bar Charts.
	    vAxis: {minValue: 0, maxValue: 120, viewWindow: {max: 120}, gridlines: {count: 6}},
	    hAxis: {direction:-1, textStyle: {fontSize: 9}},
	    axes: {
	      x: {
	        0: { side: 'bottom', label: ''} // bottom x-axis.
	      },
	      y: {
	        0: { side: 'left', label: ''} // Left side y-axis.
	      }
	    },
	    bar: { groupWidth: "80%" }
	  };
	
	  var chart = new google.visualization.ColumnChart(document.getElementById("bar_chart_QDT"));
	  chart.draw(view, options);
	}
};