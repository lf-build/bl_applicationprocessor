google.load("visualization", "1", {packages:["corechart"], "callback" : googleVisualizationCallback});

function googleVisualizationCallback(){
	drawAllQSTCharts();
	drawMDTChart();
	drawQDTChart();
};


/**
 * Loop thorugh all Quarterly Score Trends to draw all charts 
 * @return
 */
function drawAllQSTCharts(){
	var qstNodes = document.getElementsByTagName("QSTChart");
	for (var i=0; qstNodes != null && i < qstNodes.length; i++) {
		drawQSTChart(qstNodes[i]);
	}
};


// Draw the Quarterly Score Trends Chart
function drawQSTChart(qstNode) {
//alert("drawQSTChart");

	var chartTitle, tableSTCL, chartPlot;
	
	// find the valid chart nodes to draw (excluding comment nodes, etc.)
	var chartNodes = qstNode.childNodes;
	for (var i=0; chartNodes != null && i < chartNodes.length; i++) {
		if (chartNodes[i].id == 'chartTitle') {
			chartTitle = chartNodes[i].firstChild.nodeValue;
			continue;
		}
		else if (chartNodes[i].id == 'table_STCL') {
			tableSTCL = chartNodes[i];
			continue;
		}
		else if (chartNodes[i].id == 'bar_chart_QST') {
			chartPlot = chartNodes[i];
			continue;
		}
	}
	
 	var cnt, dateRange, Score, bExists=false;

	// ScoreTrendsCreditLimit should only occur once and exists if not null
	if (tableSTCL != null) {
 		bExists=true;
		var xmlSTCL=loadXMLString(tableSTCL.innerHTML.replace(/(\r\n|\n|\r)/gm,""));
		//alert("tableSTCL="+tableSTCL.innerHTML);
		// documentElement always represents the root node
		var xmlSTCLChild=xmlSTCL.documentElement.childNodes;
		cnt = xmlSTCLChild.length;

		// Create Google Charts DataTable
		var data = new google.visualization.DataTable();
		// Declare columns
		data.addColumn('string', 'Date Range');
		data.addColumn('number', 'Score');
		data.addColumn({type: 'string', role: 'tooltip'});

 		for (i=0;i<cnt;i++){
 			dateRange = xmlSTCLChild[i].childNodes[0].childNodes[0].nodeValue;
 			Score = xmlSTCLChild[i].childNodes[1].childNodes[0].nodeValue;
	 		//alert("i["+i+"]="+dateRange+"-["+Score+"]");
			data.addRow([dateRange, Number(Score), dateRange+', '+Number(Score)]);
		}
	}

	if (bExists) {
 		var view = new google.visualization.DataView(data);
  	view.setColumns([0, 1,
                   { calc: "stringify",
                     sourceColumn: 1,
                     type: "string",
                     role: "annotation" }, 2]);

	  var options = {
	    title: chartTitle,
	    titleTextStyle: {fontSize: 13},
	    fontName: "arial", 
	    width: 400,
	    height: 300,
	    annotations: {alwaysOutside: true, textStyle: {color: 'black' }},
	    colors: ['#67A8DB'],
	    chartArea: {height: 175, width: 300, backgroundColor: '#D3D3D3'},
	    legend: { position: 'none' },
	    chart: { title: '',
	             subtitle: '' },
	    bars: 'horizontal', // Required for Material Bar Charts.
	    vAxis: {title: 'Score', titleTextStyle: {italic: false, bold: true}, minValue: 0, maxValue: 100, viewWindow: {max: 100}, gridlines: {color: '#A9A9A9', count: 6}, textStyle: {fontSize: 12}},
	    hAxis: {title: 'Month', titleTextStyle: {italic: false, bold: true}, slantedText: true, slantedTextAngle: 45, direction:-1, textStyle: {fontSize: 11}},
	    axes: {
	      x: {
	        0: { side: 'bottom', label: ''} // bottom x-axis.
	      },
	      y: {
	        0: { side: 'left', label: ''} // left side y-axis.
	      }
	    },
	    bar: { groupWidth: "80%" }
	  };

	  var chart = new google.visualization.ColumnChart(chartPlot);
	  chart.draw(view, options);		
	}
};

// Draw the Monthly DBT Trends Chart
function drawMDTChart() {
//alert("drawMDTChart");

	var tableMDT=document.getElementById("table_MDT");
 	var cnt, dateMMMYY, DBT, bExists=false, maxDBT;

	// PaymentTrends should only occur once and exists if not null
	if (tableMDT != null) {
 		bExists=true;
		var xmlMDT=loadXMLString(tableMDT.innerHTML.replace(/(\r\n|\n|\r)/gm,""));
		//alert("tableMDT="+tableMDT.innerHTML);
		// documentElement always represents the root node
		var xmlMDTChild=xmlMDT.documentElement.childNodes;
		cnt = xmlMDTChild.length;
		
		// Create Google Charts DataTable
		var data = new google.visualization.DataTable();
		// Declare columns
		data.addColumn('string', 'Date');
		data.addColumn('number', 'DBT');
		data.addColumn({type: 'string', role: 'tooltip'});
 		
 		for (i=0;i<cnt;i++){
 			// first element will <maxDBT> for both monthly/quarterly DBT
 			if (i==0)
 				maxDBT = xmlMDTChild[i].childNodes[0].nodeValue;
 			else {
	 			dateMMMYY = xmlMDTChild[i].childNodes[0].childNodes[0].nodeValue;
	 			DBT = xmlMDTChild[i].childNodes[1].childNodes[0].nodeValue;
		 		//alert("i["+i+"]="+dateMMMYY+"-["+DBT+"]");
				data.addRow([dateMMMYY, Number(DBT), dateMMMYY+', '+Number(DBT)]);
			}
		}
	}

	if (bExists) {
 		var view = new google.visualization.DataView(data);
  	view.setColumns([0, 1,
                   { calc: "stringify",
                     sourceColumn: 1,
                     type: "string",
                     role: "annotation" }, 2]);

	  var options = {
	    title: 'Monthly DBT Trends',
	    titleTextStyle: {fontSize: 12},
	    fontName: "verdana", 
	    width: 330,
	    annotations: {alwaysOutside: true, textStyle: {color: 'black' }},
	    colors: ['#67A8DB'],
	    chartArea: {height: 150, width: 275, backgroundColor: '#D3D3D3'},
	    legend: { position: 'none' },
	    chart: { title: '',
	             subtitle: '' },
	    bars: 'horizontal', // Required for Material Bar Charts.
	    vAxis: {minValue: 0, maxValue: maxDBT, viewWindow: {max: maxDBT}, gridlines: {color: '#A9A9A9', count: 7}},
	    hAxis: {slantedText: true, slantedTextAngle: 45, direction:-1, textStyle: {fontSize: 10}},
	    axes: {
	      x: {
	        0: { side: 'bottom', label: ''} // bottom x-axis.
	      },
	      y: {
	        0: { side: 'left', label: ''} // Left side y-axis.
	      }
	    },
	    bar: { groupWidth: "80%" }
	  };
	
	  var chart = new google.visualization.ColumnChart(document.getElementById("bar_chart_MDT"));
	  chart.draw(view, options);
	}
};

// Draw the Quarterly DBT Trends Chart
function drawQDTChart() {
//alert("drawQDTChart");

	var tableQDT=document.getElementById("table_QDT");
 	var cnt, dateQQYY, DBT, bExists=false;

	// QuarterlyPaymentTrends should only occur once and exists if not null
	if (tableQDT != null) {
 		bExists=true;
		var xmlQDT=loadXMLString(tableQDT.innerHTML.replace(/(\r\n|\n|\r)/gm,""));
		//alert("tableQDT="+tableQDT.innerHTML);
		// documentElement always represents the root node
		var xmlQDTChild=xmlQDT.documentElement.childNodes;
		cnt = xmlQDTChild.length;
		
		// Create Google Charts DataTable
		var data = new google.visualization.DataTable();
		// Declare columns
		data.addColumn('string', 'Date');
		data.addColumn('number', 'DBT');
		data.addColumn({type: 'string', role: 'tooltip'});

 		for (i=0;i<cnt;i++){
 			// first element will <maxDBT> for both monthly/quarterly DBT
 			if (i==0)
 				maxDBT = xmlQDTChild[i].childNodes[0].nodeValue;
 			else {
	 			DBT = xmlQDTChild[i].childNodes[0].childNodes[0].nodeValue;
	 			dateQQYY = xmlQDTChild[i].childNodes[1].childNodes[0].nodeValue;
		 		//alert("i["+i+"]="+dateQQYY+"-["+DBT+"]");
				data.addRow([dateQQYY, Number(DBT), dateQQYY+', '+Number(DBT)]);
			}
		}
	}

	if (bExists) {
	 	var view = new google.visualization.DataView(data);
  	view.setColumns([0, 1,
                   { calc: "stringify",
                     sourceColumn: 1,
                     type: "string",
                     role: "annotation" }, 2]);

	  var options = {
	    title: 'Quarterly DBT Trends',
	    titleTextStyle: {fontSize: 12},
	    fontName: "verdana", 
	    width: 330,
	    annotations: {alwaysOutside: true, textStyle: {color: 'black' }},
	    colors: ['#67A8DB'],
	    chartArea: {height: 150, width: 275, backgroundColor: '#D3D3D3'},
	    legend: { position: 'none' },
	    chart: { title: '',
	             subtitle: '' },
	    bars: 'horizontal', // Required for Material Bar Charts.
	    vAxis: {minValue: 0, maxValue: maxDBT, viewWindow: {max: maxDBT}, gridlines: {color: '#A9A9A9', count: 7}},
	    hAxis: {slantedText: true, slantedTextAngle: 45, direction:-1, textStyle: {fontSize: 10}},
	    axes: {
	      x: {
	        0: { side: 'bottom', label: ''} // bottom x-axis.
	      },
	      y: {
	        0: { side: 'left', label: ''} // Left side y-axis.
	      }
	    },
	    bar: { groupWidth: "80%" }
	  };
	
	  var chart = new google.visualization.ColumnChart(document.getElementById("bar_chart_QDT"));
	  chart.draw(view, options);
	}
};