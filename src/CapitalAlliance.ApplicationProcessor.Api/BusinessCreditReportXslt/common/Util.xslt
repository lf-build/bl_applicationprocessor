<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:prd="http://www.experian.com/ARFResponse">
  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />
    
    
  <xsl:variable name="ipV1Model">
    <xsl:value-of select="214" />
  </xsl:variable>
  
  <xsl:variable name="ipV1ScoreOnlyModel">
    <xsl:value-of select="215" />
  </xsl:variable>
  
  <xsl:variable name="ipV2Model">
    <xsl:value-of select="224" />
  </xsl:variable>
  
  <xsl:variable name="ipV2ScoreOnlyModel">
    <xsl:value-of select="225" />
  </xsl:variable>
  
  <xsl:variable name="fsrModel">
    <xsl:value-of select="223" />
  </xsl:variable>
  
  <xsl:variable name="score999">
    <xsl:value-of select="999" />
  </xsl:variable>
  
  <xsl:variable name="score998">
    <xsl:value-of select="998" />
  </xsl:variable>
    <xsl:variable name="ciModel">
    <xsl:value-of select="210" />
  </xsl:variable>
  
  <xsl:variable name="ciScoreOnlyModel">
    <xsl:value-of select="211" />
  </xsl:variable>
  
  <xsl:variable name="sbiModel">
    <xsl:value-of select="212" />
  </xsl:variable>
  
  <xsl:variable name="sbiScoreOnlyModel">
    <xsl:value-of select="213" />
  </xsl:variable>
  
  <!-- TFS Change: Start -->
  <xsl:variable name="lossScoreModel">
    <xsl:value-of select="230" />
  </xsl:variable>
  
  <xsl:variable name="delinquencyScoreModel">
    <xsl:value-of select="231" />
  </xsl:variable>
  <!-- TFS Change: End -->
  
  <xsl:variable name="sbcsV2Model">
    <xsl:value-of select="233" />
  </xsl:variable>

  <xsl:variable name="titleColor">
    <xsl:value-of select="'#015CAE'" />
  </xsl:variable>  
  
  <xsl:variable name="borderColor">
    <xsl:value-of select="'#015CAE'" />
  </xsl:variable>

  <xsl:variable name="reportTextColor">
    <xsl:value-of select="'#595959'" />
  </xsl:variable>
  
  <!-- text color for legacy reports -->
  <xsl:variable name="textColor">
    <xsl:value-of select="'#595959'" />
  </xsl:variable>
  
  <xsl:variable name="subtitleColor">
    <xsl:value-of select="'#b3b3b3'" />
  </xsl:variable>
  
  <xsl:variable name="rowBorderColor">
    <xsl:value-of select="'#b3b3b3'" />
  </xsl:variable>

  <xsl:variable name="scoreMeterWidth">
    <xsl:value-of select="300"></xsl:value-of>
  </xsl:variable>
  
  <xsl:variable name="zebraStripColor">
    <xsl:value-of select="'#e5f5fa'"></xsl:value-of>
  </xsl:variable>
  
  <xsl:variable name="alternateColor">
    <xsl:value-of select="'#e5f5fa'" />
  </xsl:variable>

  <xsl:variable name="lowRiskColor">
    <xsl:value-of select="'#209a5c'" />
  </xsl:variable>

  <xsl:variable name="lowMedRiskColor">
    <xsl:value-of select="'#8aca64'" />
  </xsl:variable>

  <xsl:variable name="medRiskColor">
    <xsl:value-of select="'#f4e35b'" />
  </xsl:variable>

  <xsl:variable name="medHighRiskColor">
    <xsl:value-of select="'#ee7240'" />
  </xsl:variable>

  <xsl:variable name="highRiskColor">
    <xsl:value-of select="'#d6373e'" />
  </xsl:variable>

  <xsl:variable name="lowRiskText">
    <xsl:value-of select="'LOW RISK'" />
  </xsl:variable>

  <xsl:variable name="lowMedRiskText">
    <xsl:value-of select="'LOW TO MEDIUM RISK'" />
  </xsl:variable>

  <xsl:variable name="medRiskText">
    <xsl:value-of select="'MEDIUM RISK'" />
  </xsl:variable>

  <xsl:variable name="medHighRiskText">
    <xsl:value-of select="'MEDIUM TO HIGH RISK'" />
  </xsl:variable>

  <xsl:variable name="highRiskText">
    <xsl:value-of select="'HIGH RISK'" />
  </xsl:variable>
  
  <xsl:variable name="basePath">
   <!-- <xsl:value-of select="'../images/'"></xsl:value-of> -->
   <xsl:value-of select="'../images/'"></xsl:value-of>
  </xsl:variable>
    
  <xsl:variable name="modelCode">
    <xsl:choose>
      <xsl:when test="//prd:InputSummary/prd:Inquiry">
        <xsl:value-of select="number(substring(substring-after(//prd:InputSummary/prd:Inquiry, 'MC-'), 1,6))" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="0" />
      </xsl:otherwise>
    </xsl:choose>    
  </xsl:variable>
  

<!-- This only applies to IP score models -->
  <xsl:variable name="segmentUsed">
    <xsl:call-template name="GetSegmentUsed">
        <!-- !!USE MAIN MODEL ONLY!!  -->
    	<xsl:with-param name="intelliscoreNode" select="//prd:IntelliscoreScoreInformation[number(prd:ModelInformation/prd:ModelCode) != $fsrModel and number(prd:ModelInformation/prd:ModelCode) != $delinquencyScoreModel]" />
    </xsl:call-template>
  </xsl:variable>

  <xsl:variable name="isCommercial">
    <xsl:choose>
      <xsl:when test="number($segmentUsed) != 0 and (number($segmentUsed) = 6 or number($segmentUsed) = 7)">
        <xsl:value-of select="0" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="1" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="isBlendedModel">
    <xsl:choose>
      <xsl:when test="number($segmentUsed) = 6">
        <xsl:value-of select="1" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="0" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  
  <xsl:variable name="isOwnerGuarantorModel">
    <xsl:choose>
      <xsl:when test="number($segmentUsed) = 7">
        <xsl:value-of select="1" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="0" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  
 
 <!--
  *********************************************
  * Initial main HTML template
  *********************************************
  -->
  <xsl:template name="MainHTML">
    <xsl:param name="productTitle" select="''"/>
    <html>
      <head>
        <title><xsl:value-of select="$productTitle" /></title>
        <xsl:call-template name="ReportCSS"></xsl:call-template>
        
        <!-- Google Charts -->
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script type="text/javascript" src="../common/loadxmldoc.js"></script>
        <script type="text/javascript" src="../common/dateUtil.js"></script>
        <script type="text/javascript" src="../common/drawBarChartsSBCS.js"></script>
        <script type="text/javascript" src="../common/drawBarChartsPPR.js"></script>

      </head>

      <body>
        <a name="top"><font><!-- FOR IE7, DO NOT REMOVE THIS EMPTY TAG --></font></a>
        <table class="report_container" width="715" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td>
              <!-- Response template -->
              <xsl:apply-templates select="//prd:Products" />
            </td>
          </tr>
        </table>
        
      </body>
    </html>

  </xsl:template>
  
  
<!--**************************************************End SBCS V2**************************************************-->


  
  <xsl:template name="SpellDigitNumber">
    <xsl:param name="upperCase" select="false()" />
    <xsl:param name="value" />

    <xsl:variable name="result">
      <xsl:choose>
        <xsl:when test="number($value) = 1">
          <xsl:text disable-output-escaping="yes">One</xsl:text>
        </xsl:when>
        <xsl:when test="number($value) = 2">
          <xsl:text disable-output-escaping="yes">Two</xsl:text>
        </xsl:when>
        <xsl:when test="number($value) = 3">
          <xsl:text disable-output-escaping="yes">Three</xsl:text>
        </xsl:when>
        <xsl:when test="number($value) = 4">
          <xsl:text disable-output-escaping="yes">Four</xsl:text>
        </xsl:when>
        <xsl:when test="number($value) = 5">
          <xsl:text disable-output-escaping="yes">Five</xsl:text>
        </xsl:when>
        <xsl:when test="number($value) = 6">
          <xsl:text disable-output-escaping="yes">Six</xsl:text>
        </xsl:when>
        <xsl:when test="number($value) = 7">
          <xsl:text disable-output-escaping="yes">Seven</xsl:text>
        </xsl:when>
        <xsl:when test="number($value) = 8">
          <xsl:text disable-output-escaping="yes">Eight</xsl:text>
        </xsl:when>
        <xsl:when test="number($value) = 9">
          <xsl:text disable-output-escaping="yes">Nine</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text disable-output-escaping="yes">Unknown</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$upperCase">
        <xsl:value-of select="translate($result, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')" />
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="$result" />
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

  
  
  
  
  


  <!--
  *********************************************
  * SectionTitle template
  *********************************************
  -->
  <xsl:template name="SectionTitle">
    <xsl:param name="title" />
    <xsl:param name="color" select="'#193385'" />

    <!-- section title -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
          <font size="3" color="{$color}"><b>
          <xsl:value-of select="$title" />
          </b></font>
        </td>
      </tr>
    </table>

  </xsl:template>

 <!--
  ***********************************************
  * Formats address info into one single line
  ***********************************************
  -->
  <xsl:template name="FormatAddressLine">
    <xsl:param name="businessName" />
    <xsl:param name="street1" />
    <xsl:param name="street2" />
    <xsl:param name="city" />
    <xsl:param name="state"/>
    <xsl:param name="zip"/>
    <xsl:param name="zipExt"/>
    <xsl:if test="$businessName!=''">
    <xsl:value-of select="concat($businessName,', ')"/>
    </xsl:if>
    <xsl:if test="$street1!=''">
    <xsl:value-of select="concat($street1,', ')"/>
    </xsl:if>
    <xsl:if test="street2!=''">
    <xsl:value-of select="concat($street2,', ')"/>
    </xsl:if>
        <xsl:value-of select="$city"></xsl:value-of>
        <xsl:if test="$city!='' and $state!=''">, </xsl:if>
        <xsl:value-of select="$state"></xsl:value-of>
        <xsl:if test="($city!='' or $state!='') and $zip!=''"> </xsl:if>
        <xsl:value-of select="$zip"></xsl:value-of>
        <xsl:if test="$zipExt!=''">-</xsl:if>
        <xsl:value-of select="$zipExt"></xsl:value-of>
  </xsl:template>


<!--
  ***********************************************
  * Formats address info in the following layout
  * 123 Main St
  * Apt 123
  * Los Angeles, CA 92020-1234
  ***********************************************
  -->
  <xsl:template name="FormatAddress">
    <xsl:param name="street1" />
    <xsl:param name="street2" />
    <xsl:param name="city" />
    <xsl:param name="state"/>
    <xsl:param name="zip"/>
    <xsl:param name="zipExt"/>
    <div><xsl:value-of select="$street1"></xsl:value-of></div>
    <xsl:if test="street2!=''">
    <div><xsl:value-of select="$street2"></xsl:value-of></div>
    </xsl:if>
    <div>
        <xsl:value-of select="$city"></xsl:value-of>
        <xsl:if test="$city!='' and $state!=''">, </xsl:if>
        <xsl:value-of select="$state"></xsl:value-of>
        <xsl:if test="($city!='' or $state!='') and $zip!=''"><xsl:value-of select="' '"></xsl:value-of></xsl:if>
        <xsl:value-of select="$zip"></xsl:value-of>
        <xsl:if test="$zipExt!=''">-</xsl:if>
        <xsl:value-of select="$zipExt"></xsl:value-of>
    </div>
  </xsl:template>
  
  
<!--
  ****************************************************
  * RiskClassByScore
  * Determine risk class by a given model's score
  ****************************************************
   -->
  <xsl:template name="RiskClassByScore">
    <xsl:param name="model" />
    <xsl:param name="score" />
    <xsl:choose>
        <xsl:when test="$model=$ipV1Model or $model=$ipV1ScoreOnlyModel">
            <xsl:choose>
                <xsl:when test="$score &lt; 11">
                    <xsl:value-of select="5"></xsl:value-of>
                </xsl:when>
                <xsl:when test="$score &lt; 26">
                    <xsl:value-of select="4"></xsl:value-of>
                </xsl:when>
                <xsl:when test="$score &lt; 51">
                    <xsl:value-of select="3"></xsl:value-of>
                </xsl:when>
                <xsl:when test="$score &lt; 76">
                    <xsl:value-of select="2"></xsl:value-of>
                </xsl:when>
                <xsl:when test="$score &lt; 101">
                    <xsl:value-of select="1"></xsl:value-of>
                </xsl:when>
            </xsl:choose>
        </xsl:when>
        <xsl:when test="$model=$ciModel or $model=$ciScoreOnlyModel">
            <xsl:choose>
                <xsl:when test="$score &lt; 32.48">
                    <xsl:value-of select="5"></xsl:value-of>
                </xsl:when>
                <xsl:when test="$score &lt; 58.64">
                    <xsl:value-of select="4"></xsl:value-of>
                </xsl:when>
                <xsl:when test="$score &lt; 70.20">
                    <xsl:value-of select="3"></xsl:value-of>
                </xsl:when>
                <xsl:when test="$score &lt; 77.72">
                    <xsl:value-of select="2"></xsl:value-of>
                </xsl:when>
                <xsl:when test="$score &lt; 100.01">
                    <xsl:value-of select="1"></xsl:value-of>
                </xsl:when>
            </xsl:choose>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="''"></xsl:value-of>
        </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <!--
  *********************************************
  * FormatDate template
  * Pattern: 'year' or 'yr' for year
  *          'dt' for day
  *          'mo' for month
  * Value has to be in format 'YYYYMMDD', 'MMDDYYYY', 'YYMMDD', or 'MMDDYY'
  *********************************************
  -->
  <xsl:template name="FormatDate">    
    <xsl:param name="pattern" />
    <xsl:param name="value" />
    <xsl:param name="yearDigit" select="4" />
    <xsl:param name="isYearLast" select="false()" />
    <xsl:param name="delimiter" select="'/'" />

    <xsl:variable name="yyyy">
      <xsl:choose>                    
        <xsl:when test="$yearDigit = 4">
          <xsl:choose>                    
            <xsl:when test="$isYearLast">
              <xsl:value-of select="substring($value, 5, 4)" />
            </xsl:when>

            <xsl:otherwise>
              <xsl:value-of select="substring($value, 1, 4)" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>

        <xsl:otherwise>
          <xsl:choose>                    
            <xsl:when test="$isYearLast">
              <xsl:choose>                    
                <xsl:when test="substring($value, 5, 2) &gt; 40">
                  <xsl:value-of select="normalize-space(concat('19', substring($value, 5, 2)))" />
                </xsl:when>

                <xsl:otherwise>
                  <xsl:value-of select="normalize-space(concat('20', substring($value, 5, 2)))" />
                </xsl:otherwise>
              </xsl:choose>    
            </xsl:when>

            <xsl:otherwise>
              <xsl:choose>
                <xsl:when test="substring($value, 1, 2) &gt; 40">
                  <xsl:value-of select="normalize-space(concat('19', substring($value, 1, 2)))" />
                </xsl:when>
        
                <xsl:otherwise>
                  <xsl:value-of select="normalize-space(concat('20', substring($value, 1, 2)))" />
                </xsl:otherwise>
              </xsl:choose>    
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
 
    <xsl:variable name="yy">
      <xsl:choose>                    
        <xsl:when test="$yearDigit = 4">
          <xsl:choose>                    
            <xsl:when test="$isYearLast">
              <xsl:value-of select="substring($value, 7, 2)" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="substring($value, 3, 2)" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>

        <xsl:otherwise>
          <xsl:choose>                    
            <xsl:when test="$isYearLast">
              <xsl:value-of select="substring($value, 5, 2)" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="substring($value, 1, 2)" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
 
    <xsl:variable name="MM">
      <xsl:choose>                    
        <xsl:when test="$isYearLast">
          <xsl:value-of select="substring($value, 1, 2)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:choose>                    
            <xsl:when test="$yearDigit = 4">
              <xsl:value-of select="substring($value, 5, 2)" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="substring($value, 3, 2)" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
 
    <xsl:variable name="dd">
      <xsl:choose>                    
        <xsl:when test="$isYearLast">
          <xsl:value-of select="substring($value, 3, 2)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:choose>                    
            <xsl:when test="$yearDigit = 4">
              <xsl:value-of select="substring($value, 7, 2)" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="substring($value, 5, 2)" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:choose>                      
      <xsl:when test="number($yyyy) = 0">
        <xsl:value-of select="''" />
      </xsl:when>
      
      <xsl:when test="number($MM) = 0">
        <xsl:choose>                      
          <xsl:when test="contains($pattern, 'year')">
            <xsl:value-of select="$yyyy" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="$yy" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:when>
      
      <xsl:when test="number($dd) = 0">
        <xsl:choose>                      
          <xsl:when test="starts-with(normalize-space($pattern), 'year') or starts-with(normalize-space($pattern), 'yr')">
            <xsl:choose>                  
              <xsl:when test="contains($pattern, 'year')">
                <xsl:value-of select="concat(normalize-space($yyyy), $delimiter, normalize-space($MM))" />
              </xsl:when>
      
              <xsl:otherwise>
                <xsl:value-of select="concat(normalize-space($yy), $delimiter, normalize-space($MM))" />
              </xsl:otherwise>
            </xsl:choose>    
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:choose>                  
              <xsl:when test="contains($pattern, 'year')">
                <xsl:value-of select="concat(normalize-space($MM), $delimiter, normalize-space($yyyy))" />
              </xsl:when>
      
              <xsl:otherwise>
                <xsl:value-of select="concat(normalize-space($MM), $delimiter, normalize-space($yy))" />
              </xsl:otherwise>
            </xsl:choose>    
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:when>
      
      <xsl:when test="contains($pattern, 'year')">
        <xsl:value-of select="translate(translate(translate($pattern, 'dt', $dd), 'mo', $MM), 'year', $yyyy)" />
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="translate(translate(translate($pattern, 'dt', $dd), 'mo', $MM), 'yr', $yy)" />
      </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>


  <!--
  ************************************************
  * FormatPhone template
  * Value has to be in format 9999999999
  ************************************************
  -->
  <xsl:template name="FormatPhone">
    <xsl:param name="value" />
    <xsl:value-of select="concat('(',substring($value, 1, 3), ') ', substring($value, 4, 3), '-', substring($value, 7, 4))" />
  </xsl:template>




  <!--
  ***********************************************
  * FormatZip template
  * Value has to be in format 999999999 or 99999
  ***********************************************
  -->
  <xsl:template name="FormatZip">
    <xsl:param name="value" />
    
    <xsl:choose>                      
      <xsl:when test="string-length(normalize-space($value)) &gt; 5">
        <xsl:value-of select="concat(substring($value, 1, 5), '-', normalize-space(substring($value, 6)))" />
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="$value" />
      </xsl:otherwise>
    </xsl:choose>    
  </xsl:template>


  <!--
  *********************************************
  * FormatMonth template
  * Value has to be 1 to 12
  *********************************************
  -->
  <xsl:template name="FormatMonth">
    <xsl:param name="monthValue" select="1" />
    <xsl:param name="full" select="false()" />
    <xsl:param name="upperCase" select="false()" />
    
    <xsl:variable name="result">
      <xsl:choose>                    
        <xsl:when test="number($monthValue) = 1">
          <xsl:choose>                    
            <xsl:when test="$full">
              <xsl:value-of select="'January'" />
            </xsl:when>
      
            <xsl:otherwise>
              <xsl:value-of select="'Jan'" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>
  
        <xsl:when test="$monthValue = 2">
          <xsl:choose>                    
            <xsl:when test="$full">
              <xsl:value-of select="'February'" />
            </xsl:when>
      
            <xsl:otherwise>
              <xsl:value-of select="'Feb'" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>
  
        <xsl:when test="$monthValue = 3">
          <xsl:choose>                    
            <xsl:when test="$full">
              <xsl:value-of select="'March'" />
            </xsl:when>
      
            <xsl:otherwise>
              <xsl:value-of select="'Mar'" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>
  
        <xsl:when test="$monthValue = 4">
          <xsl:choose>                    
            <xsl:when test="$full">
              <xsl:value-of select="'April'" />
            </xsl:when>
      
            <xsl:otherwise>
              <xsl:value-of select="'Apr'" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>
  
        <xsl:when test="$monthValue = 5">
          <xsl:choose>                    
            <xsl:when test="$full">
              <xsl:value-of select="'May'" />
            </xsl:when>
      
            <xsl:otherwise>
              <xsl:value-of select="'May'" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>
  
        <xsl:when test="$monthValue = 6">
          <xsl:choose>                    
            <xsl:when test="$full">
              <xsl:value-of select="'June'" />
            </xsl:when>
      
            <xsl:otherwise>
              <xsl:value-of select="'Jun'" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>
  
        <xsl:when test="$monthValue = 7">
          <xsl:choose>                    
            <xsl:when test="$full">
              <xsl:value-of select="'July'" />
            </xsl:when>
      
            <xsl:otherwise>
              <xsl:value-of select="'Jul'" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>
  
        <xsl:when test="$monthValue = 8">
          <xsl:choose>                    
            <xsl:when test="$full">
              <xsl:value-of select="'August'" />
            </xsl:when>
      
            <xsl:otherwise>
              <xsl:value-of select="'Aug'" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>
  
        <xsl:when test="$monthValue = 9">
          <xsl:choose>                    
            <xsl:when test="$full">
              <xsl:value-of select="'September'" />
            </xsl:when>
      
            <xsl:otherwise>
              <xsl:value-of select="'Sep'" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>
  
        <xsl:when test="$monthValue = 10">
          <xsl:choose>                    
            <xsl:when test="$full">
              <xsl:value-of select="'October'" />
            </xsl:when>
      
            <xsl:otherwise>
              <xsl:value-of select="'Oct'" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>
  
        <xsl:when test="$monthValue = 11">
          <xsl:choose>                    
            <xsl:when test="$full">
              <xsl:value-of select="'November'" />
            </xsl:when>
      
            <xsl:otherwise>
              <xsl:value-of select="'Nov'" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>
  
        <xsl:otherwise>
          <xsl:choose>                    
            <xsl:when test="$full">
              <xsl:value-of select="'December'" />
            </xsl:when>
      
            <xsl:otherwise>
              <xsl:value-of select="'Dec'" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:otherwise>
  
      </xsl:choose>    
    </xsl:variable>
    
    
    <xsl:choose>                      
      <xsl:when test="$upperCase">
        <xsl:value-of select="translate($result, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')" />
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="$result" />
      </xsl:otherwise>
    </xsl:choose>    
    
  </xsl:template>
  
  
  <!--
  *********************************************
  * JoinNodeset template
  *********************************************
  -->
  <xsl:template name="JoinNodeset">
    <xsl:param name="nodeset" />
    <xsl:param name="order" />
    <xsl:param name="delimiter" select="'&lt;br /&gt;'" />
    
    <xsl:for-each select="$nodeset">
      <xsl:sort select="position()" order="{$order}" data-type="text"/>
      <xsl:if test="position() &gt; 1">
        <xsl:value-of select="$delimiter" />
      </xsl:if>
      <xsl:value-of select="." />
    </xsl:for-each>
  </xsl:template>


  <!--
  *********************************************
  * translate collection status template
  * code is status code
  *********************************************
  -->
  <xsl:template name="translateCollStatus">
    <xsl:param name="code" select="''" />
      
    <xsl:choose>                      
    <xsl:when test="number($code) = 0">
            <xsl:text disable-output-escaping="yes">Open Account</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 1">
            <xsl:text disable-output-escaping="yes">Disputed</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 2">
            <xsl:text disable-output-escaping="yes">Payment Plan</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 3">
            <xsl:text disable-output-escaping="yes">Paid in Full</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 4">
            <xsl:text disable-output-escaping="yes">Settlement Paid Full</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 5">
            <xsl:text disable-output-escaping="yes">Closed, Partial Payment</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 6">
            <xsl:text disable-output-escaping="yes">Closed, Uncollected</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 7">
            <xsl:text disable-output-escaping="yes">Closed, Out of Business</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 8">
            <xsl:text disable-output-escaping="yes">Closed, Bankruptcy</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 9">
            <xsl:text disable-output-escaping="yes">Closed, Creditors Request</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 10">
            <xsl:text disable-output-escaping="yes">Closed, Uncollected Judgment</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 11">
            <xsl:text disable-output-escaping="yes">Closed, Judgment Satisfied</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 12">
            <xsl:text disable-output-escaping="yes">Closed, Cannot Locate</xsl:text>
        </xsl:when>
    <xsl:otherwise>
            <xsl:text disable-output-escaping="yes">Unknown</xsl:text>
    </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>


  <!--
  *********************************************
  * translate legal filing type template
  * code is status code
  *********************************************
  -->
  <xsl:template name="translateLegalFilingType">
    <xsl:param name="code" select="''" />
      
    <xsl:choose>                      
    <xsl:when test="number($code) = 1">
            <xsl:text disable-output-escaping="yes">Bankruptcy</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 2">
            <xsl:text disable-output-escaping="yes">Federal Tax Lien</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 3">
            <xsl:text disable-output-escaping="yes">State Tax Lien</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 4">
            <xsl:text disable-output-escaping="yes">County Tax Lien</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 5">
            <xsl:text disable-output-escaping="yes">Judgment</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 6">
            <xsl:text disable-output-escaping="yes">Attachment Lien</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 7">
            <xsl:text disable-output-escaping="yes">Bulk Transfer</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 8">
            <xsl:text disable-output-escaping="yes">UCC</xsl:text>
        </xsl:when>
    <xsl:otherwise>
            <xsl:text disable-output-escaping="yes">Unknown</xsl:text>
    </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>


  <!--
  *********************************************
  * JoinCollateralText template
  *********************************************
  -->
  <xsl:template name="JoinCollateralText">
    <xsl:param name="nodeset" />

    <xsl:for-each select="$nodeset">
      <xsl:sort select="@code" order="ascending" data-type="text"/>
      <xsl:if test="position() &gt; 1">
        <xsl:value-of select="', '" />
      </xsl:if>
      <xsl:call-template name="UCCCollateralCodesTable">
        <xsl:with-param name="value" select="normalize-space(./@code)" />
      </xsl:call-template>
    </xsl:for-each>
  </xsl:template>


    <!-- 
    *********************************************
    * UCC Collateral Code Translation template
    *********************************************
     -->
    <xsl:template name="UCCCollateralCodesTable">
        <xsl:param name="value"/>

        <xsl:choose>
        <xsl:when test="normalize-space($value) = '0'">
            <xsl:text disable-output-escaping="yes">UNDEFINED</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = '1'">
            <xsl:text disable-output-escaping="yes">ACCTS REC</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = '2'">
            <xsl:text disable-output-escaping="yes">APPLIANCES</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = '3'">
            <xsl:text disable-output-escaping="yes">CHATTEL PAPER</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = '4'">
            <xsl:text disable-output-escaping="yes">EQUIP</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = '5'">
            <xsl:text disable-output-escaping="yes">FURN &amp; FIX</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = '6'">
            <xsl:text disable-output-escaping="yes">INVENTORY</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = '7'">
            <xsl:text disable-output-escaping="yes">LIVESTOCK</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = '8'">
            <xsl:text disable-output-escaping="yes">MACHINERY</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = '9'">
            <xsl:text disable-output-escaping="yes">HEREAFTER AQUIRED PROP</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'A'">
            <xsl:text disable-output-escaping="yes">PRODUCTS</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'B'">
            <xsl:text disable-output-escaping="yes">SIGN</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'C'">
            <xsl:text disable-output-escaping="yes">CROPS</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'D'">
            <xsl:text disable-output-escaping="yes">ACCTS</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'E'">
            <xsl:text disable-output-escaping="yes">PROCEEDS</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'F'">
            <xsl:text disable-output-escaping="yes">CONTRACT RIGHTS</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'G'">
            <xsl:text disable-output-escaping="yes">FARM PRODUCTS</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'H'">
            <xsl:text disable-output-escaping="yes">REAL PROPERTY</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'I'">
            <xsl:text disable-output-escaping="yes">BUILDINGS</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'J'">
            <xsl:text disable-output-escaping="yes">CERT DESCR INVENTORY</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'K'">
            <xsl:text disable-output-escaping="yes">ETC.</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'L'">
            <xsl:text disable-output-escaping="yes">LEASES</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'M'">
            <xsl:text disable-output-escaping="yes">FURNISHINGS</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'N'">
            <xsl:text disable-output-escaping="yes">FISHING VESSELS</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'O'">
            <xsl:text disable-output-escaping="yes">LOCATED ON CERT REAL PROPERTY</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'P'">
            <xsl:text disable-output-escaping="yes">AIRCRAFT</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'Q'">
            <xsl:text disable-output-escaping="yes">VEHICLES</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'R'">
            <xsl:text disable-output-escaping="yes">TOOLS</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'S'">
            <xsl:text disable-output-escaping="yes">FISHING GEAR</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'T'">
            <xsl:text disable-output-escaping="yes">CERT DESCR TIRES TUBES WHEELS ETC.</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'U'">
            <xsl:text disable-output-escaping="yes">NOTES RECEIVABLE</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'V'">
            <xsl:text disable-output-escaping="yes">CERT DESCR COMPUTER EQUIP</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'W'">
            <xsl:text disable-output-escaping="yes">ASSIGNMENT OF CERTAIN NOTE</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'X'">
            <xsl:text disable-output-escaping="yes">FISH</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'Y'">
            <xsl:text disable-output-escaping="yes">ASSIGN OF CERTAIN LEASE</xsl:text>
        </xsl:when>
        <xsl:when test="normalize-space($value) = 'Z'">
            <xsl:text disable-output-escaping="yes">ASSIGN OF CERT R/E CONTRACT</xsl:text>
        </xsl:when>
        </xsl:choose>
    </xsl:template>    


    <!-- following template code added newly on 04/03/10 to handle code descriptions problem -->
<xsl:template name="KOBtext">
    <xsl:param name="value"/>

    <xsl:choose>
        <xsl:when test="$value = 'AB'">
            <xsl:text disable-output-escaping="yes">Auto Rental</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'AC'">
            <xsl:text disable-output-escaping="yes">Auto Leasing</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'AF'">
            <xsl:text disable-output-escaping="yes">Farm Implement Dealers</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'AL'">
            <xsl:text disable-output-escaping="yes">Truck Dealers</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'AN'">
            <xsl:text disable-output-escaping="yes">Automobile Dealers, New</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'AP'">
            <xsl:text disable-output-escaping="yes">Automotive Parts</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'AR'">
            <xsl:text disable-output-escaping="yes">Auto Repair, Body Shops</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'AS'">
            <xsl:text disable-output-escaping="yes">Service Stations</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'AT'">
            <xsl:text disable-output-escaping="yes">TBA Stores, Tire Dealers</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'AU'">
            <xsl:text disable-output-escaping="yes">Automobile Dealers, Used</xsl:text>
        </xsl:when>
          <xsl:when test="$value = 'AZ'">
            <xsl:text disable-output-escaping="yes">Automotive - Non-Specific</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'BB'">
            <xsl:text disable-output-escaping="yes">All Banks - Non-Specific</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'BC'">
            <xsl:text disable-output-escaping="yes">Bank Credit Cards</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'BI'">
            <xsl:text disable-output-escaping="yes">Bank - Installment Loans</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'BM'">
            <xsl:text disable-output-escaping="yes">Bank - Mortgage Department</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'BN'">
            <xsl:text disable-output-escaping="yes">Industrial Banks</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'BO'">
            <xsl:text disable-output-escaping="yes">Co-op Banks</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'BS'">
            <xsl:text disable-output-escaping="yes">Savings Banks</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'CG'">
            <xsl:text disable-output-escaping="yes">General Clothing Store</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'CS'">
            <xsl:text disable-output-escaping="yes">Specialty Clothing Store</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'CZ'">
            <xsl:text disable-output-escaping="yes">Clothing - Non-Specific</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'DC'">
            <xsl:text disable-output-escaping="yes">Complete Department Stores</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'DV'">
            <xsl:text disable-output-escaping="yes">Variety Stores</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'DZ'">
            <xsl:text disable-output-escaping="yes">Department And Variety Stores - Non-Specific</xsl:text>
        </xsl:when>

        <xsl:when test="$value = 'EB'">
            <xsl:text disable-output-escaping="yes">Business Education</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'EC'">
            <xsl:text disable-output-escaping="yes">Colleges</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'EL'">
            <xsl:text disable-output-escaping="yes">Student Loans</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'ET'">
            <xsl:text disable-output-escaping="yes">Technical Education</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'EU'">
            <xsl:text disable-output-escaping="yes">Universities</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'EV'">
            <xsl:text disable-output-escaping="yes">Vocational And Trade Schools</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'EZ'">
            <xsl:text disable-output-escaping="yes">Education - Non-Specific</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'FA'">
            <xsl:text disable-output-escaping="yes">Automobile Financing Company</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'FB'">
            <xsl:text disable-output-escaping="yes">Mortgage Brokers</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'FC'">
            <xsl:text disable-output-escaping="yes">Credit Unions</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'FD'">
            <xsl:text disable-output-escaping="yes">Bail Bonds</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'FF'">
            <xsl:text disable-output-escaping="yes">Sales Financing Company</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'FI'">
            <xsl:text disable-output-escaping="yes">Investment Firms</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'FL'">
            <xsl:text disable-output-escaping="yes">Savings And Loans - Mortgage</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'FM'">
            <xsl:text disable-output-escaping="yes">Mortgage Companies</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'FP'">
            <xsl:text disable-output-escaping="yes">Personal Loan Companies</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'FR'">
            <xsl:text disable-output-escaping="yes">Mortgage Reporters</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'FS'">
            <xsl:text disable-output-escaping="yes">Savings And Loan Companies</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'FT'">
            <xsl:text disable-output-escaping="yes">Investment Securities</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'FU'">
            <xsl:text disable-output-escaping="yes">Bulk Purchase Finance</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'FW'">
            <xsl:text disable-output-escaping="yes">Bulk Purchase Finance - General Processors</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'FY'">
            <xsl:text disable-output-escaping="yes">Misc Loan Broker</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'FZ'">
            <xsl:text disable-output-escaping="yes">Finance Companies - Non-Specific</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'GD'">
            <xsl:text disable-output-escaping="yes">Dairies</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'GN'">
            <xsl:text disable-output-escaping="yes">Neighborhood Grocers</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'GS'">
            <xsl:text disable-output-escaping="yes">Supermarkets</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'GZ'">
            <xsl:text disable-output-escaping="yes">Groceries - Non-Specific</xsl:text>
        </xsl:when>

        <xsl:when test="$value = 'HA'">
            <xsl:text disable-output-escaping="yes">Appliance Sales And Service</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'HC'">
            <xsl:text disable-output-escaping="yes">Carpets And Flooring</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'HD'">
            <xsl:text disable-output-escaping="yes">Interior Decorators/Design</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'HF'">
            <xsl:text disable-output-escaping="yes">Home Furnishings Stores</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'HM'">
            <xsl:text disable-output-escaping="yes">Music And Record Stores</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'HR'">
            <xsl:text disable-output-escaping="yes">Furniture Rentals</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'HT'">
            <xsl:text disable-output-escaping="yes">Television And Radio Sales And Service</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'HZ'">
            <xsl:text disable-output-escaping="yes">Home Furnishings - Non-Specific</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'IG'">
            <xsl:text disable-output-escaping="yes">General Insurance</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'IL'">
            <xsl:text disable-output-escaping="yes">Life Insurance</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'IZ'">
            <xsl:text disable-output-escaping="yes">Insurance - Non-Specific</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'JA'">
            <xsl:text disable-output-escaping="yes">Jewelers</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'JC'">
            <xsl:text disable-output-escaping="yes">Cameras</xsl:text>
        </xsl:when>

        <xsl:when test="$value = 'JP'">
            <xsl:text disable-output-escaping="yes">Computer Sales and Services</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'JV'">
            <xsl:text disable-output-escaping="yes">Video Tape Rental and Sales</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'JZ'">
            <xsl:text disable-output-escaping="yes">Jewelry, Cameras &amp; Computers - Non-Specific</xsl:text>
        </xsl:when>

         <xsl:when test="$value = 'KG'">
            <xsl:text disable-output-escaping="yes">General Contractors</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'KI'">
            <xsl:text disable-output-escaping="yes">Home Improvement Contractor</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'KS'">
            <xsl:text disable-output-escaping="yes">Sub-Contractors</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'KZ'">
            <xsl:text disable-output-escaping="yes">Contractors - Non-Specific</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'LA'">
            <xsl:text disable-output-escaping="yes">Air Conditioning/Heating/Plumbing/Electrical Sales</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'LF'">
            <xsl:text disable-output-escaping="yes">Fixture Suppliers</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'LH'">
            <xsl:text disable-output-escaping="yes">Hardware Stores</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'LP'">
            <xsl:text disable-output-escaping="yes">Paint, Glass, Wallpaper Stores</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'LY'">
            <xsl:text disable-output-escaping="yes">Lumber Yard</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'LZ'">
            <xsl:text disable-output-escaping="yes">Lumber/Building Material/Hardware - Non-Specific</xsl:text>
        </xsl:when>

        <xsl:when test="$value = 'MA'">
            <xsl:text disable-output-escaping="yes">Animal Hospitals</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'MB'">
            <xsl:text disable-output-escaping="yes">Dentists</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'MC'">
            <xsl:text disable-output-escaping="yes">Chiropractors</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'MD'">
            <xsl:text disable-output-escaping="yes">Doctors</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'MF'">
            <xsl:text disable-output-escaping="yes">Funeral Homes</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'MG'">
            <xsl:text disable-output-escaping="yes">Medical Group</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'MH'">
            <xsl:text disable-output-escaping="yes">Hospitals And Clinics</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'MM'">
            <xsl:text disable-output-escaping="yes">Cemeteries</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'MO'">
            <xsl:text disable-output-escaping="yes">Osteopaths</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'MP'">
            <xsl:text disable-output-escaping="yes">Pharmacies And Drug Stores</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'MS'">
            <xsl:text disable-output-escaping="yes">Optometrists And Optical Stores</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'MV'">
            <xsl:text disable-output-escaping="yes">Veterinarians</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'MZ'">
            <xsl:text disable-output-escaping="yes">Medical And Health Related - Non-Specific</xsl:text>
        </xsl:when>

        <xsl:when test="$value = 'NA'">
            <xsl:text disable-output-escaping="yes">Airlines</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'ND'">
            <xsl:text disable-output-escaping="yes">Credit Card - Dept. Store</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'NF'">
            <xsl:text disable-output-escaping="yes">Credit Card - Finance Company</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'NS'">
            <xsl:text disable-output-escaping="yes">Credit Card - Savings and Loan</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'NU'">
            <xsl:text disable-output-escaping="yes">Credit Card - Credit Union</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'NZ'">
            <xsl:text disable-output-escaping="yes">National Credit Card/Airlines - Non-Specific</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'OC'">
            <xsl:text disable-output-escaping="yes">Oil Company Credit Cards</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'OZ'">
            <xsl:text disable-output-escaping="yes">Oil Companies - Non-Specific</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'PA'">
            <xsl:text disable-output-escaping="yes">Accountants And Related Services</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'PB'">
            <xsl:text disable-output-escaping="yes">Barber And Beauty Shops</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'PC'">
            <xsl:text disable-output-escaping="yes">Equipment Leasing</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'PD'">
            <xsl:text disable-output-escaping="yes">Dry Cleaning/Laundry/Related</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'PE'">
            <xsl:text disable-output-escaping="yes">Engineering/All Kinds</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'PF'">
            <xsl:text disable-output-escaping="yes">Florists</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'PG'">
            <xsl:text disable-output-escaping="yes">Photographers</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'PH'">
            <xsl:text disable-output-escaping="yes">Health And Fitness Clubs</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'PI'">
            <xsl:text disable-output-escaping="yes">Detective Service</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'PL'">
            <xsl:text disable-output-escaping="yes">Legal And Related Services</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'PM'">
            <xsl:text disable-output-escaping="yes">Check Cashing Service</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'PN'">
            <xsl:text disable-output-escaping="yes">Restaurants/Concessions</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'PP'">
            <xsl:text disable-output-escaping="yes">Pest Control</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'PR'">
            <xsl:text disable-output-escaping="yes">Country Clubs</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'PS'">
            <xsl:text disable-output-escaping="yes">Employment Screening</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'PT'">
            <xsl:text disable-output-escaping="yes">Consumer Credit Counseling Service</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'PX'">
            <xsl:text disable-output-escaping="yes">Prequalification Sservices</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'PY'">
            <xsl:text disable-output-escaping="yes">Internet Prequalification Services</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'PZ'">
            <xsl:text disable-output-escaping="yes">Personal Services/Non-Medical - Non-Specific</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'QZ'">
            <xsl:text disable-output-escaping="yes">Mail Order Houses - Non-Specific</xsl:text>
        </xsl:when>

        <xsl:when test="$value = 'RA'">
            <xsl:text disable-output-escaping="yes">Apartments</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'RC'">
            <xsl:text disable-output-escaping="yes">Office Leasing</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'RD'">
            <xsl:text disable-output-escaping="yes">Mobile Home Dealers</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'RE'">
            <xsl:text disable-output-escaping="yes">Real Estate Sales And Rentals</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'RH'">
            <xsl:text disable-output-escaping="yes">Hotels</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'RM'">
            <xsl:text disable-output-escaping="yes">Motels</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'RP'">
            <xsl:text disable-output-escaping="yes">Mobile Home Parks</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'RR'">
            <xsl:text disable-output-escaping="yes">Property And Property Management Companies</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'RZ'">
            <xsl:text disable-output-escaping="yes">Real Estate/Public Accommodations - Non-Specific</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'SA'">
            <xsl:text disable-output-escaping="yes">Aircraft Sales And Service</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'SB'">
            <xsl:text disable-output-escaping="yes">Boats And Marinas Sales And Service</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'SM'">
            <xsl:text disable-output-escaping="yes">Motorcycles And Bicycles Sales</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'SZ'">
            <xsl:text disable-output-escaping="yes">Sporting Goods - Non-Specific</xsl:text>
        </xsl:when>

        <xsl:when test="$value = 'TC'">
            <xsl:text disable-output-escaping="yes">Farm Chemical And Fertilizer Stores</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'TF'">
            <xsl:text disable-output-escaping="yes">Feed And Feed Stores</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'TN'">
            <xsl:text disable-output-escaping="yes">Nursery And Landscaping</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'TZ'">
            <xsl:text disable-output-escaping="yes">Farm And Garden Suppliers - Non-Specific</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'UA'">
            <xsl:text disable-output-escaping="yes">Water Utilities/Bottled Water</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'UC'">
            <xsl:text disable-output-escaping="yes">Cable TV Providers</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'UD'">
            <xsl:text disable-output-escaping="yes">Garbage And Rubbish Disposal</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'UE'">
            <xsl:text disable-output-escaping="yes">Electric Light And Power Company</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'UF'">
            <xsl:text disable-output-escaping="yes">Fuel Oil Distributors</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'UG'">
            <xsl:text disable-output-escaping="yes">Gas Company, Natural And Bottled</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'UH'">
            <xsl:text disable-output-escaping="yes">Coal/Wood Suppliers</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'UL'">
            <xsl:text disable-output-escaping="yes">Long Distance Telephone Service Providers</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'UO'">
            <xsl:text disable-output-escaping="yes">Online/Internet Access Providers</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'UP'">
            <xsl:text disable-output-escaping="yes">Paging Companies</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'UR'">
            <xsl:text disable-output-escaping="yes">Waste Recycling/Hazardous Waste Handlers</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'US'">
            <xsl:text disable-output-escaping="yes">Satellite TV/Direct Broadcast Satellite Providers</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'UT'">
            <xsl:text disable-output-escaping="yes">Telephone Companies</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'UV'">
            <xsl:text disable-output-escaping="yes">Home Security Company</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'UW'">
            <xsl:text disable-output-escaping="yes">Wireless Telephone Service Providers</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'UZ'">
            <xsl:text disable-output-escaping="yes">Utilities And Fuel - Non-Specific</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'VC'">
            <xsl:text disable-output-escaping="yes">City And County Government</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'VF'">
            <xsl:text disable-output-escaping="yes">Federal Government</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'VK'">
            <xsl:text disable-output-escaping="yes">Child Support</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'VL'">
            <xsl:text disable-output-escaping="yes">Law Enforcement</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'VS'">
            <xsl:text disable-output-escaping="yes">State Government</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'VX'">
            <xsl:text disable-output-escaping="yes">Court Codes</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'VZ'">
            <xsl:text disable-output-escaping="yes">Government - Non-Specific</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'WA'">
            <xsl:text disable-output-escaping="yes">Automotive Supplies</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'WB'">
            <xsl:text disable-output-escaping="yes">Building Supplies/Hardware</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'WC'">
            <xsl:text disable-output-escaping="yes">Clothing And Dry Goods</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'WD'">
            <xsl:text disable-output-escaping="yes">Drugs, Chemicals And Related Goods</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'WG'">
            <xsl:text disable-output-escaping="yes">Grocery and Related Products</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'WH'">
            <xsl:text disable-output-escaping="yes">Home Furnishings</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'WM'">
            <xsl:text disable-output-escaping="yes">Machinery, Equipment Supplies</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'WP'">
            <xsl:text disable-output-escaping="yes">Credit Card Processors</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'WZ'">
            <xsl:text disable-output-escaping="yes">Wholesale - Non-Specific</xsl:text>
        </xsl:when>

        <xsl:when test="$value = 'XA'">
            <xsl:text disable-output-escaping="yes">Advertising Agencies</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'XD'">
            <xsl:text disable-output-escaping="yes">Direct Mail List Services</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'XL'">
            <xsl:text disable-output-escaping="yes">List Processing Vendors</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'XM'">
            <xsl:text disable-output-escaping="yes">Media</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'XZ'">
            <xsl:text disable-output-escaping="yes">Miscellaneous Advertising</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'YA'">
            <xsl:text disable-output-escaping="yes">Collection Department - ACB Credit Bureau</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'YB'">
            <xsl:text disable-output-escaping="yes">Collection Department - Bank</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'YC'">
            <xsl:text disable-output-escaping="yes">Other Collection Agencies</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'YD'">
            <xsl:text disable-output-escaping="yes">Collection Department - Department Store</xsl:text>
        </xsl:when>
          <xsl:when test="$value = 'YF'">
            <xsl:text disable-output-escaping="yes">Collection Department - Loan Company</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'YL'">
            <xsl:text disable-output-escaping="yes">Collections Attorney</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'YR'">
            <xsl:text disable-output-escaping="yes">Repo Company</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'YZ'">
            <xsl:text disable-output-escaping="yes">Collections - Non-Specific</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'ZA'">
            <xsl:text disable-output-escaping="yes">Auto Reseller</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'ZB'">
            <xsl:text disable-output-escaping="yes">Credit Report Brokers</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'ZC'">
            <xsl:text disable-output-escaping="yes">Credit Bureaus</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'ZD'">
            <xsl:text disable-output-escaping="yes">Reseller - Direct to Customer</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'ZE'">
            <xsl:text disable-output-escaping="yes">Employment Reseller</xsl:text>
        </xsl:when>
          <xsl:when test="$value = 'ZF'">
            <xsl:text disable-output-escaping="yes">Finance Reseller</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'ZG'">
            <xsl:text disable-output-escaping="yes">IRSG Reseller 5A</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'ZH'">
            <xsl:text disable-output-escaping="yes">IRSG Reseller 5B</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'ZI'">
            <xsl:text disable-output-escaping="yes">Insurance Reseller</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'ZL'">
            <xsl:text disable-output-escaping="yes">Leasing and Rental Reseller</xsl:text>
        </xsl:when>
         <xsl:when test="$value = 'ZM'">
            <xsl:text disable-output-escaping="yes">Manufacturing</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'ZP'">
            <xsl:text disable-output-escaping="yes">Personal Service Reseller</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'ZR'">
            <xsl:text disable-output-escaping="yes">Retail, Not Elsewhere Classified</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'ZS'">
            <xsl:text disable-output-escaping="yes">Services, Not Elsewhere Classified</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'ZT'">
            <xsl:text disable-output-escaping="yes">Tenant Screeners (Reseller)</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'ZW'">
            <xsl:text disable-output-escaping="yes">Wholesale, Not Elsewhere Classified</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'ZY'">
            <xsl:text disable-output-escaping="yes">Collection Reseller</xsl:text>
        </xsl:when>
        <xsl:when test="$value = 'ZZ'">
            <xsl:text disable-output-escaping="yes">All others not classified elsewhere</xsl:text>
        </xsl:when>

        <xsl:otherwise>
            <xsl:value-of select="$value"/>
    </xsl:otherwise>
    </xsl:choose>

  </xsl:template>
<!-- above template code added newly on 04/03/10 to handle code descriptions problem -->


  <!--
  *********************************************
  * translate legal filing status template
  * code is status code
  *********************************************
  -->
  <xsl:template name="translateLegalFilingStatus">
    <xsl:param name="code" select="''" />
      
    <xsl:choose>                      
    <xsl:when test="number($code) = 1">
            <xsl:text disable-output-escaping="yes">Filed</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 2">
            <xsl:text disable-output-escaping="yes">Amended</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 3">
            <xsl:text disable-output-escaping="yes">Assigned</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 4">
            <xsl:text disable-output-escaping="yes">Partial Release</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 5">
            <xsl:text disable-output-escaping="yes">Full Release</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 6">
            <xsl:text disable-output-escaping="yes">Filed</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 7">
            <xsl:text disable-output-escaping="yes">Released</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 8">
            <xsl:text disable-output-escaping="yes">Terminated</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 9">
            <xsl:text disable-output-escaping="yes">Continued</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 10">
            <xsl:text disable-output-escaping="yes">Satisfied</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 11">
            <xsl:text disable-output-escaping="yes">Abstract</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 12">
            <xsl:text disable-output-escaping="yes">Chapter 7 Involuntary</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 13">
            <xsl:text disable-output-escaping="yes">Chapter 7 Voluntary</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 14">
            <xsl:text disable-output-escaping="yes">Chapter 7</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 15">
            <xsl:text disable-output-escaping="yes">Chapter 11 Involuntary</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 16">
            <xsl:text disable-output-escaping="yes">Chapter 11 Voluntary</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 17">
            <xsl:text disable-output-escaping="yes">Chapter 11</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 18">
            <xsl:text disable-output-escaping="yes">Involuntary</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 19">
            <xsl:text disable-output-escaping="yes">Voluntary</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 20">
            <xsl:text disable-output-escaping="yes">Chapter 13</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 21">
            <xsl:text disable-output-escaping="yes">Vacated</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 22">
            <xsl:text disable-output-escaping="yes">Chapter 7 Dismissed</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 23">
            <xsl:text disable-output-escaping="yes">Chapter 11 Dismissed</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 24">
            <xsl:text disable-output-escaping="yes">Chapter 7 Discharged</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 25">
            <xsl:text disable-output-escaping="yes">Chapter 11 Discharged</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 26">
            <xsl:text disable-output-escaping="yes">Chapter 13 Completed</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 27">
            <xsl:text disable-output-escaping="yes">Chapter 13 Dismissed</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 28">
            <xsl:text disable-output-escaping="yes">Chapter 12</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 29">
            <xsl:text disable-output-escaping="yes">Chapter 12 Dismissed</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 30">
            <xsl:text disable-output-escaping="yes">Chapter 12 Discharged</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 31">
            <xsl:text disable-output-escaping="yes">UCC Filed 10 Years</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 32">
            <xsl:text disable-output-escaping="yes">Chapter 10</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 33">
            <xsl:text disable-output-escaping="yes">Chapter 10 Dismissed</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 34">
            <xsl:text disable-output-escaping="yes">Chapter 10 Discharged</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 35">
            <xsl:text disable-output-escaping="yes">Chapter 10 Closed</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 36">
            <xsl:text disable-output-escaping="yes">Chapter 7 Closed</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 37">
            <xsl:text disable-output-escaping="yes">Chapter 11 Closed</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 38">
            <xsl:text disable-output-escaping="yes">Chapter 12 Closed</xsl:text>
        </xsl:when>
        <xsl:when test="number($code) = 39">
            <xsl:text disable-output-escaping="yes">Chapter 13 Closed</xsl:text>
        </xsl:when>
    <xsl:otherwise>
            <xsl:text disable-output-escaping="yes">Unknown</xsl:text>
    </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>


  <!--
  *********************************************
  * translate fraud shield summary template
  * code is FraudServices indicator
  *********************************************
  -->
  <xsl:template name="translateFraudServicesIndicator">
    <xsl:param name="code" select="''" />

    <xsl:choose>                      
    <xsl:when test="number($code) = 1">
            <xsl:text disable-output-escaping="yes">Inquiry/onfile current address conflict</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 2">
            <xsl:text disable-output-escaping="yes">Inquiry address 1st reported &lt; 90 days</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 3">
            <xsl:text disable-output-escaping="yes">Inquiry current address not onfile</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 4">
            <xsl:text disable-output-escaping="yes">Inquiry SSN has not been issued</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 5">
            <xsl:text disable-output-escaping="yes">Inquiry SSN recorded as deceased</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 6">
            <xsl:text disable-output-escaping="yes">Inquiry age younger than SSN issue date</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 7">
            <xsl:text disable-output-escaping="yes">Credit established before age 18</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 8">
            <xsl:text disable-output-escaping="yes">Credit established prior to SSN issue date</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 9">
            <xsl:text disable-output-escaping="yes">More than 3 inquiries in last 30 days</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 10">
            <xsl:text disable-output-escaping="yes">Inquiry address: alert</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 11">
            <xsl:text disable-output-escaping="yes">Inquiry address: non-residential</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 12">
            <xsl:text disable-output-escaping="yes">Security statement present on report</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 13">
            <xsl:text disable-output-escaping="yes">High probability SSN belongs to another</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 14">
            <xsl:text disable-output-escaping="yes">Inquiry SSN format is invalid</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 15">
            <xsl:text disable-output-escaping="yes">Inquiry address: cautious</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 16">
            <xsl:text disable-output-escaping="yes">Onfile address: alert</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 17">
            <xsl:text disable-output-escaping="yes">Onfile address: non-residential</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 18">
            <xsl:text disable-output-escaping="yes">Onfile address: cautious</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 19">
            <xsl:text disable-output-escaping="yes">Current address rpt by new trade only</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 20">
            <xsl:text disable-output-escaping="yes">Current address rpt by trade open &lt; 90 days</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 21">
            <xsl:text disable-output-escaping="yes">Telephone number inconsistent w/address</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 22">
            <xsl:text disable-output-escaping="yes">Drivers license inconsistent w/onfile</xsl:text>
        </xsl:when>
    <xsl:otherwise>
            <xsl:text disable-output-escaping="yes">No record found</xsl:text>
    </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>


  <!--
  *********************************************
  * translate unscorable ScorexPlus template
  * code is ScorexPlus unscorable score
  *********************************************
  -->
  <xsl:template name="translateUnscorableScorexPlus">
    <xsl:param name="code" select="''" />

    <xsl:choose>                      
    <xsl:when test="number($code) = 9000">
            <xsl:text disable-output-escaping="yes">The Credit profile contains more than 500 tradelines, inquiries, and public records</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 9001">
            <xsl:text disable-output-escaping="yes">One or more tradelines on the Credit Profile has a deceased status</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 9003">
            <xsl:text disable-output-escaping="yes">The Credit Profile does not contain any valid tradelines</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 9005">
            <xsl:text disable-output-escaping="yes">Credit profile has had no activity in the last 12 months</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 9006">
            <xsl:text disable-output-escaping="yes">Block Consumer: The file belongs to an individual on Experian's block consumer file. These are names of individuals Experian will not sell</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 9007">
            <xsl:text disable-output-escaping="yes">Edit rejects</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 9008">
            <xsl:text disable-output-escaping="yes">Insufficient Information: Unable to identify consumer due to insufficient information</xsl:text>
        </xsl:when>
    <xsl:otherwise>
            <xsl:text disable-output-escaping="yes">No record found</xsl:text>
    </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>


  <!--
  *********************************************
  * translate Vantage V3 Scorex template
  * code is Vantage V3 score
  *********************************************
  -->
  <xsl:template name="translateVantageThreeScoreFactorTable">
    <xsl:param name="code" select="''" />

    <xsl:choose>                      
        <xsl:when test="normalize-space($code) = '4'"><xsl:text disable-output-escaping="yes">THE BALANCES ON YOUR ACCOUNTS ARE TOO HIGH COMPARED TO LOAN LIMITS</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '5'"><xsl:text disable-output-escaping="yes">TOO MANY OF THE DELINQUENCIES ON YOUR ACCOUNTS ARE RECENT</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '6'"><xsl:text disable-output-escaping="yes">YOU HAVE TOO MANY ACCOUNTS THAT WERE OPENED RECENTLY</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '7'"><xsl:text disable-output-escaping="yes">YOU HAVE TOO MANY DELINQUENT OR DEROGATORY ACCOUNTS</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '8'"><xsl:text disable-output-escaping="yes">YOU HAVE EITHER TOO FEW LOANS OR TOO MANY LOANS WITH RECENT DELINQUENCIES</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '9'"><xsl:text disable-output-escaping="yes">THE MOST SERIOUS PAYMENT STATUS ON YOUR ACCTS IS DELINQUENT OR DEROGATORY</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '10'"><xsl:text disable-output-escaping="yes">YOU HAVE NOT PAID ENOUGH OF YOUR ACCOUNTS ON TIME</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '11'"><xsl:text disable-output-escaping="yes">THE TOTAL OF YOUR DELINQUENT OR DEROGATORY ACCOUNT BALANCES IS TOO HIGH</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '12'"><xsl:text disable-output-escaping="yes">THE DATE THAT YOU OPENED YOUR OLDEST ACCOUNT IS TOO RECENT</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '13'"><xsl:text disable-output-escaping="yes">YOUR MOST RECENTLY OPENED ACCOUNT IS TOO NEW</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '14'"><xsl:text disable-output-escaping="yes">LACK OF SUFFICIENT CREDIT HISTORY</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '15'"><xsl:text disable-output-escaping="yes">NEWEST DELINQUENT/DEROGATORY PAYMENT STATUS ON YOUR ACCTS IS TOO RECENT</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '16'"><xsl:text disable-output-escaping="yes">THE TOTAL OF ALL BALANCES ON YOUR OPEN ACCOUNTS IS TOO HIGH</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '17'"><xsl:text disable-output-escaping="yes">BALANCE ON PREVIOUSLY DELINQUENT ACCTS ARE TOO HIGH COMPARED TO LOAN AMTS</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '18'"><xsl:text disable-output-escaping="yes">TOTAL OF BALANCES ON ACCTS NEVER LATE IS TOO HIGH COMPARED TO LOAN AMTS</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '21'"><xsl:text disable-output-escaping="yes">NO OPEN ACCOUNTS IN YOUR CREDIT FILE</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '22'"><xsl:text disable-output-escaping="yes">NO RECENTLY REPORTED ACCOUNT INFORMATION</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '23'"><xsl:text disable-output-escaping="yes">LACK OF SUFFICIENT RELEVANT ACCOUNT INFORMATION</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '29'"><xsl:text disable-output-escaping="yes">TOO MANY OF YOUR OPEN BANK CARD OR REVOLVING ACCOUNTS HAVE A BALANCE</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '30'"><xsl:text disable-output-escaping="yes">TOO FEW OF YOUR BANK CARD OR OTHER REVOLVING ACCOUNTS HAVE HIGH LIMITS</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '31'"><xsl:text disable-output-escaping="yes">TOO MANY BANK CARD OR OTHER REVOLVING ACCOUNTS WERE OPENED RECENTLY</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '32'"><xsl:text disable-output-escaping="yes">BALANCES ON BANKCARD/REVOLVING ACCTS TOO HIGH COMPARED TO CREDIT LIMITS</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '33'"><xsl:text disable-output-escaping="yes">YOUR WORST BANKCARD OR REVOLVING ACCOUNTS STATUS IS DELINQUENT/DEROGATORY</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '34'"><xsl:text disable-output-escaping="yes">TOTAL OF ALL BALANCES ON BANK CARD OR REVOLVING ACCOUNTS IS TOO HIGH</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '35'"><xsl:text disable-output-escaping="yes">YOUR HIGHEST BANKCARD OR REVOLVING ACCOUNT BALANCE IS TOO HIGH</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '36'"><xsl:text disable-output-escaping="yes">YOUR LARGEST CREDIT LIMIT ON OPEN BANKCARD OR REVOLVING ACCOUNTS IS TOO LOW</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '39'"><xsl:text disable-output-escaping="yes">AVAILABLE CREDIT ON YOUR OPEN BANKCARD OR REVOLVING ACCOUNTS IS TOO LOW</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '40'"><xsl:text disable-output-escaping="yes">THE DATE YOU OPENED YOUR OLDEST BANK CARD OR REVOLVING ACCT IS TOO RECENT</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '42'"><xsl:text disable-output-escaping="yes">THE DATE YOU OPENED YOUR NEWEST BANK CARD OR REVOLVING ACCT IS TOO RECENT</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '43'"><xsl:text disable-output-escaping="yes">LACK OF SUFFICIENT CREDIT HISTORY ON BANK CARD OR REVOLVING ACCOUNTS</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '44'"><xsl:text disable-output-escaping="yes">TOO MANY BANKCARD OR REVOLVING ACCOUNTS WITH DELINQUENT/DEROGATORY STATUS</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '45'"><xsl:text disable-output-escaping="yes">TOTAL BALANCES TOO HIGH ON DELINQUENT/DEROGATORY BANKCARD/REVOLVING ACCTS</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '46'"><xsl:text disable-output-escaping="yes">NO BANKCARD, REVOLVING ACCTS THAT CAN BE USED TO DETERMINE A CREDIT SCORE</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '47'"><xsl:text disable-output-escaping="yes">NO OPEN BANK CARD OR REVOLVING ACCOUNTS IN YOUR CREDIT FILE</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '48'"><xsl:text disable-output-escaping="yes">NO BANKCARD OR REVOLVING RECENTLY REPORTED ACCOUNT INFORMATION</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '49'"><xsl:text disable-output-escaping="yes">LACK OF SUFFICIENT RELEVANT BANKCARD OR REVOLVING ACCOUNT INFORMATION</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '53'"><xsl:text disable-output-escaping="yes">THE WORST STATUS ON YOUR REAL ESTATE ACCOUNTS IS DELINQUENT OR DEROGATORY</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '54'"><xsl:text disable-output-escaping="yes">THE AMT OF BALANCE PAID DOWN ON YOUR OPEN REAL ESTATE ACCOUNTS IS TOO LOW</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '55'"><xsl:text disable-output-escaping="yes">OPEN REAL ESTATE ACCT BALANCES ARE TOO HIGH COMPARED TO THEIR LOAN AMTS</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '56'"><xsl:text disable-output-escaping="yes">TOO FEW REAL ESTATE ACCOUNTS</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '57'"><xsl:text disable-output-escaping="yes">TOO MANY REAL ESTATE ACCTS WITH DELINQUENT OR DEROGATORY PAYMENT STATUS</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '58'"><xsl:text disable-output-escaping="yes">THE TOTAL OF ALL BALANCES ON YOUR OPEN REAL ESTATE ACCOUNTS IS TOO HIGH</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '59'"><xsl:text disable-output-escaping="yes">NEWEST DELINQUENT OR DEROGATORY STATUS ON REAL ESTATE ACCTS IS TOO RECENT</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '61'"><xsl:text disable-output-escaping="yes">NO OPEN REAL ESTATE ACCOUNTS IN YOUR CREDIT FILE</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '62'"><xsl:text disable-output-escaping="yes">NO RECENTLY REPORTED REAL ESTATE ACCOUNT INFORMATION</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '63'"><xsl:text disable-output-escaping="yes">LACK OF SUFFICIENT RELEVANT REAL ESTATE ACCOUNT INFORMATION</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '66'"><xsl:text disable-output-escaping="yes">YOUR OPEN AUTO ACCOUNT BALANCES ARE TOO HIGH COMPARED TO THEIR LOAN AMTS</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '67'"><xsl:text disable-output-escaping="yes">NO AUTO ACCOUNTS THAT CAN BE USED TO DETERMINE A CREDIT SCORE</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '68'"><xsl:text disable-output-escaping="yes">NO OPEN AUTO ACCOUNTS IN YOUR CREDIT FILE</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '69'"><xsl:text disable-output-escaping="yes">LACK OF SUFFICIENT RELEVANT AUTO ACCOUNT INFORMATION</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '71'"><xsl:text disable-output-escaping="yes">YOU HAVE NOT PAID ENOUGH OF YOUR INSTALLMENT ACCOUNTS ON TIME</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '72'"><xsl:text disable-output-escaping="yes">TOO MANY INSTALLMENT ACCTS WITH A DELINQUENT OR DEROGATORY PAYMENT STATUS</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '73'"><xsl:text disable-output-escaping="yes">THE WORST STATUS ON YOUR INSTALLMENT ACCOUNTS IS DELINQUENT OR DEROGATORY</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '74'"><xsl:text disable-output-escaping="yes">THE BALANCE AMOUNT PAID DOWN ON YOUR OPEN INSTALLMENT ACCOUNTS IS TOO LOW</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '75'"><xsl:text disable-output-escaping="yes">THE INSTALLMENT ACCOUNT THAT YOU OPENED MOST RECENTLY IS TOO NEW</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '76'"><xsl:text disable-output-escaping="yes">YOU HAVE INSUFFICIENT CREDIT HISTORY ON INSTALLMENT LOANS</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '77'"><xsl:text disable-output-escaping="yes">NEWEST DELINQUENT OR DEROGATORY STATUS ON INSTALLMENT ACCTS IS TOO RECENT</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '78'"><xsl:text disable-output-escaping="yes">BALANCES ON INSTALLMENT ACCTS ARE TOO HIGH COMPARED TO THEIR LOAN AMOUNTS</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '79'"><xsl:text disable-output-escaping="yes">TOO MANY OF THE DELINQUENCIES ON YOUR INSTALLMENT ACCOUNTS ARE RECENT</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '81'"><xsl:text disable-output-escaping="yes">NO OPEN INSTALLMENT ACCOUNTS IN YOUR CREDIT FILE</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '82'"><xsl:text disable-output-escaping="yes">NO RECENTLY REPORTED INSTALLMENT ACCOUNT INFORMATION</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '83'"><xsl:text disable-output-escaping="yes">LACK OF SUFFICIENT RELEVANT INSTALLMENT ACCOUNT INFORMATION</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '84'"><xsl:text disable-output-escaping="yes">NUMBER OF INQUIRIES IMPACTED YOUR SCORE, BUT EFFECT WAS NOT SIGNIFICANT</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '85'"><xsl:text disable-output-escaping="yes">YOU HAVE TOO MANY INQUIRIES ON YOUR CREDIT REPORT.</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '86'"><xsl:text disable-output-escaping="yes">YOUR CREDIT REPORT CONTAINS TOO MANY DEROGATORY PUBLIC RECORDS</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '87'"><xsl:text disable-output-escaping="yes">YOUR CREDIT REPORT CONTAINS TOO MANY UNSATISFIED PUBLIC RECORDS</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '88'"><xsl:text disable-output-escaping="yes">ONE OR MORE DEROGATORY PUBLIC RECORDS IN YOUR CREDIT FILE IS TOO RECENT</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '90'"><xsl:text disable-output-escaping="yes">TOO FEW DISCHARGED BANKRUPTCIES</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '93'"><xsl:text disable-output-escaping="yes">THE WORST STATUS ON YOUR STUDENT LOAN ACCTS IS DELINQUENT OR DEROGATORY</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '94'"><xsl:text disable-output-escaping="yes">THE BALANCE AMOUNT PAID DOWN ON YOUR OPEN STUDENT LOAN ACCTS IS TOO LOW</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '95'"><xsl:text disable-output-escaping="yes">YOU HAVE TOO MANY COLLECTION AGENCY ACCOUNTS THAT ARE UNPAID</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '96'"><xsl:text disable-output-escaping="yes">THE TOTAL YOU OWE ON COLLECTION AGENCY ACCOUNTS IS HIGH</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '97'"><xsl:text disable-output-escaping="yes">YOU HAVE TOO FEW CREDIT ACCOUNTS</xsl:text></xsl:when>
        <xsl:when test="normalize-space($code) = '98'"><xsl:text disable-output-escaping="yes">HERE IS A BANKRUPTCY ON YOUR CREDIT REPORT</xsl:text></xsl:when>
        <xsl:otherwise><xsl:text disable-output-escaping="yes">NO RECORD FOUND</xsl:text></xsl:otherwise>
    </xsl:choose>
  </xsl:template>


  <!--
  *********************************************
  * translate unscorable ScorexPlus template
  * code is ScorexPlus unscorable score
  *********************************************
  -->
  <xsl:template name="translateArfScorexRiskFactorTable">
    <xsl:param name="code" select="''" />

    <xsl:choose>                      
    <xsl:when test="number($code) = 1">
            <xsl:text disable-output-escaping="yes">Lack Of Open Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 2">
            <xsl:text disable-output-escaping="yes">Lack Of Open Automotive Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 3">
            <xsl:text disable-output-escaping="yes">Lack Of Open Bankcard Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 5">
            <xsl:text disable-output-escaping="yes">Lack Of Open Installment Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 6">
            <xsl:text disable-output-escaping="yes">Lack Of Open Real-Estate Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 7">
            <xsl:text disable-output-escaping="yes">Lack Of Open Revolving Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 8">
            <xsl:text disable-output-escaping="yes">Lack Of Open Retail Revolving Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 10">
            <xsl:text disable-output-escaping="yes">Number Of Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 11">
            <xsl:text disable-output-escaping="yes">Number Of Recently Opened Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 13">
            <xsl:text disable-output-escaping="yes">Number Of Recently Opened Bankcard Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 14">
            <xsl:text disable-output-escaping="yes">Number Of Collection Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 16">
            <xsl:text disable-output-escaping="yes">Number Of Finance Installment Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 18">
            <xsl:text disable-output-escaping="yes">Number Of Recently Opened Finance Installment Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 19">
            <xsl:text disable-output-escaping="yes">Number Of Real-Estate Trades</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 20">
            <xsl:text disable-output-escaping="yes">Number Of Revolving Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 23">
            <xsl:text disable-output-escaping="yes">Number Of Recently Opened Auto Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 24">
            <xsl:text disable-output-escaping="yes">Number Of Retail Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 28">
            <xsl:text disable-output-escaping="yes">Balances On Revolving Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 30">
            <xsl:text disable-output-escaping="yes">Credit Limits/Loan Amounts On Open Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 31">
            <xsl:text disable-output-escaping="yes">Credit Limits On Open Bankcard Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 34">
            <xsl:text disable-output-escaping="yes">Loan Amounts On Real-Estate Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 35">
            <xsl:text disable-output-escaping="yes">Available Credit On Open Bankcard Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 36">
            <xsl:text disable-output-escaping="yes">Available Credit On Open Revolving Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 37">
            <xsl:text disable-output-escaping="yes">Monthly Payment On Open Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 40">
            <xsl:text disable-output-escaping="yes">Number Of Accounts With High Balance-To-Limit Ratios</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 41">
            <xsl:text disable-output-escaping="yes">Ratio Of Balance To Limit On Open Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 42">
            <xsl:text disable-output-escaping="yes">Ratio Of Balance To Limit On Open Bankcard Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 43">
            <xsl:text disable-output-escaping="yes">Ratio Of Balance To Limit On Open Revolving Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 47">
            <xsl:text disable-output-escaping="yes">Ratio Of Balance To Limit On Open Retail Revolving Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 50">
            <xsl:text disable-output-escaping="yes">Presence Of Delinquent Or Derogatory Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 51">
            <xsl:text disable-output-escaping="yes">Presence Of Delinquent Or Derogatory Auto Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 52">
            <xsl:text disable-output-escaping="yes">Presence Of Delinquent Or Derogatory Bankcard Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 54">
            <xsl:text disable-output-escaping="yes">Presence Of Delinquent Or Derogatory Installment Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 55">
            <xsl:text disable-output-escaping="yes">Presence Of Delinquent Or Derogatory Real-Estate Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 56">
            <xsl:text disable-output-escaping="yes">Presence Of Delinquent Or Derogatory Revolving Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 57">
            <xsl:text disable-output-escaping="yes">Number Of Never-Delinquent Bankcard Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 58">
            <xsl:text disable-output-escaping="yes">Number Of Current Installment Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 59">
            <xsl:text disable-output-escaping="yes">Number Of Recently Delinquent Installment Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 60">
            <xsl:text disable-output-escaping="yes">Number Of Current Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 61">
            <xsl:text disable-output-escaping="yes">Number Of Delinquent Or Derogatory Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 63">
            <xsl:text disable-output-escaping="yes">Number Of Recently Delinquent Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 64">
            <xsl:text disable-output-escaping="yes">Number Of Never-Delinquent Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 67">
            <xsl:text disable-output-escaping="yes">Number Of Accounts With Past-Due Balances</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 70">
            <xsl:text disable-output-escaping="yes">Age Of Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 72">
            <xsl:text disable-output-escaping="yes">Age Of Revolving Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 80">
            <xsl:text disable-output-escaping="yes">Number Of Inquiries</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 81">
            <xsl:text disable-output-escaping="yes">Number Of Recent Inquiries</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 85">
            <xsl:text disable-output-escaping="yes">Presence Of Derogatory Public Record</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 87">
            <xsl:text disable-output-escaping="yes">Balances On Public Record Information</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 90">
            <xsl:text disable-output-escaping="yes">Recency Of Delinquent Or Derogatory Account</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 93">
            <xsl:text disable-output-escaping="yes">Number Of Never-Delinquent Revolving Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 94">
            <xsl:text disable-output-escaping="yes">Number Of Never-Delinquent Real-Estate Accounts</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 95">
            <xsl:text disable-output-escaping="yes">Presence Of Delinquent Or Derogatory Retail Revolving Accounts</xsl:text>
        </xsl:when>
    <xsl:otherwise>
            <xsl:text disable-output-escaping="yes">No record found</xsl:text>
    </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>


  <!--
  *********************************************
  * TranslateState template
  * Value has to be 2 char state
  *********************************************
  -->
  <xsl:template name="TranslateState">
    <xsl:param name="value" select="''" />
    <xsl:param name="upperCase" select="false()" />
      
    <xsl:variable name="state">
      <xsl:value-of select="translate($value, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')" />
    </xsl:variable>
    
    <xsl:variable name="result">
      <xsl:choose>                    
        <xsl:when test="$state = 'AL'">
          <xsl:value-of select="'Alabama'" />
        </xsl:when>
  
        <xsl:when test="$state = 'AK'">
          <xsl:value-of select="'Alaska'" />
        </xsl:when>
  
        <xsl:when test="$state = 'AS'">
          <xsl:value-of select="'American Samoa'" />
        </xsl:when>
  
        <xsl:when test="$state = 'AZ'">
          <xsl:value-of select="'Arizona'" />
        </xsl:when>
  
        <xsl:when test="$state = 'AR'">
          <xsl:value-of select="'Arkansas'" />
        </xsl:when>
  
        <xsl:when test="$state = 'CA'">
          <xsl:value-of select="'California'" />
        </xsl:when>
  
        <xsl:when test="$state = 'CO'">
          <xsl:value-of select="'Colorado'" />
        </xsl:when>
  
        <xsl:when test="$state = 'CT'">
          <xsl:value-of select="'Connecticut'" />
        </xsl:when>
  
        <xsl:when test="$state = 'DE'">
          <xsl:value-of select="'Delaware'" />
        </xsl:when>
  
        <xsl:when test="$state = 'DC'">
          <xsl:value-of select="'District of Columbia'" />
        </xsl:when>
  
        <xsl:when test="$state = 'FM'">
          <xsl:value-of select="'Federated States of Micronesia'" />
        </xsl:when>
  
        <xsl:when test="$state = 'FL'">
          <xsl:value-of select="'Florida'" />
        </xsl:when>
  
        <xsl:when test="$state = 'GA'">
          <xsl:value-of select="'Georgia'" />
        </xsl:when>
  
        <xsl:when test="$state = 'GU'">
          <xsl:value-of select="'Guam'" />
        </xsl:when>
  
        <xsl:when test="$state = 'HI'">
          <xsl:value-of select="'Hawaii'" />
        </xsl:when>
  
        <xsl:when test="$state = 'ID'">
          <xsl:value-of select="'Idaho'" />
        </xsl:when>
  
        <xsl:when test="$state = 'IL'">
          <xsl:value-of select="'Illinois'" />
        </xsl:when>
  
        <xsl:when test="$state = 'IN'">
          <xsl:value-of select="'Indiana'" />
        </xsl:when>
  
        <xsl:when test="$state = 'IA'">
          <xsl:value-of select="'Iowa'" />
        </xsl:when>
  
        <xsl:when test="$state = 'KS'">
          <xsl:value-of select="'Kansas'" />
        </xsl:when>
  
        <xsl:when test="$state = 'KY'">
          <xsl:value-of select="'Kentucky'" />
        </xsl:when>
  
        <xsl:when test="$state = 'LA'">
          <xsl:value-of select="'Louisiana'" />
        </xsl:when>
  
        <xsl:when test="$state = 'ME'">
          <xsl:value-of select="'Maine'" />
        </xsl:when>
  
        <xsl:when test="$state = 'MH'">
          <xsl:value-of select="'Marshall Islands'" />
        </xsl:when>
  
        <xsl:when test="$state = 'MD'">
          <xsl:value-of select="'Maryland'" />
        </xsl:when>
  
        <xsl:when test="$state = 'MA'">
          <xsl:value-of select="'Massachusetts'" />
        </xsl:when>
  
        <xsl:when test="$state = 'MI'">
          <xsl:value-of select="'Michigan'" />
        </xsl:when>
  
        <xsl:when test="$state = 'MN'">
          <xsl:value-of select="'Minnesota'" />
        </xsl:when>
  
        <xsl:when test="$state = 'MS'">
          <xsl:value-of select="'Mississippi'" />
        </xsl:when>
  
        <xsl:when test="$state = 'MO'">
          <xsl:value-of select="'Missouri'" />
        </xsl:when>
  
        <xsl:when test="$state = 'MT'">
          <xsl:value-of select="'Montana'" />
        </xsl:when>
  
        <xsl:when test="$state = 'NE'">
          <xsl:value-of select="'Nebraska'" />
        </xsl:when>
  
        <xsl:when test="$state = 'NV'">
          <xsl:value-of select="'Nevada'" />
        </xsl:when>
  
        <xsl:when test="$state = 'NH'">
          <xsl:value-of select="'New Hampshire'" />
        </xsl:when>
  
        <xsl:when test="$state = 'NJ'">
          <xsl:value-of select="'New Jersey'" />
        </xsl:when>
  
        <xsl:when test="$state = 'NM'">
          <xsl:value-of select="'New Mexico'" />
        </xsl:when>
  
        <xsl:when test="$state = 'NY'">
          <xsl:value-of select="'New York'" />
        </xsl:when>
  
        <xsl:when test="$state = 'NC'">
          <xsl:value-of select="'North Carolina'" />
        </xsl:when>
  
        <xsl:when test="$state = 'ND'">
          <xsl:value-of select="'North Dakota'" />
        </xsl:when>
  
        <xsl:when test="$state = 'MP'">
          <xsl:value-of select="'Northern Mariana Island'" />
        </xsl:when>
  
        <xsl:when test="$state = 'OH'">
          <xsl:value-of select="'Ohio'" />
        </xsl:when>
  
        <xsl:when test="$state = 'OK'">
          <xsl:value-of select="'Oklahoma'" />
        </xsl:when>
  
        <xsl:when test="$state = 'OR'">
          <xsl:value-of select="'Oregon'" />
        </xsl:when>
  
        <xsl:when test="$state = 'PW'">
          <xsl:value-of select="'Palau'" />
        </xsl:when>
  
        <xsl:when test="$state = 'PA'">
          <xsl:value-of select="'Pennsylvania'" />
        </xsl:when>
  
        <xsl:when test="$state = 'PR'">
          <xsl:value-of select="'Puerto Rico'" />
        </xsl:when>
  
        <xsl:when test="$state = 'RI'">
          <xsl:value-of select="'Rhode Island'" />
        </xsl:when>
  
        <xsl:when test="$state = 'SC'">
          <xsl:value-of select="'South Carolina'" />
        </xsl:when>
  
        <xsl:when test="$state = 'SD'">
          <xsl:value-of select="'South Dakota'" />
        </xsl:when>
  
        <xsl:when test="$state = 'TN'">
          <xsl:value-of select="'Tennessee'" />
        </xsl:when>
  
        <xsl:when test="$state = 'TX'">
          <xsl:value-of select="'Texas'" />
        </xsl:when>
  
        <xsl:when test="$state = 'UT'">
          <xsl:value-of select="'Utah'" />
        </xsl:when>
  
        <xsl:when test="$state = 'VT'">
          <xsl:value-of select="'Vermont'" />
        </xsl:when>
  
        <xsl:when test="$state = 'VI'">
          <xsl:value-of select="'Virgin Islands'" />
        </xsl:when>
  
        <xsl:when test="$state = 'VA'">
          <xsl:value-of select="'Virginia'" />
        </xsl:when>
  
        <xsl:when test="$state = 'WA'">
          <xsl:value-of select="'Washington'" />
        </xsl:when>
  
        <xsl:when test="$state = 'WV'">  
          <xsl:value-of select="'West Virginia'" />
        </xsl:when>
  
        <xsl:when test="$state = 'WI'">
          <xsl:value-of select="'Wisconsin'" />
        </xsl:when>
  
        <xsl:when test="$state = 'WY'">
          <xsl:value-of select="'Wyoming'" />
        </xsl:when>
  
      </xsl:choose>    
    </xsl:variable>

    <xsl:choose>                      
      <xsl:when test="$upperCase">
        <xsl:value-of select="translate($result, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')" />
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="$result" />
      </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>

  <!--
  *********************************************
  * Determines if a state is canadian province or territory.
  * Returns 'true' if the state is canadian, otherwise returns 'false'
  *********************************************
  -->
  <xsl:template name="isCanadianState">
    <xsl:param name="value" select="''" />
    <xsl:variable name="state">
      <xsl:value-of select="translate($value, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')" />
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$state = 'ON' or $state = 'QC' or $state = 'NS' or $state = 'NB' or $state = 'MB'">
        <xsl:value-of select="'true'"/>
      </xsl:when>
      <xsl:when test="$state = 'BC' or $state = 'PE' or $state = 'SK' or $state = 'AB' or $state = 'NL'">
        <xsl:value-of select="'true'"/>
      </xsl:when>
      <xsl:when test="$state = 'NT' or $state = 'YT' or $state = 'NU'">
        <xsl:value-of select="'true'"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="'false'"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!--
  *********************************************
  * translate AuthAddressHighRiskTable
  *********************************************
  -->
  <xsl:template name="AuthAddressHighRiskTable">
    <xsl:param name="code" select="''" />

    <xsl:choose>                      
    <xsl:when test="normalize-space($code) = 'N' ">
            <xsl:text disable-output-escaping="yes">No address high risk information found</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'NP' ">
            <xsl:text disable-output-escaping="yes">Test not in profile</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'YA' ">
            <xsl:text disable-output-escaping="yes">A high risk business was identified at this address</xsl:text>
        </xsl:when>
    <xsl:otherwise>
            <xsl:text disable-output-escaping="yes">Not available</xsl:text>
    </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>


  <!--
  *********************************************
  * translate AuthAddressVerifTable
  *********************************************
  -->
  <xsl:template name="AuthAddressVerifTable">
    <xsl:param name="code" select="''" />

    <xsl:choose>                      
    <xsl:when test="normalize-space($code) = 'A' ">
            <xsl:text disable-output-escaping="yes">Address ambiguous</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'B' ">
            <xsl:text disable-output-escaping="yes">Match to business name - residential address</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'BB' ">
            <xsl:text disable-output-escaping="yes">Match to business name - business address</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'BM' ">
            <xsl:text disable-output-escaping="yes">Match to business name - mixed use address</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'E' ">
            <xsl:text disable-output-escaping="yes">Matching records exceed maximum defined on profile</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'H' ">
            <xsl:text disable-output-escaping="yes">House number not found on street</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'I' ">
            <xsl:text disable-output-escaping="yes">Incomplete or blank address</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'N' ">
            <xsl:text disable-output-escaping="yes">No match to name - residential address</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'NA' ">
            <xsl:text disable-output-escaping="yes">Data not available</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'NB' ">
            <xsl:text disable-output-escaping="yes">No match to name - business address</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'NM' ">
            <xsl:text disable-output-escaping="yes">No match to name - mixed use address</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'NP' ">
            <xsl:text disable-output-escaping="yes">Test not in profile</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'NS' ">
            <xsl:text disable-output-escaping="yes">Standardization database has expired - contact Help Desk</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'R' ">
            <xsl:text disable-output-escaping="yes">Road name - City/Zip mismatch</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'S' ">
            <xsl:text disable-output-escaping="yes">Match to surname - residential address</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'SB' ">
            <xsl:text disable-output-escaping="yes">Match to surname - business address</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'SM' ">
            <xsl:text disable-output-escaping="yes">Match to surname - mixed use address</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'SX' ">
            <xsl:text disable-output-escaping="yes">Standardization database has expired - contact Help Desk</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'T' ">
            <xsl:text disable-output-escaping="yes">City - State mismatch</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'U' ">
            <xsl:text disable-output-escaping="yes">Address unverifiable - not in database</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'UR' ">
            <xsl:text disable-output-escaping="yes">Address residential - name match unavailable</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'Y' ">
            <xsl:text disable-output-escaping="yes">Match to full name - residential address</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'YB' ">
            <xsl:text disable-output-escaping="yes">Match to full name - business address</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'YM' ">
            <xsl:text disable-output-escaping="yes">Match to full name - mixed use address</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'Z' ">
            <xsl:text disable-output-escaping="yes">City/State - Zip mismatch</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = '00' ">
            <xsl:text disable-output-escaping="yes">unknown message code - contact Help Desk</xsl:text>
        </xsl:when>
    <xsl:otherwise>
            <xsl:text disable-output-escaping="yes">Not available</xsl:text>
    </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>


  <!--
  *********************************************
  * translate AuthAddressTypeTable
  *********************************************
  -->
  <xsl:template name="AuthAddressTypeTable">
    <xsl:param name="code" select="''" />

    <xsl:choose>                      
    <xsl:when test="normalize-space($code) = 'C' ">
            <xsl:text disable-output-escaping="yes">Single company</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'E' ">
            <xsl:text disable-output-escaping="yes">Test error</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'EB' ">
            <xsl:text disable-output-escaping="yes">Seasonal - business</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'EM' ">
            <xsl:text disable-output-escaping="yes">Seasonal - residential</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'EX' ">
            <xsl:text disable-output-escaping="yes">Seasonal - mixed use</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'M' ">
            <xsl:text disable-output-escaping="yes">Residential</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'N' ">
            <xsl:text disable-output-escaping="yes">No information available</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'NA' ">
            <xsl:text disable-output-escaping="yes">Data not available</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'NP' ">
            <xsl:text disable-output-escaping="yes">Test not in profile</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'O' ">
            <xsl:text disable-output-escaping="yes">Office building</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'P' ">
            <xsl:text disable-output-escaping="yes">Post office box</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'S' ">
            <xsl:text disable-output-escaping="yes">Residential</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'SE' ">
            <xsl:text disable-output-escaping="yes">Seasonal - residential</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'V' ">
            <xsl:text disable-output-escaping="yes">Vacant - unknown type</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'VB' ">
            <xsl:text disable-output-escaping="yes">Vacant - business</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'VM' ">
            <xsl:text disable-output-escaping="yes">Vacant - residential</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'VS' ">
            <xsl:text disable-output-escaping="yes">Vacant - residential</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'VX' ">
            <xsl:text disable-output-escaping="yes">Vacant - mixed use</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'X' ">
            <xsl:text disable-output-escaping="yes">Mixed use</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = '00' ">
            <xsl:text disable-output-escaping="yes">unknown message code - contact Help Desk</xsl:text>
        </xsl:when>
    <xsl:otherwise>
            <xsl:text disable-output-escaping="yes">Not available</xsl:text>
    </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>


  <!--
  *********************************************
  * translate AuthAddressUnitTable
  *********************************************
  -->
  <xsl:template name="AuthAddressUnitTable">
    <xsl:param name="code" select="''" />

    <xsl:choose>                      
    <xsl:when test="normalize-space($code) = 'EU' ">
            <xsl:text disable-output-escaping="yes">Unit number is extra - not expected at this address</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'MU' ">
            <xsl:text disable-output-escaping="yes">Unit number is missing - expected at this address</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'WU' ">
            <xsl:text disable-output-escaping="yes">Unit number wrong - unit number does not match unit number at this address</xsl:text>
        </xsl:when>
    <xsl:otherwise>
            <xsl:text disable-output-escaping="yes">Not available</xsl:text>
    </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>


  <!--
  *********************************************
  * translate AuthPhoneHighRiskTable
  *********************************************
  -->
  <xsl:template name="AuthPhoneHighRiskTable">
    <xsl:param name="code" select="''" />

    <xsl:choose>                      
    <xsl:when test="normalize-space($code) = 'N' ">
            <xsl:text disable-output-escaping="yes">No phone high risk information found</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'NP' ">
            <xsl:text disable-output-escaping="yes">Test not in profile</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'YP' ">
            <xsl:text disable-output-escaping="yes">A high risk business was identified for the address at this phone</xsl:text>
        </xsl:when>
    <xsl:otherwise>
            <xsl:text disable-output-escaping="yes">Not available</xsl:text>
    </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>


  <!--
  *********************************************
  * translate AuthPhoneVerifTable
  *********************************************
  -->
  <xsl:template name="AuthPhoneVerifTable">
    <xsl:param name="code" select="''" />

    <xsl:choose>                      
    <xsl:when test="normalize-space($code) = 'A' ">
            <xsl:text disable-output-escaping="yes">Match to address only - residential phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'AB' ">
            <xsl:text disable-output-escaping="yes">Match to address only - business phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'AM' ">
            <xsl:text disable-output-escaping="yes">Match to address only - mixed use phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'B' ">
            <xsl:text disable-output-escaping="yes">Match to business name and address - residential phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'BB' ">
            <xsl:text disable-output-escaping="yes">Match to business name and address - business phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'BM' ">
            <xsl:text disable-output-escaping="yes">Match to business name and address - mixed use phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'C' ">
            <xsl:text disable-output-escaping="yes">Probable cellular phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'D' ">
            <xsl:text disable-output-escaping="yes">Match to business name - residential phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'DB' ">
            <xsl:text disable-output-escaping="yes">Match to business name - business phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'DM' ">
            <xsl:text disable-output-escaping="yes">Match to business name - mixed use phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'E' ">
            <xsl:text disable-output-escaping="yes">Matching records exceed maximum defined on profile</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'F' ">
            <xsl:text disable-output-escaping="yes">Match to full name only - residential phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'FB' ">
            <xsl:text disable-output-escaping="yes">Match to full name only - business phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'FM' ">
            <xsl:text disable-output-escaping="yes">Match to full name only - mixed use phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'H' ">
            <xsl:text disable-output-escaping="yes">Match to surname and address - residential phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'HB' ">
            <xsl:text disable-output-escaping="yes">Match to surname and address - business phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'HM' ">
            <xsl:text disable-output-escaping="yes">Match to surname and address - mixed use phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'I' ">
            <xsl:text disable-output-escaping="yes">Phone is incorrect length</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'IA' ">
            <xsl:text disable-output-escaping="yes">Invalid area code</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'M' ">
            <xsl:text disable-output-escaping="yes">Phone missing</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'MA' ">
            <xsl:text disable-output-escaping="yes">Match to header data</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'N' ">
            <xsl:text disable-output-escaping="yes">No match to name or address - residential phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'NA' ">
            <xsl:text disable-output-escaping="yes">Data not available</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'NB' ">
            <xsl:text disable-output-escaping="yes">No match to name or address - business phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'NM' ">
            <xsl:text disable-output-escaping="yes">No match to name or address - mixed use phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'NP' ">
            <xsl:text disable-output-escaping="yes">Test not in profile</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'NS' ">
            <xsl:text disable-output-escaping="yes">Standardization database has expired - contact Help Desk</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'P' ">
            <xsl:text disable-output-escaping="yes">Probable pager</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'S' ">
            <xsl:text disable-output-escaping="yes">Match to surname only - residential phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'SB' ">
            <xsl:text disable-output-escaping="yes">Match to surname only - business phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'SM' ">
            <xsl:text disable-output-escaping="yes">Match to surname only - mixed use phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'U' ">
            <xsl:text disable-output-escaping="yes">Phone unverifiable - not in database</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'X' ">
            <xsl:text disable-output-escaping="yes">Prefix - Zip mismatch</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'Y' ">
            <xsl:text disable-output-escaping="yes">Match to full name and address - residential phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'YB' ">
            <xsl:text disable-output-escaping="yes">Match to full name and address - business phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'YM' ">
            <xsl:text disable-output-escaping="yes">Match to full name and address - mixed use phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = '00' ">
            <xsl:text disable-output-escaping="yes">unknown message code - contact Help Desk</xsl:text>
        </xsl:when>
    <xsl:otherwise>
            <xsl:text disable-output-escaping="yes">Not available</xsl:text>
    </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>


  <!--
  *********************************************
  * translate AuthPhoneUnitTable
  *********************************************
  -->
  <xsl:template name="AuthPhoneUnitTable">
    <xsl:param name="code" select="''" />

    <xsl:choose>                      
    <xsl:when test="normalize-space($code) = 'EU' ">
            <xsl:text disable-output-escaping="yes">Unit number is extra - not expected at the address for this phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'MU' ">
            <xsl:text disable-output-escaping="yes">Unit number is missing - expected for the address for this phone</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'WU' ">
            <xsl:text disable-output-escaping="yes">Unit number wrong - unit number does not match unit number for the address at this phone</xsl:text>
        </xsl:when>
    <xsl:otherwise>
            <xsl:text disable-output-escaping="yes">Not available</xsl:text>
    </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>


  <!--
  *********************************************
  * translate AuthOFACValidationTable
  *********************************************
  -->
  <xsl:template name="AuthOFACValidationTable">
    <xsl:param name="code" select="''" />

    <xsl:choose>                      
    <xsl:when test="number($code) = 1 ">
            <xsl:text disable-output-escaping="yes">No Match</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 2 ">
            <xsl:text disable-output-escaping="yes">Match to full name only</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 3 ">
            <xsl:text disable-output-escaping="yes">Match to SSN only</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 4 ">
            <xsl:text disable-output-escaping="yes">Match to name and SSN</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 5 ">
            <xsl:text disable-output-escaping="yes">Match to name and DOB</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 6 ">
            <xsl:text disable-output-escaping="yes">Match to name and YOB</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 7 ">
            <xsl:text disable-output-escaping="yes">Match to SSN and DOB</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 8 ">
            <xsl:text disable-output-escaping="yes">Match to SSN and YOB</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 9 ">
            <xsl:text disable-output-escaping="yes">Match to name, SSN and  DOB</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 10 ">
            <xsl:text disable-output-escaping="yes">Match to name, SSN, and YOB</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 11 ">
            <xsl:text disable-output-escaping="yes">Match to company name only</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 12 ">
            <xsl:text disable-output-escaping="yes">Match to company address only</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 13 ">
            <xsl:text disable-output-escaping="yes">Match to company name and address</xsl:text>
        </xsl:when>
    <xsl:when test="number($code) = 14 ">
            <xsl:text disable-output-escaping="yes">Match to surname and first name</xsl:text>
        </xsl:when>
    <xsl:otherwise>
            <xsl:text disable-output-escaping="yes">Not available</xsl:text>
    </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>


  <!--
  *********************************************
  * translate AuthTaxIDTable
  *********************************************
  -->
  <xsl:template name="AuthTaxIDTable">
    <xsl:param name="code" select="''" />

    <xsl:choose>                      
    <xsl:when test="normalize-space($code) = 'AB' ">
            <xsl:text disable-output-escaping="yes">Match to business address only</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'B' ">
            <xsl:text disable-output-escaping="yes">Match to business name and address</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'DB' ">
            <xsl:text disable-output-escaping="yes">Match to business name only</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'I' ">
            <xsl:text disable-output-escaping="yes">Invalid format</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'M' ">
            <xsl:text disable-output-escaping="yes">Tax ID missing</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'N' ">
            <xsl:text disable-output-escaping="yes">No match to business name or address</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'U' ">
            <xsl:text disable-output-escaping="yes">Unverifiable - not in database</xsl:text>
        </xsl:when>
    <xsl:otherwise>
            <xsl:text disable-output-escaping="yes">Not available</xsl:text>
    </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>


  <!--
  ********************************************************
  * translate RiskCategoryDescriptionCodeTable
  ********************************************************
  -->
  <xsl:template name="RiskCategoryDescriptionCodeTable">
    <xsl:param name="code" select="''" />

    <xsl:choose>                      
    <xsl:when test="normalize-space($code) = 'A' ">
            <xsl:text disable-output-escaping="yes">This company is credit active, and pays its bills no later than 9 days late on average. there are no derogatory legal records on file for this company.</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'B' ">
            <xsl:text disable-output-escaping="yes">This company has previously filed for bankruptcy.</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'C' ">
            <xsl:text disable-output-escaping="yes">This company is credit active.  Based on the current payment performance and/or legal records on file for this company, Experian suggests further investigation if the credit amount requested warrants.</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'S' ">
            <xsl:text disable-output-escaping="yes">Risk based on seriously derogatory payment performance and/or seriously derogatory legal records on file, Experian strongly recommends further investigation prior to making any credit or business decisions. 15% of businesses fall into this higher risk category.</xsl:text>
        </xsl:when>
    <xsl:when test="normalize-space($code) = 'W' ">
            <xsl:text disable-output-escaping="yes">Derogatory payment performance information and/or derogatory legal records exist on file for this company. Experian recommends further investigation prior to making credit or business decisions.</xsl:text>
        </xsl:when>
    <xsl:otherwise>
            <xsl:text disable-output-escaping="yes">Not available</xsl:text>
    </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>
  
   <xsl:template name="ReportCSS">
    <style type="text/css">
        /**
         * Start css for new reports that works with iText
         */
        @media screen
        {
            .report_container .print_only {display: none;}
            .report_container .hidden_on_screen {display:none}

            .report_container div.pageBreak {
                margin: 0;
                padding: 0;
                height: 12px;
            }

        }

        @media print
        {
            .fusion_chart_print, .scoreGraphic {
                page-break-inside : avoid;
                -fs-keep-with-inline : keep;
                -fs-table-paginate: paginate;
            }

            .report_container .fusion_chart {display: none;}
            .report_container .show_on_print {display:block}
            .report_container .hidden_on_print {display:none;}

            .report_container img[src$="spacer.gif"] {
                visibility:hidden;
            }

            .report_container #report_footer_title {
                display: block;
                position: running(footertitle);
                margin-left: 6px;
                font-size: 10px;
            }

            .report_container #report_footer_counter {
                display: block;
                position: running(footercounter);
                padding:0;
                margin-right: -10px;
                font-size: 10px;
            }

            .report_container #page_number:before {
                content: counter(page);
            }

            .report_container #page_count:before {
                content: counter(pages);
            }

            .report_container tr.sbscTrendInfo td .valueBox .trend {
                height: 18px;
                padding-bottom: 8px;
            }

            .report_container tr.sbscTrendInfo td .valueBox .value {
                padding-bottom: 8px;
            }

            /*.report_container table.pageBreak,*/
            .report_container div.pageBreak {
                page-break-before:always;
            }

            /*Below may cause FS out of memory*/
            /*.report_container .pageNobreak {
                page-break-inside : avoid;
                -fs-keep-with-inline : keep;
                -fs-table-paginate: paginate;
            }

            .report_container table.section {
                page-break-inside : avoid;
                -fs-keep-with-inline : keep;
                -fs-table-paginate: paginate;
            }*/
        }

        @page {
            @bottom-left {
                content: element(footertitle);
            }
            @bottom-right {
                content: element(footercounter);
            }

            size: 8.5in 11in;
            margin-right: 0in;
            margin-left: 0.375in;
            margin-top: 0.25in;
            margin-bottom: 0.5in;
            border: none;
            padding: 0;
            tex-align: center;
        }


        @media all {
            body .report_container,.report_container a,.report_container td,.report_container th {
                color: <xsl:value-of select = "$reportTextColor"/>;
                font-size: 11px;
                font-family: arial,verdana,helvetica, sans-serif;
            }

            .report_body sup {
                color           :   inherit;
                #color          :   #595959;
                font-size       :   smaller;
                vertical-align  :   super;
            }

            .report_container div.pageBreak hr {
                display: none;
            }

            .report_container li{
                list-style:none;
            }

            .report_container a[href] {
                text-decoration: underline;
            }

            .report_container .bright {
              color: #0193dc;
            }

            .report_container {
                width: 715px;
            }

            .report_container .alert {
              color: #ff3300;
            }

            .report_container .product_title {
                font-size: 18px;
                font-family: arial, helvetica, sans-serif;
            }

            .report_container .product_title sup {
                font-size: 11px;
            }

            .report_container .report_section_title {
                font-size: 16px;
                font-family: arial, helvetica, sans-serif;
            }

            .report_container .font_size_1_minus {
              font-size: 9px;
              font-family: arial, helvetica, sans-serif;
            }

            .report_container .font_size_1 {
              font-size: 10px;
              font-family: arial, helvetica, sans-serif;
            }

            .report_container .font_size_2 {
                font-size: 12px;
                font-family: arial, helvetica, sans-serif;
            }

            .report_container .font_size_3 {
                font-size: 16px;
                font-family: arial, helvetica, sans-serif;
            }

            .report_container .font_size_4 {
                font-size: 18px;
                font-family: arial, helvetica, sans-serif;
            }

            .report_container .divider {
              border-bottom: 1px solid <xsl:value-of select = "$borderColor"/>;
              height:1px;
            }

            .report_container ul.list {
                padding: 0;
                margin:0;
            }

            .report_container .list li {
                padding: 0 0 0 10px;
                margin: 5px 0 0 0;
                background: transparent url(<xsl:value-of select="$basePath"/>sprites_icons.png) no-repeat -95px -244px;
            }

            .report_container table.section {
                border: 1px solid <xsl:value-of select = "$borderColor"/>;
                align: left;
                valign: top;
                margin-top: 5px;
                border-collapse: separate;
            }

            .report_container div.block {
              display: inline-block;

              /* IE7 bs - enables inline-block for div-elements*/
              *display: inline;
              zoom: 1;
            }

            .report_container table.inner {
              padding:2px;
            }

            .report_container table.section .box_padding {
              padding: 5px;
            }

            .report_container .tableheight {
                height      :       18px;
            }
            .report_container table.subsection {
                margin-top: 0;
            }

            .report_container table tr.even td, .report_container table tr td.even {
                /*background-color  :   <xsl:value-of select = "$zebraStripColor"/>;*/
                border-bottom       :   1px solid <xsl:value-of select = "$rowBorderColor"/>;
            }
            .report_container table tr.odd td, .report_container table tr td.odd {
                /*background-color  :   <xsl:value-of select = "$zebraStripColor"/>;*/
                border-bottom       :   1px solid <xsl:value-of select = "$rowBorderColor"/>;
            }

            .report_container table.section thead th,.report_container table.section thead td, .report_container table.section .subtitle th, .report_container table.section .subtitle td{
                height  :   23px;
                vertical-align  :   middle;
            }
            .report_container table.dataTable tr td, .report_container table.dataTable tr th{
                height      :   20px;
                padding     :   0 5px;
            }
            .report_container table.dataTable tr.secondaryRow td {
                height: 14px;
                padding-bottom: 2px;
            }           
            .report_container table.section thead th{
                /*background-color: <xsl:value-of select="$titleColor"/>;*/
                /*background        :   #fff url(<xsl:value-of select="$basePath"/>titlebar.png) repeat-x 0 0;
                height          :   30px;*/
                background      :   #fff url(<xsl:value-of select="$basePath"/>blue-tile-3.gif) repeat-x 0 0;
                height          :   24px;
                color           :   #FFFFFF;
                font-size       :   12px;
                font-weight     :   bold;
            }

            .report_container table.section thead th a {
                height          :   24px;
                color           :   #FFFFFF;
                font-size       :   12px;
                padding-top     :   4px;
            }

            .report_container table.section thead th.doubleheightTitle {
                background      :   #fff url(<xsl:value-of select="$basePath"/>blue-tile-double-line.gif) repeat-x 0 0;
                height   : 45px;
            }

            .report_container table.section thead th.doubleheightTitle .secondLine {
                padding-top:2px;
                font-weight:normal;
            }

            .report_container table th a {
                font-size       :   12px;
            }

            .report_container table.section tr.subtitle th, .report_container table.section tr.subtitle th a{
                background-color:   <xsl:value-of select = "$subtitleColor"/>;
                background-image:   none;
                font-size       :   11px;
                font-weight     :   bold;
                /*color         :   <xsl:value-of select = "$reportTextColor"/>;*/
                color           :   black
            }

            .report_container table.section tr.subtitle div.doubleSubtitle {
                padding:2px 0;
                text-align:center;
            }

            .report_container td.footnote,
            .report_container th.footnote {
                padding-top: 2px;
                font-size:10px;
            }

            .report_container .firstColumn {
                /*width: 100%;*/
                padding: 0 5px;
            }

            .report_container .paragraphSpacer {
              height: 8px;
            }

            .report_container .subsectionSpacer {
              height: 5px;
            }

            .report_container table.dataTable tr.spacer td {
              height: 14px;
              border-top:1px solid <xsl:value-of select = "$rowBorderColor"/>;
            }

            .report_container div span.label {
                clear       :       both;
                float       :       left;
            }

            .report_container div span.value {
                float       :       right;
            }

            .report_container table.section td {
                border-style        :       none;
                border-width        :       0px;
                vertical-align      :       top;
            }

            .report_container table.dataTable td{
                vertical-align      :       middle;
            }

            .report_container table.dataTable .highlight{
                background: #fff url(<xsl:value-of select="$basePath"/>colorPads.gif) no-repeat -3px -1380px;
            }

            .report_container table.section td.label.forImage {
                vertical-align: middle;
            }

            .report_container table tr.datahead th, .report_container table tr.datahead td{
                background-color:   #FFFFFF;
                color           :   <xsl:value-of select = "$reportTextColor"/>;
                font-weight     :   bold;
                border-bottom   :   solid 1px #7F7F7F;
            }

            .report_container table tr.center th, .report_container table tr.center td{
                text-align: center;
            }

            .report_container table tr.datahead th.firstColumn,
            .report_container table tr.datahead td.firstColumn,
            .report_container table.dataTable tr td.firstColumn {
              padding-left:7px;
            }

            .report_container table tr.datahead th.lastColumn,
            .report_container table tr.datahead td.lastColumn,
            .report_container table.dataTable tr td.lastColumn {
              padding-right:7px;
            }

            .report_container table tr.summaryhead th, .report_container table tr.summaryhead td{
                background-color:   #FFFFFF;
                color           :   <xsl:value-of select = "$reportTextColor"/>;
                border-bottom   :   solid 1px <xsl:value-of select = "$borderColor"/>;
                text-align      :   center;
                font-size       :   11px;
                font-weight: bold;
                padding: 2px 0;
            }

            .report_container table tr.summary th, .report_container table tr.summary td {
                background-color:   #cccccc;
                font-weight     :   bold;
                height          :   20px;
                border-top      :   solid 1px #cccccc;  /*<xsl:value-of select = "$borderColor"/>;*/
                /*border-bottom :   solid 1px <xsl:value-of select = "$borderColor"/>;*/
            }

            .report_container table.stackHeader td div + div {
                padding-left: 10px;;
            }

            .report_container .label,
            .report_container .strong {
                font-weight: bold;
            }
            .report_container .secondLevel {
                padding-left    :   16pt;
            }

            .report_container .rightalign{
                text-align: right;
            }

            .report_container table.section th, .report_container table tr.subtitle td {
                text-align: left;
                padding-left: 5px;
            }

            .report_container table.section tbody {
                background-color: #FFFFFF;
            }

            .report_container table.section thead th .titleLabel {
                font-size: 16px;
            }

            .report_container table tr td.centerLabel,.report_container table tr th.centerLabel, .report_container table tr th .centerLabel,.report_container table tr td .centerLabel {
                text-align  :   center;
                width       :   100%;
            }

            .report_container table.section thead th .smallTitle {
                font-size   :   11px;
                font-weight :   normal;
            }

            .report_container .GrayBox,.report_container .BigPad,.report_container .MiddlePad,.report_container .SmallPad,.report_container .scoreGraphic {
                font-family: arial, verdana, helvetica, sans-serif;
            }

            .report_container .verticalMiddleBox {
                #position   :   relative;
                display     :   table;
                overflow    :   hidden;
                text-align  :   center;
            }
            .report_container .verticalMiddleBox .wrapInner {
                #position       :   absolute;
                #top            :   50%;
                #left           :   50%;
                display         :   table-cell;
                vertical-align  :   middle;
            }
            .report_container .verticalMiddleBox .wrapInner .innerText{
                #position       :   relative;
                #top            :   -50%;
                #left           :   -50%
            }

            .report_container table td.leftborder {
                border-left     :   solid 1px <xsl:value-of select = "$borderColor"/>;
            }

            .report_container table td.bottomborder {
                border-bottom       :   solid 1px <xsl:value-of select = "$borderColor"/>;
            }

            .report_container .grayOuterBox {
                /*background-color  :   #cccccc;*/
                text-align          :   center;
                font-size           :   10px;
                /*font-weight           :   bold;*/
                color               :   <xsl:value-of select = "$reportTextColor"/>;
                padding             :   4px!important;
            }
            .report_container .grayOuterBox .whiteInnerBox {
                background-color    :   #ffffff;
                padding             :   4px;
            }
            .report_container .grayOuterBox .whiteInnerBox .grayInnerBox {
                /*background-color  :   #cccccc;*/
                padding             :   4px;
            }


            .report_container .GrayBox {
                height      :   80px;
                color       :   #363636;
                margin      :   10px;
                font-weight :   bold;
                font-size   :   8pt;
                background-color:   #cccccc;
            }
            .report_container img.verticalAlign {
                height      :   100%;
                width       :   1px;
                vertical-align  :   middle;
            }

            .report_container .BigPad {
                width: 111px;
                height: 111px;
                position: relative;
                margin: 10px;
            }

            .report_container .ActiveBusniessIndicator {
                background  : #fff url(<xsl:value-of select="$basePath"/>colorPads.gif) no-repeat -87px -73px;
                height      : 34px;
                width       : 34px;
                margin: 5px;
            }

            .report_container .InActiveBusniessIndicator {
                background: #fff url(<xsl:value-of select="$basePath"/>colorPads.gif) no-repeat -87px -32px;
                height      : 34px;
                width       : 34px;
                margin: 5px;
            }

            .report_container .BigPadGreen {
                background: #fff url(<xsl:value-of select="$basePath"/>colorPads.gif) no-repeat 0 -752px;
            }

            .report_container .BigPadYellow {
                background: #fff url(<xsl:value-of select="$basePath"/>colorPads.gif) no-repeat 0 -632px;
            }

            .report_container .BigPadRed {
                background: #fff url(<xsl:value-of select="$basePath"/>colorPads.gif) no-repeat 0 -992px;
            }

            .report_container .BigPad .title {
                text-align: center;
                left:       0px;
                color:      white;
                font-size:  10pt;
                top:        0px;
                font-weight:bold;
                width:      105px;
                #position:  relative;
                display:    table;
                height:     30px;
                overflow:   hidden;
                padding :   0 5px 0 0;
            }

            .report_container .BigPad .value {
                text-align: center;
                color: white;
                font-size: 40pt;
                top: 23px;
                left    :   0px;
                position: absolute;
                width   :   111px;
                font-weight : bold;
            }

            .report_container .BigPad .bottom {
                text-align: center;
                left: 0px;
                color: white;
                font-size: 10pt;
                top: 85px;
                position: absolute;
                font-weight: bold;
                width: 111px;
            }

            .report_container .MiddlePad {
                width: 56px;    /* For working with IE6 */
                height: 56px;
                margin: 5px auto;
                padding: 0 auto;
                line-height:56px;
            }

            .report_container .scoreLowRisk {
                background: #fff url(<xsl:value-of select="$basePath"/>colorPads.gif) no-repeat 0 -1718px;
            }
            .report_container .scoreLowMedRisk {
                background: #fff url(<xsl:value-of select="$basePath"/>colorPads.gif) no-repeat 0 -1787px;
            }
            .report_container .scoreMedRisk {
                background: #fff url(<xsl:value-of select="$basePath"/>colorPads.gif) no-repeat 0 -1858px;
            }
            .report_container .scoreMedHighRisk {
                background: #fff url(<xsl:value-of select="$basePath"/>colorPads.gif) no-repeat 0 -1932px;
            }
            .report_container .scoreHighRisk {
                background: #fff url(<xsl:value-of select="$basePath"/>colorPads.gif) no-repeat 0 -2009px;
            }

            .report_container .MiddlePadGreen {
                background: #fff url(<xsl:value-of select="$basePath"/>colorPads.gif) no-repeat 0 -1718px;
                /*background: #fff url(<xsl:value-of select="$basePath"/>colorPads.gif) no-repeat 0 -177px;*/
            }

            .report_container .MiddlePadYellow {
                background: #fff url(<xsl:value-of select="$basePath"/>colorPads.gif) no-repeat 0 -1858px;
                /*background: #fff url(<xsl:value-of select="$basePath"/>colorPads.gif) no-repeat 0 -113px;*/
            }

            .report_container .MiddlePadRed {
                background: #fff url(<xsl:value-of select="$basePath"/>colorPads.gif) no-repeat 0 -2009px;
                /*background: #fff url(<xsl:value-of select="$basePath"/>colorPads.gif) no-repeat 0 -307px;*/
            }

            .report_container .MiddlePad .title {
                text-align: center;
                left: 0px;
                color: white;
                font-size: 8pt;
                top: 5px;
                height: 10px;
                position: absolute;
                font-weight: bold;
                width: 55px;
            }

            .report_container .MiddlePad .value {
                color: white;
                font-size: 26px;
            }
            .report_container .MiddlePad.scoreMedHighRisk .value,.report_container .MiddlePad.scoreLowMedRisk .value,.report_container .MiddlePad.scoreMedRisk .value,.report_container .MiddlePad.MiddlePadYellow .value,.report_container .MiddlePad.scoreUnkownRisk .value {
                color   :   <xsl:value-of select = "$reportTextColor"/>;
            }

            .report_container .MiddlePad .bottom {
                text-align: center;
                left: 0px;
                color: white;
                font-size: 12pt;
                top: 85px;
                position: absolute;
                font-weight: bold;
                width: 111px;
            }

            .report_container .SmallPad {
                width: 34px;
                height: 34px;
                position: relative;
                margin: 5px;
            }

            .report_container .SmallPadGreen {
                background: #fff url(<xsl:value-of select="$basePath"/>colorPads.gif) no-repeat -87px -175px;
            }

            .report_container .SmallPadYellow {
                background: #fff url(<xsl:value-of select="$basePath"/>colorPads.gif) no-repeat -87px -111px;
            }

            .report_container .SmallPadRed {
                background: #fff url(<xsl:value-of select="$basePath"/>colorPads.gif) no-repeat -87px -306px;
            }

            .report_container .SmallPad .title {
                text-align: center;
                left: 0px;
                color: white;
                font-size: 8pt;
                top: 2px;
                height: 16px;
                position: absolute;
                width: 32px;
            }

            .report_container .SmallPad .value {
                text-align: center;
                color: white;
                font-size: 10pt;
                top: 7px;
                left: 0px;
                position: absolute;
                width: 32px;
                font-weight: bold;
            }

            .report_container .SmallPad .valueWithTitle {
                text-align: center;
                color: white;
                font-size: 12pt;
                top: 12px;
                left: 0px;
                position: absolute;
                width: 32px;
                font-weight: bold;
            }

            .report_container .scoreGraphic {
                width: 380px;
                height: 90px;
                position: relative;
                margin: 5px 30px 0px 10px;
                /*background: #fff url(<xsl:value-of select="$basePath"/>barMeter.png) no-repeat 0 0;*/
            }
            .report_container .meter210 {
                background: #fff url(<xsl:value-of select="$basePath"/>commercial-intelliscore-210.png) no-repeat 0 0;
            }
            .report_container .meter214 {
                background: #fff url(<xsl:value-of select="$basePath"/>intelliscore-plus-bar.png) no-repeat 0 0;
            }
            .report_container .meter223 {
                background: #fff url(<xsl:value-of select="$basePath"/>fsr-bar.png) no-repeat 0 0;
            }
            .report_container .dbtMeter {
                background: #fff url(<xsl:value-of select="$basePath"/>dbt-ranges.png) no-repeat 0 0;
            }

            .report_container .graphicTitle,
            .report_container .scoreGraphic .title {
                font-size       :   13px;
                font-family     :   verdana, arial, helvetica, sans-serif;
                position        :   relative;
            }

            .report_container .scoreGraphic .scoreMeter {
                position: absolute;
                left: 50px;
                top: 50px;
                width: 300px;
                height: 25px;
                /*background: #fff url(<xsl:value-of select="$basePath"/>color_meter_round.gif) no-repeat 0 0;*/
            }

            .report_container .scoreGraphic .LeftText {
                color: <xsl:value-of select = "$reportTextColor"/>;
                font-size: 7pt;
                text-align: right;
                position: absolute;
                top: 55px;
            }

            .report_container .scoreGraphic .RightText {
                color: <xsl:value-of select = "$reportTextColor"/>;
                font-size: 7pt;
                text-align: left;
                position: absolute;
                top: 55px;
                left: 355px;
            }

            .report_container .scoreGraphic .MinValue {
                position: absolute;
                top: 55px;
                left: 50px;
                color: <xsl:value-of select = "$reportTextColor"/>;
                font-size: 7pt;
            }

            .report_container .scoreGraphic .MaxValue {
                position: absolute;
                top: 55px;
                left: 330px;
                color: <xsl:value-of select = "$reportTextColor"/>;
                font-size: 7pt;
            }

            .report_container .scoreGraphic .BottomText {
                color: <xsl:value-of select = "$reportTextColor"/>;
                font-size: 7pt;
                text-align: center;
                position: absolute;
                width: 350px;
                top: 75px;
            }

            .report_container .scoreGraphic .scoreValueArrow {
                position: absolute;
                top: 20px;
                font-size: 7pt;
                width: 13px;
                height: 13px;
                background: transparent url(<xsl:value-of select="$basePath"/>barMeter.png) no-repeat -390px 0;
            }

            .report_container .scoreGraphic .scoreValue {
                position        :   absolute;
                top             :   3px;
                padding         :   5px 0 0 0;
                color           :   <xsl:value-of select = "$reportTextColor"/>;
                font-size       :   10px;
                width           :   34px;
                height          :   34px;
                font-weight     :   bold;
                text-align      :   center;
                /*background    :   #fff url(<xsl:value-of select="$basePath"/>colorPads.gif) no-repeat -86px -502px;*/
            }

            .report_container table.section td <xsl:text disable-output-escaping="yes">&gt;</xsl:text> div.GrayBox {
                padding     :   0;
            }

            .report_container table tr.section<xsl:text disable-output-escaping="yes">&gt;</xsl:text>td {
                border-top: 1px solid <xsl:value-of select = "$borderColor"/>;
            }


            .report_container table.section td <xsl:text disable-output-escaping="yes">&gt;</xsl:text> div {
                /*padding: 0 5px;*/
            }

            .report_container .LowScoreText {
                color   :   #209a5c;
            }
            .report_container .LowMedScoreText {
                color   :   #8aca64;
            }
            .report_container .MedScoreText {
                color   :   #f4e35b;
            }
            .report_container .MedHighScoreText {
                color   :   #ee7240;
            }
            .report_container .HighScoreText {
                color   :   #d6373e;
            }

            .report_container .indent1 {
                padding-left    :       16pt;
            }
            .report_container .indent2 {
                padding-left    :       32pt;
            }
            .report_container .indent3 {
                padding-left    :       48pt;
            }
            .report_container .indent4 {
                padding-left    :       64pt;
            }

            .report_container .DBTChart {
                border          :       solid <xsl:value-of select = "$borderColor"/> 2px;
                width           :       350px;
                text-align      :       center;
                padding         :       20px 5px;
                height          :       100px;
                margin          :       20px;
                overflow        :       hidden;
            }

            .report_container .DBTChart .valueArrow {
                background      :       #fff url(<xsl:value-of select="$basePath"/>triangle_blue.gif) no-repeat 0 0;
                width           :       5px;
                height          :       14px;
            }

            .report_container .DBTChart .DBTmeter {
                height          :       20px;
                width           :       300px;
                test-align      :       center;
                padding-top     :       3px;
                margin          :       0 auto
            }
            .report_container .DBTChart .DBTmeterValue {
                height          :       20px;
                width           :       300px;
                test-align      :       center;
                clear           :       both;
                font-weight     :       bold;
                margin          :       0 auto
            }
            .report_container .DBTmeter div, .report_container .DBTmeterValue div {
                float           :       left;
            }

            .report_container .DBTChart .Green {
                width           :       240px;
            }
            .report_container .DBTChart .Yellow {
                width           :       33px;
            }
            .report_container .DBTChart .Red {
                width           :       27px;
            }
            .report_container .DBTmeter .Green {
                height          :       100%;
                background      :       #00aa00;

                filter          :       progid:DXImageTransform.Microsoft.gradient(GradientType="1", startColorstr='#00aa00', endColorstr='#ffff00');   /* IE */
                background      :       -webkit-gradient(linear, left top, right top, from(#00aa00), to(#ffff00));
                background      :       -moz-linear-gradient(left,  #00aa00,  #ffff00);
                border-style    :       none;
            }
            .report_container .DBTmeter .Yellow {
                height          :       100%;
                background      :       #ffff00;

                filter          :       progid:DXImageTransform.Microsoft.gradient(GradientType="1", startColorstr='#ffff00', endColorstr='#ff0000');   /* IE */
                background      :       -webkit-gradient(linear, left top, right top, from(#ffff00), to(#ff0000));
                background      :       -moz-linear-gradient(left,  #ffff00,  #ff0000);
                border-style    :       none;
            }
            .report_container .DBTmeter .Red {
                height          :       100%;
                background      :       #ff0000;
                border-style    :       none;
            }

            .report_container .verifiedLegalName {
                background      :       transparent url(<xsl:value-of select="$basePath"/>check.jpg) no-repeat 0 0px;
                height          :       56px;
                width           :       75px;
            }

            .report_container .adminDemo {
                display: none;
            }

            .report_container .riskHigh {
                background-color: #CD3239;
                color: #ffffff;
            }

            .report_container .riskMediumHigh {
                background-color: #DE6237;
                color: #ffffff;
            }

            .report_container .riskMedium {
                background-color: #EFD957;
            }

            .report_container .riskMediumLow {
                background-color: #84B95B;
                color: #ffffff;
            }

            .report_container .riskLow {
                background-color: #20985B;
                color: #ffffff;
            }

            .report_container .legacyScoreBar {
                text-align: center;
                margin: auto;
                border: 2px solid #78826C;
                vertical-align: middle;
                font-weight: bold;
                line-height: 22px;
                height: 22px;
                width: 325px;
            }

            .report_container .legacyScoreBar div.block {
                float: left;
                width: 65px;
            }

            .report_container .legacyDBTBar {
                width: 300px;
                text-align: center;
                margin: auto;
                border: 2px solid #78826C;
                font-weight: bold;
                line-height: 22px;
                height: 22px;
            }

            .report_container .legacyDBTBar div.block {
                float: left;
            }

            .report_container .legacyDBTBar .riskLow {
                width: 240px;
                background-color: #20985B;
                color: #ffffff;
            }

            .report_container .legacyDBTBar .riskMedium {
                width: 33px;
                background-color: #EFD957;
            }

            .report_container .legacyDBTBar .riskHigh {
                width: 27px;
                background-color: #CD3239;
                color: #ffffff;
            }

            /* SBCS specific CSS for trend chart info */
            .report_container tr.sbscTrendInfo td {
                padding-top: 7px;
                width: 200px\9;
                *width: 313px;
            }

            .report_container tr.sbscTrendInfo td + td {
                vertical-align: middle;
                padding-left: 40px;
            }

            .report_container tr.sbscTrendInfo div.block {
                vertical-align: middle;
            }

            .report_container tr.sbscTrendInfo td .label {
                font-size: 12px;
                line-height: 34px;
                font-weight: bold;
                width: auto;
            }

            .report_container tr.sbscTrendInfo td .valueBox {
                font-size: 18px;
                width: 76px;
                border: 1px solid <xsl:value-of select = "$borderColor"/>;
                line-height: 30px;
                height: 32px;
                font-weight: bold;
                text-align: center;
            }

            .report_container tr.sbscTrendInfo td .valueBox .value {
                width: auto;
                height: 32px;
            }

            .report_container tr.sbscTrendInfo td .valueBox .trend {
                overflow: visible;
                width: 15px;
                height: 30px;
                *height: 18px;
            }

            .report_container div.chart {
                height: auto;
            }

            #UCCFilingSectionGTTen table tr td, #UCCFilingSectionFirstTen table tr td {
                padding: 5px;
                vertical-align: top;
            }
        }
    </style>

  </xsl:template>
  
    <!--
  *********************************************
  * HTMLDOC page break template
  * This is a HTMLDOC specific command to make PDF have page break
  *********************************************
  -->
  <xsl:template name="HtmldocPageBreak">
    <div class="pageBreak"><hr break="" /></div>
  </xsl:template>

  <!--
  *********************************************
  * GetTargetIntelliscoreNodeIndex template
  * Return position of <IntelliscoreScoreInformation> that has main model and whose 
  * attributes will be used for populating portfolio
  * Loop through main models to get attributes from the targeted <IntelliscoreScoreInformation>
  * If there is one with same desired model type, use that one as targeted node to get attributes.
  *********************************************
  -->
	<!--
  *********************************************
  * GetTargetIntelliscoreNodeIndex template
  * Return position of <IntelliscoreScoreInformation> that has main model and whose 
  * attributes will be used for populating portfolio
  * Loop through main models to get attributes from the targeted <IntelliscoreScoreInformation>
  * If there is one with same desired model type, use that one as targeted node to get attributes.
  *********************************************
  -->
  <xsl:template name="GetTargetIntelliscoreNodeIndex">
    <xsl:param name="intelliscoreNodeList"/>
    <xsl:param name="useBlended"/>

	<xsl:variable name="tempTargetIndexPipe">
		<xsl:for-each select="$intelliscoreNodeList">
			<xsl:variable name="hasBlended">
				<xsl:call-template name="IsScoreBlended">
					<xsl:with-param name="intelliscoreNode" select="." />
				</xsl:call-template>
			</xsl:variable>
			<xsl:if test="$hasBlended = $useBlended">
				<xsl:value-of select="concat(position(),'|')" />
			</xsl:if>
		</xsl:for-each>
	</xsl:variable>

	<xsl:variable name="tempTargetIndex" select="substring-before($tempTargetIndexPipe, '|')">
	</xsl:variable>

	<xsl:choose>
		<xsl:when test="string(number($tempTargetIndex)) != 'NaN'">
			<xsl:value-of select="$tempTargetIndex" />
		</xsl:when>			
		<xsl:otherwise>
			<xsl:value-of select="1" />
		</xsl:otherwise>
	</xsl:choose>
  </xsl:template>
  
  <!--
  *********************************************
  * IsScoreBlended template
  * Return 1 if the model score from a given <IntelliscoreScoreInformation> is blended
  *********************************************
  -->
  <xsl:template name="IsScoreBlended">
    <xsl:param name="intelliscoreNode"/>

    <xsl:variable name="tmpSegmentUsed">
      <xsl:call-template name="GetSegmentUsed">
    	<xsl:with-param name="intelliscoreNode" select="$intelliscoreNode" />
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="number($tmpSegmentUsed) != 0 and (number($tmpSegmentUsed) = 6 or number($tmpSegmentUsed) = 7)">
        <xsl:value-of select="1" />
      </xsl:when>
      <xsl:when test="number($tmpSegmentUsed) = 99">
        <xsl:value-of select="99" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="0" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>


  <!--
  *********************************************
  * GetSegmentUsed template
  * Return value of Segment Used based on filler field of a given IP <IntelliscoreScoreInformation>. 
  *********************************************
  -->	
  
  <xsl:template name="GetSegmentUsed">
    <xsl:param name="intelliscoreNode"/>
    <xsl:variable name="filler">
      <!-- !!USE MAIN MODEL ONLY!!  -->
      <xsl:choose>
        <xsl:when test="$intelliscoreNode/prd:Filler">
          <xsl:value-of select="$intelliscoreNode/prd:Filler" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$modelCode = $ipV2Model or $modelCode = $ipV2ScoreOnlyModel or $modelCode = $sbcsV2Model">
        <xsl:choose>
          <xsl:when test="number($filler) = 0">
            <xsl:value-of select="99" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="number($filler)" />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
    
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="starts-with(normalize-space($filler), 'L')">
            <xsl:value-of select="1" />
          </xsl:when>
          <xsl:when test="starts-with(normalize-space($filler), 'M')">
            <xsl:value-of select="2" />
          </xsl:when>
          <xsl:when test="starts-with(normalize-space($filler), 'S')">
            <xsl:value-of select="3" />
          </xsl:when>
          <xsl:when test="starts-with(normalize-space($filler), 'X')">
            <xsl:value-of select="4" />
          </xsl:when>
          <xsl:when test="starts-with(normalize-space($filler), 'D')">
            <xsl:value-of select="5" />
          </xsl:when>
          <xsl:when test="starts-with(normalize-space($filler), 'B') and normalize-space($filler) != 'BK'">
            <xsl:value-of select="6" />
          </xsl:when>
          <xsl:when test="starts-with(normalize-space($filler), 'P')">
            <xsl:value-of select="7" />
          </xsl:when>
          <xsl:when test="normalize-space($filler) = 'BK'">
            <xsl:value-of select="98" />
          </xsl:when>
          <xsl:when test="starts-with(normalize-space($filler), 'N') or normalize-space($filler) = '00' or normalize-space($filler) = '0'">
            <xsl:value-of select="99" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="0" />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
	
  </xsl:template>

  

</xsl:stylesheet>