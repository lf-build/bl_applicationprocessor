function loadXMLString(txt) 
{
	var xmlDoc;
	//alert("txt="+txt);
	if (window.DOMParser)
   {
   	//alert("window.DOMParser");
   	parser=new DOMParser();
   	xmlDoc=parser.parseFromString(txt.replace(/&.*;/g,''),"text/xml");
   }
	else // code for IE
   {
   	//alert("ActiveXObject");
   	xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
   	xmlDoc.async=false;
   	xmlDoc.loadXML(txt); 
   }
	return xmlDoc;
}