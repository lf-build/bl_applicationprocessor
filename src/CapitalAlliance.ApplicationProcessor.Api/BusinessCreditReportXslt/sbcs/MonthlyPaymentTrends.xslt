
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rsp="http://www.experian.com/NetConnectResponse"
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * MonthlyPaymentTrends template
  *********************************************
  -->
  <xsl:template name="MonthlyPaymentTrends">
    <xsl:param name="color" select="'#0099cc'" />

    <!-- Section title -->
    <xsl:call-template name="SectionTitle">
      <xsl:with-param name="title" select="'Monthly Payment Trends'" />
      <xsl:with-param name="color" select="$titleColor" />
    </xsl:call-template>

    <xsl:variable name="sic">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSBusinessNameAndAddress/prd:SIC[prd:SICIndicator = 'P']">                    
          <xsl:call-template name="convertcase">
            <xsl:with-param name="toconvert" select="concat(translate(prd:SBCSBusinessNameAndAddress/prd:SIC[prd:SICIndicator = 'P']/prd:SICCode, 'amp;amp;', 'amp;'), ' INDUSTRY SIC: ', substring(prd:SBCSBusinessNameAndAddress/prd:SIC[prd:SICIndicator = 'P']/prd:SICCode/@code, 1, 6))" />
            <xsl:with-param name="conversion" select="'upper'" />
          </xsl:call-template>
        </xsl:when>

        <xsl:when test="prd:SBCSBusinessNameAndAddress/prd:SIC[prd:SICIndicator = 'A']">                    
          <xsl:call-template name="convertcase">
            <xsl:with-param name="toconvert" select="concat(translate(prd:SBCSBusinessNameAndAddress/prd:SIC[prd:SICIndicator = 'A']/prd:SICCode, 'amp;amp;', 'amp;'), ' INDUSTRY SIC: ', substring(prd:SBCSBusinessNameAndAddress/prd:SIC[prd:SICIndicator = 'A']/prd:SICCode/@code, 1, 6))" />
            <xsl:with-param name="conversion" select="'upper'" />
          </xsl:call-template>
        </xsl:when>
      
        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="{$color}">

          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#ffffff">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="32" bgcolor="{$color}" colspan="5" align="center" valign="middle">
                      <b><font color="#ffffff">Payment Trends Analysis

                      <xsl:if test="normalize-space($sic) != ''">
                        <br />
                        
                        <xsl:value-of select="$sic" />
                      </xsl:if> 

                      </font></b></td>

                    <td height="32" bgcolor="{$color}" colspan="6" align="center" valign="middle">
                      <b><font color="#ffffff">Account Status<br />
                      Days Beyond Terms</font></b></td>
                  </tr>

                  <!-- Column Headers -->
                  <tr bgcolor="#ffffff">
                    <td align="center" width="12%" rowspan="1"><font size="1" style="FONT-FAMILY: 'verdana';"><b>Date Reported</b></font></td>
                    <td align="center" width="16%" colspan="2"><font size="1" style="FONT-FAMILY: 'verdana';"><b>Industry</b></font><br />

                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="50%" align="center"><font size="1" style="FONT-FAMILY: 'verdana';"><b>Cur</b></font></td>
                          <td width="50%" align="center"><font size="1" style="FONT-FAMILY: 'verdana';"><b>DBT</b></font></td>
                        </tr>
                      </table>

                    </td>
                    <td align="center" width="8%" rowspan="1"><font size="1" style="FONT-FAMILY: 'verdana';"><b>Business<br />DBT</b></font></td>
                    <td align="center" width="16%" rowspan="1"><font size="1" style="FONT-FAMILY: 'verdana';"><b>Balance</b></font></td>
                    <td align="center" width="8%" rowspan="1"><font size="1" style="FONT-FAMILY: 'verdana';"><b>Cur</b></font></td>
                    <td align="center" width="8%" rowspan="1"><font size="1" style="FONT-FAMILY: 'verdana';"><b>1-30</b></font></td>
                    <td align="center" width="8%" rowspan="1"><font size="1" style="FONT-FAMILY: 'verdana';"><b>31-60</b></font></td>
                    <td align="center" width="8%" rowspan="1"><font size="1" style="FONT-FAMILY: 'verdana';"><b>61-90</b></font></td>
                    <td align="center" width="8%" rowspan="1"><font size="1" style="FONT-FAMILY: 'verdana';"><b>91-120</b></font></td>
                    <td align="center" width="8%" rowspan="1"><font size="1" style="FONT-FAMILY: 'verdana';"><b>121+</b></font></td>
                  </tr>
                  

                  <xsl:apply-templates select="prd:SBCSPaymentTrends/prd:CurrentMonth" mode="mpt" />
                  <xsl:apply-templates select="prd:SBCSPaymentTrends/prd:PriorMonth" mode="mpt" />
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    
  </xsl:template>
  

  <!--
  *********************************************
  * CurrentMonth | PriorMonth template
  *********************************************
  -->
  <xsl:template match="prd:CurrentMonth | prd:PriorMonth" mode="mpt" >
    <xsl:variable name="position">
      <xsl:value-of select="position()" />
    </xsl:variable>

    <xsl:variable name="tmpMonth">
	<xsl:value-of select="number(substring(prd:Date, 5, 2))" />
    </xsl:variable>
	
    <xsl:variable name="tmpYear">
	<xsl:choose>		              
	  <xsl:when test="number($tmpMonth) = 0">		    		   		   
	    <xsl:value-of select="number(substring(prd:Date, 1, 4)) - 1" />
	  </xsl:when>
	
	  <xsl:otherwise>
	    <xsl:value-of select="number(substring(prd:Date, 1, 4))" />
	  </xsl:otherwise>
	</xsl:choose>    
    </xsl:variable>

    <xsl:variable name="newMonth">
	<xsl:choose>		              
	  <xsl:when test="number($tmpMonth) = 0">		    		   		   
	    <xsl:value-of select="12" />
	  </xsl:when>
	
	  <xsl:otherwise>
	    <xsl:value-of select="$tmpMonth" />
	  </xsl:otherwise>
	</xsl:choose>    
    </xsl:variable>

    <xsl:variable name="dateReported">
      <xsl:choose>		              
        <!-- 
        <xsl:when test="name() = 'CurrentMonth'">		    		   		   
          <xsl:value-of select="'CURRENT'" />
        </xsl:when>
        -->
          
        <xsl:when test="prd:Date">
          <xsl:variable name="month">
            <xsl:call-template name="FormatMonth">
      		    <xsl:with-param name="monthValue" select="number($newMonth)" />
      		    <xsl:with-param name="upperCase" select="true()" />
      		  </xsl:call-template>
          </xsl:variable>		    		   		   

          <xsl:value-of select="concat(normalize-space($month), substring(normalize-space($tmpYear), 3, 2))" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="industryCurrent">
      <xsl:choose>		              
        <xsl:when test="name() = 'CurrentMonth'">
          <xsl:value-of select="'N/A'" />
        </xsl:when>

        <xsl:when test="$position - 1 = 0 and ../../prd:IndustryPaymentTrends/prd:CurrentMonth/prd:CurrentPercentage">
          <xsl:value-of select="format-number(../../prd:IndustryPaymentTrends/prd:CurrentMonth/prd:CurrentPercentage div 100, '##0%')" />
        </xsl:when>

        <xsl:when test="$position - 1 &gt; 0 and ../../prd:IndustryPaymentTrends/prd:PriorMonth[number($position - 1)]/prd:CurrentPercentage">
          <xsl:value-of select="format-number(../../prd:IndustryPaymentTrends/prd:PriorMonth[number($position - 1)]/prd:CurrentPercentage div 100, '##0%')" />
        </xsl:when>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="industryDBT">
      <xsl:choose>                  
        <xsl:when test="name() = 'CurrentMonth'">
          <xsl:value-of select="'N/A'" />
        </xsl:when>

        <xsl:when test="$position - 1 = 0 and ../../prd:IndustryPaymentTrends/prd:CurrentMonth/prd:DBT">
          <xsl:value-of select="number(../../prd:IndustryPaymentTrends/prd:CurrentMonth/prd:DBT)" />
        </xsl:when>

        <xsl:when test="$position - 1 &gt; 0 and ../../prd:IndustryPaymentTrends/prd:PriorMonth[number($position - 1)]/prd:DBT">
          <xsl:value-of select="number(../../prd:IndustryPaymentTrends/prd:PriorMonth[number($position - 1)]/prd:DBT)" />
        </xsl:when>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="businessDBT">
      <xsl:choose>		              
        <xsl:when test="prd:DBT">
          <xsl:value-of select="number(prd:DBT)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="businessCurrent">
      <xsl:choose>		              
        <xsl:when test="prd:CurrentPercentage and number(prd:CurrentPercentage) != 0">
          <xsl:value-of select="format-number(prd:CurrentPercentage div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="balance">
      <xsl:choose>		              
        <xsl:when test="prd:TotalAccountBalance">
          <xsl:value-of select="concat(prd:TotalAccountBalance/prd:Modifier/@code, format-number(prd:TotalAccountBalance/prd:Amount, '$###,###,##0'))" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="DBT30">
      <xsl:choose>		              
        <xsl:when test="prd:DBT30 and number(prd:DBT30) != 0">
          <xsl:value-of select="format-number(prd:DBT30 div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="DBT60">
      <xsl:choose>		              
        <xsl:when test="prd:DBT60 and number(prd:DBT60) != 0">
          <xsl:value-of select="format-number(prd:DBT60 div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="DBT90">
      <xsl:choose>
        <xsl:when test="prd:DBT90 and number(prd:DBT90) != 0">
          <xsl:value-of select="format-number(prd:DBT90 div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="DBT120">
      <xsl:choose>
        <xsl:when test="prd:DBT120 and number(prd:DBT120) != 0">
          <xsl:value-of select="format-number(prd:DBT120 div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="DBT121Plus">
      <xsl:choose>
        <xsl:when test="prd:DBT121Plus and number(prd:DBT121Plus) != 0">
          <xsl:value-of select="format-number(prd:DBT121Plus div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="bgColor">
      <xsl:choose>		              
        <xsl:when test="name() = 'CurrentMonth'">
          <xsl:value-of select="'#e5f5fa'" />
        </xsl:when>

        <xsl:when test="position() mod 2 = 0">		    		   		   
          <xsl:value-of select="'#e5f5fa'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'#ffffff'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <tr>
      <td height="20" bgcolor="{normalize-space($bgColor)}" align="center"><font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$dateReported" /></font>
      </td>

      <td height="20" width="8%" bgcolor="{normalize-space($bgColor)}" align="center"><font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$industryCurrent" /></font>
      </td>

      <td height="20" width="8%" bgcolor="{normalize-space($bgColor)}" align="center"><font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$industryDBT" /></font>
      </td>

      <td height="20" bgcolor="{normalize-space($bgColor)}" align="center"><font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$businessDBT" /></font>
      </td>

      <td height="20" bgcolor="{normalize-space($bgColor)}">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="70%" align="right"><font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$balance" /></font>
            </td>
            <td width="30%">
            </td>
          </tr>
        </table>
      </td>

      <td height="20" bgcolor="{normalize-space($bgColor)}" align="center"><font size="1" style="FONT-FAMILY: 'verdana';">
        <xsl:value-of select="$businessCurrent" /></font>
      </td>

      <td height="20" bgcolor="{normalize-space($bgColor)}" align="center"><font size="1" style="FONT-FAMILY: 'verdana';">
        <xsl:value-of select="$DBT30" /></font>
      </td>

      <td height="20" bgcolor="{normalize-space($bgColor)}" align="center"><font size="1" style="FONT-FAMILY: 'verdana';">
        <xsl:value-of select="$DBT60" /></font>
      </td>

      <td height="20" bgcolor="{normalize-space($bgColor)}" align="center"><font size="1" style="FONT-FAMILY: 'verdana';">
        <xsl:value-of select="$DBT90" /></font>
      </td>

      <td height="20" bgcolor="{normalize-space($bgColor)}" align="center"><font size="1" style="FONT-FAMILY: 'verdana';">
        <xsl:value-of select="$DBT120" /></font>
      </td>

      <td height="20" bgcolor="{normalize-space($bgColor)}" align="center"><font size="1" style="FONT-FAMILY: 'verdana';">
        <xsl:value-of select="$DBT121Plus" /></font>
      </td>
    </tr>

  </xsl:template>

</xsl:stylesheet>