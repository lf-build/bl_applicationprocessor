
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rsp="http://www.experian.com/NetConnectResponse"
                xmlns:prd="http://www.experian.com/ARFResponse">

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />



  <!--
  *********************************************
  * CurrentDBT template
  *********************************************
  -->
  <xsl:template name="CurrentDBT">
    <xsl:param name="color" select="'#0099cc'" />
  
    <xsl:variable name="currentDBT">
      <xsl:choose>                  
        <xsl:when test="normalize-space(prd:SBCSExecutiveElements/prd:CurrentDBT) != ''">                    
          <xsl:value-of select="number(prd:SBCSExecutiveElements/prd:CurrentDBT)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="dbtPointer">
      <xsl:choose>                  
        <xsl:when test="$currentDBT &gt;= 0 and $currentDBT &lt;= 15">                     
          <xsl:value-of select="1" />
        </xsl:when>

        <xsl:when test="$currentDBT &gt;= 16 and $currentDBT &lt;= 50">                    
          <xsl:value-of select="2" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="3" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="accountBalance">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveElements/prd:CurrentAccountBalance">                    
          <xsl:value-of select="format-number(prd:SBCSExecutiveElements/prd:CurrentAccountBalance, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="allIndustryDBT">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveSummary/prd:AllIndustryDBT">                    
          <xsl:value-of select="concat(number(prd:SBCSExecutiveSummary/prd:AllIndustryDBT), ' DBT')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="industryDBT">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveSummary/prd:IndustryDBT">                     
          <xsl:value-of select="concat(number(prd:SBCSExecutiveSummary/prd:IndustryDBT), ' DBT')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="imgHeight">
      <xsl:choose>                  
        <xsl:when test="normalize-space($currentDBT) != ''">                     
          <xsl:value-of select="'5'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'8'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="trHeight">
      <xsl:choose>                  
        <xsl:when test="normalize-space($currentDBT) != ''">                     
          <xsl:value-of select="'18'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'20'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="monthlyAvgDBT">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveElements/prd:MonthlyAverageDBT and normalize-space($currentDBT) != ''">                    
          <xsl:value-of select="number(prd:SBCSExecutiveElements/prd:MonthlyAverageDBT)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="highestDBT6M">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveElements/prd:HighestDBT6Months and normalize-space($currentDBT) != ''">                    
          <xsl:value-of select="number(prd:SBCSExecutiveElements/prd:HighestDBT6Months)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="highestDBT5Q">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveElements/prd:HighestDBT5Quarters and normalize-space($currentDBT) != ''">                    
          <xsl:value-of select="number(prd:SBCSExecutiveElements/prd:HighestDBT5Quarters)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="highestDBTEver">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:HighestDBTEver and normalize-space($currentDBT) != ''">                    
          <xsl:value-of select="number(prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:HighestDBTEver)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="predictedDBT">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveSummary/prd:PredictedDBT">                    
          <xsl:value-of select="concat(number(prd:SBCSExecutiveSummary/prd:PredictedDBT), ' DBT')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'N/A'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="predictedDBTDate">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveSummary/prd:PredictedDBTDate">                    
          <xsl:variable name="date">
             <xsl:call-template name="FormatDate">
               <xsl:with-param name="pattern" select="'mo/dt/year'" />
               <xsl:with-param name="value" select="prd:SBCSExecutiveSummary/prd:PredictedDBTDate" />
             </xsl:call-template>
          </xsl:variable>
          <xsl:value-of select="concat(' for ', $date)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>


    <!-- begin current DBT -->
    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="{$color}">
        
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#ffffff">
                <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                  <tr height="20">
                    <td bgcolor="{$color}" colspan="3" align="left" valign="middle">
                      <font color="#ffffff"><b><xsl:text disable-output-escaping="yes">&#160;&#160;</xsl:text>Current DBT Range (Non-financial Accounts) *</b></font>
                    </td>
                  </tr>  

                  <tr>
                    <td style="line-height:2px;" valign="bottom" colspan="3">&#160;</td>
                  </tr>  

                  <tr>
                    <td colspan="3" align="center">
                      <table width="300" border="0" cellspacing="0" cellpadding="0">
                        <tr>

                          <xsl:choose>                  
                            <xsl:when test="normalize-space($currentDBT) != ''">                     
                              <xsl:choose>                  
                                <xsl:when test="$dbtPointer = 1">                    
                                  <td width="240" align="center" nowrap="nowrap"><font size="3"><b><xsl:value-of select="$currentDBT" /> DBT</b></font><br />
                                    (<xsl:value-of select="$accountBalance" /> Balance)</td>
                                  <td width="60"></td>
                                </xsl:when>
                        
                                <xsl:otherwise>
                                  <td width="300" align="right">
                                    <table border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td align="center" nowrap="nowrap">
                                         <font size="3"><b><xsl:value-of select="$currentDBT" /> DBT</b></font><br />
                                         (<xsl:value-of select="$accountBalance" /> Balance)</td>
                                      </tr>
                                    </table>  
                                  </td>
                                </xsl:otherwise>
                              </xsl:choose>    
      
                            </xsl:when>
                    
                            <xsl:otherwise>
                              <td width="300" align="center" nowrap="nowrap"><img src="../images/spacer.gif" border="0" width="1" height="6" /><br /><b>Current DBT cannot be calculated.</b><br />
                                <img src="../images/spacer.gif" border="0" width="1" height="12" alt=""/></td>
                            </xsl:otherwise>
                          </xsl:choose>    


                        </tr>
                      </table>    
                    </td>
                  </tr>  

                  <xsl:if test="normalize-space($currentDBT) != ''">                     
                    <tr>
                      <td colspan="3" align="center">
                        <table width="300" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="240"></td>
  
                            <td width="33"></td>
  
                            <td width="27"></td>
                          </tr>
  
                          <tr>
                            <xsl:call-template name="PointerLoop">
                              <xsl:with-param name="dbtPointer" select="$dbtPointer" />
                            </xsl:call-template>
                          </tr>
  
                        </table>    
                      </td>
                    </tr>  
                  </xsl:if>

                  <tr>
                    <td colspan="3" align="center">
                      <table width="300" border="0" cellspacing="0" cellpadding="0">
                        
                        <!-- color bar-->
                        <tr>
                          <td bgcolor="#000000">
                            <table width="300" border="0" cellspacing="0" cellpadding="0">
                              <tr height="20">
                                <td valign="middle" width="240" align="center" bgcolor="#007700"><font size="1" color="#ffffff">
                                  0-15</font></td>

                                <td valign="middle" width="33" align="center" bgcolor="#ffff00"><font size="1" color="#000000">
                                  16-50</font></td>

                                <td valign="middle" width="27" align="center" bgcolor="#ff0000"><font size="1" color="#ffffff">
                                  51+</font></td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <!-- end color bar-->

                        <!-- space -->
                        <tr>
                          <td style="line-height:1px;">&#160;</td>
                        </tr>

                        <!-- color bar labels -->
                        <tr>
                          <td>
                            <table width="300" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="240" align="center"><font size="1" style="FONT-FAMILY: 'verdana';"><b>80%</b></font></td>

                                <td width="33" align="center"><font size="1" style="FONT-FAMILY: 'verdana';"><b>11%</b></font></td>

                                <td width="27" align="center"><font size="1" style="FONT-FAMILY: 'verdana';"><b>9%</b></font></td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <!-- end color bar labels -->

                      </table>    
                    </td>
                  </tr>  
                  
                  <tr>
                    <td width="1%">
                      <img src="../images/spacer.gif" border="0" width="5" height="1" /></td>
                    <td width="98%">  
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                   
                        <tr>  
                          <td colspan="2" align="center">
                          <font size="1" style="FONT-FAMILY: 'verdana';"><b>% of US businesses falling within DBT range</b></font><br /><img src="../images/spacer.gif" border="0" width="1" height="6" /><br /></td>
                        </tr>
          
                        <tr>  
                          <td colspan="2">
                            <b>DBT Norms:</b></td>
                        </tr>

                        <tr>  
                          <td width="50%">
                            All industries: <b><xsl:value-of select="$allIndustryDBT" /></b></td>
                          <td width="50%">
                            Same industry: <b><xsl:value-of select="$industryDBT" /></b></td>
                        </tr>
                      </table> 
                    </td>
                    <td width="1%">
                      <img src="../images/spacer.gif" border="0" width="5" height="1" /></td>
                  </tr> 
                          
                  <!-- space -->
                  <tr>
                    <td style="line-height:{number($imgHeight)}px;" colspan="3">&#160;</td>
                  </tr>
                          
                  <tr>
                    <td>
                      <img src="../images/spacer.gif" border="0" width="5" height="1" /></td>
                    <td>  
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>  
                          <td colspan="2">
                            <b>DBT Performance Analysis:</b></td>
                        </tr>

                        <tr height="{number($trHeight)}">
                          <td width="70%">
                            Monthly average DBT:</td>
                          <td width="30%" align="right"><b><xsl:value-of select="$monthlyAvgDBT" /></b></td>
                        </tr>
                        
                        <tr height="{number($trHeight)}">
                          <td>
                            Highest DBT previous 6 months:</td>
                          <td align="right"><b><xsl:value-of select="$highestDBT6M" /></b></td>
                        </tr>
                      
                        
                        <tr height="{number($trHeight)}">
                          <td>
                          Highest DBT previous 5 quarters:</td>
                          <td align="right"><b><xsl:value-of select="$highestDBT5Q" /></b></td>
                        </tr>
                      
                        
                        <tr height="{number($trHeight)}">
                          <td>
                          Highest DBT ever:</td>
                          <td align="right"><b><xsl:value-of select="$highestDBTEver" /></b></td>
                        </tr>
                      
                        
                        <tr height="{number($trHeight)}">
                          <td>
                          Predicted DBT<xsl:value-of select="$predictedDBTDate" />:</td>
                          <td align="right"><b><xsl:value-of select="$predictedDBT" /></b></td>
                        </tr>
                      
                      </table> 
                    </td>
                    <td>
                      <img src="../images/spacer.gif" border="0" width="5" height="1" /></td>
                  </tr>    
                  
                  <tr>
                    <td colspan="3" style="line-height:3px;">&#160;</td>
                  </tr>

                </table>
              </td>
            </tr>     
          </table> 
  
        </td>
      </tr>
    </table>
    <!-- end current DBT -->

  </xsl:template>

  <!--
  *********************************************
  * PointerLoop template
  *********************************************
  -->
  <xsl:template name="PointerLoop">
    <xsl:param name="dbtPointer" select="''" />
    <xsl:param name="index" select="1" />
    
    <xsl:variable name="total" select="3" />
    
    <xsl:choose>                  
      <xsl:when test="$dbtPointer = $index">                     
        <td align="center" style="line-height:13px;" valign="middle"><img src="../images/triangle_blue.gif" border="0" width="3" height="14" alt=""/></td>
      </xsl:when>

      <xsl:otherwise>
        <td align="center"></td>
      </xsl:otherwise>
    </xsl:choose>    

    <!-- Test condition and call template if less than number -->
    <xsl:if test="$index &lt; $total">
      <xsl:call-template name="PointerLoop">
        <xsl:with-param name="index" select="($index + 1)" />
        <xsl:with-param name="dbtPointer" select="$dbtPointer" />
      </xsl:call-template>
    </xsl:if>
    
  </xsl:template>

      
</xsl:stylesheet>
