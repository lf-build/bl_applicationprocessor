
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:rsp="http://www.experian.com/NetConnectResponse"
  xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * Global variables and parameters
  *********************************************
  <xsl:param name="systemYear" select="'2010'" />
  -->
  <xsl:param name="product" select="'SBCSScore'" />
  <xsl:param name="baseProduct" select="''" />


  <!--
  *********************************************
  * include template
  *********************************************
  -->
  <xsl:include href="../common/ReportHeader.xslt" />
  <xsl:include href="../common/Util.xslt" />
  <xsl:include href="../common/case.xslt" />
  <xsl:include href="../common/BackToTop.xslt" />
  <xsl:include href="../common/ErrorProcessing.xslt" />
  <xsl:include href="../common/ReportFooter.xslt" />
  <xsl:include href="../common/CorporateLinkage.xslt" />
  <xsl:include href="IdentifyingInformation.xslt" />
  <xsl:include href="../common/MatchingNameAndAddress.xslt" />
  <xsl:include href="../Intelliscore/CustomFooterMessage.xslt" />
  <xsl:include href="SBCSScorable.xslt" />
  <xsl:include href="BusinessInfo.xslt" />

  <!--
  *********************************************
  * Initial template
  *********************************************
  -->  
  <xsl:template match="/">
    <html>
      <head>
        <title>SBCS Financial Score Report</title>

          <style type="text/css">
            td {font-size: 9pt; font-family: 'arial';}
          </style>

      </head>
      <body>
        <a name="top"></a>
        <table width="715" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td>      

              <!-- SBCSScore template -->
              <xsl:apply-templates select="//prd:SmallBusinessCreditShare" />
              
            </td>
          </tr>
        </table>    
      </body>
    </html>
  </xsl:template>


  <!--
  *********************************************
  * SBCSScore template
  *********************************************
  -->
  <xsl:template match="prd:SmallBusinessCreditShare" >
    <!-- Report Date  -->
    <xsl:variable name="reportDate">
      <xsl:value-of select="prd:SBCSBusinessNameAndAddress/prd:ProfileDate" />
    </xsl:variable>

    <!-- Report Time  -->
    <xsl:variable name="reportTime">
      <xsl:value-of select="prd:SBCSBusinessNameAndAddress/prd:ProfileTime" />
    </xsl:variable>

    <!-- Profile Type  -->
    <xsl:variable name="profileType">
    	<xsl:value-of select="'SBCS Financial Score'" />
    </xsl:variable>

    <!-- Report Header -->
    <xsl:call-template name="ReportHeader">
      <xsl:with-param name="reportType" select="$profileType" />
      <xsl:with-param name="reportDate" select="$reportDate" />
      <xsl:with-param name="reportTime" select="$reportTime" />
      <xsl:with-param name="reportName" select="prd:SBCSBusinessNameAndAddress/prd:BusinessName" />
    </xsl:call-template>

    <br />

    <xsl:variable name="isHTMLModel">
       <xsl:call-template name="isHTMLModel" />
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="normalize-space($isHTMLModel) = 'false' ">
        <xsl:call-template name="NoHTMLReport" />
      </xsl:when>

      <xsl:when test="normalize-space(prd:BusinessNameAndAddress/prd:ProfileType/@code) = 'NO RECORD' ">
        <xsl:call-template name="businessNotFound" />
      </xsl:when>

      <xsl:otherwise>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top" height="20" align="center"><font size="4" color="{$titleColor}"><b><a name="SBCSScore">Small Business Credit Share Financial Score<font size="3"></font></a></b></font></td>
          </tr>
        </table>

  	    <!-- Identifying Information -->
  	    <xsl:call-template name="IdentifyingInformation" />
  	
        <br />

          <xsl:if test="prd:SBCSBusinessNameAndAddress/prd:MatchingNameAndAddress">
            <!-- Matching Name And Address -->
            <xsl:apply-templates select="prd:SBCSBusinessNameAndAddress/prd:MatchingNameAndAddress" />
  
          <br />
        </xsl:if>
  
	    <xsl:if test="prd:CorporateLinkage">
	      <!-- Corporate Linkage -->
	      <xsl:call-template name="CorporateLinkage" />
	    </xsl:if>

        <!-- SBCS Score template -->
        <xsl:call-template name="SBCSScorable" />
    
        <br />

        <xsl:if test="prd:SBCSExecutiveElements and prd:IntelliscoreScoreInformation/prd:ModelInformation/prd:ModelCode and number(prd:IntelliscoreScoreInformation/prd:ModelInformation/prd:ModelCode) != 220">
          <!-- Business Infomation -->
          <xsl:call-template name="BusinessInformation" />
  
          <br />
        </xsl:if>

        <!-- custom footer message -->
        <xsl:call-template name="CustomFooterMessage" />
  
	 <!-- is it a limited report?  -->
  	 <xsl:variable name="reportEnd">
  	    <xsl:choose>		              
  	      <xsl:when test="normalize-space(prd:BillingIndicator/@code) = 'A' ">
  	         <xsl:value-of select="'End of limited'" />
  	      </xsl:when>

  	      <xsl:otherwise>
  	         <xsl:value-of select="'End of report'" />
  	      </xsl:otherwise>
  	    </xsl:choose>    
  	 </xsl:variable>
  
  	 <!-- Report Footer -->
  	 <xsl:call-template name="ReportFooter">
  	   <xsl:with-param name="reportType" select="$product" />
  	   <xsl:with-param name="reportDate" select="$reportDate" />
  	   <xsl:with-param name="reportEnd" select="$reportEnd" />
  	 </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>

  <!--
  *********************************************
  * this template overrides the built-in rule
  * do nothing with stuff I'm not interested in
  *********************************************
  -->
  <xsl:template match="text()|@*">
  </xsl:template>

</xsl:stylesheet>