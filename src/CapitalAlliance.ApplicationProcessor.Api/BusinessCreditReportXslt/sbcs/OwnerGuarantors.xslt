
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rsp="http://www.experian.com/NetConnectResponse"
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * OwnerDetails template
  *********************************************
  -->
  <xsl:template name="OwnerGuarantors">
    <xsl:param name="title" select="''" />
    <xsl:param name="node" />
    <xsl:param name="color" select="'#0099cc'" />
    
    <!-- Section title -->
    <xsl:call-template name="SectionTitle">
      <xsl:with-param name="title" select="$title" />
      <xsl:with-param name="color" select="$titleColor" />
    </xsl:call-template>

    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="{$color}">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#ffffff">
                <table width="100%" border="0" cellspacing="0" cellpadding="4">

                  <!-- loops through owners -->
                  <xsl:apply-templates select="$node" />

                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

  </xsl:template>



  <!--
  *********************************************
  * IndividualOwnerPrincipal template
  *********************************************
  -->
  <xsl:template match="prd:IndividualPersonalGuarantor | prd:IndividualOwnerPrincipal | prd:CompanyGuarantor | prd:CompanyOwnerPrincipal" >

    <xsl:variable name="sequenceNumber">
      <xsl:choose>                  
        <xsl:when test="prd:TradeLinkKey/prd:SequenceNumber">                    
          <xsl:value-of select="number(prd:TradeLinkKey/prd:SequenceNumber)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="classCode">
      <xsl:choose>                  
        <xsl:when test="prd:TradeLinkKey/prd:TradeClassificationCode">                    
          <xsl:value-of select="prd:TradeLinkKey/prd:TradeClassificationCode" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="subclassCode">
      <xsl:choose>                  
        <xsl:when test="prd:TradeLinkKey/prd:TradeSubClassCode">                    
          <xsl:value-of select="prd:TradeLinkKey/prd:TradeSubClassCode" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="classDescription">
      <xsl:choose>                  
        <xsl:when test="normalize-space($classCode) = 'N'">                    
          <xsl:value-of select="'TRADE PAYMENT ACCOUNT'" />
        </xsl:when>

        <xsl:when test="normalize-space($classCode) = 'A'">                    
          <xsl:value-of select="'ADDITIONAL PAYMENT EXPERIENCES'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:call-template name="convertcase">
            <xsl:with-param name="toconvert" select="//prd:SBCSTradeData[prd:FinancialTradeSubClassification/@code =  normalize-space($subclassCode)]/prd:FinancialTradeSubClassification" />
            <xsl:with-param name="conversion" select="'upper'" />
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="linkKey">
      <xsl:value-of select="concat(normalize-space($classCode), normalize-space($subclassCode), normalize-space($sequenceNumber))" />
    </xsl:variable>

    <xsl:variable name="individualCompany">
      <xsl:choose>                  
        <xsl:when test="name(.) = 'IndividualPersonalGuarantor' or name(.) = 'IndividualOwnerPrincipal'">                    
          <xsl:value-of select="'Individual'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'Company'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="ownershipType">
      <xsl:choose>                  
        <xsl:when test="name(.) = 'IndividualPersonalGuarantor' or name(.) = 'CompanyGuarantor'">                    
          <xsl:value-of select="'Guarantor'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'Owner'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="internetAddrType">
      <xsl:choose>                  
        <xsl:when test="normalize-space($individualCompany) = 'Individual'">                    
          <xsl:value-of select="'Email'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'URL'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="name">
      <xsl:choose>                  
        <xsl:when test="normalize-space($individualCompany) = 'Individual'">                    
		      <xsl:variable name="first">
		        <xsl:choose>                  
		          <xsl:when test="prd:FirstName">                     
		            <xsl:value-of select="normalize-space(prd:FirstName)" />
		          </xsl:when>
		  
		          <xsl:otherwise>
		            <xsl:value-of select="''" />
		          </xsl:otherwise>
		        </xsl:choose>    
		      </xsl:variable>
		
		      <xsl:variable name="middle">
		        <xsl:choose>                  
		          <xsl:when test="prd:MiddleName">                     
		            <xsl:value-of select="normalize-space(prd:MiddleName)" />
		          </xsl:when>
		  
		          <xsl:otherwise>
		            <xsl:value-of select="''" />
		          </xsl:otherwise>
		        </xsl:choose>    
		      </xsl:variable>
		
		      <xsl:variable name="last">
		        <xsl:choose>                  
		          <xsl:when test="prd:Surname">                     
		            <xsl:value-of select="normalize-space(prd:Surname)" />
		          </xsl:when>
		  
		          <xsl:otherwise>
		            <xsl:value-of select="''" />
		          </xsl:otherwise>
		        </xsl:choose>    
		      </xsl:variable>
		
		      <xsl:variable name="generation">
		        <xsl:choose>                  
		          <xsl:when test="prd:GenerationCode/@code != ''">                     
		            <xsl:value-of select="normalize-space(prd:GenerationCode)" />
		          </xsl:when>
		  
		          <xsl:otherwise>
		            <xsl:value-of select="''" />
		          </xsl:otherwise>
		        </xsl:choose>    
		      </xsl:variable>
		      
		      <xsl:value-of select="concat($first, ' ', $middle, ' ', $last, ' ', $generation)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="prd:BusinessName" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="street1">
      <xsl:choose>                  
        <xsl:when test="prd:StreetAddress">                     
          <xsl:value-of select="prd:StreetAddress" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="street2">
      <xsl:choose>                  
        <xsl:when test="prd:AddressLine2">                     
          <xsl:value-of select="prd:AddressLine2" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="city">
      <xsl:choose>                  
        <xsl:when test="prd:City">                     
          <xsl:value-of select="prd:City" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="state">
      <xsl:choose>                  
        <xsl:when test="prd:State">                     
          <xsl:value-of select="prd:State" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="zip">
      <xsl:choose>                  
        <xsl:when test="prd:Zip">                     
          <xsl:call-template name="FormatZip">
            <xsl:with-param name="value" select="concat(prd:Zip, prd:ZipExtension)" />
          </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="phone">
      <xsl:choose>                  
        <xsl:when test="prd:PhoneNumber">                     
          <xsl:call-template name="FormatPhone">
            <xsl:with-param name="value" select="prd:PhoneNumber" />
          </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="taxIDType">
      <xsl:choose>                  
        <xsl:when test="normalize-space($individualCompany) = 'Individual'">                    
          <xsl:value-of select="'Tax ID'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="prd:TaxIDType" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="taxID">
	    <xsl:variable name="tmpID">
	      <xsl:choose>                  
	        <xsl:when test="normalize-space($individualCompany) = 'Individual'">                    
	          <xsl:value-of select="prd:SSN" />
	        </xsl:when>
	
	        <xsl:otherwise>
	          <xsl:value-of select="prd:TaxID" />
	        </xsl:otherwise>
	      </xsl:choose>    
	    </xsl:variable>
      <xsl:choose>                  
        <xsl:when test="normalize-space($tmpID) = 'XXXXX'">                    
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="$tmpID" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="internetAddress">
      <xsl:choose>                  
        <xsl:when test="normalize-space($individualCompany) = 'Individual'">                    
          <xsl:value-of select="prd:EmailAddress" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="prd:InternetDomain" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="status">
      <xsl:choose>                  
        <xsl:when test="prd:PreviousCurrentIndicator">                     
          <xsl:value-of select="concat(prd:PreviousCurrentIndicator, ' ', $ownershipType)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="dateReported">
      <xsl:choose>                  
        <xsl:when test="prd:DateReported">                    
           <xsl:call-template name="FormatDate">
             <xsl:with-param name="pattern" select="'mo/dt/year'" />
             <xsl:with-param name="value" select="prd:DateReported" />
           </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="guarantor">
      <xsl:choose>                  
        <xsl:when test="prd:OwnerIsGuarantorIndicator">                     
          <xsl:value-of select="prd:OwnerIsGuarantorIndicator" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="changed">
      <xsl:choose>                  
        <xsl:when test="prd:NameAddressChangeIndicator">                     
          <xsl:value-of select="prd:NameAddressChangeIndicator" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="bgColor">
      <xsl:choose>                  
        <xsl:when test="position() mod 2 = 0">                     
          <xsl:value-of select="'#e5f5fa'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'#ffffff'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <tr>
      
      <td width="98%" bgcolor="{normalize-space($bgColor)}">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td colspan="4" height="16"><font size="1" style="FONT-FAMILY: 'verdana';">
              <b><u><xsl:value-of select="normalize-space($ownershipType)" /> #<xsl:value-of select="normalize-space(position())" />-<xsl:value-of select="normalize-space($individualCompany)" /></u></b></font></td>
          </tr>
          
          <tr>
            <td width="15%"><font size="1" style="FONT-FAMILY: 'verdana';">
              <b>Name</b></font></td>
              
            <td width="32%"><font size="1" style="FONT-FAMILY: 'verdana';">
              <xsl:value-of select="$name" /></font></td>

            <td width="18%"><font size="1" style="FONT-FAMILY: 'verdana';">
              <b>Associated Record</b></font></td>
              
            <td width="35%"><font size="1" style="FONT-FAMILY: 'verdana';">
              <xsl:value-of select="$classDescription" /> #<xsl:value-of select="normalize-space($sequenceNumber)" /></font> <a href="#{normalize-space($linkKey)}"><font size="1">details</font></a></td>
          </tr>

          <tr>
            <td valign="top" rowspan="2"><font size="1" style="FONT-FAMILY: 'verdana';">
              <b>Address</b></font></td>
              
            <td rowspan="2" valign="top"><font size="1" style="FONT-FAMILY: 'verdana';">

              <xsl:if test="normalize-space($street1) != ''">
                <xsl:value-of select="$street1" />

                <xsl:if test="normalize-space($street2) != ''">
                  <br />
                  <xsl:value-of select="$street2" />
                </xsl:if>

                <xsl:if test="normalize-space($city) != ''">
                  <br />
                </xsl:if>
              </xsl:if>
              
              <xsl:if test="normalize-space($city) != ''">
                <xsl:value-of select="concat(normalize-space($city), ', ', normalize-space($state), ' ', normalize-space($zip))" />
              </xsl:if>
              </font></td>
                              
            <td><font size="1" style="FONT-FAMILY: 'verdana';">
              <b>Ownership Status</b></font></td>
              
            <td><font size="1" style="FONT-FAMILY: 'verdana';">
              <xsl:call-template name="convertcase">
                <xsl:with-param name="toconvert" select="$status" />
                <xsl:with-param name="conversion" select="'upper'" />
              </xsl:call-template></font></td>
          </tr>

          <tr>
            <td><font size="1" style="FONT-FAMILY: 'verdana';">
              <b>Date Reported</b></font></td>
              
            <td><font size="1" style="FONT-FAMILY: 'verdana';">
              <xsl:value-of select="$dateReported" /></font></td>
          </tr>
                          
          <tr>
            <td colspan="2" valign="top">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <xsl:if test="normalize-space($phone) != ''">
                  <tr>
                    <td width="32%"><font size="1" style="FONT-FAMILY: 'verdana';">
                      <b>Phone</b></font></td>
                    
                    <td width="68%"><font size="1" style="FONT-FAMILY: 'verdana';">
                      <xsl:value-of select="$phone" /></font></td>
                  </tr>
                </xsl:if>

                <xsl:if test="normalize-space($taxID) != ''">
                  <tr>
                    <td width="32%"><font size="1" style="FONT-FAMILY: 'verdana';">
                      <b><xsl:value-of select="normalize-space($taxIDType)" /></b></font></td>
                      
                    <td width="68%"><font size="1" style="FONT-FAMILY: 'verdana';">
                      <xsl:value-of select="$taxID" /></font></td>
                  </tr>
                </xsl:if>

                <xsl:if test="normalize-space($internetAddress) != ''">
                  <tr>
                    <td width="32%"><font size="1" style="FONT-FAMILY: 'verdana';">
                      <b><xsl:value-of select="normalize-space($internetAddrType)" /></b></font></td>
                      
                    <td width="68%"><font size="1" style="FONT-FAMILY: 'verdana';">
                      <xsl:value-of select="$internetAddress" /></font></td>
                  </tr>
                </xsl:if>
              </table>
            </td>

            <td colspan="2" valign="top">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <xsl:if test="normalize-space($guarantor) = 'Y'">
                  <tr>
                    <td width="34%" valign="top"><font size="1" style="FONT-FAMILY: 'verdana';">
                      <b>Guarantor</b></font></td>
                    
                    <td width="66%"><font size="1" style="FONT-FAMILY: 'verdana';">
                      This owner is also a guarantor for the associated record</font></td>
                  </tr>
                </xsl:if>
              </table>
            </td>
          </tr>

          <xsl:if test="normalize-space($changed) = 'Y'">
            <tr>
              <td colspan="4"><font size="1" style="FONT-FAMILY: 'verdana';">
              <img height="3" width="1" border="0" src="../images/spacer.gif" /><br/>This information represents a change to a prior 
              <xsl:call-template name="convertcase">
                <xsl:with-param name="toconvert" select="$ownershipType" />
                <xsl:with-param name="node" select="'lower'" />
              </xsl:call-template>
               name or address</font></td>
            </tr>
          </xsl:if>
        </table>
      </td>

      
    </tr>
  </xsl:template>
  
  
</xsl:stylesheet>