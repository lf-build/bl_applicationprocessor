
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * KeyMetrics template
  *********************************************
  -->
  <xsl:template name="KeyMetrics">
  
    <!-- Section title -->
    <xsl:call-template name="SectionTitle">
      <xsl:with-param name="title" select="'SBCS Key Metrics'" />
      <xsl:with-param name="color" select="$titleColor" />
    </xsl:call-template>

    <xsl:if test="prd:SBCSKeyMetrics">
      <!-- SBCSKeyMetrics template -->
      <xsl:apply-templates select="prd:SBCSKeyMetrics"/>

      <xsl:if test="prd:SBCSExecutiveElements or prd:SBCSQuarterlyPaymentTrends or prd:SBCSPaymentTrends">
        <br />
      </xsl:if>
    </xsl:if>

    <xsl:if test="prd:SBCSExecutiveElements">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
      
        <tr>
          <!-- Public Record column -->
          <td width="49%" valign="top">
            <xsl:call-template name="CurrentDBT">
              <xsl:with-param name="color" select="$borderColor" />
            </xsl:call-template>
          </td>
      
          <td width="2%">
          </td>
      
          <!-- Performance Analysis column -->
          <td width="49%" valign="top">
            <xsl:call-template name="PerformanceAnalysis">
              <xsl:with-param name="color" select="$borderColor" />
            </xsl:call-template>
          </td>
        </tr>
  
        <xsl:if test="prd:SBCSExecutiveElements">
          <tr>
            <td colspan="3">
              <font size="1"><i>
            * Days Beyond Terms (DBT) is a dollar weighted calculation of the average number of 
              days that payment was made beyond the invoice due date based on trades on file that 
              have been updated in the previous 3 months.
              </i></font>
            </td>
          </tr>
        </xsl:if>    
            
      </table>

      <xsl:if test="prd:SBCSQuarterlyPaymentTrends and prd:SBCSPaymentTrends">
        <br />
      </xsl:if>
    </xsl:if>

    <xsl:if test="prd:SBCSQuarterlyPaymentTrends and prd:SBCSPaymentTrends">
      <xsl:call-template name="DBTCharts">
        <xsl:with-param name="color" select="$borderColor" />
      </xsl:call-template>
    </xsl:if>

  </xsl:template>

</xsl:stylesheet>