
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rsp="http://www.experian.com/NetConnectResponse"
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * AccountDetails template
  *********************************************
  -->
  <xsl:template name="AccountDetails">
    <!-- Section title -->
    <xsl:call-template name="SectionTitle">
      <xsl:with-param name="title" select="'SBCS Account Details'" />
      <xsl:with-param name="color" select="$titleColor" />
    </xsl:call-template>

    <!-- loops through trade data -->
    <xsl:apply-templates select="prd:SBCSTradeData">
      <xsl:with-param name="color" select="$borderColor" />
    </xsl:apply-templates>

  </xsl:template>



  <!--
  *********************************************
  * SBCSTradeData template
  *********************************************
  -->
  <xsl:template match="prd:SBCSTradeData" >
    <xsl:param name="color" select="'#0099cc'" />

    <xsl:variable name="recordType">
      <xsl:choose>                  
        <xsl:when test="prd:RecordType/@code">                     
          <xsl:value-of select="prd:RecordType/@code" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="sequenceNumber">
      <xsl:choose>                  
        <xsl:when test="prd:TradeLinkKey/prd:SequenceNumber">                    
          <xsl:value-of select="number(prd:TradeLinkKey/prd:SequenceNumber)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="classDescription">
      <xsl:choose>                  
        <xsl:when test="normalize-space($recordType) = 'N'">                    
          <xsl:value-of select="'Trade Payment Account'" />
        </xsl:when>

        <xsl:when test="normalize-space($recordType) = 'A'">                    
          <xsl:value-of select="'Additional Payment Experiences'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="prd:FinancialTradeSubClassification" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="classCode">
      <xsl:choose>                  
        <xsl:when test="prd:TradeLinkKey/prd:TradeClassificationCode">                    
          <xsl:value-of select="prd:TradeLinkKey/prd:TradeClassificationCode" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="subclassCode">
      <xsl:choose>                  
        <xsl:when test="prd:TradeLinkKey/prd:TradeSubClassCode">                    
          <xsl:value-of select="prd:TradeLinkKey/prd:TradeSubClassCode" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="linkKey">
      <xsl:value-of select="concat(normalize-space($classCode), normalize-space($subclassCode), normalize-space($sequenceNumber))" />
    </xsl:variable>

    <xsl:variable name="businessCategory">
      <xsl:choose>                  
        <xsl:when test="prd:BusinessCategory">                     
          <xsl:value-of select="prd:BusinessCategory" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="paymentTerms">
      <xsl:variable name="tmpTerms">
        <xsl:value-of select="prd:Terms" />
      </xsl:variable>

      <xsl:variable name="numPayments">
        <xsl:choose>                  
          <xsl:when test="string-length(normalize-space($tmpTerms)) &gt;= 4">                     
            <xsl:value-of select="substring(normalize-space($tmpTerms), 1, 4)" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="$tmpTerms" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>

      <xsl:choose>                  
        <xsl:when test="string(number($numPayments)) != 'NaN'">                    
          <xsl:variable name="frequency">
            <xsl:call-template name="PaymentTermsTable">
              <xsl:with-param name="value" select="substring(normalize-space($tmpTerms), 6, 2)" />
            </xsl:call-template>
          </xsl:variable>
          
          <xsl:value-of select="concat(number($numPayments), ' payment(s) - ', $frequency)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:variable name="tmp">
            <xsl:call-template name="PaymentTermsTable">
              <xsl:with-param name="value" select="normalize-space($tmpTerms)" />
            </xsl:call-template>
          </xsl:variable>

          <xsl:choose>                  
            <xsl:when test="normalize-space($tmp) = ''">                     
              <xsl:value-of select="$tmpTerms" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="$tmp" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="dateReported">
      <xsl:choose>                  
        <xsl:when test="prd:DateReported">                    
           <xsl:call-template name="FormatDate">
             <xsl:with-param name="pattern" select="'mo/dt/year'" />
             <xsl:with-param name="value" select="prd:DateReported" />
           </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="paymentStatus">
      <xsl:choose>                  
        <xsl:when test="prd:PaymentStatusCode">                    
          <xsl:value-of select="prd:PaymentStatusCode" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="lastActivity">
      <xsl:variable name="activityType">
        <xsl:choose>                  
          <xsl:when test="prd:LastActivityType">                     
            <xsl:value-of select="prd:LastActivityType" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>

      <xsl:variable name="activityDate">
        <xsl:choose>                  
          <xsl:when test="prd:DateLastActivity">                    
             <xsl:call-template name="FormatDate">
               <xsl:with-param name="pattern" select="'mo/dt/year'" />
               <xsl:with-param name="value" select="prd:DateLastActivity" />
             </xsl:call-template>
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>

      <xsl:choose>                  
        <xsl:when test="normalize-space($activityType) = '' and normalize-space($activityDate) != ''">                    
          <xsl:value-of select="$activityDate" />
        </xsl:when>

        <xsl:when test="normalize-space($activityType) != '' and normalize-space($activityDate) = ''">                    
          <xsl:value-of select="$activityType" />
        </xsl:when>

        <xsl:when test="normalize-space($activityType) != '' and normalize-space($activityDate) != ''">                    
          <xsl:value-of select="concat($activityType, ' (', normalize-space($activityDate), ')')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="comments">
      <xsl:variable name="comment1">
        <xsl:choose>                  
          <xsl:when test="prd:CommentCode1Description">                     
            <xsl:value-of select="prd:CommentCode1Description" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>

      <xsl:variable name="comment2">
        <xsl:variable name="tmp2">
          <xsl:choose>                  
            <xsl:when test="prd:CommentCode2Description">                     
              <xsl:value-of select="prd:CommentCode2Description" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="''" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:variable>

        <xsl:choose>                  
          <xsl:when test="normalize-space($comment1) != '' and normalize-space($tmp2) != ''">                    
            <xsl:value-of select="concat(normalize-space($comment1), '; ', normalize-space($tmp2))" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="concat($comment1, $tmp2)" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>

      <xsl:variable name="comment3">
        <xsl:variable name="tmp3">
          <xsl:choose>                  
            <xsl:when test="prd:CommentCode3Description">                     
              <xsl:value-of select="prd:CommentCode3Description" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="''" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:variable>

        <xsl:choose>                  
          <xsl:when test="normalize-space($comment2) != '' and normalize-space($tmp3) != ''">                    
            <xsl:value-of select="concat(normalize-space($comment2), '; ', normalize-space($tmp3))" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="concat($comment2, $tmp3)" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>

      <xsl:value-of select="$comment3" />
    </xsl:variable>

    <xsl:variable name="paymentHistory">
      <xsl:choose>                  
        <xsl:when test="prd:PaymentHistoryProfile">                     
          <xsl:value-of select="translate(prd:PaymentHistoryProfile, ' ', '_')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="amountCurrent">
      <xsl:choose>                  
        <xsl:when test="prd:CurrentAmountDue and number(prd:CurrentAmountDue) != 0">                    
          <xsl:value-of select="format-number(prd:CurrentAmountDue, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="amount30">
      <xsl:choose>                  
        <xsl:when test="prd:DBT1to30Amount and number(prd:DBT1to30Amount) != 0">                    
          <xsl:value-of select="format-number(prd:DBT1to30Amount, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="amount60">
      <xsl:choose>                  
        <xsl:when test="prd:DBT31to60Amount and number(prd:DBT31to60Amount) != 0">                    
          <xsl:value-of select="format-number(prd:DBT31to60Amount, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="amount90">
      <xsl:choose>                  
        <xsl:when test="prd:DBT61to90Amount and number(prd:DBT61to90Amount) != 0">                    
          <xsl:value-of select="format-number(prd:DBT61to90Amount, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="amount120">
      <xsl:choose>                  
        <xsl:when test="prd:DBT91to120Amount and number(prd:DBT91to120Amount) != 0">                    
          <xsl:value-of select="format-number(prd:DBT91to120Amount, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="amount121Plus">
      <xsl:choose>                  
        <xsl:when test="(prd:DBT121to150Amount or prd:DBT151to180Amount or prd:DBT181PlusAmount) and (number(prd:DBT121to150Amount + prd:DBT151to180Amount + prd:DBT181PlusAmount) != 0)">                    
          <xsl:value-of select="format-number((prd:DBT121to150Amount + prd:DBT151to180Amount + prd:DBT181PlusAmount), '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="percentCurrent">
      <xsl:choose>		              
        <xsl:when test="prd:CurrentPercentage and number(prd:CurrentPercentage) != 0">		    		   		   
          <xsl:value-of select="format-number(prd:CurrentPercentage div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="percentDBT30">
      <xsl:choose>                  
        <xsl:when test="prd:DBT30 and number(prd:DBT30) != 0">                     
          <xsl:value-of select="format-number(prd:DBT30 div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="percentDBT60">
      <xsl:choose>                  
        <xsl:when test="prd:DBT60 and number(prd:DBT60) != 0">                     
          <xsl:value-of select="format-number(prd:DBT60 div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="percentDBT90">
      <xsl:choose>                  
        <xsl:when test="prd:DBT90 and number(prd:DBT90) != 0">                     
          <xsl:value-of select="format-number(prd:DBT90 div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="percentDBT120">
      <xsl:choose>                  
        <xsl:when test="prd:DBT120 and number(prd:DBT120) != 0">                     
          <xsl:value-of select="format-number(prd:DBT120 div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="percentDBT121Plus">
      <xsl:choose>                  
        <xsl:when test="prd:DBT150 and number(prd:DBT150) != 0">                     
          <xsl:value-of select="format-number(prd:DBT150 div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="creditLoan">
      <xsl:choose>                  
        <xsl:when test="prd:CurrentCreditLimitOrLoanAmount and number(prd:CurrentCreditLimitOrLoanAmount) != 0">                    
          <xsl:value-of select="format-number(prd:CurrentCreditLimitOrLoanAmount, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="highCredit">
      <xsl:choose>                  
        <xsl:when test="prd:RecentHighCredit and number(prd:RecentHighCredit/prd:Amount) != 0">                    
          <xsl:value-of select="concat(prd:RecentHighCredit/prd:Modifier/@code, format-number(prd:RecentHighCredit/prd:Amount, '$###,###,##0'))" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="presentBalance">
      <xsl:choose>                  
        <xsl:when test="(prd:AccountBalance and number(prd:AccountBalance/prd:Amount) != 0) or normalize-space($creditLoan) != '' or normalize-space($highCredit) != ''">                    
          <xsl:value-of select="concat(prd:AccountBalance/prd:Modifier/@code, format-number(prd:AccountBalance/prd:Amount, '$###,###,##0'))" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="accountNumber">
      <xsl:choose>                  
        <xsl:when test="prd:AccountNumber">                     
          <xsl:value-of select="prd:AccountNumber" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="accountType">
      <xsl:choose>                  
        <xsl:when test="prd:AccountTypeCode">                     
          <xsl:value-of select="prd:AccountTypeCode" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="accountStatus">
      <xsl:choose>                  
        <xsl:when test="prd:AccountStatusIndicator">                     
          <xsl:value-of select="prd:AccountStatusIndicator" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="colorLightBlue">
      <xsl:value-of select="'#e5f5fa'" />
    </xsl:variable>

    <xsl:variable name="colorWhite">
      <xsl:value-of select="'#ffffff'" />
    </xsl:variable>

    <xsl:variable name="line1Color">
      <xsl:value-of select="$colorLightBlue" />
    </xsl:variable>

    <xsl:variable name="dateOpened">
      <xsl:choose>                  
        <xsl:when test="prd:DateOpened">                    
           <xsl:call-template name="FormatDate">
             <xsl:with-param name="pattern" select="'mo/dt/year'" />
             <xsl:with-param name="value" select="prd:DateOpened" />
           </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="dateClosed">
      <xsl:choose>                  
        <xsl:when test="prd:DateClosed">                    
           <xsl:call-template name="FormatDate">
             <xsl:with-param name="pattern" select="'mo/dt/year'" />
             <xsl:with-param name="value" select="prd:DateClosed" />
           </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="reasonClosed">
      <xsl:choose>                  
        <xsl:when test="prd:ReasonClosed">                     
          <xsl:value-of select="prd:ReasonClosed" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="line2Color">
      <xsl:choose>                  
        <xsl:when test="normalize-space($dateOpened) != '' or normalize-space($dateClosed) != '' or normalize-space($reasonClosed) != ''">                     
          <xsl:choose>                  
            <xsl:when test="normalize-space($line1Color) = normalize-space($colorWhite)">                     
              <xsl:value-of select="$colorLightBlue" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="$colorWhite" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="$line1Color" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="dateLastPayment">
      <xsl:choose>                  
        <xsl:when test="prd:DateOfLastPayment">                    
           <xsl:call-template name="FormatDate">
             <xsl:with-param name="pattern" select="'mo/dt/year'" />
             <xsl:with-param name="value" select="prd:DateOfLastPayment" />
           </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="actualPayment">
      <xsl:choose>                  
        <xsl:when test="prd:ActualPaymentAmount and number(prd:ActualPaymentAmount) != 0">                    
          <xsl:value-of select="format-number(prd:ActualPaymentAmount, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="monthlyPayment">
      <xsl:choose>                  
        <xsl:when test="(prd:MonthlyPayment and number(prd:MonthlyPayment/prd:Amount) = 0) and normalize-space(prd:MonthlyPayment/prd:Modifier/@code) = '+'">                    
          <xsl:value-of select="'UNK'" />
        </xsl:when>

        <xsl:when test="prd:MonthlyPayment and number(prd:MonthlyPayment/prd:Amount) != 0">                    
          <xsl:value-of select="concat(prd:MonthlyPayment/prd:Modifier/@code, format-number(prd:MonthlyPayment/prd:Amount, '$###,###,##0'))" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="line3Color">
      <xsl:choose>                  
        <xsl:when test="normalize-space($dateLastPayment) != '' or normalize-space($actualPayment) != '' or normalize-space($monthlyPayment) != ''">                     
          <xsl:choose>                  
            <xsl:when test="normalize-space($line2Color) = normalize-space($colorWhite)">                     
              <xsl:value-of select="$colorLightBlue" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="$colorWhite" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="$line2Color" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="chargeOffDate">
      <xsl:choose>                  
        <xsl:when test="prd:ChargeOffDate">                    
           <xsl:call-template name="FormatDate">
             <xsl:with-param name="pattern" select="'mo/dt/year'" />
             <xsl:with-param name="value" select="prd:ChargeOffDate" />
           </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="chargeOffAmount">
      <xsl:choose>                  
        <xsl:when test="prd:ChargeOffAmount and number(prd:ChargeOffAmount) != 0">                    
          <xsl:value-of select="format-number(prd:ChargeOffAmount, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="chargeOff">
      <xsl:choose>                  
        <xsl:when test="normalize-space($chargeOffDate) != '' or normalize-space($chargeOffAmount) != ''">                    
          <xsl:value-of select="'Yes'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="line4Color">
      <xsl:choose>                  
        <xsl:when test="normalize-space($chargeOffDate) != '' or normalize-space($chargeOffAmount) != '' or normalize-space($chargeOff) != ''">                     
          <xsl:choose>                  
            <xsl:when test="normalize-space($line3Color) = normalize-space($colorWhite)">                     
              <xsl:value-of select="$colorLightBlue" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="$colorWhite" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="$line3Color" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="guarantors">
      <xsl:choose>                  
        <xsl:when test="prd:NumberOfGuarantors and number(prd:NumberOfGuarantors) &gt; 0">                     
          <xsl:value-of select="'Yes'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'No'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="secured">
      <xsl:choose>                  
        <xsl:when test="prd:SecuredIndicator and normalize-space(prd:SecuredIndicator) = 'Y'">                     
          <xsl:value-of select="'Yes'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'No'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="collateral">
      <xsl:choose>                  
        <xsl:when test="prd:Collateral">                     
          <xsl:value-of select="prd:Collateral" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="line5Color">
      <xsl:choose>                  
        <xsl:when test="normalize-space($guarantors) = 'Yes' or normalize-space($secured) = 'Yes' or normalize-space($collateral) != ''">                     
          <xsl:choose>                  
            <xsl:when test="normalize-space($line4Color) = normalize-space($colorWhite)">                     
              <xsl:value-of select="$colorLightBlue" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="$colorWhite" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="$line4Color" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="balloonDate">
      <xsl:choose>                  
        <xsl:when test="prd:BalloonPaymentDate">                    
           <xsl:call-template name="FormatDate">
             <xsl:with-param name="pattern" select="'mo/dt/year'" />
             <xsl:with-param name="value" select="prd:BalloonPaymentDate" />
           </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="balloonAmount">
      <xsl:choose>                  
        <xsl:when test="prd:BalloonPaymentAmount and number(prd:BalloonPaymentAmount) != 0">                    
          <xsl:value-of select="format-number(prd:BalloonPaymentAmount, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="line6Color">
      <xsl:choose>                  
        <xsl:when test="normalize-space($balloonDate) != '' or normalize-space($balloonAmount) != ''">                     
          <xsl:choose>                  
            <xsl:when test="normalize-space($line5Color) = normalize-space($colorWhite)">                     
              <xsl:value-of select="$colorLightBlue" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="$colorWhite" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="$line5Color" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="accountTypeCode">
      <xsl:choose>                  
        <xsl:when test="prd:AccountTypeCode/@code">                     
          <xsl:value-of select="prd:AccountTypeCode/@code" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="supplierNumber">
      <xsl:choose>                  
        <xsl:when test="prd:VerificationReport/prd:SupplierNumber">                     
          <xsl:value-of select="prd:VerificationReport/prd:SupplierNumber" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="SIN">
      <xsl:choose>                  
        <xsl:when test="prd:VerificationReport/prd:SIN">                     
          <xsl:value-of select="prd:VerificationReport/prd:SIN" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="line7Color">
      <xsl:choose>                  
        <xsl:when test="normalize-space($supplierNumber) != '' or normalize-space($SIN) != ''">                     
          <xsl:choose>                  
            <xsl:when test="normalize-space($line6Color) = normalize-space($colorWhite)">                     
              <xsl:value-of select="$colorLightBlue" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="$colorWhite" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="$line6Color" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="TIN">
      <xsl:choose>                  
        <xsl:when test="prd:VerificationReport/prd:TIN">                     
          <xsl:value-of select="prd:VerificationReport/prd:TIN" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="LVID">
      <xsl:choose>                  
        <xsl:when test="prd:VerificationReport/prd:LVID">                     
          <xsl:value-of select="prd:VerificationReport/prd:LVID" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="line8Color">
      <xsl:choose>                  
        <xsl:when test="normalize-space($TIN) != '' or normalize-space($LVID) != ''">                     
          <xsl:choose>                  
            <xsl:when test="normalize-space($line7Color) = normalize-space($colorWhite)">                     
              <xsl:value-of select="$colorLightBlue" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="$colorWhite" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="$line7Color" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>


    <xsl:if test="position() &gt; 1">
      <!-- white space between rows -->
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td style="line-height:8px;">&#160;</td>
        </tr>
      </table>
    </xsl:if>
    
    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="{$color}">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#ffffff">
                <a name="{normalize-space($linkKey)}"></a>

                <!-- trade title -->
                <table width="100%" border="0" cellspacing="0" cellpadding="3">
                  <tr>
                    <td><font color="{$color}"><b><xsl:value-of select="$classDescription" /> # <xsl:value-of select="normalize-space($sequenceNumber)" /></b></font></td>
                  </tr>
                </table>
                <!-- end trade title -->
                  
                <table width="100%" border="0" cellspacing="0" cellpadding="2">
                  <!-- business category, payment terms -->
                  <tr>
                    <td>
                      <table width="100%" border="0" cellspacing="2" cellpadding="0">  
                        <tr>
                          <td width="18%"><font size="1" style="FONT-FAMILY: 'verdana';">
                            <b>Business Category:</b></font></td>

                          <td width="32%"><font size="1" style="FONT-FAMILY: 'verdana';">
                            <xsl:value-of select="$businessCategory" /></font></td>

                          <td width="16%"><font size="1" style="FONT-FAMILY: 'verdana';">
                            <b>Payment Terms:</b></font></td>

                          <td width="34%"><font size="1" style="FONT-FAMILY: 'verdana';">
                            <xsl:value-of select="$paymentTerms" /></font></td>
                        </tr>

                        <!-- date reported, payment status -->
                        <tr>
                          <td><font size="1" style="FONT-FAMILY: 'verdana';">
                            <b>Date Reported:</b></font></td>

                          <td><font size="1" style="FONT-FAMILY: 'verdana';">
                            <xsl:value-of select="$dateReported" /></font></td>

                          <td valign="top"><font size="1" style="FONT-FAMILY: 'verdana';">
                            <b>Payment Status:</b></font></td>

                          <td valign="top"><font size="1" style="FONT-FAMILY: 'verdana';">
                            <xsl:value-of select="$paymentStatus" /></font></td>
                        </tr>

                        <!-- last activity -->
                        <tr>
                          <td valign="top"><font size="1" style="FONT-FAMILY: 'verdana';">
                            <b>Last Activity:</b></font></td>

                          <td valign="top"><font size="1" style="FONT-FAMILY: 'verdana';">
                            <xsl:value-of select="$lastActivity" /></font></td>

                          <td><font size="1" style="FONT-FAMILY: 'verdana';">
                            <b>Comments:</b></font></td>

                          <td><font size="1" style="FONT-FAMILY: 'verdana';">
                            <xsl:value-of select="$comments" /></font></td>
                        </tr>

                        <!-- white space line -->
                        <tr>
                          <td style="line-height:2px;" colspan="4">&#160;</td>
                        </tr>
                      </table>
                    </td>
                  </tr>                    
                </table>

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <!-- title for 36 month history and present payment status -->
                  <tr>
                    <td width="1%">
                      <img height="1" width="2" border="0" src="../images/spacer.gif" /></td>

                    <td width="32%"><font size="1" color="{$color}" style="FONT-FAMILY: 'verdana';">
                      <b>36 Month Payment History</b></font></td>

                    <td width="1%">
                      <img height="1" width="10" border="0" src="../images/spacer.gif" /></td>

                    <td width="65%"><font size="1" color="{$color}" style="FONT-FAMILY: 'verdana';">
                      <b>Present Payment Status</b></font></td>

                    <td width="1%">
                      <img height="1" width="2" border="0" src="../images/spacer.gif" /></td>

                  </tr>

                  <!-- white space line -->
                  <tr>
                    <td style="line-height:2px;" colspan="5">&#160;</td>
                  </tr>

                  <tr>
                    <td>
                      <img height="1" width="4" border="0" src="../images/spacer.gif" /></td>

                    <td valign="top">

                      <!-- 36 month history row -->
                      <table width="100%" border="0" cellpadding="0" cellspacing="0">

                        <xsl:call-template name="PaymentHistoryRows">
                          <xsl:with-param name="row" select="'1'"/>
                          <xsl:with-param name="totalRows" select="'3'"/>
                          <xsl:with-param name="historyChars" select="normalize-space($paymentHistory)"/>
                        </xsl:call-template>
                      </table>

                    </td>

                    <td>
                      <img height="1" width="5" border="0" src="../images/spacer.gif" /></td>

                    <td>
                      <!-- present payment status table -->
                      <table width="100%" bgcolor="{$color}" border="0" cellpadding="1" cellspacing="0">
                        <tr>
                          <td bgcolor="{$color}">
                            <table width="100%" border="0" cellpadding="0" cellspacing="1">
                              
                              <!-- days past due -->
                              <tr>
                                <td colspan="6" bgcolor="#ffffff">
                                  <table width="100%" border="0" cellpadding="0" cellspacing="1">
                                    <tr height="15">
                                      <td width="17%" align="center">
                                        <font size="1" style="FONT-FAMILY: 'verdana';">
                                        <b>Current</b></font></td>
                                    
                                      <td width="17%" align="center">
                                        <font size="1" style="FONT-FAMILY: 'verdana';">
                                        <b>1-30</b></font></td>
                                    
                                      <td width="17%" align="center">
                                        <font size="1" style="FONT-FAMILY: 'verdana';">
                                        <b>31-60</b></font></td>
                                    
                                      <td width="17%" align="center">
                                        <font size="1" style="FONT-FAMILY: 'verdana';">
                                        <b>61-90</b></font></td>
                                    
                                      <td width="16%" align="center">
                                        <font size="1" style="FONT-FAMILY: 'verdana';">
                                        <b>91-120</b></font></td>

                                      <td width="16%" align="center">
                                        <font size="1" style="FONT-FAMILY: 'verdana';">
                                        <b>121+</b></font></td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                              
                              <!-- amount past due -->
                              <tr height="15">
                                <td width="17%" align="center" bgcolor="#007700">
                                  <font size="1" color="#ffffff" style="FONT-FAMILY: 'verdana';">
                                  <xsl:value-of select="$amountCurrent" disable-output-escaping="yes" /></font></td>
                              
                                <td width="17%" align="center" bgcolor="#ffff00">
                                  <font size="1" style="FONT-FAMILY: 'verdana';">
                                  <xsl:value-of select="$amount30" disable-output-escaping="yes" /></font></td>

                                <td width="17%" align="center" bgcolor="#ffff00">
                                  <font size="1" style="FONT-FAMILY: 'verdana';">
                                  <xsl:value-of select="$amount60" disable-output-escaping="yes" /></font></td>

                                <td width="17%" align="center" bgcolor="#ffff00">
                                  <font size="1" style="FONT-FAMILY: 'verdana';">
                                  <xsl:value-of select="$amount90" disable-output-escaping="yes" /></font></td>

                                <td width="16%" align="center" bgcolor="#ff0000">
                                  <font size="1" color="#ffffff" style="FONT-FAMILY: 'verdana';">
                                  <xsl:value-of select="$amount120" disable-output-escaping="yes" /></font></td>

                                <td width="16%" align="center" bgcolor="#ff0000">
                                  <font size="1" color="#ffffff" style="FONT-FAMILY: 'verdana';">
                                  <xsl:value-of select="$amount121Plus" disable-output-escaping="yes" /></font></td>
                              </tr>
                              
                              <xsl:if test="normalize-space($recordType) = 'N' or normalize-space($recordType) = 'A'">
                              
                                <!-- percentage past due -->
                                <tr height="15">
                                  <td align="center" bgcolor="#007700">
                                    <font size="1" color="#ffffff" style="FONT-FAMILY: 'verdana';">
                                    <xsl:value-of select="$percentCurrent" disable-output-escaping="yes" /></font></td>
                                
                                  <td align="center" bgcolor="#ffff00">
                                    <font size="1" style="FONT-FAMILY: 'verdana';">
                                    <xsl:value-of select="$percentDBT30" disable-output-escaping="yes" /></font></td>
  
                                  <td align="center" bgcolor="#ffff00">
                                    <font size="1" style="FONT-FAMILY: 'verdana';">
                                    <xsl:value-of select="$percentDBT60" disable-output-escaping="yes" /></font></td>
  
                                  <td align="center" bgcolor="#ffff00">
                                    <font size="1" style="FONT-FAMILY: 'verdana';">
                                    <xsl:value-of select="$percentDBT90" disable-output-escaping="yes" /></font></td>
  
                                  <td align="center" bgcolor="#ff0000">
                                    <font size="1" color="#ffffff" style="FONT-FAMILY: 'verdana';">
                                    <xsl:value-of select="$percentDBT120" disable-output-escaping="yes" /></font></td>
  
                                  <td align="center" bgcolor="#ff0000">
                                    <font size="1" color="#ffffff" style="FONT-FAMILY: 'verdana';">
                                    <xsl:value-of select="$percentDBT121Plus" disable-output-escaping="yes" /></font></td>
                                </tr>
                              </xsl:if>

                              <!-- present balance, recent high credit, original loan -->
                              <tr>
                                <td colspan="6" bgcolor="#ffffff">
                                  <table width="100%" border="0" cellpadding="0" cellspacing="2">
                                    <tr>
                                      <td width="60%">
                                        <font size="1" style="FONT-FAMILY: 'verdana';">
                                        <b>Present Balance:</b></font></td>
                                    
                                      <td width="40%" align="right">
                                        <font size="1" style="FONT-FAMILY: 'verdana';">
                                        <xsl:value-of select="$presentBalance" /><xsl:text disable-output-escaping="yes">&#160;</xsl:text></font></td>
                                    </tr>

                                    <tr>
                                      <td>
                                        <font size="1" style="FONT-FAMILY: 'verdana';">
                                        <b>Recent High Credit:</b></font></td>
                                    
                                      <td align="right">
                                        <font size="1" style="FONT-FAMILY: 'verdana';">
                                        <xsl:value-of select="$highCredit" /><xsl:text disable-output-escaping="yes">&#160;</xsl:text></font></td>
                                    </tr>

                                    <tr>
                                      <td>
                                        <font size="1" style="FONT-FAMILY: 'verdana';">
                                        <b>Credit Limit/Original Loan Amount:</b></font></td>
                                    
                                      <td align="right">
                                        <font size="1" style="FONT-FAMILY: 'verdana';">
                                        <xsl:value-of select="$creditLoan" /><xsl:text disable-output-escaping="yes">&#160;</xsl:text></font></td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                              
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>

                    <td>
                      <img height="1" width="4" border="0" src="../images/spacer.gif" /></td>
                  </tr>

                  <!-- white space line -->
                  <tr>
                    <td style="line-height:5px;" colspan="5">&#160;</td>
                  </tr>
                </table>

                <!-- Additional account information -->
                <table width="100%" border="0" cellspacing="0" cellpadding="4">
                  <tr>
                    <td colspan="6"><font size="1" color="{$color}" style="FONT-FAMILY: 'verdana';">
                      <b>Additional Account Information</b></font></td>
                  </tr>

                  <tr>
                    <td width="19%" bgcolor="#e5f5fa" valign="top"><font size="1" style="FONT-FAMILY: 'verdana';">
                      <b>Account Number</b></font></td>

                    <td width="15%" bgcolor="#e5f5fa" valign="top"><font size="1" style="FONT-FAMILY: 'verdana';">
                      <xsl:value-of select="$accountNumber" /></font></td>

                    <td width="18%" bgcolor="#e5f5fa" valign="top"><font size="1" style="FONT-FAMILY: 'verdana';">
                      <b>Account Type</b></font></td>

                    <td width="16%" bgcolor="#e5f5fa" valign="top"><font size="1" style="FONT-FAMILY: 'verdana';">
                      <xsl:value-of select="$accountType" /></font></td>

                    <td width="20%" bgcolor="#e5f5fa" valign="top"><font size="1" style="FONT-FAMILY: 'verdana';">
                      <b>Account Status</b></font></td>

                    <td width="12%" bgcolor="#e5f5fa" valign="top"><font size="1" style="FONT-FAMILY: 'verdana';">
                      <xsl:value-of select="$accountStatus" /></font></td>
                  </tr>
                  
                  <xsl:if test="normalize-space($dateOpened) != '' or normalize-space($dateClosed) != '' or normalize-space($reasonClosed) != ''">
                    <tr>
                      <td valign="top" bgcolor="{normalize-space($line2Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <b>Date Opened</b></font></td>

                      <td valign="top" bgcolor="{normalize-space($line2Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <xsl:value-of select="$dateOpened" /></font></td>

                      <td valign="top" bgcolor="{normalize-space($line2Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <b>Date Closed</b></font></td>

                      <td valign="top" bgcolor="{normalize-space($line2Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <xsl:value-of select="$dateClosed" /></font></td>

                      <td valign="top" bgcolor="{normalize-space($line2Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <b>Reason Closed</b></font></td>

                      <td valign="top" bgcolor="{normalize-space($line2Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <xsl:value-of select="$reasonClosed" /></font></td>
                    </tr>
                  </xsl:if>

                  <xsl:if test="normalize-space($dateLastPayment) != '' or normalize-space($actualPayment) != '' or normalize-space($monthlyPayment) != ''">
                    <tr>
                      <td bgcolor="{normalize-space($line3Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <b>Date of Last Payment</b></font></td>

                      <td bgcolor="{normalize-space($line3Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <xsl:value-of select="$dateLastPayment" /></font></td>

                      <td bgcolor="{normalize-space($line3Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <b>Actual Payment Amt</b></font></td>

                      <td bgcolor="{normalize-space($line3Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <xsl:value-of select="$actualPayment" /></font></td>

                      <td bgcolor="{normalize-space($line3Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <b>Monthly Payment Amt</b></font></td>

                      <td bgcolor="{normalize-space($line3Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <xsl:value-of select="$monthlyPayment" /></font></td>
                    </tr>
                  </xsl:if>

                  <xsl:if test="normalize-space($chargeOff) != '' or normalize-space($chargeOffAmount) != '' or normalize-space($chargeOffDate) != ''">
                    <tr>
                      <td bgcolor="{normalize-space($line4Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <b>Charge off</b></font></td>

                      <td bgcolor="{normalize-space($line4Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <xsl:value-of select="$chargeOff" /></font></td>

                      <td bgcolor="{normalize-space($line4Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <b>Charge off Amt</b></font></td>

                      <td bgcolor="{normalize-space($line4Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <xsl:value-of select="$chargeOffAmount" /></font></td>

                      <td bgcolor="{normalize-space($line4Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <b>Charge off Date</b></font></td>

                      <td bgcolor="{normalize-space($line4Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <xsl:value-of select="$chargeOffDate" /></font></td>
                    </tr>
                  </xsl:if>

                  <xsl:if test="normalize-space($guarantors) = 'Yes' or normalize-space($secured) = 'Yes' or normalize-space($collateral) != ''">
                    <tr>
                      <td bgcolor="{normalize-space($line5Color)}" valign="top"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <b>Guarantors</b></font></td>

                      <td bgcolor="{normalize-space($line5Color)}" valign="top"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <xsl:value-of select="$guarantors" /></font></td>

                      <td bgcolor="{normalize-space($line5Color)}" valign="top"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <b>Secured</b></font></td>

                      <td bgcolor="{normalize-space($line5Color)}" valign="top"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <xsl:value-of select="$secured" /></font></td>

                      <td bgcolor="{normalize-space($line5Color)}" valign="top"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <b>Collateral</b></font></td>

                      <td bgcolor="{normalize-space($line5Color)}" valign="top"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <xsl:value-of select="$collateral" /></font></td>
                    </tr>
                  </xsl:if>

                  <xsl:if test="normalize-space($balloonDate) != '' or normalize-space($balloonAmount) != ''">
                    <tr>
                      <td bgcolor="{normalize-space($line6Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <b>Balloon Payment Amt</b></font></td>

                      <td bgcolor="{normalize-space($line6Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <xsl:value-of select="$balloonAmount" /></font></td>

                      <td bgcolor="{normalize-space($line6Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <b>Balloon Payment Date</b></font></td>

                      <td bgcolor="{normalize-space($line6Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <xsl:value-of select="$balloonDate" /></font></td>

                      <td bgcolor="{normalize-space($line6Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <xsl:text disable-output-escaping="yes">&#160;</xsl:text></font></td>

                      <td bgcolor="{normalize-space($line6Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <xsl:text disable-output-escaping="yes">&#160;</xsl:text></font></td>
                    </tr>
                  </xsl:if>

                  <xsl:if test="normalize-space($supplierNumber) != '' or normalize-space($SIN) != ''">                     
                    <tr>
                      <td bgcolor="{normalize-space($line7Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <b>Supplier Number</b></font></td>
                      
                      <td bgcolor="{normalize-space($line7Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <xsl:value-of select="$supplierNumber" /></font></td>
                      
                      <td bgcolor="{normalize-space($line7Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <b>SIN</b></font></td>
                      
                      <td bgcolor="{normalize-space($line7Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <xsl:value-of select="$SIN" /></font></td>
                      
                      <td bgcolor="{normalize-space($line7Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <b>Account Type</b></font></td>
                      
                      <td bgcolor="{normalize-space($line7Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <xsl:value-of select="$accountTypeCode" /></font></td>
                    </tr>
                  </xsl:if>

                  <xsl:if test="normalize-space($TIN) != '' or normalize-space($LVID) != ''">                     
                    <tr>
                      <td bgcolor="{normalize-space($line8Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <b>TIN</b></font></td>
                      
                      <td bgcolor="{normalize-space($line8Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <xsl:value-of select="$TIN" /></font></td>
                      
                      <td bgcolor="{normalize-space($line8Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <b>LVID</b></font></td>
                      
                      <td bgcolor="{normalize-space($line8Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <xsl:value-of select="$LVID" /></font></td>
                      
                      <td bgcolor="{normalize-space($line8Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <xsl:text disable-output-escaping="yes">&#160;</xsl:text></font></td>
                      
                      <td bgcolor="{normalize-space($line8Color)}"><font size="1" style="FONT-FAMILY: 'verdana';">
                        <xsl:text disable-output-escaping="yes">&#160;</xsl:text></font></td>
                    </tr>
                  </xsl:if>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </xsl:template>



  <!--
  ********************************************************
  * translate PaymentTermsTable
  ********************************************************
  -->
  <xsl:template name="PaymentTermsTable">
    <xsl:param name="value" select="''" />

    <xsl:choose>                  
      <xsl:when test="normalize-space($value) = 'REV' ">
        <xsl:text disable-output-escaping="yes">Revolving</xsl:text>
      </xsl:when>
      <xsl:when test="normalize-space($value) = 'P' ">
        <xsl:text disable-output-escaping="yes">One Payment</xsl:text>
      </xsl:when>
      <xsl:when test="normalize-space($value) = 'DEFERRD' ">
        <xsl:text disable-output-escaping="yes">Deferred</xsl:text>
      </xsl:when>
      <xsl:when test="normalize-space($value) = 'WK' ">
        <xsl:text disable-output-escaping="yes">Weekly</xsl:text>
      </xsl:when>
      <xsl:when test="normalize-space($value) = 'BW' ">
        <xsl:text disable-output-escaping="yes">Biweekly</xsl:text>
      </xsl:when>
      <xsl:when test="normalize-space($value) = 'SM' ">
        <xsl:text disable-output-escaping="yes">Semimonthly</xsl:text>
      </xsl:when>
      <xsl:when test="normalize-space($value) = 'MO' ">
        <xsl:text disable-output-escaping="yes">Monthly</xsl:text>
      </xsl:when>
      <xsl:when test="normalize-space($value) = 'QT' ">
        <xsl:text disable-output-escaping="yes">Quarterly</xsl:text>
      </xsl:when>
      <xsl:when test="normalize-space($value) = 'TR' ">
        <xsl:text disable-output-escaping="yes">Triennially</xsl:text>
      </xsl:when>
      <xsl:when test="normalize-space($value) = 'SA' ">
        <xsl:text disable-output-escaping="yes">Semiannually</xsl:text>
      </xsl:when>
      <xsl:when test="normalize-space($value) = 'AN' ">
        <xsl:text disable-output-escaping="yes">Annually</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text disable-output-escaping="yes"></xsl:text>
      </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>


  <!--
  ********************************************************
  * Row loop for Payment History
  ********************************************************
  -->
  <xsl:template name="PaymentHistoryRows">
    <xsl:param name="row" select="'1'" />
    <xsl:param name="totalRows" select="'3'"  />
    <xsl:param name="historyChars" select="''"  />

    <tr>
      <td>
        <table border="0" cellpadding="0" cellspacing="0">
          <tr>
            <xsl:call-template name="PaymentHistoryCols">
              <xsl:with-param name="col" select="'1'"/>
              <xsl:with-param name="totalCols" select="'12'"/>
              <xsl:with-param name="historyChars" select="substring(normalize-space($historyChars), (($row - 1) * 12 + 1), 12)"/>
            </xsl:call-template>
          </tr>
        </table>
      </td>
    </tr>

    <xsl:if test="$row &lt; $totalRows">
      <!-- white space between rows -->
      <tr>
        <td style="line-height:2px;">&#160;</td>
      </tr>

      <xsl:call-template name="PaymentHistoryRows">
        <xsl:with-param name="row" select="$row + 1"/>
        <xsl:with-param name="totalRows" select="$totalRows"/>
        <xsl:with-param name="historyChars" select="normalize-space($historyChars)"/>
      </xsl:call-template>
    </xsl:if>

  </xsl:template>
  
  
  <!--
  ********************************************************
  * Column loop for Payment History
  ********************************************************
  -->
  <xsl:template name="PaymentHistoryCols">
    <xsl:param name="col" select="'1'" />
    <xsl:param name="totalCols" select="'12'"  />
    <xsl:param name="historyChars" select="''"  />

    <td><img height="15" width="15" broder="0" src="../images/box_{translate(substring(normalize-space($historyChars),$col , 1), '_', '')}.gif" /><img height="2" width="3" broder="0" src="../images/spacer.gif" /></td>

    <xsl:if test="$col &lt; $totalCols">
      <xsl:call-template name="PaymentHistoryCols">
        <xsl:with-param name="col" select="$col + 1"/>
        <xsl:with-param name="totalCols" select="$totalCols"/>
        <xsl:with-param name="historyChars" select="normalize-space($historyChars)"/>
      </xsl:call-template>
    </xsl:if>

  </xsl:template>
  
</xsl:stylesheet>