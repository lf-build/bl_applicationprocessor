
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />

  <!--
  *********************************************
  * SBCS Score template
  *********************************************
  -->
  <xsl:template name="SBCSScorable">

    <xsl:variable name="scoreBarWidth">
      <xsl:value-of select="'300'" />
    </xsl:variable>
    
    <xsl:variable name="pointerTableWidth">
      <xsl:value-of select="'308'" />
    </xsl:variable>
  
    <xsl:variable name="pointerWidth">
      <xsl:value-of select="'9'" />
    </xsl:variable>
    
    <xsl:variable name="action">
      <xsl:choose>
        <xsl:when test="prd:IntelliscoreScoreInformation/prd:Action">
          <xsl:value-of select="prd:IntelliscoreScoreInformation/prd:Action" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="score">
      <xsl:value-of select="(prd:IntelliscoreScoreInformation/prd:ScoreInfo/prd:Score div 100)" />
    </xsl:variable>

    <xsl:variable name="scoreText">
      <xsl:choose>
        <xsl:when test="contains($score, '.')">
          <xsl:value-of select="normalize-space(substring-before($score, '.'))" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="$score" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="scoreDisplay">
      <xsl:choose>
        <xsl:when test="number($scoreText) = 998 or number($scoreText) = 999">
          <xsl:value-of select="'N/A'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="$scoreText" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="segmentUsed">
      <xsl:value-of select="//prd:IntelliscoreScoreInformation/prd:Filler" />
    </xsl:variable>

    <xsl:variable name="isExclusion">
      <xsl:choose>
        <xsl:when test="number($scoreText) = 998  or normalize-space($segmentUsed) = 'BK' or number($scoreText) = 999  or number($segmentUsed) = 1">
          <xsl:value-of select="1" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="0" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="hasScore">
      <xsl:choose>
        <xsl:when test="prd:IntelliscoreScoreInformation/prd:ScoreInfo/prd:Score">
          <xsl:value-of select="1" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="0" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="pointerCenter">
      <xsl:value-of select="round($score div 100 * $scoreBarWidth)" />
    </xsl:variable>
    
    <xsl:variable name="pointerLeft">
      <xsl:value-of select="$pointerCenter" />
    </xsl:variable>

    <xsl:variable name="boxTitle">
      <xsl:choose>
        <xsl:when test="number($scoreText) = 998 or normalize-space($segmentUsed) = 'BK'">
          <xsl:value-of select="'Bankruptcy'" />
        </xsl:when>
        <xsl:when test="number($scoreText) = 999 or number($segmentUsed) = 1">
          <xsl:value-of select="'Score Exclusion'" />
        </xsl:when>
        <xsl:when test="number($segmentUsed) = 6">
          <xsl:value-of select="'Commercial Model with Financial Trade'" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'Commercial Model without Financial Trade'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="scoreBasedText">
      <xsl:choose>
       <xsl:when test="boolean(number($isExclusion))">
          <xsl:value-of select="''" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'Based on business credit'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="percentRanking">
      <xsl:choose>                  
        <xsl:when test="prd:IntelliscoreScoreInformation/prd:PercentileRanking">                     
          <xsl:value-of select="format-number(prd:IntelliscoreScoreInformation/prd:PercentileRanking div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="#015CAE">
        
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#ffffff">
                <!-- header banner -->
                <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                  <tr height="20">
                    <td bgcolor="#015CAE" align="center" valign="middle">
                      <font color="#ffffff"><b><xsl:value-of select="$boxTitle" /></b></font>
                    </td>
                  </tr>  

                  <tr>
                    <td style="line-height:10px;" valign="bottom">&#160;</td>
                  </tr>  

                  <tr>
                    <td align="center">
                      <font color="#015CAE"><b>
                        <xsl:choose>
                          <xsl:when test="number($scoreText) = 998">
                            This is an exclusion report (998) due to a bankruptcy reported within the last 24 months on the
                            <br/>inquired business. Therefore an SBCS Financial Score cannot be calculated.
                          </xsl:when>
        
                          <xsl:when test="number($scoreText) = 999">
                            This is an exclusion report (999). This business does not have the information necessary to
                            <br/>statistically <font color="#ff0000">predict</font> serious future delinquency. Therefore an SBCS
                            <br/>Financial Score cannot be calculated.
                          </xsl:when>

                          <xsl:otherwise>
                            The SBCS Financial Score predicts the likelihood of serious credit delinquency <font color="#ff0000">on a financial trade</font> within
                            <br/>the next 12 months based on business risk factors. Higher scores indicate lower risk.
                          </xsl:otherwise>
                        </xsl:choose>    

                     </b></font></td>
                  </tr>  

                  <tr>
                    <td style="line-height:15px;" valign="bottom">&#160;</td>
                  </tr>  

                </table>
                <!-- end header banner -->

                <xsl:if test="boolean(number($hasScore))" >

                  <!-- score text -->
                  <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td align="center">
                        <font size="4" color="000000"><b>SBCS SCORE: <xsl:value-of select="$scoreDisplay" /></b></font></td>
                    </tr>  
                    <tr>
                      <td style="line-height:3px;">&#160;</td>
                    </tr>
                  </table>  

                  <!-- Pointer gif -->
                  <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                    
                      <xsl:choose>
                        <xsl:when test="boolean(number($isExclusion))">
                          <td style="line-height:12px;" align="center">&#160;</td>
                        </xsl:when>
    
                        <xsl:otherwise>
                          <td align="center">
                            <table width="{$pointerTableWidth}" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                        
                                <xsl:if test="$pointerLeft &gt; 0" >
                                  <td width="{$pointerLeft}"><img src="../images/spacer.gif" border="0" width="{$pointerLeft}" height="1" /></td>
                                </xsl:if>

                                <td width="{$pointerTableWidth - $pointerLeft}" align="left" height="10" valign="middle"><img src="../images/triangle_black_down.gif" border="0" width="11" height="12" /></td>
                              </tr>
                            </table>    
                          </td>
                        
                        </xsl:otherwise>
                      </xsl:choose>    
                
                    </tr>  
                  </table>  
                  <!-- end Pointer down gif -->
                </xsl:if>

                <!-- color bar-->
                <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="center">
                      <table width="{$scoreBarWidth + 120}" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="60" align="right">
                            High risk <xsl:text disable-output-escaping="yes">&#160;</xsl:text>
                          </td>
                          
                          <td width="{$scoreBarWidth}">
                            <img src="../images/color_meter_round.gif" border="0" width="{$scoreBarWidth}" height="25" /></td>

                          <td width="60" align="left">
                            <xsl:text disable-output-escaping="yes">&#160;</xsl:text> Low risk 
                          </td>
                        </tr>
                      </table>    
                    </td>
                  </tr>  
                </table>  
                <!-- end color bar-->

                <!-- color bar labels -->
                <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="center">
                      <table width="{$scoreBarWidth}" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1" align="left">0</td>

                          <td width="{$scoreBarWidth - 2}" align="center"><font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$scoreBasedText" /></font></td>

                          <td width="1" align="right">100</td>
                        </tr>
                      </table>
                    </td>
                  </tr>  
                </table>    
                <!-- end color bar labels -->
                        
                <xsl:choose>
                  <xsl:when test="boolean(number($isExclusion))">
                    <!-- Exclusion Score info -->
                    <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td style="line-height:15px;" valign="bottom">&#160;</td>
                      </tr>  
                      
                      <tr>
                        <td align="center">
                          <table width="{$scoreBarWidth + 120}" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td align="left">
                              </td>
                            </tr>
                            <tr>
                              <td align="left">
                              </td>
                            </tr>
                          </table>    
                        </td>
                      </tr>  
                      
                      <tr>
                        <td style="line-height:10px;" valign="bottom">&#160;</td>
                      </tr>  
                    </table>  
                    <!-- end Exclusion Score info -->
                  </xsl:when>

                  <xsl:otherwise>
                    <!-- score factors -->
                    <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td style="line-height:15px;" colspan="3" valign="bottom">&#160;</td>
                      </tr>  

                      <tr>
                        <td width="2%">
                          <img src="../images/spacer.gif" border="0" width="5" height="1" /></td>
                        <td width="96%">  
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>  
                              <td width="48%" valign="top">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td colspan="2"><font color="#015CAE"><b>Factors Lowering the Score</b></font></td>
                                  </tr>
                                  <tr>
                                    <td style="line-height:1px;" colspan="2">&#160;</td>
                                  </tr>      
                                  
                                  <xsl:apply-templates select="prd:ScoreFactors/prd:ScoreFactor" />                                
                                </table>  
                              </td>
                              
                              <td width="4%"><xsl:text disable-output-escaping="yes">&#160;</xsl:text></td>
                              
                              <td width="48%" valign="top">
                                <font color="#015CAE"><b>All Industry Risk Comparison</b></font>
                                <br/>
                                When compared to all businesses, 
                                <xsl:value-of select="$percentRanking" /> of businesses indicate 
                                a higher likelihood of severe delinquency than this business.

                              </td>
                            </tr>
                            <tr>
                              <td colspan="3" style="line-height:3px;">&#160;</td>
                            </tr>
                          </table> 
                        </td>
                        <td width="2%" align="left">
                          <img src="../images/spacer.gif" border="0" width="3" height="1" /></td>
                      </tr>    
                    </table>
                  
                    <xsl:if test="normalize-space($action) != ''">
                      <!-- action code -->
                      <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
                      
                        <tr>
                          <td style="line-height:15px;" valign="bottom">&#160;</td>
                        </tr>  

                        <tr>
                          <td align="center">
                          
                          
                            <table width="95%" border="0" cellspacing="0" cellpadding="1">
                              <tr>
                                <td bgcolor="#015CAE">
                              
                                  <table width="100%" border="0" cellspacing="0" cellpadding="5">
                                    <tr>
                                      <td align="center" bgcolor="#ffffff">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                            <td align="center" >
                                              <font color="#015CAE">
                                              <b>Based on your company's action or risk threshold, this business
                                              falls within the following category:</b></font>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td style="line-height:3px;">&#160;</td>
                                          </tr>
                                          <tr>
                                            <td align="center" >
                                              <b><font color="#ff0000" size="3"><xsl:value-of select="$action" /></font></b>
                                            </td>
                                          </tr>
                                        </table>
                                      </td>
                                    </tr>     
                                  </table> 
                           
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>  
                        
                        <tr>
                          <td style="line-height:10px;" valign="bottom">&#160;</td>
                        </tr>  
                      </table>
                      <!-- end action code -->
                      
                    </xsl:if>
                  </xsl:otherwise>
                </xsl:choose>    
                        
                        
                        
              </td>
            </tr>     
          </table> 
  
        </td>
      </tr>
    </table>
    
  </xsl:template>


  <!--
  *********************************************
  * ScoreFactor template
  *********************************************
  -->
  <xsl:template match="prd:ScoreFactor" >
    <!-- Only need 4 factors -->
    <xsl:if test="position() &lt; 5">
      <xsl:if test="position() &gt; 1">
        <tr>
          <td style="line-height:3px;" colspan="2">&#160;</td>
        </tr>                                      
      </xsl:if>

        <tr>
          <td width="1%" align="left" valign="top"><img src="../images/ball_bullet_small_green.gif" border="0" /></td>
          <td width="99%" valign="bottom"><font size="1"><xsl:value-of select="." /></font></td>
        </tr>
    </xsl:if>
  </xsl:template>
  
</xsl:stylesheet>
