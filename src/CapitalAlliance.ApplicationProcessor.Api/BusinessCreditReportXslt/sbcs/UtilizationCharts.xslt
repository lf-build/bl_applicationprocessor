
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rsp="http://www.experian.com/NetConnectResponse"
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * SBCSKeyMetrics template
  *********************************************
  -->
  <xsl:template match="prd:SBCSKeyMetrics" >

    <xsl:if test="prd:FinancialAccounts and sum(prd:FinancialAccounts/prd:PercentageUsed) &gt; 0">
      <!-- FinancialTrades template -->
      <xsl:apply-templates select="prd:FinancialTrades">
        <xsl:with-param name="color" select="$borderColor" />
      </xsl:apply-templates>

      <xsl:if test="(prd:RevolvingAccounts and sum(prd:RevolvingAccounts/prd:PercentageUsed) &gt; 0) or (prd:AllAccountsTotal and sum(prd:AllAccountsTotal/prd:PercentageUsed) &gt; 0) or prd:PaymentStatus/prd:Status or prd:DBTHistory/prd:DBT">
        <br />
      </xsl:if>
    </xsl:if>
    
    <xsl:if test="prd:RevolvingAccounts and sum(prd:RevolvingAccounts/prd:PercentageUsed) &gt; 0">
      <!-- RevolvingTrades template -->
      <xsl:apply-templates select="prd:RevolvingTrades">
        <xsl:with-param name="color" select="$borderColor" />
      </xsl:apply-templates>

      <xsl:if test="(prd:AllAccountsTotal and sum(prd:AllAccountsTotal/prd:PercentageUsed) &gt; 0) or prd:PaymentStatus/prd:Status or prd:DBTHistory/prd:DBT">
        <br />
      </xsl:if>
    </xsl:if>
    
    <xsl:if test="prd:PaymentStatus/prd:Status">
      <!-- PaymentStatus template -->
      <xsl:call-template name="PaymentStatus36Months">
        <xsl:with-param name="color" select="$borderColor" />
      </xsl:call-template>

      <xsl:if test="(prd:AllAccountsTotal and sum(prd:AllAccountsTotal/prd:PercentageUsed) &gt; 0) or prd:DBTHistory/prd:DBT">
        <br />
      </xsl:if>
    </xsl:if>
    
    <xsl:if test="prd:AllAccountsTotal and sum(prd:AllAccountsTotal/prd:PercentageUsed) &gt; 0">
      <!-- AllTrades template -->
      <xsl:apply-templates select="prd:AllTrades">
        <xsl:with-param name="color" select="$borderColor" />
      </xsl:apply-templates>

      <xsl:if test="prd:DBTHistory/prd:DBT">
        <br />
      </xsl:if>
    </xsl:if>
    
    <xsl:if test="prd:DBTHistory/prd:DBT">
      <!-- DBT template -->
      <xsl:call-template name="DBT36Months">
        <xsl:with-param name="color" select="$borderColor" />
      </xsl:call-template>

    </xsl:if>
    
  </xsl:template>
    
    
  <!--
  *********************************************
  * FinancialTrades template
  *********************************************
  -->
  <xsl:template match="prd:FinancialTrades" >
    <xsl:param name="color" select="'#0099cc'" />

    <xsl:variable name="current">
      <xsl:value-of select="number(prd:TotalAccountPercentageUsed)" />
    </xsl:variable>

    <xsl:variable name="compare">
      <xsl:variable name="previous">
        <xsl:value-of select="number(../prd:FinancialAccounts[1]/prd:PercentageUsed)" />
      </xsl:variable>

      <xsl:choose>                  
        <xsl:when test="$current &gt; $previous">
          <xsl:value-of select="'up'" />
        </xsl:when>

        <xsl:when test="$current &lt; $previous">
          <xsl:value-of select="'down'" />
        </xsl:when>
  
        <xsl:otherwise>
          <xsl:value-of select="'equal'" />
        </xsl:otherwise>
      </xsl:choose>                
    </xsl:variable>

    <xsl:variable name="balance">
      <xsl:choose>                  
        <xsl:when test="prd:TotalCurrentBalance">                     
          <xsl:value-of select="format-number(prd:TotalCurrentBalance, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="limit">
      <xsl:choose>                  
        <xsl:when test="prd:TotalCurrentCreditLimitOLoanAmount">                     
          <xsl:value-of select="format-number(prd:TotalCurrentCreditLimitOLoanAmount, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <!-- blue box border -->
    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="{$color}">
    
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#ffffff">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                  <tr>
                    <td bgcolor="{$color}" colspan="3" align="left" valign="middle" height="20">
                      <b><font color="#ffffff"><xsl:text disable-output-escaping="yes">&#160;&#160;</xsl:text>Financial Account Utilization Trend - Previous 36 Months</font></b>
                    </td>
                  </tr>

                  <tr>
                    <td style="line-height:6px;" colspan="3" >&#160;</td>
                  </tr>

                  <tr>

                    <td align="right" width="35%">
                      <b>Current Financial Account Utilization =<xsl:text disable-output-escaping="yes">&#160;</xsl:text> </b>
                    </td>
                    
                    <td width="15%" align="left">
                      <table border="0" cellspacing="0" cellpadding="1">
                        <tr>
                          <td bgcolor="{$color}">
                            <table height="35" border="0" cellspacing="0" cellpadding="1">
                              <tr>
                                <td align="right" bgcolor="#ffffff">
                                  <font size="4"><xsl:text disable-output-escaping="yes">&#160;</xsl:text><b><xsl:value-of select="$current" />%</b></font></td>
                                <td align="left" bgcolor="#ffffff">
                                  <img src="../images/arrow_{normalize-space($compare)}.gif" /></td>
                                <td align="left" bgcolor="#ffffff">
                                  <xsl:text disable-output-escaping="yes">&#160;</xsl:text></td>
                              </tr>
                            </table>
                          </td>  
                        </tr>
                      </table>
                    </td>
                    
                    <td width="50%">
                      <b>Current Outstanding Balance = </b>  
                      <xsl:value-of select="$balance" />
                      <br />
                      <b>Original Loan Amount/Limit = </b>  
                      <xsl:value-of select="$limit" />
                    </td>
                    
                  </tr>
                  
                  <tr>
                    <td colspan="3">
                      <img src="{normalize-space($financialChartPath)}" width="685" height="95" alt="Financial Account Utilization Trend" />
                    </td>
                  </tr>

                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

  </xsl:template>
    

  <!--
  *********************************************
  * RevolvingTrades template
  *********************************************
  -->
  <xsl:template match="prd:RevolvingTrades" >
    <xsl:param name="color" select="'#0099cc'" />

    <xsl:variable name="current">
      <xsl:value-of select="number(prd:CurrentPercentageUsed)" />
    </xsl:variable>

    <xsl:variable name="compare">
      <xsl:variable name="previous">
        <xsl:value-of select="number(../prd:RevolvingAccounts[1]/prd:PercentageUsed)" />
      </xsl:variable>

      <xsl:choose>                  
        <xsl:when test="$current &gt; $previous">
          <xsl:value-of select="'up'" />
        </xsl:when>

        <xsl:when test="$current &lt; $previous">
          <xsl:value-of select="'down'" />
        </xsl:when>
  
        <xsl:otherwise>
          <xsl:value-of select="'equal'" />
        </xsl:otherwise>
      </xsl:choose>                
    </xsl:variable>

    <xsl:variable name="balance">
      <xsl:choose>                  
        <xsl:when test="prd:TotalCurrentBalance">                     
          <xsl:value-of select="format-number(prd:TotalCurrentBalance, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="limit">
      <xsl:choose>                  
        <xsl:when test="prd:TotalCurrentCreditLimitOrLoanAmount">                     
          <xsl:value-of select="format-number(prd:TotalCurrentCreditLimitOrLoanAmount, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <!-- blue box border -->
    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="{$color}">
    
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#ffffff">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                  <tr>
                    <td bgcolor="{$color}" colspan="3" align="left" valign="middle" height="20">
                      <b><font color="#ffffff"><xsl:text disable-output-escaping="yes">&#160;&#160;</xsl:text>Revolving Account Utilization Trend - Previous 36 Months</font></b>
                    </td>
                  </tr>

                  <tr>
                    <td style="line-height:6px;" colspan="3" >&#160;</td>
                  </tr>

                  <tr>

                    <td align="right" width="35%">
                      <b>Current Revolving Account Utilization =<xsl:text disable-output-escaping="yes">&#160;</xsl:text> </b>
                    </td>
                    
                    <td width="15%" align="left">
                      <table border="0" cellspacing="0" cellpadding="1">
                        <tr>
                          <td bgcolor="{$color}">
                            <table height="35" border="0" cellspacing="0" cellpadding="1">
                              <tr>
                                <td align="right" bgcolor="#ffffff">
                                  <font size="4"><xsl:text disable-output-escaping="yes">&#160;</xsl:text><b><xsl:value-of select="$current" />%</b></font></td>
                                <td align="left" bgcolor="#ffffff">
                                  <img src="../images/arrow_{normalize-space($compare)}.gif" /></td>
                                <td align="left" bgcolor="#ffffff">
                                  <xsl:text disable-output-escaping="yes">&#160;</xsl:text></td>
                              </tr>
                            </table>
                          </td>  
                        </tr>
                      </table>
                    </td>
                    
                    <td width="50%">
                      <b>Current Outstanding Balance = </b>  
                      <xsl:value-of select="$balance" />
                      <br />
                      <b>High Credit/Limit = </b>  
                      <xsl:value-of select="$limit" />
                    </td>
                    
                  </tr>
                  
                  <tr>
                    <td colspan="3">
                      <img src="{normalize-space($revolvingChartPath)}" width="685" height="95" alt="Revolving Account Utilization Trend" />
                    </td>
                  </tr>

                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

  </xsl:template>
    
  
  <!--
  *********************************************
  * PaymentStatus36Months template
  *********************************************
  -->
  <xsl:template name="PaymentStatus36Months">
    <xsl:param name="color" select="'#0099cc'" />

    <!-- blue box border -->
    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="{$color}">
    
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#ffffff" align="center">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td bgcolor="{$color}" colspan="3" align="left" valign="middle" height="20">
                      <b><font color="#ffffff"><xsl:text disable-output-escaping="yes">&#160;&#160;</xsl:text>Payment Performance - Previous 36 Months</font></b>
                    </td>
                  </tr>
                </table>

                <table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td style="line-height:10px;" colspan="37">&#160;</td>
                  </tr>

                  <tr>
                    <td width="2">
                      <img src="../images/spacer.gif" width="2" height="1" /></td>

                    <xsl:apply-templates select="prd:PaymentStatus/prd:Status">
                      <xsl:with-param name="currentDate" select="prd:CurrentDate" />
                      <xsl:sort select="position()" data-type="number" order="descending"/>                      
                    </xsl:apply-templates>

                  </tr>
                  
                </table>

                <table width="100%" border="0" cellspacing="2" cellpadding="0">
                  <tr>
                    <td style="line-height:12px;" colspan="11">&#160;</td>
                  </tr>


                  <tr>
                    <td width="1%" rowspan="3">
                      <img src="../images/spacer.gif" width="4" height="1" /></td>
    
                    <td width="2%" valign="top">
                      <img src="../images/box_C.gif" /></td>
                    <td width="18%" valign="top" align="left">
                      <font size="1" style="FONT-FAMILY: 'verdana';">Current</font></td>
                      
                    <td width="2%" valign="top">
                      <img src="../images/box_0.gif" /></td>
                    <td width="18%" valign="top" align="left">
                      <font size="1" style="FONT-FAMILY: 'verdana';">Current, zero balance</font></td>
                      
                    <td width="2%" valign="top">
                      <img src="../images/box_1.gif" /></td>
                    <td width="18%" valign="top" align="left">
                      <font size="1" style="FONT-FAMILY: 'verdana';">1-30 days late</font></td>
                      
                    <td width="2%" valign="top">
                      <img src="../images/box_2.gif" /></td>
                    <td width="18%" valign="top" align="left">
                      <font size="1" style="FONT-FAMILY: 'verdana';">31-60 days late</font></td>
                      
                    <td width="2%" valign="top">
                      <img src="../images/box_3.gif" /></td>
                    <td width="17%" valign="top" align="left">
                      <font size="1" style="FONT-FAMILY: 'verdana';">61-90 days late</font></td>
                      
                  </tr>
                  
                  <tr>
                    <td valign="top">
                      <img src="../images/box_4.gif" /></td>
                    <td valign="top" align="left">
                      <font size="1" style="FONT-FAMILY: 'verdana';">91-120 days late</font></td>
                      
                    <td valign="top">
                      <img src="../images/box_5.gif" /></td>
                    <td valign="top" align="left">
                      <font size="1" style="FONT-FAMILY: 'verdana';">121+ days late</font></td>
                      
                    <td valign="top">
                      <img src="../images/box_B.gif" /></td>
                    <td valign="top" align="left">
                      <font size="1" style="FONT-FAMILY: 'verdana';">Bankruptcy</font></td>
                      
                    <td valign="top">
                      <img src="../images/box_G.gif" /></td>
                    <td valign="top" align="left">
                      <font size="1" style="FONT-FAMILY: 'verdana';">Collection</font></td>
                      
                    <td valign="top">
                      <img src="../images/box_H.gif" /></td>
                    <td valign="top" align="left">
                      <font size="1" style="FONT-FAMILY: 'verdana';">Foreclosure</font></td>
                      
                  </tr>

                  <tr>
                    <td valign="top">
                      <img src="../images/box_J.gif" /></td>
                    <td valign="top" align="left">
                      <font size="1" style="FONT-FAMILY: 'verdana';">Voluntary Surrender</font></td>
                      
                    <td valign="top">
                      <img src="../images/box_K.gif" /></td>
                    <td valign="top" align="left">
                      <font size="1" style="FONT-FAMILY: 'verdana';">Repossession</font></td>
                      
                    <td valign="top">
                      <img src="../images/box_L.gif" /></td>
                    <td valign="top" align="left">
                      <font size="1" style="FONT-FAMILY: 'verdana';">Uncollected/Charge-off</font></td>
                      
                    <td valign="top">
                      <img src="../images/box_-.gif" /></td>
                    <td valign="top" align="left">
                      <font size="1" style="FONT-FAMILY: 'verdana';">No data reported</font></td>
                      
                    <td valign="top">
                      <img src="../images/box_.gif" /></td>
                    <td valign="top" align="left">
                      <font size="1" style="FONT-FAMILY: 'verdana';">No prior payment history</font></td>
                      
                  </tr>
                  
                  <tr>
                    <td style="line-height:2px;" colspan="11">&#160;</td>
                  </tr>
                
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </xsl:template>


  <!--
  *********************************************
  * PaymentStatus template
  *********************************************
  -->
  <xsl:template match="prd:Status" >
    <xsl:param name="currentDate" select="''" />

    <xsl:variable name="currentYear">
      <xsl:value-of select="number(substring($currentDate, 1, 4))" />
    </xsl:variable>
        
    <xsl:variable name="currentMonth">
      <xsl:value-of select="number(substring($currentDate, 5, 2))" />
    </xsl:variable>

    <xsl:variable name="newDate">
      <xsl:choose>		              
        <xsl:when test="$currentMonth - (last() - (position() - 1)) &gt; 0">
          <xsl:variable name="newMonth">
            <xsl:call-template name="FormatMonth">
              <xsl:with-param name="monthValue" select="$currentMonth - (last() - (position() - 1))" />
            </xsl:call-template>
          </xsl:variable>  

          <xsl:variable name="newYear">
            <xsl:value-of select="substring(normalize-space($currentYear), 3, 2)" />
          </xsl:variable>  

          <xsl:value-of select="concat(substring(normalize-space($newMonth), 1, 1), normalize-space($newYear))" />
        </xsl:when>
  
        <xsl:when test="$currentMonth - (last() - (position() - 1)) = 0">
          <xsl:variable name="newMonth">
            <xsl:call-template name="FormatMonth">
              <xsl:with-param name="monthValue" select="$currentMonth - (last() - (position() - 1)) + 12" />
            </xsl:call-template>
          </xsl:variable>  

          <xsl:variable name="newYear">
            <xsl:value-of select="substring(($currentYear - 1), 3, 2)" />
          </xsl:variable>  

          <xsl:value-of select="concat(substring(normalize-space($newMonth), 1, 1), normalize-space($newYear))" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:variable name="monthDiff">
            <xsl:value-of select="($currentMonth - (last() - (position() - 1))) * (-1)" />
          </xsl:variable>  
          
          <xsl:variable name="newMonth">
            <xsl:call-template name="FormatMonth">
              <xsl:with-param name="monthValue" select="12 - ($monthDiff mod 12)" />
            </xsl:call-template>
          </xsl:variable>  

          <xsl:variable name="newYear">
            <xsl:value-of select="substring(($currentYear - (floor($monthDiff div 12) + 1)), 3, 2)" />
          </xsl:variable>  

          <xsl:value-of select="concat(substring(normalize-space($newMonth), 1, 1), normalize-space($newYear))" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <td align="center" width="19"><img src="../images/box_{normalize-space(./@code)}.gif" /><br/><font size="0" style="FONT-FAMILY: 'Arial'; FONT-SIZE:9px"><xsl:value-of select="$newDate" /></font></td>
  </xsl:template>  
  
  
  <!--
  *********************************************
  * AllTrades template
  *********************************************
  -->
  <xsl:template match="prd:AllTrades" >
    <xsl:param name="color" select="'#0099cc'" />

    <xsl:variable name="current">
      <xsl:value-of select="number(prd:TotalAccountPercentageUsed)" />
    </xsl:variable>

    <xsl:variable name="compare">
      <xsl:variable name="previous">
        <xsl:value-of select="number(../prd:AllAccountsTotal[1]/prd:PercentageUsed)" />
      </xsl:variable>

      <xsl:choose>                  
        <xsl:when test="$current &gt; $previous">
          <xsl:value-of select="'up'" />
        </xsl:when>

        <xsl:when test="$current &lt; $previous">
          <xsl:value-of select="'down'" />
        </xsl:when>
  
        <xsl:otherwise>
          <xsl:value-of select="'equal'" />
        </xsl:otherwise>
      </xsl:choose>                
    </xsl:variable>

    <xsl:variable name="balance">
      <xsl:choose>                  
        <xsl:when test="prd:TotalCurrentBalance">                     
          <xsl:value-of select="format-number(prd:TotalCurrentBalance, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="limit">
      <xsl:choose>                  
        <xsl:when test="prd:TotalCurrentCreditLimitOrLoanAmount">                     
          <xsl:value-of select="format-number(prd:TotalCurrentCreditLimitOrLoanAmount, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <!-- blue box border -->
    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="{$color}">
    
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#ffffff">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                  <tr>
                    <td bgcolor="{$color}" colspan="3" align="left" valign="middle" height="20">
                      <b><font color="#ffffff"><xsl:text disable-output-escaping="yes">&#160;&#160;</xsl:text>Total Account Utilization Trend - Previous 36 Months</font></b>
                    </td>
                  </tr>

                  <tr>
                    <td style="line-height:6px;" colspan="3" >&#160;</td>
                  </tr>

                  <tr>

                    <td align="right" width="35%">
                      <b>Current Total Credit Utilization =<xsl:text disable-output-escaping="yes">&#160;</xsl:text> </b>
                    </td>
                    
                    <td width="15%" align="left">
                      <table border="0" cellspacing="0" cellpadding="1">
                        <tr>
                          <td bgcolor="{$color}">
                            <table height="35" border="0" cellspacing="0" cellpadding="1">
                              <tr>
                                <td align="right" bgcolor="#ffffff">
                                  <font size="4"><xsl:text disable-output-escaping="yes">&#160;</xsl:text><b><xsl:value-of select="$current" />%</b></font></td>
                                <td align="left" bgcolor="#ffffff">
                                  <img src="../images/arrow_{normalize-space($compare)}.gif" /></td>
                                <td align="left" bgcolor="#ffffff">
                                  <xsl:text disable-output-escaping="yes">&#160;</xsl:text></td>
                              </tr>
                            </table>
                          </td>  
                        </tr>
                      </table>
                    </td>
                    
                    <td width="50%">
                      <b>Current Outstanding Balance = </b>  
                      <xsl:value-of select="$balance" />
                      <br />
                      <b>Original Loan Amount/Limit = </b>  
                      <xsl:value-of select="$limit" />
                    </td>
                    
                  </tr>
                  
                  <tr>
                    <td colspan="3">
                      <img src="{normalize-space($totalChartPath)}" width="685" height="95" alt="Total Account Utilization Trend" />
                    </td>
                  </tr>

                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

  </xsl:template>


  <!--
  *********************************************
  * DBT36Months template
  *********************************************
  -->
  <xsl:template name="DBT36Months">
    <xsl:param name="color" select="'#0099cc'" />
  
    <!-- blue box border -->
    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="{$color}">
    
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#ffffff">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                  <tr>
                    <td bgcolor="{$color}" align="left" valign="middle" height="20">
                      <b><font color="#ffffff"><xsl:text disable-output-escaping="yes">&#160;&#160;</xsl:text>Average DBT Trend (Non-financial Accounts) - Previous 36 Months</font></b>
                    </td>
                  </tr>

                  <tr>
                    <td style="line-height:6px;">&#160;</td>
                  </tr>

                  <tr>
                    <td>
                      <img src="{normalize-space($dbt36MChartPath)}" width="685" height="95" alt="Average DBT Trend" />
                    </td>
                  </tr>

                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </xsl:template>
  
</xsl:stylesheet>