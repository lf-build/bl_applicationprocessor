
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:rsp="http://www.experian.com/NetConnectResponse"
  xmlns:prd="http://www.experian.com/ARFResponse">

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * Global variables and parameters
  *********************************************
  <xsl:param name="systemYear" select="'2006'" />
  -->
  <xsl:param name="product" select="'SBCSBPR'" />
  <xsl:param name="baseProduct" select="''" />

  <xsl:param name="financialChartPath" select="'../images/default_line_chart.gif'" />
  <xsl:param name="revolvingChartPath" select="'../images/default_line_chart.gif'" />
  <xsl:param name="totalChartPath" select="'../images/default_line_chart.gif'" />
  <xsl:param name="dbt36MChartPath" select="'../images/default_line_chart.gif'" />
  <xsl:param name="dbt7MChartPath" select="'../images/default_bar_chart.gif'" />
  <xsl:param name="dbt5QChartPath" select="'../images/default_bar_chart.gif'" />


  <!--
  *********************************************
  * include template
  *********************************************
  -->
  <xsl:include href="../common/ReportHeader.xslt" />
  <xsl:include href="../common/Util.xslt" />
  <xsl:include href="../common/case.xslt" />
  <xsl:include href="../common/BackToTop.xslt" />
  <xsl:include href="../common/ErrorProcessing.xslt" />
  <xsl:include href="../common/ReportFooter.xslt" />
  <xsl:include href="IdentifyingInformation.xslt" />
  <xsl:include href="SBCSScorable.xslt" />
  <xsl:include href="BusinessInfo.xslt" />
  <xsl:include href="../common/MatchingNameAndAddress.xslt" />
  <xsl:include href="../common/CorporateLinkage.xslt" />
  <xsl:include href="ExecutiveSummary.xslt" />
  <xsl:include href="FinancialAccountSummary.xslt" />
  <xsl:include href="TradeAccountSummary.xslt" />
  <xsl:include href="PublicRecord.xslt" />
  <xsl:include href="CollectionsDerogatory.xslt" />
  <xsl:include href="KeyMetrics.xslt" />
  <xsl:include href="UtilizationCharts.xslt" />
  <xsl:include href="CurrentDBT.xslt" />
  <xsl:include href="PerformanceAnalysis.xslt" />
  <xsl:include href="DBTCharts.xslt" />
  <xsl:include href="LegalFilingsCollections.xslt" />
  <xsl:include href="OwnerGuarantors.xslt" />
  <xsl:include href="CoDebtors.xslt" />
  <xsl:include href="Inquiries.xslt" />
  <xsl:include href="MonthlyPaymentTrends.xslt" />
  <xsl:include href="QuarterlyPaymentTrends.xslt" />
  <xsl:include href="AccountDetails.xslt" />
  <xsl:include href="UCCProfile.xslt" />
  <xsl:include href="CompanyBackground.xslt" />
  <xsl:include href="AdditionalCompanyBackground.xslt" />
  <xsl:include href="AdditionalBusinessIdentities.xslt" />
  <xsl:include href="../businessprofile/StandardAndPoors.xslt" />
  <xsl:include href="../Intelliscore/CustomFooterMessage.xslt" />

  <!--
  *********************************************
  * Initial template
  *********************************************
  -->  
  <xsl:template match="/">
    <html>
      <head>
        <title>SBCS Profile and Financial Score Report</title>

        <style type="text/css">
            td {font-size: 9pt; font-family: 'arial';}
        </style>
      </head>

      <body>
        <a name="top"></a>
        <table width="715" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td>
               
              <!-- SBCS template -->
              <xsl:apply-templates select="//prd:SmallBusinessCreditShare" />
              
            </td>
          </tr>
        </table>    
      </body>
    </html>
  </xsl:template>


  <!--
  *********************************************
  * SmallBusinessCreditShare template
  *********************************************
  -->
  <xsl:template match="prd:SmallBusinessCreditShare" >
    <!-- Report Date  -->
    <xsl:variable name="reportDate">
      <xsl:choose>
        <xsl:when test="prd:SBCSBusinessNameAndAddress/prd:ProfileDate">
          <xsl:value-of select="prd:SBCSBusinessNameAndAddress/prd:ProfileDate" />
        </xsl:when>
	
        <xsl:otherwise>
          <xsl:value-of select="prd:BusinessNameAndAddress/prd:ProfileDate" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="reportTime">
      <xsl:choose>
        <xsl:when test="prd:SBCSBusinessNameAndAddress/prd:ProfileTime">
          <xsl:value-of select="prd:SBCSBusinessNameAndAddress/prd:ProfileTime" />
        </xsl:when>
  
        <xsl:otherwise>
          <xsl:value-of select="prd:BusinessNameAndAddress/prd:ProfileTime" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <!-- Profile Type  -->
    <xsl:variable name="profileType">
    	<xsl:value-of select="'SBCS Profile and Financial Score'" />
    </xsl:variable>

    <!-- Report Header -->
    <xsl:call-template name="ReportHeader">
      <xsl:with-param name="reportType" select="$profileType" />
      <xsl:with-param name="reportName" select="prd:SBCSBusinessNameAndAddress/prd:BusinessName" />
      <xsl:with-param name="reportDate" select="$reportDate" />
      <xsl:with-param name="reportTime" select="$reportTime" />
    </xsl:call-template>

    <br />

    <xsl:choose>
      <xsl:when test="normalize-space(prd:SBCSBusinessNameAndAddress/prd:ProfileType/@code) = 'NO RECORD' or normalize-space(prd:BusinessNameAndAddress/prd:ProfileType/@code) = 'NO RECORD'">
        <xsl:call-template name="businessNotFound" />
      </xsl:when>

      <xsl:otherwise>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top" height="20" align="center"><font size="4" color="{$titleColor}"><b><a name="SBCSScore">Small Business Credit Share Financial Score<font size="3"></font></a></b></font></td>
          </tr>
        </table>

  	    <!-- Identifying Information -->
  	    <xsl:call-template name="IdentifyingInformation" />
  
          <br />
  
          <xsl:if test="prd:SBCSBusinessNameAndAddress/prd:MatchingNameAndAddress">
            <!-- Matching Name And Address -->
            <xsl:apply-templates select="prd:SBCSBusinessNameAndAddress/prd:MatchingNameAndAddress" />
  
            <br />
          </xsl:if>
  
	    <xsl:if test="prd:CorporateLinkage">
	      <!-- Corporate Linkage -->
	      <xsl:call-template name="CorporateLinkage" />
	    </xsl:if>

          <!-- SBCS Score template -->
          <xsl:call-template name="SBCSScorable" />

          <br />

          <xsl:if test="prd:SBCSExecutiveElements and prd:IntelliscoreScoreInformation/prd:ModelInformation/prd:ModelCode and number(prd:IntelliscoreScoreInformation/prd:ModelInformation/prd:ModelCode) != 220">
            <!-- Business Infomation -->
            <xsl:call-template name="BusinessInformation" />
  
            <!-- back to top graphic -->
            <xsl:call-template name="BackToTop" />
          </xsl:if>

          <xsl:if test="prd:SBCSExecutiveElements or prd:SBCSFinancialSummary or prd:SBCSPaymentTotals">
            <!-- SBCS Executive Summary -->
            <xsl:call-template name="ExecutiveSummary" />
  
            <!-- back to top graphic -->
            <xsl:call-template name="BackToTop" />
          </xsl:if>
          
          <xsl:if test="prd:SBCSKeyMetrics or prd:SBCSExecutiveElements">
            <!-- SBCS Key Metrics -->
            <xsl:call-template name="KeyMetrics" />
  
            <!-- back to top graphic -->
            <xsl:call-template name="BackToTop" />
          </xsl:if>
  
          <xsl:if test="prd:CollectionData or prd:SBCSBankruptcy or prd:SBCSTaxLien or prd:SBCSJudgment">
            <!-- LegalFilingsCollections -->
            <xsl:call-template name="LegalFilingsCollections" />
  
            <!-- back to top graphic -->
            <xsl:call-template name="BackToTop" />
          </xsl:if>
  
          <xsl:if test="prd:IndividualPersonalGuarantor">
            <!-- Owner Guarantors 2110 -->
            <xsl:call-template name="OwnerGuarantors">
              <xsl:with-param name="title" select="'Individual Guarantor Details'" />
              <xsl:with-param name="node" select="prd:IndividualPersonalGuarantor" />
              <xsl:with-param name="color" select="$borderColor" />
            </xsl:call-template>
  
            <!-- back to top graphic -->
            <xsl:call-template name="BackToTop" />
          </xsl:if>
  
          <xsl:if test="prd:CompanyGuarantor">
            <!-- Owner Guarantors 2120 -->
            <xsl:call-template name="OwnerGuarantors">
              <xsl:with-param name="title" select="'Company Guarantor Details'" />
              <xsl:with-param name="node" select="prd:CompanyGuarantor" />
              <xsl:with-param name="color" select="$borderColor" />
            </xsl:call-template>
  
            <!-- back to top graphic -->
            <xsl:call-template name="BackToTop" />
          </xsl:if>
  
          <xsl:if test="prd:IndividualOwnerPrincipal">
            <!-- Owner Guarantors 2130 -->
            <xsl:call-template name="OwnerGuarantors">
              <xsl:with-param name="title" select="'Individual Owner Details'" />
              <xsl:with-param name="node" select="prd:IndividualOwnerPrincipal" />
              <xsl:with-param name="color" select="$borderColor" />
            </xsl:call-template>
  
            <!-- back to top graphic -->
            <xsl:call-template name="BackToTop" />
          </xsl:if>
  
          <xsl:if test="prd:CompanyOwnerPrincipal">
            <!-- Owner Guarantors 2140 -->
            <xsl:call-template name="OwnerGuarantors">
              <xsl:with-param name="title" select="'Company Owner Details'" />
              <xsl:with-param name="node" select="prd:CompanyOwnerPrincipal" />
              <xsl:with-param name="color" select="$borderColor" />
            </xsl:call-template>
  
            <!-- back to top graphic -->
            <xsl:call-template name="BackToTop" />
          </xsl:if>
  
          <xsl:if test="prd:UCCCoDebtor">
            <!-- CoDebtors -->
            <xsl:call-template name="CoDebtors">
                <xsl:with-param name="color" select="$borderColor" />
            </xsl:call-template>
  
            <!-- back to top graphic -->
            <xsl:call-template name="BackToTop" />
          </xsl:if>
  
          <xsl:if test="prd:SBCSInquiry">
            <!-- Inquiries -->
            <xsl:call-template name="Inquiries">
                <xsl:with-param name="color" select="$borderColor" />
            </xsl:call-template>

            <!-- back to top graphic -->
            <xsl:call-template name="BackToTop" />
          </xsl:if>
  
          <xsl:if test="prd:SBCSPaymentTrends">
            <!-- MonthlyPaymentTrends -->
            <xsl:call-template name="MonthlyPaymentTrends">
                  <xsl:with-param name="color" select="$borderColor" />
            </xsl:call-template>

            <!-- back to top graphic -->
            <xsl:call-template name="BackToTop" />
          </xsl:if>
  
          <xsl:if test="prd:SBCSQuarterlyPaymentTrends">
            <!-- QuarterlyPaymentTrends -->
            <xsl:call-template name="QuarterlyPaymentTrends">
                  <xsl:with-param name="color" select="$borderColor" />
            </xsl:call-template>
  
            <!-- back to top graphic -->
            <xsl:call-template name="BackToTop" />
          </xsl:if>
  
          <xsl:if test="prd:SBCSTradeData">
            <!-- AccountDetails -->
            <xsl:call-template name="AccountDetails" />
  
            <!-- back to top graphic -->
            <xsl:call-template name="BackToTop" />
          </xsl:if>
  
          <xsl:if test="prd:UCCFilingsSummaryCounts or prd:SBCSUCCFilings">
            <!-- UCCProfile -->
            <xsl:call-template name="UCCProfile" />
  
            <!-- back to top graphic -->
            <xsl:call-template name="BackToTop" />
          </xsl:if>
  
          <xsl:if test="prd:CorporateInformation or prd:CorporateOwnerInformation or prd:CorporateLinkageSummary or prd:DemographicInformation or prd:CorporateLinkageNameAndAddress or prd:KeyPersonnelExecutiveInformation">
            <!-- CompanyBackground -->
            <xsl:call-template name="CompanyBackground" />
  
            <!-- back to top graphic -->
            <xsl:call-template name="BackToTop" />
          </xsl:if>
  
          <xsl:if test="prd:AdditionalBusinessIdentity">
            <!-- AdditionalBusinessIdentities -->
            <xsl:call-template name="AdditionalBusinessIdentities">
                    <xsl:with-param name="color" select="$borderColor" />
            </xsl:call-template>

            <!-- back to top graphic -->
            <xsl:call-template name="BackToTop" />
          </xsl:if>
    
	    <xsl:if test="prd:StandardAndPoorsFinancialInformation">
	      <!-- StandardAndPoorsFinancialInformation -->
	      <xsl:call-template name="StandardAndPoors" />
            <xsl:call-template name="BackToTop" />
	    </xsl:if>

        <!-- custom footer message -->
        <xsl:call-template name="CustomFooterMessage" />

          <!-- is it a limited report?  -->
          <xsl:variable name="reportEnd">
            <xsl:choose>		              
              <xsl:when test="normalize-space(prd:BillingIndicator/@code) = 'A' ">
                <xsl:value-of select="'End of limited'" />
              </xsl:when>
          
              <xsl:otherwise>
                <xsl:value-of select="'End of report'" />
              </xsl:otherwise>
            </xsl:choose>    
          </xsl:variable>
  	
  	    <!-- Report Footer -->
  	    <xsl:call-template name="ReportFooter">
          <xsl:with-param name="reportType" select="'SBCS'" />
          <xsl:with-param name="reportDate" select="$reportDate" />
          <xsl:with-param name="reportEnd" select="$reportEnd" />
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>    

  </xsl:template>


  <!--
  *********************************************
  * this template overrides the built-in rule
  * do nothing with stuff I'm not interested in
  *********************************************
  -->
  <xsl:template match="text()|@*">
  </xsl:template>

</xsl:stylesheet>