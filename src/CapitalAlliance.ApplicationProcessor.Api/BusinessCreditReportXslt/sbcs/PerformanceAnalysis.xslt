
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rsp="http://www.experian.com/NetConnectResponse"
                xmlns:prd="http://www.experian.com/ARFResponse">

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />



  <!--
  *********************************************
  * PerformanceAnalysis template
  *********************************************
  -->
  <xsl:template name="PerformanceAnalysis">
    <xsl:param name="color" select="'#0099cc'" />

    <xsl:variable name="avg5QBalance">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveElements/prd:AverageBalance5Quarters">                    
          <xsl:value-of select="format-number(prd:SBCSExecutiveElements/prd:AverageBalance5Quarters, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
  
    <xsl:variable name="highestCredit">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveElements/prd:SingleLineHighCredit">                    
          <xsl:value-of select="format-number(prd:SBCSExecutiveElements/prd:SingleLineHighCredit, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="balanceRange">

      <xsl:variable name="lowBalance">
        <xsl:choose>                  
          <xsl:when test="prd:SBCSExecutiveElements/prd:LowBalance6Months">                    
            <xsl:value-of select="format-number(prd:SBCSExecutiveElements/prd:LowBalance6Months, '$###,###,##0')" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="'$0'" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>

      <xsl:variable name="highBalance">
        <xsl:choose>                  
          <xsl:when test="prd:SBCSExecutiveElements/prd:HighBalance6Months">                    
            <xsl:value-of select="format-number(prd:SBCSExecutiveElements/prd:HighBalance6Months, '$###,###,##0')" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="'$0'" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>

      <xsl:value-of select="concat($lowBalance, ' - ', $highBalance)" />
    </xsl:variable>

    <xsl:variable name="inquiries6M">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:InquiriesPrevious6Months">                    
          <xsl:value-of select="number(prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:InquiriesPrevious6Months)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="inquiries12M">
      <xsl:choose>                  
        <xsl:when test="prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:InquiriesPrevious12Months">                    
          <xsl:value-of select="number(prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:InquiriesPrevious12Months)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

  
    <xsl:variable name="paymentIndicator">
      <xsl:choose>                  
        <xsl:when test="normalize-space(prd:SBCSExecutiveSummary/prd:PaymentTrendIndicator/@code) != ''">                    
          <xsl:value-of select="prd:SBCSExecutiveSummary/prd:PaymentTrendIndicator" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="paymentComparison">
      <xsl:choose>                  
        <xsl:when test="normalize-space(prd:SBCSExecutiveSummary/prd:IndustryPaymentComparison/@code) != ''">                    
          <xsl:value-of select="concat('Has paid ', prd:SBCSExecutiveSummary/prd:IndustryPaymentComparison, ' similar firms')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="commonTerms">
      <xsl:variable name="terms1">
        <xsl:choose>                  
          <xsl:when test="prd:SBCSExecutiveSummary/prd:CommonTerms">                     
            <xsl:value-of select="prd:SBCSExecutiveSummary/prd:CommonTerms" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>

      <xsl:variable name="terms2">
        <xsl:choose>                  
          <xsl:when test="prd:SBCSExecutiveSummary/prd:CommonTerms2">                     
            <xsl:value-of select="prd:SBCSExecutiveSummary/prd:CommonTerms2" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>

      <xsl:variable name="terms3">
        <xsl:choose>                  
          <xsl:when test="prd:SBCSExecutiveSummary/prd:CommonTerms3">                     
            <xsl:value-of select="prd:SBCSExecutiveSummary/prd:CommonTerms3" />
          </xsl:when>
  
          <xsl:otherwise>
            <xsl:value-of select="''" />
          </xsl:otherwise>
        </xsl:choose>    
      </xsl:variable>

      <xsl:choose>                  
        <xsl:when test="normalize-space($terms1) != '' and normalize-space($terms2) != '' and normalize-space($terms3) != ''">                     
          <xsl:value-of select="concat($terms1, ', ', $terms2, ', and ', $terms3)" />
        </xsl:when>

        <xsl:when test="normalize-space($terms1) != '' and normalize-space($terms2) != ''">
          <xsl:value-of select="concat($terms1, ' and ', $terms2)" />
        </xsl:when>
        
        <xsl:when test="normalize-space($terms1) != '' and normalize-space($terms3) != ''">
          <xsl:value-of select="concat($terms1, ' and ', $terms3)" />
        </xsl:when>
        
        <xsl:when test="normalize-space($terms2) != '' and normalize-space($terms3) != ''">
          <xsl:value-of select="concat($terms2, ' and ', $terms3)" />
        </xsl:when>
        
        <xsl:otherwise>
          <xsl:value-of select="concat($terms1, $terms2, $terms3)" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>


    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="{$color}">
        
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#ffffff">
                <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                  <tr height="20">
                    <td bgcolor="{$color}" colspan="3" align="left" valign="middle">
                      <font color="#ffffff"><b><xsl:text disable-output-escaping="yes">&#160;&#160;</xsl:text>Performance Analysis</b></font>
                    </td>
                  </tr>  

                  <tr>
                    <td width="1%">
                      <img src="../images/spacer.gif" border="0" width="5" height="1" /></td>
                    <td width="98%">  
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td style="line-height:4px;" colspan="2">&#160;</td>
                        </tr>  

                        <tr height="20">  
                          <td colspan="2">
                            <b>Performance Metrics:</b></td>
                        </tr>

                        <tr height="20">
                          <td colspan="2">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="65%">
                                  Average balance previous 5 quarters:</td>
                                <td width="35%" align="right" nowrap="nowrap"><b><xsl:value-of select="$avg5QBalance" /></b></td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        
                        <tr height="20">
                          <td colspan="2">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="65%">
                                  Highest credit amount extended:</td>
                                <td width="35%" align="right" nowrap="nowrap"><a name="highestcredit"/><b><xsl:value-of select="$highestCredit" /></b></td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      
                        
                        <tr height="20">
                          <td colspan="2">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="40%">
                                  6 month balance range:</td>
                                <td width="60%" align="right" nowrap="nowrap"><b><xsl:value-of select="$balanceRange" /></b></td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      
                        
                        <tr height="20">
                          <td width="90%">
                            Inquiries previous 6 months:</td>
                          <td width="10%" align="right"><b><xsl:value-of select="$inquiries6M" /></b></td>
                        </tr>
                      
                        
                        <tr height="20">
                          <td width="90%">
                          Inquiries previous 12 months:</td>
                          <td width="10%" align="right"><b><xsl:value-of select="$inquiries12M" /></b></td>
                        </tr>

                        <tr>
                          <td style="line-height:3px;" colspan="2">&#160;</td>
                        </tr>
                        
                        <tr colspan="2" height="38">  
                          <td>
                            <b>Payment Trend Indication:</b><br />
                            <xsl:value-of select="$paymentIndicator" />
                          </td>
                        </tr>
                        
                        <tr colspan="2" height="38">  
                          <td>
                            <b>Industry Payment Comparison:</b><br />
                            <xsl:value-of select="$paymentComparison" />
                          </td>
                        </tr>
                      
                        <tr colspan="2" height="38">  
                          <td>
                            <b>Most Frequent Industry Purchasing Terms:</b><br />
                            <xsl:value-of select="$commonTerms" />
                          </td>
                        </tr>
                    
                      </table> 
                    </td>
                    <td width="1%">
                      <img src="../images/spacer.gif" border="0" width="5" height="1" /></td>
                  </tr>    
                  
                  <tr>
                    <td colspan="3" style="line-height:3px;">&#160;</td>
                  </tr>

                </table>
              </td>
            </tr>     
          </table> 
  
        </td>
      </tr>
    </table>

  </xsl:template>

      
</xsl:stylesheet>
