
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">
                

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />



  <!--
  *********************************************
  * SBCSFinancialSummary template
  *********************************************
  -->
  <xsl:template match="prd:SBCSFinancialSummary" >
    <xsl:param name="color" select="'#0099cc'" />

    <!-- blue box border -->
    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="{$color}">
    
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#ffffff">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td bgcolor="{$color}" colspan="11" align="left" valign="middle" height="20">
                      <b><font color="#ffffff"><xsl:text disable-output-escaping="yes">&#160;&#160;</xsl:text>SBCS Financial Account Summary</font></b>
                    </td>
                  </tr>
    
                  <tr bgcolor="#ffffff">
                    <td align="center" width="30%" colspan="2"><font size="1" style="FONT-FAMILY: 'verdana';"><b>Type</b></font>
                    </td>
    
                    <td align="center" width="3%"><font size="1" style="FONT-FAMILY: 'verdana';"><b>#</b></font>
                    </td>
                    
                    <td align="center" width="17%" colspan="2"><font size="1" style="FONT-FAMILY: 'verdana';"><b>Balance</b></font>
                    </td>
                    
                    <td align="center" width="17%" colspan="2"><font size="1" style="FONT-FAMILY: 'verdana';"><b>Credit Limit/<br/>Original Loan Amt</b></font>
                    </td>
                    
                    <td align="center" width="17%" colspan="2"><font size="1" style="FONT-FAMILY: 'verdana';"><b>Monthly<br/>Payment</b></font>
                    </td>
                    
                    <td align="center" width="16%" colspan="2"><font size="1" style="FONT-FAMILY: 'verdana';"><b>Past Due</b></font>
                    </td>
                  </tr>
    
                  <xsl:apply-templates select="prd:AccountTypeSummary">
                    <xsl:with-param name="color" select="$color" />
                  </xsl:apply-templates>

                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

  </xsl:template>



  <!--
  *********************************************
  * AccountTypeSummary template
  *********************************************
  -->
  <xsl:template match="prd:AccountTypeSummary" >
    <xsl:param name="color" select="'#0099cc'" />

    <xsl:variable name="category">
      <xsl:call-template name="convertcase">
        <xsl:with-param name="toconvert" select="prd:AccountCategory" />
        <xsl:with-param name="conversion" select="'upper'" />
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="linesReported">
      <xsl:choose>                  
        <xsl:when test="prd:NumberOfTradeLines">                    
          <xsl:value-of select="number(prd:NumberOfTradeLines)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="0" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="balance">
      <xsl:choose>
        <xsl:when test="prd:TotalBalance and number(prd:TotalBalance/prd:Amount) != 0">                    
          <xsl:value-of select="concat(prd:TotalBalance/prd:Modifier/@code, format-number(prd:TotalBalance/prd:Amount, '$###,###,##0'))" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="creditLimit">
      <xsl:choose>
        <xsl:when test="prd:HighCredit and number(prd:HighCredit/prd:Amount) != 0">                    
          <xsl:value-of select="concat(prd:HighCredit/prd:Modifier/@code, format-number(prd:HighCredit/prd:Amount, '$###,###,##0'))" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="monthlyPayment">
      <xsl:choose>
        <xsl:when test="prd:MonthlyPayment and number(prd:MonthlyPayment/prd:Amount) != 0">                    
          <xsl:value-of select="concat(prd:MonthlyPayment/prd:Modifier/@code, format-number(prd:MonthlyPayment/prd:Amount, '$###,###,##0'))" />
        </xsl:when>

        <xsl:when test="prd:MonthlyPayment and number(prd:MonthlyPayment/prd:Amount) = 0 and prd:MonthlyPayment/prd:Modifier/@code = '+'">                    
          <xsl:value-of select="'UNK'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="pastDue">
      <xsl:choose>
        <xsl:when test="prd:TotalPastDueAmount and number(prd:TotalPastDueAmount) != 0">                    
          <xsl:value-of select="format-number(prd:TotalPastDueAmount, '$###,###,##0')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="bgColor">
      <xsl:choose>                  
        <xsl:when test="position() mod 2 = 0">                     
          <xsl:value-of select="'#ffffff'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'#e5f5fa'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>


    <xsl:if test="position() = last()">
      <tr>
        <td bgcolor="{$color}" colspan="11" style="line-height:1px">&#160;</td>
      </tr>
    </xsl:if>
    
    <tr height="20">
      <td bgcolor="{normalize-space($bgColor)}" align="right"><font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$category" /></font>
      </td>
      <td bgcolor="{normalize-space($bgColor)}" width="2%">
        <img height="1" width="1" border="0" src="../images/spacer.gif" /></td>
      
      <td bgcolor="{normalize-space($bgColor)}" align="center"><font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$linesReported" /></font>
      </td>
      
      <td bgcolor="{normalize-space($bgColor)}" align="right"><font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$balance" /></font>
      </td>
      <td bgcolor="{normalize-space($bgColor)}" width="2%">
        <img height="1" width="1" border="0" src="../images/spacer.gif" /></td>
      
      <td bgcolor="{normalize-space($bgColor)}" align="right"><font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$creditLimit" /></font>
      </td>
      <td bgcolor="{normalize-space($bgColor)}" width="2%">
        <img height="1" width="1" border="0" src="../images/spacer.gif" /></td>
      
      <td bgcolor="{normalize-space($bgColor)}" align="right"><font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$monthlyPayment" /></font>
      </td>
      <td bgcolor="{normalize-space($bgColor)}" width="2%">
        <img height="1" width="1" border="0" src="../images/spacer.gif" /></td>
      
      <td bgcolor="{normalize-space($bgColor)}" align="right"><font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$pastDue" /></font>
      </td>
      <td bgcolor="{normalize-space($bgColor)}" width="2%">
        <img height="1" width="1" border="0" src="../images/spacer.gif" /></td>
    </tr>
    
  </xsl:template>

</xsl:stylesheet>