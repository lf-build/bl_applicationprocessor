
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rsp="http://www.experian.com/NetConnectResponse"
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * SBCSPaymentTotals template
  *********************************************
  -->
  <xsl:template match="prd:SBCSPaymentTotals" >
    <xsl:param name="color" select="'#0099cc'" />

    <!-- blue box border -->
    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="{$color}">
    
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#ffffff">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                  <tr>
                    <td bgcolor="{$color}" colspan="11" align="left" valign="middle" height="20">
                      <b><font color="#ffffff"><xsl:text disable-output-escaping="yes">&#160;&#160;</xsl:text>SBCS Trade Account Summary</font></b>
                    </td>
                  </tr>

                  <tr bgcolor="#ffffff">
                    <td align="center" width="21%"><font size="1" style="FONT-FAMILY: 'verdana';"><b>Type</b></font></td>
                    <td align="center" width="7%"><font size="1" style="FONT-FAMILY: 'verdana';"><b>#</b></font></td>

                    <td width="12%">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr bgcolor="#ffffff">
                          <td width="20%"></td>
                          <td width="80%" align="center"><font size="1" style="FONT-FAMILY: 'verdana';"><b>Balance</b></font></td>
                        </tr>
                      </table>
                    </td>

                    <td width="12%">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr bgcolor="#ffffff">
                          <td width="20%"></td>
                          <td width="80%" align="center"><font size="1" style="FONT-FAMILY: 'verdana';"><b>Recent<br />High<br />Credit</b></font></td>
                        </tr>
                      </table>
                    </td>

                    <td align="center" width="6%"><font size="1" style="FONT-FAMILY: 'verdana';"><b>Cur</b></font></td>
                    <td align="center" width="6%"><font size="1" style="FONT-FAMILY: 'verdana';"><b>1-30</b></font></td>
                    <td align="center" width="6%"><font size="1" style="FONT-FAMILY: 'verdana';"><b>31-60</b></font></td>
                    <td align="center" width="6%"><font size="1" style="FONT-FAMILY: 'verdana';"><b>61-90</b></font></td>
                    <td align="center" width="6%"><font size="1" style="FONT-FAMILY: 'verdana';"><b>91-120</b></font></td>
                    <td align="center" width="6%"><font size="1" style="FONT-FAMILY: 'verdana';"><b>121+</b></font></td>
                    <td align="left" width="12%"><font size="1" style="FONT-FAMILY: 'verdana';"><b>Comments</b></font></td>
                  </tr>

                  <!-- ContinuouslyReportedTradeLines template -->
                  <xsl:apply-templates select="prd:ContinuouslyReportedTradeLines">
                      <xsl:with-param name="type" select="'Continuously Reported'" />
                  </xsl:apply-templates>
                          
                  <!-- NewlyReportedTradeLines template -->
                  <xsl:apply-templates select="prd:NewlyReportedTradeLines">
                      <xsl:with-param name="type" select="'Newly Reported'" />
                  </xsl:apply-templates>

                  <tr>
                    <td bgcolor="{$color}" colspan="12" style="line-height:1px">&#160;</td>
                  </tr>

                  <!-- CombinedTradeLines template -->
                  <xsl:apply-templates select="prd:CombinedTradeLines">
                      <xsl:with-param name="type" select="'Current Trade Lines Totals'" />
                  </xsl:apply-templates>
                  
                  <tr>
                    <td bgcolor="{$color}" colspan="12" style="line-height:1px">&#160;</td>
                  </tr>

                  <!-- AdditionalTradeLines template -->
                  <xsl:apply-templates select="prd:AdditionalTradeLines">
                      <xsl:with-param name="type" select="'Additional Experiences'" />
                  </xsl:apply-templates>
                  
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

  </xsl:template>
    
  
  <!--
  *********************************************
  * Trade Lines template
  *********************************************
  -->
  <xsl:template match="prd:NewlyReportedTradeLines | prd:ContinuouslyReportedTradeLines | prd:CombinedTradeLines  | prd:AdditionalTradeLines" >
    <xsl:param name="type" />
    
    <xsl:variable name="linesReported">
      <xsl:choose>                  
        <xsl:when test="prd:NumberOfLines">                    
          <xsl:value-of select="number(prd:NumberOfLines)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="0" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="recentHighCredit">
      <xsl:choose>
        <xsl:when test="$linesReported = 0">                     
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:when test="prd:TotalHighCreditAmount and number(prd:TotalHighCreditAmount/prd:Amount) != 0">                    
          <xsl:value-of select="concat(prd:TotalHighCreditAmount/prd:Modifier/@code, format-number(prd:TotalHighCreditAmount/prd:Amount, '$###,###,##0'))" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="balance">
      <xsl:choose>                  
        <xsl:when test="$linesReported = 0">                     
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:when test="prd:TotalAccountBalance and number(prd:TotalAccountBalance/prd:Amount) != 0">                    
          <xsl:value-of select="concat(prd:TotalAccountBalance/prd:Modifier/@code, format-number(prd:TotalAccountBalance/prd:Amount, '$###,###,##0'))" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'$0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="current">
      <xsl:choose>                  
        <xsl:when test="prd:CurrentPercentage and number(prd:CurrentPercentage) != 0">                     
          <xsl:value-of select="format-number(prd:CurrentPercentage div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="DBT30">
      <xsl:choose>                  
        <xsl:when test="prd:DBT30 and number(prd:DBT30) != 0">                     
          <xsl:value-of select="format-number(prd:DBT30 div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="DBT60">
      <xsl:choose>                  
        <xsl:when test="prd:DBT60 and number(prd:DBT60) != 0">                     
          <xsl:value-of select="format-number(prd:DBT60 div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="DBT90">
      <xsl:choose>                  
        <xsl:when test="prd:DBT90 and number(prd:DBT90) != 0">                     
          <xsl:value-of select="format-number(prd:DBT90 div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="DBT120">
      <xsl:choose>                  
        <xsl:when test="prd:DBT120 and number(prd:DBT120) != 0">                     
          <xsl:value-of select="format-number(prd:DBT120 div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="DBT121Plus">
      <xsl:choose>                  
        <xsl:when test="prd:DBT121Plus and number(prd:DBT121Plus) != 0">                     
          <xsl:value-of select="format-number(prd:DBT121Plus div 100, '##0%')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="comments">
      <xsl:choose>                  
        <xsl:when test="$linesReported = 0">                     
          <xsl:value-of select="''" />
        </xsl:when>

        <xsl:when test="prd:DBT and number(prd:DBT) != 0">                     
          <xsl:value-of select="number(prd:DBT)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>
    
    <xsl:variable name="bgColor">
      <xsl:choose>                  
        <xsl:when test="$type = 'Newly Reported' or $type = 'Additional Experiences'">                     
          <xsl:value-of select="'#ffffff'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'#e5f5fa'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <tr>
      <td height="20" bgcolor="{normalize-space($bgColor)}" align="right"><font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$type" /></font>
      </td>
      <td height="20" bgcolor="{normalize-space($bgColor)}" align="center"><font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$linesReported" /></font>
      </td>

      <td height="20" bgcolor="{normalize-space($bgColor)}" align="right">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="88%" align="right"><font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$balance" /></font>
            </td>
            <td width="12%">
            </td>
          </tr>
        </table>
      </td>

      <td height="20" bgcolor="{normalize-space($bgColor)}" align="right">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="88%" align="right"><font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$recentHighCredit" /></font>
            </td>
            <td width="12%">
            </td>
          </tr>
        </table>
      </td>

      <td height="20" bgcolor="{normalize-space($bgColor)}" align="center"><font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$current" /></font>
      </td>
      <td height="20" bgcolor="{normalize-space($bgColor)}" align="center"><font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$DBT30" /></font>
      </td>
      <td height="20" bgcolor="{normalize-space($bgColor)}" align="center"><font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$DBT60" /></font>
      </td>
      <td height="20" bgcolor="{normalize-space($bgColor)}" align="center"><font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$DBT90" /></font>
      </td>
      <td height="20" bgcolor="{normalize-space($bgColor)}" align="center"><font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$DBT120" /></font>
      </td>
      <td height="20" bgcolor="{normalize-space($bgColor)}" align="center"><font size="1" style="FONT-FAMILY: 'verdana';"><xsl:value-of select="$DBT121Plus" /></font>
      </td>
      <td height="20" bgcolor="{normalize-space($bgColor)}" align="left"><font size="1" style="FONT-FAMILY: 'verdana';">DBT: <xsl:value-of select="$comments" /></font>
      </td>
    </tr>

  </xsl:template>
  
</xsl:stylesheet>