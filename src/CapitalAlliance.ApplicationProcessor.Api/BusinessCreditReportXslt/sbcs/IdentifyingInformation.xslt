
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">
                

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * Global variables and parameters
  *********************************************
  -->

  <xsl:variable name="textCase">
        <xsl:value-of select="'proper'" />
  </xsl:variable>


  <!--
  *********************************************
  * IdentifyingInformation template
  *********************************************
  -->
  <xsl:template name="IdentifyingInformation">

    <!-- Section title -->
    <xsl:call-template name="SectionTitle">
      <xsl:with-param name="title" select="'Identifying Information'" />
      <xsl:with-param name="color" select="$titleColor" />
    </xsl:call-template>

    <!-- blue box border -->
    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="{$borderColor}">

          <!-- inner white box -->
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#ffffff">

                <table width="100%" border="0" cellspacing="0" cellpadding="4">
                    <tr>
                      <td width="100%" valign="top" colspan="2"><font size="1" style="FONT-FAMILY: 'verdana';">
                        This information is the primary name and address for the business you inquired on. All data in this report pertains to the business.</font></td>
                    </tr>

                  <!-- business data section -->  
                  <xsl:choose>		              
                    <xsl:when test="prd:SBCSBusinessNameAndAddress">
                      <xsl:apply-templates select="prd:SBCSBusinessNameAndAddress"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <tr>
                        <td colspan="2">
                          <font color="#ff0000" size="1"><b>No business data available</b></font>
                        </td>
                      </tr>  
                    </xsl:otherwise>
                  </xsl:choose>    
                  <!-- end business data section -->  
                  
                </table>
              </td>
            </tr>
          </table>
          <!-- end inner white box -->
        </td>
      </tr>  
    </table>

  </xsl:template>


  <!--
  *********************************************
  * BusinessNameAndAddress template
  *********************************************
  -->
  <xsl:template match="prd:SBCSBusinessNameAndAddress" >

    <!-- Report Date  -->
    <xsl:variable name="reportDate">
      <xsl:value-of select="prd:ProfileDate" />
    </xsl:variable>

    <!-- get year from date extension -->
    <xsl:variable name="reportYear">
      <xsl:value-of select="substring(normalize-space($reportDate), 1, 4)"/>
    </xsl:variable>

    <!-- StreetAddress -->
    <xsl:variable name="streetAddress">
      <xsl:variable name="StreetAddress">
        <xsl:value-of select="prd:StreetAddress" />
      </xsl:variable>
	
      <xsl:call-template name="convertcase">
	      <xsl:with-param name="toconvert" select="$StreetAddress" />
	      <xsl:with-param name="conversion" select="$textCase" />
      </xsl:call-template>
    </xsl:variable>

    <!-- City -->
    <xsl:variable name="city">
      <xsl:variable name="City">
	      <xsl:value-of select="normalize-space(prd:City)" />
      </xsl:variable>
	
      <xsl:call-template name="convertcase">
        <xsl:with-param name="toconvert" select="$City" />
        <xsl:with-param name="conversion" select="$textCase" />
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="businessType">
      <xsl:choose>                  
        <xsl:when test="prd:BusinessType/@code != ''">                     
          <xsl:call-template name="convertcase">
            <xsl:with-param name="toconvert" select="prd:BusinessType" />
            <xsl:with-param name="conversion" select="'upper'" />
          </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="stateOrigin">
      <xsl:choose>                  
        <xsl:when test="prd:CorporateInfo/prd:StateOfOrigin and normalize-space(prd:CorporateInfo/prd:StateOfOrigin) != ''">                     
          <xsl:value-of select="concat(' ', prd:CorporateInfo/prd:StateOfOrigin/@code, ' ', $businessType, ' ')" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="dateIncorporated">
      <xsl:choose>                  
        <xsl:when test="prd:CorporateInfo/prd:DateIncorporated and normalize-space(prd:CorporateInfo/prd:DateIncorporated) != ''">                     
          <xsl:value-of select="concat('INCORPORATED ', substring(prd:CorporateInfo/prd:DateIncorporated, 1, 4))" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="statusFlag">
      <xsl:choose>                  
        <xsl:when test="prd:CorporateInfo/prd:StatusFlag/@code != ''">                     
          <xsl:call-template name="convertcase">
            <xsl:with-param name="toconvert" select="prd:CorporateInfo/prd:StatusFlag" />
            <xsl:with-param name="conversion" select="'upper'" />
          </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="corporateStatus">
      <xsl:choose>                  
        <xsl:when test="normalize-space($stateOrigin) != '' or normalize-space($dateIncorporated) != ''">   
        
          <xsl:choose>                  
            <xsl:when test="normalize-space($statusFlag) != ''">   
              <xsl:value-of select="concat($statusFlag, $stateOrigin, $dateIncorporated)" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="concat($stateOrigin, $dateIncorporated)" />
            </xsl:otherwise>
          </xsl:choose>    
        
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="$statusFlag" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <!-- Date Incorporation
    <xsl:variable name="dateIncorporation">
      <xsl:choose>
        <xsl:when test="//prd:SBCSExecutiveElements/prd:YearofIncorporation and normalize-space(//prd:SBCSExecutiveElements/prd:YearofIncorporation) != '' ">
          <xsl:call-template name="FormatDate">
            <xsl:with-param name="pattern" select="'mo/dt/year'" />
            <xsl:with-param name="value" select="//prd:SBCSExecutiveElements/prd:YearofIncorporation" />
          </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
 -->

    <!-- SIC -->
    <xsl:variable name="sicCodePrimary">
      <xsl:choose>
        <xsl:when test="prd:SIC[prd:SICIndicator = 'P']">
          <xsl:call-template name="convertcase">
            <xsl:with-param name="toconvert" select="prd:SIC[prd:SICIndicator = 'P']/prd:SICCode" />
            <xsl:with-param name="conversion" select="'upper'" />
          </xsl:call-template>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="sicCodePrimaryDisplay">
      <xsl:choose>
        <xsl:when test="normalize-space($sicCodePrimary)">
          <xsl:value-of select="concat($sicCodePrimary, ' - ' , substring(prd:SIC[prd:SICIndicator = 'P']/prd:SICCode/@code, 1, 6))" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="substring(prd:SIC[prd:SICIndicator = 'P']/prd:SICCode/@code, 1, 6)" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="sicCodeAlt">
      <xsl:choose>
        <xsl:when test="prd:SIC[prd:SICIndicator = 'A']">
          <xsl:call-template name="convertcase">
            <xsl:with-param name="toconvert" select="prd:SIC[prd:SICIndicator = 'A']/prd:SICCode" />
            <xsl:with-param name="conversion" select="'upper'" />
          </xsl:call-template>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="sicCodeAltDisplay">
      <xsl:choose>
        <xsl:when test="normalize-space($sicCodeAlt)">
          <xsl:value-of select="concat($sicCodeAlt, ' - ' , substring(prd:SIC[prd:SICIndicator = 'A']/prd:SICCode/@code, 1, 6))" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="substring(prd:SIC[prd:SICIndicator = 'A']/prd:SICCode/@code, 1, 6)" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- Tax ID -->
    <xsl:variable name="taxID">
      <xsl:choose>
        <xsl:when test="prd:FederalTaxID and normalize-space(prd:FederalTaxID) != '' ">
          <xsl:value-of select="prd:FederalTaxID" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <tr>
      <!-- company name etc column -->
      <td width="50%" valign="top">
        <font size="1" style="FONT-FAMILY: 'verdana';">
          <b><xsl:value-of select="prd:BusinessName" /></b>
          <xsl:if test="prd:StreetAddress and string-length(normalize-space(prd:StreetAddress)) &gt; 0">
             <br />
             <xsl:value-of select="$streetAddress" /> 
          </xsl:if>
          <br />
          <xsl:value-of select="normalize-space($city)" /><xsl:if test="normalize-space($city) != '' and normalize-space(prd:State) != ''">,</xsl:if> 
          <xsl:value-of select="prd:State" />
          <xsl:text disable-output-escaping="yes"> </xsl:text>
          <xsl:call-template name="FormatZip">
            <xsl:with-param name="value" select="concat(prd:Zip, prd:ZipExtension)" />
          </xsl:call-template>
          <br />
          <xsl:if test="prd:PhoneNumber">
				  <xsl:call-template name="FormatPhone">
				    <xsl:with-param name="value" select="translate(prd:PhoneNumber, '-', '')" />
				  </xsl:call-template>
          </xsl:if>

          <xsl:if test="../prd:IntelliscoreScoreInformation/prd:CustomerDispute/prd:Statement/@code = 'Y' or ../prd:Statement">
	          <br />
	          <font color="#ff0000" size="1" style="font-family: 'verdana';">
	          	<b>See business statement on file</b>
	          	<!-- <a href="#BusinessStatement">details</a> -->
 		   </font>
          </xsl:if>
        </font>

      </td>
      <!-- end company name etc column -->
      
      <!-- file number etc column -->
      <td width="50%" valign="top">


        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td colspan="2">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
	            <td width="40%" nowrap="nowrap"><font size="1" style="font-family: 'verdana';">
	              <b>Business Identification Number:</b></font></td>
	            <td width="60%" align="right" nowrap="nowrap"><font size="1" style="font-family: 'verdana';">
	              <xsl:value-of select="prd:ExperianBIN" /></font></td>
          	   </tr>
          	 </table>
            </td>
          </tr>

          <xsl:if test="../prd:IntelliscoreScoreInformation/prd:ProfileNumber">
            <tr>
              <td nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">
                <b>Full Report Number:</b></font></td>
              <td align="right" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">
                FR-<xsl:value-of select="../prd:IntelliscoreScoreInformation/prd:ProfileNumber" /></font></td>
            </tr>
          </xsl:if>

          <tr>
            <td width="40%" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">
              <b>Years on File:</b></font></td>
            <td width="60%" align="right" nowrap="nowrap">
              <font size="1" style="FONT-FAMILY: 'verdana';">
              <xsl:if test="prd:FileEstablishDate">
              	<xsl:value-of select="($reportYear - number(substring(prd:FileEstablishDate, 1, 4)))" /><xsl:if test="prd:FileEstablishFlag/@code = 'P'">+ </xsl:if>
	              <xsl:choose>		              
	                <xsl:when test="prd:FileEstablishFlag/@code = 'P'">
	                  (FILE ESTABLISHED PRIOR TO 01/1977)
	                </xsl:when>
	
	                <xsl:otherwise>
	                  (FILE ESTABLISHED 
							   <xsl:call-template name="FormatDate">
							     <xsl:with-param name="pattern" select="'mo/year'" />
							     <xsl:with-param name="value" select="prd:FileEstablishDate" />
							   </xsl:call-template>)
	                </xsl:otherwise>
	              </xsl:choose>    
              </xsl:if>
              </font>
            </td> 
             
          </tr>

          <xsl:if test="prd:SIC[prd:SICIndicator = 'P']">
            <tr>
              <td colspan="2">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="15%" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">
                      <b>SIC Code:</b></font></td> 
                    <td width="85%" align="right" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">
                      	<xsl:value-of select="$sicCodePrimaryDisplay" /></font></td>
                  </tr>
                </table>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="$product = 'SBCS' and prd:SIC[prd:SICIndicator = 'A']">
            <tr>
              <td colspan="2">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="15%" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">
                      <b>Secondary SIC:</b></font></td> 
                    <td width="85%" align="right" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">
                      	<xsl:value-of select="$sicCodeAltDisplay" /></font></td>
                  </tr>
                </table>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="normalize-space($taxID)">
            <tr>
              <td nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">
                <b>Federal Tax ID:</b></font></td>
              <td align="right" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">
                <xsl:value-of select="$taxID" /></font></td>
            </tr>
          </xsl:if>
          
          <xsl:if test="normalize-space($corporateStatus)">
            <tr>
              <td nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">
                <b>Corporate status:</b></font></td>
              <td align="right" nowrap="nowrap"><font size="1" style="FONT-FAMILY: 'verdana';">
                <xsl:value-of select="$corporateStatus" /></font></td>
            </tr>
          </xsl:if>

        </table>

      </td>
      <!-- end file number etc column -->

    </tr>

  </xsl:template>
</xsl:stylesheet>