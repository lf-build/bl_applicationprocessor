
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rsp="http://www.experian.com/NetConnectResponse"
                xmlns:prd="http://www.experian.com/ARFResponse">

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />



  <!--
  *********************************************
  * DBTCharts template
  *********************************************
  -->
  <xsl:template name="DBTCharts">
    <xsl:param name="color" select="'#0099cc'" />

      <table width="100%" border="0" cellspacing="0" cellpadding="0">
      
        <tr>
          <!-- Monthly Payment Trends column -->
          <td width="49%" valign="top">
            
            <table width="100%" border="0" cellspacing="0" cellpadding="1">
              <tr>
                <td bgcolor="{$color}">
            
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td bgcolor="#ffffff">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        
                          <tr>
                            <td bgcolor="{$color}" align="left" valign="middle" height="20">
                              <b><font color="#ffffff"><xsl:text disable-output-escaping="yes">&#160;&#160;</xsl:text>7 Month DBT Trends (Non-financial Accounts)</font></b>
                            </td>
                          </tr>
        
                          <tr>
                            <td style="line-height:5px;">&#160;</td>
                          </tr>
        
                          <tr>
                            <td>
								<xsl:choose>
									<xsl:when test="prd:SBCSPaymentTrends">
										<div id="bar_chart_MDT" style="width: 330px; height: 150px;"></div>
									</xsl:when>
									<xsl:otherwise>
										<div style="width: 330px; height: 175px; font-size: 20px; margin-top: 25px; text-align: center">Data not available</div>
									</xsl:otherwise>
								</xsl:choose>
								<!--
								  <img src="{normalize-space($dbt7MChartPath)}" width="330" height="140" alt="7 Month DBT Trends" />
								-->
                            </td>
                          </tr>
        
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>

          </td>
      
          <td width="2%">
          </td>
      
          <!-- Quarterly Payment Trends column -->
          <td width="49%" valign="top">

            <table width="100%" border="0" cellspacing="0" cellpadding="1">
              <tr>
                <td bgcolor="{$color}">
            
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td bgcolor="#ffffff">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        
                          <tr>
                            <td bgcolor="{$color}" align="left" valign="middle" height="20">
                              <b><font color="#ffffff"><xsl:text disable-output-escaping="yes">&#160;&#160;</xsl:text>5 Quarter DBT Trends (Non-financial Accounts)</font></b>
                            </td>
                          </tr>
        
                          <tr>
                            <td style="line-height:5px;">&#160;</td>
                          </tr>
        
                          <tr>
                            <td>
								<xsl:choose>
									<xsl:when test="prd:SBCSQuarterlyPaymentTrends">
										<div id="bar_chart_QDT" style="width: 330px; height: 150px;"></div>
									</xsl:when>
									<xsl:otherwise>
										<div style="width: 330px; height: 50px; font-size: 20px; margin-top: 25px; text-align: center">Data not available</div>
									</xsl:otherwise>
								</xsl:choose>
								<!--
								<img src="{normalize-space($dbt5QChartPath)}" width="330" height="140" alt="5 Quarter DBT Trends" />
								-->
                            </td>
                          </tr>
        
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>

          </td>
        </tr>
            
      </table>
  </xsl:template>

      
</xsl:stylesheet>
