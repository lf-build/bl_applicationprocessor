
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * ExecutiveSummary template
  *********************************************
  -->
  <xsl:template name="ExecutiveSummary">
  
    <!-- Section title -->
    <xsl:call-template name="SectionTitle">
      <xsl:with-param name="title" select="'SBCS Executive Summary'" />
      <xsl:with-param name="color" select="$titleColor" />
    </xsl:call-template>

    <xsl:if test="prd:SBCSFinancialSummary">
      <!-- SBCSFinancialSummary template -->
      <xsl:apply-templates select="prd:SBCSFinancialSummary" >
        <xsl:with-param name="color" select="$borderColor" />
      </xsl:apply-templates>
  
      <xsl:if test="prd:SBCSExecutiveElements or prd:SBCSPaymentTotals">
        <br />
      </xsl:if>
    </xsl:if>

    <xsl:if test="prd:SBCSPaymentTotals">
      <!-- SBCSPaymentTotals template -->
      <xsl:apply-templates select="prd:SBCSPaymentTotals">
        <xsl:with-param name="color" select="$borderColor" />
      </xsl:apply-templates>
  
      <xsl:if test="prd:SBCSExecutiveElements">
        <br />
      </xsl:if>
    </xsl:if>

    <xsl:if test="prd:SBCSExecutiveElements">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
      
        <tr>
          <!-- Public Record column -->
          <td width="49%" valign="top">
            <xsl:call-template name="PublicRecord" >
	        <xsl:with-param name="color" select="$borderColor" />
            </xsl:call-template>
          </td>
      
          <td width="2%">
          </td>
      
          <!-- Collections Derog column -->
          <td width="49%" valign="top">
            <xsl:call-template name="CollectionsDerogatory">
	        <xsl:with-param name="color" select="$borderColor" />
            </xsl:call-template>
          </td>
        </tr>
  
        <xsl:if test="prd:SBCSExecutiveElements/prd:UCCDerogatoryCount and number(prd:SBCSExecutiveElements/prd:UCCDerogatoryCount) &gt; 0">
          <tr>
            <td colspan="3">
              <font size="1"><i>
              ** Cautionary UCC Filings include one or more of the following collateral:<br/>
              Accounts, Accounts Receivables, Contract Rights, Hereafter Acquired Property, Inventory, Leases, Notes Receivable or Proceeds. 
              </i></font>
            </td>
          </tr>
        </xsl:if>    
            
      </table>
    </xsl:if>

  </xsl:template>

</xsl:stylesheet>