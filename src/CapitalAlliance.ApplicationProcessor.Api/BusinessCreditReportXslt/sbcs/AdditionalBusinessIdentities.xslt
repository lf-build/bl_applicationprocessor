
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rsp="http://www.experian.com/NetConnectResponse"
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * AdditionalBusinessIdentities template
  *********************************************
  -->
  <xsl:template name="AdditionalBusinessIdentities">
    <xsl:param name="color" select="'#0099cc'" />
  
    <!-- Section title -->
    <xsl:call-template name="SectionTitle">
      <xsl:with-param name="title" select="'Additional Business Identities'" />
      <xsl:with-param name="color" select="$titleColor" />
    </xsl:call-template>

    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="{$color}">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#ffffff">
                <table width="100%" border="0" cellspacing="0" cellpadding="4">

                  <!-- loops through business identites -->
                  <xsl:apply-templates select="prd:AdditionalBusinessIdentity" />

                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

  </xsl:template>



  <!--
  *********************************************
  * AdditionalBusinessIdentity template
  *********************************************
  -->
  <xsl:template match="prd:AdditionalBusinessIdentity" >

    <xsl:variable name="name">
      <xsl:choose>                  
        <xsl:when test="prd:BusinessName">                    
          <xsl:value-of select="prd:BusinessName" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="street">
      <xsl:choose>                  
        <xsl:when test="prd:StreetAddress">                     
          <xsl:value-of select="prd:StreetAddress" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="city">
      <xsl:choose>                  
        <xsl:when test="prd:City">                     
          <xsl:value-of select="prd:City" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="state">
      <xsl:choose>                  
        <xsl:when test="prd:State">                     
          <xsl:value-of select="prd:State" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="zip">
      <xsl:choose>                  
        <xsl:when test="prd:Zip">                     
          <xsl:call-template name="FormatZip">
            <xsl:with-param name="value" select="concat(prd:Zip, prd:ZipExtension)" />
          </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="phone">
      <xsl:choose>                  
        <xsl:when test="prd:PhoneNumber">                     
          <xsl:call-template name="FormatPhone">
            <xsl:with-param name="value" select="translate(prd:PhoneNumber, '-', '')" />
          </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="taxID">
      <xsl:choose>                  
        <xsl:when test="prd:FederalTaxID">                     
          <xsl:value-of select="prd:FederalTaxID" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="bgColor">
      <xsl:choose>                  
        <xsl:when test="position() mod 2 = 0">                     
          <xsl:value-of select="'#e5f5fa'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'#ffffff'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <tr>
      
      <td width="98%" bgcolor="{normalize-space($bgColor)}">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td colspan="2" height="20"><font size="1" style="FONT-FAMILY: 'verdana';">
              <b><u>Additional Business Identity #<xsl:value-of select="position()" /></u></b></font></td>
          </tr>
          
          <tr>
            <td width="25%"><font size="1" style="FONT-FAMILY: 'verdana';">
              <b>Name</b></font></td>
              
            <td width="75%"><font size="1" style="FONT-FAMILY: 'verdana';">
              <xsl:value-of select="$name" /></font></td>
          </tr>

          <tr>
            <td valign="top"><font size="1" style="FONT-FAMILY: 'verdana';">
              <b>Address</b></font></td>
              
            <td><font size="1" style="FONT-FAMILY: 'verdana';">
             
              <xsl:if test="normalize-space($street) != ''">
                <xsl:value-of select="$street" />
                <xsl:if test="normalize-space($city) != ''">
                  <br />
                </xsl:if>
              </xsl:if>

              <xsl:if test="normalize-space($city) != ''">
                <xsl:value-of select="concat(normalize-space($city), ', ', normalize-space($state), ' ', normalize-space($zip))" />
              </xsl:if>

              </font></td>
          </tr>

          <xsl:if test="normalize-space($taxID) != ''">
            <tr>
              <td><font size="1" style="FONT-FAMILY: 'verdana';">
                <b>Tax ID</b></font></td>
                
              <td><font size="1" style="FONT-FAMILY: 'verdana';">
                <xsl:value-of select="$taxID" /></font></td>
            </tr>
          </xsl:if>

          <xsl:if test="normalize-space($phone) != ''">
            <tr>
              <td><font size="1" style="FONT-FAMILY: 'verdana';">
                <b>Phone Number</b></font></td>
                
              <td><font size="1" style="FONT-FAMILY: 'verdana';">
                <xsl:value-of select="$phone" /></font></td>
            </tr>
          </xsl:if>

        </table>
      </td>

    </tr>


  </xsl:template>
  
  
</xsl:stylesheet>