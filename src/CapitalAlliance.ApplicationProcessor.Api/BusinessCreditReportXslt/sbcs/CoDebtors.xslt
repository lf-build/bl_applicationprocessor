
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rsp="http://www.experian.com/NetConnectResponse"
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * CoDebtors template
  *********************************************
  -->
  <xsl:template name="CoDebtors">
    <xsl:param name="color" select="'#0099cc'" />

    <!-- Section title -->
    <xsl:call-template name="SectionTitle">
      <xsl:with-param name="title" select="'Uniform Commercial Code Co-debtors'" />
      <xsl:with-param name="color" select="$titleColor" />
    </xsl:call-template>

    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="{$color}">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#ffffff">
                <table width="100%" border="0" cellspacing="0" cellpadding="4">

                  <!-- loops through codebtors -->
                  <xsl:apply-templates select="prd:UCCCoDebtor" />

                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

  </xsl:template>



  <!--
  *********************************************
  * UCCCoDebtor template
  *********************************************
  -->
  <xsl:template match="prd:UCCCoDebtor" >

    <xsl:variable name="sequenceNumber">
      <xsl:choose>                  
        <xsl:when test="prd:SequenceNumber">                    
          <xsl:value-of select="number(prd:SequenceNumber)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'0'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="dateFiled">
      <xsl:choose>                  
        <xsl:when test="prd:DateFiled">                    
           <xsl:call-template name="FormatDate">
             <xsl:with-param name="pattern" select="'mo/dt/year'" />
             <xsl:with-param name="value" select="prd:DateFiled" />
           </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="filingNumber">
      <xsl:choose>                  
        <xsl:when test="prd:DocumentNumber">                     
          <xsl:value-of select="prd:DocumentNumber" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="filingLocation">
      <xsl:choose>                  
        <xsl:when test="prd:FilingLocation">                     
          <xsl:value-of select="prd:FilingLocation" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="name">
      <xsl:choose>                  
        <xsl:when test="prd:Name">                     
          <xsl:value-of select="prd:Name" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="streetAddress">
      <xsl:choose>                  
        <xsl:when test="prd:StreetAddress">                     
          <xsl:value-of select="prd:StreetAddress" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="city">
      <xsl:choose>                  
        <xsl:when test="prd:City">                     
          <xsl:value-of select="prd:City" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="state">
      <xsl:choose>                  
        <xsl:when test="prd:State">                     
          <xsl:value-of select="prd:State" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="zip">
      <xsl:choose>                  
        <xsl:when test="prd:Zip">                     
          <xsl:call-template name="FormatZip">
            <xsl:with-param name="value" select="concat(prd:Zip, prd:ZipExtension)" />
          </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="taxID">
      <xsl:choose>                  
        <xsl:when test="prd:TaxId">                     
          <xsl:value-of select="prd:TaxId" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="bgColor">
      <xsl:choose>                  
        <xsl:when test="position() mod 2 = 0">                     
          <xsl:value-of select="'#e5f5fa'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'#ffffff'" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <tr>
      
      <td width="98%" bgcolor="{normalize-space($bgColor)}">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td colspan="4" height="16"><font size="1" style="FONT-FAMILY: 'verdana';">
              <b><u>Co-debtor #<xsl:value-of select="normalize-space($sequenceNumber)" /></u></b></font></td>
          </tr>
          
          <tr>
            <td width="15%" valign="top"><font size="1" style="FONT-FAMILY: 'verdana';">
              <b>Name</b></font></td>
              
            <td width="35%"><font size="1" style="FONT-FAMILY: 'verdana';">
              <xsl:value-of select="$name" /></font></td>

            <td width="18%"><font size="1" style="FONT-FAMILY: 'verdana';">
              <b>Filing Number</b></font></td>
              
            <td width="32%"><font size="1" style="FONT-FAMILY: 'verdana';">
              <xsl:value-of select="$filingNumber" /></font></td>
          </tr>

          <tr>
            <td valign="top" rowspan="2"><font size="1" style="FONT-FAMILY: 'verdana';">
              <b>Address</b></font></td>
              
            <td rowspan="2" valign="top"><font size="1" style="FONT-FAMILY: 'verdana';">

              <xsl:if test="normalize-space($streetAddress) != ''">
                <xsl:value-of select="$streetAddress" />

                <xsl:if test="normalize-space($city) != ''">
                  <br />
                </xsl:if>
              </xsl:if>
              
              <xsl:if test="normalize-space($city) != ''">
                <xsl:value-of select="concat(normalize-space($city), ', ', normalize-space($state), ' ', normalize-space($zip))" />
              </xsl:if>
              </font></td>
                              
            <td><font size="1" style="FONT-FAMILY: 'verdana';">
              <b>Filing Source</b></font></td>
              
            <td><font size="1" style="FONT-FAMILY: 'verdana';">
              <xsl:value-of select="$filingLocation" /></font></td>
          </tr>

          <tr>
            <td><font size="1" style="FONT-FAMILY: 'verdana';">
              <b>Original Filing Date</b></font></td>
              
            <td><font size="1" style="FONT-FAMILY: 'verdana';">
              <xsl:value-of select="$dateFiled" /></font></td>
          </tr>

          <xsl:if test="normalize-space($taxID) != ''">
            <tr>
              <td><font size="1" style="FONT-FAMILY: 'verdana';">
                <b>Tax ID</b></font></td>
                
              <td><font size="1" style="FONT-FAMILY: 'verdana';">
                <xsl:value-of select="$taxID" /></font></td>
                
              <td><xsl:text disable-output-escaping="yes">&#160;</xsl:text></td>  
              <td><xsl:text disable-output-escaping="yes">&#160;</xsl:text></td>  
            </tr>
          </xsl:if>
        </table>
      </td>

      
    </tr>
  </xsl:template>
  
  
</xsl:stylesheet>