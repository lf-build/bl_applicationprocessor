
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">
                

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />



  <!--
  *********************************************
  * CollectionsDerogatory template
  *********************************************
  -->
  <xsl:template name="CollectionsDerogatory">
    <xsl:param name="color" select="'#0099cc'" />

    <xsl:variable name="totalCollection">
      <xsl:value-of select="number(prd:SBCSExecutiveElements/prd:CollectionCount)" />
    </xsl:variable>

    <xsl:variable name="collectionDates">
      <xsl:choose>                  
        <xsl:when test="$totalCollection &gt; 0 and prd:SBCSExecutiveElements/prd:EarliestCollectionDate and number(prd:SBCSExecutiveElements/prd:EarliestCollectionDate) != 0">
        
          <xsl:variable name="oldDate">
            <xsl:call-template name="FormatDate">
              <xsl:with-param name="pattern" select="'mo/yr'" />
              <xsl:with-param name="value" select="prd:SBCSExecutiveElements/prd:EarliestCollectionDate" />
            </xsl:call-template>
          </xsl:variable>
        
          <xsl:variable name="recentDate">
            <xsl:choose>                  
              <xsl:when test="prd:SBCSExecutiveElements/prd:MostRecentCollectionDate and number(prd:SBCSExecutiveElements/prd:MostRecentCollectionDate) != 0">
               <xsl:call-template name="FormatDate">
                 <xsl:with-param name="pattern" select="'mo/yr'" />
                 <xsl:with-param name="value" select="prd:SBCSExecutiveElements/prd:MostRecentCollectionDate" />
               </xsl:call-template>
              </xsl:when>
      
              <xsl:otherwise>
                <xsl:value-of select="''" />
              </xsl:otherwise>
            </xsl:choose>                
          </xsl:variable>
                       
          <xsl:choose>                  
            <xsl:when test="normalize-space($oldDate) != normalize-space($recentDate) and normalize-space($recentDate) != ''">                     
              <xsl:value-of select="concat('(PLACED ', normalize-space($oldDate), '-', normalize-space($recentDate), ')')" />
            </xsl:when>
    
            <xsl:otherwise>
              <xsl:value-of select="concat('(PLACED ', normalize-space($oldDate), ')')" />
            </xsl:otherwise>
          </xsl:choose>    
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="accountDelinquent">
      <xsl:value-of select="number(prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:NbrOfAccountsCurrentlyDelinquent)" />
    </xsl:variable>

    <xsl:variable name="worstFinancial">
      <xsl:choose>
        <xsl:when test="prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:WorstFinancialTradePerformanceEver">                    
          <xsl:value-of select="prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:WorstFinancialTradePerformanceEver" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="worstNonFinancial">
      <xsl:choose>
        <xsl:when test="prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:WorstNonFinancialTradePerformanceEver">                    
          <xsl:value-of select="prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:WorstNonFinancialTradePerformanceEver" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:variable>

    <xsl:variable name="trades100Percent">
      <xsl:value-of select="number(prd:SBCSExecutiveElements/prd:AdditionalDataVariables/prd:NbrOfFinancialTradesGreaterThan100Utilized)" />
    </xsl:variable>


    <!-- blue box border -->
    <table width="100%" border="0" cellspacing="0" cellpadding="1">
      <tr>
        <td bgcolor="{$color}">
    
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#ffffff">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td bgcolor="{$color}" colspan="11" align="left" valign="middle" height="20">
                      <b><font color="#ffffff"><xsl:text disable-output-escaping="yes">&#160;&#160;</xsl:text>Collections and Derogatory Items</font></b>
                    </td>
                  </tr>
    
                  <tr>
                    <td width="1%">
                      <img height="1" width="1" border="0" src="../images/spacer.gif" /></td>

                    <td width="98%" align="left">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">

                        <tr height="20">  
                          <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="95%">
                                  Total collections: 
                                  <xsl:if test="$totalCollection &gt; 0">
                                    <a href="#collections"><font size="1">details</font></a><xsl:text disable-output-escaping="yes">&#160;</xsl:text>
                                  </xsl:if>
                                  <xsl:value-of select="$collectionDates" /></td>
                                <td width="5%" align="right" nowrap="nowrap"><b><xsl:value-of select="$totalCollection" /></b></td>
                              </tr>
                            </table>
                          </td>  
                        </tr>
    
                        <tr height="20">  
                          <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="95%">
                                  All accounts now delinquent/derogatory:</td>
                                <td width="5%" align="right" nowrap="nowrap"><b><xsl:value-of select="$accountDelinquent" /></b></td>
                              </tr>
                            </table>
                          </td>  
                        </tr>

                        <tr height="20">  
                          <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td colspan="2">
                                  Worst financial performance ever:</td>
                              </tr>
                              <tr>
                                <td width="2%">
                                  <xsl:text disable-output-escaping="yes">&#160;&#160;</xsl:text></td>
                                <td width="98%"><b><xsl:value-of select="$worstFinancial" /></b></td>
                              </tr>
                            </table>
                          </td>  
                        </tr>

                        <tr height="20">  
                          <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td colspan="2">
                                Worst non-financial trade performance ever:</td>
                              </tr>
                              <tr>
                                <td width="2%">
                                  <xsl:text disable-output-escaping="yes">&#160;&#160;</xsl:text></td>
                                <td width="98%"><b><xsl:value-of select="$worstNonFinancial" /></b></td>
                              </tr>
                            </table>
                          </td>  
                        </tr>

                        <tr height="20">  
                          <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="95%">
                                  Number of financial trades > 100%:</td>
                                <td width="5%" align="right" nowrap="nowrap"><b><xsl:value-of select="$trades100Percent" /></b></td>
                              </tr>
                            </table>
                          </td>  
                        </tr>

                      </table>
                    </td>

                    <td width="1%">
                      <img height="1" width="1" border="0" src="../images/spacer.gif" /></td>

                  </tr>       
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

  </xsl:template>


</xsl:stylesheet>
