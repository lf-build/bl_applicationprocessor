<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:prd="http://www.experian.com/ARFResponse">

  <!--
  *********************************************
  * Output method
  *********************************************
  -->

  <!--
    *********************************************
    * Account Data template
    * This template produces account data information
    * which could be used outside of the report area.
    *********************************************
    -->
  <xsl:template name="Account_Data_XML">
  	<xsl:param name="product" select="''" />

		<div id="div_account_xml" style="display:none;">
			<xml id="account_xml">
						<BusinessNameAndAddress>
							<BusinessName>
								<xsl:value-of select="//prd:ExpandedBusinessNameAndAddress/prd:LegalName/prd:LegalBusinessName | //prd:ExpandedBusinessNameAndAddress/prd:BusinessName" />
							</BusinessName>
							<ExperianFileNumber>
								<xsl:value-of select="normalize-space(//prd:ExpandedBusinessNameAndAddress/prd:ExperianBIN)" />
							</ExperianFileNumber>
							<xsl:copy-of select="//prd:ExpandedBusinessNameAndAddress/prd:StreetAddress"/>
							<xsl:copy-of select="//prd:ExpandedBusinessNameAndAddress/prd:City"/>
							<xsl:copy-of select="//prd:ExpandedBusinessNameAndAddress/prd:State"/>
							<xsl:copy-of select="//prd:ExpandedBusinessNameAndAddress/prd:Zip"/>
							<xsl:copy-of select="//prd:ExpandedBusinessNameAndAddress/prd:PhoneNumber"/>

							<xsl:if test="//prd:BusinessFacts/prd:FileEstablishedDate">
								<FileEstablishDate>
									<xsl:value-of select="//prd:BusinessFacts/prd:FileEstablishedDate"/>
								</FileEstablishDate>
							</xsl:if>
						</BusinessNameAndAddress>
						<ExecutiveSummary>
							<xsl:copy-of select="//prd:ExpandedCreditSummary/prd:CurrentDBT"/>
							<xsl:copy-of select="//prd:ExecutiveSummary/prd:PaymentTrendIndicator"/>
						</ExecutiveSummary>
						<ExecutiveElements>
							<xsl:copy-of select="//prd:ExpandedCreditSummary/prd:BankruptcyCount"/>
							<xsl:copy-of select="//prd:ExpandedCreditSummary/prd:TaxLienCount"/>
							<xsl:copy-of select="//prd:ExpandedCreditSummary/prd:JudgmentCount"/>
							<xsl:copy-of select="//prd:ExpandedCreditSummary/prd:CollectionCount"/>
							<xsl:copy-of select="//prd:ExpandedCreditSummary/prd:LegalBalance"/>			<!-- @TODO cannot find where it is -->

							<xsl:if test="//prd:ExpandedCreditSummary/prd:SingleHighCredit">
								<SingleLineHighCredit>
									<xsl:value-of select="//prd:ExpandedCreditSummary/prd:SingleHighCredit"/>
								</SingleLineHighCredit>
							</xsl:if>

							<xsl:copy-of select="//prd:ExpandedCreditSummary/prd:UCCFilings"/>
							<xsl:if test="//prd:Inquiry/prd:InquiryCount/prd:Count">
								<InquiryCount>
									<xsl:value-of select="sum(//prd:Inquiry/prd:InquiryCount/prd:Count) div 2"/>
								</InquiryCount>
							</xsl:if>
							<xsl:copy-of select="//prd:ExpandedBusinessNameAndAddress/prd:TaxID"/>
							<xsl:if test="//prd:ExpandedCreditSummary/prd:AverageBalance5Quarters">
								<xsl:copy-of select="//prd:ExpandedCreditSummary/prd:AverageBalance5Quarters"/>
							</xsl:if>
						</ExecutiveElements>
						<PaymentTotalsNew>
							<xsl:copy-of select="//prd:PaymentTotals/prd:NewlyReportedTradeLines/prd:NumberOfLines"/>
							<xsl:copy-of select="//prd:PaymentTotals/prd:NewlyReportedTradeLines/prd:TotalAccountBalance/prd:Amount"/>

							<xsl:if test="//prd:PaymentTotals/prd:NewlyReportedTradeLines/prd:TotalHighCreditAmount/prd:Amount">
								<TotalHighCreditAmount>
									<xsl:copy-of select="//prd:PaymentTotals/prd:NewlyReportedTradeLines/prd:TotalHighCreditAmount/prd:Amount"/>
								</TotalHighCreditAmount>
							</xsl:if>
							<xsl:copy-of select="//prd:PaymentTotals/prd:NewlyReportedTradeLines/prd:DBT30"/>
							<xsl:copy-of select="//prd:PaymentTotals/prd:NewlyReportedTradeLines/prd:DBT60"/>
							<xsl:copy-of select="//prd:PaymentTotals/prd:NewlyReportedTradeLines/prd:DBT90"/>
							<xsl:copy-of select="//prd:PaymentTotals/prd:NewlyReportedTradeLines/prd:DBT90Plus"/>
							<xsl:copy-of select="//prd:PaymentTotals/prd:NewlyReportedTradeLines/prd:DBT"/>
						</PaymentTotalsNew>
						<PaymentTotalsContinous>
							<xsl:copy-of select="//prd:PaymentTotals/prd:ContinouslyReportedTradeLines/prd:NumberOfLines"/>
							<xsl:copy-of select="//prd:PaymentTotals/prd:ContinouslyReportedTradeLines/prd:TotalAccountBalance/prd:Amount"/>
							<xsl:if test="//prd:PaymentTotals/prd:ContinouslyReportedTradeLines/prd:TotalHighCreditAmount/prd:Amount">
								<TotalHighCreditAmount>
									<xsl:copy-of select="//prd:PaymentTotals/prd:ContinouslyReportedTradeLines/prd:TotalHighCreditAmount/prd:Amount"/>
								</TotalHighCreditAmount>
							</xsl:if>
							<xsl:copy-of select="//prd:PaymentTotals/prd:ContinouslyReportedTradeLines/prd:DBT30"/>
							<xsl:copy-of select="//prd:PaymentTotals/prd:ContinouslyReportedTradeLines/prd:DBT60"/>
							<xsl:copy-of select="//prd:PaymentTotals/prd:ContinouslyReportedTradeLines/prd:DBT90"/>
							<xsl:copy-of select="//prd:PaymentTotals/prd:ContinouslyReportedTradeLines/prd:DBT90Plus"/>
							<xsl:copy-of select="//prd:PaymentTotals/prd:ContinouslyReportedTradeLines/prd:DBT"/>
						</PaymentTotalsContinous>
						<PaymentTotalsCombined>
							<xsl:copy-of select="//prd:PaymentTotals/prd:CombinedTradeLines/prd:NumberOfLines"/>
							<xsl:copy-of select="//prd:PaymentTotals/prd:CombinedTradeLines/prd:TotalAccountBalance/prd:Amount"/>
							<xsl:if test="//prd:PaymentTotals/prd:CombinedTradeLines/prd:TotalHighCreditAmount/prd:Amount">
								<TotalHighCreditAmount>
									<xsl:copy-of select="//prd:PaymentTotals/prd:CombinedTradeLines/prd:TotalHighCreditAmount/prd:Amount"/>
								</TotalHighCreditAmount>
							</xsl:if>
							<xsl:copy-of select="//prd:PaymentTotals/prd:CombinedTradeLines/prd:DBT30"/>
							<xsl:copy-of select="//prd:PaymentTotals/prd:CombinedTradeLines/prd:DBT60"/>
							<xsl:copy-of select="//prd:PaymentTotals/prd:CombinedTradeLines/prd:DBT90"/>
							<xsl:copy-of select="//prd:PaymentTotals/prd:CombinedTradeLines/prd:DBT90Plus"/>
							<xsl:copy-of select="//prd:PaymentTotals/prd:CombinedTradeLines/prd:DBT"/>
						</PaymentTotalsCombined>
						<DemographicInformation>
							<xsl:if test="//prd:SICCodes/prd:SIC">
								<PrimarySICCode>
									<xsl:value-of select="//prd:SICCodes[1]/prd:SIC/@code"/>
								</PrimarySICCode>
							</xsl:if>
							<xsl:if test="//prd:BusinessFacts/prd:EmployeeSize">
								<EmployeeSizeOrLowRange>
									<xsl:value-of select="//prd:BusinessFacts/prd:EmployeeSize"/>
								</EmployeeSizeOrLowRange>
							</xsl:if>
							<xsl:if test="//prd:BusinessFacts/prd:SalesRevenue">
								<SalesRevenueOrLowRange>
									<xsl:value-of select="//prd:BusinessFacts/prd:SalesRevenue"/>
								</SalesRevenueOrLowRange>
							</xsl:if>
						</DemographicInformation>
						<ScoreFactors>
							<xsl:copy-of select="//prd:ScoreFactors[number(prd:ModelCode) != $fsrModel]/prd:ScoreFactor"/>
						</ScoreFactors>
						<ModelCode>
							<xsl:value-of select="//prd:ModelInformation[number(prd:ModelCode) != $fsrModel]/prd:ModelCode"/>
						</ModelCode>
						<Score>
							<xsl:value-of select="//prd:IntelliscoreScoreInformation[number(prd:ModelInformation/prd:ModelCode) != $fsrModel]/prd:ScoreInfo/prd:Score"/>
						</Score>
						<RiskClass>
							<xsl:value-of select="//prd:IntelliscoreScoreInformation[number(prd:ModelInformation/prd:ModelCode) != $fsrModel]/prd:RiskClass"/>
						</RiskClass>
						<PercentileRanking>
							<xsl:value-of select="//prd:IntelliscoreScoreInformation[number(prd:ModelInformation/prd:ModelCode) != $fsrModel]/prd:PercentileRanking"/>
						</PercentileRanking>
						<ScoreFactors model="{$fsrModel}">
							<xsl:copy-of select="//prd:ScoreFactors[number(prd:ModelCode) = $fsrModel]/prd:ScoreFactor"/>
						</ScoreFactors>
						<ModelCode model="{$fsrModel}">
							<xsl:value-of select="//prd:ModelInformation[number(prd:ModelCode) = $fsrModel]/prd:ModelCode"/>
						</ModelCode>
						<Score model="{$fsrModel}">
							<xsl:value-of select="//prd:IntelliscoreScoreInformation[number(prd:ModelInformation/prd:ModelCode) = $fsrModel]/prd:ScoreInfo/prd:Score"/>
						</Score>
						<RiskClass model="{$fsrModel}">
							<xsl:value-of select="//prd:IntelliscoreScoreInformation[number(prd:ModelInformation/prd:ModelCode) = $fsrModel]/prd:RiskClass"/>
						</RiskClass>
						<PercentileRanking model="{$fsrModel}">
							<xsl:value-of select="//prd:IntelliscoreScoreInformation[number(prd:ModelInformation/prd:ModelCode) = $fsrModel]/prd:PercentileRanking"/>
						</PercentileRanking>
						<premierProfile>
							<xsl:if test="//prd:ExpandedBusinessNameAndAddress/prd:WebsiteURL">
								<websiteurl><xsl:value-of select="normalize-space(//prd:ExpandedBusinessNameAndAddress/prd:WebsiteURL)"/></websiteurl>
							</xsl:if>
							<xsl:if test="//prd:DoingBusinessAs/prd:DBAName">
								<DoingBusinessAs><xsl:value-of select="normalize-space(//prd:DoingBusinessAs/prd:DBAName)"/></DoingBusinessAs>
							</xsl:if>
							<xsl:if test="//prd:CorporateRegistration/prd:ProfitFlag and normalize-space(//prd:CorporateRegistration/prd:ProfitFlag/@code) != ''">
								<isNonProfit><xsl:value-of select="normalize-space(//prd:CorporateRegistration/prd:ProfitFlag/@code)"/></isNonProfit>
							</xsl:if>
							<xsl:if test="//prd:BusinessFacts/prd:PublicIndicator">
								<isPublic><xsl:value-of select="normalize-space(//prd:BusinessFacts/prd:PublicIndicator)"/></isPublic>
							</xsl:if>
							<xsl:if test="//prd:Stocks/prd:TickerSymbol">
								<ticker><xsl:value-of select="normalize-space(//prd:Stocks/prd:TickerSymbol)"/></ticker>
							</xsl:if>
							<matchingAddress>
								<xsl:copy-of select="//prd:ExpandedBusinessNameAndAddress/prd:MatchingBranchAddress"/>
							</matchingAddress>
							<xsl:if test="//prd:BusinessFacts/prd:SalesRevenue">
								<sales><xsl:value-of select="number(//prd:BusinessFacts/prd:SalesRevenue)"/></sales>
							</xsl:if>
							<xsl:if test="//prd:BusinessFacts/prd:EmployeeSize">
								<noEmployee><xsl:value-of select="number(//prd:BusinessFacts/prd:EmployeeSize)"/></noEmployee>
							</xsl:if>
							<xsl:if test="//prd:NAICSCodes/prd:NAICS">
								<naics><xsl:value-of select="normalize-space(//prd:NAICSCodes/prd:NAICS/@code)"/></naics>
							</xsl:if>
							<xsl:if test="//prd:SICCodes/prd:SIC">
								<sic><xsl:value-of select="normalize-space(//prd:SICCodes/prd:SIC/@code)"/></sic>
							</xsl:if>
							<xsl:if test="//prd:ExpandedCreditSummary/prd:OFACMatch or //prd:CommercialFraudShieldSummary/prd:OFACMatchCode">
								<xsl:choose>
									<xsl:when test="normalize-space(//prd:ExpandedCreditSummary/prd:OFACMatch/@code)='Y'
												or number(//prd:CommercialFraudShieldSummary/prd:OFACMatchCode/@code)=11
												or number(//prd:CommercialFraudShieldSummary/prd:OFACMatchCode/@code)=12
												or number(//prd:CommercialFraudShieldSummary/prd:OFACMatchCode/@code)=13
												">
										<OFACMatch>true</OFACMatch>
									</xsl:when>
									<xsl:otherwise>
										<OFACMatch>false</OFACMatch>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:if>
							<xsl:if test="//prd:ExpandedCreditSummary/prd:CommercialFraudRiskIndicatorCount">
								<xsl:choose>
									<xsl:when test="number(//prd:ExpandedCreditSummary/prd:CommercialFraudRiskIndicatorCount) > 0">
										<fraudAlertMatch>true</fraudAlertMatch>
									</xsl:when>
									<xsl:otherwise>
										<fraudAlertMatch>false</fraudAlertMatch>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:if>

						</premierProfile>
			</xml>
		</div>
	</xsl:template>
</xsl:stylesheet>