<xsl:stylesheet  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:rsp="http://www.experian.com/NetConnectResponse"
  xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->


  <!--
  *********************************************
  * Risk Dashboard template
  *********************************************
  -->
  <xsl:template name="RiskDashboardBlended">
	<!-- 
	* Loop through main models to get attributes from the targeted <IntelliscoreScoreInformation>
	* If there is one with blended model, use that blended one as targeted node to get attributes.
	* Otherwise, we will use commercial one.
	-->
	<xsl:variable name="targetIntelliscoreNodeIndex">
		<xsl:call-template name="GetTargetIntelliscoreNodeIndex">
			<xsl:with-param name="intelliscoreNodeList" select="prd:IntelliscoreScoreInformation[number(prd:ModelInformation/prd:ModelCode) != $fsrModel and number(prd:ModelInformation/prd:ModelCode) != $delinquencyScoreModel]" />
			<xsl:with-param name="useBlended" select="1" />
		</xsl:call-template>
	</xsl:variable>	
	
		<xsl:variable name="targetIntelliscoreNode" select="(//prd:IntelliscoreScoreInformation[number(prd:ModelInformation/prd:ModelCode) != $fsrModel and number(prd:ModelInformation/prd:ModelCode) != $delinquencyScoreModel])[number($targetIntelliscoreNodeIndex)]" />

	<xsl:variable name="title">
			<xsl:value-of select="'Risk Scores'"/>
    </xsl:variable>
    <xsl:variable name="textBoxHeight">
		<xsl:value-of select="45+16+14+60"/>
    </xsl:variable>
	
	<!-- <span style="float:left;width:74%;padding:0;margin:0;"> -->
	<table class="section" height="100%" cellspacing="0" cellpadding="0" style="width:100%;height:100%">
		<colgroup style="width:43%"/>
		<colgroup style="width:57%" />
		
		<thead>
			<tr>
				<th colspan="2"><a class="report_section_title">Risk Dashboard - Blended</a></th>
				<!--<th>Credit Assessment</th>-->
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<table style="width:100%;height:10%" height="100%" cellspacing="0" cellpadding="0">
						<tr class="subtitle"><th colspan="2" class="centerLabel"><xsl:value-of select="$title"/></th></tr>

						<tr>
							<xsl:apply-templates select="$targetIntelliscoreNode" mode="RiskDashboardBlended"/>
						</tr>
						<tr>
							<td colspan="2" style="text-align:center;vertical-align:middle;height:20px;">
						    	<xsl:choose>
									<xsl:when test="number($targetIntelliscoreNode/prd:ScoreInfo/prd:Score) div 100 != $score999 and number($targetIntelliscoreNode/prd:ScoreInfo/prd:Score) div 100 != $score998">
										<b>Score range: 1 - 100 percentile</b>
									</xsl:when>
									<xsl:otherwise>
										&#160;
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>
					</table>

				</td>

				<td>
					<table height="100%" cellspacing="0" cellpadding="0" style="width:100%;height:100%">
						<tr class="subtitle"><th class="centerLabel">Key Score Factors</th>
						</tr>
						
						<tr>
							<td class="leftborder" style="padding:5px 0 10px 5px;">
								<div>This score predicts the likelihood of serious credit delinquencies for this business 
								within the next 12 months. Payment history and public record along with other variables are 
								used to predict future risk. Higher scores indicate lower risk.</div>
							</td>
						</tr>
						
						<tr>
							<td class="leftborder" style="padding:0 0 5px 5px;">
								<div class="label">Factors lowering the score</div>
								<ul class="list">
									<xsl:for-each select="prd:ScoreFactors[number(prd:ModelCode) = number($targetIntelliscoreNode/prd:ModelInformation/prd:ModelCode) and prd:ModelCode/@code = 'B']/prd:ScoreFactor">
										<li><xsl:value-of select="text()"></xsl:value-of></li>
									</xsl:for-each>
								</ul>
							</td>
						</tr>
					</table>
				</td>

			</tr>
		</tbody>
	</table>
	<!-- </span>
	<span style="clear:both"/> -->
  </xsl:template>


  <xsl:template match="prd:IntelliscoreScoreInformation" mode="RiskDashboardBlended">
    <xsl:variable name="model">
      <xsl:value-of select="number(prd:ModelInformation/prd:ModelCode)" />
    </xsl:variable>

    <xsl:variable name="score">
      <xsl:value-of select="number(prd:ScoreInfo/prd:Score) div 100" />
    </xsl:variable>

    <xsl:variable name="scoreText">
      <xsl:choose>
        <xsl:when test="contains($score, '.')">
          <xsl:value-of select="normalize-space(substring-before($score, '.'))" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$score" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

	<xsl:variable name="riskClass">
      <xsl:choose>
        <xsl:when test="prd:RiskClass">
          <xsl:value-of select="prd:RiskClass" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="RiskClassByScore">
            <xsl:with-param name="model" select="$model" />
            <xsl:with-param name="score" select="$score" />
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
	</xsl:variable>

    <xsl:variable name="action">
      <xsl:choose>
        <xsl:when test="$riskClass = 1">
          <xsl:value-of select="$lowRiskText" />
        </xsl:when>
        <xsl:when test="$riskClass = 2">
          <xsl:value-of select="$lowMedRiskText" />
        </xsl:when>
        <xsl:when test="$riskClass = 3">
          <xsl:value-of select="$medRiskText" />
        </xsl:when>
        <xsl:when test="$riskClass = 4">
          <xsl:value-of select="$medHighRiskText" />
        </xsl:when>
        <xsl:when test="$riskClass = 5">
          <xsl:value-of select="$highRiskText" />
        </xsl:when>
        <xsl:when test="prd:Action">
          <xsl:value-of select="normalize-space(prd:Action)" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

	<xsl:variable name="actionColor">
		<xsl:choose>
			<xsl:when test="$riskClass = 1">
				<xsl:value-of select="$lowRiskColor"></xsl:value-of>
			</xsl:when>
			<xsl:when test="$riskClass = 2">
				<xsl:value-of select="lowMedRiskColor"></xsl:value-of>
			</xsl:when>
			<xsl:when test="$riskClass = 3">
				<xsl:value-of select="medRiskColor"></xsl:value-of>
			</xsl:when>
			<xsl:when test="$riskClass = 4">
				<xsl:value-of select="medHighRiskColor"></xsl:value-of>
			</xsl:when>
			<xsl:when test="$riskClass = 5">
				<xsl:value-of select="highRiskColor"></xsl:value-of>
			</xsl:when>
			<xsl:otherwise><xsl:value-of select="'#cccccc'"></xsl:value-of></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="scoreRiskClass">
		<xsl:choose>
			<xsl:when test="$riskClass = 1">
				<xsl:value-of select="'scoreLowRisk'"></xsl:value-of>
			</xsl:when>
			<xsl:when test="$riskClass = 2">
				<xsl:value-of select="'scoreLowMedRisk'"></xsl:value-of>
			</xsl:when>
			<xsl:when test="$riskClass = 3">
				<xsl:value-of select="'scoreMedRisk'"></xsl:value-of>
			</xsl:when>
			<xsl:when test="$riskClass = 4">
				<xsl:value-of select="'scoreMedHighRisk'"></xsl:value-of>
			</xsl:when>
			<xsl:when test="$riskClass = 5">
				<xsl:value-of select="'scoreHighRisk'"></xsl:value-of>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="'scoreUnkownRisk'"></xsl:value-of>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

    <xsl:variable name="predictText">
		<xsl:value-of select="'serious future risk'"></xsl:value-of>
    </xsl:variable>

	<xsl:variable name="actonSpaceWidth">
		<xsl:choose>
			<xsl:when test="$action = ''">
				<xsl:value-of select="'45px'"></xsl:value-of>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="'15px'"></xsl:value-of>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<td align="center" style="height: 94px; width: 50%;">
		<br/>
		<div>
			<span style="display:block;text-decoration:none">Blended Intelliscore Plus</span>
		</div>
		<xsl:choose>
			<xsl:when test="$score = $score999">
				<div style="float:left; margin: 5px 3pt 5px 15px;">
					<div style="text-align:left;">
						Score unavailable.<br/>
						Information on file not proven to predict
						<xsl:value-of select="$predictText" />.
					</div>
				</div>
			</xsl:when>
			<xsl:when test="$score = $score998">
				<div style="float:left; margin: 5px 3pt 5px 15px;">
					<div style="text-align:left;">
						Score unavailable.<br/>
						Bankruptcy on file.
					</div>
				</div>
			</xsl:when>
			<xsl:otherwise>
				<div style="float:left; margin: 5px 0pt 5px {$actonSpaceWidth};">
					<xsl:attribute name="class">
						<xsl:value-of select="concat('MiddlePad ',$scoreRiskClass)"></xsl:value-of>
					</xsl:attribute>
					<div class="value"><xsl:value-of select="$scoreText"/></div>
				</div>
				<xsl:if test="$action != ''">
					<div style="margin: 20px 10px 0pt 0pt; width: 70px; float: right;"><xsl:value-of select="$action"/></div>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</td>

	<td align="center" style="height: 94px; width: 50%;">
		<br/>
		<div>
			<span style="display:block;text-decoration:none">Risk Class</span>
		</div>

		<xsl:choose>
			<xsl:when test="$score != $score999 and $score != $score998">
				<div style="float:left; margin: 5px 0pt 5px {$actonSpaceWidth};">
					<xsl:attribute name="class">
						<xsl:value-of select="concat('MiddlePad ',$scoreRiskClass)"></xsl:value-of>
					</xsl:attribute>
					<div class="value"><xsl:value-of select="$riskClass"/></div>
				</div>
				<xsl:if test="$action != ''">
					<div style="margin: 20px 10px 0pt 0pt; width: 70px; float: right;"><xsl:value-of select="$action"/></div>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<div style="float:left; margin: 5px 3pt 5px 15px;">
					<div style="text-align:left;">
						Risk class unavailable.
					</div>
				</div>
			</xsl:otherwise>
		</xsl:choose>
	</td>

  </xsl:template>
</xsl:stylesheet>