<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:prd="http://www.experian.com/ARFResponse">


  <!--
  *********************************************
  * Output method
  *********************************************
  -->

  <!--
  *********************************************
  * UCCProfile template
  *********************************************
  -->
  <xsl:template name="UCCProfile">
    
<!-- Section title -->
  	<xsl:if test="prd:UCCFilings or prd:SBCSUCCFilings">
	<table class="section dataTable pageBreak" width="100%" cellspacing="0" cellpadding="0">
		<thead>
			<tr>
				<th colspan="7"><a name="uccfilings" class="report_section_title">Uniform Commercial Code (UCC) Filings</a></th>
			</tr>
		</thead>

	  	<xsl:if test="prd:UCCFilingsSummaryCounts">
		<thead>
			<tr class="subtitle">
				<th colspan="7">UCC Filing Summary</th>
			</tr>
			<tr class="datahead">
				<td>Date Range</td>
				<td>Year</td>
				<td>Cautionary<br />UCCs<sup>**</sup></td>
				<td>Total<br/>Filed</td>
				<td>Released /<br />Termination</td>
				<td>Continuous</td>
				<td>Amended /<br />Assigned</td>
			</tr>
		</thead>
		<tbody>
	    	<!-- UCCFilingsSummaryCounts -->
	    	<xsl:apply-templates select="prd:UCCFilingsSummaryCounts" />
	    </tbody>
	    </xsl:if>

	    <xsl:if test="prd:UCCFilings or prd:SBCSUCCFilings">
		<!--<tbody>-->
		<xsl:call-template name="UCCFilingSection" />
	    <!-- </tbody> -->
	    </xsl:if>
	</table>
    <xsl:call-template name="BackToTop" />
	</xsl:if>
  </xsl:template>


  <!--
  *********************************************
  * UCCFilingsSummaryCounts template
  *********************************************
  -->
  <xsl:template match="prd:UCCFilingsSummaryCounts">

                  <!-- MostRecent6Months -->
                  <xsl:apply-templates select="prd:MostRecent6Months" >
                  	<xsl:with-param name="previousRowCount" select="0"/>
                  </xsl:apply-templates>

                  <!-- Previous6Months -->
                  <xsl:apply-templates select="prd:Previous6Months" >
                  	<xsl:with-param name="previousRowCount" select="count(prd:MostRecent6Months)"/>
                  </xsl:apply-templates>

                  <!-- total line -->
                  <tr class="summary">
                    <td>Total</td>
                    <td>&#160;</td>
                    <td><xsl:value-of select="sum(.//prd:FilingsWithDerogatoryCollateral)" /></td>
                    <td><xsl:value-of select="sum(.//prd:FilingsTotal)" /></td>
                    <td><xsl:value-of select="sum(.//prd:ReleasesAndTerminationsTotal)" /></td>
                    <td><xsl:value-of select="sum(.//prd:ContinuationsTotal)" /></td>
                    <td><xsl:value-of select="sum(.//prd:AmendedAndAssignedTotal)" /></td>
                  </tr>

      <tr>
        <td colspan="7" class="footnote" style="padding:2px 5px 8px;">
            ** Cautionary UCC Filings include one or more of the following collateral:<br/>
            Accounts, Accounts Receivables, Contract Rights, Hereafter Acquired Property, Inventory, Leases, Notes Receivable or Proceeds.
        </td>
      </tr>

  </xsl:template>


  <!--
  ************************************************************
  * MostRecent6Months | Previous6Months template
  ************************************************************
  -->
  <xsl:template match="prd:MostRecent6Months | prd:Previous6Months">
  	<xsl:param name="previousRowCount"/>
    <xsl:variable name="month">
	   <xsl:call-template name="FormatMonth">
	     <xsl:with-param name="monthValue" select="number(substring(prd:StartDate, 5, 2))" />
	     <xsl:with-param name="upperCase" select="true()" />
	   </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="nextMonth">
	   <xsl:call-template name="FormatMonth">
	     <xsl:with-param name="monthValue" select="number(substring(prd:StartDate, 5, 2)) + 5" />
	     <xsl:with-param name="upperCase" select="true()" />
	   </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="dateRange">
      <xsl:choose>
        <xsl:when test="name() = 'MostRecent6Months'">
          <xsl:value-of select="concat($month, ' - PRESENT')" />
        </xsl:when>

        <xsl:when test="position() != last()">
          <xsl:value-of select="concat($month, ' - ', $nextMonth)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="concat('PRIOR TO ', $month)" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="year">
      <xsl:value-of select="substring(prd:StartDate, 1, 4)" />
    </xsl:variable>

    <xsl:variable name="cautionary">
      <xsl:choose>
        <xsl:when test="prd:FilingsWithDerogatoryCollateral and number(prd:FilingsWithDerogatoryCollateral) != 0">
          <xsl:value-of select="number(prd:FilingsWithDerogatoryCollateral)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="total">
      <xsl:choose>
        <xsl:when test="prd:FilingsTotal and number(prd:FilingsTotal) != 0">
          <xsl:value-of select="number(prd:FilingsTotal)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="released">
      <xsl:choose>
        <xsl:when test="prd:ReleasesAndTerminationsTotal and number(prd:ReleasesAndTerminationsTotal) != 0">
          <xsl:value-of select="number(prd:ReleasesAndTerminationsTotal)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="continuous">
      <xsl:choose>
        <xsl:when test="prd:ContinuationsTotal and number(prd:ContinuationsTotal) != 0">
          <xsl:value-of select="number(prd:ContinuationsTotal)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="amended">
      <xsl:choose>
        <xsl:when test="prd:AmendedAndAssignedTotal and number(prd:AmendedAndAssignedTotal) != 0">
          <xsl:value-of select="number(prd:AmendedAndAssignedTotal)" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <tr>
		<xsl:attribute name="class">
			<xsl:choose>
				<xsl:when test="(position()+$previousRowCount) mod 2=1"><xsl:value-of select="'even'"></xsl:value-of></xsl:when>
				<xsl:when test="(position()+$previousRowCount) mod 2=0"><xsl:value-of select="'odd'"></xsl:value-of></xsl:when>
			</xsl:choose>
		</xsl:attribute>
      <td><xsl:value-of select="$dateRange" /></td>
      <td><xsl:value-of select="$year" /></td>
      <td><xsl:value-of select="$cautionary" /></td>
      <td><xsl:value-of select="$total" /></td>
      <td><xsl:value-of select="$released" /></td>
      <td><xsl:value-of select="$continuous" /></td>
      <td><xsl:value-of select="$amended" /></td>
    </tr>
  </xsl:template>


  <!--
  *********************************************
  * UCCFilingSection template
  *********************************************
  -->
  <xsl:template name="UCCFilingSection">

    <xsl:variable name="UCCCount">
      <xsl:value-of select="count(prd:UCCFilings | prd:SBCSUCCFilings)" />
    </xsl:variable>

    <xsl:variable name="max">
      <xsl:choose>
        <xsl:when test="$UCCCount &gt; 10">
          <xsl:value-of select="10" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="$UCCCount" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>


	<xsl:variable name="pNumCols" select="2"/>

	<!-- <xsl:variable name="vSorted">
	  <xsl:for-each select="prd:UCCFilings">
            <xsl:sort order="descending" select="prd:DateFiled" />
            <xsl:copy-of select="."/>
	  </xsl:for-each>
	</xsl:variable> -->

	<xsl:variable name="vNumRows" select="ceiling($max div $pNumCols)"/>

	<thead>
		<tr class="subtitle">
			<th colspan="7">UCC Details</th>
		</tr>
	</thead>
	<tbody>
      <tr>
        <td colspan="7" id="UCCFilingSectionFirstTen" style="padding:0;">

          <!-- row of UCC filings -->
          <table>
          	<colgroup>
          		<col/>
          		<col/>
          	</colgroup>
                <xsl:for-each select="prd:UCCFilings | prd:SBCSUCCFilings">
                    <xsl:sort order="descending" select="prd:DateFiled" />
                    <xsl:if test="(position()-1) mod $pNumCols = 0 and position() &lt; ($max+1)">
                        <tr>
                            <xsl:apply-templates select="." mode="columnlevel">
                                <xsl:with-param name="count" select="$pNumCols"/>
                                <xsl:with-param name="max" select="$max"/>
                            </xsl:apply-templates>
                            <xsl:variable name="pos" select="position()"/>
                            <xsl:for-each select="../prd:UCCFilings | ../prd:SBCSUCCFilings">
                                <xsl:sort order="descending" select="prd:DateFiled" />
                                <xsl:if test="position() = $pos + 1">
                                    <xsl:apply-templates select="." mode="columnlevel">
                                        <xsl:with-param name="count" select="1"/>
                                        <xsl:with-param name="max" select="$max"/>
                                    </xsl:apply-templates>
                                </xsl:if>
                            </xsl:for-each>
                        </tr>
                    </xsl:if>
                </xsl:for-each>
          </table>
        </td>
      </tr>
    </tbody>

      <xsl:if test="($UCCCount) &gt; $max">
        <xsl:call-template name="UCCFilingSectionGTTen" >
            <xsl:with-param name="pNumCols" select="$pNumCols"/>
            <xsl:with-param name="UCCCount" select="$UCCCount"/>
        </xsl:call-template>
      </xsl:if>
  </xsl:template>






  <!--
    *********************************************
    * UCCFilingSectionGTTen template
    *********************************************
    -->
    <xsl:template name="UCCFilingSectionGTTen">
        <xsl:param name="vSorted"/>
        <xsl:param name="pNumCols"/>
        <xsl:param name="UCCCount"/>

      <xsl:variable name="max">
          <xsl:value-of select="count(prd:UCCFilings | prd:SBCSUCCFilings)" />
      </xsl:variable>

	<xsl:variable name="vNumRows" select="ceiling(($max - 10) div $pNumCols)"/>

        <!--<tr>
          <td>
            <img src="../images/spacer.gif" width="0" height="3" alt=""/></td>
        </tr>-->
	<tbody>
		<tr class="hidden_on_print">
			<td colspan="7">
	          	<a href="#addlUcc" style="text-decoration:none;" onclick="toggleDisplay(UCCFilingSectionGTTen);"><span id="toggleUCC" style="vertical-align:middle;"><img id="plusminus" src="../images/plus.gif" border="0" width="11" height="11"/></span></a>
	            <b class="indent1" style="padding-left: 5px;">View Additional UCC Details</b>
			</td>
		</tr>

	</tbody>

	<tbody>
        <tr>
           <td colspan="7" style="padding:0; height:3px;">
	      	<div id="UCCFilingSectionGTTen" class="hidden_on_screen show_on_print" style="width:100%;padding:0; display:none">
             <!-- row of UCC filings -->
                <table>
                      <colgroup>
                              <col/>
                              <col/>
                      </colgroup>

                      <tr class="subtitle">
                        <th colspan="2" align="left" valign="middle" height="23">
                          <a name="uccfilings">UCC Details (Continued)</a></th>
                      </tr>

                        <xsl:for-each select="prd:UCCFilings | prd:SBCSUCCFilings">
                            <xsl:sort order="descending" select="prd:DateFiled" />
                            <xsl:if test="(position()-1) mod $pNumCols = 0 and position() &gt; 10">
                            <tr>
                                <xsl:apply-templates select="." mode="columnlevel">
                                    <xsl:with-param name="count" select="$pNumCols"/>
                                    <xsl:with-param name="max" select="$max"/>
                                </xsl:apply-templates>
                                <xsl:variable name="pos" select="position()"/>
                                <xsl:for-each select="../prd:UCCFilings | ../prd:SBCSUCCFilings">
                                    <xsl:sort order="descending" select="prd:DateFiled" />
                                    <xsl:if test="position() = $pos + 1">
                                        <xsl:apply-templates select="." mode="columnlevel">
                                            <xsl:with-param name="count" select="1"/>
                                            <xsl:with-param name="max" select="$max"/>
                                        </xsl:apply-templates>
                                    </xsl:if>
                                </xsl:for-each>
                            </tr>
                            </xsl:if>
                        </xsl:for-each>
                </table>
             </div>
           </td>
         </tr>
    </tbody>
    </xsl:template>

  <xsl:template match="prd:UCCFilings | prd:SBCSUCCFilings" mode="columnlevel">
    <xsl:param name="count"/>
    <xsl:param name="max"/>

    <xsl:variable name="legalAction">
      <xsl:choose>
        <xsl:when test="prd:LegalAction">
          <xsl:value-of select="prd:LegalAction" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="dateFiled">
      <xsl:choose>
        <xsl:when test="prd:DateFiled">
    		   <xsl:call-template name="FormatDate">
    		     <xsl:with-param name="pattern" select="'mo/dt/year'" />
    		     <xsl:with-param name="value" select="prd:DateFiled" />
    		   </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="fileNumber">
      <xsl:choose>
        <xsl:when test="prd:DocumentNumber">
          <xsl:value-of select="prd:DocumentNumber" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="originalDate">
      <xsl:choose>
        <xsl:when test="prd:OriginalUCCFilingsInfo/prd:DateFiled and normalize-space(prd:OriginalUCCFilingsInfo/prd:DateFiled)!=''">
    		   <xsl:call-template name="FormatDate">
    		     <xsl:with-param name="pattern" select="'mo/dt/year'" />
    		     <xsl:with-param name="value" select="prd:OriginalUCCFilingsInfo/prd:DateFiled" />
    		   </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="originalNumber">
      <xsl:choose>
        <xsl:when test="prd:OriginalUCCFilingsInfo/prd:DocumentNumber">
          <xsl:value-of select="prd:OriginalUCCFilingsInfo/prd:DocumentNumber" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="originalState">
      <xsl:choose>
        <xsl:when test="prd:OriginalUCCFilingsInfo/prd:FilingState">
          <xsl:value-of select="prd:OriginalUCCFilingsInfo/prd:FilingState" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="jurisdiction">
      <xsl:choose>
        <xsl:when test="prd:FilingLocation">
          <xsl:value-of select="prd:FilingLocation" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="securedParty">
      <xsl:choose>
        <xsl:when test="prd:SecuredParty">
          <xsl:value-of select="prd:SecuredParty" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="'&#160;'" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="collateral">
      <xsl:choose>
        <xsl:when test="prd:CollateralCodes/prd:Collateral[normalize-space(@code) != '']">
          <xsl:call-template name="JoinCollateralText">
            <xsl:with-param name="nodeset" select="prd:CollateralCodes/prd:Collateral[normalize-space(@code) != '']" />
          </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="stateStatement">
      <xsl:choose>
        <xsl:when test="contains(normalize-space($jurisdiction), 'SEC OF STATE N CAROL')">
          <xsl:value-of select="'THIS DATA IS FOR INFORMATION PURPOSES ONLY. CERTIFICATION CAN ONLY BE OBTAINED THROUGH THE NORTH CAROLINA DEPARTMENT OF THE SECRETARY OF STATE.'" />
        </xsl:when>

        <xsl:otherwise>
          <xsl:value-of select="''" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

      <td>
          <xsl:if test="position() &gt; 1">
            <div>
                <img src="../images/spacer.gif" width="1" height="10" alt=""/>
            </div>
          </xsl:if>

          <xsl:if test="normalize-space($stateStatement) != ''">
            <div>
                <xsl:value-of select="$stateStatement" />
            </div>
          </xsl:if>

            <div>
              <b>UCC <xsl:value-of select="$legalAction" />
              Date: </b><xsl:value-of select="$dateFiled" />
            </div>

            <div>
              <b>Filing Number: </b><xsl:value-of select="$fileNumber" />
            </div>

          <xsl:if test="normalize-space($originalDate) != ''">
              <div>
                <b>Original Filing Date: </b><xsl:value-of select="$originalDate" />
              </div>
          </xsl:if>

          <xsl:if test="normalize-space($originalNumber) != ''">
            <div>
                <b>Original Filing Number: </b><xsl:value-of select="$originalNumber" />
            </div>
          </xsl:if>

          <xsl:if test="normalize-space($originalState) != ''">
            <div>
                <b>Original Filing State: </b><xsl:value-of select="$originalState" />
            </div>
          </xsl:if>

          <div>
              <b>Jurisdiction: </b><xsl:value-of select="$jurisdiction" />
          </div>

          <div>
              <b>Secured Party: </b><xsl:value-of select="$securedParty" />
          </div>

          <xsl:if test="normalize-space($collateral) != ''">
          <div>
              <b>Collateral: </b><xsl:value-of select="$collateral" />
          </div>
          </xsl:if>
      </td>
  </xsl:template>

</xsl:stylesheet>




