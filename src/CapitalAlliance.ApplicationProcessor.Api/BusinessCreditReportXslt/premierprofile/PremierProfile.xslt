<xsl:stylesheet  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:rsp="http://www.experian.com/NetConnectResponse"
  xmlns:prd="http://www.experian.com/ARFResponse">

	
  <!--
  *********************************************
  * Output method
  *********************************************
  -->

  <!--
  	*********************************************
  	* include template
  	*********************************************
  	-->
  	<xsl:include href="Demographic.xslt" />
  	<xsl:include href="RiskDashboard.xslt" />
  	<xsl:include href="BusinessFacts.xslt" />
  	<xsl:include href="CommercialFraudShield.xslt" />
  	<xsl:include href="CreditRiskScoreCreditLimitReccomendation.xslt" />
  	<xsl:include href="ExecutiveSummary.xslt" />
  	<xsl:include href="PaymentExperiences.xslt"/>
  	<xsl:include href="PublicRecord.xslt"/>
  	<xsl:include href="UCCProfile.xslt"/>
  	<xsl:include href="CommercialFinance.xslt"/>
  	<xsl:include href="AdditionalBusinessFacts.xslt"/>
  	<xsl:include href="CorporateLinkage.xslt"/>
  	<xsl:include href="Inquiries.xslt"/>
  	<xsl:include href="StandardAndPoors.xslt"/>
  	<xsl:include href="AccountXML.xslt"/>
	<xsl:include href="../intelliscore/IPOwnerSummary.xslt"/>
  	
	<!--
  	*********************************************
  	* PremierProfile response template
  	*********************************************
  	-->	
	<xsl:template match="prd:PremierProfile" mode="PPR" >
		<xsl:param name="standalone" select="1" />

		<xsl:choose>
			<xsl:when
				test="normalize-space(prd:ExpandedBusinessNameAndAddress/prd:ProfileType/@code) = 'NO RECORD' ">
				<xsl:call-template name="businessNotFound" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="not(boolean(number($standalone)))">
		        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
			            <tr>
			              <td valign="top" height="20" align="center"><font size="4" color="{$titleColor}"><b><a class="product_title" name="PPR">Premier Profile</a></b></font></td>
			            </tr>
		          	</table>
		        </xsl:if>
        		
				<!-- Identifying Information -->
				<xsl:call-template name="BusinessDemographic" >
			      <xsl:with-param name="reportName" select="$searchName" />
			      <xsl:with-param name="businessName" select="$searchName" />
				</xsl:call-template>
				
				<xsl:call-template name="BackToTop" />
				
				<xsl:call-template name="RiskDashboard" />
				
				<xsl:call-template name="BackToTop" />
				<!--<xsl:apply-templates select="prd:IntelliscoreScoreInformation" name="RiskDashboard" />-->
				
				<xsl:apply-templates select="prd:BusinessFacts" />
				
				<xsl:call-template name="BackToTop" />
			  	
			  	<xsl:if test="prd:CommercialFraudShieldSummary">
			  	
					<xsl:call-template name="CommercialFraudShield">
				      <xsl:with-param name="reportName" select="$searchName" />
					</xsl:call-template>
					<xsl:call-template name="BackToTop">
					  <xsl:with-param name="class" select="'hidden_on_print'" />
					</xsl:call-template>
				</xsl:if>
				
				<xsl:if test="prd:IntelliscoreScoreInformation">
					<xsl:call-template name="CreditRiskScoreDetails"/>
				</xsl:if>
				
				<xsl:call-template name="BackToTop" />
				
				<xsl:call-template name="ExecutiveSummary" />
				
				<!-- <xsl:call-template name="BackToTop" /> -->
				<xsl:call-template name="PaymentExperiences"/>
				
				<xsl:call-template name="PublicRecord"/>
				
				<xsl:call-template name="UCCProfile"/>
				
				<xsl:call-template name="CommercialFinance"/>
				
				<xsl:call-template name="AdditionalBusinessFacts"/>
				
				<xsl:call-template name="CorporateLinkage"/>
				
				<!--<xsl:call-template name="MergersAcquisitions"/>--> <!-- @TODO -->
				<xsl:call-template name="Inquiries"/>
				
				<xsl:call-template name="CompanyFinancialInformation"/>

				<xsl:if test="prd:SmallBusinessKeyModelElements">				
					  <xsl:apply-templates select="prd:SmallBusinessKeyModelElements" >
					  </xsl:apply-templates>
				</xsl:if>
				<br />

			</xsl:otherwise>
		</xsl:choose> 
	</xsl:template>
	
	
	<!--
  	*********************************************
  	* PremierProfile header template
  	*********************************************
  	-->
  	<xsl:template match="prd:PremierProfile" mode="header" >
    	<!-- Report Header -->
	    <xsl:call-template name="ReportHeader">
	    	<xsl:with-param name="reportType" select="$reportType" />
      		<xsl:with-param name="reportName" select="prd:ExpandedBusinessNameAndAddress/prd:BusinessName" />
	      	<xsl:with-param name="reportDate" select="prd:ExpandedBusinessNameAndAddress/prd:ProfileDate" />
	      	<xsl:with-param name="reportTime" select="prd:ExpandedBusinessNameAndAddress/prd:ProfileTime" />
	    </xsl:call-template>
	    
    <br />
  	</xsl:template>
  	
  	<!--
  	*********************************************
  	* PremierProfile footer template
  	*********************************************
  	-->
  	<xsl:template match="prd:PremierProfile" mode="footer" >
    	<!-- Report Footer -->
    	<xsl:call-template name="ReportFooter">
      		<xsl:with-param name="reportDate" select="prd:ExpandedBusinessNameAndAddress/prd:ProfileDate" />
    	</xsl:call-template>
  	</xsl:template>
	
</xsl:stylesheet>