<xsl:stylesheet  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:rsp="http://www.experian.com/NetConnectResponse"
  xmlns:prd="http://www.experian.com/ARFResponse">

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
  <xsl:output method="html"
    doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
    doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
    indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * Global variables and parameters
  *********************************************
  -->
  
  <xsl:param name="product" select="'PPR'" />
  <xsl:param name="baseProduct" select="'PPR'" />
  <xsl:param name="reportType" select="'Premier Profile'" />
  <xsl:param name="defaultbarchart" select="'../images/default_bar_chart.gif'" />

  <xsl:variable name="searchName">
   <xsl:choose>
    <xsl:when test="//prd:PremierProfile/prd:ExpandedBusinessNameAndAddress/prd:BusinessName">
     <xsl:value-of select="normalize-space(//prd:PremierProfile/prd:ExpandedBusinessNameAndAddress/prd:BusinessName)" />
    </xsl:when>
    <xsl:otherwise>
     <xsl:choose>
      <xsl:when
       test="//prd:PremierProfile/prd:ExpandedBusinessNameAndAddress/prd:LegalName/prd:LegalBusinessName">
       <xsl:value-of
        select="normalize-space(//prd:PremierProfile/prd:ExpandedBusinessNameAndAddress/prd:LegalName/prd:LegalBusinessName)" />
      </xsl:when>
      <xsl:otherwise>
       <xsl:value-of select="''" />
      </xsl:otherwise>
     </xsl:choose>
    </xsl:otherwise>
   </xsl:choose>
  </xsl:variable>
  
  
  <xsl:variable name="FusionChartHeight">
    <xsl:value-of select="'300px'" />
  </xsl:variable>
  

  <!--
  *********************************************
  * include template
  *********************************************
  -->
  
  <xsl:include href="../common/ReportHeader.xslt" />
  <xsl:include href="../common/Util.xslt" />
  <xsl:include href="../common/case.xslt" />
  <xsl:include href="../common/BackToTop.xslt" />
  <xsl:include href="../common/ErrorProcessing.xslt" />
  <xsl:include href="../common/ReportFooter.xslt" />
  
  <!--
    *********************************************
    * include template
    *********************************************
    -->
    
     <xsl:include href="PremierProfile.xslt" />
  
    
  <!--
  *********************************************
  * Initial template
  *********************************************
  -->
  <xsl:template match="/">
    <html>
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>PPR</title>

        <xsl:call-template name="ReportCSS" />
      </head>

      <body>
        <a name="top"></a>
        <table class="report_container" width="715" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td>      
               
                <!-- PPR header template -->
                <xsl:apply-templates select="//prd:PremierProfile" mode="header" />
                
                <!-- PPR template -->
                <xsl:apply-templates select="//prd:PremierProfile" mode="PPR"/>
                
                
                <!-- PPR BOP footer template -->
                <xsl:apply-templates select="//prd:PremierProfile" mode="footer" />
              
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
  

  <!--
  *********************************************
  * this template overrides the built-in rule
  * do nothing with stuff I'm not interested in
  *********************************************
  -->
  <xsl:template match="text()|@*">
  </xsl:template>

</xsl:stylesheet>