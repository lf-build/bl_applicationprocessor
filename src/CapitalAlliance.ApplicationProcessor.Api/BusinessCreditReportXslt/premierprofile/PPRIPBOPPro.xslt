<xsl:stylesheet  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:rsp="http://www.experian.com/NetConnectResponse"
  xmlns:prd="http://www.experian.com/ARFResponse">

  <!--
  *********************************************
  * Output method
  *********************************************
  -->
	 <xsl:output method="html"
		doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN"
		doctype-system="http://www.w3c.org/TR/xhtml/DTD/xhtml1-strict.dtd"
		indent="yes" encoding="UTF-8" />


  <!--
  *********************************************
  * Global variables and parameters
  *********************************************
  -->
  
  <xsl:param name="product" select="'PPRIPBOP'" />
  <xsl:param name="baseProduct" select="'PPR'" />
  <xsl:param name="reportType" select="'PPR with BOP and Blended Score'" />
  <xsl:param name="defaultbarchart" select="'../images/default_bar_chart.gif'" />

  <xsl:variable name="searchName">
   <xsl:choose>
    <xsl:when test="//prd:PremierProfile/prd:ExpandedBusinessNameAndAddress/prd:BusinessName">
     <xsl:value-of select="normalize-space(//prd:PremierProfile/prd:ExpandedBusinessNameAndAddress/prd:BusinessName)" />
    </xsl:when>
    <xsl:otherwise>
     <xsl:choose>
      <xsl:when
       test="//prd:PremierProfile/prd:ExpandedBusinessNameAndAddress/prd:LegalName/prd:LegalBusinessName">
       <xsl:value-of
        select="normalize-space(//prd:PremierProfile/prd:ExpandedBusinessNameAndAddress/prd:LegalName/prd:LegalBusinessName)" />
      </xsl:when>
      <xsl:otherwise>
       <xsl:value-of select="''" />
      </xsl:otherwise>
     </xsl:choose>
    </xsl:otherwise>
   </xsl:choose>
  </xsl:variable>
  
  
  <xsl:variable name="FusionChartHeight">
    <xsl:value-of select="'300px'" />
  </xsl:variable>
  

  <!--
  *********************************************
  * include template
  *********************************************
  -->
  
  <xsl:include href="../common/ReportHeader.xslt" />
  <xsl:include href="../common/Util.xslt" />
  <xsl:include href="../common/case.xslt" />
  <xsl:include href="../common/BackToTop.xslt" />
  <xsl:include href="../common/ErrorProcessing.xslt" />
  <xsl:include href="../common/ReportFooter.xslt" />
  
  <!--
  	*********************************************
  	* include template
  	*********************************************
  	-->
	<xsl:include href="IPBlendedScorePro.xslt" />
    <xsl:include href="PremierProfilePro.xslt" />
  	<xsl:include href="../ownerprofile/CreditProfile.xslt" />
  
	
  <!--
  *********************************************
  * Initial template
  *********************************************
  -->
  <xsl:template match="/">
    <html>
      <head>
        <title>PPR with BOP and Blended Score</title>

        <xsl:call-template name="ReportCSS" />

		<!-- Google Charts -->
		<!--Load the AJAX API-->
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script type="text/javascript" src="../common/loadxmldoc.js"></script>
		<script type="text/javascript" src="../common/drawBarChartsPPR.js"></script>

      </head>

      <body>
        <a name="top"></a>
        <table class="report_container" width="715" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td>      
               
              	<!-- PPR BOP header template -->
				<xsl:apply-templates select="//prd:PremierProfile" mode="header" />					
				
				<xsl:if test="normalize-space(//prd:ProprietorNameAndAddress/prd:ProfileType/@code) != 'NO RECORD'">						
					<xsl:apply-templates select="//prd:PremierProfile" mode="IPBlended">
						<xsl:with-param name="standalone" select="0" />
					</xsl:apply-templates>
				</xsl:if>	
				
				<!-- PPR template -->
				<xsl:apply-templates select="//prd:PremierProfile" mode="PPR">
					<xsl:with-param name="standalone" select="0" />
				</xsl:apply-templates>
				
				<!-- BOP template -->
				<xsl:if test="//prd:CreditProfile"> 
		      		<xsl:call-template name="BackToTop" />
		      		<xsl:apply-templates select="//prd:CreditProfile">
		      			<xsl:with-param name="standalone" select="0" /> 
		    		</xsl:apply-templates>
		    	</xsl:if>
		    	
		    	<!-- PPR BOP footer template -->
				<xsl:apply-templates select="//prd:PremierProfile" mode="footer" />
              
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
  

  <!--
  *********************************************
  * this template overrides the built-in rule
  * do nothing with stuff I'm not interested in
  *********************************************
  -->
  <xsl:template match="text()|@*">
  </xsl:template>


</xsl:stylesheet>