<xsl:stylesheet  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:rsp="http://www.experian.com/NetConnectResponse"
  xmlns:prd="http://www.experian.com/ARFResponse">

	
	<!--
	*********************************************
	* Output method
	*********************************************
	-->

	
	<!--
  	*********************************************
  	* include template
  	*********************************************
  	-->
	<xsl:include href="RiskDashboardBlended.xslt" />


	<!--
  	*********************************************
  	* SupplierProfile response template
  	*********************************************
  	-->	
	<xsl:template match="prd:PremierProfile" mode="IPBlended">	
		<xsl:param name="standalone" select="1" />
		<xsl:variable name="tmpIdx">
		  <xsl:value-of select="position()" />
		</xsl:variable>
		
	
		<xsl:choose>
			<xsl:when
				test="normalize-space(prd:ExpandedBusinessNameAndAddress/prd:ProfileType/@code) = 'NO RECORD' ">
				<xsl:call-template name="businessNotFound" />
			</xsl:when>
			<xsl:otherwise>

				<!-- Admin Demo Statement -->
        		<xsl:call-template name="AdminDemoNote" />
			
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
			        <tr>
			           <td valign="top" height="20" align="center"><b><a class="product_title" name="IPBlended">Blended Score</a></b></td>
			        </tr>
		       	</table>
				
				<xsl:call-template name="ConsumerSectionsDisclaimerHeader"/>
				
	        	<!-- Identifying Information -->
				<xsl:call-template name="BusinessDemographic" >
				      <xsl:with-param name="reportName" select="$searchName" />
				      <xsl:with-param name="businessName" select="$searchName" />
				</xsl:call-template>
				
				<xsl:call-template name="BackToTop" />
				
				<xsl:call-template name="RiskDashboardBlended" />
	        	
				<xsl:call-template name="ExecutiveSummary">
					<xsl:with-param name="isBlended" select="1" />
				</xsl:call-template>	
	        								


			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
  	

</xsl:stylesheet>