﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Hosting;

namespace CapitalAlliance.ApplicationProcessor.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {

            var host = new WebHostBuilder()
                .UseKestrel()
                .UseStartup<Startup>()
                .Start("http://*:5000");

            using (host)
            {
                Console.WriteLine("Use Ctrl-C to shutdown the host...");
                host.WaitForShutdown();
            }

        }


    }
}
