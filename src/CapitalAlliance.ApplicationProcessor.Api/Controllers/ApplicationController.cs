﻿using CapitalAlliance.ApplicationProcessor.Request;
using LendFoundry.Business.OfferEngine;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http.Headers;
// using Microsoft.Net.Http.Headers;
using RestSharp.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CapitalAlliance.Cashflow;
using LendFoundry.ProductRule;
using CapitalAlliance.ApplicationProcessor.Exceptions;
using LendFoundry.Security.Identity;
using LendFoundry.Business.Applicant;
using LendFoundry.Syndication.Experian.Response;
using LendFoundry.Syndication.LexisNexis.Bankruptcy;
using LendFoundry.Syndication.LexisNexis.ADLFunctions;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport;
using LendFoundry.Syndication.Paynet.Response;
using LendFoundry.Business.Application;

namespace CapitalAlliance.ApplicationProcessor.Api.Controllers
{
    [Route("/application")]
    public class ApplicationController : ExtendedController
    {
        #region Constructor

        public ApplicationController(IApplicationProcessorClientService applicationService)
        {
            if (applicationService == null)
                throw new ArgumentNullException(nameof(applicationService));

            ApplicationService = applicationService;
        }

        #endregion Constructor

        #region Private Variables

        private IApplicationProcessorClientService ApplicationService { get; }

        #endregion Private Variables

        #region Submit Application

        [ProducesResponseType(typeof(IApplicationResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpPost("submit/business")]
        public async Task<IActionResult> SubmitApplication([FromBody]SubmitBusinessApplicationRequest submitBusinessApplicationRequest)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await ApplicationService.SubmitBusinessApplication(submitBusinessApplicationRequest)));
            }
            catch (ApplicantAlreadyAssociateWithApplication exception)
            {
                return new ErrorResult(409, exception.Message);
            }
            catch (UsernameAlreadyExists exception)
            {
                return new ErrorResult(409, exception.Message);
            }
            catch (UnableToCreateUserException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        #endregion Submit Application

        #region Add Lead
        [ProducesResponseType(typeof(IApplicationDetails), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpPost("lead")]
        public async Task<IActionResult> AddLead([FromBody]SubmitBusinessApplicationRequest submitApplicationRequest)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await ApplicationService.AddLead(submitApplicationRequest)));
            }
            catch (ApplicantAlreadyAssociateWithApplication exception)
            {
                return new ErrorResult(409, exception.Message);
            }
            catch (UsernameAlreadyExists exception)
            {
                return new ErrorResult(409, exception.Message);
            }
            catch (UnableToCreateUserException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        #endregion Add Lead

        #region Submit Document
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpPost("{entityid}/{category}/{*tags}")]
        public async Task<IActionResult> SubmitDocument(string entityid, string category, IFormFile file, string tags)
        {
            if (file == null)
                return ErrorResult.BadRequest("Please upload file.");

            var fileName = ContentDispositionHeaderValue
                .Parse(file.ContentDisposition)
                .FileName
                .Trim('"');

            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.SubmitDocument(entityid, category,
                            file.OpenReadStream().ReadAsBytes(), fileName, TagValidation(tags))));
        }

        #endregion Submit Document

        #region Sign Consent
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpPost("{entityid}/{category}/consent/sign")]
        public async Task<IActionResult> Sign(string entityid, string category, [FromBody]object body)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.SignConsent(entityid, category, body)));
        }

        #endregion Sign Consent

        #region Get Business Report
        [ProducesResponseType(typeof(ISearchBusinessResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpPost("{entityid}/experian/business/search")]
        public async Task<IActionResult> BusinessSearch(string entityid, [FromBody]BusinessSearchRequest request)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.BusinessSearch(entityid, request)));
        }

        [ProducesResponseType(typeof(IBusinessReportResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpPost("{entityid}/experian/business/report")]
        public async Task<IActionResult> GetBusinessReport(string entityid, [FromBody]BusinessReportRequest request)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.GetBusinessReport(entityid, request)));
        }

        #endregion Get Business Report

        #region Get Personal Report
        [ProducesResponseType(typeof(IBusinessReportResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpPost("{entityid}/report/personal")]
        public async Task<IActionResult> GetPersonalReport(string entityid, [FromBody]PersonalReportRequest request)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.GetPersonalReport(entityid, request)));
        }

        #endregion Get Personal Report

        #region Choose Final Offer
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpGet("{entityid}/{offerId}/offers/final")]
        public async Task<IActionResult> ChooseOffer(string entityid, string offerId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.ChooseOffer(entityid, offerId)));
        }

        #endregion Choose Final Offer

        #region Perform Business Verification 1
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpGet("{entityid}/businessverification1")]
        public async Task<IActionResult> PerformBusinessVerification1(string entityId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.PerformBusinessVerification1(entityId)));
        }

        #endregion Perform Business Verification 1

        #region Perform Business Verification 2
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpGet("{entityid}/businessverification2")]
        public async Task<IActionResult> PerformBusinessVerification2(string entityId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.PerformBusinessVerification2(entityId)));
        }

        #endregion Perform Business Verification 2

        #region Resend Bank Linking
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpPost("{entityid}/resend/banklink/{emailId?}")]
        public async Task<IActionResult> ResendBankLinking(string entityId,string emailId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.ResendBankLinking(entityId,emailId)));
        }

        #endregion Resend Bank Linking

        #region Compute Offers
        [ProducesResponseType(typeof(IProductRuleResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpGet("{entityid}/compute/offer")]
        public async Task<IActionResult> ComputeOffer(string entityId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.ComputeOffer(entityId)));
        }

        [ProducesResponseType(typeof(IProductRuleResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpPost("{entityid}/pricingcalculator")]
        public async Task<IActionResult> GetComputedOffer(string entityid, [FromBody]object body)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.GetComputedOffer(entityid, body)));
        }

        #endregion Compute Offers        


        #region Present Offer

        [ProducesResponseType(typeof(IProductRuleResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpGet("{entityid}/present/offer")]
        public async Task<IActionResult> PresentOffer(string entityId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.PresentOffer(entityId)));
        }

        #endregion Present Offer

        #region Generate And Send Agreement


        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpGet("{entityid}/generate/agreement")]
        public async Task<IActionResult> GenerateAndSendAgreement(string entityId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.GenerateAndSendAgreement(entityId)));
        }
        #endregion Generate And Send Agreement


        #region Private Methods

        private static List<string> TagValidation(string tags)
        {
            if (string.IsNullOrWhiteSpace(tags))
                return null;

            tags = WebUtility.UrlDecode(tags);
            var tagList = SplitTags(tags);
            return tagList.ToList();
        }

        private static IEnumerable<string> SplitTags(string tags)
        {
            return string.IsNullOrWhiteSpace(tags)
                ? new List<string>()
                : tags.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        #endregion Private Methods

        #region Paynet

        #region Search Company

        [ProducesResponseType(typeof(ISearchCompanyResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpPost("{entityid}/paynet/search")]
        public async Task<IActionResult> SearchPaynetCompany(string entityid, [FromBody]SearchCompanyRequest request)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.SearchCompany(entityid, request)));
        }

        #endregion Search Company

        #region Get Company Report

        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpGet("{entityid}/{paynetId}/paynet/report")]
        public async Task<IActionResult> GetPaynetReport(string entityid, string paynetId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.GetPaynetReport(entityid, paynetId)));
        }

        #endregion Get Company Report

        #endregion Paynet

        #region Perform CreditCheck Verification

        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpGet("{entityid}/creditcheckverification")]
        public async Task<IActionResult> PerformCreditCheckVerification(string entityId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.PerformCreditCheckVerification(entityId)));
        }

        #endregion Perform CreditCheck Verification

        #region Perform Business Tax Id Verification

        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpGet("{entityid}/businesstaxidverification")]
        public async Task<IActionResult> PerformBusinessTaxIdVerification(string entityId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.PerformBusinessTaxIdVerification(entityId)));
        }

        #endregion Perform Business Tax Id Verification

        #region Perform Business Review CreditRisk

        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpGet("{entityid}/businessreviewcreditrisk")]
        public async Task<IActionResult> PerformBusinessReviewCreditRisk(string entityId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.PerformBusinessReviewCreditRisk(entityId)));
        }

        #endregion Perform Business Review CreditRisk

        #region Get Application Details

        [ProducesResponseType(typeof(IApplicationDetails), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpGet("{entityid}/details")]
        public async Task<IActionResult> ApplicationDetails(string entityid)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.GetApplicationDetails(entityid)));
        }

        #endregion Get Application Details

        #region Edit Application

        [ProducesResponseType(typeof(IApplicationDetails), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpPut("{entityid}/edit")]
        public async Task<IActionResult> EditApplication(string entityid, [FromBody]SubmitBusinessApplicationRequest submitBusinessApplicationRequest)
        {
            return await ExecuteAsync(async () => Ok(await ApplicationService.EditApplication(entityid, submitBusinessApplicationRequest)));
        }

        #endregion Edit Application

        #region Add Deal

        [ProducesResponseType(typeof(IApplicationOffer), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpPost("{entityid}/add/deal")]
        public async Task<IActionResult> AddDeal(string entityId, [FromBody]DealOfferRequest request)
        {
            return await ExecuteAsync(async () => Ok(await ApplicationService.AddDeal(entityId, request)));
        }

        #endregion Add Deal

        #region LexisNexis

         #region ADL search

        [ProducesResponseType(typeof(IADLSearchResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
       [HttpPost("{entityId}/adl/search")]
        public async Task<IActionResult> ADLSearch(string entityid, [FromBody] CapitalAlliance.ApplicationProcessor.Request.SearchADLRequest request)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.ADLSearch(entityid, request)));
        }

        #endregion ADL search

        #region Bankruptcy search

        [ProducesResponseType(typeof(IBankruptcySearchResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpPost("{entityid}/bankruptcy/search")]
        public async Task<IActionResult> BankruptcySearch(string entityid, [FromBody] CapitalAlliance.ApplicationProcessor.Request.SearchBankruptcyRequest request)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.BankruptcySearch(entityid, request)));
        }

        #endregion Bankruptcy search

        #region Bankruptcy Report

        [ProducesResponseType(typeof(IBankruptcyReportResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpPost("{entityid}/bankruptcy/report")]
        public async Task<IActionResult> BankruptcyReport(string entityid, [FromBody] CapitalAlliance.ApplicationProcessor.Request.BankruptcyReportRequest request)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.BankruptcyReport(entityid, request)));
        }

        #endregion Bankruptcy Report

        #region Criminal Search

        [ProducesResponseType(typeof(ICriminalRecordSearchResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpPost("{entityid}/criminalrecord/search")]
        public async Task<IActionResult> CriminalRecordSearch(string entityid, [FromBody] CriminalRecordSearchRequest request)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.CriminalRecordSearch(entityid, request)));
        }

        #endregion Criminal Search

        #region Criminal Report

        [ProducesResponseType(typeof(ICriminalReportResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpPost("{entityid}/criminalrecord/report")]
        public async Task<IActionResult> CriminalRecordReport(string entityid, [FromBody] CriminalRecordReportRequest request)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.CriminalRecordReport(entityid, request)));
        }

        #endregion Criminal Report

        #endregion LexisNexis

        #region fund application

        [ProducesResponseType(typeof(Task), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpPost("{entityid}/add/fundingrequest")]
        public async Task<IActionResult> AddFundingRequest(string entityid)
        {
            return await ExecuteAsync(async () =>
            {
                await ApplicationService.AddFundingRequest(entityid);
                return Ok();
            });
        }
        #endregion


        [ProducesResponseType(typeof(Task), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpPut("{entityid}/update/SicCode/{sicCode}")]
        public async Task<IActionResult> UpdateSICCode(string entityid, string sicCode)
        {
            return await ExecuteAsync(async () =>
            {
                await ApplicationService.UpdateSICCode(entityid, sicCode);
                return Ok();
            });
        }

        [ProducesResponseType(typeof(object), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpGet("{entityid}/{documentName}")]
        public async Task<IActionResult> GetApplicationDocuments(string entityId, string documentName)
        {
            return await ExecuteAsync(async () => Ok(await ApplicationService.GetApplicationDocuments(entityId, documentName)));
        }
        #region CashFlowData

        [ProducesResponseType(typeof(ICashFlowResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpGet("{entityid}/account/cashflow")]
        public async Task<IActionResult> GetcashflowAccountDetails(string entityId)
        {
            return await ExecuteAsync(async () => Ok(await ApplicationService.GetcashflowAccountDetails(entityId)));
        }

        [ProducesResponseType(typeof(ICashFlowResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpGet("{entityid}/account/funding")]
        public async Task<IActionResult> GetfundingAccountDetails(string entityId)
        {
            return await ExecuteAsync(async () => Ok(await ApplicationService.GetFundingAccountDetails(entityId)));
        }
        #endregion

        #region Switch Owner

        [ProducesResponseType(typeof(Task), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpPost("{entityid}/switch/{ownerId}")]
        public async Task<IActionResult> SwitchOwner(string entityid, string ownerId)
        {
            return await ExecuteAsync(async () =>
            {
                await ApplicationService.SwitchOwner(entityid, ownerId);
                return Ok();
            });
        }
        #endregion

        #region Compute Provisional Offers

        [ProducesResponseType(typeof(Dictionary<string, object>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpGet("{entityid}/compute/provisional/offer")]
        public async Task<IActionResult> ComputeProvisionalOffer(string entityId)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.ComputeProvisionalOffer(entityId)));
        }
        #endregion  

        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpGet("{entityId}/re_apply_check")]
        public async Task<IActionResult> CheckReApplyEligibility(string entityId)
        {
            return await ExecuteAsync(async () => Ok(await ApplicationService.CheckReApplyEligibility(entityId)));
        }

        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpGet("{entityId}/revival_check")]
        public async Task<IActionResult> CheckRevivalEligibility(string entityId)
        {
            return await ExecuteAsync(async () => Ok(await ApplicationService.CheckRevivalEligibility(entityId)));
        }

        [ProducesResponseType(typeof(Task), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpPost("{entityId}/revive")]
        public async Task<IActionResult> ReviveApplication(string entityId)
        {
            return await ExecuteAsync(async () =>
            {
                await ApplicationService.ReviveApplication(entityId);
                return Ok();
            });
        }

        [ProducesResponseType(typeof(Task), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpPost("{entityId}/enable/reapply")]
        public async Task<IActionResult> EnableReapply(string entityId)
        {
            return await ExecuteAsync(async () =>
            {
                await ApplicationService.EnableReapply(entityId);
                return Ok();
            });
        }


        #region Extrcted Data

        [ProducesResponseType(typeof(object), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpGet("{entityid}/getextracteddata/{key}")]
        public async Task<IActionResult> GetExtractedData(string entityId,string key)
        {
            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationService.GetExtractedData(entityId,key)));
            
        }

        #endregion Extrcted Data
    }
}