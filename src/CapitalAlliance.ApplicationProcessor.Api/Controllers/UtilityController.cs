﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;
using CapitalAlliance.ApplicationProcessor.Request;

namespace CapitalAlliance.ApplicationProcessor.Api.Controllers
{
    [Route("/utility")]
    public class UtilityController : ExtendedController
    {
        #region Constructor
        public UtilityController(IApplicationProcessorClientService applicationService)
        {
            if (applicationService == null)
                throw new ArgumentNullException(nameof(applicationService));

            ApplicationService = applicationService;
        }

        #endregion Constructor

        #region Private Variables

        private IApplicationProcessorClientService ApplicationService { get; }

        #endregion Private Variables
        [ProducesResponseType(typeof(Task), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpPost("add/attribute/product")]
        public async Task<IActionResult> AddAttribute([FromBody]AddAttributeRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                await ApplicationService.AddAttribute(request);
                return Ok();
            });
        }
    }
}
