﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using LendFoundry.StatusManagement;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;

namespace CapitalAlliance.ApplicationProcessor.Api.Controllers
{
    [Route("/status-management")]
    public class StatusManagementController : ExtendedController
    {

        public StatusManagementController(IStatusManagementService statusManagementService)
        {
            if (statusManagementService == null)
                throw new ArgumentNullException(nameof(statusManagementService));

            StatusManagementService = statusManagementService;
        }

        private IStatusManagementService StatusManagementService { get; set; }

        [ProducesResponseType(typeof(Task), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpPost("{applicationNumber}/{newStatus}/{*reasons}")]
        public async Task<IActionResult> ChangeStatus(string applicationNumber, string newStatus, string reasons)
        {
            return await ExecuteAsync(async () =>
            {
                await StatusManagementService.ChangeStatus(applicationNumber, newStatus, Reasons(reasons));
                return Ok();
            });
        }

        [ProducesResponseType(typeof(Task), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpPost("{applicationNumber}/reject/{*reasons}")]
        public async Task<IActionResult> RejectApplication(string applicationNumber, string reasons)
        {
            return await ExecuteAsync(async () =>
            {
                await StatusManagementService.RejectApplication(applicationNumber, Reasons(reasons));
                return Ok();
            });
        }


        [ProducesResponseType(typeof(Task), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpPost("{applicationNumber}/applicationapproved")]
        public async Task<IActionResult> ApplicationStatusMove(string applicationNumber)
        {
            return await ExecuteAsync(async () =>
            {
                await StatusManagementService.ApplicationStatusMove(applicationNumber);
                return Ok();
            });
        }     

        [ProducesResponseType(typeof(IEnumerable<IActivity>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 409)]
        [HttpGet("{applicationNumber}/activities")]
        public async Task<IActionResult> GetActivities(string applicationNumber)
        {
            return await ExecuteAsync(async () =>
            {

                return Ok(await StatusManagementService.GetActivities(applicationNumber));
            });
        }

        private static IEnumerable<string> SplitReasons(string reasons)
        {
            return string.IsNullOrWhiteSpace(reasons)
                ? new List<string>()
                : reasons.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }


        private static List<string> Reasons(string tags)
        {
            if (string.IsNullOrWhiteSpace(tags))
                return null;

            tags = WebUtility.UrlDecode(tags);
            var tagList = SplitReasons(tags);
            return tagList.ToList();
        }
    }
}
